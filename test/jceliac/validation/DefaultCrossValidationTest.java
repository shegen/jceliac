/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.validation;

import jceliac.validation.generic.DefaultCrossValidation;
import jceliac.validation.generic.ValidationMethodParameters;
import jceliac.validation.generic.ValidationMethod;
import java.util.ArrayList;
import java.util.Random;
import jceliac.classification.ClassificationResult;
import jceliac.classification.classificationParameters.ClassificationParametersKNN;
import jceliac.classification.model.GenerativeModel;
import jceliac.classification.model.NullGenerativeModel;
import jceliac.classification.model.NullGenerativeModelParameters;
import jceliac.classification.KNN.ClassificationMethodKNN;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureOptimization.FeatureOptimizationMethodNULL;
import jceliac.featureOptimization.FeatureOptimizationMethodSFS;
import jceliac.featureOptimization.FeatureOptimizationParametersNULL;
import jceliac.featureOptimization.FeatureOptimizationParametersSFS;
import jceliac.tools.timer.PerformanceTimer;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author shegen
 */
public class DefaultCrossValidationTest
{
    
    /**
     * Test of validateOuter method, of class DefaultCrossValidation.
     */
 /*   @Test
    public void testValidateOuter() 
            throws Exception
    {
        int classCount = 10;
        int numFeatures = 40;
        // generate random feaures
        @SuppressWarnings("unchecked")
        Random rand = new Random(1234567890);
        ArrayList<DiscriminativeFeatures> trainingFeatures = DistinctSetValidationTest.generateRandomFeatures(classCount, numFeatures, 0.15, 1, rand);
     
        ClassificationParametersKNN params = new ClassificationParametersKNN();
        params.setkMin(1);
        params.setkMax(20);
        params.setNeighborEvaluationMode(ClassificationParametersKNN.ENeighborEvaluationMode.BEST_K_VALUE);
        params.setCrossValidationType("LOOCV");

        ClassificationMethodKNN knn = new ClassificationMethodKNN(params);
        ValidationMethodParameters validationParams = new ValidationMethodParameters();
        validationParams.optimizationType = "OUTER";
        validationParams.crossValidationType = "LOO";

        FeatureOptimizationParametersNULL optParams = new FeatureOptimizationParametersNULL();
        FeatureOptimizationMethodNULL optMethod = new FeatureOptimizationMethodNULL(optParams);

        GenerativeModel model = new NullGenerativeModel(new NullGenerativeModelParameters());
        ValidationMethod validation = new DefaultCrossValidation(validationParams,
                                                                  knn,
                                                                  model,
                                                                  optMethod);
        
        PerformanceTimer timer = new PerformanceTimer();
        timer.startTimer();
        ClassificationResult result = validation.validate(trainingFeatures, null);
        double ocr = result.getOverallClassificationRate();
        System.out.println("validateOuter:" +ocr);
        timer.stopTimer();
        System.out.println("Outer CV in: "+timer.getSeconds());
        assertEquals(ocr, 0.88, 0.01);
    }*/
    
    
    /**
     * Test of validateOuter method, of class DefaultCrossValidation.
     */
/*    @Test
    public void testValidateInner() 
            throws Exception
    {
        int classCount = 10;
        int numFeatures = 40;
        // generate random feaures
        @SuppressWarnings("unchecked")
        Random rand = new Random(1234567890);
        ArrayList<DiscriminativeFeatures> trainingFeatures = DistinctSetValidationTest.generateRandomFeatures(classCount, numFeatures, 0.15, 1, rand);
     
        ClassificationParametersKNN params = new ClassificationParametersKNN();
        params.setkMin(1);
        params.setkMax(20);
        params.setNeighborEvaluationMode(ClassificationParametersKNN.ENeighborEvaluationMode.BEST_K_VALUE);
        params.setCrossValidationType("LOOCV");

        ClassificationMethodKNN knn = new ClassificationMethodKNN(params);
        ValidationMethodParameters validationParams = new ValidationMethodParameters();
        validationParams.optimizationType = "INNER";
        validationParams.crossValidationType = "LOO";

        FeatureOptimizationParametersSFS optParams = new FeatureOptimizationParametersSFS();
        FeatureOptimizationMethodSFS optMethod = new FeatureOptimizationMethodSFS(optParams);

        GenerativeModel model = new NullGenerativeModel(new NullGenerativeModelParameters());
        ValidationMethod validation = new DefaultCrossValidation(validationParams,
                                                                  knn,
                                                                  model,
                                                                  optMethod);
        
        PerformanceTimer timer = new PerformanceTimer();
        timer.startTimer();
        ClassificationResult result = validation.validate(trainingFeatures, null);
        double ocr = result.getOverallClassificationRate();
        System.out.println("validateInner:" +ocr);
        timer.stopTimer();
        System.out.println("Inner CV in: "+timer.getSeconds());
        assertEquals(ocr, 0.875, 0.01);
    }
    */
}
