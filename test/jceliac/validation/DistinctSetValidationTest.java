/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.validation;

import java.util.ArrayList;
import java.util.Random;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.AbstractFeatureVector;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FixedFeatureVector;
import jceliac.featureExtraction.StandardEuclideanFeatures;
import jceliac.featureExtraction.fractal.multiFractalSpectrum.MultiFractalSpectrumMR8Features;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;
/**
 *
 * @author shegen
 */
public class DistinctSetValidationTest
{

    /**
     * Test of validateOuter method, of class DistinctSetValidation.
     * <p>
     * @throws java.lang.Exception
     */
    /*@Test
    public void testValidateOuterNULL()
            throws Exception
    {
        int classCount = 10;
        int numFeatures = 100;
        // generate random feaures
        @SuppressWarnings("unchecked")
        Random rand = new Random(1234567890);
        ArrayList<DiscriminativeFeatures> trainingFeatures = DistinctSetValidationTest.generateRandomFeatures(classCount, numFeatures, 0.15, 1, rand);
        ArrayList<DiscriminativeFeatures> evaluationFeatures = DistinctSetValidationTest.generateRandomFeatures(classCount, numFeatures, 0.15, 1, rand);

        ClassificationParametersKNN params = new ClassificationParametersKNN();
        params.setkMin(1);
        params.setkMax(20);
        params.setNeighborEvaluationMode(ClassificationParametersKNN.ENeighborEvaluationMode.BEST_K_VALUE);
        params.setCrossValidationType("DISTINCT");

        ClassificationMethodKNN knn = new ClassificationMethodKNN(params);
        ValidationMethodParameters validationParams = new ValidationMethodParameters();
        validationParams.optimizationType = "OUTER";

        FeatureOptimizationParametersNULL optParams = new FeatureOptimizationParametersNULL();
        FeatureOptimizationMethodNULL optMethod = new FeatureOptimizationMethodNULL(optParams);

        GenerativeModel model = new NullGenerativeModel(new NullGenerativeModelParameters());
        ValidationMethod validation = new DistinctSetValidation(validationParams,
                                                                  knn,
                                                                  model,
                                                                  optMethod);
        ClassificationResult result = validation.validate(trainingFeatures, evaluationFeatures);
        double ocr = result.getOverallClassificationRate();

        assertEquals(ocr, 0.895, 0.001);
    }*/

    /**
     * Test of validateOuter method, of class DistinctSetValidation.
     * <p>
     * @throws java.lang.Exception
     */
  /*  @Test
    public void testValidateInnerNULL()
            throws Exception
    {
        int classCount = 10;
        int numFeatures = 100;
        // generate random feaures
        @SuppressWarnings("unchecked")
        Random rand = new Random(1234567890);
        ArrayList<DiscriminativeFeatures> trainingFeatures = DistinctSetValidationTest.generateRandomFeatures(classCount, numFeatures, 0.15, 1, rand);
        ArrayList<DiscriminativeFeatures> evaluationFeatures = DistinctSetValidationTest.generateRandomFeatures(classCount, numFeatures, 0.15, 1, rand);

        ClassificationParametersKNN params = new ClassificationParametersKNN();
        params.setkMin(1);
        params.setkMax(20);
        params.setNeighborEvaluationMode(ClassificationParametersKNN.ENeighborEvaluationMode.BEST_K_VALUE);
        params.setCrossValidationType("DISTINCT");

        ClassificationMethodKNN knn = new ClassificationMethodKNN(params);
        ValidationMethodParameters validationParams = new ValidationMethodParameters();
        validationParams.optimizationType = "INNER";

        FeatureOptimizationParametersNULL optParams = new FeatureOptimizationParametersNULL();
        FeatureOptimizationMethodNULL optMethod = new FeatureOptimizationMethodNULL(optParams);

        GenerativeModel model = new NullGenerativeModel(new NullGenerativeModelParameters());
        ValidationMethod validation = new DistinctSetValidation(validationParams,
                                                                  knn,
                                                                  model,
                                                                  optMethod);
        ClassificationResult result = validation.validate(trainingFeatures, evaluationFeatures);
        double ocr = result.getOverallClassificationRate();
    }
*/
  /*  @Test
    public void testValidateInnerSFS()
            throws Exception
    {
        int classCount = 10;
        int numFeatures = 100;
        double bias1 = 0.1;
        double bias2 = 0.1;
        
        // generate random feaures
        @SuppressWarnings("unchecked")
        Random rand = new Random(1234567890);
        ArrayList<DiscriminativeFeatures> trainingFeatures = DistinctSetValidationTest.generateRandomFeatures(classCount, numFeatures, bias1, 10, rand);
        ArrayList<DiscriminativeFeatures> evaluationFeatures = DistinctSetValidationTest.generateRandomFeatures(classCount, numFeatures, bias2, 10, rand);

        ClassificationParametersKNN params = new ClassificationParametersKNN();
        params.setkMin(1);
        params.setkMax(5);
        params.setNeighborEvaluationMode(ClassificationParametersKNN.ENeighborEvaluationMode.MEAN_STD_OVER_RANGE);
        params.setCrossValidationType("DISTINCT");

        ClassificationMethodKNN knn = new ClassificationMethodKNN(params);
        ValidationMethodParameters validationParams = new ValidationMethodParameters();
        validationParams.optimizationType = "INNER";

        FeatureOptimizationParametersSFS optParams = new FeatureOptimizationParametersSFS();
        FeatureOptimizationMethodSFS optMethod = new FeatureOptimizationMethodSFS(optParams);

        GenerativeModel model = new NullGenerativeModel(new NullGenerativeModelParameters());
        ValidationMethod validation = new DistinctSetValidation(validationParams,
                                                                  knn,
                                                                  model,
                                                                  optMethod);
        ClassificationResult result = validation.validate(trainingFeatures, evaluationFeatures);
        double ocr = result.getOverallClassificationRate();
        assertEquals(0.9316, ocr, 0);
    }
    */
    
    /* @Test
    public void testValidateInnerSBS()
            throws Exception
    {
        int classCount = 10;
        int numFeatures = 100;
        double bias1 = 0.01;
        double bias2 = 0.01;
        
        // generate random feaures
        @SuppressWarnings("unchecked")
        Random rand = new Random(1234567890);
        ArrayList<DiscriminativeFeatures> trainingFeatures = DistinctSetValidationTest.generateRandomFeatures(classCount, numFeatures, bias1, 10, rand);
        ArrayList<DiscriminativeFeatures> evaluationFeatures = DistinctSetValidationTest.generateRandomFeatures(classCount, numFeatures, bias2, 10, rand);

        ClassificationParametersKNN params = new ClassificationParametersKNN();
        params.setkMin(1);
        params.setkMax(5);
        params.setNeighborEvaluationMode(ClassificationParametersKNN.ENeighborEvaluationMode.MEAN_STD_OVER_RANGE);
        params.setCrossValidationType("DISTINCT");

        ClassificationMethodKNN knn = new ClassificationMethodKNN(params);
        ValidationMethodParameters validationParams = new ValidationMethodParameters();
        validationParams.optimizationType = "INNER";

        FeatureOptimizationParametersSBS optParams = new FeatureOptimizationParametersSBS();
        FeatureOptimizationMethodSBS optMethod = new FeatureOptimizationMethodSBS(optParams);
        
        GenerativeModel model = new NullGenerativeModel(new NullGenerativeModelParameters());
        ValidationMethod validation = new DistinctSetValidation(validationParams,
                                                                  knn,
                                                                  model,
                                                                  optMethod);
        ClassificationResult result = validation.validate(trainingFeatures, evaluationFeatures);
        double ocr = result.getOverallClassificationRate();
        assertEquals(0.1474, ocr, 0);
    }*/

    /*@Test
    public void testGenerateRandomSplitLabels()
    {
        ValidationMethod validation = new DistinctSetValidation(null,
                                                                  null,
                                                                  null,
                                                                  null);

        int[] foldLabels = validation.generateRandomFoldLabels(112, 5, new Random(1234567890));
    }
*/
    public static ArrayList<DiscriminativeFeatures> generateRandomFeatures(int classCount,
                                                                     int numFeatures,
                                                                     double bias,
                                                                     int featureDimension,
                                                                     Random rand)
            throws JCeliacGenericException
    {

        ArrayList<DiscriminativeFeatures> features = new ArrayList<>();
        for(int c = 0; c < classCount; c++) {
            for(int i = 0; i < numFeatures; i++) {

                StandardEuclideanFeatures euclideanFeatures = new StandardEuclideanFeatures();
                for(int d = 0; d < featureDimension; d++) {

                    euclideanFeatures.setSignalIdentifier("" + Math.random());
                    euclideanFeatures.setClassNumber(c);
                    double[] randomFeatureVector = new double[50];
                    for(int j = 0; j < randomFeatureVector.length; j++) {
                        randomFeatureVector[j] = rand.nextDouble() + c * bias;
                    }
                    AbstractFeatureVector vector = new FixedFeatureVector();
                    vector.setData(randomFeatureVector);
                    euclideanFeatures.addAbstractFeature(vector);

                }
                features.add(euclideanFeatures);
            }
        }
        return features;
    }
    
    
      public static ArrayList<DiscriminativeFeatures> generateRandomLocalFeatures(int classCount,
                                                                                int numFeatures,
                                                                                int numLocalFeatures,
                                                                                double bias,
                                                                                int featureDimension,
                                                                                Random rand)
            throws JCeliacGenericException
    {

        ArrayList<DiscriminativeFeatures> features = new ArrayList<>();
        for(int c = 0; c < classCount; c++) {
            for(int i = 0; i < numFeatures; i++) {

                MultiFractalSpectrumMR8Features mr8Features = new MultiFractalSpectrumMR8Features();
                for(int d = 0; d < featureDimension; d++) {

                    mr8Features.setSignalIdentifier("" + Math.random());
                    mr8Features.setClassNumber(c);
                    
                    double [][] localFeatures = new double[numLocalFeatures][8];
                    for(int l = 0; l < numLocalFeatures; l++) {
                        for(int j = 0; j < localFeatures[0].length; j++) {
                            localFeatures[l][j] = rand.nextDouble() + c * bias;
                        }
                    }
                    ArrayList <double [][]> tmp = new ArrayList <>();
                    tmp.add(localFeatures);
                    mr8Features.setLocalFeatureVectorList(tmp);
                }
                features.add(mr8Features);
            }
        }
        return features;
    }
      
    

    private ArrayList <FeatureVectorSubsetEncoding>  generateAllFeatureSubsets(int curDimension)
    {
        ArrayList <FeatureVectorSubsetEncoding> list = new ArrayList <>();

        for(int j = 0; j < curDimension; j++) {
            FeatureVectorSubsetEncoding subset = new FeatureVectorSubsetEncoding(curDimension);
            subset.setFeatureIndex(j);
            list.add(subset);
        }
        
        
        while(list.size() != Math.pow(2,curDimension)-1) {
            ArrayList <FeatureVectorSubsetEncoding> curLevelList = new ArrayList <>();
            for(FeatureVectorSubsetEncoding tmp : list) 
            {
                for(int j = 0; j < curDimension; j++) {
                    FeatureVectorSubsetEncoding subset = new FeatureVectorSubsetEncoding(tmp);
                    subset.setFeatureIndex(j);
                    if(false == curLevelList.contains(subset)) {
                        curLevelList.add(subset);
                    }
                 }
            }
            list = curLevelList;
        }
        return list;   
    }

}
