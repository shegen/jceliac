/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.classification.model.bow;

import java.util.ArrayList;
import java.util.Random;
import jceliac.classification.model.GenerativeModelFeatureMapping;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;
import jceliac.validation.DistinctSetValidationTest;

import org.junit.Test;

/**
 *
 * @author shegen
 */
public class BagOfVisualWordsModelTest
{
    

    /**
     * Test of trainModel method, of class BagOfVisualWordsModel.
     */
    @Test
    public void testTrainModel() 
            throws Exception
    {
        int classCount = 10;
        int numFeatures = 10;
        Random rand = new Random(1234567890);
        
        ArrayList<DiscriminativeFeatures> trainingFeatures = 
                    DistinctSetValidationTest.generateRandomLocalFeatures(classCount, numFeatures, 128*128, 0.15, 8, rand);
        FeatureCollection trainingFeatureCollection = 
                    FeatureCollection.createClonedFeatureCollection(trainingFeatures);
        
        
        BagOfVisualWordsParameters bowParams = new BagOfVisualWordsParameters();
        BagOfVisualWordsModel instance = new BagOfVisualWordsModel(bowParams);
        instance.initializeModel();
        GenerativeModelFeatureMapping modelMapping = instance.trainModel(trainingFeatureCollection);
        modelMapping.mapFeatures(trainingFeatureCollection);
        
    }
     
}
