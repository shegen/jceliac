/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification;

import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import jceliac.featureExtraction.LBP.LBPFeatures;
import org.junit.Test;

/**
 *
 * @author shegen
 */
public class ClassificationResultTest
{
    
    public ClassificationResultTest()
    {
    }

    @Test
    public void testSerialization()
    {
        ClassificationResult result = new ClassificationResult(2);
        final int numOutcomes = 200;
        
        int [] groundtruth = new int[numOutcomes];
        int [] estimatedClasses = new int[numOutcomes];
        for(int i = 0; i < numOutcomes; i++) 
        {
            ClassificationOutcome outcome = new ClassificationOutcome();
            int estimatedClass = (int)(Math.random() * 1000) % 2;
            int realClass = (int)(Math.random() * 1000) % 2;
            
            groundtruth[i] = realClass;
            estimatedClasses[i] = estimatedClass;
            
            outcome.setEstimatedClass(estimatedClass);
            outcome.setGuessed(false);
            outcome.setTied(false);
            
            LBPFeatures f = new LBPFeatures(null);
            f.setClassNumber(realClass);
            outcome.setFeatureVector(f);
            result.addOutcome(outcome);
        }
        
        try(FileOutputStream fileOutStream = new FileOutputStream("result-test.ser");
            ObjectOutputStream objOutStream = new ObjectOutputStream(fileOutStream)) 
        {
            objOutStream.writeObject(result);
        }catch(IOException e) {
            e.printStackTrace();
        }
        
        try(FileInputStream fileInStream = new FileInputStream("result-test.ser");
            ObjectInputStream objInStream = new ObjectInputStream(fileInStream)) 
        {
            Object o = objInStream.readObject();
            ClassificationResult resultIn = (ClassificationResult) o;

            // check that the estimated classes and the real classes are still the same 
            // as generated
            ArrayList <ClassificationOutcome> outcomes = resultIn.getClassificationOutcomes();
            for(int index = 0; index < numOutcomes; index++) {
                ClassificationOutcome outcome = outcomes.get(index);
                assert(outcome.getEstimatedClass() == estimatedClasses[index]);
                assert(outcome.getFeatures().getClassNumber() == groundtruth[index]);
            }
      
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
}
