/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jceliac.classification;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class ReceiverOperatorCharacteristicTest {
    
    public ReceiverOperatorCharacteristicTest() {
    }
    

    /**
     * Test of getAUC method, of class ReceiverOperatorCharacteristic.
     */
    @Test
    public void testGetAUC() {
        System.out.println("getAUC");
        int [] correct = {1,1,1,1,1,1,1,1};
        int [] wrong =   {0,0,0,0,0,0,0,0};
        
        ReceiverOperatorCharacteristic instance = new ReceiverOperatorCharacteristic(correct, wrong);
        
        
        double expResult = 100.0;
        double result = instance.getAUC();
        assertEquals(expResult, result, 0.0000001);
        
        int [] correct2 = { 1, 1, 1, 1, 0, 0, 0,0};
        int [] wrong2 = {0,0,0,0,1,1,1,1};
        instance = new ReceiverOperatorCharacteristic(correct2, wrong2);
        result = instance.getAUC();
        assertEquals(result, 50.0, 0.000001);
        
    }
}