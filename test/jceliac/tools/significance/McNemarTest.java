/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.significance;

import jceliac.classification.ClassificationOutcome;
import jceliac.classification.ClassificationResult;
import jceliac.featureExtraction.LBP.LBPFeatures;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import umontreal.iro.lecuyer.probdist.ChiSquareDist;

/**
 *
 * @author shegen
 */
public class McNemarTest
{
    
    public McNemarTest()
    {
    }


    /**
     * Test of gamma method, of class McNemar.
     */
    @Test
    public void testGamma() throws Exception
    {
        System.out.println("gamma");
        int n = 1;
        double expResult = 1;
        double result = McNemar.gamma(n);
        assert(Math.abs(expResult-result) < 0.00000001);
        
        n = 2;
        expResult = 1;
        result = McNemar.gamma(n);
        assert(Math.abs(expResult-result) < 0.00000001);
        
        n = 3;
        expResult = 2;
        result = McNemar.gamma(n);
        assert(Math.abs(expResult-result) < 0.00000001);
        
        n = 4;
        expResult = 6;
        result = McNemar.gamma(n);
        assert(Math.abs(expResult-result) < 0.00000001);
        
        n = 12;
        expResult = 39916800;
        result = McNemar.gamma(n);
        assert(Math.abs(expResult-result) < 0.00001);
    }
    
    
        /**
     * Test of gamma method, of class McNemar.
     */
    @Test
    public void testGammaComplex() throws Exception
    {
        System.out.println("gamma");
        double real = 1.7;
        
        double expResult = 0.908638732853290;
        double result = McNemar.gamma(real);
        assert(Math.abs(expResult-result) < 0.00000001);
        
        expResult = 2.288037795340032;
        result = McNemar.gamma(Math.PI);
        assert(Math.abs(expResult-result) < 0.00000001);
        
        
        expResult = 1.280451571678219;
        result = McNemar.gamma(2.445673451234567);
        assert(Math.abs(expResult-result) < 0.00000001);
        
        expResult = 2.707206222615191;
        result = McNemar.gamma(0.33);
        assert(Math.abs(expResult-result) < 0.00000001);
        
        expResult = -4.085465850315993;
        result = McNemar.gamma(-0.33);
        assert(Math.abs(expResult-result) < 0.00000001);
        
        expResult = 1.015697144460219;
        result = McNemar.gamma(-Math.PI);
        assert(Math.abs(expResult-result) < 0.00000001);
        
    }
    
    @Test
    public void testSignificance()
    {
        int [] ass1 = {1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1};
        int [] ass2 = {1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1};
        int [] groundtruth = {1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1};
        McNemar test = new McNemar(ass1, ass2, groundtruth, 1);
        
        boolean sig = test.testSignificance(0.1);
        assertEquals(sig,false);
        
        sig = test.testSignificance(0.92);
        assertEquals(sig,true);
    }
    
    @Test
    public void testSignificanceSerialized()
    {
        int [] ass1 = {1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1};
        int [] ass2 = {1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1};
        int [] groundtruth = {1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1};
        
        ClassificationResult result1 = new ClassificationResult(2);
        ClassificationResult result2 = new ClassificationResult(2);
        
        for(int i = 0; i < ass1.length; i++) 
        {
            ClassificationOutcome outcome1 = new ClassificationOutcome();
            ClassificationOutcome outcome2 = new ClassificationOutcome();
            
            int estimatedClass1 = (int)ass1[i];
            int estimatedClass2 = (int)ass2[i];
            int realClass = (int)groundtruth[i];
  
            outcome1.setEstimatedClass(estimatedClass1);
            outcome1.setGuessed(false);
            outcome1.setTied(false);
            
            LBPFeatures f1 = new LBPFeatures(null);
            f1.setClassNumber(realClass);
            outcome1.setFeatureVector(f1);
            result1.addOutcome(outcome1);
            
            outcome2.setEstimatedClass(estimatedClass2);
            outcome2.setGuessed(false);
            outcome2.setTied(false);
            
            LBPFeatures f2 = new LBPFeatures(null);
            f2.setClassNumber(realClass);
            outcome2.setFeatureVector(f2);
            result2.addOutcome(outcome2); 
        }
        
        try {
            McNemar test = new McNemar(result1, result2, 1);

            boolean sig = test.testSignificance(0.1);
            assertEquals(sig,false);

            sig = test.testSignificance(0.92);
            assertEquals(sig,true);
        }catch(Exception e) {
            fail();
        }
        
    }
    
    
}
