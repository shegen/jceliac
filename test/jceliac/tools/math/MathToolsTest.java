/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.math;

import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class MathToolsTest
{
    
    public MathToolsTest()
    {
    }

    
    /**
     * Test of erfinv method, of class MathTools.
     */
    @Test
    public void testErfinv()
    {
        System.out.println("erfinv");
        double x = 0.56;
        double expResult = 0.5460230;
        double result = MathTools.erfinv(x);
        assertEquals(expResult, result, 0.001);
        
        x = 0.95;
        expResult = 1.3859038;
        result = MathTools.erfinv(x);
        assertEquals(expResult, result, 0.001);

        
        x = 0.6022146;
        expResult = 0.597917499243596;
        result = MathTools.erfinv(x);
        assertEquals(expResult, result, 0.001);
        
        x = -0.6011767;
        expResult = -0.596603;
        result = MathTools.erfinv(x);
        assertEquals(expResult, result, 0.001);
        
        x = 0;
        expResult = 0;
        result = MathTools.erfinv(x);
        assertEquals(expResult, result, 0.001);
        
        x = 1;
        expResult = -Double.POSITIVE_INFINITY;
        result = MathTools.erfinv(x);
        assertEquals(expResult, result, 0.001);
    }

    @Test
    public void testErf()
    {
        double x = 0.56;
        double expResult = 0.571615763823768;
        
        assertEquals(MathTools.erf(x), expResult, 0.00001);
        
        x = 0.95;
        expResult = 0.820890807273278;
        assertEquals(MathTools.erf(x), expResult, 0.00001);
        
        x = 0.404540520078376;
        expResult = 0.432750302913106;
        assertEquals(MathTools.erf(x), expResult, 0.00001);
        
        x = 2.0d;
        expResult = 0.995322265018953;
        assertEquals(MathTools.erf(x), expResult, 0.00001);
        
        x = -2.0d;
        expResult = -0.995322265018953;
        double result = MathTools.erf(x);
        assert(Math.abs(result - expResult) < 0.00001);
    }
    

  
}
