/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jceliac.tools.math;

import java.util.AbstractMap.SimpleEntry;
import jceliac.JCeliacGenericException;
import jceliac.tools.filter.scalespace.SecondMomentMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class MatrixToolsTest {
    
    public MatrixToolsTest() {
    }
    

    @Test
    public void testEigensolution()
    {
        for(int i = 0; i < 1000; i++) {
            double a = Math.random();
            double b = Math.random();
            double c = Math.random();
            
            double [][] A = {{a,b},{b,c}};
            SimpleEntry <RealVector [], double []> solution = MatrixTools.eigensolution(A);
            RealVector [] expVector = MatrixTools.eigenvectors(A);
            double [] expValues = MatrixTools.eigenvalues(A);

            RealVector [] result = solution.getKey();
            double [] resultValues = solution.getValue();
            
            assert(Math.abs(Math.abs(result[0].getEntry(0)) - Math.abs(expVector[0].getEntry(0))) < 0.0001);
            assert(Math.abs(Math.abs(result[0].getEntry(1)) - Math.abs(expVector[0].getEntry(1))) < 0.0001);
            
            assert(Math.abs(Math.abs(result[1].getEntry(0)) - Math.abs(expVector[1].getEntry(0))) < 0.0001);
            assert(Math.abs(Math.abs(result[1].getEntry(1)) - Math.abs(expVector[1].getEntry(1))) < 0.0001);
            
            
            assert(Math.abs(expValues[0] - resultValues[0]) < 0.0001);
            assert(Math.abs(expValues[1] - resultValues[1]) < 0.0001);
        }
       
        
        double [][] B = {{11.4357707,-6.873034},{-6.873034,12.87532388}};
        SimpleEntry <RealVector [], double []> solution = MatrixTools.eigensolution(B);
        RealVector [] expVector = MatrixTools.eigenvectors(B);
        double [] expValues = MatrixTools.eigenvalues(B);

        RealVector [] result = solution.getKey();
        double [] resultValues = solution.getValue();
        
        assert(Math.abs(Math.abs(result[0].getEntry(0)) - Math.abs(expVector[0].getEntry(0))) < 0.0001);
        assert(Math.abs(Math.abs(result[0].getEntry(1)) - Math.abs(expVector[0].getEntry(1))) < 0.0001);
            
        assert(Math.abs(Math.abs(result[1].getEntry(0)) - Math.abs(expVector[1].getEntry(0))) < 0.0001);
        assert(Math.abs(Math.abs(result[1].getEntry(1)) - Math.abs(expVector[1].getEntry(1))) < 0.0001);
            
            
        assert(Math.abs(expValues[0] - resultValues[0]) < 0.0001);
        assert(Math.abs(expValues[1] - resultValues[1]) < 0.0001);
        
    }
    
    @Test
    public void testSqrtm() 
            throws JCeliacGenericException 
    {
        double [][] m = MatrixTools.squareRoot(9, 0, 0, 0.41);
        assertEquals(m[0][0], 3, 0.001);
        assertEquals(m[0][1], 0, 0.001);
        assertEquals(m[1][0], 0, 0.001);
        assertEquals(m[1][1], 0.6403, 0.001);
                
    }
    
}