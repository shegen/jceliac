/*
 * 
 JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at> 
 The source code of this software is not yet made available to the open source 
 community. If you obtained this source code without permission of the author 
 please remove all the source files.

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 * Each line should be prefixed with  * 
 */

package jceliac.tools.math;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class PolyFitTest
{
    
    /**
     * Test of fitSlope method, of class PolyFit.
     */
    @Test
    public void testFit() throws Exception
    {
        System.out.println("fit");
        double[] x = {0, 0.6931, 1.0986, 1.3863, 1.6094, 1.7918, 1.9459, 2.0794};
        double[] y = {4.3372, 5.5926, 5.9973, 6.3105, 6.5368, 6.6279, 6.5610, 6.4699};
        double expResult = 1.0477d;
        double slope = PolyFit.fitSlope(x, y);
        assertEquals(slope, expResult, 0.001);
        
    }

}
