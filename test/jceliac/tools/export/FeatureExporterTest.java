/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.export;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.function.DoubleFunction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class FeatureExporterTest
{
    
    public FeatureExporterTest()
    {
    }
    
 

    /**
     * Test of exportFeatures method, of class FeatureExporter.
     */
    @Test
    public void testExportFeatures() throws Exception
    {
        System.out.println("exportFeatures");
        ArrayList <ExportableFeature> features = new ArrayList <>();
        for(int i = 0; i < 200; i++) {
            features.add(this.generateRandomExportableFeature());
        }
        
         try (FileOutputStream fileOutStream = new FileOutputStream("TEST"+"-features.ser");
             ObjectOutputStream objOutStream = new ObjectOutputStream(fileOutStream)
            ) 
        {
            objOutStream.writeObject(features);
        }catch(Exception e) {
            throw e;
        }
         
        ArrayList <ExportableFeature> importedFeature;
        try(FileInputStream fileInStream = new FileInputStream("TEST"+"-features.ser");
            ObjectInputStream objInStream = new ObjectInputStream(fileInStream)
           )
        {
            importedFeature = (ArrayList <ExportableFeature>) objInStream.readObject();
        }catch(Exception e) {
            throw e;
        }
        
        // compare
        for(int i = 0; i < features.size(); i++) {
            ExportableFeature f1 = features.get(i);
            ExportableFeature f2 = importedFeature.get(i);
            
            assert(f1.getClassNumber() == f2.getClassNumber());
            assert(Arrays.equals(f1.getFeatureVector(), f2.getFeatureVector()));
            assert(f1.getFilename().equals(f2.getFilename()));
        }
    }
    
    private ExportableFeature generateRandomExportableFeature()
    {
        double [] featureVector = new double[59];
        String signalIdentifier;
        int classNumber;
        
        for(int i = 0; i < featureVector.length; i++) {
            featureVector[i] = Math.random();
        }
        // create some random string
        double r = Math.random();
        signalIdentifier = String.valueOf(r);

        // create some random class identifier
        classNumber = Math.random() > 0.5 ? 1:0;
        
        return new ExportableFeature(classNumber, featureVector, signalIdentifier);
        
    }
}