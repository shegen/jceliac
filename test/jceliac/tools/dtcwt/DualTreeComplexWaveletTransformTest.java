/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 *  Author: Michael Liedlgruber <mliedl at cosy.sbg.ac.at>
 * 
 */
package jceliac.tools.dtcwt;

import jceliac.tools.transforms.dtcwt.ComplexArray2D;
import jceliac.tools.transforms.dtcwt.DualTreeComplexWaveletTransform;
import jceliac.tools.transforms.dtcwt.DualTreeComplexWaveletDecomposition;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.Frame;
import jceliac.tools.transforms.dtcwt.filter.BiorthogonalFilter;
import jceliac.tools.transforms.dtcwt.filter.BiorthogonalFilter_Near_Sym_A;
import jceliac.tools.transforms.dtcwt.filter.QShiftFilter;
import jceliac.tools.transforms.dtcwt.filter.QShiftFilter_QShift_B;
import jceliac.tools.export.MatrixImporter;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author shegen
 */
public class DualTreeComplexWaveletTransformTest
{

    /**
     * Test of doDTCWT2D method, of class DTCWT2D.
     */
    @Test
    public void testDoDTCWT2D()
    {
        try {
            BufferedImage image = ImageIO.read(new File("testdata/images/44-scale_3_im_3_col.png"));
            Frame content = Frame.convertBufferedImageToFrameOptimized(image);
            BiorthogonalFilter biort = new BiorthogonalFilter_Near_Sym_A();
            QShiftFilter qshift = new QShiftFilter_QShift_B();
            
            DualTreeComplexWaveletTransform transform = new DualTreeComplexWaveletTransform(biort, qshift);
            transform.transformForward(content.getGrayData(), 6);
            DualTreeComplexWaveletDecomposition decomposition = transform.getDecompositionTree();
            
            ComplexArray2D expectedScale1Subband1 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale1_subband1.csv"));
            ComplexArray2D expectedScale1Subband2 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale1_subband2.csv"));
            ComplexArray2D expectedScale1Subband3 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale1_subband3.csv"));
            ComplexArray2D expectedScale1Subband4 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale1_subband4.csv"));
            ComplexArray2D expectedScale1Subband5 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale1_subband5.csv"));
            ComplexArray2D expectedScale1Subband6 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale1_subband6.csv"));
            
            ComplexArray2D expectedScale2Subband1 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale2_subband1.csv"));
            ComplexArray2D expectedScale2Subband2 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale2_subband2.csv"));
            ComplexArray2D expectedScale2Subband3 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale2_subband3.csv"));
            ComplexArray2D expectedScale2Subband4 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale2_subband4.csv"));
            ComplexArray2D expectedScale2Subband5 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale2_subband5.csv"));
            ComplexArray2D expectedScale2Subband6 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale2_subband6.csv"));
            
            ComplexArray2D expectedScale3Subband1 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale3_subband1.csv"));
            ComplexArray2D expectedScale3Subband2 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale3_subband2.csv"));
            ComplexArray2D expectedScale3Subband3 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale3_subband3.csv"));
            ComplexArray2D expectedScale3Subband4 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale3_subband4.csv"));
            ComplexArray2D expectedScale3Subband5 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale3_subband5.csv"));
            ComplexArray2D expectedScale3Subband6 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale3_subband6.csv"));
            
            ComplexArray2D expectedScale4Subband1 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale4_subband1.csv"));
            ComplexArray2D expectedScale4Subband2 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale4_subband2.csv"));
            ComplexArray2D expectedScale4Subband3 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale4_subband3.csv"));
            ComplexArray2D expectedScale4Subband4 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale4_subband4.csv"));
            ComplexArray2D expectedScale4Subband5 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale4_subband5.csv"));
            ComplexArray2D expectedScale4Subband6 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale4_subband6.csv"));
            
            ComplexArray2D expectedScale5Subband1 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale5_subband1.csv"));
            ComplexArray2D expectedScale5Subband2 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale5_subband2.csv"));
            ComplexArray2D expectedScale5Subband3 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale5_subband3.csv"));
            ComplexArray2D expectedScale5Subband4 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale5_subband4.csv"));
            ComplexArray2D expectedScale5Subband5 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale5_subband5.csv"));
            ComplexArray2D expectedScale5Subband6 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale5_subband6.csv"));
            
            ComplexArray2D expectedScale6Subband1 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale6_subband1.csv"));
            ComplexArray2D expectedScale6Subband2 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale6_subband2.csv"));
            ComplexArray2D expectedScale6Subband3 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale6_subband3.csv"));
            ComplexArray2D expectedScale6Subband4 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale6_subband4.csv"));
            ComplexArray2D expectedScale6Subband5 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale6_subband5.csv"));
            ComplexArray2D expectedScale6Subband6 = MatrixImporter.importComplexMatrix(new File("testdata/jceliac.tools.dtcwt/scale6_subband6.csv"));
            
            
            double [][] expectedScale1RealSubband = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.dtcwt/scale1_real_subband.csv"));
            double [][] expectedScale2RealSubband = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.dtcwt/scale2_real_subband.csv"));
            double [][] expectedScale3RealSubband = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.dtcwt/scale3_real_subband.csv"));
            double [][] expectedScale4RealSubband = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.dtcwt/scale4_real_subband.csv"));
            double [][] expectedScale5RealSubband = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.dtcwt/scale5_real_subband.csv"));
            double [][] expectedScale6RealSubband = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.dtcwt/scale6_real_subband.csv"));
            
            
            // SCALE 1 
            if(ArrayTools.compare(expectedScale1Subband1, decomposition.getComplexSubband(1, 1), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale1Subband2, decomposition.getComplexSubband(1, 2), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale1Subband3, decomposition.getComplexSubband(1, 3), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale1Subband4, decomposition.getComplexSubband(1, 4), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale1Subband5, decomposition.getComplexSubband(1, 5), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale1Subband6, decomposition.getComplexSubband(1, 6), 1e-5) == false) {
                fail();
            }
            
            // SCALE 2
            if(ArrayTools.compare(expectedScale2Subband1, decomposition.getComplexSubband(2, 1), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale2Subband2, decomposition.getComplexSubband(2, 2), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale2Subband3, decomposition.getComplexSubband(2, 3), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale2Subband4, decomposition.getComplexSubband(2, 4), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale2Subband5, decomposition.getComplexSubband(2, 5), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale2Subband6, decomposition.getComplexSubband(2, 6), 1e-5) == false) {
                fail();
            }
            
            // SCALE 3
            if(ArrayTools.compare(expectedScale3Subband1, decomposition.getComplexSubband(3, 1), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale3Subband2, decomposition.getComplexSubband(3, 2), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale3Subband3, decomposition.getComplexSubband(3, 3), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale3Subband4, decomposition.getComplexSubband(3, 4), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale3Subband5, decomposition.getComplexSubband(3, 5), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale3Subband6, decomposition.getComplexSubband(3, 6), 1e-5) == false) {
                fail();
            }
            
            // SCALE 4
            if(ArrayTools.compare(expectedScale4Subband1, decomposition.getComplexSubband(4, 1), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale4Subband2, decomposition.getComplexSubband(4, 2), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale4Subband3, decomposition.getComplexSubband(4, 3), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale4Subband4, decomposition.getComplexSubband(4, 4), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale4Subband5, decomposition.getComplexSubband(4, 5), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale4Subband6, decomposition.getComplexSubband(4, 6), 1e-5) == false) {
                fail();
            }
            
            // SCALE 5
            if(ArrayTools.compare(expectedScale5Subband1, decomposition.getComplexSubband(5, 1), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale5Subband2, decomposition.getComplexSubband(5, 2), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale5Subband3, decomposition.getComplexSubband(5, 3), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale5Subband4, decomposition.getComplexSubband(5, 4), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale5Subband5, decomposition.getComplexSubband(5, 5), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale5Subband6, decomposition.getComplexSubband(5, 6), 1e-5) == false) {
                fail();
            }
            
            // SCALE 5
            if(ArrayTools.compare(expectedScale6Subband1, decomposition.getComplexSubband(6, 1), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale6Subband2, decomposition.getComplexSubband(6, 2), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale6Subband3, decomposition.getComplexSubband(6, 3), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale6Subband4, decomposition.getComplexSubband(6, 4), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale6Subband5, decomposition.getComplexSubband(6, 5), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale6Subband6, decomposition.getComplexSubband(6, 6), 1e-5) == false) {
                fail();
            }
            
            // REAL subbands
            if(ArrayTools.compare(expectedScale1RealSubband, decomposition.getRealLowpassSubband(1), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale2RealSubband, decomposition.getRealLowpassSubband(2), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale3RealSubband, decomposition.getRealLowpassSubband(3), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale4RealSubband, decomposition.getRealLowpassSubband(4), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale5RealSubband, decomposition.getRealLowpassSubband(5), 1e-5) == false) {
                fail();
            }
            if(ArrayTools.compare(expectedScale6RealSubband, decomposition.getRealLowpassSubband(6), 1e-5) == false) {
                fail();
            }
            
            
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    
    @Test
    public void testExtendSignal()
    {
        try {
            BufferedImage image = ImageIO.read(new File("testdata/images/44-scale_3_im_3_col.png"));
            Frame content = Frame.convertBufferedImageToFrameOptimized(image);
            content.crop(0, 0, 128, 127);
            BiorthogonalFilter biort = new BiorthogonalFilter_Near_Sym_A();
            QShiftFilter qshift = new QShiftFilter_QShift_B();
           
            DualTreeComplexWaveletTransform transform = new DualTreeComplexWaveletTransform(biort, qshift);
            transform.transformForward(content.getGrayData(), 6);
            DualTreeComplexWaveletDecomposition decomposition = transform.getDecompositionTree();
            double [][] rl6 = decomposition.getRealLowpassSubband(6);
            double [][] expectedRL6 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.dtcwt/extend1.csv"));
           
            if(ArrayTools.compare(expectedRL6, rl6, 1e-5) == false) {
                fail();
            }
            
            image = ImageIO.read(new File("testdata/images/44-scale_3_im_3_col.png"));
            content = Frame.convertBufferedImageToFrameOptimized(image);
            content.crop(0, 0, 127, 128);
          
            transform = new DualTreeComplexWaveletTransform(biort, qshift);
            transform.transformForward(content.getGrayData(), 6);
            decomposition = transform.getDecompositionTree();
            double [][] rl6_2 = decomposition.getRealLowpassSubband(6);
            double [][] expectedRL6_2 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.dtcwt/extend2.csv"));
           
            if(ArrayTools.compare(expectedRL6_2, rl6_2, 1e-5) == false) {
                fail();
            }
            
            image = ImageIO.read(new File("testdata/images/44-scale_3_im_3_col.png"));
            content = Frame.convertBufferedImageToFrameOptimized(image);
            content.crop(0, 0, 91, 91);
            double [][] data = content.getGrayData();
            
            transform = new DualTreeComplexWaveletTransform(biort, qshift);
            transform.transformForward(content.getGrayData(), 6);
            decomposition = transform.getDecompositionTree();
            double [][] rl6_3 = decomposition.getRealLowpassSubband(6);
            double [][] expectedRL6_3 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.dtcwt/extend3.csv"));
           
            if(ArrayTools.compare(expectedRL6_3, rl6_3, 1e-5) == false) {
                fail();
            }
            
        } catch(Exception e) { 
           e.printStackTrace();
            fail();
        }
    }
    

}
