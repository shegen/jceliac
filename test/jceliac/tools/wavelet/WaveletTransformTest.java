/*
 * 
 JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at> 
 The source code of this software is not yet made available to the open source 
 community. If you obtained this source code without permission of the author 
 please remove all the source files.

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 * Each line should be prefixed with  * 
 */

package jceliac.tools.wavelet;

import jceliac.tools.transforms.wavelet.WaveletTransform;
import jceliac.tools.transforms.wavelet.WaveletDecomposition;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.DataSource;
import jceliac.tools.data.Frame;
import jceliac.tools.export.MatrixImporter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class WaveletTransformTest
{
    

  

    /**
     * Test of transformForwardSingleChannel method, of class WaveletTransform.
     */
    @Test
    public void testTransformForwardSingleChannel() throws Exception
    {
        try {
            BufferedImage image = ImageIO.read(new File("testdata/images/44-scale_3_im_3_col.png"));
            Frame content = Frame.convertBufferedImageToFrameOptimized(image);

            System.out.println("transformForwardSingleChannel");
            WaveletTransform instance = new WaveletTransform(WaveletTransform.EFilterBankType.CDF97);
            
           
                    
            double [][] input = new double[][] {
                {0.963089, 0.624060, 0.037739, 0.261871, 0.106762},
                {0.546806, 0.679136, 0.885168, 0.335357, 0.653757},
                {0.521136, 0.395515, 0.913287, 0.679728, 0.494174},
                {0.231594, 0.367437, 0.796184, 0.136553, 0.779052},
                {0.488898, 0.987982, 0.098712, 0.721227, 0.715037}
            };
            
            double [][] imgData = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.wavelet/input.csv"));
            if(ArrayTools.compare(content.getGrayData(), imgData, 0.001) == false) {
                fail();
            }
            
            instance.transformForwardSingleChannel(content.getGrayData(), 1, DataSource.CHANNEL_GRAY);
            WaveletDecomposition decomp = instance.getWaveletDecompositionTree(DataSource.CHANNEL_GRAY);
             
            double [][] expectedApproxScale1 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.wavelet/approx_scale1.csv"));
            double [][] approxScale1 = decomp.getApproximationSubband(1);
            if(ArrayTools.compare(approxScale1, expectedApproxScale1, 0.01) == false) {
                fail();
            }
            
            double [][] expectedCHScale1 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.wavelet/ch_scale1.csv"));
            double [][] CHScale1 = decomp.getHorizontalDetailSubband(1);
            if(ArrayTools.compare(CHScale1, expectedCHScale1, 0.01) == false) {
                fail();
            }
            
            double [][] expectedCDScale1 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.wavelet/cd_scale1.csv"));
            double [][] CDScale1 = decomp.getDiagonalDetailSubband(1);
            if(ArrayTools.compare(CDScale1, expectedCDScale1, 0.01) == false) {
                fail();
            }
            
            double [][] expectedCVScale1 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.wavelet/cv_scale1.csv"));
            double [][] CVScale1 = decomp.getVerticalDetailSubband(1);
            if(ArrayTools.compare(CVScale1, expectedCVScale1, 0.01) == false) {
                fail();
            }
            
           
            
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    
    
}
