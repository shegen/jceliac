/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */


package jceliac.tools.parallel;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import org.junit.Test;


/**
 *
 * @author shegen
 */
public class ParallelTaskExecutorTest
{
    
    /**
     * Test of executeTasks method, of class ParallelTaskExecutor.
     */
    @Test
    public void testExecuteTasks() 
            throws Exception
    {
        ArrayList <Callable <Double>> tasks = new ArrayList <>();
        for(int i = 0; i < 10000; i++) {
            tasks.add(new ParallelSingle());
            tasks.add(new ParallelSingle());
            tasks.add(new ParallelSingle());
        } 
        @SuppressWarnings("unchecked")
      //  ParallelTaskExecutor exec = new ParallelTaskExecutor(tasks);
       // ArrayList <ArrayList <Double>> results = exec.executeTasks();
        
        ArrayList <Callable <ArrayList <Double>>> tasks2 = new ArrayList <>();
        for(int i = 0; i < 10000; i++) {
            tasks2.add(new ParallelList());
            tasks2.add(new ParallelList());
            tasks2.add(new ParallelList());
        } 
       // @SuppressWarnings("unchecked")
      //  ParallelTaskExecutor exec2 = new ParallelTaskExecutor(tasks2);
      //  ArrayList <ArrayList <Double>> results2 = exec2.executeTasks();
    }
    
    private class ParallelSingle
            implements Callable<Double>
    {
        @Override
        public Double call() throws Exception
        {
            double stuff = 0;
            for(int i = 0; i < 1000; i++) {
                stuff += Math.sin(Math.random());
            }
            return stuff;
        }
    }

    private class ParallelList
            implements Callable<ArrayList<Double>>
    {
        @Override
        public ArrayList<Double> call() throws Exception
        {
            ArrayList<Double> list = new ArrayList<>();
            for(int i = 0; i < 1000; i++) {
                list.add(Math.sin(Math.random()));

            }
            return list;
        }
    }

}
