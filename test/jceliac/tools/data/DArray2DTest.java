/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.tools.data;

import jceliac.tools.timer.PerformanceTimer;
import org.junit.Test;

/**
 *
 * @author shegen
 */
public class DArray2DTest
{
    
    /**
     * Test of get method, of class DArray2D.
     */
    @Test
    public void testPerformance()
    {
        double [][] randArray = new double[128][128];
        double [][] doubleArray = new double[128][128];
        DArray2D dArray = new DArray2D(128, 128);
        
        for(int i = 0; i < 128; i++) {
            for(int j = 0; j < 128; j++) {
                double rand = Math.random();
                randArray[i][j] = rand;     
            }
        }
        
        PerformanceTimer timer1 = new PerformanceTimer();
        timer1.startTimer();
        for(int i = 0; i < 128; i++) {
            for(int j = 0; j < 128; j++) {
                doubleArray[i][j] = randArray[i][j];     
            }
        }
        timer1.stopTimer();

        PerformanceTimer timer2 = new PerformanceTimer();
        timer2.startTimer();
        for(int i = 0; i < 128; i++) {
            for(int j = 0; j < 128; j++) {
                dArray.set(i,j, randArray[i][j]);     
            }
        } 
        timer2.stopTimer();
        
        System.out.println("Linear access double Array: "+timer1.getSeconds());
        System.out.println("Linear access DArray2D: "+timer2.getSeconds());
    }
       
    
}
