/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.tools.filter;

import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.Frame;
import jceliac.tools.export.MatrixImporter;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class MR8FilterTest
{
 
    /**
     * Test of filterData method, of class MR8Filter.
     * @throws java.lang.Exception
     */
    @Test
    public void testComputeMR8FilterBank() 
            throws Exception
    {
        MR8Filter mr8 = new MR8Filter();
        for(int i = 1; i <= 38; i++) {
            double [][] expectedFilter = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/MR8-filter-"+i+".csv"));
            double [][] computedFilter = mr8.getMR8Filter(i);
            
            if(false == ArrayTools.compare(expectedFilter, computedFilter, 1e-11)) {
                fail("Results mismatch!");
            }
        }
    }

    @Test
    public void testComputeMR8FilterResponse()
    {
        try {
            BufferedImage image = ImageIO.read(new File("testdata/images/44-scale_3_im_3_col.png"));
            Frame content = Frame.convertBufferedImageToFrameOptimized(image);
            MR8Filter mr8 = new MR8Filter();
            
            double [][] filterResponse = mr8.computeMR8FilterResponse1D(content.getGrayData());
            double [][] expectedFilterResponse = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/MR8-filter-response.csv"));
            
            if(false == ArrayTools.compare(filterResponse, expectedFilterResponse, 1e-11)) {
                fail("Results mismatch!");
            }
        }catch(Exception e) {
            fail(e.getLocalizedMessage());
        }
        
    }
 
    
}
