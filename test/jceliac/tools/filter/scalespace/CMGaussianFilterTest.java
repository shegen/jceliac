/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jceliac.tools.filter.scalespace;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class CMGaussianFilterTest {
    


    /**
     * Test of filterData method, of class CMGaussianFilter.
     */
    @Test
    public void testFilterData() throws Exception {
        System.out.println("filterData");
        double[][] signal = new double[128][128];
        
        
        for(int i = 0; i < signal.length; i++) {
            for(int j = 0; j < signal[0].length; j++) {
                signal[i][j] = Math.random();
            }
        }
        CMGaussianFilter instance = new CMGaussianFilter(10);
        instance.filterData(signal);
      
    }

    
}
