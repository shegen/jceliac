/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jceliac.tools.filter.scalespace;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class ScalespaceParametersTest
{

    /**
     * Test of getSigmaForScale method, of class ScalespaceParameters.
     */
    @Test
    public void testScalespaceParameters()
    {
        ScalespaceParameters parameters = new ScalespaceParameters(Math.sqrt(2.0d),
                                                                   -4,
                                                                   4,
                                                                   0.25,
                                                                   2.1214d);

        double[] expectedScales = {0.5303, 0.5784, 0.6307, 0.6878, 0.7500, 0.8179, 0.8919, 0.9727,
                                   1.0607, 1.1567, 1.2614, 1.3756, 1.5001, 1.6358, 1.7839, 1.9453,
                                   2.1214, 2.3134, 2.5228, 2.7511, 3.0001, 3.2716, 3.5678, 3.8907,
                                   4.2428, 4.6268, 5.0456, 5.5022, 6.0002, 6.5433, 7.1355, 7.7813,
                                   8.4856};

        // check getNumberOfScales
        int numberOfScales = parameters.getNumberOfScales();
        assert (numberOfScales == expectedScales.length);

        for(int scaleLevel = 0; scaleLevel < parameters.getNumberOfScales(); scaleLevel++) {
            assertEquals(parameters.getSigmaForScaleLevel(scaleLevel), expectedScales[scaleLevel], 0.001);
        }
        parameters = new ScalespaceParameters(Math.sqrt(2.0d), -4,
                                              8,
                                              0.25,
                                              2.1214d);

        assert (parameters.getNumberOfScales() == 49);
        parameters = new ScalespaceParameters(Math.sqrt(2.0d), 0,
                                              8,
                                              0.25,
                                              2.1214d);
        assert (parameters.getNumberOfScales() == 33);
        parameters = new ScalespaceParameters(Math.sqrt(2.0d),4,
                                              8,
                                              0.25,
                                              2.1214d);
        assert (parameters.getNumberOfScales() == 17);
        parameters = new ScalespaceParameters(Math.sqrt(2.0d),-4,
                                              -1,
                                              0.25,
                                              2.1214d);
        assert (parameters.getNumberOfScales() == 13);
        parameters = new ScalespaceParameters(Math.sqrt(2.0d),-4,
                                              -1,
                                              0.17,
                                              2.1214d);
        assert (parameters.getNumberOfScales() == 18);
        
        

    }

}
