/*
 * 
 JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at> 
 The source code of this software is not yet made available to the open source 
 community. If you obtained this source code without permission of the author 
 please remove all the source files.

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 * Each line should be prefixed with  * 
 */

package jceliac.tools.filter;

import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.Frame;
import jceliac.tools.export.MatrixImporter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class ImageResizeTest
{
   
    /**
     * Test of resizeImage method, of class ImageResize.
     */
    @Test
    public void testResizeImage() throws Exception
    {
        
        try {
            System.out.println("resizeImage");
            
            BufferedImage image = ImageIO.read(new File("testdata/images/44-scale_3_im_3_col.png"));
            double [][] expectedResult1 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/imresize_test1.csv"));
            Frame content = Frame.convertBufferedImageToFrameOptimized(image);
            double [][] data = content.getGrayData();
            
            ImageResize resizer = new ImageResize(ImageResize.EInterpolationType.BILINEAR);
            double[][] result = resizer.resizeImage(data, 64, 64);
            if(ArrayTools.compare(expectedResult1, result, 1e-5) == false) {
                fail();
            }
            
            double [][] expectedResult2 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/imresize_test2.csv"));
            resizer = new ImageResize(ImageResize.EInterpolationType.NEAREST);
            double[][] result2  = resizer.resizeImage(data, 64, 64);
            if(ArrayTools.compare(expectedResult2, result2, 1e-5) == false) {
                fail();
            }
            
            double [][] expectedResult3 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/imresize_test_nearest_3.csv"));
            resizer = new ImageResize(ImageResize.EInterpolationType.NEAREST);
            double[][] result3  = resizer.resizeImage(data, 77, 34);
            if(ArrayTools.compare(expectedResult3, result3, 1e-5) == false) {
                fail();
            }
            
            double [][] expectedResult4 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/imresize_test_bilinear_3.csv"));
            resizer = new ImageResize(ImageResize.EInterpolationType.BILINEAR);
            double[][] result4  = resizer.resizeImage(data, 77, 34);
            if(ArrayTools.compare(expectedResult4, result4, 1e-5) == false) {
                fail();
            }
            
            double [][] expectedResult5 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/imresize_test_bilinear_4.csv"));
            resizer = new ImageResize(ImageResize.EInterpolationType.BILINEAR);
            double[][] result5  = resizer.resizeImage(data, 256, 256);
            if(ArrayTools.compare(expectedResult5, result5, 1e-5) == false) {
                fail();
            }
            
            double [][] expectedResult6 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/imresize_test_nearest_4.csv"));
            resizer = new ImageResize(ImageResize.EInterpolationType.NEAREST);
            double[][] result6  = resizer.resizeImage(data, 256, 256);
            if(ArrayTools.compare(expectedResult6, result6, 1e-5) == false) {
                fail();
            }
            
            double [][] expectedResult7 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/imresize_test_bilinear_5.csv"));
            resizer = new ImageResize(ImageResize.EInterpolationType.BILINEAR);
            double[][] result7  = resizer.resizeImage(data, 192, 192);
            if(ArrayTools.compare(expectedResult7, result7, 1e-5) == false) {
                fail();
            }
            
            double [][] expectedResult8 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/imresize_test_nearest_5.csv"));
            resizer = new ImageResize(ImageResize.EInterpolationType.NEAREST);
            double[][] result8  = resizer.resizeImage(data, 192, 192);
            if(ArrayTools.compare(expectedResult8, result8, 1e-5) == false) {
                fail();
            }
            
        }catch(Exception e) {
            e.printStackTrace();
            fail();
        }

    }


    
}
