/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.filter.scalespace.SecondMomentMatrix;
import jceliac.tools.data.Frame;
import jceliac.tools.export.MatrixImporter;
import jceliac.tools.timer.PerformanceTimer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class LowpassFilterTest
{
    
    public LowpassFilterTest()
    {
    }

    /**
     * Test of computeGaussianRadiusForSupport method, of class LowpassFilter.
     */
    @Test
    public void testComputeGaussianRadiusForSupport()
    {
        System.out.println("computeGaussianRadiusForSupport");
        double areaPercentage = 0.9;
        double sigma = 11.31;
        double expResult = 18.60329;
        double result = LowpassFilter.computeGaussianRadiusForSupport(areaPercentage, sigma);
        assertEquals(expResult, result, 0.0001);
    }
    
    @Test
    public void testPerformance()
    {
        try {
            BufferedImage image = ImageIO.read(new File("testdata/images/Bektas_Sena_OEGD_20070619_2943-0.png"));
            Frame f = Frame.convertBufferedImageToFrameOptimized(image);
            
            LowpassFilter gaussianSeparable = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 5, 1.5);
            LowpassFilter gaussianFFT = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 5, 1.5);
            
            
            PerformanceTimer timer1 = new PerformanceTimer();
            timer1.startTimer();
            for(int i = 0; i < 100; i++) {
                gaussianSeparable.filterData(f.getRedData());
            }
            timer1.stopTimer();
            
            System.out.println("Convolution 128x128 Separable: "+timer1.getSeconds());
            
            PerformanceTimer timer2 = new PerformanceTimer();
            timer2.startTimer();
            for(int i = 0; i < 100; i++)  {
                gaussianFFT.filterDataFFT(f.getRedData());
            }
            timer2.stopTimer();
            
            System.out.println("Convolution 128x128 FFT: "+timer2.getSeconds());
            
            
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testComputeGaussianMatrixWithAreaIntegration1DValue()
    {
        try {
            LowpassFilter lp = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 127, 5);
            
            double [] filter = lp.computeGaussianMatrixWithAreaIntegration1D(5, 0.234);
            double [] expected = { 0, 0.001256283562983, 0.997487432874034, 0.001256283562983, 0};
            assert(ArrayTools.compare(filter, expected, 0.00001d));
            
        } catch (Exception ex) {
            Logger.getLogger(LowpassFilterTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
        
    }
    
    
    @Test
    public void testComputeGaussianMatrixWithAreaIntegration2DValue()
    {
          try {
              LowpassFilter lp = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 127, 5);
            
              double [][] filter = lp.computeGaussianMatrixWithAreaIntegration2D(5, 0.234);
            
              double[][] expected = {{0, 0, 0, 0, 0},
                                     {0, 0.000001578248391, 0.001253127066202, 0.000001578248391, 0},
                                     {0, 0.001253127066202, 0.994981178741630, 0.001253127066202, 0},
                                     {0, 0.000001578248391, 0.001253127066202, 0.000001578248391, 0},
                                     {0, 0, 0, 0, 0}};
              
            assert(ArrayTools.compare(filter, expected, 0.000001d));
            
        } catch (Exception ex) {
            Logger.getLogger(LowpassFilterTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
    }
    
    @Test(expected = Exception.class)
    public void testComputeGaussianMatrixWithAreaIntegration2DException()
            throws Exception {
        LowpassFilter lp = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 128);
        lp.computeGaussianMatrixWithAreaIntegration2D(128, 5);
    }
    
    @Test(expected = Exception.class)
    public void testComputeGaussianMatrixWithAreaIntegration1DException()
            throws Exception {
        LowpassFilter lp = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 128);
        lp.computeGaussianMatrixWithAreaIntegration1D(128, 5);
    }
    
    
    @Test
    public void testSeparableGaussianConvolution()
    {
         try 
        {
            BufferedImage rand2 = ImageIO.read(new File("testdata/jceliac.tools.filter/rand2.png"));
            Frame rand2Frame = Frame.convertBufferedImageToFrameOptimized(rand2);
            double[][] I1_1_expectedResult = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/I1_1_fft_data.txt"));
            double[][] I1_2_expectedResult = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/I1_2_fft_data.txt"));
            double[][] I1_3_expectedResult = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/I1_3_fft_data.txt"));
            
            LowpassFilter g2d_1 = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 127, 4.123);
            LowpassFilter g2d_2 = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 3,   4.123);
            LowpassFilter g2d_3 = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 7,   0.5043);
            
            // frame is grayscale, so each colorchannel is the gray data
            double [][] I1_1_result = g2d_1.filterData(rand2Frame.getGrayData());
            double [][] I1_2_result = g2d_2.filterData(rand2Frame.getGrayData());
            double [][] I1_3_result = g2d_3.filterData(rand2Frame.getGrayData());
            
            assert(ArrayTools.compare(I1_1_expectedResult, I1_1_result, 0.0001d));
            assert(ArrayTools.compare(I1_2_expectedResult, I1_2_result, 0.0001d));
            assert(ArrayTools.compare(I1_3_expectedResult, I1_3_result, 0.0001d));

        }catch(Exception e) {
            fail();
        }
    }
    
    @Test
    public void testNonSeparableGaussianConvolution()
    {
         try 
        {
            BufferedImage rand2 = ImageIO.read(new File("testdata/jceliac.tools.filter/rand2.png"));
            Frame rand2Frame = Frame.convertBufferedImageToFrameOptimized(rand2);
            double[][] I1_1_expectedResult = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/I1_1_fft_data.txt"));
            double[][] I1_2_expectedResult = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/I1_2_fft_data.txt"));
            double[][] I1_3_expectedResult = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/I1_3_fft_data.txt"));
            
            LowpassFilter g2d_1 = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 127, 4.123);
            LowpassFilter g2d_2 = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 3,   4.123);
            LowpassFilter g2d_3 = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 7,   0.5043);
            
            // frame is grayscale, so each colorchannel is the gray data
            double [][] I1_1_result = g2d_1.filterDataNonSeparable(rand2Frame.getGrayData());
            double [][] I1_2_result = g2d_2.filterDataNonSeparable(rand2Frame.getGrayData());
            double [][] I1_3_result = g2d_3.filterDataNonSeparable(rand2Frame.getGrayData());
            
            assert(ArrayTools.compare(I1_1_expectedResult, I1_1_result, 0.0001d));
            assert(ArrayTools.compare(I1_2_expectedResult, I1_2_result, 0.0001d));
            assert(ArrayTools.compare(I1_3_expectedResult, I1_3_result, 0.0001d));

        }catch(Exception e) {
            fail();
        }
    }
    

}
