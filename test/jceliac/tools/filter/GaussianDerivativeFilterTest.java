/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.Frame;
import jceliac.tools.export.MatrixImporter;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class GaussianDerivativeFilterTest
{
    
    /**
     * Test of filterData method, of class GaussianDerivativeFilter.
     */
    @Test
    public void testFilterData() throws Exception
    {
        
        try 
        {
            BufferedImage testDataImage = ImageIO.read(new File("testdata/GaussianDerivativeFilterData/testdata.png"));
            Frame testData = Frame.convertBufferedImageToFrameOptimized(testDataImage);

            GaussianDerivativeFilter gx = new GaussianDerivativeFilter(GaussianDerivativeFilter.EDerivativeType.DX,33,Math.sqrt(5.0d));
            GaussianDerivativeFilter gy = new GaussianDerivativeFilter(GaussianDerivativeFilter.EDerivativeType.DY,33,Math.sqrt(5.0d));
            GaussianDerivativeFilter gxx = new GaussianDerivativeFilter(GaussianDerivativeFilter.EDerivativeType.DXDX,33,Math.sqrt(5.0d));
            GaussianDerivativeFilter gyy = new GaussianDerivativeFilter(GaussianDerivativeFilter.EDerivativeType.DYDY,31,Math.sqrt(5.0d));
            
            double [][] gxExpectedResult = MatrixImporter.importDoubleMatrix(new File("testdata/GaussianDerivativeFilterData/gx.csv"));
            double [][] gyExpectedResult = MatrixImporter.importDoubleMatrix(new File("testdata/GaussianDerivativeFilterData/gy.csv"));
            double [][] gxxExpectedResult = MatrixImporter.importDoubleMatrix(new File("testdata/GaussianDerivativeFilterData/gxx.csv"));
            double [][] gyyExpectedResult = MatrixImporter.importDoubleMatrix(new File("testdata/GaussianDerivativeFilterData/gyy.csv"));
           
            double [][] gxFilteredData =  gx.filterData(testData.getRedData());
            double [][] gyFilteredData =  gy.filterData(testData.getRedData());
            double [][] gxxFilteredData = gxx.filterData(testData.getRedData());
            double [][] gyyFilteredData = gyy.filterData(testData.getRedData());
            
            if(ArrayTools.compare(gxExpectedResult, gxFilteredData, 0)) {
                fail("gx Filter failed!");
            }
            if(ArrayTools.compare(gyExpectedResult, gyFilteredData, 0)) {
                fail("gy Filter failed!");
            }
            if(ArrayTools.compare(gxxExpectedResult, gxxFilteredData, 0)) {
                fail("gxx Filter failed!");
            }
            if(ArrayTools.compare(gyyExpectedResult, gyyFilteredData, 0)) {
                fail("gyy Filter failed!");
            }
        }catch(Exception e) {
            e.printStackTrace();
            fail("Could not read testdata!");
        }
        
    }

}
