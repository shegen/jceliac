/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter;

import jceliac.tools.data.Frame;
import javax.imageio.ImageIO;
import java.io.File;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.export.MatrixImporter;
import jceliac.tools.timer.PerformanceTimer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class DataFilter2DTest
{
    
   


    /**
     * Test of convolveMirrorSeparable method, of class DataFilter2D.
     */
    @Test
    public void testConvolveMirrorSeparable()
    {
        try 
        {
            BufferedImage testDataImage = ImageIO.read(new File("testdata/GaussianDerivativeFilterData/testdata.png"));
            Frame testData = Frame.convertBufferedImageToFrameOptimized(testDataImage);
            double [][] expectedResult = MatrixImporter.importDoubleMatrix(new File("testdata/GaussianDerivativeFilterData/datafilter2dtestresult.csv"));
            double [][] expectedResult2 = MatrixImporter.importDoubleMatrix(new File("testdata/GaussianDerivativeFilterData/datafilter2dtestresult2.csv"));
            
            double [] f = new double[] { 0.25, 0.5, 1 ,-0.5, -0.25};
            double[][] result = DataFilter2D.convolveMirrorSeparable(testData.getRedData(), f, 0);
            
            // matlab has a weird rounding behaviour in imfilter, so use a sanity margin of 1 
            if(ArrayTools.compare(expectedResult, result,1) == false) {
                fail("Results mismatch!");
            }
            result = DataFilter2D.convolveMirrorSeparable(testData.getRedData(), f, 1);
            // matlab has a weird rounding behaviour in imfilter, so use a sanity margin of 2 because of error propagation from above!
            if(ArrayTools.compare(expectedResult2, result,2) == false) {
                fail("Results mismatch!");
            }
            
            double [] nonSymmetricFilter =  {1,2,3,4,5};
            double [][] input = new double[][] {
                {0.963089, 0.624060, 0.037739, 0.261871, 0.106762},
                {0.546806, 0.679136, 0.885168, 0.335357, 0.653757},
                {0.521136, 0.395515, 0.913287, 0.679728, 0.494174},
                {0.231594, 0.367437, 0.796184, 0.136553, 0.779052},
                {0.488898, 0.987982, 0.098712, 0.721227, 0.715037}
            };
            double[][] expResult = new double [][] {
                                                {11.148, 10.877, 8.0554, 4.3772, 2.0319},
                                                {9.4668, 9.0644, 9.4305, 9.9037, 9.3714},
                                                {7.3298, 8.3831, 8.7812, 9.1524, 10.436},
                                                {4.9894, 4.9156, 6.0684, 7.7687, 8.5589},
                                                {10.437, 8.2827, 8.85, 9.6436, 7.6749}
                                             };
            result = DataFilter2D.convolveMirrorSeparable(input, nonSymmetricFilter, 1);
            if(ArrayTools.compare(result, expResult, 0.001) == false) {
                fail();
            }
            
            double [][] expResult2 = new double[][] {
                {  61.704},{ 70.302},{ 65.292 }
            };
           
            double [][] nonSymmetricFilter2 = { {1,2,3,4,5}, {6,7,8,9,10}, {11,12,13,14,15}};
            result = DataFilter2D.convolveValid(input, nonSymmetricFilter2);
            if(ArrayTools.compare(result, expResult2, 0.001) == false) {
                fail();
            }
            
            double[][] expResult3 = new double[][]{
                                            {79.943, 73.567, 55.444, 35.716, 25.06},
                                            {76.75, 71.79, 61.704, 50.841, 43.906},
                                            {68.889, 67.45, 70.302, 73.781, 73.306},
                                            {60.193, 60.706, 65.292, 70.625, 72.418},
                                            {61.068, 53.043, 61.936, 71.829, 65.039}
                                        };
            double [][] result3 = DataFilter2D.convolveMirror(input, nonSymmetricFilter2, false);
            if(ArrayTools.compare(result3, expResult3, 0.001) == false) {
                fail();
            }

            
        }catch(Exception e) {
            e.printStackTrace();
            fail("Could not read testdata!");
        }
    }
    
     @Test
    public void testConvolveMirrorSeparableSpeed()
    {
        try 
        {
            double [][] testData = new double[128][128];
            double [] filter = new double[128];
            
            for(int i = 0; i < 128; i++) {
                for(int j = 0; j < 128; j++) {
                    testData[i][j] = Math.random();
                }
                filter[i] = Math.random();
            }
            PerformanceTimer timer = new PerformanceTimer();
            
            
            timer.startTimer();
            for(int c = 0; c < 1000; c++) {
                DataFilter2D.convolveMirrorSeparable(testData, filter, 0);
                DataFilter2D.convolveMirrorSeparable(testData, filter, 1);
            }
            timer.stopTimer();
            
            System.out.println("Elapsed Time: "+timer.getSeconds());
        }catch(Exception e) {
            e.printStackTrace();
            fail("Could not read testdata!");
        }
    }

   
}
