/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter;


import javax.imageio.ImageIO;
import java.io.File;
import jceliac.tools.data.Frame;
import java.awt.image.BufferedImage;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.export.MatrixImporter;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class ConvolutionFFTTest
{

    /**
     * Test of convolveLinearFFT method, of class ConvolutionFFT.
     */
    @Test
    public void testConvolveLinearFFT_3args()
    {
        System.out.println("convolveLinearFFT");

        try {
            BufferedImage testDataImage = ImageIO.read(new File("testdata/GaussianDerivativeFilterData/testdata.png"));
            Frame testData = Frame.convertBufferedImageToFrameOptimized(testDataImage);
            ConvolutionFFT instance = new ConvolutionFFT();

            double[][] expectedResult = MatrixImporter.importDoubleMatrix(new File("testdata/overlapadd2d2f/overlapadd2d2fresult.csv"));

            double[] g1 = new double[]{0.000000000001360, 0.000000000030186, 0.000000000548597,
                0.000000008162972, 0.000000099445360, 0.000000991886165,
                0.000008099910956, 0.000054155149644, 0.000296442440144,
                0.001328562843977, 0.004874891216128, 0.014644982561928,
                0.036020844672157, 0.072537073483930, 0.119593415967294,
                0.161434225871553, 0.178412411615296, 0.161434225871553,
                0.119593415967294, 0.072537073483930, 0.036020844672157,
                0.014644982561928, 0.004874891216128, 0.001328562843977,
                0.000296442440144, 0.000054155149644, 0.000008099910956,
                0.000000991886165, 0.000000099445360, 0.000000008162972,
                0.000000000548597, 0.000000000030186, 0.000000000001360};

            double[] g2 = new double[]{0.000000000070441, 0.000000001370529, 0.000000021624839,
                0.000000276285736, 0.000002852762525, 0.000023745762416,
                0.000158807184535, 0.000849414457980, 0.003609596754588,
                0.012064270749628, 0.031188387323292, 0.060448447444074,
                0.081773554168928, 0.059880624038968, -0.024681614638961,
                -0.133266779622394, -0.184103211480332, -0.133266779622394,
                -0.024681614638961, 0.059880624038968, 0.081773554168928,
                0.060448447444074, 0.031188387323292, 0.012064270749628,
                0.003609596754588, 0.000849414457980, 0.000158807184535,
                0.000023745762416, 0.000002852762525, 0.000000276285736,
                0.000000021624839, 0.000000001370529, 0.000000000070441};

            double[][] result = instance.convolveLinearFFT(testData.getRedData(), g1, g2);
            if (ArrayTools.compare(result, expectedResult, 0.01) == false) {
                fail("Result mismatch!");
            }
        } catch (Exception e) {
            fail("Bad things happened!");
        }
    }
    
    // g2d_1 = createTinyGaussian(127, 4.123, '2d');
    // I1_1 = imfilter(rand2,g2d_1, 'symmetric', 'conv');
    //
    // g2d_2 = createTinyGaussian(3, 4.123, '2d');
    // I1_2 = imfilter(rand2,g2d_2, 'symmetric', 'conv');
    //
    // g1d_3 = createTinyGaussian(7, 0.5043, '1d');
    // I1_3 = imfilter(rand2,g2d_3, 'symmetric', 'conv');
    //
    @Test
    public void testFFT()
    {
        try 
        {
            BufferedImage rand2 = ImageIO.read(new File("testdata/jceliac.tools.filter/rand2.png"));
            Frame rand2Frame = Frame.convertBufferedImageToFrameOptimized(rand2);
            double[][] I1_1_expectedResult = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/I1_1_fft_data.txt"));
            double[][] I1_2_expectedResult = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/I1_2_fft_data.txt"));
            double[][] I1_3_expectedResult = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.filter/I1_3_fft_data.txt"));
            
            LowpassFilter g2d_1 = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 127, 4.123);
            LowpassFilter g2d_2 = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 3,   4.123);
            LowpassFilter g2d_3 = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 7,   0.5043);
            
            // frame is grayscale, so each colorchannel is the gray data
            double [][] I1_1_result = g2d_1.filterDataFFT(rand2Frame.getGrayData());
            double [][] I1_2_result = g2d_2.filterDataFFT(rand2Frame.getGrayData());
            double [][] I1_3_result = g2d_3.filterDataFFT(rand2Frame.getGrayData());
            
            assert(ArrayTools.compare(I1_1_expectedResult, I1_1_result, 0.0001d));
            assert(ArrayTools.compare(I1_2_expectedResult, I1_2_result, 0.0001d));
            assert(ArrayTools.compare(I1_3_expectedResult, I1_3_result, 0.0001d));

        }catch(Exception e) {
            fail();
        }
    }
}
