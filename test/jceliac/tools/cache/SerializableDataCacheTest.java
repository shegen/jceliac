package jceliac.tools.cache;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import javax.imageio.ImageIO;
import jceliac.tools.filter.scalespace.LaplacianScalespaceRepresentation;
import jceliac.tools.filter.scalespace.ScalespaceParameters;
import jceliac.tools.filter.scalespace.ScalespaceRepresentation;
import jceliac.tools.cache.Cacheable;
import jceliac.tools.cache.SerializableDataCache;
import jceliac.tools.data.Frame;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author shegen
 */
@Ignore("Test are broken I think!")
public class SerializableDataCacheTest {
    
    public SerializableDataCacheTest() {
    }
    

    
    @Test
    public void testFrameCaching()
            throws Exception
    {
       
        BufferedImage image = ImageIO.read(new File("testdata/images/Bektas_Sena_OEGD_20070619_2943-0.png"));
        Frame frame1 = Frame.convertBufferedImageToFrameOptimized(image);
        
        SerializableDataCache cache = new SerializableDataCache("/tmp", 2, Frame.class);
        cache.setMaxMemoryConsumption(5000);
        if(cache.containsData(frame1.computeHashCode(() -> "0-0")) == false) {
              cache.cacheData(frame1, () -> "0-0");
        }
        if(cache.containsData(frame1.computeHashCode(() -> "1-0")) == false) {
              cache.cacheData(frame1, () -> "1-0");
        }
        if(cache.containsData(frame1.computeHashCode(() -> "2-0")) == false) {
              cache.cacheData(frame1, () -> "2-0");
        }
        if(cache.containsData(frame1.computeHashCode(() -> "3-0")) == false) {
              cache.cacheData(frame1, () -> "3-0");
        }
        if(cache.containsData(frame1.computeHashCode(() -> "4-0")) == false) {
              cache.cacheData(frame1, () -> "4-0");
        }
        Frame retrievedFrame = (Frame)cache.retrieveData(frame1.computeHashCode(()-> "0-0"));
        
        assert(frame1.almostEquals(retrievedFrame));
        cache.close();
    }

    @Test
    public void testCaching()
            throws Exception
    {
        
        SerializableDataCache cache = new SerializableDataCache("/tmp", 1, CacheableData.class);
        cache.setMaxMemoryConsumption(3);
        
        if(cache.containsData(new CacheableData(1,1).computeHashCode(() -> "1")) == false)
        {
            cache.cacheData(new CacheableData(1,1), () -> "1");
        }
        
        // this should lead to a write of cached data
        if(cache.containsData(new CacheableData(3,2).computeHashCode(()-> "2")) == false)
        {
            cache.cacheData(new CacheableData(3,2), () -> "2");
        }
        
        assert(cache.containsData("2") == true);
        assert(cache.containsData("1") == true);
        assert(cache.containsData("5") == false);
        
        // this should be read from disk
        CacheableData ret1 = (CacheableData)cache.retrieveData(new CacheableData(1,1).computeHashCode(()->"1"));
        CacheableData ret2 = (CacheableData)cache.retrieveData(new CacheableData(3,2).computeHashCode(()->"2"));
        
       assert(ret1.featureValue == 1);
       assert(ret2.featureValue == 2);
       cache.close();
    }
    
    @Test
    public void testGetMemoryConsumption()
            throws Exception
    {
        int numData = (int)(Math.random() * 100);
        if(numData == 0) {
            numData = 1;
        }
        
        SerializableDataCache cache = new SerializableDataCache("/tmp", 17, CacheableData.class);
        cache.setMaxMemoryConsumption(Integer.MAX_VALUE);
        CacheableData  d;
        double expectedResult = 0;
        
        for(int i = 0; i < numData; i++) {
            int mem = (int)(Math.random() * 100);
            d = new CacheableData(mem, i);
            expectedResult += mem;
            final int hashInfo = i;
            cache.cacheData(d,() -> String.valueOf(hashInfo));
        }
        Method [] methods = SerializableDataCache.class.getDeclaredMethods();
        
        Method m = Arrays.asList(methods).stream()
                    .filter((a) -> a.getName().compareTo("getMemoryConsumption") == 0)
                    .findFirst().get();
        m.setAccessible(true);
        int result = (Integer)m.invoke(cache);
        
        assert(result == expectedResult);
    }
    
    
   
    
    
}
