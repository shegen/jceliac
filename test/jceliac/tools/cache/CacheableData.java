/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jceliac.tools.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class CacheableData implements Cacheable
{

    public int consumedMemory;
    public int featureValue;

    public CacheableData() {
    }

    public CacheableData(int consumedMemory, int featureValue) {
        this.consumedMemory = consumedMemory;
        this.featureValue = featureValue;
    }
    
    @Override
    public int getConsumedMemorySize() {
        return this.consumedMemory;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(this.featureValue);
        out.writeObject(this.consumedMemory);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        this.featureValue = (Integer) in.readObject();
        this.consumedMemory = (Integer) in.readObject();
    }
}