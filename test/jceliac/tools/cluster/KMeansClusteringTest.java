/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jceliac.tools.cluster;

import java.io.File;
import jceliac.JCeliacGenericException;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.export.MatrixImporter;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class KMeansClusteringTest {
    
    public KMeansClusteringTest() {
    }

    /**
     * Test of iterate method, of class KMeansClustering.
     */
    @Test
    public void testClustering()
            throws JCeliacGenericException
    {
        try {
            double [][] randomDataX2 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.cluster/random_data1.csv"));
            KMeansClustering kmeansX2 = new KMeansClustering(randomDataX2, 2, new int [] {49, 149}, 200);
            kmeansX2.cluster();
            KMeansDataPoint [] centersX2 = kmeansX2.getClusterCenters();
            
            assertEquals(centersX2[0].dataVector[0],  1.11876689794177, 1e-10);
            assertEquals(centersX2[0].dataVector[1],  0.952921019751279, 1e-10);
            assertEquals(centersX2[1].dataVector[0],  -1.11777589216333  , 1e-10);
            assertEquals(centersX2[1].dataVector[1],  -1.06067671480859, 1e-10);
            
            double [][] randomDataX8 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.cluster/random_data2.csv"));
            KMeansClustering kmeansX8 = new KMeansClustering(randomDataX8, 2, new int [] {49, 149}, 200);
            kmeansX8.cluster();
            KMeansDataPoint [] centersX8 = kmeansX8.getClusterCenters();
            
            assertEquals(centersX8[0].dataVector[0],  0.948417852135866, 1e-10);
            assertEquals(centersX8[0].dataVector[1],  0.919334997698761, 1e-10);
            assertEquals(centersX8[0].dataVector[2],  0.961038401021482, 1e-10);
            assertEquals(centersX8[0].dataVector[3],  0.923244403046444, 1e-10);
            assertEquals(centersX8[0].dataVector[4],  1.02945291754194, 1e-10);
            assertEquals(centersX8[0].dataVector[5],  0.8617681394608, 1e-10);
            assertEquals(centersX8[0].dataVector[6],  1.00897564760608, 1e-10);
            assertEquals(centersX8[0].dataVector[7],  0.901276266145286, 1e-10);
            
            assertEquals(centersX8[1].dataVector[0],  -0.931469408844581, 1e-10);
            assertEquals(centersX8[1].dataVector[1],  -0.999158653327911, 1e-10);
            assertEquals(centersX8[1].dataVector[2],  -1.05521086320637, 1e-10);
            assertEquals(centersX8[1].dataVector[3],  -0.90973374424428, 1e-10);
            assertEquals(centersX8[1].dataVector[4],  -1.05847926579654, 1e-10);
            assertEquals(centersX8[1].dataVector[5],  -0.981404781071283, 1e-10);
            assertEquals(centersX8[1].dataVector[6],  -1.09751210404534, 1e-10);
            assertEquals(centersX8[1].dataVector[7],  -0.921384095234988, 1e-10);
            
          
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
    
    
    
        /**
     * Test of iterate method, of class KMeansClustering.
     */
    @Test
    public void testClustering2()
            throws JCeliacGenericException
    {
        try {

            double[][] randomData = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.cluster/random_data3.csv"));
            double[][] expectedCenters = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.cluster/data3_centers.csv"));
             int [] seedIndices = new int[]{0, 862, 1725, 2587, 3449, 4311, 5174, 6036, 6898, 7760,
                                            8623, 9485, 10347, 11209, 12072, 12934, 13796, 14658, 15521,
                                            16383}; 
            KMeansClustering kmeans = new KMeansClustering(randomData, 20, seedIndices, 200);
            kmeans.cluster();
            KMeansDataPoint[] centers = kmeans.getClusterCenters();
            for(int i = 0; i < expectedCenters.length; i++) {
                for(int j = 0; j < expectedCenters[0].length; j++) {
                    assertEquals(centers[i].dataVector[j], expectedCenters[i][j],
                                 1e-10);
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
    
    
    
    
            
}