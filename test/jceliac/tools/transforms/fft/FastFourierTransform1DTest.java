/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.tools.transforms.fft;

import java.io.File;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.export.MatrixImporter;
import jceliac.tools.transforms.dtcwt.ComplexArray1D;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class FastFourierTransform1DTest
{
   
    @Test
    public void testTransform()
    {
          try 
        {
            double [] inputData = MatrixImporter.importDoubleMatrix1D(new File("/home/stud3/shegen/JCeliac/testdata/jceliac.tools.transforms.fft/input_data1.csv"));
            ComplexArray1D resultData = MatrixImporter.importComplexMatrix1D(new File("/home/stud3/shegen/JCeliac/testdata/jceliac.tools.transforms.fft/result_data1.csv"));
            FastFourierTransform fft = new FastFourierTransform(inputData.length);
            ComplexArray1D complexResult = fft.transformFullOptimized(inputData);
            
            if (ArrayTools.compare(resultData, complexResult, 1e-05) == false) {
                fail("Result mismatch!");
            }
           
           
        }catch(Exception e) {
            e.printStackTrace();
            fail();
        }
     
    }

  
}
