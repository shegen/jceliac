/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.tools.transforms.logpolar;

import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.Frame;
import jceliac.tools.export.MatrixImporter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class LogpolarTransformTest
{
    

    /**
     * Test of transformLogPolar method, of class LogpolarTransform.
     * @throws java.lang.Exception
     */
    @Test
    public void testTransformLogPolar() 
            throws Exception
    {
        System.out.println("transformLogPolar");
        try {
            BufferedImage image = ImageIO.read(new File("testdata/images/44-scale_3_im_3_col.png"));
            Frame frame = Frame.convertBufferedImageToFrameOptimized(image);
            
            double [][] expectedTransformed = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.transforms.logpolar/result.csv"));
            double [][] transformedValue = LogpolarTransform.transformLogPolar(frame.getGrayData());
            if(ArrayTools.compare(expectedTransformed, transformedValue, 1e-5) == false) {
                fail();
            }
        }catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
}
