/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */
package jceliac.tools.transforms.steerablePyramid;

import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.Frame;
import jceliac.tools.export.MatrixImporter;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class SteerablePyramidTransformTest
{

    /**
     * Test of transformForward method, of class SteerablePyramidTransform.
     */
    @Test
    public void testTransformForward()
            throws Exception
    {

        BufferedImage image = ImageIO.read(new File("testdata/images/44-scale_3_im_3_col.png"));
        Frame content = Frame.convertBufferedImageToFrameOptimized(image);

        double[][] dataChannel = content.getGrayData();
        double[][] expectedSubband1 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.transforms.steerablePyramid/subband-1.csv"));
        double[][] expectedSubband2 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.transforms.steerablePyramid/subband-2.csv"));
        double[][] expectedSubband3 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.transforms.steerablePyramid/subband-3.csv"));
        double[][] expectedSubband4 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.transforms.steerablePyramid/subband-4.csv"));
        double[][] expectedSubband5 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.transforms.steerablePyramid/subband-5.csv"));
        double[][] expectedSubband6 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.transforms.steerablePyramid/subband-6.csv"));
        double[][] expectedSubband7 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.transforms.steerablePyramid/subband-7.csv"));
        double[][] expectedSubband8 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.transforms.steerablePyramid/subband-8.csv"));
        double[][] expectedSubband9 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.transforms.steerablePyramid/subband-9.csv"));
        double[][] expectedSubband10 = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.tools.transforms.steerablePyramid/subband-10.csv"));

        SteerablePyramidTransform instance = new SteerablePyramidTransform();
        instance.transformForward(dataChannel, 5, 4);
        SteerablePyramidDecomposition decomposition = instance.getDecomposition();

        if(ArrayTools.compare(expectedSubband1, decomposition.getHighpassSubband(), 1e-5) == false) {
            fail();
        }
        if(ArrayTools.compare(expectedSubband2, decomposition.getBandpassSubband(0,0), 1e-5) == false) {
            fail();
        }
        if(ArrayTools.compare(expectedSubband3, decomposition.getBandpassSubband(0,1), 1e-5) == false) {
            fail();
        }
        if(ArrayTools.compare(expectedSubband4, decomposition.getBandpassSubband(0,2), 1e-5) == false) {
            fail();
        }
        if(ArrayTools.compare(expectedSubband5, decomposition.getBandpassSubband(0,3), 1e-5) == false) {
            fail();
        }
        if(ArrayTools.compare(expectedSubband6, decomposition.getBandpassSubband(1,0), 1e-5) == false) {
            fail();
        }
        if(ArrayTools.compare(expectedSubband7, decomposition.getBandpassSubband(1,1), 1e-5) == false) {
            fail();
        }
        if(ArrayTools.compare(expectedSubband8, decomposition.getBandpassSubband(1,2), 1e-5) == false) {
            fail();
        }
        if(ArrayTools.compare(expectedSubband9, decomposition.getBandpassSubband(1,3), 1e-5) == false) {
            fail();
        }
        if(ArrayTools.compare(expectedSubband10, decomposition.getLowpassSubband(), 1e-5) == false) {
            fail();
        }
    }

}
