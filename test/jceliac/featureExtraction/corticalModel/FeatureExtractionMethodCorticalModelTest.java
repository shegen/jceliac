/*
 * 
 JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at> 
 The source code of this software is not yet made available to the open source 
 community. If you obtained this source code without permission of the author 
 please remove all the source files.

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 * Each line should be prefixed with  * 
 */

package jceliac.featureExtraction.corticalModel;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.tools.data.Frame;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class FeatureExtractionMethodCorticalModelTest
{
    
    public FeatureExtractionMethodCorticalModelTest()
    {
    }
    

    /**
     * Test of extractDiscriminativeFeatures method, of class FeatureExtractionMethodCorticalModel.
     * @throws java.lang.Exception
     */
    @Test
    public void testExtractFeatures() 
            throws Exception
    {
         try {
            BufferedImage image = ImageIO.read(new File("testdata/images/44-scale_3_im_3_col.png"));
            System.out.println("FeatureExtractionMethodMultiFractalSpectrum: test extractFeatures");
            FeatureExtractionMethodCorticalModel instance = new FeatureExtractionMethodCorticalModel(new FeatureExtractionParametersCorticalModel(FeatureExtractionParametersCorticalModel.ECorticalModelType.ICM));
            instance.initialize();

            DiscriminativeFeatures mfsFeatures = instance.extractFeatures(Frame.convertBufferedImageToFrameOptimized(image));
            double[] featureData  = mfsFeatures.getAllFeatureVectorData();
            
            double [] expectedFeatures = {0.918946, 0.862382, 0.535283, 0.235433, 0.482697, 
                                          0.591898, 0.445057, 0.454037, 0.522288, 0.560790, 
                                          0.564936, 0.586670, 0.466203, 0.593158, 0.580428, 
                                          0.510459, 0.571673, 0.603295, 0.584437, 0.530048, 
                                          0.590793, 0.584437, 0.580267, 0.618539, 0.590161, 
                                          0.522643, 0.616731, 0.614616, 0.585554, 0.581232, 
                                          0.593473, 0.601592, 0.601127, 0.591109, 0.620789, 
                                          0.609596, 0.596922};
            
            assertArrayEquals(featureData, expectedFeatures, 0.01);
            
            
            instance = new FeatureExtractionMethodCorticalModel(new FeatureExtractionParametersCorticalModel(FeatureExtractionParametersCorticalModel.ECorticalModelType.SCM));
            instance.initialize();

            mfsFeatures = instance.extractFeatures(Frame.convertBufferedImageToFrameOptimized(image));
            featureData = mfsFeatures.getAllFeatureVectorData();

            double [] expectedFeaturesSCM = {0.295588, 0.429791, 0.174890, 0.000000, 0.000000, 
                                             0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
                                             0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
                                             0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
                                             0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
                                             0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
                                             0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
                                             0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
                                             0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
                                             0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
                                             0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
                                             0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
                                             0.040408, 0.032334, 0.047970, 0.055589, 0.048030, 
                                             0.058465, 0.049658, 0.041729, 0.054495, 0.044066, 
                                             0.035984, 0.050605, 0.043966, 0.035883, 0.050534, 
                                             0.049273, 0.041328, 0.054232, 0.044266, 0.036186, 
                                             0.050747, 0.044066, 0.035984, 0.050605, 0.045560, 
                                             0.037500, 0.051660, 0.044366, 0.036288, 0.050818, 
                                             0.041230, 0.033147, 0.048569, 0.038851, 0.030808, 
                                             0.046824, 0.035355, 0.027441, 0.044192, 0.042555, 
                                             0.034465, 0.049525, 0.029162, 0.021696, 0.039294, 
                                             0.031303, 0.023650, 0.041025, 0.024737, 0.017774, 
                                             0.035565};
            
            assertArrayEquals(featureData, expectedFeaturesSCM, 0.01);
            

        } catch(IOException  e) {
            System.out.println(e.getMessage());
        } catch(JCeliacGenericException e) {
            fail();
        }
    }

    
}
