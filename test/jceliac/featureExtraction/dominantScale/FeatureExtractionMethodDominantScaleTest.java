/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.featureExtraction.dominantScale;

import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.dtcwt.FeatureExtractionMethodD3TCWTCyclicShift;
import jceliac.featureExtraction.dtcwt.FeatureExtractionParametersD3TCWTCyclicShift;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.Frame;
import jceliac.tools.export.MatrixImporter;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class FeatureExtractionMethodDominantScaleTest
{
    
    public FeatureExtractionMethodDominantScaleTest()
    {
    }

    @Test
    public void testExtractFeatures() throws Exception
    {
        try {
            BufferedImage image = ImageIO.read(new File("testdata/images/44-scale_3_im_3_col.png"));
            Frame content = Frame.convertBufferedImageToFrameOptimized(image);

            double [] expectedFeatures = MatrixImporter.importDoubleMatrix1D(new File("testdata/jceliac.featureExtraction.dominantScale/features1.csv"));
            FeatureExtractionParametersDominantScale params = new FeatureExtractionParametersDominantScale();
            params.numberOfOrientations = 4;
            params.numberOfScales = 2;
            
            FeatureExtractionMethodDominantScale instance = new FeatureExtractionMethodDominantScale(params);
            DiscriminativeFeatures result = instance.extractFeatures(content);
            double [] computedFeatureVector = result.getAllFeatureVectorData();
            if(ArrayTools.compare(expectedFeatures, computedFeatureVector, 1e-5) == false) {
                fail();
            }

        }catch(Exception e) {
            e.printStackTrace();
            fail(e.getLocalizedMessage());
        }
    }

    
}
