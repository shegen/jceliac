/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import jceliac.tools.data.Frame;
import jceliac.tools.filter.scalespace.LaplacianScalespaceRepresentation;
import jceliac.tools.filter.scalespace.ScaleLevelIndexedDataSupplier;
import jceliac.tools.filter.scalespace.ScalespaceParameters;
import jceliac.tools.filter.scalespace.ScalespacePoint;
import jceliac.tools.filter.scalespace.ScalespaceRepresentation;
import jceliac.tools.filter.scalespace.ScalespaceStructureTensor;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class OrientationEstimationTest {
    
    public OrientationEstimationTest() {
    }

    @Test
    public void estimateOrientation() 
    {
        try {
           // BufferedImage image = ImageIO.read(new File("/home/stud3/shegen/WORK/data/Scale-Invariant-Data/Kylberg/128x128-Affine-Tiny/images_a/evaluation/scalex-0.500-scaley-0.500-rot-0.0/scarf2/scarf2-a-p003.png"));
            BufferedImage image = ImageIO.read(new File("/home/stud3/shegen/WORK/data/Scale-Invariant-Data/Kylberg/128x128-Affine-Tiny/images_a/evaluation/scalex-0.500-scaley-0.500-rot-0.0/wall1/wall1-a-p004.png"));
            Frame frame = Frame.convertBufferedImageToFrameOptimized(image);
            ScalespaceParameters scalespaceParams = new ScalespaceParameters(Math.sqrt(2.0d),
                                                             -4, 
                                                              8,
                                                              0.25,
                                                              2.1214);
            
            ScalespaceRepresentation scaleRep = new LaplacianScalespaceRepresentation(frame.getGrayData(),
                                                                                      scalespaceParams);
            scaleRep.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_SUM);
            ScalespaceStructureTensor structureTensor = new ScalespaceStructureTensor(scaleRep);
            ScaleLevelIndexedDataSupplier<ArrayList<ScalespacePoint>> ip1D = scaleRep.computeMaxima1D();
            scaleRep.computeMaxima();
            
        //    OrientationEstimation estimation = new OrientationEstimation(scaleRep,
                    //                                                   24);
       //     estimation.estimateGlobalOrientation();
            //estimation.getDominantOrientation();
            
      
        }catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void computeAverageOrientationDifference()
    {
        
    }
}