/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jceliac.featureExtraction.affineScaleLBPScaleBias;

import jceliac.featureExtraction.SALBP.LBPScaleFilteredDataSupplier;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class LBPScaleFilteredDataStorageTest {
    
   

    /**
     * Test of retrieveLBPScaleFilteredDataForLBPRadius method, of class LBPScaleFilteredDataSupplier.
     */
    @Test
    public void testComputeSigmaForLBPRadius() 
            throws Exception {
       
        LBPScaleFilteredDataSupplier instance = new LBPScaleFilteredDataSupplier(null);
        List <Method> methods  = (List <Method>)Arrays.asList(LBPScaleFilteredDataSupplier.class.getDeclaredMethods());
        
        Method m =    methods.stream()
                      .filter(a -> a.getName().compareTo("computeSigmaForLBPRadius") == 0).findFirst().get();
        
        m.setAccessible(true);
        
        double sigma1 = (double) m.invoke(instance, 0.75d);
        double sigma2 = (double) m.invoke(instance, 1.0d);
        double sigma3 = (double) m.invoke(instance, 1.4141d);
        double sigma4 = (double) m.invoke(instance, 1.75d);
        double sigma5 = (double) m.invoke(instance, 2d);
        
        
        assertEquals(sigma1 , 0.2287/2.0d, 0.001d);
        assertEquals(sigma2 , 0.3049/2.0d, 0.001d);
        assertEquals(sigma3 , 0.4312/2.0d, 0.001d);
        assertEquals(sigma4 , 0.5336/2.0d, 0.001d);
        assertEquals(sigma5 , 0.6098/2.0d, 0.001d);  
    }
}