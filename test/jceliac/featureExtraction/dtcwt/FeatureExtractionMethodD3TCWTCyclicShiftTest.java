/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.featureExtraction.dtcwt;

import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.Frame;
import jceliac.tools.export.MatrixImporter;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class FeatureExtractionMethodD3TCWTCyclicShiftTest
{
   
    /**
     * Test of extractDiscriminativeFeatures method, of class FeatureExtractionMethodD3TCWTCyclicShift.
     */
    @Test
    public void testExtractFeatures() throws Exception
    {
        try {
            BufferedImage image = ImageIO.read(new File("testdata/images/44-scale_3_im_3_col.png"));
            Frame content = Frame.convertBufferedImageToFrameOptimized(image);
            double [] expectedFeatureVectorScaleEnergy = MatrixImporter.importDoubleMatrix1D(new File("testdata/jceliac.featureExtraction.dtcwt/d3tcwt_cyclic_shift_scale_energy1.csv"));
            double [] expectedFeatureVectorScaleEntropy = MatrixImporter.importDoubleMatrix1D(new File("testdata/jceliac.featureExtraction.dtcwt/d3tcwt_cyclic_shift_scale_entropy1.csv"));
            
            double [] expectedFeatureVectorScaleOrientationEnergy = MatrixImporter.importDoubleMatrix1D(new File("testdata/jceliac.featureExtraction.dtcwt/d3tcwt_cyclic_shift_scale_orientation_energy1.csv"));
            double [] expectedFeatureVectorScaleOrientationEntropy = MatrixImporter.importDoubleMatrix1D(new File("testdata/jceliac.featureExtraction.dtcwt/d3tcwt_cyclic_shift_scale_orientation_entropy1.csv"));
            
            FeatureExtractionParametersD3TCWTCyclicShift params = new FeatureExtractionParametersD3TCWTCyclicShift();
            params.numScales = 3;
            params.featureType = "energy";
            params.alignOrientationDimension = false;
            params.alignScaleDimension = true;
            
            FeatureExtractionMethodD3TCWTCyclicShift instance = new FeatureExtractionMethodD3TCWTCyclicShift(params);
            DiscriminativeFeatures result = instance.extractFeatures(content);
            double [] computedFeatureVector = result.getAllFeatureVectorData();
            if(ArrayTools.compare(expectedFeatureVectorScaleEnergy, computedFeatureVector, 1e-5) == false) {
                fail();
            }
            
            params = new FeatureExtractionParametersD3TCWTCyclicShift();
            params.numScales = 3;
            params.featureType = "entropy";
            params.alignOrientationDimension = false;
            params.alignScaleDimension = true;
            
            instance = new FeatureExtractionMethodD3TCWTCyclicShift(params);
            result = instance.extractFeatures(content);
            computedFeatureVector = result.getAllFeatureVectorData();
            if(ArrayTools.compare(expectedFeatureVectorScaleEntropy, computedFeatureVector, 0.5) == false) {
                fail();
            }
          
            
            params = new FeatureExtractionParametersD3TCWTCyclicShift();
            params.numScales = 3;
            params.featureType = "energy";
            params.alignOrientationDimension = true;
            params.alignScaleDimension = true;
            
            instance = new FeatureExtractionMethodD3TCWTCyclicShift(params);
            result = instance.extractFeatures(content);
            computedFeatureVector = result.getAllFeatureVectorData();
            if(ArrayTools.compare(expectedFeatureVectorScaleOrientationEnergy, computedFeatureVector, 1e-5) == false) {
                fail();
            }
            
            params = new FeatureExtractionParametersD3TCWTCyclicShift();
            params.numScales = 3;
            params.featureType = "entropy";
            params.alignOrientationDimension = true;
            params.alignScaleDimension = true;
            
            instance = new FeatureExtractionMethodD3TCWTCyclicShift(params);
            result = instance.extractFeatures(content);
            computedFeatureVector = result.getAllFeatureVectorData();
            if(ArrayTools.compare(expectedFeatureVectorScaleOrientationEntropy, computedFeatureVector, 0.5) == false) {
                fail();
            }
            
        }catch(Exception e) {
            e.printStackTrace();
            fail(e.getLocalizedMessage());
        }
    }

   
    
}
