/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBP;

import jceliac.tools.filter.scalespace.SecondMomentMatrix;
import jceliac.tools.timer.PerformanceTimer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class EllipseTest
{
    
    public EllipseTest()
    {
    }


    @Test
    public void testSecondMomentConversion()
    {
      
       SecondMomentMatrix smm = new SecondMomentMatrix(22.86,12.203,12.203,20.304);
        try {
            Ellipse e = new Ellipse(smm, 10, Ellipse.AREA_NORMALIZATION_MODE);
            assertEquals(e.a, 19.066, 0.01);
            assertEquals(e.b, 5.244, 0.01);
            assertEquals(e.phi, 0.73322, 0.01); 
       }catch(Exception e) {
           e.printStackTrace();
           fail();
       }
        
       try {
            Ellipse e = new Ellipse(smm, 10, Ellipse.CIRCUMFERENCE_NORMALIZATION_MODE);
            assertEquals(e.a, 14.4897, 0.01);
            assertEquals(e.b, 3.9860, 0.01);
            assertEquals(e.phi, 0.73322, 0.01); 
       }catch(Exception e) {
           e.printStackTrace();
           fail();
       } 
        
        
       smm = new SecondMomentMatrix(0.5,0,0,0.5);
       try {
            Ellipse e = new Ellipse(smm, 10, Ellipse.AREA_NORMALIZATION_MODE);
            assertEquals(e.a, 10, 0.01);
            assertEquals(e.b, 10, 0.01);
            assertEquals(e.phi % Math.PI, Math.PI % Math.PI, 0.01); 
       }catch(Exception e) {
           e.printStackTrace();
           fail();
       }
       
       try {
            Ellipse e = new Ellipse(smm, 10, Ellipse.CIRCUMFERENCE_NORMALIZATION_MODE);
            assertEquals(e.a, 10, 0.01);
            assertEquals(e.b, 10, 0.01);
            assertEquals(e.phi % Math.PI, Math.PI % Math.PI, 0.01); 
       }catch(Exception e) {
           e.printStackTrace();
           fail();
       }
       
       smm = new SecondMomentMatrix(0.00341496584490314,0.000821892702383521,0.000821892702383521,0.0154969778287999);
       try {
            Ellipse e = new Ellipse(smm, 18.2391, Ellipse.AREA_NORMALIZATION_MODE);
            assertEquals(e.a, 39.244, 0.01);
            assertEquals(e.b, 8.476, 0.01);
            assertEquals(e.phi, 1.5031, 0.01); 
       }catch(Exception e) {
           e.printStackTrace();
           fail();
       }
    }
    
    
    @Test
    public void testPerformance()
    {
        PerformanceTimer timer = new PerformanceTimer();
        timer.startTimer();
        try {
            for(int i = 0; i < 100; i++) {
            double a = Math.random();
            double b = Math.random();
            double d = Math.random();
            double sigma = Math.random() * 10;
            Ellipse ell = new Ellipse(new SecondMomentMatrix(a,b,b,d), sigma, Ellipse.CIRCUMFERENCE_NORMALIZATION_MODE);
        }
        }catch(Exception e) {
            e.printStackTrace();
        }
        timer.stopTimer();
        
        System.out.println(timer.getSeconds());
    }
}
