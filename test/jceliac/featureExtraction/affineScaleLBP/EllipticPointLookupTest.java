/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBP;

import java.util.Random;
import jceliac.JCeliacGenericException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author shegen
 */
@Ignore("Computation too long for standard testing!")
public class EllipticPointLookupTest
{
    

    /**
     * Test of computeArcLength method, of class EllipticPointLookup.
     */
    @Test
    public void testComputeArcLength()
           throws JCeliacGenericException
    {
        System.out.println("computeArcLength");
        double point1 = 0.0;
        double point2 = 1.0;
        double a = 1.0;
        double b = 1.0;
        EllipticPointLookup instance = EllipticPointLookup.create();
        double expResult = Math.PI/2;
        double result = instance.computeArcLength(point1, point2, a, b);
        assertEquals(expResult, result, 0.05);
        
        point1 = -1.0;
        point2 = 0;
        a = 1.0;
        b = 1.0;
        instance = EllipticPointLookup.create();
        expResult = Math.PI/2;
        result = instance.computeArcLength(point1, point2, a, b);
        assertEquals(expResult, result, 0.05);
        
        point1 = -0.3;
        point2 = 5.6;
        a = 15.78;
        b = 4.99;
        instance = EllipticPointLookup.create();
        expResult = 5.9127;
        result = instance.computeArcLength(point1, point2, a, b);
        assertEquals(expResult, result, 0.05);
        
        point1 = -4.99;
        point2 = 4.99;
        a = 4.99;
        b = 15.78;
        instance =  EllipticPointLookup.create();
        expResult = 34.40573;
        result = instance.computeArcLength(point1, point2, a, b);
        assertEquals(expResult, result, 0.05);
    }

    /**
     * Test of distributePointsOnEllipse method, of class EllipticPointLookup.
     */
    @Test
    public void testDistributePointsOnEllipse()
            throws JCeliacGenericException
    {
        System.out.println("distributePointsOnEllipse");
        double x0 = 0.0;
        double y0 = 0.0;
        double a = 0.67;
        double b = 3.4;
        double phi = 0.0;
        
        EllipticPointLookup instance = EllipticPointLookup.create();
        EllipticPoint[] expResult = new EllipticPoint[8];
        
        expResult[0] = new EllipticPoint(0.0d,3.4d,1);
        expResult[2] = new EllipticPoint(-0.67d,0d,3);
        expResult[4] = new EllipticPoint(0.0d,-3.4d,5);
        expResult[6] = new EllipticPoint(0.67d,0d,7);
        expResult[7] = new EllipticPoint(0.55833d,1.87942d,8);
        expResult[5] = new EllipticPoint(0.55833d,-1.87942d,6);
        expResult[1] = new EllipticPoint(-0.55833d,1.87942d,2);
        expResult[3] = new EllipticPoint(-0.55833d,-1.87942d,4);
        
        EllipticPoint[] result = instance.distributePointsOnEllipse(x0, y0, a, b, 0);
        for(int i = 0; i < expResult.length; i++) {
            assertEquals(expResult[i].x, result[i].x, 0.01);
            assertEquals(expResult[i].y, result[i].y, 0.01);
            assertEquals(expResult[i].position, result[i].position, 0);
        }
        
        x0 = 67.0;
        y0 = 34.0;
        a = 9.76;
        b = 4.34;
        
        instance = EllipticPointLookup.create();
        expResult = new EllipticPoint[8];
        
        expResult[2] = new EllipticPoint(67d,38.34d,3);
        expResult[4] = new EllipticPoint(57.24d,34d,5);
        expResult[6] = new EllipticPoint(67d,29.66d,7);
        expResult[0] = new EllipticPoint(76.76d,34d,1);
        expResult[1] = new EllipticPoint(72.6425d,37.54121d,2);
        expResult[7] = new EllipticPoint(72.6425d,30.45878d,8);
        expResult[3] = new EllipticPoint(61.3575d,37.54121d,4);
        expResult[5] = new EllipticPoint(61.3575d,30.45878d,6);
        
        result = instance.distributePointsOnEllipse(x0, y0, a, b,0);
        for(int i = 0; i < expResult.length; i++) {
            assertEquals(expResult[i].x, result[i].x, 0.01);
            assertEquals(expResult[i].y, result[i].y, 0.01);
            assertEquals(expResult[i].position, result[i].position, 0);
        }
    }

    
    @Test 
    public void testCircleStartPoints()
            throws Exception
    {
        double x0 = 0;
        double y0 = 0;
        double a = 1.02;
        double b = 0.97;
        double phi = 1.2;
        
        EllipticPointLookup instance = EllipticPointLookup.create();
        EllipticPoint[] expResult = new EllipticPoint[8];
        
        expResult[0] = new EllipticPoint(0,0.97,1);
        expResult[2] = new EllipticPoint(-1.02,0,3);
        expResult[4] = new EllipticPoint(0,-0.97,5);
        expResult[6] = new EllipticPoint(1.02,0,7);
        expResult[7] = new EllipticPoint(0.713654479980469, 0.693040480366359,8);
        expResult[5] = new EllipticPoint(0.713654479980469,-0.693040480366359,6);
        expResult[1] = new EllipticPoint(-0.713654479980469,0.693040480366359,2);
        expResult[3] = new EllipticPoint(-0.713654479980469,-0.693040480366359,4);
        
        EllipticPoint[] result = instance.lookupOld(x0, y0, a, b, phi);
        for(int i = 0; i < expResult.length; i++) {
            assertEquals(expResult[i].position, result[i].position, 0);
            assertEquals(expResult[i].x, result[i].x, 0.01);
            assertEquals(expResult[i].y, result[i].y, 0.01);
        }
        
        phi = 2.2;
        result = instance.lookupOld(x0, y0, a, b, phi);
        for(int i = 0; i < expResult.length; i++) {
            assertEquals(expResult[i].position, result[i].position, 0);
            assertEquals(expResult[i].x, result[i].x, 0.01);
            assertEquals(expResult[i].y, result[i].y, 0.01);
        }
        
        phi = 0.23;
        result = instance.lookupOld(x0, y0, a, b, phi);
        for(int i = 0; i < expResult.length; i++) {
            assertEquals(expResult[i].position, result[i].position, 0);
            assertEquals(expResult[i].x, result[i].x, 0.01);
            assertEquals(expResult[i].y, result[i].y, 0.01);
        }
        
        a = 0.85;
        b = 0.83;
        phi = 0.67;
        
        expResult[0] = new EllipticPoint(0, 0.83, 1);
        expResult[2] = new EllipticPoint(-0.85,  0, 3);
        expResult[4] = new EllipticPoint(0, -0.83, 5);
        expResult[6] = new EllipticPoint(0.85,  0, 7);
        expResult[7] = new EllipticPoint(0.59471206665039, 0.593014019282555, 8);
        expResult[5] = new EllipticPoint(0.59471206665039, -0.593014019282555, 6);
        expResult[1] = new EllipticPoint(-0.59471206665039, 0.593014019282555, 2);
        expResult[3] = new EllipticPoint(-0.59471206665039, -0.593014019282555, 4);
        
        result = instance.lookupOld(x0, y0, a, b, phi);
        for(int i = 0; i < expResult.length; i++) {
            assertEquals(expResult[i].position, result[i].position, 0);
            assertEquals(expResult[i].x, result[i].x, 0.01);
            assertEquals(expResult[i].y, result[i].y, 0.01);
        }
    }

    /**
     * Test of lookup method, of class EllipticPointLookup.
     */
    @Test
    public void testLookup()
            throws Exception
    {
        System.out.println("lookup");
        double x0 = 12.0;
        double y0 = 56.0;
        double a = 3.78;
        double b = 2.17;
        double phi = Math.PI/8;
        
        EllipticPointLookup instance = EllipticPointLookup.create();
        EllipticPoint[] expResult = new EllipticPoint[8];
        
        expResult[2] = new EllipticPoint(-0.848829692924974+x0,1.9974955319918d+y0,3);
        expResult[4] = new EllipticPoint(-3.47950834605022d+x0,-1.4753225949498d+y0,5);
        expResult[6] = new EllipticPoint(0.848829692924974d+x0,-1.9974955319918d+y0,7);
        expResult[0] = new EllipticPoint(3.47950834605022d+x0,   1.4753225949498d+y0,1);
        expResult[1] = new EllipticPoint(1.46932070595406d+x0,   2.48242754906611d+y0,2);
        expResult[7] = new EllipticPoint(2.80837119285296d+x0,  -0.668672591499295d+y0,8);
        expResult[3] = new EllipticPoint(-2.80837119285296d+x0,  0.668672591499295d+y0,4);
        expResult[5] = new EllipticPoint(-1.46932070595406d+x0, -2.48242754906611d+y0,6);
        
        
        EllipticPoint[] result = instance.lookupOld(x0, y0, a, b, phi);
        for(int i = 0; i < expResult.length; i++) {
            assertEquals(expResult[i].x, result[i].x, 0.05);
            assertEquals(expResult[i].y, result[i].y, 0.05);
            assertEquals(expResult[i].position, result[i].position, 0);
        }
        
        
        x0 = 0;
        y0 = 0;
        a = 9.54;
        b = 12.34;
        phi = 2.45;
       
        expResult[4] = new EllipticPoint(-7.96388804728518d+x0,-9.45298842808819d+y0,5);
        expResult[6] = new EllipticPoint(7.30806398735505d+x0,-6.10763930971956d+y0,7);
        expResult[0] = new EllipticPoint(7.96388804728518d+x0,9.45298842808819d+y0,1);
        expResult[2] = new EllipticPoint(-7.30806398735505d+x0,   6.10763930971956d+y0,3);
        expResult[3] = new EllipticPoint(-10.7486648010854d+x0,   -1.67183464948351d+y0,4);
        expResult[1] = new EllipticPoint(-0.213431179947194d+x0,  10.8332936140629d+y0,2);
        expResult[5] = new EllipticPoint(0.213431179947194d+x0,  -10.8332936140629d+y0,6);
        expResult[7] = new EllipticPoint(10.7486648010854d+x0, 1.67183464948351d+y0,8);
        
        
        result = instance.lookupOld(x0, y0, a, b, phi);
        for(int i = 0; i < expResult.length; i++) {
            assertEquals(expResult[i].position, result[i].position, 0);
            assertEquals(expResult[i].x, result[i].x, 0.05);
            assertEquals(expResult[i].y, result[i].y, 0.05);
            
        }
        
        // test circles
        x0 = 0;
        y0 = 0;
        a = 1;
        b = 1;
        phi = 0;
       
        expResult[0] = new EllipticPoint(0d+x0,1d+y0,1);
        expResult[2] = new EllipticPoint(-1d+x0,0d+y0,3);
        expResult[4] = new EllipticPoint(0d+x0,-1d+y0,5);
        expResult[6] = new EllipticPoint(1d+x0,   0d+y0,7);
        expResult[7] = new EllipticPoint(0.707080726598634d+x0,   0.70713283481447d+y0,8);
        expResult[5] = new EllipticPoint(0.707080726598634d+x0,  -0.70713283481447d+y0,6);
        expResult[1] = new EllipticPoint(-0.707080726598634d+x0,  0.70713283481447d+y0,2);
        expResult[3] = new EllipticPoint(-0.707080726598634d+x0, -0.70713283481447d+y0,4);
          
        result = instance.lookupOld(x0, y0, a, b, phi);
        for(int i = 0; i < expResult.length; i++) {
            assertEquals(expResult[i].position, result[i].position, 0);
            assertEquals(expResult[i].x, result[i].x, 0.05);
            assertEquals(expResult[i].y, result[i].y, 0.05);
            
        }
        
        x0 = 0;
        y0 = 0;
        a = 1;
        b = 1;
        phi = 1.67;
       
        expResult[0] = new EllipticPoint(0d+x0,1d+y0,1);
        expResult[2] = new EllipticPoint(-1d+x0,0d+y0,3);
        expResult[4] = new EllipticPoint(0d+x0,-1d+y0,5);
        expResult[6] = new EllipticPoint(1d+x0,   0d+y0,7);
        expResult[7] = new EllipticPoint(0.707080726598634d+x0,   0.70713283481447d+y0,8);
        expResult[5] = new EllipticPoint(0.707080726598634d+x0,  -0.70713283481447d+y0,6);
        expResult[1] = new EllipticPoint(-0.707080726598634d+x0,  0.70713283481447d+y0,2);
        expResult[3] = new EllipticPoint(-0.707080726598634d+x0, -0.70713283481447d+y0,4);
          
        result = instance.lookupOld(x0, y0, a, b, phi);
        for(int i = 0; i < expResult.length; i++) {
            assertEquals(expResult[i].position, result[i].position, 0);
            assertEquals(expResult[i].x, result[i].x, 0.05);
            assertEquals(expResult[i].y, result[i].y, 0.05);
            
        }
       
        
        Random rand = new Random(1234567890);
        for(int i = 0; i < 10; i++) {
            a = rand.nextDouble() * 10;
            b = rand.nextDouble() * 10;
            phi = rand.nextDouble() * 2*Math.PI;
            x0 = rand.nextInt() % 256;
            y0 = rand.nextInt() % 256;
            
            result = instance.lookupOld(x0, y0, a, b, phi);
            expResult = instance.distributePointsOnEllipse(x0, y0, a, b, phi);
            for(int j = 0; i < expResult.length; i++) 
            {
                assertEquals(expResult[j].x, result[j].x, 0.05);
                assertEquals(expResult[j].y, result[j].y, 0.05);
                assertEquals(expResult[j].position, result[j].position, 0);
            }
        }
        
        
    }



    /**
     * Test of computeLookupTable method, of class EllipticPointLookup.
     */
    @Test
    public void testComputeLookupTable() throws Exception
    {
        System.out.println("computeLookupTable");
        double ratio = 1.74;
        EllipticPointLookup instance = EllipticPointLookup.create();

        // index 24 for ratio 1.74 in matlab
        EllipticPoint [] expPoints = new EllipticPoint[8];
        expPoints[2] = new EllipticPoint(-0.390731128489274d, 0.92050485345244d, 3);
        expPoints[4] = new EllipticPoint(-1.60167844500725d, -0.679872163571336d, 5);
        expPoints[6] = new EllipticPoint(0.390731128489274d, -0.92050485345244d, 7);
        expPoints[0] = new EllipticPoint(1.60167844500725d, 0.679872163571336d, 1);
        expPoints[1] = new EllipticPoint(0.676353975756632d, 1.14397582906272d, 2);
        expPoints[7] = new EllipticPoint(1.29274229512279d, -0.308144051382163d, 8);
        expPoints[3] = new EllipticPoint(-1.29274229512279d, 0.308144051382163d, 4);
        expPoints[5] = new EllipticPoint(-0.676353975756632d, -1.14397582906272d, 6);

        EllipticLookupTableEntry result = instance.computeLookupTableEntry(ratio);
        
        EllipticPoint[] resultPoints = result.pointsByAngle[23];
        for(int i = 0; i < expPoints.length; i++) {
            assertEquals(expPoints[i].x, resultPoints[i].x, 0.05);
            assertEquals(expPoints[i].y, resultPoints[i].y, 0.05);
            assertEquals(expPoints[i].position, resultPoints[i].position, 0);
        }
       
        result = instance.computeLookupTableEntry(1.0d);
        int x0 = 0;
        int y0 = 0;
        expPoints[0] = new EllipticPoint(0d+x0,1d+y0,1);
        expPoints[2] = new EllipticPoint(-1d+x0,0d+y0,3);
        expPoints[4] = new EllipticPoint(0d+x0,-1d+y0,5);
        expPoints[6] = new EllipticPoint(1d+x0,   0d+y0,7);
        expPoints[7] = new EllipticPoint(0.707080726598634d+x0,   0.70713283481447d+y0,8);
        expPoints[5] = new EllipticPoint(0.707080726598634d+x0,  -0.70713283481447d+y0,6);
        expPoints[1] = new EllipticPoint(-0.707080726598634d+x0,  0.70713283481447d+y0,2);
        expPoints[3] = new EllipticPoint(-0.707080726598634d+x0, -0.70713283481447d+y0,4);
       
        resultPoints = result.pointsByAngle[0];
        for(int i = 0; i < expPoints.length; i++) {
            assertEquals(expPoints[i].x, resultPoints[i].x, 0.05);
            assertEquals(expPoints[i].y, resultPoints[i].y, 0.05);
            assertEquals(expPoints[i].position, resultPoints[i].position, 0);
        }
    }



}
