/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jceliac.featureExtraction.affineScaleLBP;

import data.MATParser;
import data.MatlabArray;
import data.MatlabVariable;
import data.MatlabWorkspace;
import org.junit.Test;
import java.lang.reflect.*;
import java.util.Arrays;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.tools.filter.scalespace.SecondMomentMatrix;
import jceliac.featureExtraction.affineScaleLBPScaleBias.FeatureExtractionParametersAffineScaleLBP;

/**
 *
 * @author shegen
 */
public class FeatureExtractionMethodAffineScaleLBPNonUniformGaussianTest 
{
    
    @Test
    public void testComputeTransformedSamplingPointCoordinates()
            throws Exception
    {
        
        // expected result
   /*     MATParser parser = new MATParser("/home/stud3/shegen/JCeliac/testdata/jceliac.featureExtraction.affineScaleLBP/ellipticPointResults.mat");
        parser.parseMATFile();
        MatlabWorkspace ws = parser.getLoadedWorkspace();
        MatlabArray expectedResult1 = ws.getVariable("ellipticPoints1").getData();
        MatlabArray expectedResult2 = ws.getVariable("ellipticPoints2").getData();
        
        
        Method [] methods = FeatureExtractionMethodAffineScaleLBPNonUniformGaussian.class.getDeclaredMethods();
        // find the method
        Method testMethod = Arrays.asList(methods).stream()
                                      .filter((a) -> a.getName()
                                      .compareTo("computeTransformedSamplingPointCoordinates") == 0)
                                      .findFirst().get();
        FeatureExtractionParametersAffineScaleLBP param = new FeatureExtractionParametersAffineScaleLBP();
        FeatureExtractionMethodAffineScaleLBPNonUniformGaussian g = 
                    new FeatureExtractionMethodAffineScaleLBPNonUniformGaussian(FeatureExtractionMethod.EFeatureExtractionMethod.AFFINESCALELBP,
                                                                                param);
        
        // computeTransformedSamplingPointCoordinates(int x0, int y0, SecondMomentMatrix smm, double sigma);
        SecondMomentMatrix smm = new SecondMomentMatrix(10, 0.5, 0.5, 1);
        testMethod.setAccessible(true);
        EllipticPoint [] points = (EllipticPoint [])testMethod.invoke(g, 64,64,smm, 10);
        
        for(int i = 0; i < 8; i++) 
        {
            assert(Math.abs((double)expectedResult1.getElementSubIndex(i+1,1) - points[i].x) < 0.001);
            assert(Math.abs((double)expectedResult1.getElementSubIndex(i+1,2) - points[i].y) < 0.001);
        }
        
        smm = new SecondMomentMatrix(1.6781, 0.1234, 0.1234, 9.2455);
        testMethod.setAccessible(true);
        points = (EllipticPoint [])testMethod.invoke(g, 21,89,smm, 2.41);
        
        for(int i = 0; i < 8; i++) 
        {
            assert(Math.abs((double)expectedResult2.getElementSubIndex(i+1,1) - points[i].x) < 0.001);
            assert(Math.abs((double)expectedResult2.getElementSubIndex(i+1,2) - points[i].y) < 0.001);
        }

*/
        
                
    }
}
