/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBP;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class EllipticPointTest
{
    
    public EllipticPointTest()
    {
    }

    /**
     * Test of rotatePoint method, of class EllipticPoint.
     */
    @Test
    public void testRotatePoint()
    {
        System.out.println("rotatePoint");
        double x0 = 10.0;
        double y0 = 10.0;
        double phi = Math.PI / 8;
        EllipticPoint instance = new EllipticPoint(0,1);
        EllipticPoint [] array = new EllipticPoint[1];
        array[0] = instance;
                
        EllipticPoint [] rotatedPoints = EllipticPoint.rotatePoints(array, x0, y0, phi);
        
        double expectedX = 4.20535556617294;
        double expectedY = -2.14175011625248;
        
        assertEquals(expectedX, rotatedPoints[0].x, 0.05);
        assertEquals(expectedY, rotatedPoints[0].y, 0.05);
    }
    
    // check if multiple rotates on a set of points change the points values
    @Test
    public void testMultipleRotates()
    {
        double x0 = 10.0;
        double y0 = 10.0;
        double phi = Math.PI / 8;
        EllipticPoint instance = new EllipticPoint(0,1);
        EllipticPoint [] array = new EllipticPoint[1];
        array[0] = instance;

        for(int i = 0 ; i < 10; i++) 
        {
            EllipticPoint [] rotatedPoints = EllipticPoint.rotatePoints(array, x0, y0, phi);
            double expectedX = 4.20535556617294;
            double expectedY = -2.14175011625248;
        
            assertEquals(expectedX, rotatedPoints[0].x, 0.05);
            assertEquals(expectedY, rotatedPoints[0].y, 0.05);
        }  
    }
}
