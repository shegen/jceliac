/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.fractal.multiFractalSpectrum;


import jceliac.featureExtraction.DynamicFeatureVector;
import jceliac.featureExtraction.DiscriminativeFeatures;
import java.awt.image.BufferedImage;
import static org.junit.Assert.*;
import jceliac.tools.data.Frame;
import javax.imageio.ImageIO;
import org.junit.Test;
import java.io.File;
import java.io.IOException;
import jceliac.JCeliacGenericException;
import jceliac.tools.export.MatrixImporter;

/**
 *
 * @author shegen
 */
public class FeatureExtractionMethodMultiFractalSpectrumTest
{

    public FeatureExtractionMethodMultiFractalSpectrumTest()
    {
    }

    /**
     * Test of initialize method, of class
     * FeatureExtractionMethodMultiFractalSpectrum.
     * <p>
     * @throws java.lang.Exception
     */
    @Test
    public void testExtractFeatures()
            throws Exception
    {
        try {
            BufferedImage image = ImageIO.read(new File("testdata/images/44-scale_3_im_3_col.png"));
            System.out.println("FeatureExtractionMethodMultiFractalSpectrum: test extractFeatures");
            double [] expectedFeatureVector = MatrixImporter.importDoubleMatrix1D(new File("testdata/jceliac.featureExtraction.multiFractalSpectrum/result1.csv"));
            
            /* Default parameters used by matlab are not the best I found during experiments, this might
             * be caused by a bug I fixed in the matlab code computing the gaussian filters. That 
             * bug was fixed in matlab and is not present int the java code. 
             */ 
            FeatureExtractionParametersMultiFractalSpectrum params = new FeatureExtractionParametersMultiFractalSpectrum();
            params.sigma = 0.5d;
            params.steps = 26;
            params.stepmax = 2.42;
            FeatureExtractionMethodMultiFractalSpectrum instance = new FeatureExtractionMethodMultiFractalSpectrum(params);
            instance.initialize();

            DiscriminativeFeatures mfsFeatures = instance.extractFeatures(Frame.convertBufferedImageToFrameOptimized(image));
            double[] featureData = mfsFeatures.getAllFeatureVectorData();

            assertArrayEquals(expectedFeatureVector, featureData, 0.001);
        } catch(IOException  e) {
            System.out.println(e.getMessage());
        } catch(JCeliacGenericException e) {
            fail();
        }
    }

}
