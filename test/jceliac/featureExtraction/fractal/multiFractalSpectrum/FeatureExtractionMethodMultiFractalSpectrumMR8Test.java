/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.fractal.multiFractalSpectrum;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import jceliac.JCeliacGenericException;
import jceliac.tools.data.Frame;
import jceliac.tools.export.MatrixImporter;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author shegen
 */
public class FeatureExtractionMethodMultiFractalSpectrumMR8Test
{
    
   
    /**
     * Test of computeDiscriminativeFeatures method, of class FeatureExtractionMethodMultiFractalSpectrumMR8.
     */
    @Test
    
    public void testExtractFeatures() 
            throws Exception
    {
         try {
            BufferedImage image = ImageIO.read(new File("testdata/images/44-scale_3_im_3_col.png"));
            System.out.println("FeatureExtractionMethodMultiFractalSpectrum: test extractFeatures");
            double [][] expectedFeatureVector = MatrixImporter.importDoubleMatrix(new File("testdata/jceliac.featureExtraction.multiFractalSpectrum/result_mr8_1.csv"));
            
            /* Default parameters used by matlab are not the best I found during experiments, this might
             * be caused by a bug I fixed in the matlab code computing the gaussian filters. That 
             * bug was fixed in matlab and is not present int the java code. 
             */ 
            FeatureExtractionParametersMultiFractalSpectrumMR8 params = new FeatureExtractionParametersMultiFractalSpectrumMR8();
            params.sigma = 0.5d;
            params.steps = 26;
            params.stepmax = 2.42;
            FeatureExtractionMethodMultiFractalSpectrumMR8 instance = new FeatureExtractionMethodMultiFractalSpectrumMR8(params);
            instance.initialize();

            MultiFractalSpectrumMR8Features mfsFeatures = (MultiFractalSpectrumMR8Features)instance.extractFeatures(Frame.convertBufferedImageToFrameOptimized(image));
            // method not done yet
            ArrayList <double []> localFeatures = mfsFeatures.getLocalFeatureVectorData();
            for(int j = 0 ; j < expectedFeatureVector[0].length; j++) {
                double [] vecData = localFeatures.get(j);
                for(int i = 0; i < expectedFeatureVector.length; i++) {
                    assertEquals(vecData[i], expectedFeatureVector[i][j], 1e-10);
                }
            }
        } catch(IOException  e) {
            System.out.println(e.getMessage());
        } catch(JCeliacGenericException e) {
            fail();
        }
    }

    
    
}
