/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.fractal.multiFractalSpectrum;

import org.junit.Test;

/**
 *
 * @author shegen
 */
public class BoxCountTest
{

    public BoxCountTest()
    {
    }

    /**
     * Test of count method, of class BoxCount.
     */
    @Test
    public void testCount1D()
    {
        System.out.println("count 1D");
        double[] data1D = new double[100];
        for(int i = 0; i < 100; i++) {
            data1D[i] = i + 1;
        }
        BoxCountResult result = BoxCount.count(data1D);
        assert (result.n[0] == 100);
        assert (result.n[1] == 50);
        assert (result.n[2] == 25);
        assert (result.n[3] == 13);
        assert (result.n[4] == 7);
        assert (result.n[5] == 4);
        assert (result.n[6] == 2);
        assert (result.n[7] == 1);

        assert (result.r[0] == 1);
        assert (result.r[1] == 2);
        assert (result.r[2] == 4);
        assert (result.r[3] == 8);
        assert (result.r[4] == 16);
        assert (result.r[5] == 32);
        assert (result.r[6] == 64);
        assert (result.r[7] == 128);

        for(int i = 0; i < 100; i++) {
            data1D[i] = i * 2;
        }
        result = BoxCount.count(data1D);
        assert (result.n[0] == 99);
        assert (result.n[1] == 50);
        assert (result.n[2] == 25);
        assert (result.n[3] == 13);
        assert (result.n[4] == 7);
        assert (result.n[5] == 4);
        assert (result.n[6] == 2);
        assert (result.n[7] == 1);

        assert (result.r[0] == 1);
        assert (result.r[1] == 2);
        assert (result.r[2] == 4);
        assert (result.r[3] == 8);
        assert (result.r[4] == 16);
        assert (result.r[5] == 32);
        assert (result.r[6] == 64);
        assert (result.r[7] == 128);

        double[] data1DRand = {0.8147, 0.9058, 0.1270, 0.9134, 
                               0.6324, 0.0975, 0.2785, 0.5469, 
                               0.9575, 0.9649};
        
        result = BoxCount.count(data1DRand);
        assert (result.n[0] == 10);
        assert (result.n[1] == 5);
        assert (result.n[2] == 3);
        assert (result.n[3] == 2);
        assert (result.n[4] == 1);

        assert (result.r[0] == 1);
        assert (result.r[1] == 2);
        assert (result.r[2] == 4);
        assert (result.r[3] == 8);
        assert (result.r[4] == 16);
    }
    
    
   /**
     * Test of count method, of class BoxCount.
     */
    @Test
    public void testCount2D()
    {
        System.out.println("count 2D");
        double[][] data2D = new double[100][100];
        for(int i = 0; i < 100; i++) {
            for(int j =0 ; j < 100; j++) {
                data2D[i][j] = j +1;
            }
        }
        BoxCountResult result = BoxCount.count(data2D);
        assert (result.n[0] == 10000);
        assert (result.n[1] == 2500);
        assert (result.n[2] == 625);
        assert (result.n[3] == 169);
        assert (result.n[4] == 49);
        assert (result.n[5] == 16);
        assert (result.n[6] == 4);
        assert (result.n[7] == 1);

        assert (result.r[0] == 1);
        assert (result.r[1] == 2);
        assert (result.r[2] == 4);
        assert (result.r[3] == 8);
        assert (result.r[4] == 16);
        assert (result.r[5] == 32);
        assert (result.r[6] == 64);
        assert (result.r[7] == 128);
        
        double[][] data2DRand = {{0.8147, 0.1576, 0.6557, 0.7060, 0.4387, 0.2760, 0.7513, 0.8407, 0.3517, 0.0759},
                                 {0.9058, 0.9706, 0.0357, 0.0318, 0.3816, 0.6797, 0.2551, 0.2543, 0.8308, 0.0540},
                                 {0.1270, 0.9572, 0.8491, 0.2769, 0.7655, 0.6551, 0.5060, 0.8143, 0.5853, 0.5308},
                                 {0.9134, 0.4854, 0.9340, 0.0462, 0.7952, 0.1626, 0.6991, 0.2435, 0.5497, 0.7792},
                                 {0.6324, 0.8003, 0.6787, 0.0971, 0.1869, 0.1190, 0.8909, 0.9293, 0.9172, 0.9340},
                                 {0.0975, 0.1419, 0.7577, 0.8235, 0.4898, 0.4984, 0.9593, 0.3500, 0.2858, 0.1299},
                                 {0.2785, 0.4218, 0.7431, 0.6948, 0.4456, 0.9597, 0.5472, 0.1966, 0.7572, 0.5688},
                                 {0.5469, 0.9157, 0.3922, 0.3171, 0.6463, 0.3404, 0.1386, 0.2511, 0.7537, 0.4694},
                                 {0.9575, 0.7922, 0.6555, 0.9502, 0.7094, 0.5853, 0.1493, 0.6160, 0.3804, 0.0119},
                                 {0.9649, 0.9595, 0.1712, 0.0344, 0.7547, 0.2238, 0.2575, 0.4733, 0.5678, 0.3371}
        };
        result = BoxCount.count(data2DRand);
        assert (result.n[0] == 100);
        assert (result.n[1] == 25);
        assert (result.n[2] == 9);
        assert (result.n[3] == 4);
        assert (result.n[4] == 1);

        assert (result.r[0] == 1);
        assert (result.r[1] == 2);
        assert (result.r[2] == 4);
        assert (result.r[3] == 8);
        assert (result.r[4] == 16);

    }

}
