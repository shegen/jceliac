/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac;

/**
 * Holds all command line parameter variables supported by the application.
 * <p>
 * @author shegen
 */
public class CommandLineParameters
{
    public String featureExtractionParamString = null;
    public String optimizationMethodParamString = null;
    public String classificationParamString = null;
    public String configXMLFile = null;
    public String featureExtractionMethod = null;
    public String classificationMethod = null;
    public String optimizationMethod = null;
    public String generativeModel = null;
    public String validationMethod = null;
    public String experimentName = null;
    public String trainingDataBaseDirectory = null;
    public String testDataBaseDirectory = null;
    public String featureSubset = null;
    public String exportDistanceMatrixFile = null;
    public boolean interactive = false;
    public boolean offline = true;

    // indicate that a class used in classification is not within the training data
    // we use this to evaluate how patches from the ventriculus or esophagus are classified
    public boolean allowForeignEvaluationClasses = false;

    public String SSHPassword = null;

}
