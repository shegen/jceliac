/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.timer;

/**
 * Used to monitor the performance of methods. 
 * 
 * @author shegen
 */
public class PerformanceTimer
{

    private long timeStart = 0;
    private long timeStop = 0;

    public void startTimer()
    {
        timeStart = System.nanoTime();
    }

    public void stopTimer()
    {
        timeStop = System.nanoTime();
    }

    public double getSeconds()
    {
        double nanoDifference = (double) (timeStop - timeStart);

        return (double) (nanoDifference / 1000000000.0d);
    }

    public long getNanos()
    {
        return (this.timeStop - this.timeStart);
    }
}
