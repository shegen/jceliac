/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.distortionsim;

import jceliac.JCeliacGenericException;
import jceliac.tools.data.Frame;
import java.util.Random;

/**
 * Simulates Gaussian white noise.
 * <p>
 * @author shegen
 */
public class GaussianWhiteNoiseSimulator
{

    private final Frame frameData;
    private final Random random;
    private final static long SEED = 1238795723;

    public static final int CHANNEL_RED = 0;
    public static final int CHANNEL_GREEN = 1;
    public static final int CHANNEL_BLUE = 2;
    public static final int CHANNEL_GRAY = 3;

    public GaussianWhiteNoiseSimulator(Frame frameData)
    {
        this.frameData = frameData;
        this.random = new Random(SEED);
    }

    /**
     * Returns a distorted color channel.
     * <p>
     * @param sigma        Standard deviation of the gaussian.
     * @param mu           Mean of the gaussian.
     * @param colorChannel Color channel to distort.
     * <p>
     * @return Distorted color channel of frame provided in the constructor.
     * <p>
     * @throws JCeliacGenericException
     */
    public double[][] getDistortedChannel(double sigma, double mu, int colorChannel)
            throws JCeliacGenericException
    {
        switch(colorChannel) {
            case CHANNEL_RED:
                return this.distort(sigma, mu, this.frameData.getRedData());
            case CHANNEL_BLUE:
                return this.distort(sigma, mu, this.frameData.getBlueData());
            case CHANNEL_GREEN:
                return this.distort(sigma, mu, this.frameData.getGreenData());
            case CHANNEL_GRAY:
                return this.distort(sigma, mu, this.frameData.getGrayData());
        }
        throw new JCeliacGenericException("Unknown Color Channel!");
    }

    /**
     * Creates a copy with added gaussian white noise of the supplied data.
     * <p>
     * @param sigma Standard deviation of the gaussian.
     * @param mu    Mean of the gaussian.
     * @param data  The data to distort.
     * <p>
     * @return A copy of data with added distortion.
     */
    private double[][] distort(double sigma, double mu, double[][] data)
    {
        double[][] noisyData = new double[data.length][data[0].length];

        for(int i = 0; i < data.length; i++) {
            for(int j = 0; j < data[0].length; j++) {
                // returns a gaussian with mu = 0, and sigma = 1.0
                double gaussianValue = this.random.nextGaussian();

                double noisyValue = data[i][j] + (mu + sigma * gaussianValue);
                if(noisyValue < 0) {
                    noisyValue = 0;
                }
                if(noisyValue > 255) {
                    noisyValue = 255;
                }
                noisyData[i][j] = noisyValue;
            }
        }
        return noisyData;
    }

}
