/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.cpu;

import java.lang.management.OperatingSystemMXBean;
import java.lang.management.ManagementFactory;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * Used to query CPU information.
 * <p>
 * @author shegen
 */
public class CPUInfo
{

    /**
     * Computes the system cpu load.
     * <p>
     * @return The system cpu load as double between 0 and 1.
     */
    public static double getSystemCPULoad()
    {
        OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
        for(Method method : operatingSystemMXBean.getClass().getDeclaredMethods()) {
            method.setAccessible(true);
            if(method.getName().startsWith("getSystemCpuLoad")
               && Modifier.isPublic(method.getModifiers())) {
                Object value;
                try {
                    value = method.invoke(operatingSystemMXBean);
                } catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    value = e;
                }
                return (double) value;
            }
        }
        return 0;
    }

    /**
     * Queries the number of cpus.
     * <p>
     * @return Number of cores.
     */
    public static int getNumberOfCPUs()
    {
        OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
        return operatingSystemMXBean.getAvailableProcessors();
    }

}
