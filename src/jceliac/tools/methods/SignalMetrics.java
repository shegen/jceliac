/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.methods;

/**
 * Provides metrics for signals.
 * <p>
 * @author shegen
 */
public class SignalMetrics
{

    private static final double MAX_VALUE = 255.0d;

    /**
     * Calculates the Peak Signal to Noise Ratio (PSNR) between two signals.
     * <p>
     * @param d1 Signal one.
     * @param d2 Signal two.
     * <p>
     * @return PSNR between one and two.
     */
    public static double computePSNR(double[][] d1, double[][] d2)
    {
        double psnr = 20 * Math.log10(SignalMetrics.MAX_VALUE)
                      - 10 * Math.log10(SignalMetrics.computeMSE(d1, d2));
        return psnr;
    }

    /**
     * Calculates the Mean Squared Error between to signals.
     * <p>
     * @param d1 Signal one.
     * @param d2 Signal two.
     * <p>
     * @return MSE between one and two.
     */
    public static double computeMSE(double[][] d1, double[][] d2)
    {
        double sum = 0;

        for(int i = 0; i < d1.length; i++) {
            for(int j = 0; j < d1[0].length; j++) {
                double diff = d1[i][j] - d2[i][j];
                sum += (diff * diff);
            }
        }
        return sum / (d1.length * d1[0].length);
    }
}
