/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.methods;

import jceliac.tools.filter.DataFilter2D;
import jceliac.JCeliacGenericException;
import jceliac.tools.filter.*;
import jceliac.tools.data.*;
import java.util.*;

/**
 * Computes a measure for blur following Haan et al.
 * <p>
 * @author shegen
 */
public class Haanblur
{

    private final double sigmaA = 1.8;
    private final double sigmaB = 4.0;

    private class Region
    {

        public double[][] data;
        public int x;
        public int y;
    }

    private class SigmaRegion
    {

        public double sigma;
        public int x;
        public int y;
    }

    /**
     * Computes the blur measure for a frame.
     * <p>
     * @param frame      Frame to compute measure.
     * @param regionsize Dimension of regions.
     * <p>
     * @return Visualized blur measure.
     * <p>
     * @throws JCeliacGenericException
     */
    public Frame compute(Frame frame, int regionsize)
            throws JCeliacGenericException
    {

        LowpassFilter gaussianFilterA = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 7, sigmaA);
        LowpassFilter gaussianFilterB = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 7, sigmaB);

        Frame noiseSuppressed = frame;
        double[][] dataA = gaussianFilterA.filterData(noiseSuppressed.getGrayData());
        double[][] dataB = gaussianFilterB.filterData(noiseSuppressed.getGrayData());
        double[][] dataDiff = difference(dataA, dataB);

        ArrayList<Region> regionsOrig = extractRegions(noiseSuppressed.getGrayData(), regionsize);
        ArrayList<Region> regionsA = extractRegions(dataA, regionsize);
        ArrayList<Region> regionsDiff = extractRegions(dataDiff, regionsize);
        ArrayList<SigmaRegion> sigmas = new ArrayList<>();

        for(int k = 0; k < regionsOrig.size(); k++) {
            Region orig = regionsOrig.get(k);
            Region a = regionsA.get(k);
            Region diff = regionsDiff.get(k);

            double sigma = computeBlur(orig.data, a.data, diff.data);
            SigmaRegion sr = new SigmaRegion();
            sr.sigma = sigma;
            sr.x = orig.x;
            sr.y = orig.y;
            sigmas.add(sr);
        }
        double[][] vis = visualize(sigmas, regionsize, frame.getWidth(), frame.getHeight());
        Frame f = new Frame();
        f.setGrayData(vis);
        return f;
    }

    /**
     * Computes blur measure.
     * <p>
     * @param orig Original image.
     * @param a    Filtered image.
     * @param diff Difference image.
     * <p>
     * @return Blur measure.
     */
    private double computeBlur(double[][] orig, double[][] a, double[][] diff)
    {
        double max = 0;

        for(int i = 0; i < orig.length; i++) {
            for(int j = 0; j < orig[0].length; j++) {
                if(diff[i][j] == 0) {
                    continue;
                }
                double r = (orig[i][j] - a[i][j]) / diff[i][j];
                if(r > max) {
                    max = r;
                }
            }
        }
        double sigma = (this.sigmaA * this.sigmaB) / ((this.sigmaB - this.sigmaA) * max + this.sigmaB);
        return sigma;

    }

    /**
     * Computes the difference between two double arrays, counts only
     * differences geq 1.
     * <p>
     * @param a Array one.
     * @param b Array two.
     * <p>
     * @return Sum of differences.
     * <p>
     * @throws JCeliacGenericException
     */
    private double[][] difference(double[][] a, double[][] b)
            throws JCeliacGenericException
    {
        if(a.length != b.length || a[0].length != b[0].length) {
            throw new JCeliacGenericException("Array Dimensions mismatch");
        }
        double[][] difference = new double[a.length][a[0].length];
        for(int i = 0; i < a.length; i++) {
            for(int j = 0; j < a[0].length; j++) {
                difference[i][j] = a[i][j] - b[i][j];
                if(difference[i][j] < 1) {
                    difference[i][j] = 0;
                }
            }
        }
        return difference;
    }

    /**
     * Extracts multiple regions.
     * <p>
     * @param data       Raw image data.
     * @param regionsize Size of regions.
     * <p>
     * @return List of regions.
     */
    private ArrayList<Region> extractRegions(double[][] data, int regionsize)
    {
        ArrayList<Region> regions = new ArrayList<>();

        // ignore the rest of the image if its not divisible by regionsize
        int regionsX = data.length / regionsize;
        int regionsY = data[0].length / regionsize;

        for(int i = 0; i < regionsX; i++) {
            for(int j = 0; j < regionsY; j++) {
                Region cur = extract(data, i, j, regionsize);
                regions.add(cur);
            }
        }
        return regions;
    }

    /**
     * Extracts a region from an image.
     * <p>
     * @param data       Image data.
     * @param x          Upper left coordinate or region, x.
     * @param y          Upper left coordinate or region, y.
     * @param regionsize Dimensions of regions.
     * <p>
     * @return Extracted region.
     */
    private Region extract(double[][] data, int x, int y, int regionsize)
    {
        double[][] region = new double[regionsize][regionsize];

        for(int i = 0; i < regionsize; i++) {
            for(int j = 0; j < regionsize; j++) {
                region[i][j] = data[x * regionsize + i][y * regionsize + j];
            }
        }
        Region reg = new Region();
        reg.data = region;
        reg.x = x * regionsize;
        reg.y = y * regionsize;
        return reg;
    }

    /**
     * Visualizes regions.
     * <p>
     * @param regions    Regions.
     * @param regionsize Dimensions of regions.
     * @param width      Width of visualization.
     * @param height     Height of visualization.
     * <p>
     * @return Visualized image.
     */
    private double[][] visualize(ArrayList<SigmaRegion> regions, int regionsize, int width, int height)
    {
        double[][] vis = new double[width][height];
        for(SigmaRegion cur : regions) {
            for(int i = 0; i < regionsize; i++) {
                for(int j = 0; j < regionsize; j++) {
                    vis[cur.x + i][cur.y + j] = cur.sigma;
                }
            }
        }
        DataFilter2D.scaleData(vis);
        return vis;
    }
}
