/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter;

import jceliac.tools.transforms.fft.FastFourierTransform2D;
import jceliac.tools.transforms.fft.FastFourierTransform;
import jceliac.JCeliacGenericException;
import jceliac.experiment.*;
import jceliac.logging.*;
import jceliac.tools.transforms.dtcwt.ComplexArray1D;

/**
 * Implements a convolution of two signals using the Fast Fourier Transform.
 * <p>
 * @author shegen
 */
public class ConvolutionFFT
{

    public ConvolutionFFT()
    {
    }

    /**
     * This is the circular convolution, this is usually not wanted in digital
     * signal processing.
     * <p>
     * @param signal Signal to filter.
     * @param filter Filter.
     * <p>
     * @return Returns signal convolved with filter in a circular convolution.
     * <p>
     * @throws jceliac.JCeliacGenericException <p>
     */
    public double[][] convolveCircularFFT(double[][] signal, double[][] filter)
            throws JCeliacGenericException
    {
        if(signal.length != signal[0].length) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Only squared signals are supported in FFT2D.");
            throw new JCeliacGenericException("Only squared signals are supported in FFT2D");
        }
        double ld2 = Math.log(signal.length) / Math.log(2);
        if(ld2 != (int) ld2) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Only signals with a length of a power of 2 are supported in FFT2D.");
            throw new JCeliacGenericException("Only Signals with a length of a Power of 2 are Supported in FFT2D");
        }
        FastFourierTransform2D fft2D = new FastFourierTransform2D(signal.length);
        double[][][] transformedData = fft2D.transform(fft2D.makeComplex(signal));
        double[][][] transformedFilter = fft2D.transform(fft2D.makeComplex(filter));

        // multiply point wise
        for(int i = 0; i < transformedData.length; i++) {
            for(int j = 0; j < transformedData[0].length; j++) {
                double real, image;
                real = (transformedData[i][j][0] * transformedFilter[i][j][0])
                       - (transformedData[i][j][1] * transformedFilter[i][j][1]);
                image = transformedData[i][j][1] * transformedFilter[i][j][0]
                        + transformedData[i][j][0] * transformedFilter[i][j][1];
                transformedData[i][j][0] = real;
                transformedData[i][j][1] = image;
            }
        }
        return fft2D.transformInverse(transformedData);
    }

    /**
     * Complex multiplication of two arrays.
     * <p>
     * @param c1 Array one, dimension 0 is real part, dimension 1 is complex
     *           part.
     * @param c2 Array two, dimension 0 is real part, dimension 1 is complex
     *           part.
     * <p>
     * @return Result of complex multiplication.
     */
    private double[][] multiplyComplex(double[][] c1, double[][] c2)
    {
        double[][] result = new double[c1.length][c1[0].length];

        // multiply point wise
        for(int i = 0; i < c1.length; i++) {
            double real, image;
            real = (c1[i][0] * c2[i][0]) - (c1[i][1] * c2[i][1]);
            image = c1[i][1] * c2[i][0] + c1[i][0] * c2[i][1];
            result[i][0] = real;
            result[i][1] = image;
        }
        return result;
    }

    /**
     * Complex multiplication of two arrays, full optimized.
     * <p>
     * @param c1Real  Array 1 real part.
     * @param c1Image Array 1 imaginary part.
     * @param c2Real  Array 2 real part.
     * @param c2Image Array 2 imaginary part.
     * <p>
     * @return Result of complex multiplication Object[0] = double array real
     *         part, Object[1] = double array imaginary part.
     */
    private Object[] multiplyComplexFullOptimized(double[] c1Real, double[] c1Image, double[] c2Real, double[] c2Image)
    {
        double[] resultReal = new double[c1Real.length];
        double[] resultImage = new double[c1Real.length];

        // multiply point wise
        for(int i = 0; i < c1Real.length; i++) {
            double real, image;
            real = (c1Real[i] * c2Real[i]) - (c1Image[i] * c2Image[i]);
            image = c1Image[i] * c2Real[i] + c1Real[i] * c2Image[i];
            resultReal[i] = real;
            resultImage[i] = image;
        }
        Object[] ret = new Object[2];
        ret[0] = resultReal;
        ret[1] = resultImage;
        return ret;
    }

    /**
     * Adds to arrays element wise, stores results in dst array.
     * <p>
     * @param src    Array one.
     * @param srcPos Start index of array one.
     * @param dst    Destination array.
     * @param dstPos Start index of destination array.
     * @param len    Number of elements to add.
     * <p>
     */
    private void addArrays(double[] src, int srcPos, double[] dst, int dstPos, int len)
    {
        for(int count = 0; count < len; count++) {
            dst[dstPos + count] += src[srcPos + count];
        }
    }

    /**
     * The linear Convolution is achieved by using the overlap add method. I
     * wrote this code originally in matlab, you can find it in Blob-LBP
     * overlapadd2.m.
     * <p>
     * @param signal Signal to convolve with filter.
     * @param filter The filter to convolve with signal.
     * <p>
     * @return Returns signal convolved with filter.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public double[][] convolveLinearFFT(double[][] signal, double[] filter)
            throws JCeliacGenericException
    {
        double[][] convolvedData = new double[signal.length][];
        for(int i = 0; i < signal.length; i++) {
            convolvedData[i] = overlapadd(signal[i], filter);
        }
        for(int j = 0; j < signal[0].length; j++) {
            // extract row
            double[] row = new double[signal.length];
            for(int cIndex = 0; cIndex < signal.length; cIndex++) {
                row[cIndex] = convolvedData[cIndex][j];
            }
            double[] tmp = overlapadd(row, filter);
            // put the signal into the convolved dataq
            for(int cIndex = 0; cIndex < signal.length; cIndex++) {
                convolvedData[cIndex][j] = tmp[cIndex];
            }
        }
        return convolvedData;
    }

    /**
     * The linear Convolution in 2D.
     * <p>
     * @param signal           Signal to convolve with filters.
     * @param filterHorizontal Horizontal filter.
     * @param filterVertical   Vertical filter.
     * <p>
     * @return Returns signal convolved with both filters in a 2D convolution.
     */
    public double[][] convolveLinearFFT(double[][] signal, double[] filterHorizontal,
                                        double[] filterVertical)
    {
        double[][] convolvedData = new double[signal.length][];
        for(int i = 0; i < signal.length; i++) {
            convolvedData[i] = this.overlapadd(signal[i], filterHorizontal);
        }
        for(int j = 0; j < signal[0].length; j++) {
            // extract row
            double[] row = new double[signal.length];
            for(int cIndex = 0; cIndex < signal.length; cIndex++) {
                row[cIndex] = convolvedData[cIndex][j];
            }
            double[] tmp = this.overlapadd(row, filterVertical);
            // put the signal into the convolved dataq
            for(int cIndex = 0; cIndex < signal.length; cIndex++) {
                convolvedData[cIndex][j] = tmp[cIndex];
            }
        }
        return convolvedData;
    }

    /**
     * Performs a linear convolution in fourier space by implementing the
     * overlap add method.
     * <p>
     * @param signal Signal to convolve with filter.
     * @param filter Filter to convovle with signal.
     * <p>
     * @return Returns the result of the overlap add.
     */
    protected double[] overlapadd(double[] signal, double[] filter)
    {
        int hlen = filter.length;

        /* find out how many pixels to mirror, this depends on the length of the
         * filter for a N length filter convoluting with a M length filter we
         * will have a N+M-1 length signla, so the error is N/2 in each
         * direction that means we have to mirror N/2 pixels in each direction
         */
        int mirrorLen;
        if(hlen % 2 == 0) {
            mirrorLen = filter.length / 2;
        } else {
            mirrorLen = ((filter.length - 1) / 2) + 1;
        }
        double[] mleft = new double[mirrorLen];
        double[] mright = new double[mirrorLen];
        for(int i = 0; i < mirrorLen; i++) {
            mleft[i] = signal[mirrorLen - i - 1];
            mright[i] = signal[signal.length - i - 1];
        }
        double[] mirrored = new double[signal.length + 2 * mirrorLen];
        System.arraycopy(mleft, 0, mirrored, 0, mirrorLen);
        System.arraycopy(signal, 0, mirrored, mirrorLen, signal.length);
        System.arraycopy(mright, 0, mirrored, signal.length + mirrorLen, mirrorLen);

        int xlen = mirrored.length;
        // set the buffer to the length of the filter
        int L = hlen;
        // figure out how long our convolved signal will be
        int N = L + hlen - 1;
        // figure out whats the best blocksize to use, blocksize must be >= N and
        // a power of 2
        int blocksize = (int) Math.pow(2.0d, Math.ceil(Math.log(N) / Math.log(2)));
        double[] convolved = new double[blocksize * (int) (Math.ceil(((double) signal.length / (double) blocksize) + 1.0d))];
        FastFourierTransform fft = new FastFourierTransform(blocksize);

        for(int i = 0; i < xlen; i += L) {
            int cursize = Math.min(L, xlen - (i + 1));

            // pad signal and filter to fft blocksize
            double[] xpad = new double[blocksize];
            System.arraycopy(mirrored, i, xpad, 0, cursize);

            double[] hpad = new double[blocksize];
            System.arraycopy(filter, 0, hpad, 0, filter.length);

            // perform fft
            ComplexArray1D fftXpad = fft.transformFullOptimized(xpad);
            ComplexArray1D fftHpad = fft.transformFullOptimized(hpad);

            // multiply complex 
            Object[] mult = multiplyComplexFullOptimized((double[]) fftXpad.realValues, (double[]) fftXpad.imaginaryValues, (double[]) fftHpad.realValues, (double[]) fftHpad.imaginaryValues);
            // transformFFT inverse
            double[] y = fft.transformInverseFullOptimized((double[]) mult[0], (double[]) mult[1]);

            // the last  points (the overlapping points) are the points we add to the
            // next segment
            if(i + L >= xlen) {
                // last block
                addArrays(y, 0, convolved, i, cursize);
            } else {
                // copy the last  points (the overlapping points) are the points we add to the
                // next segment
                addArrays(y, 0, convolved, i, blocksize);
            }
        }
        // return the central part of the convolution that is the same size as x
        double[] out = new double[signal.length];
        System.arraycopy(convolved, 2 * mirrorLen - 1, out, 0, out.length);
        return out;
    }
}
