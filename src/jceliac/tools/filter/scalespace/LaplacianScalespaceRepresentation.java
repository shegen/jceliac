/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter.scalespace;

import jceliac.tools.filter.LowpassFilter;
import jceliac.JCeliacGenericException;
import java.util.*;
import java.util.concurrent.Callable;
import jceliac.tools.math.MathTools;
import jceliac.tools.parallel.ParallelTaskExecutor;

/**
 * Computes the Laplacian of a frame in Scalespace and performs non-maxima
 * suppression. We use a Gaussian convolution filter to compute the Scalespace
 * instead of using the modified Bessel function as suggested by Lindeberg. The
 * Scalespace is computed using second partial derivatives implemented using the
 * finite differences method.
 * <p>
 * Alternatively this could be implemented as difference of Gaussians such as
 * used by Lowe in SIFT. However I want to stick with the initial proof
 * implementation done in Matlab first.
 * <p>
 *
 * @author shegen
 */
public class LaplacianScalespaceRepresentation
        extends ScalespaceRepresentation
{

    public LaplacianScalespaceRepresentation(double[][] data, ScalespaceParameters scalespaceParameters)
    {
        super(data, scalespaceParameters);
    }

    @Override
    public void computeMaxima()
            throws JCeliacGenericException
    {
        this.collectMaxima();
    }

    /**
     * Compute the spatial maximas at all local scales, but not between scales.
     * <p>
     * @return List of Laplacian maxima responses at each scale.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    @Override
    public ScaleLevelIndexedDataSupplier<ArrayList<ScalespacePoint>> computeMaxima1D()
            throws JCeliacGenericException
    {
        ScaleLevelIndexedDataSupplier<ArrayList<ScalespacePoint>> maxima1DPerScale
                                                                  = new ScaleLevelIndexedDataSupplier<>(this.scalespaceParameters);

        Set<Integer> keys = this.normalizedLaplacianDataSupplier.keySet();
        for(Integer scaleLevel : keys) {
            ArrayList<ScalespacePoint> m1D = super.collectMaxima1D(scaleLevel);
            maxima1DPerScale.setData(scaleLevel, m1D);
        }
        return maxima1DPerScale;
    }

    @Override
    public void computeMinima() throws JCeliacGenericException
    {
        this.collectMinima();
    }

    /**
     * Compute the spatial minima at all local scales, but not between scales.
     * <p>
     * @return List of Laplacian maxima responses at each scale.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    @Override
    public ScaleLevelIndexedDataSupplier<ArrayList<ScalespacePoint>> computeMinima1D()
            throws JCeliacGenericException
    {
        ScaleLevelIndexedDataSupplier<ArrayList<ScalespacePoint>> minima1DPerScale
                                                                  = new ScaleLevelIndexedDataSupplier<>(this.scalespaceParameters);

        Set<Integer> keys = this.normalizedLaplacianDataSupplier.keySet();
        for(Integer scaleLevel : keys) {
            ArrayList<ScalespacePoint> m1D = super.collectMinima1D(scaleLevel);
            minima1DPerScale.setData(scaleLevel, m1D);
        }
        return minima1DPerScale;
    }

    public final static int DETECTOR_TYPE_ABSOLUTE_SUM = 1;
    public final static int DETECTOR_TYPE_SUM = 2;

    @Override
    public final void computeScalespaceRepresentation(int detectorType)
            throws JCeliacGenericException
    {
        this.computeScalespaceRepresentationSingleThreaded(detectorType);
    }
     
    /**
     * Computes the representation of the scalespace with a certain detector
     * type.
     * <p>
     * Checked this method, returns the same values as the matlab code in
     * interestpointsmultiscale.m
     * <p>
     * @param detectorType Type of detector for interestpoints.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public final void computeScalespaceRepresentationSingleThreaded(int detectorType)
            throws JCeliacGenericException
    {
        for(int scaleLevel = 0;
            scaleLevel < this.scalespaceParameters.getNumberOfScales();
            scaleLevel++) 
        {
            // compute sigmas such that (sigma_n+1 / sigma_n) is equal for all sigmas
            double sigma = this.scalespaceParameters.getSigmaForScaleLevel(scaleLevel);
            LowpassFilter gaussianFilter;
            int maxWidth = Math.min(this.data.length, this.data[0].length);
            if(maxWidth % 2 == 0) {
                maxWidth = maxWidth - 1;
            }
            int filterWidth = LowpassFilter.computeOptimalFilterSize(maxWidth, sigma);
            gaussianFilter = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, filterWidth, sigma);
            double[][] scaledData = gaussianFilter.filterData(this.data);

           // this.suppressEdgeResponse(scaledData, sigma);
            double[][] dxdx = MathTools.derivativeOrder2(scaledData, 1);
            double[][] dydy = MathTools.derivativeOrder2(scaledData, 2);

            double[][] normlaplacian;
            switch(detectorType) {
                case DETECTOR_TYPE_ABSOLUTE_SUM:
                    normlaplacian = this.computeNormalizedLaplacianAbsoluteSum(dxdx, dydy, sigma);
                    break;

                case DETECTOR_TYPE_SUM:
                    normlaplacian = this.computeNormalizedLaplacianSum(dxdx, dydy, sigma);
                    break;
                default:
                    throw new JCeliacGenericException("Unknown Detector Type");

            }
            this.scalespaceDataSupplier.setData(scaleLevel, scaledData);
            this.normalizedLaplacianDataSupplier.setData(scaleLevel, normlaplacian);
        }
    }
    
    /**
     * This code is actually only used for performance experiments to avoid computing
     * multiple features at once, but have a more realistic computation of the scalespace. 
     * 
     */
    private class ParallelScalespaceComputation
            implements Callable<Integer> {

        private final int scaleLevel;
        private final ScalespaceParameters scalespaceParameters;
        private final ScaleLevelIndexedDataSupplier<double[][]> scalespaceDataSupplier;
        private final ScaleLevelIndexedDataSupplier<double[][]> normlaplacianDataSupplier;
        private double [][] data;
            

        public ParallelScalespaceComputation(int scaleLevel, 
                                             ScalespaceParameters scalespaceParameters, 
                                             ScaleLevelIndexedDataSupplier<double[][]> scalespaceDataSupplier,
                                             ScaleLevelIndexedDataSupplier<double[][]> normlaplacianDataSupplier,
                                             double [][] data) 
        {
            this.scaleLevel = scaleLevel;
            this.scalespaceParameters = scalespaceParameters;
            this.normlaplacianDataSupplier = normlaplacianDataSupplier;
            this.scalespaceDataSupplier = scalespaceDataSupplier;
            this.data = data;
        }

        @Override
        public Integer call()
                throws JCeliacGenericException 
        {
            double sigma = this.scalespaceParameters.getSigmaForScaleLevel(this.scaleLevel);
            double [][] scaledData = this.computeScalespaceAtScaleLevel(sigma);
            
            double[][] dxdx = MathTools.derivativeOrder2(scaledData, 1);
            double[][] dydy = MathTools.derivativeOrder2(scaledData, 2);
            double [][] normlaplacian = this.computeNormalizedLaplacianAbsoluteSum(dxdx, dydy, sigma);
           
            synchronized(this.scalespaceDataSupplier) {
                this.scalespaceDataSupplier.setData(scaleLevel, scaledData);
                this.normlaplacianDataSupplier.setData(scaleLevel, normlaplacian);
            }
            return 0;

        }

        private double[][] computeScalespaceAtScaleLevel(double sigma)
                throws JCeliacGenericException {
            // compute sigmas such that (sigma_n+1 / sigma_n) is equal for all sigmas
            LowpassFilter gaussianFilter;
            int maxWidth = Math.min(this.data.length, this.data[0].length);
            if (maxWidth % 2 == 0) {
                maxWidth = maxWidth - 1;
            }
            int filterWidth = LowpassFilter.computeOptimalFilterSize(maxWidth, sigma);
            gaussianFilter = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, filterWidth, sigma);
            double[][] scaledData = gaussianFilter.filterData(this.data);
            return scaledData;
        }

        private double[][] computeNormalizedLaplacianAbsoluteSum(double[][] dxdx, double[][] dydy, double sigma) {
        // compute normalized laplacian for each pixel, please note that we actually
            // compute the squared of the laplacian to ignore the phase (the sign of the pixel)
            double[][] normlaplacian = new double[dxdx.length][dxdx[0].length];

            for (int i = 0; i < dxdx.length; i++) {
                for (int j = 0; j < dxdx[0].length; j++) {
                    normlaplacian[i][j] = (sigma * sigma) * Math.abs(dxdx[i][j] + dydy[i][j]);
                }
            }
            return normlaplacian;
        }
    }

    // ONLY use in the performance experiment, this does not make sense for anything else!
    public final void computeScalespaceRepresentationMultiThreaded(int detectorType)
            throws JCeliacGenericException
    {
        ArrayList taskList = new ArrayList();
        for(int scaleLevel = 0;
            scaleLevel < this.scalespaceParameters.getNumberOfScales();
            scaleLevel++) 
        {
            taskList.add(new ParallelScalespaceComputation(scaleLevel, this.scalespaceParameters, 
                                                           this.scalespaceDataSupplier, this.normalizedLaplacianDataSupplier,
                                                           this.data));
        }
        ParallelTaskExecutor scalespaceComputationTasks = new ParallelTaskExecutor(taskList, false);
        scalespaceComputationTasks.executeTasks();
    }
    

    /**
     * Computes the absolute normalized laplacian for each signal position.
     * <p>
     * @param dxdx  Second derivative with respect to x of signal.
     * @param dydy  Second derivative with respect to y of signal.
     * @param sigma Sigma of scale-level.
     * <p>
     * @return Scale normalized Laplacian response.
     */
    private double[][] computeNormalizedLaplacianAbsoluteSum(double[][] dxdx, double[][] dydy, double sigma)
    {
        // compute normalized laplacian for each pixel, please note that we actually
        // compute the squared of the laplacian to ignore the phase (the sign of the pixel)
        double[][] normlaplacian = new double[dxdx.length][dxdx[0].length];

        for(int i = 0; i < dxdx.length; i++) {
            for(int j = 0; j < dxdx[0].length; j++) {
                normlaplacian[i][j] = (sigma * sigma) * Math.abs(dxdx[i][j] + dydy[i][j]);
            }
        }
        return normlaplacian;
    }

    /**
     * Computes the normalized laplacian for each signal position.
     * <p>
     * @param dxdx  Second derivative with respect to x of signal.
     * @param dydy  Second derivative with respect to y of signal.
     * @param sigma Sigma of scale-level.
     * <p>
     * @return Scale normalized Laplacian response.
     */
    private double[][] computeNormalizedLaplacianSum(double[][] dxdx, double[][] dydy, double sigma)
    {
        // compute normalized laplacian for each pixel, please note that we actually
        // compute the squared of the laplacian to ignore the phase (the sign of the pixel)
        double[][] normlaplacian = new double[dxdx.length][dxdx[0].length];

        for(int i = 0; i < dxdx.length; i++) {
            for(int j = 0; j < dxdx[0].length; j++) {
                normlaplacian[i][j] = (sigma * sigma) * (dxdx[i][j] + dydy[i][j]);
            }
        }
        return normlaplacian;
    }

    /**
     * Computes the scale-space representation, used as reference.
     * <p>
     * Checked this method, returns the same values as the matlab code in
     * interestpointsmultiscale.m
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    protected final void computeScalespaceRepresentationSlow()
            throws JCeliacGenericException
    {
        for(int scaleLevel = 0;
            scaleLevel < this.scalespaceParameters.getNumberOfScales();
            scaleLevel++) {
            // compute sigmas such that (sigma_n+1 / sigma_n) is equal for all sigmas
            double sigma = this.scalespaceParameters.getSigmaForScaleLevel(scaleLevel);
            LowpassFilter gaussianFilter;

            int minDataDimension = Math.min(this.data.length, this.data[0].length);
            double[][] scaledData;
            if(minDataDimension != 128 && minDataDimension != 256) {
                // pick the smaller dimension for the gaussian filter
                int filterLen = Math.min(this.data.length, this.data[0].length);
                if(filterLen % 2 == 0) {
                    filterLen = filterLen - 1;
                }
                // create gaussian filter with adaptive size
                gaussianFilter = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, filterLen, sigma);
                scaledData = gaussianFilter.filterData(this.data);

            } else {
                gaussianFilter = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, minDataDimension - 1, sigma);
                scaledData = gaussianFilter.filterDataFFT(this.data);
            }
            double[][] dxdx = MathTools.derivativeOrder2(scaledData, 1);
            double[][] dydy = MathTools.derivativeOrder2(scaledData, 2);

            // compute normalized laplacian for each pixel, please note that we actually
            // compute the squared of the laplacian to ignore the phase (the sign of the pixel)
            double[][] normlaplacian = new double[scaledData.length][scaledData[0].length];

            for(int i = 0; i < scaledData.length; i++) {
                for(int j = 0; j < scaledData[0].length; j++) {
                    // scale-normalized laplacian
                    normlaplacian[i][j] = (sigma * sigma) * Math.abs(dxdx[i][j] + dydy[i][j]);
                }
            }
            this.scalespaceDataSupplier.setData(scaleLevel, scaledData);
            this.normalizedLaplacianDataSupplier.setData(scaleLevel, normlaplacian);
        }
    }
    
    private void suppressEdgeResponse(double [][] dataInScalespace, double detectionSigma) 
            throws JCeliacGenericException
    {
        StructureTensor tensor = new StructureTensor(dataInScalespace, detectionSigma);
        tensor.prepareData();
        for(int i = 0; i < dataInScalespace.length; i++) {
            for(int j = 0; j < dataInScalespace[0].length; j++) {
                SecondMomentMatrix smm = tensor.getTensor(i,j);
                double ratio = smm.getEigenvalueRatio();
                if(ratio > 3) {
                    dataInScalespace[i][j] = 0;
                }
            }
        }
        
    }
}
