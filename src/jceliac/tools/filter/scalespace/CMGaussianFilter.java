/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jceliac.tools.filter.scalespace;

import jceliac.JCeliacGenericException;
import jceliac.tools.filter.DataFilter2D;

/**
 *
 * @author shegen
 */
public class CMGaussianFilter extends DataFilter2D
{

    private double [][] filterX;
    private double [][] filterY;
    
    private double b = 0.7;
    // sigma = A*lambda
    private double A = (1.0d/Math.PI) * Math.sqrt(Math.log(2.0) / 2.0d) * ((Math.pow(2.0d, b)+1) / (Math.pow(2.0d, b)-1));
    
    private double lambda0 = 8.0d;
    private double K = 6.0d; //6.0d;
    public CMGaussianFilter(int n) 
    {
        double lambda = lambda0 * Math.pow(2,n/K);
        // compute sigma from lambda
        double sigma = this.A * lambda;
        // finite response filter length of at least 4*sigma is required
        //this.computeFilters(sigma, lambda, (int)Math.ceil(4*sigma));
        this.computeFilterCoeffsForScale(sigma);
    }
    
    private double a0 = 0.2678;
    private double a1 = 1.0305;
    private double b0 = 1.3127;
    private double b1 = 1.3827;
    private double c0 = 0.7264;
    private double c1 = -1.1576;
    private double omega0 = 4.4808;
    private double omega1 = 2.7935;
            
    
    private double n33Plus(double a0, double a1, double b0, double b1, double c0, double c1, double sigma, double omega0, double omega1) {
        return Math.exp(-(b1/sigma) - ((2*b0)/sigma)) * (c1 * Math.sin(omega1/sigma) - c0 * Math.cos(omega1/sigma)) +
               Math.exp(-(b0/sigma) - ((2*b1)/sigma)) * (a1 * Math.sin(omega0/sigma) - a0 * Math.cos(omega0/sigma)); 
    }
    
    private double n22Plus(double a0, double a1, double b0, double b1, double c0, double c1, double sigma, double omega0, double omega1) {
        return 2 * Math.exp(-(b0/sigma) - (b1/sigma)) * ((a0+c0) * Math.cos(omega0/sigma) * Math.cos(omega1/sigma) -
                   Math.cos(omega1/sigma) * a1 * Math.sin(omega0/sigma) - Math.cos(omega0/sigma) * c1 * Math.sin(omega1/sigma)) + 
                   c0 * Math.exp(-((2*b0)/sigma)) + a0 * Math.exp(-((2*b1) / sigma));
                
               
    }
    
    private double n11Plus(double a0, double a1, double b0, double b1, double c0, double c1, double sigma, double omega0, double omega1) {
        return Math.exp(-(b1/sigma)) * (c1 * Math.sin(omega1/sigma) - (c0 + 2*a0)*Math.cos(omega1/sigma)) + 
               Math.exp(-(b0/sigma)) * (a1 * Math.sin(omega0/sigma) - (2*c0 + a0)*Math.cos(omega0/sigma));
    }
    
    private double n00Plus(double a0, double c0) {
        return a0+c0;
    }
    
    private double d44PlusMinus(double b0, double b1, double sigma) {
        return Math.exp(-((2*b0)/sigma) - ((2*b1)/sigma));
    }
    
    private double d33PlusMinus(double b0, double b1, double sigma, double omega0, double omega1) {
        return -2 * Math.cos(omega0/sigma) * Math.exp(-(b0/sigma) - ((2*b1) / sigma)) - 2 * Math.cos(omega1/sigma) * Math.exp(-(b1/sigma) - ((2*b0)/sigma));
    }
     
    private double d22PlusMinus(double b0, double b1, double sigma, double omega0, double omega1) {
        return 4 * Math.cos(omega1/sigma) * Math.cos(omega0/sigma) * Math.exp(-(b0/sigma) - (b1/sigma)) + Math.exp(-((2*b1)/sigma)) + 
               Math.exp(-((2*b0) / sigma));
    }
    
    private double d11PlusMinus(double b0, double b1, double sigma, double omega0, double omega1) {
        return -2 * Math.exp(-(b1/sigma)) * Math.cos(omega1/sigma) - 2 * Math.exp(-(b0/sigma)) * Math.cos(omega0/sigma);
    }
    
    private double n33Minus(double a0, double a1, double b0, double b1, double c0, double c1, double sigma, double omega0, double omega1) {
        return n33Plus(a0, a1, b0, b1, c0, c1, sigma, omega0, omega1) - d33PlusMinus(b0, b1, sigma, omega0, omega1) * n00Plus(a0,c0);
    }
    
    public double n22Minus(double a0, double a1, double b0, double b1, double c0, double c1, double sigma, double omega0, double omega1) 
    {
        return n22Plus(a0, a1, b0, b1, c0, c1, sigma, omega0, omega1) - d22PlusMinus(b0, b1, sigma, omega0, omega1) * n00Plus(a0, c0);
    }
    public double n11Minus(double a0, double a1, double b0, double b1, double c0, double c1, double sigma, double omega0, double omega1) 
    {
        return n11Plus(a0, a1, b0, b1, c0, c1, sigma, omega0, omega1) - d11PlusMinus(b0, b1, sigma, omega0, omega1) * n00Plus(a0, c0);
    }
    public double n44Minus(double a0, double b0, double b1, double c0, double sigma) {
        return -d44PlusMinus(b0, b1, sigma) * n00Plus(a0, c0);
    }

    private double n00P;
    private double n11P;
    private double n22P;
    private double n33P;
    private double n11M;
    private double n22M;
    private double n33M;
    private double n44M;

    private double d11PM;
    private double d22PM;
    private double d33PM;
    private double d44PM;

    private void computeFilterCoeffsForScale(double sigma) 
    {
        this.n00P = this.n00Plus(a0, c0);
        this.n11P = this.n11Plus(a0, a1, b0, b1, c0, c1, sigma, omega0, omega1);
        this.n22P = this.n22Plus(a0, a1, b0, b1, c0, c1, sigma, omega0, omega1);
        this.n33P = this.n33Plus(a0, a1, b0, b1, c0, c1, sigma, omega0, omega1);

        this.n11M = this.n11Minus(a0, a1, b0, b1, c0, c1, sigma, omega0, omega1);
        this.n22M = this.n22Minus(a0, a1, b0, b1, c0, c1, sigma, omega0, omega1);
        this.n33M = this.n33Minus(a0, a1, b0, b1, c0, c1, sigma, omega0, omega1);
        this.n44M = this.n44Minus(a0, b0, b1, c0, sigma);

        this.d11PM = this.d11PlusMinus(b0, b1, sigma, omega0, omega1);
        this.d22PM = this.d22PlusMinus(b0, b1, sigma, omega0, omega1);
        this.d33PM = this.d33PlusMinus(b0, b1, sigma, omega0, omega1);
        this.d44PM = this.d44PlusMinus(b0, b1, sigma);
    }


    public double ykPlusM4(double [] data, int k) 
    {
        if(k < 0 || k > data.length) {
            return 0;
        }
        double [] xks = new double[4];
        for(int i = 0; i < xks.length; i++) {
            if(k-i < 0) break;
            xks[i] = data[k-i];
        }
        
        double yk = this.n00P * xks[0];  // xk
        yk += n11P * xks[1];           // xk-1 xks is in reverse order!
        yk += n22P * xks[2];           // xk-2
        yk += n33P * xks[3];           // xk-3
        
        // recursive part
        yk -= d11PM * this.ykPlusM4(data, k-1);
        yk -= d22PM * this.ykPlusM4(data, k-2);
        yk -= d33PM * this.ykPlusM4(data, k-3);
        yk -= d44PM * this.ykPlusM4(data, k-4);
        return yk;
    }
    
    public double ykMinusM4(double [] data, int k) 
    {
        if(k < 0 || k > data.length) {
            return 0;
        }
        double [] xks = new double[4];
        for(int i = 0; i < xks.length; i++) {
            if(k+i+1 >= data.length) {
                break;
            }
            xks[i] = data[k+i+1];
        }
        // xks are k+1
        double yk = this.n11M * xks[0]; // k+1
        yk += this.n22M * xks[1];    // k+2
        yk += this.n33M * xks[2];    // k+3
        yk += this.n44M * xks[3];    // k+4
        return yk;
    }
    
    public double yk(double [] data, int k) 
    {
        return this.ykPlusM4(data, k) + this.ykMinusM4(data, k);
    }
    
    
 /*  private void computeFilters(double sigma, double lambda, int dimension)
    {
        this.filterX = new double[dimension][dimension];
        this.filterY = new double[dimension][dimension];
        
        double center = (((double) (dimension - 1)) / 2.0d);
        double sumx = 0;
        double sumy = 0;

        for(double x = -center; x <= center; x++) {
            for(double y = -center; y <= center; y++) {
                this.filterX[(int) (x + center)][(int) (y + center)] = Math.exp(-(x * x + y * y) / (2 * sigma * sigma)) * Math.cos((2.0d*Math.PI*x)/lambda);
                this.filterY[(int) (x + center)][(int) (y + center)] = Math.exp(-(x * x + y * y) / (2 * sigma * sigma)) * Math.cos((2.0d*Math.PI*y)/lambda);
                sumx += Math.abs(this.filterX[(int) (x + center)][(int) (y + center)]);
                sumy += Math.abs(this.filterY[(int) (x + center)][(int) (y + center)]);
            }
        }
        // normalize to 1
       for(double x = -center; x <= center; x++) {
            for(double y = -center; y <= center; y++) {
                this.filterX[(int) (x + center)][(int) (y + center)] = (this.filterX[(int) (x + center)][(int) (y + center)] / sumx);
                this.filterY[(int) (y + center)][(int) (y + center)] = (this.filterX[(int) (y + center)][(int) (y + center)] / sumy);
            }
        }
    }
*/
    
    
    @Override
    public double[][] filterData(double[][] signal)
            throws JCeliacGenericException {
        
        
        // compute sigma from lambda
        double sigma = 2;
        this.computeFilterCoeffsForScale(sigma);
        
        
        int N = 8; //(int)Math.ceil(2*3*sigma);
        double[] sig = new double[N];
        double[] filter = new double[N];
        
        double [] ykPlus = new double[N];
        double [] ykMinus = new double[N];
        
        sig[0] = 1.0d;
        for (int k = 0; k < N; k++) {
            ykPlus[k] = this.ykPlusM4(sig, k);
        }
        
        for (int k = N-1, i = 0; k >= 0; k--) {
            ykMinus[k] = this.ykMinusM4(sig, k);
        }
        double [] yk = new double[N];
        for(int i = 0; i < ykPlus.length; i++) {
            yk[i] = ykPlus[i] + ykMinus[i];
        }

        return null;
    }

    
    
    @Override
    public double[][] filterDataFFT(double[][] signal) 
            throws JCeliacGenericException 
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double filterContinuousDataPosition(double i, double j, double[][] data) 
            throws JCeliacGenericException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double filterDiscreteDataPosition(int i, int j, double[][] data) 
            throws JCeliacGenericException 
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
