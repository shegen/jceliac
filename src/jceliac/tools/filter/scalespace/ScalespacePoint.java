/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter.scalespace;

import java.util.ArrayList;

/**
 * Represents a point in Scalespace.
 * <p>
 * @author shegen
 * <p>
 */
public class ScalespacePoint
        implements Comparable<ScalespacePoint>
{

    private double scaleExponent; // index of scalespace
    private final double response;           // actual normalized value of point
    private final int coordX;             // X-coordinate in frame
    private final int coordY;             // Y-coordinate in frame
    private boolean valid = true;
    private double sigma;           // sigma used to compute scale 
    private ScalespaceParameters scalespaceParameters;  // parameter of the parent scalespace
    private SecondMomentMatrix secondMomentMatrix;  // second moment matrix computed at this interestpoint
    private double isotropyMeasure; // isotropy measure is basically 1 minus ratio of eigenvalues of the second moment matrix
    private int children; // the number of pixels assigned to this point with a reliability > 0
    private double scaleConfidence;
    private ScalespacePoint parent;
    private final ArrayList<ScalespacePoint> parents;
    private final int scaleLevel;

    public ScalespacePoint(int scaleLevel, double response, int coordX, int coordY, ScalespaceParameters scalespaceParameters)
    {
        this.scaleLevel = scaleLevel;
        this.response = response;
        this.coordX = coordX;
        this.coordY = coordY;
        this.sigma = scalespaceParameters.getSigmaForScaleLevel(scaleLevel);
        this.scalespaceParameters = scalespaceParameters;
        this.parents = new ArrayList<>();
    }

    public int getScaleLevel()
    {
        return this.scaleLevel;

    }

    public double getSigma()
    {
        return this.sigma;
    }

    public double getResponse()
    {
        return response;
    }

    public int getCoordX()
    {
        return coordX;
    }

    public int getCoordY()
    {
        return coordY;
    }

    @Override
    public int compareTo(ScalespacePoint t)
    {
        if(this.response > t.getResponse()) {
            return 1;
        } else {
            return -1;
        }
    }

    public boolean isValid()
    {
        return valid;
    }

    public void setScaleExponent(double scaleExponent)
    {
        this.scaleExponent = scaleExponent;
    }

    public void setValid(boolean valid)
    {
        this.valid = valid;
    }

    public SecondMomentMatrix getSecondMomentMatrix()
    {
        return secondMomentMatrix;
    }

    public void setSecondMomentMatrix(SecondMomentMatrix secondMomentMatrix)
    {
        this.secondMomentMatrix = secondMomentMatrix;
    }

    public void setSigma(double sigma)
    {
        this.sigma = sigma;
    }

    public double getIsotropyMeasure()
    {
        return isotropyMeasure;
    }

    public void setIsotropyMeasure(double isotropyMeasure)
    {
        this.isotropyMeasure = isotropyMeasure;
    }

    public int getChildren()
    {
        return children;
    }

    public void setChildren(int children)
    {
        this.children = children;
    }

    public double getScaleConfidence()
    {
        return scaleConfidence;
    }

    public void setScaleConfidence(double scaleConfidence)
    {
        this.scaleConfidence = scaleConfidence;
    }

    public ScalespaceParameters getScalespaceParameters()
    {
        return scalespaceParameters;
    }

    public void setScalespaceParameters(ScalespaceParameters scalespaceParameters)
    {
        this.scalespaceParameters = scalespaceParameters;
    }

    public ScalespacePoint getParent()
    {
        return parent;
    }

    public void setParent(ScalespacePoint parent)
    {
        this.parent = parent;
    }
}
