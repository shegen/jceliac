/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter.scalespace;

import jceliac.JCeliacGenericException;
import jceliac.tools.filter.LowpassFilter;
import jceliac.tools.math.MathTools;

/**
 *
 * @author shegen
 */
public class StructureTensor
{

    private final double[][] imageData;
    private final double detectionSigma;
    private double[][] IxxMoments;
    private double[][] IxyMoments;
    private double[][] IyyMoments;

    /**
     * This expects image data or the image data in scale-space.
     * <p>
     * @param imageData      Image data.
     * @param detectionSigma Detection sigma of structure tensor.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public StructureTensor(double[][] imageData, double detectionSigma)
            throws JCeliacGenericException
    {
        this.detectionSigma = detectionSigma;
        this.imageData = imageData;

        this.prepareData();
    }

    /**
     * Computes the structure tensors for the supplied image data using sqrt(2)
     * detectionSigma as integration scale.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public final void prepareData()
            throws JCeliacGenericException
    {
        // scale of the detection of a feature, scales are in terms of standard deviation of 
        // the gaussian kernel
        double detectionScaleSigma = this.detectionSigma;
        double integrationScaleSigma = (detectionScaleSigma * Math.sqrt(2.0d));

        int optimalFilterWidth = LowpassFilter.computeOptimalFilterSize(127, detectionScaleSigma);
        LowpassFilter gaussianDetection = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN,
                                                            optimalFilterWidth, detectionScaleSigma);

        double[][] scaledData = gaussianDetection.filterData(this.imageData);

        // compute derivatives
        double[][] dx = MathTools.derivativeOrder1(scaledData, 1);
        double[][] dy = MathTools.derivativeOrder1(scaledData, 2);

        double[][] dxx = this.multiply(dx, dx);
        double[][] dyy = this.multiply(dy, dy);
        double[][] dxy = this.multiply(dx, dy);

        optimalFilterWidth = LowpassFilter.computeOptimalFilterSize(127, integrationScaleSigma);
        LowpassFilter gaussian = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN,
                                                   optimalFilterWidth, integrationScaleSigma);

        double[][] dxxMom = gaussian.filterData(dxx);
        double[][] dxyMom = gaussian.filterData(dxy);
        double[][] dyyMom = gaussian.filterData(dyy);

        this.IxxMoments = dxxMom;
        this.IxyMoments = dxyMom;
        this.IyyMoments = dyyMom;
    }

    /**
     * Returns structure tensor at position (x,y).
     * <p>
     * @param x Position x.
     * @param y Position y.
     * <p>
     * @return Structure tenros at (x,y).
     */
    public SecondMomentMatrix getTensor(int x, int y)
    {
        return new SecondMomentMatrix(this.IxxMoments[x][y], this.IxyMoments[x][y],
                                      this.IxyMoments[x][y], this.IyyMoments[x][y]);
    }

    /**
     * Multpilies two arrays.
     * <p>
     * @param d1 Array one.
     * @param d2 Array two.
     * <p>
     * @return Element wise multiplication of one * two.
     */
    protected double[][] multiply(double[][] d1, double[][] d2)
    {
        double[][] res = new double[d1.length][d1[0].length];

        for(int i = 0; i < d1.length; i++) {
            for(int j = 0; j < d1[0].length; j++) {
                res[i][j] = d1[i][j] * d2[i][j];
            }
        }
        return res;
    }
}
