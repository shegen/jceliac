/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter.scalespace;

import jceliac.featureExtraction.affineScaleLBP.Ellipse;

/**
 * This are methods used to debug and improve the methods based on the
 * scale-space and Laplacian detector. The methods visualize several data such
 * as maxima and so on.
 * <p>
 * @author shegen
 */
public class LaplacianVisualization
{

    /**
     * Draw a circle into the image.
     * <p>
     * @param image  Image to draw into.
     * @param i      Center position of circle.
     * @param j      Center position of circle.
     * @param radius Radius of circle.
     */
    public static void drawCircle(double[][] image, int i, int j, double radius)
    {
        for(double angle = 0; angle < 2 * Math.PI; angle += 0.01) {
            int l = (int) Math.round(i + radius * Math.cos(angle));
            // in fact it does not matter whether this is + or minues because the circle is symmetric
            int m = (int) Math.round(j - radius * Math.sin(angle));

            if(l <= 0 || l >= image.length) {
                continue;
            }
            if(m <= 0 || m >= image.length) {
                continue;
            }

            image[l][m] = 255; // draw pixel
        }
    }

    /**
     * Draws an ellipse into an image.
     * <p>
     * @param image Image to draw into.
     * @param x     Center position of ellipse.
     * @param y     Center position of ellipse.
     * @param ell   Ellipse.
     */
    public static void drawEllipse(double[][] image, int x, int y, Ellipse ell)
    {
        for(double angle = 0; angle < 2 * Math.PI; angle += 0.01) {
            int l = x + (int) Math.round(ell.a * Math.cos(angle) * Math.cos(ell.phi) - ell.b * Math.sin(angle) * Math.sin(ell.phi));
            // note: matlab plot grows up in the y-dimension, but images grow downward in the y-dimension therefore y - term
            int m = y + (int) Math.round(ell.a * Math.cos(angle) * Math.sin(ell.phi) + ell.b * Math.sin(angle) * Math.cos(ell.phi));

            if(l <= 0 || l >= image.length) {
                continue;
            }
            if(m <= 0 || m >= image.length) {
                continue;
            }

            image[l][m] = 255; // draw pixel
        }
    }

}
