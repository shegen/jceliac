/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter.scalespace;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Represents parameters for computing a scale-space.
 * <p>
 * @author shegen
 */
public class ScalespaceParameters
        implements Serializable
{

    protected double scaleConstant;
    private final double startScaleLevel;
    private final double endScaleLevel;
    private final double scaleStepping;
    private final double constantFactor;
    private final ArrayList <Double> scalespaceSigmas = new ArrayList <>();
    
    
    public ScalespaceParameters(double scaleConstant, double startScaleLevel,
                                double endScaleLevel, double scaleStepping,
                                double constantFactor)
    {
        this.scaleConstant = scaleConstant;
        this.startScaleLevel = startScaleLevel;
        this.endScaleLevel = endScaleLevel;
        this.scaleStepping = scaleStepping;
        this.constantFactor = constantFactor;

        for(double scaleExponent = startScaleLevel; 
            scaleExponent <= endScaleLevel; 
            scaleExponent += scaleStepping) 
        {
            double sigma = Math.pow(scaleConstant, scaleExponent) * constantFactor;
            this.scalespaceSigmas.add(sigma);
        }
    }
    
    public ScalespaceParameters(double [] sigmas) 
    {
        for(int i = 0; i < sigmas.length; i++) {
            this.scalespaceSigmas.add(sigmas[i]);
        }
        this.scaleStepping = 1;
        this.scaleConstant = 1;
        this.startScaleLevel = sigmas[0];
        this.endScaleLevel = sigmas[sigmas.length-1];
        this.constantFactor = 1;
    }

    public double getSigmaForScaleExponent(double scaleExponent)
    {
        return this.constantFactor * Math.pow(this.scaleConstant, scaleExponent);
    }

    public double getStartScaleLevel()
    {
        return startScaleLevel;
    }

    public double getEndScaleLevel()
    {
        return endScaleLevel;
    }

    public double getScaleStepping()
    {
        return scaleStepping;
    }

    public int getNumberOfScales()
    {
        return this.scalespaceSigmas.size();
    }

    public double getSigmaForScaleLevel(int level)
    {
        return this.scalespaceSigmas.get(level);  
    }

}
