/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter.scalespace;

import jceliac.JCeliacGenericException;
import java.util.HashMap;
import java.util.Set;

/**
 * Provides a data structure to query scale-space data by the corresponding
 * scale level.
 * <p>
 * @author shegen
 * @param <T> Type of data probiveded by the supplier.
 */
public class ScaleLevelIndexedDataSupplier<T>
{

    private final HashMap<Integer, T> data;
    private final ScalespaceParameters scalespaceParameters;

    public ScaleLevelIndexedDataSupplier(ScalespaceParameters scalespaceParameters)
    {
        this.data = new HashMap<>();
        this.scalespaceParameters = scalespaceParameters;
    }

    public T getData(int scaleLevel)
            throws JCeliacGenericException
    {
        if(this.data.containsKey(scaleLevel) == false) {
            throw new JCeliacGenericException("No entry for scale level: " + scaleLevel);
        }
        return this.data.get(scaleLevel);
    }

    public void setData(int scaleLevel, T data)
    {
        this.data.put(scaleLevel, data);
    }

    public int getScaleCount()
    {
        return this.data.size();
    }

    public Set<Integer> keySet()
    {
        return this.data.keySet();
    }

    public ScalespaceParameters getScalespaceParameters()
    {
        return scalespaceParameters;
    }
}
