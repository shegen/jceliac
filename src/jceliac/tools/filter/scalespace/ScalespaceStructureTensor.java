/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter.scalespace;

import jceliac.featureExtraction.affineScaleLBP.Ellipse;
import jceliac.tools.filter.LowpassFilter;
import jceliac.JCeliacGenericException;
import jceliac.tools.math.MatrixTools;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.List;
import jceliac.tools.math.MathTools;

/**
 * Implements the computation and representation of a multi-scale second moment
 * matrix or structure tensor in scale-space.
 * <p>
 * @author shegen
 */
public class ScalespaceStructureTensor
{

    private ScalespaceParameters scalespaceParameters;
    private final ScaleLevelIndexedDataSupplier<double[][]> scalespaceData;
    private final ScaleLevelIndexedDataSupplier<double[][]> IxxMoments;
    private final ScaleLevelIndexedDataSupplier<double[][]> IxyMoments;
    private final ScaleLevelIndexedDataSupplier<double[][]> IyyMoments;
    private int width;
    private int height;

    // this expects the scalespace representation of an image
    public ScalespaceStructureTensor(ScalespaceRepresentation scalespaceRep)
            throws JCeliacGenericException
    {
        this.scalespaceParameters = scalespaceRep.getScalespaceParameters();
        this.scalespaceData = scalespaceRep.getScalespaceDataSupplier();
        this.IxxMoments = new ScaleLevelIndexedDataSupplier<>(this.scalespaceParameters);
        this.IxyMoments = new ScaleLevelIndexedDataSupplier<>(this.scalespaceParameters);
        this.IyyMoments = new ScaleLevelIndexedDataSupplier<>(this.scalespaceParameters);
    }

    /**
     * Computes the structure tensors for a given scale-space representation.
     * <p>
     * @param params Scale-space parameters of corresponding scale-space
     *               representation.
     * <p>
     * @throws JCeliacGenericException
     */
    public void computeStructureTensorsAtAllScales()
            throws JCeliacGenericException
    {

        for(int scaleLevel = 0;
            scaleLevel < this.scalespaceParameters.getNumberOfScales();
            scaleLevel++) 
        {
            double[][] scaleData = this.scalespaceData.getData(scaleLevel);
            this.width = scaleData.length;
            this.height = scaleData[0].length;

            // compute derivatives
            double[][] dx = MathTools.derivativeOrder1(scaleData, 1);
            double[][] dy = MathTools.derivativeOrder1(scaleData, 2);

            double[][] dxx = this.multiply(dx, dx);
            double[][] dyy = this.multiply(dy, dy);
            double[][] dxy = this.multiply(dx, dy);

            // scale of the detection of a feature, scales are in terms of standard deviation of the gaussian kernel
            double detectionScaleSigma = this.scalespaceParameters.getSigmaForScaleLevel(scaleLevel);
            double integrationScaleSigma = detectionScaleSigma * Math.sqrt(2.0d);

            int optimalFilterWidth = LowpassFilter.computeOptimalFilterSize(127, integrationScaleSigma);
            LowpassFilter gaussian = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN,
                                                           optimalFilterWidth, integrationScaleSigma);

            double[][] dxxMom = gaussian.filterData(dxx);
            double[][] dxyMom = gaussian.filterData(dxy);
            double[][] dyyMom = gaussian.filterData(dyy);

            this.IxxMoments.setData(scaleLevel, dxxMom);
            this.IxyMoments.setData(scaleLevel, dxyMom);
            this.IyyMoments.setData(scaleLevel, dyyMom);
        }
    }
    
    public void computeStructureTensorsAtSpecificScales(int [] scalespaceIndices)
            throws JCeliacGenericException
    {

        for(int index = 0;
            index < scalespaceIndices.length; 
            index++) 
        {
            double[][] scaleData = this.scalespaceData.getData(scalespaceIndices[index]);
            this.width = scaleData.length;
            this.height = scaleData[0].length;

            // compute derivatives
            double[][] dx = MathTools.derivativeOrder1(scaleData, 1);
            double[][] dy = MathTools.derivativeOrder1(scaleData, 2);

            double[][] dxx = this.multiply(dx, dx);
            double[][] dyy = this.multiply(dy, dy);
            double[][] dxy = this.multiply(dx, dy);

            // scale of the detection of a feature, scales are in terms of standard deviation of the gaussian kernel
            double detectionScaleSigma = this.scalespaceParameters.getSigmaForScaleLevel(scalespaceIndices[index]);
            double integrationScaleSigma = detectionScaleSigma * Math.sqrt(2.0d);

            int optimalFilterWidth = LowpassFilter.computeOptimalFilterSize(127, integrationScaleSigma);
            LowpassFilter gaussian = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN,
                                                           optimalFilterWidth, integrationScaleSigma);

            double[][] dxxMom = gaussian.filterData(dxx);
            double[][] dxyMom = gaussian.filterData(dxy);
            double[][] dyyMom = gaussian.filterData(dyy);

            this.IxxMoments.setData(scalespaceIndices[index], dxxMom);
            this.IxyMoments.setData(scalespaceIndices[index], dxyMom);
            this.IyyMoments.setData(scalespaceIndices[index], dyyMom);
        }
    }

    /**
     * Computes sturcture tensors at interstpoint positions, the computed
     * tensors are stored in the interstpoint stuctures.
     * <p>
     * @param interestpoints Interstpoints to compute tensors at.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public void computeStructureTensorsForInterestpoints(ArrayList<ScalespacePoint> interestpoints)
            throws JCeliacGenericException
    {

        for(ScalespacePoint tmp : interestpoints) {
            int x = tmp.getCoordX();
            int y = tmp.getCoordY();

            double[][] IxxMom = this.IxxMoments.getData(tmp.getScaleLevel());
            double[][] IxyMom = this.IxyMoments.getData(tmp.getScaleLevel());
            double[][] IyyMom = this.IyyMoments.getData(tmp.getScaleLevel());

            SecondMomentMatrix mu = new SecondMomentMatrix(IxxMom[x][y], IxyMom[x][y], IxyMom[x][y], IyyMom[x][y]);
            tmp.setSecondMomentMatrix(mu);

            // compute isotropy measure, if the affine transform is close to a pure rotation we reached
            // a circle; and a circle is an ellipse with two axis of the same length, so the eigenvalues
            // give the length (proportionally) of the axis
            double[] eigenvalues = MatrixTools.eigenvalues(IxxMom[x][y], IxyMom[x][y], IxyMom[x][y], IyyMom[x][y]);

            // find largest eigenvalue
            double maxEigenvalue = Math.max(eigenvalues[0], eigenvalues[1]);
            double minEigenvalue = Math.min(eigenvalues[0], eigenvalues[1]);

            double Q = (1.0d - (minEigenvalue / maxEigenvalue));
            tmp.setIsotropyMeasure(Q);
        }
    }

    /**
     * Computes sturcture tensors at interstpoint positions, the computed
     * tensors are stored in the interstpoint stuctures.
     * <p>
     * @param interestpointsByScaleLevel Data supplier for interstpoints sorted
     *                                   by scale level.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public void computeStructureTensors(ScaleLevelIndexedDataSupplier<ArrayList<ScalespacePoint>> interestpointsByScaleLevel)
            throws JCeliacGenericException
    {

        for(Integer scaleLevel : interestpointsByScaleLevel.keySet()) {
            ArrayList<ScalespacePoint> interestpoints = interestpointsByScaleLevel.getData(scaleLevel);
            for(ScalespacePoint tmp : interestpoints) {
                int x = tmp.getCoordX();
                int y = tmp.getCoordY();

                double[][] IxxMom = this.IxxMoments.getData(tmp.getScaleLevel());
                double[][] IxyMom = this.IxyMoments.getData(tmp.getScaleLevel());
                double[][] IyyMom = this.IyyMoments.getData(tmp.getScaleLevel());

                SecondMomentMatrix mu = new SecondMomentMatrix(IxxMom[x][y], IxyMom[x][y], IxyMom[x][y], IyyMom[x][y]);
                tmp.setSecondMomentMatrix(mu);

                // compute isotropy measure, if the affine transform is close to a pure rotation we reached
                // a circle; and a circle is an ellipse with two axis of the same length, so the eigenvalues
                // give the length (proportionally) of the axis
                double[] eigenvalues = MatrixTools.eigenvalues(IxxMom[x][y], IxyMom[x][y], IxyMom[x][y], IyyMom[x][y]);

                // find largest eigenvalue
                double maxEigenvalue = Math.max(eigenvalues[0], eigenvalues[1]);
                double minEigenvalue = Math.min(eigenvalues[0], eigenvalues[1]);

                double Q = (1.0d - (minEigenvalue / maxEigenvalue));
                tmp.setIsotropyMeasure(Q);
            }
        }
    }

    /**
     * Returns a structures tensor at a certain position and scale level.
     * <p>
     * @param x          Position of structure tensor.
     * @param y          Position of structure tensor.
     * @param scaleLevel Scale level of structure tensor.
     * <p>
     * @return Structure tensor at the given position and scale level.
     * <p>
     * @throws JCeliacGenericException
     */
    public SecondMomentMatrix getStructureTensor(int x, int y, int scaleLevel)
            throws JCeliacGenericException
    {
        double[][] IxxMom = this.IxxMoments.getData(scaleLevel);
        double[][] IxyMom = this.IxyMoments.getData(scaleLevel);
        double[][] IyyMom = this.IyyMoments.getData(scaleLevel);
        if(IxxMom == null || IxyMom == null || IyyMom == null) {
            return null;
        }
        SecondMomentMatrix mu = new SecondMomentMatrix(IxxMom[x][y], IxyMom[x][y], IxyMom[x][y], IyyMom[x][y]);
        return mu;
    }

    /**
     * Computes the average structure tensor for a scale level.
     * <p>
     * @param scaleLevel Scale level to compute average structure tensor.
     * <p>
     * @return Average structure tensor at scale level.
     * <p>
     * @throws JCeliacGenericException
     */
    public SecondMomentMatrix getAverageStructureTensor(int scaleLevel)
            throws JCeliacGenericException
    {
        double IxxMomAvg = 0;
        double IxyMomAvg = 0;
        double IyyMomAvg = 0;

        double[][] IxxMom = this.IxxMoments.getData(scaleLevel);
        double[][] IxyMom = this.IxyMoments.getData(scaleLevel);
        double[][] IyyMom = this.IyyMoments.getData(scaleLevel);

        for(int i = 0; i < IxxMom.length; i++) {
            for(int j = 0; j < IxxMom.length; j++) {
                IxxMomAvg += IxxMom[i][j];
                IxyMomAvg += IxyMom[i][j];
                IyyMomAvg += IyyMom[i][j];
            }
        }
        // no need to divide, as we are only interested in ratios of eigenvalues anyways!
        return new SecondMomentMatrix(IxxMomAvg, IxyMomAvg, IxyMomAvg, IyyMomAvg);
    }

    /**
     * Returns the distribution of angles of structure tensors at a scale level.
     * <p>
     * @param scaleLevel Scale level to compute angle distribution.
     * <p>
     * @return Distribution of angles at a scale level.
     * <p>
     * @throws Exception
     */
    public double[] getAngleDistribution(int scaleLevel)
            throws Exception
    {
        double[][] IxxMom = this.IxxMoments.getData(scaleLevel);
        double[][] IxyMom = this.IxyMoments.getData(scaleLevel);
        double[][] IyyMom = this.IyyMoments.getData(scaleLevel);

        ArrayList<Double> values = new ArrayList<>();

        double radius = Math.sqrt(2.0d) * this.scalespaceData.getScalespaceParameters().getSigmaForScaleLevel(scaleLevel);
        for(int i = 0; i < IxxMom.length; i++) {
            for(int j = 0; j < IxxMom[0].length; j++) {
                SecondMomentMatrix smm
                                   = new SecondMomentMatrix(IxxMom[i][j], IxyMom[i][j], IxyMom[i][j], IyyMom[i][j]);
                Ellipse ell = new Ellipse(smm, radius, Ellipse.CIRCUMFERENCE_NORMALIZATION_MODE);
                values.add(ell.phi);
            }
        }
        double[] hist = this.createHist(values, 128);
        return hist;
    }

    /**
     * Computes a histogram of values with nbins number of nbins.
     * <p>
     * @param values Values to compute histogram.
     * @param nbins  Number of bins in histogram.
     * <p>
     * @return Histogram of values with nbins bins.
     */
    private double[] createHist(ArrayList<Double> values, int nbins)
    {
        double max = values.stream().max((a, b) -> a > b ? 1 : -1).get();
        double min = values.stream().min((a, b) -> a > b ? 1 : -1).get();

        // map values between 0 and nbins-1
        List<Double> normedValues = values.stream().map(a -> ((a - min) / (max - min)) * (nbins - 1)).collect(Collectors.toList());
        // collect histogram
        double[] hist = new double[nbins];
        for(int i = 0; i < normedValues.size(); i++) {
            int value = (int) Math.round(normedValues.get(i));
            hist[value]++;
        }
        return hist;
    }

    /**
     * Multiplies two arrays element wise.
     * <p>
     * @param d1 Array one.
     * @param d2 Array two.
     * <p>
     * @return Result of one*two.
     */
    protected double[][] multiply(double[][] d1, double[][] d2) {
        double[][] res = new double[d1.length][d1[0].length];

        for (int i = 0; i < d1.length; i++) {
            for (int j = 0; j < d1[0].length; j++) {
                res[i][j] = d1[i][j] * d2[i][j];
            }
        }
        return res;
    }

    public int getWidth()
    {
        return this.width;
    }

    public int getHeight()
    {
        return this.height;
    }

    public int getNumberOfScales()
    {
        return this.scalespaceData.getScaleCount();
    }

}
