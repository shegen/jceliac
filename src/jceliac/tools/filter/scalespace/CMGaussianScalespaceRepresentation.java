/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jceliac.tools.filter.scalespace;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.tools.filter.LowpassFilter;
import static jceliac.tools.filter.scalespace.LaplacianScalespaceRepresentation.DETECTOR_TYPE_ABSOLUTE_SUM;
import static jceliac.tools.filter.scalespace.LaplacianScalespaceRepresentation.DETECTOR_TYPE_SUM;
import jceliac.tools.math.MathTools;

/**
 *
 * @author shegen
 */
public class CMGaussianScalespaceRepresentation 
    extends ScalespaceRepresentation 
{

    public CMGaussianScalespaceRepresentation(double[][] data, 
                                              ScalespaceParameters parameters) 
    {
        super(data, parameters);
    }
    
    

    @Override
    public void computeMaxima() throws JCeliacGenericException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ScaleLevelIndexedDataSupplier<ArrayList<ScalespacePoint>> 
        computeMaxima1D() 
                throws JCeliacGenericException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void computeMinima() 
            throws JCeliacGenericException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ScaleLevelIndexedDataSupplier<ArrayList<ScalespacePoint>> 
        computeMinima1D() 
                throws JCeliacGenericException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void computeScalespaceRepresentation(int detectorType) 
            throws JCeliacGenericException 
    {
         for(int scaleLevel = 0;
            scaleLevel < this.scalespaceParameters.getNumberOfScales();
            scaleLevel++) 
        {
            // compute sigmas such that (sigma_n+1 / sigma_n) is equal for all sigmas
             
            CMGaussianFilter filter;
            filter = new CMGaussianFilter(scaleLevel);
            double[][] scaledData = filter.filterData(this.data);

            this.scalespaceDataSupplier.setData(scaleLevel, scaledData);
            this.normalizedLaplacianDataSupplier.setData(scaleLevel, scaledData);
        }
    }
    
}
