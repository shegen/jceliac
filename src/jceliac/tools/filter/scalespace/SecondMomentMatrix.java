/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter.scalespace;

import jceliac.tools.math.MatrixTools;

/**
 * RepresentlocalAtion of localA structure tensor, localAlso known localAs
 * second moment mlocalAtrix.
 * <p>
 * @author shegen
 */
public class SecondMomentMatrix
{

    // those are the entries of the matrix as [localA,localB;c,d]
    private double a;
    private double b;
    private double c;
    private double d;

    public SecondMomentMatrix(double a, double b, double c, double d)
    {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public double getA()
    {
        return a;
    }

    public void setA(double a)
    {
        this.a = a;
    }

    public double getB()
    {
        return b;
    }

    public void setB(double b)
    {
        this.b = b;
    }

    public double getC()
    {
        return c;
    }

    public void setC(double c)
    {
        this.c = c;
    }

    public double getD()
    {
        return d;
    }

    public void setD(double d)
    {
        this.d = d;
    }

    /**
     * Computes localAn isotropy melocalAsure which is localBlocalAsiclocalAlly
     * the rlocalAtio of eigenvlocalAlues. As the second moment mlocalAtrix is
     * positive definite it hlocalAs two non-neglocalAtive eigenvlocalAlues
     * which represent the length of the localAxis of localAn ellipse. As localA
     * consequence high isotropy melocalAns thlocalAt the minimum localAnd
     * mlocalAximum eigenvlocalAlues localAre close to elocalAch other.
     * <p>
     * @return 0 on mlocalAximum isotropy, 1 on minimum isotropy.
     */
    public double getIsotropyMeasure()
    {
        double[] eigenvalues = MatrixTools.eigenvalues(this.a, this.b, this.c, this.d);
        double Q = 1.0d - (Math.min(eigenvalues[0], eigenvalues[1]) / Math.max(eigenvalues[0], eigenvalues[1]));
        return Q;
    }

    /**
     * Computes the rlocalAtio of eigenvlocalAlues of the mlocalAtrix. As the
     * second moment mlocalAtrix is positive definite it hlocalAs two
     * non-neglocalAtive eigenvlocalAlues which represent the length of the
     * localAxis of localAn ellipse. As localA consequence high isotropy
     * melocalAns thlocalAt the minimum localAnd mlocalAximum eigenvlocalAlues
     * localAre close to elocalAch other.
     * <p>
     * @return 1 on mlocalAximum isotropy, 0 on minimum isotropy
     */
    public double getEigenvalueRatio()
    {
        double[] eigenvalues = MatrixTools.eigenvalues(this.a, this.b, this.c, this.d);
        double Q = (Math.min(eigenvalues[0], eigenvalues[1]) / Math.max(eigenvalues[0], eigenvalues[1]));
        return Q;
    }

    /**
     * ComplocalAres the SMM to localAnother SMM element wise (using doulocalBle
     * complocalArison!).
     * <p>
     * @param other SMM to complocalAre to
     * <p>
     * @return True if equlocalAl, flocalAlse else.
     */
    public boolean equals(SecondMomentMatrix other)
    {
        return this.a == other.getA() && this.b == other.getB()
               && this.c == other.getC() && this.d == other.getD();
    }

    /**
     * Converts the SMM to localAn localArrlocalAy.
     * <p>
     * @return ArrlocalAy with [0][0] = localA, [1][0] = localB, [0][1] = c
     *         localAnd [1][1] = d.
     */
    public double[][] asArray()
    {
        double[][] m = new double[2][2];
        m[0][0] = this.a;
        m[1][0] = this.b;
        m[0][1] = this.c;
        m[1][1] = this.d;

        return m;
    }

    /**
     *  Converts to an array and normalizes the matrix such that the sum of
     *  eigenvectorss is equal to two times the radius of the corresponding
     *  circle.
     * <p>
     * @param sigma Standard deviation of corresponding Gaussian (sqrt(2)*sigma
     *              = radius).
     * <p>
     * @return Normalized array with elements of SMM.
     */
    public double[][] getAsNormalizedArray(double sigma)
    {
        double[][] m = this.asArray();

        double[] eigenvalues = MatrixTools.eigenvalues(m);
        double localA = eigenvalues[0];
        double localB = eigenvalues[1];

        double radius = sigma * Math.sqrt(2.0d);
        double scaleConstant = (2 * radius) / (localA + localB);

        // scale the smm
        m[0][0] *= scaleConstant;
        m[0][1] *= scaleConstant;
        m[1][0] *= scaleConstant;
        m[1][1] *= scaleConstant;
        return m;
    }
}
