/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter.scalespace;

import java.io.IOException;
import jceliac.featureExtraction.affineScaleLBP.Ellipse;
import jceliac.JCeliacGenericException;
import jceliac.tools.math.MathTools;
import jceliac.gui.ImageViewerFrame;
import java.util.Collections;
import java.util.ArrayList;

/**
 * Basic scale-space representation.
 * <p>
 * @author shegen
 */
public abstract class ScalespaceRepresentation
{

    protected double[][] data;
    protected ScaleLevelIndexedDataSupplier<double[][]> scalespaceDataSupplier;
    protected ScaleLevelIndexedDataSupplier<double[][]> normalizedLaplacianDataSupplier;
    protected ArrayList<ScalespacePoint> maximas;
    protected ArrayList<ScalespacePoint> minimas;
    protected ScalespaceParameters scalespaceParameters;

    private final static int IGNORE_BORDER = 2; // xxx: make this dependent of filter size sometime

    public abstract void computeMaxima() throws JCeliacGenericException;

    public abstract ScaleLevelIndexedDataSupplier<ArrayList<ScalespacePoint>> computeMaxima1D()
            throws JCeliacGenericException;

    public abstract void computeMinima() throws JCeliacGenericException;

    public abstract ScaleLevelIndexedDataSupplier<ArrayList<ScalespacePoint>> computeMinima1D()
            throws JCeliacGenericException;

    public abstract void computeScalespaceRepresentation(int detectorType)
            throws JCeliacGenericException;

    public ScalespaceRepresentation(double[][] data, ScalespaceParameters parameters)
    {
        this.data = data;
        this.scalespaceParameters = parameters;
        this.scalespaceDataSupplier = new ScaleLevelIndexedDataSupplier<>(this.scalespaceParameters);
        this.normalizedLaplacianDataSupplier = new ScaleLevelIndexedDataSupplier<>(this.scalespaceParameters);
    }

    /**
     * Returns data supplier for the normalized Laplacian response.
     * <p>
     * @return data supplier for normalized Laplaican response.
     */
    public ScaleLevelIndexedDataSupplier<double[][]> getNormalizedLaplacianDataSupplier()
    {
        return this.normalizedLaplacianDataSupplier;
    }

    /**
     * Collects maximas that are spatial maximas and maximas between scales.
     * <p>
     * @throws JCeliacGenericException
     */
    protected void collectMaxima()
            throws JCeliacGenericException
    {
        this.maximas = new ArrayList<>();

        for(int scaleLevel = 1;
            scaleLevel < this.scalespaceParameters.getNumberOfScales() - 1;
            scaleLevel++) {

            double[][] scaleNminus = this.normalizedLaplacianDataSupplier.getData(scaleLevel - 1);
            double[][] scaleN = this.normalizedLaplacianDataSupplier.getData(scaleLevel);
            double[][] scaleNplus = this.normalizedLaplacianDataSupplier.getData(scaleLevel + 1);

            for(int i = 2; i < this.data.length - 2; i++) {
                for(int j = 2; j < this.data[0].length - 2; j++) {

                    double[] n1 = this.extractNeighborhood(scaleNminus, i, j);
                    double[] n2 = this.extractNeighborhoodWithoutCenter(scaleN, i, j);
                    double[] n3 = this.extractNeighborhood(scaleNplus, i, j);

                    double center = scaleN[i][j];

                    // at this point we check if the pixel is a local maximum or 
                    // a local minimum
                    if(isMaximum(center, n1) && isMaximum(center, n2) && isMaximum(center, n3)) {
                        ScalespacePoint maxima = new ScalespacePoint(scaleLevel, center, i, j, scalespaceParameters);
                        maximas.add(maxima);
                    }
                }
            }
        }
    }

    /**
     * Collects minimas that are spatial maximas and maximas between scales.
     * <p>
     * @throws JCeliacGenericException
     */
    protected void collectMinima()
            throws JCeliacGenericException
    {
        this.minimas = new ArrayList<>();

        for(int scaleLevel = 1;
            scaleLevel < this.scalespaceParameters.getNumberOfScales() - 1;
            scaleLevel++) {

            double[][] scaleNminus = this.normalizedLaplacianDataSupplier.getData(scaleLevel - 1);
            double[][] scaleN = this.normalizedLaplacianDataSupplier.getData(scaleLevel);
            double[][] scaleNplus = this.normalizedLaplacianDataSupplier.getData(scaleLevel + 1);

            for(int i = 2; i < this.data.length - 2; i++) {
                for(int j = 2; j < this.data[0].length - 2; j++) {

                    double[] n1 = this.extractNeighborhood(scaleNminus, i, j);
                    double[] n2 = this.extractNeighborhoodWithoutCenter(scaleN, i, j);
                    double[] n3 = this.extractNeighborhood(scaleNplus, i, j);

                    double center = scaleN[i][j];

                    // at this point we check if the pixel is a local maximum or 
                    // a local minimum
                    if(isMinimum(center, n1) && isMinimum(center, n2) && isMinimum(center, n3)) {
                        ScalespacePoint minima = new ScalespacePoint(scaleLevel, center, i, j, scalespaceParameters);
                        this.minimas.add(minima);
                    }
                }
            }
        }
    }

    /**
     * Collects maximas that are spatial maximas.
     * <p>
     * @param scaleLevel Scale level to collect maximas at.
     * <p>
     * @return List of local maximas.
     * <p>
     * @throws JCeliacGenericException
     */
    protected ArrayList<ScalespacePoint> collectMaxima1D(int scaleLevel)
            throws JCeliacGenericException
    {
        ArrayList<ScalespacePoint> m = new ArrayList<>();
        double[][] scaleN = this.normalizedLaplacianDataSupplier.getData(scaleLevel);

        for(int j = IGNORE_BORDER; j < this.data[0].length - IGNORE_BORDER; j++) {
            for(int i = IGNORE_BORDER; i < this.data.length - IGNORE_BORDER; i++) {

                double[] n1 = this.extractNeighborhoodWithoutCenter(scaleN, i, j);
                double center = scaleN[i][j];

                if(isMaximum(center, n1)) {
                    ScalespacePoint maxima = new ScalespacePoint(scaleLevel, center, i, j, this.scalespaceParameters);
                    m.add(maxima);
                }
            }
        }
        return m;
    }

    /**
     * Collects maximas that are spatial minimas.
     * <p>
     * @param scaleLevel Scale level to collect minimas at.
     * <p>
     * @return List of local minimas.
     * <p>
     * @throws JCeliacGenericException
     */
    protected ArrayList<ScalespacePoint> collectMinima1D(int scaleLevel)
            throws JCeliacGenericException
    {
        ArrayList<ScalespacePoint> m = new ArrayList<>();
        double[][] scaleN = this.normalizedLaplacianDataSupplier.getData(scaleLevel);

        for(int j = IGNORE_BORDER; j < this.data[0].length - IGNORE_BORDER; j++) {
            for(int i = IGNORE_BORDER; i < this.data.length - IGNORE_BORDER; i++) {

                double[] n1 = this.extractNeighborhoodWithoutCenter(scaleN, i, j);
                double center = scaleN[i][j];

                if(isMinimum(center, n1)) {
                    ScalespacePoint minimum = new ScalespacePoint(scaleLevel, center, i, j, this.scalespaceParameters);
                    m.add(minimum);
                }
            }
        }
        return m;
    }

    /**
     * Computes hessian at a signal position.
     * <p>
     * @param signal Signal in scalespace.
     * @param i      Position to compute hessian.
     * @param j      Position to compute hessian.
     * <p>
     * @return Hessian at position.
     */
    protected double[][] computeHessianAtKeypoint(double[][] signal, int i, int j)
    {
        double fxx = this.dxx(signal, i, j);
        double fxy = this.dxy(signal, i, j);
        double fyy = this.dyy(signal, i, j);

        double[][] hessian = new double[2][2];
        hessian[0][0] = fxx;
        hessian[0][1] = fxy;
        hessian[1][0] = fxy;
        hessian[1][1] = fyy;
        return hessian;
    }

    /**
     * Computes if a keypoint is an edge, see the SIFT paper on this, the ratio
     * of trace and determinant gives the ratio of eigenvalues.
     * <p>
     * @param signal signal in scale-space.
     * @param i      Position in signal.
     * @param j      Position in signal.
     * @param r      Ratio threshold.
     * <p>
     * @return True if keypoint is part of edge, false if not.
     */
    protected boolean isEdge(double[][] signal, int i, int j, double r)
    {
        double[][] hessian = this.computeHessianAtKeypoint(signal, i, j);
        double trace = MathTools.trace(hessian);
        double det = MathTools.determinant(hessian);

        // see the SIFT paper on this, the ratio of trace and determinant gives
        // the ratio of eigenvalues
        double traceSquared = trace * trace;
        double ratio = traceSquared / det;
        double threshold = (r + 1) * (r + 1) / r;
        return ratio > threshold;

    }

    /**
     * Checks if a point is a local maximum.
     * <p>
     * @param center Value of point.
     * @param n      Neighbors of point.
     * <p>
     * @return True if maximum, false else.
     */
    protected boolean isMaximum(double center, double[] n)
    {

        for(int i = 0; i < n.length; i++) {
            if(center < n[i]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if a point is a local minimum.
     * <p>
     * @param center Value of point.
     * @param n      Neighbors of point.
     * <p>
     * @return True if minimum, false else.
     */
    protected boolean isMinimum(double center, double[] n)
    {

        for(int i = 0; i < n.length; i++) {
            if(center > n[i]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if a point is either a local minimum or maximum.
     * <p>
     * @param center Value of point.
     * @param n      Neighbors of point.
     * <p>
     * @return True if minimum or maximum, false else.
     */
    protected boolean isMaximumOrMinimum(double center, double[] n)
    {
        boolean above = false;
        boolean below = false;

        for(int k = 0; k < n.length; k++) {
            if(center < n[k]) {
                if(above == false) {
                    below = true; // remember we were below
                } else { // we have been above before, now we are below, its neither maximum nor minimum
                    return false;
                }
            } else if(center >= n[k]) {
                if(below == false) {
                    above = true; // remember we were above
                } else { // we have been below before, now we are above, its neither maximum nor minimum
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Extracts the neighborhood of a signal position at (i,j) containing value
     * at (i,j). Treats the center as member of the neighborhood.
     * <p>
     * @param signal Signal in scale-space.
     * @param i      Position in signal.
     * @param j      Position in signal.
     * <p>
     * @return Values of neighborhood of position (i,j) and value at (i,j).
     */
    protected double[] extractNeighborhood(double[][] signal, int i, int j)
    {
        double[] neighborhood = new double[9];

        // the actual ordering does not matter here
        neighborhood[0] = signal[i - 1][j - 1]; // upper left
        neighborhood[1] = signal[i][j - 1];   // upper
        neighborhood[2] = signal[i + 1][j - 1]; // upper right
        neighborhood[3] = signal[i + 1][j];   // right
        neighborhood[4] = signal[i + 1][j + 1]; // lower right
        neighborhood[5] = signal[i][j + 1];   // lower
        neighborhood[6] = signal[i - 1][j + 1]; // lower left
        neighborhood[7] = signal[i - 1][j];   // left
        neighborhood[8] = signal[i][j];     // center
        return neighborhood;
    }

    /**
     * Extracts the neighborhood of a signal position at (i,j).
     * <p>
     * @param signal Signal in scale-space.
     * @param i      Position in signal.
     * @param j      Position in signal.
     * <p>
     * @return Values of neighborhood of position (i,j).
     */
    protected double[] extractNeighborhoodWithoutCenter(double[][] signal, int i, int j)
    {
        double[] neighborhood = new double[8];

        // the actual ordering does not matter here
        neighborhood[0] = signal[i - 1][j - 1]; // upper left
        neighborhood[1] = signal[i][j - 1];   // upper
        neighborhood[2] = signal[i + 1][j - 1]; // upper right
        neighborhood[3] = signal[i + 1][j];   // right
        neighborhood[4] = signal[i + 1][j + 1]; // lower right
        neighborhood[5] = signal[i][j + 1];   // lower
        neighborhood[6] = signal[i - 1][j + 1]; // lower left
        neighborhood[7] = signal[i - 1][j];   // left
        return neighborhood;
    }

    /**
     * Compute the partial derivatives in x and then y at point i,j;
     * f(x+h,y+h)-f(x+h,y-h)-f(x-h,y+h)-f(x-h,y-h) / 4h*h.
     * <p>
     * @param signal Signal in scalespace.
     * @param i      Position in signal.
     * @param j      Position in signal.
     * <p>
     * @return Partial derivatives in x and then y at point (i,j).
     */
    protected double dxy(double[][] signal, int i, int j)
    {
        double d;

        d = (signal[i + 1][j + 1] - signal[i + 1][j - 1] - signal[i - 1][j + 1] + signal[i - 1][j - 1]) / 4;
        return d;
    }

    /**
     * Compute the partial derivatives in x and then x at point i,j;
     * <p>
     * @param signal Signal in scalespace.
     * @param i      Position in signal.
     * @param j      Position in signal.
     * <p>
     * @return Partial derivatives in x and then x at point (i,j).
     */
    protected double dxx(double[][] signal, int i, int j)
    {
        double d;

        d = (signal[i + 1][j] - 2 * signal[i][j] + signal[i - 1][j]) / 1; // divided by 4h^2 (h=0.5)
        return d;
    }

    /**
     * Compute the partial derivatives in y and then y at point i,j;
     * <p>
     * @param signal Signal in scalespace.
     * @param i      Position in signal.
     * @param j      Position in signal.
     * <p>
     * @return Partial derivatives in x and then x at point (i,j).
     */
    protected double dyy(double[][] signal, int i, int j)
    {
        double d;

        d = (signal[i][j + 1] - 2 * signal[i][j] + signal[i][j - 1]) / 1;
        return d;
    }

    public ArrayList<ScalespacePoint> getMaximas()
    {
        return maximas;
    }

    public ArrayList<ScalespacePoint> getMinimas()
    {
        return minimas;
    }

    /**
     * Visualizes maximas as circles, this will hang the process until user
     * input is read.
     * <p>
     * @param image Image to use for visualization.
     */
    public synchronized void visualizeMaximas(double[][] image)
    {

        double[][] redChannel = new double[image.length][image[0].length];

        for(int i = 0; i < redChannel.length; i++) {
            System.arraycopy(image[i], 0, redChannel[i], 0, redChannel[0].length);
        }

        for(ScalespacePoint tmp : this.maximas) {
            if(tmp.isValid() == true) {
                double featureRadius = this.scalespaceParameters.getSigmaForScaleLevel(tmp.getScaleLevel()) * Math.sqrt(2.0d);
                LaplacianVisualization.drawCircle(redChannel, tmp.getCoordX(), tmp.getCoordY(), featureRadius);
            }
        }
        ImageViewerFrame.createFrame(redChannel, image, image).setVisible(true);
        try {
            System.in.read();
        } catch(IOException e) {

        }
    }

    /**
     * Visualizes maximas as ellipses, this will hang the process until user
     * input is read.
     * <p>
     * @param image            Image to use for visualization.
     * @param structureTensors Structure tensors of signal in scale-space.
     * <p>
     * @throws jceliac.JCeliacGenericException
     * <p>
     */
    public synchronized void visualizeMaximasAsEllipse(double[][] image,
                                                       ScalespaceStructureTensor structureTensors)
            throws JCeliacGenericException
    {

        int count = 0;
        double[][] redChannel = new double[image.length][image[0].length];

        for(int i = 0; i < redChannel.length; i++) {
            System.arraycopy(image[i], 0, redChannel[i], 0, redChannel[0].length);
        }

        for(ScalespacePoint tmp : this.maximas) {

            if(tmp.isValid() == true) {
                SecondMomentMatrix smm = structureTensors.getStructureTensor(tmp.getCoordX(), tmp.getCoordY(), tmp.getScaleLevel());
                double radius = tmp.getSigma() * Math.sqrt(2.0d);
                Ellipse ell = new Ellipse(smm, radius, Ellipse.CIRCUMFERENCE_NORMALIZATION_MODE);
                if(ell.a < 0.5 || ell.b < 0.5) {
                    continue;
                }
                count++;
                if(count % 10 != 0) {
                    continue;
                }
                LaplacianVisualization.drawEllipse(redChannel, tmp.getCoordX(), tmp.getCoordY(), ell);
            }
        }
        ImageViewerFrame.createFrame(redChannel, image, image).setVisible(true);
        try {
            System.in.read();
        } catch(IOException e) {

        }
    }

    public ScaleLevelIndexedDataSupplier <double[][]> getScalespaceDataSupplier()
    {
        return this.scalespaceDataSupplier;
    }

    public ScalespaceParameters getScalespaceParameters()
    {
        return scalespaceParameters;
    }

    /**
     * Returns the n-largest maxima in terms of response.
     * <p>
     * @param n Number of maxima to query.
     * <p>
     * @return List of n-largets maxima in terms of response.
     */
    public ArrayList<ScalespacePoint> getNLargestMaxima(int n)
    {
        try {
        Collections.sort(this.maximas);
        }catch(java.lang.IllegalArgumentException e)
        {
            for(int i= 0; i < this.maximas.size(); i++) {
                if(Double.isNaN(this.maximas.get(i).getResponse())) {
                    int nan = 1;
                }
            }
            int blub = 0;
        }
        ArrayList<ScalespacePoint> largestN = new ArrayList<>();

        for(int i = 0; i < Math.min(n, this.maximas.size()); i++) {
            largestN.add(this.maximas.get(this.maximas.size() - i - 1));
        }
        return largestN;
    }

    public int getWidth()
    {
        return this.data.length;
    }

    public int getHeight()
    {
        return this.data[0].length;
    }
}
