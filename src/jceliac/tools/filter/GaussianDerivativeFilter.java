/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter;

import jceliac.JCeliacGenericException;
import jceliac.experiment.*;
import java.util.ArrayList;
import jceliac.logging.*;

/**
 * A gaussian derivative filter, as the convolution with a Gaussian hold
 * commutativity and assoziativity with the derivative operator we can either
 * use the derivative of a signal and the use Gaussian filtering or use the
 * derivative of the gaussian filter (could also use DoG approach); This
 * implements the derivatives of the Gaussian filter.
 * <p>
 * <p>
 * @author shegen
 */
public class GaussianDerivativeFilter
        extends DataFilter2D
{

    private IGaussianDensity gaussDx = new GaussianDX();
    private IGaussianDensity gaussDy = new GaussianDY();
    private IGaussianDensity gaussDxDx = new GaussianDXDX();
    private IGaussianDensity gaussDyDy = new GaussianDYDY();

    private double[] coefficientMatrixHorizontal;
    private double[] coefficientMatrixVertical;

    public static enum EDerivativeType
    {

        /**
         * First derivative in x.
         */
        DX,
        /**
         * First derivative in y.
         */
        DY,
        /**
         * Second derivative in x.
         */
        DXDX,
        /**
         * Second derivative in y.
         */
        DYDY
    }

    public GaussianDerivativeFilter(EDerivativeType type, int filterDimension, double sigma)
            throws Exception
    {

        switch(type) {
            case DX:
                this.coefficientMatrixHorizontal
                = this.computeSeparableHorizontalGaussianFilter(filterDimension, sigma, this.gaussDx);
                this.coefficientMatrixVertical
                = this.computeSeparableVerticalGaussianFilter(filterDimension, sigma, this.gaussDx);
                break;

            case DXDX:
                this.coefficientMatrixHorizontal
                = this.computeSeparableHorizontalGaussianFilter(filterDimension, sigma, this.gaussDxDx);
                this.coefficientMatrixVertical
                = this.computeSeparableVerticalGaussianFilter(filterDimension, sigma, this.gaussDxDx);
                break;

            case DY:
                this.coefficientMatrixHorizontal
                = this.computeSeparableHorizontalGaussianFilter(filterDimension, sigma, this.gaussDy);
                this.coefficientMatrixVertical
                = this.computeSeparableVerticalGaussianFilter(filterDimension, sigma, this.gaussDy);
                break;

            case DYDY:
                this.coefficientMatrixHorizontal
                = this.computeSeparableHorizontalGaussianFilter(filterDimension, sigma, this.gaussDyDy);
                this.coefficientMatrixVertical
                = this.computeSeparableVerticalGaussianFilter(filterDimension, sigma, this.gaussDyDy);
                break;
        }
    }

    @Override
    public double[][] filterData(double[][] data) throws JCeliacGenericException
    {
        if(this.coefficientMatrixHorizontal.length != this.coefficientMatrixVertical.length) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Filter Dimensions not supported!");
            throw new JCeliacGenericException("Filter Dimensions not supported");
        }
        if((this.coefficientMatrixHorizontal.length % 2) == 0) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Filter Dimensions must be odd!");
            throw new JCeliacGenericException("Filter Dimensions must be odd");
        }
        double[][] filteredData = convolveMirrorSeparable(data, coefficientMatrixHorizontal, 0);
        return convolveMirrorSeparable(filteredData, coefficientMatrixVertical, 1);
    }

    @Override
    public double[][] filterDataFFT(double[][] data)
            throws JCeliacGenericException
    {
        ConvolutionFFT convolution = new ConvolutionFFT();
        return convolution.convolveLinearFFT(data, this.coefficientMatrixHorizontal, this.coefficientMatrixVertical);
    }

    /**
     * Computes a separable horizontal gaussian filter.
     * <p>
     * @param filterDimension Spatial dimension of filter.
     * @param sigma           Standard deviation of gaussian.
     * @param gauss           Density function of the specific derivative of the
     *                        gaussian.
     * <p>
     * @return Seperable horizontal gaussian derivative filter.
     */
    private double[] computeSeparableHorizontalGaussianFilter(int filterDimension, double sigma,
                                                              IGaussianDensity gauss)
    {
        double center = (((double) (filterDimension - 1)) / 2.0d);
        double[] filter = new double[filterDimension];
        double sum = 0;

        for(double x = -center; x <= center; x++) {
            filter[(int) (x + center)] = gauss.computeHorizontal(x, sigma);
            sum += Math.abs(filter[(int) (x + center)]);
        }
        // normalize to 1
        for(double x = -center; x <= center; x++) {
            filter[(int) (x + center)] = (filter[(int) (x + center)] / sum);
        }
        return filter;
    }

    /**
     * Computes a separable vertical gaussian filter.
     * <p>
     * @param filterDimension Spatial dimension of filter.
     * @param sigma           Standard deviation of gaussian.
     * @param gauss           Density function of the specific derivative of the
     *                        gaussian.
     * <p>
     * @return Seperable vertical gaussian derivative filter.
     */
    private double[] computeSeparableVerticalGaussianFilter(int filterDimension, double sigma, IGaussianDensity gauss)
    {
        double center = (((double) (filterDimension - 1)) / 2.0d);
        double[] filter = new double[filterDimension];
        double sum = 0;

        for(double x = -center; x <= center; x++) {
            filter[(int) (x + center)] = gauss.computeVertical(x, sigma);
            sum += Math.abs(filter[(int) (x + center)]);
        }
        // normalize to 1
        for(double x = -center; x <= center; x++) {
            filter[(int) (x + center)] = (filter[(int) (x + center)] / sum);
        }
        return filter;
    }

    /**
     * Represents a gaussian density with respect to derivatives.
     * <p>
     */
    private interface IGaussianDensity
    {

        public double computeHorizontal(double x, double sigma);

        public double computeVertical(double y, double sigma);
    }

    private class GaussianDX
            implements IGaussianDensity
    {

        @Override
        public double computeHorizontal(double x, double sigma)
        {
            return -Math.exp((-(x * x)) / (2 * sigma * sigma)) * (x / (sigma * sigma));
        }

        @Override
        public double computeVertical(double y, double sigma)
        {
            return -Math.exp((-(y * y)) / (2 * sigma * sigma));
        }
    }

    private class GaussianDY
            implements IGaussianDensity
    {

        @Override
        public double computeHorizontal(double x, double sigma)
        {
            return -Math.exp((-(x * x)) / (2 * sigma * sigma));
        }

        @Override
        public double computeVertical(double y, double sigma)
        {
            return -Math.exp((-(y * y)) / (2 * sigma * sigma)) * (y / (sigma * sigma));
        }
    }

    private class GaussianDXDX
            implements IGaussianDensity
    {

        @Override
        public double computeHorizontal(double x, double sigma)
        {
            return Math.exp((-(x * x)) / (2 * sigma * sigma)) * (((x * x) / (sigma * sigma * sigma * sigma)) - ((1 / (sigma * sigma))));
        }

        @Override
        public double computeVertical(double y, double sigma)
        {
            return Math.exp((-(y * y)) / (2 * sigma * sigma));
        }
    }

    private class GaussianDYDY
            implements IGaussianDensity
    {

        @Override
        public double computeHorizontal(double x, double sigma)
        {
            return Math.exp((-(x * x)) / (2 * sigma * sigma));
        }

        @Override
        public double computeVertical(double y, double sigma)
        {
            return Math.exp((-(y * y)) / (2 * sigma * sigma)) * (((y * y) / (sigma * sigma * sigma * sigma)) - ((1 / (sigma * sigma))));
        }
    }

    @Override
    public double filterContinuousDataPosition(double i, double j, double[][] data)
            throws JCeliacGenericException
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double filterDiscreteDataPosition(int i, int j, double[][] data)
            throws JCeliacGenericException
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
