/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter;

import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import jceliac.tools.transforms.fft.FastFourierTransform2D;
import jceliac.JCeliacGenericException;
import jceliac.tools.arrays.ArrayTools;
import jceliac.experiment.*;
import jceliac.logging.*;

/**
 * Implementation of the basis for a 2D data filter.
 * <p>
 * @author shegen
 */
public abstract class DataFilter2D
{

    public static final int BORDER_MIRROR = 1;

    public abstract double[][] filterData(double[][] signal) throws
            JCeliacGenericException;

    public abstract double[][] filterDataFFT(double[][] signal) throws
            JCeliacGenericException;

    public static double[][] convolve(double[][] signal, double[][] filter, int borderHandling)
            throws JCeliacGenericException
    {
        return DataFilter2D.convolve(signal, filter, borderHandling, false);
    }

    public static double[][] convolve(double[][] signal, double[] filter, int borderHandling)
            throws JCeliacGenericException
    {
        return DataFilter2D.convolve(signal, filter, borderHandling, false);
    }

    /**
     * Standard 2D convolution.
     * <p>
     * @param signal         Signal to convolve.
     * @param filter         2D Filter to use with convolution.
     * @param borderHandling Type of border handling.
     * @param round          Indicates wether the convolved values are rounded
     *                       or not.
     * <p>
     * @return Convolved signal.
     * <p>
     * @throws JCeliacGenericException
     */
    public static double[][] convolve(double[][] signal, double[][] filter, int borderHandling, boolean round)
            throws JCeliacGenericException
    {
        int filterDimension;
        int artificialMargin;

        filterDimension = filter.length;
        if(filter.length != filter[0].length) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Filter Dimensions not supported!");
            throw new JCeliacGenericException("Filter Dimensions not supported");
        }
        if((filterDimension % 2) == 0) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Filter Dimensions must be odd!");
            throw new JCeliacGenericException("Filter Dimensions must be odd");
        }
        artificialMargin = (int) (filterDimension / 2);

        switch(borderHandling) {
            case BORDER_MIRROR:
                return convolveMirror(signal, filter, round);

            default:
                Experiment.log(JCeliacLogger.LOG_ERROR, "ConvolutionFilter: Unknown Border Handling Type!");
                throw new JCeliacGenericException("ConvolutionFilter: Unknown Border Handling Type");
        }
    }

    /**
     * Standard 2D convolution.
     * <p>
     * @param signal         Signal to convolve.
     * @param filter         1D Filter to use with convolution.
     * @param borderHandling Type of border handling.
     * @param round          Indicates wether the convolved values are rounded
     *                       or not.
     * <p>
     * @return Convolved signal.
     * <p>
     * @throws JCeliacGenericException
     */
    public static double[][] convolve(double[][] signal, double[] filter, int borderHandling, boolean round)
            throws JCeliacGenericException
    {
        int filterDimension;

        filterDimension = filter.length;
        if((filterDimension % 2) == 0) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Filter Dimensions must be odd!");
            throw new JCeliacGenericException("Filter Dimensions must be odd");
        }
        switch(borderHandling) {
            case BORDER_MIRROR:
                double[][] filteredData = convolveMirrorSeparable(signal, filter, 0);
                return convolveMirrorSeparable(filteredData, filter, 1);

            default:
                Experiment.log(JCeliacLogger.LOG_ERROR, "ConvolutionFilter: Unknown Border Handling Type!");
                throw new JCeliacGenericException("ConvolutionFilter: Unknown Border Handling Type");
        }
    }

    /**
     * Convolves signal with filter using mirroring at the borders.
     * <p>
     * @param signal Signal to convolve with filter.
     * @param filter Filter to use with convolution.
     * @param round  Indicates wether the convolved values are rounded or not.
     * <p>
     * @return Convolved signal.
     */
    public static double[][] convolveMirror(double[][] signal, double[][] filter, boolean round)
    {
        int xIndex, yIndex;

        int deltaX = filter.length / 2;
        int deltaY = filter[0].length / 2;

        int xMax = signal.length - 1;
        int yMax = signal[0].length - 1;
        double[][] outArray = new double[signal.length][signal[0].length];

        for(int x = 0; x < signal.length; x++) {
            for(int y = 0; y < signal[0].length; y++) {
                // convolve
                double convSum = 0;
                for(int i = 0; i < filter.length; i++) {
                    for(int j = 0; j < filter[0].length; j++) {
                        xIndex = x - deltaX + i;
                        yIndex = y - deltaY + j;

                        if(xIndex < 0) // mirror x at y-axis
                        {
                            xIndex = -xIndex - 1;
                        }
                        if(yIndex < 0) // mirror y at x-axis
                        {
                            yIndex = -yIndex - 1;
                        }
                        if(xIndex > xMax) {
                            xIndex = (2 * xMax) - xIndex + 1; // this is "xMax- (x-xMax)"
                        }
                        if(yIndex > yMax) {
                            yIndex = (2 * yMax) - yIndex + 1; // this is "yMax- (y-yMax)"
                        }
                        convSum += filter[filter.length-i-1][filter[0].length-j-1] * signal[xIndex][yIndex];
                    }
                }
                if(round) {
                    outArray[x][y] = Math.round(convSum);
                } else {
                    outArray[x][y] = convSum;
                }
            }
        }
        return outArray;
    }

    /**
     * 
     * 
     * Separable convolution of a signal with a separable filter using mirroring
     * at the borders.
     * <p>
     * @param signal    Signal to convolve with filter.
     * @param filter    Filter to use with convolution.
     * @param round     Indicates wether the convolved values are rounded or
     *                  not.
     * @param dimension Indicates the signal dimension to use for convolution.
     * <p>
     * @return Convolved signal.
     */
    public static double[][] convolveMirrorSeparable(double[][] signal, double[] filter, int dimension)
    {
        int xIndex, yIndex;
        double[][] outArray = new double[signal.length][signal[0].length];

        int deltaX = filter.length / 2;
        int deltaY = filter.length / 2;

        // rows
        if(dimension == 0) {
            int xMax = signal.length - 1;

            for(int x = 0; x < signal.length; x++) {
                for(int y = 0; y < signal[0].length; y++) {
                    // convolve
                    double convSum = 0;
                    for(int i = 0; i < filter.length; i++) {
                        xIndex = x - deltaX + i;
                        yIndex = y;

                        if(xIndex < 0) // mirror x at y-axis
                        {
                            xIndex = -xIndex - 1; // mirror such that the first element is the last (matlab does it like that)
                        }else if(xIndex > xMax) {
                            xIndex = (2 * xMax) - xIndex + 1; // this is "xMax- (x-xMax)"
                        }
                        convSum += filter[filter.length-1-i] * signal[xIndex][yIndex];
                    }
                    outArray[x][y] = convSum;
                }
            }
        }
        // columns
        if(dimension == 1) {
            int yMax = signal[0].length - 1;

            for(int x = 0; x < signal.length; x++) {
                for(int y = 0; y < signal[0].length; y++) {
                    // convolve
                    double convSum = 0;
                    for(int i = 0; i < filter.length; i++) {
                        xIndex = x;
                        yIndex = y - deltaY + i;

                        if(yIndex < 0) // mirror y at x-axis
                        {
                            yIndex = -yIndex - 1;
                        }else if(yIndex > yMax) {
                            yIndex = (2 * yMax) - yIndex + 1; // this is "yMax- (y-yMax)"
                        }
                        convSum += filter[filter.length-1-i] * signal[xIndex][yIndex];
                    }
                    outArray[x][y] = convSum;
                }
            }
        }
        return outArray;
    }

    /**
     * Separable convolution of a signal with a separable filter using only the valid area of the 
     * signal, hence the dimensions of the output will be different from the signal input. 
     * <p>
     * @param signal    Signal to convolve with filter.
     * @param filter    Filter to use with convolution.
     * @param round     Indicates wether the convolved values are rounded or
     *                  not.
     * @param dimension Indicates the signal dimension to use for convolution.
     * <p>
     * @return Convolved signal.
     * @throws jceliac.JCeliacGenericException
     */
    public static double[][] convolveValidSeparable(double[][] signal, double[] filter,
                                                     boolean round, int dimension)
                throws JCeliacGenericException
    {
        int xIndex, yIndex;
        
        // filter dimension will be odd
        double[][] outArray; 
        int deltaX = filter.length / 2;
        int deltaY = filter.length / 2;
        int validMargin = filter.length / 2;
         
        // rows
        if(dimension == 0) {
            outArray = new double[signal.length-2*validMargin][signal[0].length];
            for(int x = 0; x < outArray.length; x++) {
                for(int y = 0; y < outArray[0].length; y++) {
                    // convolve
                    double convSum = 0;
                    for(int i = 0; i < filter.length; i++) {
                        xIndex = x + validMargin - deltaX + i;
                        yIndex = y;
                        convSum += filter[filter.length-1-i] * signal[xIndex][yIndex];

                    }
                    if(round) {
                        outArray[x][y] = Math.round(convSum);
                    } else {
                        outArray[x][y] = convSum;
                    }
                }
            }
        }// columns
        else if(dimension == 1) {
            outArray = new double[signal.length][signal[0].length-2*validMargin];
            for(int x = 0; x < outArray.length; x++) {
                for(int y = 0; y < outArray[0].length; y++) {
                    // convolve
                    double convSum = 0;
                    for(int i = 0; i < filter.length; i++) {
                        xIndex = x;
                        yIndex = y+validMargin - deltaY + i;
                        convSum += filter[filter.length-1-i] * signal[xIndex][yIndex];
                    }
                    if(round) {
                        outArray[x][y] = Math.round(convSum);
                    } else {
                        outArray[x][y] = convSum;
                    }
                }
            }
        }else {
            throw new JCeliacGenericException("Unknown dimension.");
        }
        return outArray;
    }
        
        
    /**
     * Circular convolution with FFT, usually not what is wanted as this does
     * not use overlapp add, it's not the linear convolution, use with care!!
     * <p>
     * @param signal Signal to convolve with filter.
     * @param filter Filter to convolve with signal.
     * <p>
     * @throws JCeliacGenericException
     * @return Signal convolved with filter.
     */
    private static double[][] convolveFFT(double[][] signal, double[][] filter)
            throws JCeliacGenericException
    {
        if(signal.length != signal[0].length) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Only Squared Signals are Supported in FFT2D.");
            throw new JCeliacGenericException();
        }
        double ld2 = Math.log(signal.length) / Math.log(2);
        if(ld2 != (int) ld2) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Only Signals with a Length of a Power of 2 are Supported in FFT2D.");
            throw new JCeliacGenericException();
        }
        FastFourierTransform2D fft2D = new FastFourierTransform2D(signal.length);
        double[][][] transformedData = fft2D.transform(fft2D.makeComplex(signal));
        double[][][] transformedFilter = fft2D.transform(fft2D.makeComplex(filter));

        // multiply point wise
        for(int i = 0; i < transformedData.length; i++) {
            for(int j = 0; j < transformedData[0].length; j++) {
                double real, image;
                real = (transformedData[i][j][0] * transformedFilter[i][j][0])
                       - (transformedData[i][j][1] * transformedFilter[i][j][1]);
                image = transformedData[i][j][1] * transformedFilter[i][j][0]
                        + transformedData[i][j][0] * transformedFilter[i][j][1];
                transformedData[i][j][0] = real;
                transformedData[i][j][1] = image;
            }
        }
        return fft2D.transformInverse(transformedData);
    }

    /**
     * Convolves two matrices using zero padding, returns only the valid area of
     * the convolved signal, as as consequence the size of signal will change!
     * <p>
     * @param signal Signal to convolve.
     * @param filter Filter to convolve the signal with.
     * <p>
     * @return Convolved signal.
     */
    public static double[][] convolveValid(double[][] signal, double[][] filter)
    {

        int xIndex, yIndex;

        int invalidX;
        int invalidY;

        // compute valid area
        invalidX = (filter.length - 1) / 2;
        invalidY = (filter[0].length - 1) / 2;

        double[][] outArray = new double[signal.length - 2 * invalidX][signal[0].length - 2 * invalidY];

        for(int x = invalidX; x < signal.length - invalidX; x++) {
            for(int y = invalidY; y < signal[0].length - invalidY; y++) {
                // convolve
                double convSum = 0;

                for(int i = 0; i < filter.length; i++) {
                    for(int j = 0; j < filter[0].length; j++) {
                        xIndex = x - invalidX + i;
                        yIndex = y - invalidY + j;

                        convSum += filter[filter.length-1-i][filter[0].length-1-j] * signal[xIndex][yIndex];
                    }
                }
                outArray[x - invalidX][y - invalidY] = convSum;
            }
        }
        return outArray;
    }
    
    /**
     * Convolves two matrices using zero padding, returns only the valid area of
     * the convolved signal, as as consequence the size of signal will change!
     * <p>
     * @param signal Signal to convolve.
     * @param filter Filter to convolve the signal with.
     * <p>
     * @return Convolved signal.
     */
    public static double[][] correlateValid(double[][] signal, double[][] filter)
    {

        int xIndex, yIndex;

        int invalidX;
        int invalidY;

        // compute valid area
        invalidX = (filter.length - 1) / 2;
        invalidY = (filter[0].length - 1) / 2;

        double[][] outArray = new double[signal.length - 2 * invalidX][signal[0].length - 2 * invalidY];

        for(int x = invalidX; x < signal.length - invalidX; x++) {
            for(int y = invalidY; y < signal[0].length - invalidY; y++) {
                // convolve
                double convSum = 0;

                for(int i = 0; i < filter.length; i++) {
                    for(int j = 0; j < filter[0].length; j++) {
                        xIndex = x - invalidX + i;
                        yIndex = y - invalidY + j;

                        convSum += filter[i][j] * signal[xIndex][yIndex];
                    }
                }
                outArray[x - invalidX][y - invalidY] = convSum;
            }
        }
        return outArray;

    }
    
    /**
     * Convolves signal with filter using mirroring at the borders.
     * <p>
     * @param signal Signal to convolve with filter.
     * @param filter Filter to use with convolution.
     * <p>
     * @return Convolved signal.
     */
    public static double[][] correlateMirror(double[][] signal, double[][] filter)
    {
        int xIndex, yIndex;

        int deltaX = filter.length / 2;
        int deltaY = filter[0].length / 2;

        int xMax = signal.length - 1;
        int yMax = signal[0].length - 1;
        double[][] outArray = new double[signal.length][signal[0].length];

        for(int x = 0; x < signal.length; x++) {
            for(int y = 0; y < signal[0].length; y++) {
                // convolve
                double convSum = 0;
                for(int i = 0; i < filter.length; i++) {
                    for(int j = 0; j < filter[0].length; j++) {
                        xIndex = x - deltaX + i;
                        yIndex = y - deltaY + j;

                        if(xIndex < 0) // mirror x at y-axis
                        {
                            xIndex = -xIndex - 1;
                        }
                        if(yIndex < 0) // mirror y at x-axis
                        {
                            yIndex = -yIndex - 1;
                        }
                        if(xIndex > xMax) {
                            xIndex = (2 * xMax) - xIndex + 1; // this is "xMax- (x-xMax)"
                        }
                        if(yIndex > yMax) {
                            yIndex = (2 * yMax) - yIndex + 1; // this is "yMax- (y-yMax)"
                        }
                        convSum += filter[i][j] * signal[xIndex][yIndex];
                    }
                }
                outArray[x][y] = convSum;
  
            }
        }
        return outArray;
    }
    
    

    /**
     * Scales the signal to the range of [0;255].
     * <p>
     * @param signal signal to scale.
     */
    public static void scaleData(double[][] signal)
    {
        double min, max;

        max = ArrayTools.max(signal);
        min = ArrayTools.min(signal);
        double width = (max - min);
        double scaleFactor = (255 / width); // scale it to max 255
        for(double[] tmp : signal) {
            for(int y = 0; y < signal[0].length; y++) {
                double k = tmp[y];
                double normalized = (k - min) * scaleFactor;
                tmp[y] = normalized;
            }
        }

    }

    public abstract double filterContinuousDataPosition(double i, double j, double[][] data)
            throws JCeliacGenericException;

    public abstract double filterDiscreteDataPosition(int i, int j, double[][] data)
            throws JCeliacGenericException;

}
