/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter;

import jceliac.JCeliacGenericException;
import java.util.ArrayList;

/**
 * Dummy filter that does nothing.
 * <p>
 * @author shegen
 */
public class NullFilter
        extends DataFilter2D
{

    @Override
    public double[][] filterData(double[][] data) throws JCeliacGenericException
    {
        return data;
    }

    @Override
    public double[][] filterDataFFT(double[][] data) throws
            JCeliacGenericException
    {
        return data;
    }

    @Override
    public double filterDiscreteDataPosition(int i, int j, double[][] data)
            throws JCeliacGenericException
    {
        return data[i][j];
    }

    @Override
    public double filterContinuousDataPosition(double i, double j, double[][] data)
            throws JCeliacGenericException
    {
        int fi = (int) i;
        int fj = (int) j;
        int ci = (int) Math.ceil(i);
        int cj = (int) Math.ceil(j);

        double d1 = this.filterDiscreteDataPosition(fi, fj, data);
        double d2 = this.filterDiscreteDataPosition(ci, fj, data);
        double d3 = this.filterDiscreteDataPosition(fi, cj, data);
        double d4 = this.filterDiscreteDataPosition(ci, cj, data);

        double tj = j - fj;
        double ti = i - fi;

        double invti = 1 - ti;
        double invtj = 1 - tj;

        double w1 = invti * invtj;
        double w2 = ti * invtj;
        double w3 = invti * tj;
        double w4 = ti * tj;

        return w1 * d1 + w2 * d2 + w3 * d3 + w4 * d4;
    }
}
