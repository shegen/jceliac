/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter;

import jceliac.JCeliacGenericException;
import jceliac.tools.data.*;
import static jceliac.tools.filter.ImageResize.EInterpolationType.NEAREST;

/**
 * Image resizer based on the matlab imresize function.
 * <p>
 * @author shegen
 */
public class ImageResize
{

    private interface IResizeKernel
    {

        public double[] map(double[] data) throws JCeliacGenericException;
        public int getKernelWidth();
    };

    private IResizeKernel kernel;
    private boolean antialiasing = true;
    private int kernelWidth;
    
    public static enum EInterpolationType
    {

        BILINEAR,
        NEAREST;
    };

    private class TriangleKernel
            implements IResizeKernel
    {

        @Override
        public double[] map(double[] xArray)
        {
            double[] f = new double[xArray.length];
            for(int i = 0; i < f.length; i++) {
                double tmp = (-1 <= xArray[i] && xArray[i] < 0) ? 1 : 0;
                double tmp2 = (0 <= xArray[i] && xArray[i] <= 1) ? 1 : 0;
                f[i] = (xArray[i] + 1) * tmp + (1 - xArray[i]) * tmp2;
            }
            return f;
        }

        @Override
        public int getKernelWidth()
        {
           return 2;
        }
        
        
    }

    

    private class BoxKernel
            implements IResizeKernel
    {

        @Override
        public double[] map(double[] data)
        {
            double[] f = new double[data.length];

            for(int i = 0; i < data.length; i++) {
                double tmp = -0.5 <= data[i] && data[i] < 0.5 ? 1 : 0;
                f[i] = tmp;
            }
            return f;
        }

        @Override
        public int getKernelWidth()
        {
           return 1;
        }
        
         
    }

    private EInterpolationType interpolationType;

    public ImageResize(EInterpolationType interpolationType)
    {
        this.interpolationType = interpolationType;
        switch(interpolationType) {
            case BILINEAR:
                this.kernel = new TriangleKernel();
                break;
            case NEAREST:
                this.antialiasing = false;
                this.kernel = new BoxKernel();
                break;
        }
    }

    /**
     * Resizes image to the specified dimensions.
     * <p>
     * @param image Input image to resize as double array.
     * @param outX  Output dimension X.
     * @param outY  Output dimension Y.
     * <p>
     * @return Resized image as double array.
     * <p>
     * @throws JCeliacGenericException
     */
    public double[][] resizeImage(double[][] image, int outX, int outY)
            throws JCeliacGenericException
    {
        double scaleX = outX / (double) image.length;
        double scaleY = outY / (double) image[0].length;

        double kernelWidthX = this.kernel.getKernelWidth();
        double kernelWidthY = this.kernel.getKernelWidth();
        if(scaleX < 1 && this.antialiasing) {
            kernelWidthX /= scaleX;  
        }
        if(scaleY < 1 && this.antialiasing) {
            kernelWidthY /= scaleY;  
        }
        
        double[] outIndicesX = this.generateOutputIndices(outX);
        double[] outIndicesY = this.generateOutputIndices(outY);
        double[][] inIndicesX = this.generateInputIndices(outIndicesX, scaleX, kernelWidthX);
        double[][] inIndicesY = this.generateInputIndices(outIndicesY, scaleY, kernelWidthY);
        double[][] weightsX = this.generateWeights(outIndicesX, scaleX, kernelWidthX);
        double[][] weightsY = this.generateWeights(outIndicesY, scaleY, kernelWidthY);

        double[][] normalizedWeightsX = this.normalizeRows(weightsX);
        double[][] normalizedWeightsY = this.normalizeRows(weightsY);

        if(this.interpolationType == NEAREST) {
            return this.resizeNearestNeighbor(image, inIndicesX, inIndicesY);
        }

        double[][] resizedImg = this.resizeDimension(image, inIndicesY, normalizedWeightsY, 1);
        return this.resizeDimension(resizedImg, inIndicesX, normalizedWeightsX, 0);
    }
    
    
        public double[][] resizeImage(double[][] image, double scaleFactor)
            throws JCeliacGenericException
        {
        int outX = (int)Math.round(image.length * scaleFactor);
        int outY = (int)Math.round(image[0].length * scaleFactor);
        
        double kernelWidthX = this.kernel.getKernelWidth();
        double kernelWidthY = this.kernel.getKernelWidth();
        if(scaleFactor < 1 && this.antialiasing) {
            kernelWidthX /= scaleFactor;  
        }
        if(scaleFactor < 1 && this.antialiasing) {
            kernelWidthY /= scaleFactor;  
        }
        
        double[] outIndicesX = this.generateOutputIndices(outX);
        double[] outIndicesY = this.generateOutputIndices(outY);
        double[][] inIndicesX = this.generateInputIndices(outIndicesX, scaleFactor, kernelWidthX);
        double[][] inIndicesY = this.generateInputIndices(outIndicesY, scaleFactor, kernelWidthY);
        double[][] weightsX = this.generateWeights(outIndicesX, scaleFactor, kernelWidthX);
        double[][] weightsY = this.generateWeights(outIndicesY, scaleFactor, kernelWidthY);

        double[][] normalizedWeightsX = this.normalizeRows(weightsX);
        double[][] normalizedWeightsY = this.normalizeRows(weightsY);

        if(this.interpolationType == NEAREST) {
            return this.resizeNearestNeighbor(image, inIndicesX, inIndicesY);
        }

        double[][] resizedImg = this.resizeDimension(image, inIndicesY, normalizedWeightsY, 1);
        return this.resizeDimension(resizedImg, inIndicesX, normalizedWeightsX, 0);
    }

    /**
     * Resizes a frame to the specified dimensions, changes the argument object.
     * <p>
     * <p>
     * @param frame Frame to change dimension, this object will be changed.
     * @param outX  Output dimension X.
     * @param outY  Output dimension Y.
     * <p>
     * @throws JCeliacGenericException
     */
    public void resizeFrame(Frame frame, int outX, int outY)
            throws JCeliacGenericException
    {

        frame.setBlueData(this.resizeImage(frame.getBlueData(), outX, outY));
        frame.setRedData(this.resizeImage(frame.getRedData(), outX, outY));
        frame.setGreenData(this.resizeImage(frame.getGreenData(), outX, outY));
        frame.setGrayData(this.resizeImage(frame.getGrayData(), outX, outY));
    }

    /**
     * Resizes a frame to the specified dimensions, returns a new frame with
     * according dimensions.
     * <p>
     * <p>
     * @param frame Frame to change dimension.
     * @param outX  Output dimension X.
     * @param outY  Output dimension Y.
     * <p>
     * @return Returns a new frame with the according dimensions based on the
     *         input frame.
     * <p>
     * @throws JCeliacGenericException
     */
    public Frame resizeFrameAndCopy(Frame frame, int outX, int outY)
            throws JCeliacGenericException
    {

        Frame resizedFrame = new Frame();

        resizedFrame.setSignalIdentifier(frame.getSignalIdentifier());
        resizedFrame.setFrameIdentifier(frame.getFrameIdentifier());
        resizedFrame.setBlueData(this.resizeImage(frame.getBlueData(), outX, outY));
        resizedFrame.setRedData(this.resizeImage(frame.getRedData(), outX, outY));
        resizedFrame.setGreenData(this.resizeImage(frame.getGreenData(), outX, outY));
        resizedFrame.setGrayData(this.resizeImage(frame.getGrayData(), outX, outY));
        return resizedFrame;
    }

    private double[][] resizeNearestNeighbor(double[][] data, double[][] indicesX, double[][] indicesY)
    {
        double[][] resizedData = new double[indicesX[0].length][indicesY[0].length];
        for(int i = 0; i < indicesX[0].length; i++) {
            for(int j = 0; j < indicesY[0].length; j++) {
                resizedData[i][j] = data[(int) indicesX[1][i]-1][(int) indicesY[1][j]-1];
            }
        }
        return resizedData;
    }

    /**
     * Generates output indices.
     * <p>
     * @param outLength Length of output dimension.
     * <p>
     * @return Output indices.
     */
    private double[] generateOutputIndices(int outLength)
    {
        double[] outIndices = new double[outLength];
        for(int index = 0; index < outLength; index++) {
            outIndices[index] = index + 1;
        }
        return outIndices;
    }

    /**
     * This figures out where the "support points" for the interpolation will
     * be.
     * <p>
     * Ported from Matlab Code imresize(), input-space coordinates, calculate
     * the inverse mapping such that 0.5 in output space maps to 0.5 in input
     * space, and 0.5+scale in output space maps to 1.5 in input space.
     * <p>
     * @param outputIndices Output indices.
     * @param scale         Scale factor of dimension.
     * <p>
     * @return Support points of interpolation.
     */
    private double[][] generateInputIndices(double[] outputIndices, double scale, double kernelWidth)
    {

        double[] left = new double[outputIndices.length];

        for(int index = 0; index < outputIndices.length; index++) {
            double u = ((outputIndices[index]) / scale) + (0.5 * (1.0 - 1.0 / scale));
            left[index] = Math.floor(u - (kernelWidth / 2.0d));
        }
        int P = (int) Math.ceil(kernelWidth) + 2;
        double outDim = outputIndices.length / scale;

        // The indices of the input pixels involved in computing the k-th output
        // pixel are in row k of the indices matrix.
        double[][] indices = new double[P][outputIndices.length];
        for(int index = 0; index < outputIndices.length; index++) {
            for(int pIndex = 0; pIndex < P; pIndex++) {
                double tmp = (left[index] + pIndex);
                if(tmp <= 1) {
                    tmp = 1;
                } else if(tmp >= outDim) {
                    tmp = outDim;
                }
                indices[pIndex][index] = tmp;
            }
        }
        return indices;
    }

    /**
     * Computes the weights for interpolation.
     * <p>
     * @param outputIndices Output indices.
     * @param scale         Scale of dimension.
     * <p>
     * @return Weights for interpolation.
     * <p>
     * @throws JCeliacGenericException
     */
    private double[][] generateWeights(double[] outputIndices, double scale, double kernelWidth)
            throws JCeliacGenericException
    {

        double[] left = new double[outputIndices.length];
        double[] u = new double[outputIndices.length];

        for(int index = 0; index < outputIndices.length; index++) {
            u[index] = (((outputIndices[index]) / scale)) + (0.5 * (1.0 - 1.0 / scale));
            left[index] = Math.floor(u[index] - (kernelWidth / 2.0d));
        }
        int P = (int) Math.ceil(kernelWidth) + 2;

        // The indices of the input pixels involved in computing the k-th output
        // pixel are in row k of the indices matrix.
        double[][] indices = new double[P][outputIndices.length];
        for(int index = 0; index < outputIndices.length; index++) {
            for(int pIndex = 0; pIndex < P; pIndex++) {
                double tmp = (left[index] + pIndex);
                indices[pIndex][index] = tmp;
            }
        }

        double[][] weights = new double[P][indices[0].length];
        for(int pIndex = 0; pIndex < weights.length; pIndex++) {
            double[] tmp = new double[weights[0].length];
            for(int index = 0; index < weights[0].length; index++) {
                tmp[index] = u[index] - indices[pIndex][index];
                
                if(this.antialiasing && scale < 1) {
                    // h = @(x) scale * kernel(scale * x); 
                    // this is used if antialiasing is used, which is the default
                    tmp[index] = tmp[index] * scale;
                }
            }
            double[] f = this.kernel.map(tmp);
            if(this.antialiasing && scale < 1) {
                // h = @(x) scale * kernel(scale * x);
                // this is used if antialiasing is used, which is the default
                for(int i = 0; i < f.length; i++) {
                    f[i] = scale * f[i];
                }
            }
            System.arraycopy(f, 0, weights[pIndex], 0, f.length);
        }
        return weights;
    }

    /**
     * Multiplies two arrays element wise.
     * <p>
     * @param x Array one.
     * @param y Array two.
     * <p>
     * @return Returns new array with result of one * two.
     * <p>
     * @throws JCeliacGenericException
     */
    private double[] mult(double[] x, double[] y)
            throws JCeliacGenericException
    {
        if(x.length != y.length) {
            throw new JCeliacGenericException("Cannot multiply (mismatching array dimensions)!");
        }
        double[] multArray = new double[x.length];
        for(int i = 0; i < x.length; i++) {
            multArray[i] = x[i] * y[i];
        }
        return multArray;
    }

    /**
     * Normalizes the rows of a matrix such that the sum of elements is 1.
     * <p>
     * @param x Matrix.
     * <p>
     * @return Normalized matrix.
     */
    private double[][] normalizeRows(double[][] x)
    {

        for(int colIndex = 0; colIndex < x[0].length; colIndex++) {
            double sum = 0;
            for(double[] rows : x) {
                sum += rows[colIndex];
            }
            if(sum != 1) {
                for(double[] rows : x) {
                    rows[colIndex] /= sum;
                }
            }
        }
        return x;
    }

    /**
     * Resizes a dimension of an image.
     * <p>
     * @param img     Image to resize.
     * @param indices Indices used for interpolation..
     * @param weights Weights used for interpolation.
     * @param dim     Dimension to resize.
     * <p>
     * @return Resized image data.
     */
    private double[][] resizeDimension(double[][] img, double[][] indices, double[][] weights, int dim)
    {

        if(dim == 0) // along second dimension
        {
            double[][] resizedImg = new double[indices[0].length][img[0].length];

            for(int index = 0; index < img[0].length; index++) {

                // compute the interpolated values for all pixels of this dimension
                for(int kIndex = 0; kIndex < indices[0].length; kIndex++) {
                    // the k-th row contains the pixels involved in computing the output
                    // pixel; same with the weights
                    double wsum = 0;
                    for(int i = 0; i < indices.length; i++) {
                        double weight = weights[i][kIndex];
                        int imgIndex = (int) indices[i][kIndex];
                        // we use matlab indexing to compute the weights and all indice, therefore subtract by 1 here to get java index
                        wsum += img[imgIndex - 1][index] * weight;
                    }
                    // cutoff out of range values, this will be small (< 1)
                    if(wsum > 255) {
                        wsum = 255;
                    }
                    if(wsum < 0) {
                        wsum = 0;
                    }
                    resizedImg[kIndex][index] = wsum;
                }

            }
            return resizedImg;
        }

        if(dim == 1) // along first dimension
        {
            double[][] resizedImg = new double[img.length][indices[0].length];
            for(double[] tmp : resizedImg) {
                for(int y = 0; y < resizedImg[0].length; y++) {
                    tmp[y] = 0;
                }
            }
            for(int index = 0; index < img.length; index++) {

                // compute the interpolated values for all pixels of this dimension
                for(int kIndex = 0; kIndex < indices[0].length; kIndex++) {
                    // the k-th row contains the pixels involved in computing the output
                    // pixel; same with the weights
                    double wsum = 0;
                    for(int i = 0; i < indices.length; i++) {
                        double weight = weights[i][kIndex];
                        int imgIndex = (int) indices[i][kIndex];
                        // we use matlab indexing to compute the weights and all indice, therefore subtract by 1 here to get java index
                        wsum += img[index][imgIndex - 1] * weight;
                    }
                    // cutoff out of range values, this will be small (< 1)
                    if(wsum > 255) {
                        wsum = 255;
                    }
                    if(wsum < 0) {
                        wsum = 0;
                    }
                    resizedImg[index][kIndex] = wsum;
                }

            }
            return resizedImg;
        }
        return null;
    }
}
