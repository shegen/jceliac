/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter;

import jceliac.JCeliacGenericException;
import jceliac.experiment.*;
import java.util.ArrayList;
import jceliac.logging.*;

/**
 * Implementation of sobel filter.
 * <p>
 * @author shegen
 */
public class SobelFilter
        extends DataFilter2D
{

    public final static int ORIENTATION_HORIZONTAL = 1;
    public final static int ORIENTATION_VERTICAL = 2;
    public final static int ORIENTATION_DIAGONAL = 3;
    public final static int ORIENTATION_MAGNITUDE = 4;
    // Gradient in X (differences on the X-axis)
    private final double[][] SOBEL_HORIZONTAL = {{1, 0, -1},
                                                 {2, 0, -2},
                                                 {1, 0, -1}};
    // Gradient in Y (differences on the Y-axis)
    private final double[][] SOBEL_VERTICAL = {{1, 2, 1},
                                               {0, 0, 0},
                                               {-1, -2, -1}};
    private final int sobelOrientation;

    public SobelFilter(int orientation)
    {
        this.sobelOrientation = orientation;

    }

    @Override
    public double[][] filterData(double[][] data) throws JCeliacGenericException
    {

        double[][] convolved;
        switch(this.sobelOrientation) {

            case ORIENTATION_HORIZONTAL:
                convolved = DataFilter2D.convolve(data, SOBEL_HORIZONTAL, DataFilter2D.BORDER_MIRROR);
                DataFilter2D.scaleData(convolved);
                return convolved;

            case ORIENTATION_VERTICAL:
                convolved = DataFilter2D.convolve(data, SOBEL_VERTICAL, DataFilter2D.BORDER_MIRROR);
                DataFilter2D.scaleData(convolved);
                return convolved;

            case ORIENTATION_DIAGONAL:
                return diagonalFilter(data);

            case ORIENTATION_MAGNITUDE:
                return magnitudeFilter(data);

            default:
                Experiment.log(JCeliacLogger.LOG_ERROR, "Unknown Sobel Filter Type!");
                throw new JCeliacGenericException("Unknown Sobel Filter Type");

        }
    }

    @Override
    public double[][] filterDataFFT(double[][] data) throws
            JCeliacGenericException
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Diagonal sobel filter is the average of the horizontal and vertical
     * response.
     * <p>
     * @param signal Signal to compute diagonal response.
     * <p>
     * @return Diagonal response of the sobel filter.
     * <p>
     * @throws JCeliacGenericException
     */
    private double[][] diagonalFilter(double[][] signal) throws
            JCeliacGenericException
    {
        double[][] horizData = DataFilter2D.convolve(signal, SOBEL_HORIZONTAL, DataFilter2D.BORDER_MIRROR);
        double[][] vertData = DataFilter2D.convolve(signal, SOBEL_VERTICAL, DataFilter2D.BORDER_MIRROR);

        DataFilter2D.scaleData(horizData);
        DataFilter2D.scaleData(vertData);

        double[][] diagonalData = new double[horizData.length][horizData[0].length];
        for(int x = 0; x < horizData.length; x++) {
            for(int y = 0; y < horizData[0].length; y++) {
                diagonalData[x][y] = Math.round((horizData[x][y] + vertData[x][y]) / 2);
            }
        }
        DataFilter2D.scaleData(diagonalData);
        return diagonalData;
    }

    /**
     * Magnitude sobel response of a signal if the magnitude of the horizontal
     * and vertical response.
     * <p>
     * @param signal Signal to compute Sobel response.
     * <p>
     * @return Magnitude of the sobel response.
     * <p>
     * @throws JCeliacGenericException
     */
    private double[][] magnitudeFilter(double[][] signal) throws
            JCeliacGenericException
    {
        double[][] horiz = DataFilter2D.convolve(signal, SOBEL_HORIZONTAL, DataFilter2D.BORDER_MIRROR);
        double[][] vert = DataFilter2D.convolve(signal, SOBEL_VERTICAL, DataFilter2D.BORDER_MIRROR);

        DataFilter2D.scaleData(horiz);
        DataFilter2D.scaleData(vert);
        double[][] magnitudeData = new double[signal.length][signal[0].length];
        for(int x = 0; x < magnitudeData.length; x++) {
            for(int y = 0; y < magnitudeData.length; y++) {
                double tmp = Math.sqrt((horiz[x][y] * horiz[x][y]) + (vert[x][y] * vert[x][y]));
                magnitudeData[x][y] = Math.round(tmp);
            }
        }
        DataFilter2D.scaleData(magnitudeData);
        return magnitudeData;
    }

    @Override
    public double filterContinuousDataPosition(double i, double j, double[][] data)
            throws JCeliacGenericException
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double filterDiscreteDataPosition(int i, int j, double[][] data)
            throws JCeliacGenericException
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
