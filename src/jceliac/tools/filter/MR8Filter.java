/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter;

import java.util.ArrayList;
import java.util.HashMap;
import jceliac.JCeliacGenericException;
import jceliac.tools.arrays.ArrayTools;

/**
 * Implements filtering with the MR8 Filter bank, ported based on the matlab
 * code from http://www.robots.ox.ac.uk/~vgg/research/texclass/filters.html.
 * <p>
 * @author shegen
 */
public class MR8Filter
{

    private final HashMap<Integer, double[][]> MR8FilterBank = new HashMap<>();
    private final int MAX_SUPPORT = 49;  // support of largest filter
    private final double NORIENT = 6.0d;
    private final int[] SCALEX = new int[]{1, 2, 4};
    private final int NEDGE = (int) (SCALEX.length * NORIENT);
    private final int NBAR = (int) (SCALEX.length * NORIENT);

    public MR8Filter()
            throws JCeliacGenericException
    {
        this.computeMR8FilterBank();
    }

    public double[][] getMR8Filter(int filterID)
    {
        return this.MR8FilterBank.get(filterID);
    }

    /**
     * Computes the MR8 filter bank.
     * 
     * @throws JCeliacGenericException 
     */
    private void computeMR8FilterBank()
            throws JCeliacGenericException
    {
        int filterID = 1;
        double[][] points = this.computeIndices(MAX_SUPPORT);
        for(int scaleIndex = 0; scaleIndex < SCALEX.length; scaleIndex++) {
            for(int orientation = 0; orientation < NORIENT; orientation++) {
                double angle = Math.PI * orientation / NORIENT;
                double[][] filter = this.makefilter(SCALEX[scaleIndex], 0, 1, points, angle);
                double[][] filter2 = this.makefilter(SCALEX[scaleIndex], 0, 2, points, angle);
                this.MR8FilterBank.put(filterID, filter);
                this.MR8FilterBank.put(filterID + NEDGE, filter2);
                filterID++;
            }
        }
        double[][] gaussFilter = LowpassFilter.computeGaussianMatrix(MAX_SUPPORT, 10);
        double[][] logFilter = this.computeLoGFilter();
        this.normalize(gaussFilter);
        this.normalize(logFilter);

        this.MR8FilterBank.put(NBAR + NEDGE + 1, gaussFilter);
        this.MR8FilterBank.put(NBAR + NEDGE + 2, logFilter);
    }

    /**
     * I feel like this should be implemented in an extra filter class, XXX: the
     * entire filter API design needs some cleanup.
     * <p>
     * @return
     */
    private double[][] computeLoGFilter()
            throws JCeliacGenericException
    {
        double sigma = 10.0d;
        double center = (((double) (MAX_SUPPORT - 1)) / 2.0d);
        double[][] kernel = new double[MAX_SUPPORT][MAX_SUPPORT];
        double sum = 0;

        for(double x = -center; x <= center; x++) {
            for(double y = -center; y <= center; y++) {
                kernel[(int) (x + center)][(int) (y + center)] = Math.exp(-(x * x + y * y) / (2 * sigma * sigma));
                sum += kernel[(int) (x + center)][(int) (y + center)];
            }
        }
        // normalize to 1
        for(double x = -center; x <= center; x++) {
            for(double y = -center; y <= center; y++) {
                kernel[(int) (x + center)][(int) (y + center)] = kernel[(int) (x + center)][(int) (y + center)] / sum;
            }
        }
        sum = 0;
        // compute laplacian = h1
        for(double x = -center; x <= center; x++) {
            for(double y = -center; y <= center; y++) {
                kernel[(int) (x + center)][(int) (y + center)] = kernel[(int) (x + center)][(int) (y + center)] * ((x * x + y * y - 2 * sigma * sigma) / (sigma * sigma * sigma * sigma));
                sum += kernel[(int) (x + center)][(int) (y + center)];
            }
        }
        double div = sum / ((double) MAX_SUPPORT * (double) MAX_SUPPORT);

        double[][] log = new double[kernel.length][kernel[0].length];
        for(int i = 0; i < log.length; i++) {
            for(int j = 0; j < log[0].length; j++) {
                log[i][j] = kernel[i][j] - div;
            }
        }
        return log;
    }

    private double[][] computeIndices(int maxSupport)
    {
        int size = (maxSupport - 1) / 2;
        double[][] ret = new double[2][(2 * size + 1) * (2 * size + 1)];

        int index = 0;
        for(int i = -size; i <= size; i++) {
            for(int j = -size; j <= size; j++) {
                ret[0][index] = i;
                ret[1][index] = -j;
                index++;
            }
        }
        return ret;
    }

    private double[][] makefilter(double scale, int phaseX, int phaseY,
                                  double[][] points, double angle)
    {
        double[][] rotatedPoints = this.rotatePoints(angle, points);
        double[] gx = this.computeGauss1d(3 * scale, 0, rotatedPoints[0], phaseX);
        double[] gy = this.computeGauss1d(scale, 0, rotatedPoints[1], phaseY);
        double[][] g2d = this.reshapeAndNormalize(gx, gy);

        return g2d;
    }

    private double[][] rotatePoints(double angle, double[][] points)
    {
        double[][] ret = new double[points.length][points[0].length];
        double c = Math.cos(angle);
        double s = Math.sin(angle);

        for(int i = 0; i < points[0].length; i++) {
            double rotPointX = c * points[0][i] - s * points[1][i];
            double rotPointY = s * points[0][i] + c * points[1][i];
            ret[0][i] = rotPointX;
            ret[1][i] = rotPointY;
        }
        return ret;
    }

    private double[] computeGauss1d(double sigma, double mean, double[] points, int ord)
    {
        double[] normedPoints = new double[points.length];
        double[] g = new double[points.length];
        for(int i = 0; i < points.length; i++) {
            normedPoints[i] = points[i] - mean;
            normedPoints[i] = points[i] * points[i];
        }
        double variance = sigma * sigma;
        for(int i = 0; i < g.length; i++) {
            g[i] = Math.exp(-normedPoints[i] / (2.0d * variance)) / Math.sqrt(Math.PI * 2 * variance);
            switch(ord) {
                case 1:
                    g[i] = -g[i] * (points[i] / variance);
                    break;
                case 2:
                    g[i] = g[i] * ((normedPoints[i] - variance) / (variance * variance));
                    break;
            }
        }
        return g;
    }

    private double[][] reshapeAndNormalize(double[] gx, double[] gy)
    {
        int size = (int) Math.sqrt(gx.length);
        double[][] g2d = new double[size][size];
        for(int index = 0; index < gx.length; index++) {
            int i = index / g2d.length;
            int j = index % g2d.length;
            g2d[i][j] = gx[index] * gy[index];
        }
        this.normalize(g2d);
        return g2d;
    }

    private void normalize(double[][] g2d)
    {
        double mean = ArrayTools.mean(g2d);
        for(int i = 0; i < g2d.length; i++) {
            for(int j = 0; j < g2d[0].length; j++) {
                g2d[i][j] = g2d[i][j] - mean;
            }
        }
        double sum = ArrayTools.abssum(g2d);
        for(int i = 0; i < g2d.length; i++) {
            for(int j = 0; j < g2d[0].length; j++) {
                g2d[i][j] = g2d[i][j] / sum;
            }
        }
    }
    
    /**
     * Computes the mr8 filter response using correlation. 
     * 
     * @param data Input data to filter. 
     * @return Reshaped and reordered MR8 filte response. 
     * @throws JCeliacGenericException 
     */
    public double [][] computeMR8FilterResponse1D(double [][] data)
            throws JCeliacGenericException
    {
        ArrayList <double [][]> maxResponsesList = new ArrayList <>();
        for(int j = 1; j <= 6; j++) {
            // rotational invariance of the edge and bar filters is achieved by taking
            // the maximum response over all orientations
            double [][] maxResponses = null;
            for(int k = 1; k <= 6; k++) {
                int filterID = (j-1)*6+k;
                double [][] filter = this.getMR8Filter(filterID);
                double [][] filterResponse = DataFilter2D.correlateMirror(data, filter);
                
                filterResponse = ArrayTools.abs(filterResponse);
                if(k == 1) {
                    maxResponses = filterResponse;
                }else {
                    maxResponses = ArrayTools.max(maxResponses, filterResponse);
                }
            }
            maxResponsesList.add(maxResponses);  
        }
        
        double [][] gaussianResponse = DataFilter2D.correlateMirror(data, this.getMR8Filter(37));
        double [][] logResponse = DataFilter2D.correlateMirror(data, this.getMR8Filter(38));
        maxResponsesList.add(gaussianResponse);
        maxResponsesList.add(logResponse);
        
        return this.reshapeAndReorderMR8Response1d(maxResponsesList);
    }
    
    /**
     * Computes the mr8 filter response using correlation. 
     * 
     * @param data Input data to filter. 
     * @return Reshaped and reordered MR8 filte response. 
     * @throws JCeliacGenericException 
     */
    public double [][][] computeMR8FilterResponse2D(double [][] data)
            throws JCeliacGenericException
    {
        ArrayList <double [][]> maxResponsesList = new ArrayList <>();
        for(int j = 1; j <= 6; j++) {
            // rotational invariance of the edge and bar filters is achieved by taking
            // the maximum response over all orientations
            double [][] maxResponses = null;
            for(int k = 1; k <= 6; k++) {
                int filterID = (j-1)*6+k;
                double [][] filter = this.getMR8Filter(filterID);
                double [][] filterResponse = DataFilter2D.correlateMirror(data, filter);
                
                filterResponse = ArrayTools.abs(filterResponse);
                if(k == 1) {
                    maxResponses = filterResponse;
                }else {
                    maxResponses = ArrayTools.max(maxResponses, filterResponse);
                }
            }
            maxResponsesList.add(maxResponses);  
        }
        
        double [][] gaussianResponse = DataFilter2D.correlateMirror(data, this.getMR8Filter(37));
        double [][] logResponse = DataFilter2D.correlateMirror(data, this.getMR8Filter(38));
        maxResponsesList.add(gaussianResponse);
        maxResponsesList.add(logResponse);
        
        return this.reshapeAndReorderMR8Response2d(maxResponsesList);
    }
    
    private double [][] reshapeAndReorderMR8Response1d(ArrayList <double [][]> maxResponsesList)
    {
        double [][] ret = new double[maxResponsesList.size()][];
        int [] orderIndices = new int[] {7,6,0,2,4,1,3,5};
        int dstIndex = 0;
        for(int index : orderIndices) {
            double [] tmpReshaped = ArrayTools.reshape(maxResponsesList.get(index));
            ret[dstIndex] = tmpReshaped;
            dstIndex++;
        }
        return ret;
    }
    
    private double [][][] reshapeAndReorderMR8Response2d(ArrayList <double [][]> maxResponsesList)
    {
        double [][][] ret = new double[maxResponsesList.size()][][];
        int [] orderIndices = new int[] {7,6,0,2,4,1,3,5};
        int dstIndex = 0;
        for(int index : orderIndices) {
            ret[dstIndex] =  maxResponsesList.get(index);
            dstIndex++;
        }
        return ret;
    }
  
}
