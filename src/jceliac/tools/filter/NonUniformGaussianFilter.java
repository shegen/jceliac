/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter;

import org.apache.commons.math3.linear.RealVector;
import jceliac.JCeliacGenericException;
import jceliac.tools.math.MatrixTools;
import java.util.ArrayList;

/**
 * Implementation of a Non-Uniform Gaussian Filter.
 * <p>
 * @author shegen
 */
public class NonUniformGaussianFilter
        extends DataFilter2D
{

    private double[][] kernel;

    public NonUniformGaussianFilter(int maxdim, double sigma, double[][] normalizedSMM)
            throws Exception
    {
        this.kernel = this.computeNonUniformGaussian(maxdim, normalizedSMM);
    }

    protected NonUniformGaussianFilter() // for tests
    {

    }

    @Override
    public double[][] filterData(double[][] data) throws JCeliacGenericException
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double[][] filterDataFFT(double[][] data) throws
            JCeliacGenericException
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Computes best non uniform filter dimension.
     * <p>
     * @param maxdim        Maximum size of filter.
     * @param normalizedSMM Normalized second moment matrix.
     * <p>
     * @return Best non uniform filter dimension.
     * <p>
     * @throws JCeliacGenericException
     */
    public static int getBestNonuniformFilterDimension(int maxdim, double[][] normalizedSMM)
            throws JCeliacGenericException
    {
        if(maxdim % 2 == 0) {
            throw new JCeliacGenericException("Use only odd filter dimensions!");
        }
        double[][] m = normalizedSMM;
        double normfactor = 1 / (2 * Math.PI * Math.sqrt(MatrixTools.determinant(m)));
        RealVector[] eigenvectors = MatrixTools.eigenvectors(m);

        double[] vec1 = eigenvectors[0].toArray();
        double[] vec2 = eigenvectors[1].toArray();
        int maxdim1 = 0;
        int maxdim2 = 0;

        double mInv[][] = MatrixTools.inverse(m[0][0], m[0][1], m[1][0], m[1][1]);
        for(int lambda = 0; lambda <= maxdim; lambda++) {
            double i = vec1[0] * lambda;
            double j = vec1[1] * lambda;
            double exponent = (mInv[0][0] * i * i + 2 * mInv[0][1] * i * j + mInv[1][1] * j * j) / 2;
            double value = normfactor * Math.exp(-exponent);
            if(value < Math.pow(10.0d, -10)) {
                maxdim1 = lambda;
                break;
            }
        }
        for(int lambda = 0; lambda <= maxdim; lambda++) {
            double i = vec2[0] * lambda;
            double j = vec2[1] * lambda;
            double exponent = (mInv[0][0] * i * i + 2 * mInv[0][1] * i * j + mInv[1][1] * j * j) / 2;
            double value = normfactor * Math.exp(-exponent);
            if(value < Math.pow(10.0d, -10)) {
                maxdim2 = lambda;
                break;
            }
        }
        int bestFilterLen = (int) Math.max(maxdim1, maxdim2);
        if(bestFilterLen % 2 == 0) {
            bestFilterLen = bestFilterLen + 1;
        }
        if(bestFilterLen > maxdim) {
            return maxdim;
        } else {
            return bestFilterLen;
        }
    }

    /**
     * Computes a non uniform gaussian kernel.
     * <p>
     * @param dim           Spatial dimension of filter.
     * @param normalizedSMM Normalized second momment matrix.
     * <p>
     * @return Non uniform gaussian filter.
     * <p>
     * @throws JCeliacGenericException
     */
    private double[][] computeNonUniformGaussian(int dim, double[][] normalizedSMM)
            throws JCeliacGenericException
    {
        if(dim % 2 == 0) {
            throw new JCeliacGenericException("Use only odd filter dimensions!");
        }
        double[][] m = normalizedSMM;
        double normfactor = 1 / (2 * Math.PI * Math.sqrt(MatrixTools.determinant(m)));
        double[][] filterKernel = new double[dim][dim];
        int width = (dim - 1) / 2;
        double mInv[][] = MatrixTools.inverse(m[0][0], m[0][1], m[1][0], m[1][1]);

        double filterSum = 0;

        for(int i = 0; i < dim; i++) {
            for(int j = 0; j < dim; j++) {
                double x = -width + i;
                double y = -width + j;
                double exponent = (mInv[0][0] * x * x + 2 * mInv[0][1] * x * y + mInv[1][1] * y * y) / 2;
                // filterKernel[i][j] = normfactor * Math.exp(-exponent);
                filterKernel[i][j] = Math.exp(-exponent);
                filterSum += filterKernel[i][j];
            }
        }
        for(int i = 0; i < dim; i++) {
            for(int j = 0; j < dim; j++) {
                filterKernel[i][j] /= filterSum;
            }
        }
        // note, this kernel is roughly normalized, the sum is not smaller than 0.99
        // we do not normalize to 1 because in the context we use this is doesn't actually 
        // do anything useful
        return filterKernel;
    }

    public double[][] getKernel()
    {
        return this.kernel;
    }

    @Override
    public double filterDiscreteDataPosition(int i, int j, double[][] data)
            throws JCeliacGenericException
    {
        int filterWidth = this.kernel.length;
        int filterHeight = this.kernel[0].length;

        int startX = i - ((filterWidth - 1) / 2);
        int startY = j - ((filterHeight - 1) / 2);

        double ret = 0;
        for(int filterX = 0; filterX < filterWidth; filterX++) {
            for(int filterY = 0; filterY < filterHeight; filterY++) {
                ret += data[startX + filterX][startY + filterY] * this.kernel[filterX][filterY];
            }
        }
        return ret;
    }

    @Override
    public double filterContinuousDataPosition(double i, double j, double[][] data)
            throws JCeliacGenericException
    {
        int fi = (int) i;
        int fj = (int) j;
        int ci = (int) Math.ceil(i);
        int cj = (int) Math.ceil(j);

        double d1 = this.filterDiscreteDataPosition(fi, fj, data);
        double d2 = this.filterDiscreteDataPosition(ci, fj, data);
        double d3 = this.filterDiscreteDataPosition(fi, cj, data);
        double d4 = this.filterDiscreteDataPosition(ci, cj, data);

        double tj = j - fj;
        double ti = i - fi;

        double invti = 1 - ti;
        double invtj = 1 - tj;

        double w1 = invti * invtj;
        double w2 = ti * invtj;
        double w3 = invti * tj;
        double w4 = ti * tj;

        return w1 * d1 + w2 * d2 + w3 * d3 + w4 * d4;
    }
}
