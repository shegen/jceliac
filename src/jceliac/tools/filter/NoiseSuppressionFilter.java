/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter;

import jceliac.JCeliacGenericException;
import jceliac.tools.color.*;
import jceliac.tools.data.*;

/**
 * Suppresses sensor noise by gaussian blurring of the luminance component (LAB)
 * of an image. The difference is built and the image back converter to RGB.
 * Java implementation of my matlab code in noisesuppress(). Very simple
 * implementation, probably not very efficient.
 * <p>
 * @author shegen
 */
public class NoiseSuppressionFilter
{

    private Frame labFrame;

    public Frame filterData(Frame frame) throws Exception
    {

        if(frame.getColorspace() == Frame.COLORSPACE_RGB) {
            this.labFrame = FrameColorConverter.convertRBGtoLAB(frame);
        } else {
            this.labFrame = frame;
        }
        double[][] luminance = this.labFrame.getLuminanceData();
        this.labFrame.setLuminanceData(filterData(luminance));
        return FrameColorConverter.convertLABtoRGB(this.labFrame);
    }

    private double[][] filterData(double[][] data)
            throws JCeliacGenericException
    {
        LowpassFilter gaussian = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 7, 2);
        return gaussian.filterData(data);

    }
}
