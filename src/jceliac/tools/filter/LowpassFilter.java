/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter;

import jceliac.JCeliacGenericException;
import jceliac.tools.math.MathTools;
import jceliac.tools.data.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Implements two lowpass filters, which is a Gaussian filter and an Averaging
 * filter.
 * <p>
 * @author shegen
 */
public class LowpassFilter
        extends DataFilter2D
{

    public static enum EFilterType
    {

        FILTER_AVERAGE,
        FILTER_GAUSSIAN,
    };
    private double[][] coefficientMatrix;
    private double[] coefficientMatrix1D;
    private EFilterType filterType;

    // says that the width of a gaussian filter is chosen such that 0.95% of the original 
    // area are retained
    public static double GAUSSIAN_SUPPORT_AREA_PERCENTAGE = 0.95;

    public LowpassFilter(EFilterType filterType, int filterDimension)
            throws JCeliacGenericException
    {
        this.filterType = filterType;
        switch(filterType) {
            case FILTER_AVERAGE:
                this.coefficientMatrix = this.computeAverageMatrix(filterDimension);
                this.coefficientMatrix1D = this.computeAverageMatrix1D(filterDimension);
                break;
            default:
                throw new JCeliacGenericException("Can not create LowpassFilter!");
        }
    }

    public LowpassFilter(EFilterType filterType, int filterDimension, double sigma)
            throws JCeliacGenericException
    {
        this.filterType = filterType;
        switch(filterType) {
            case FILTER_GAUSSIAN:
                this.coefficientMatrix1D = this.computeGaussianMatrixWithAreaIntegration1D(filterDimension, sigma);
                //this.coefficientMatrix = this.computeGaussianMatrixWithAreaIntegration2D(filterDimension, sigma);
                break;
            default:
                throw new JCeliacGenericException("Can not create LowpassFilter!");
        }
    }

    /**
     * Filteres signal non separable.
     * <p>
     * @param signal Signal to convolve.
     * <p>
     * @return Convolved signal.
     * <p>
     * @throws JCeliacGenericException
     */
    public double[][] filterDataNonSeparable(double[][] signal)
            throws JCeliacGenericException
    {
        if(this.filterType == EFilterType.FILTER_GAUSSIAN) {  // do not round
            return DataFilter2D.convolve(signal, this.coefficientMatrix, DataFilter2D.BORDER_MIRROR, false);
        } else {
            return DataFilter2D.convolve(signal, this.coefficientMatrix, DataFilter2D.BORDER_MIRROR);
        }
    }

    @Override
    public double[][] filterDataFFT(double[][] signal)
            throws JCeliacGenericException
    {
        ConvolutionFFT convolution = new ConvolutionFFT();

        if(this.filterType == EFilterType.FILTER_GAUSSIAN) {  // do not round
            return convolution.convolveLinearFFT(signal, this.coefficientMatrix1D);
        } else {
            return convolution.convolveLinearFFT(signal, this.coefficientMatrix1D);
        }
    }

    @Override
    public double[][] filterData(double[][] signal)
            throws JCeliacGenericException
    {
        if(this.filterType == EFilterType.FILTER_GAUSSIAN) {  // do not round
            return DataFilter2D.convolve(signal, this.coefficientMatrix1D, DataFilter2D.BORDER_MIRROR, false);
        } else {
            return DataFilter2D.convolve(signal, this.coefficientMatrix1D, DataFilter2D.BORDER_MIRROR);
        }
    }

    /**
     * Filters a frame, returns a copy of the frame with filtered data.
     * <p>
     * @param frame Frame to filter.
     * <p>
     * @return Copy of frame with filtered data.
     * <p>
     * @throws JCeliacGenericException
     */
    public Frame filterData(Frame frame)
            throws JCeliacGenericException
    {
        Frame filteredFrame = new Frame(frame.getParentFile(), frame.getFrameIdentifier(), frame.getColorspace());
        if(filteredFrame.getColorspace() == Frame.COLORSPACE_RGB) {
            filteredFrame.setRedData(convolve(frame.getRedData(), this.coefficientMatrix1D, DataFilter2D.BORDER_MIRROR));
            filteredFrame.setGreenData(convolve(frame.getGreenData(), this.coefficientMatrix1D, DataFilter2D.BORDER_MIRROR));
            filteredFrame.setBlueData(convolve(frame.getBlueData(), this.coefficientMatrix1D, DataFilter2D.BORDER_MIRROR));
            filteredFrame.setGrayData(convolve(frame.getGrayData(), this.coefficientMatrix1D, DataFilter2D.BORDER_MIRROR));
        } else {
            filteredFrame.setLuminanceData(convolve(frame.getLuminanceData(), this.coefficientMatrix1D, DataFilter2D.BORDER_MIRROR));
            filteredFrame.setChromAData(convolve(frame.getChromAData(), this.coefficientMatrix1D, DataFilter2D.BORDER_MIRROR));
            filteredFrame.setChromBData(convolve(frame.getChromBData(), this.coefficientMatrix1D, DataFilter2D.BORDER_MIRROR));
        }
        return filteredFrame;
    }

    /**
     * Computes kernel for 2D averaging filter.
     * <p>
     * @param dimension Dimension of filter.
     * <p>
     * @return Averaging filter kernel.
     */
    private double[][] computeAverageMatrix(int dimension)
    {
        if(dimension == 3) {
            double[][] avgMatrix = new double[][]{{0.1111, 0.1111, 0.1111},
                                                  {0.1111, 0.1111, 0.1111},
                                                  {0.1111, 0.1111, 0.1111}
            };
            return avgMatrix;
        }
        double[][] avgMatrix = new double[dimension][dimension];
        double coeffs = 1.0d / (dimension * dimension);

        for(int i = 0; i < dimension; i++) {
            for(int j = 0; j < dimension; j++) {
                avgMatrix[i][j] = coeffs;
            }
        }
        return avgMatrix;
    }

    /**
     * Computes kernel for 1D averaging filter.
     * <p>
     * @param dimension Dimension of filter.
     * <p>
     * @return Averaging filter kernel.
     */
    private double[] computeAverageMatrix1D(int dimension)
    {
        double[] avgMatrix = new double[dimension];
        double coeffs = Math.sqrt(1.0d / (dimension)); // sqrt because we need a1D*a1D = 

        for(int i = 0; i < dimension; i++) {
            avgMatrix[i] = coeffs;
        }
        return avgMatrix;
    }

    /** Computes the width of a gaussian filter, such that an area of
     * massPercentage which is a percentage of the entire area is covered by the
     * filter. Erfinv is implemented using an approximation. This can be derived
     * solving the equation: 2*Integral_0^t e^(-x^2/(2*sigma^2)) =
     * massPercentage * Integral_-Inf^Inf e^(-x^2/(2*sigma^2))) -> sqrt(PI/2) *
     * sigma * erf(x/(sqrt(2)*sigma) = (P*sigma*sqrt(2*pi))/ 2;
     * <p>
     * <p>
     * @param massPercentage Percentage of the mass of the Gaussian that is
     *                       covered by the filter.
     * @param sigma          Standard deviation of the Gaussian function.
     * <p>
     * @return Spatial width of the corresponding filter.
     */
    public static double computeGaussianRadiusForSupport(double massPercentage, double sigma)
    {
        return MathTools.erfinv(massPercentage) * Math.sqrt(2.0d) * sigma;
    }

    
    private static HashMap <DoubleKey,Integer> erfLookup = new HashMap<>();
    
    /** Compute the radius of a gaussian filter computed using the
     * AreaIntegration method such that the smallest value in the filter is
     * smaller than the maximum error of the erfinv function.
     * <p>
     * <p>
     * @param maxDimension Maximum dimension of the filter.
     * @param sigma        Standard deviation of the Gaussian function.
     * <p>
     * @return The optimal spatial filter size.
     * <p>
     */
    public static int computeOptimalFilterSize(int maxDimension, double sigma)
    {
        int width = (int) (maxDimension / 2.0d);
        int optimalWidth = maxDimension;
        double threshold = 1.5d * Math.pow(10, -8);

        for(int value = 0; value <= width; value++) {
            double coeff = 0.5d * -(MathTools.erf(((double) value - 0.5d) / sigma) - MathTools.erf(((double) value + 0.5d) / sigma));
            if(coeff <= threshold) {
                optimalWidth = 2 * (value) - 1;
                break;
            }
        }
        return optimalWidth;
    }

    /** This uses a slower approach than using the exponential function directly
     * and is based on the areas of the gaussian between pixel boundaries, this
     * should be better for small filter sizes and small sigmas and is the
     * preferred method to compute gaussians.
     * <p>
     * @param dimension Dimension of Gaussian filter.
     * @param sigma     Standard deviation of Gaussian filter.
     * <p>
     * @return 2D Gaussian filter kernel.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public static final double[][] computeGaussianMatrixWithAreaIntegration2D(int dimension, double sigma)
            throws JCeliacGenericException
    {
        if(dimension % 2 == 0) {
            throw new JCeliacGenericException("Only Odd Filter Lengths should be used!");
        }
        double center = (((double) (dimension - 1)) / 2.0d);
        double[][] kernel = new double[dimension][dimension];

        for(double x = -center; x <= center; x++) {
            for(double y = -center; y <= center; y++) {
                kernel[(int) (x + center)][(int) (y + center)]
                = 0.25d * (MathTools.erf((x - 0.5d) / sigma) - MathTools.erf((x + 0.5d) / sigma))
                  * (MathTools.erf((y - 0.5d) / sigma) - MathTools.erf((y + 0.5d) / sigma));
            }
        }
        // is allready normalized
        return kernel;
    }

    /** This uses a slower approach than using the exponential function directly
     * and is based on the areas of the gaussian between pixel boundaries, this
     * should be better for small filter sizes and small sigmas and is the
     * preferred method to compute gaussians.
     * <p>
     * @param dimension Dimension of Gaussian filter.
     * @param sigma     Standard deviation of Gaussian filter.
     * <p>
     * @return 1D Gaussian filter kernel.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public static final double[] computeGaussianMatrixWithAreaIntegration1D(int dimension, double sigma)
            throws JCeliacGenericException
    {
        if(dimension % 2 == 0) {
            throw new JCeliacGenericException("Only Odd Filter Lengths should be used!");
        }

        double center = (((double) (dimension - 1)) / 2.0d);
        double[] kernel = new double[dimension];

        for(double x = -center; x <= center; x++) {
            kernel[(int) (x + center)] = 0.5d * -(MathTools.erf((x - 0.5d) / sigma) - MathTools.erf((x + 0.5d) / sigma));
        }
        // is allready normalized
        return kernel;
    }

    /**
     * Direct approach of sampling the exponential functino to compute a
     * Gaussian kernel, not as accurate as using the Area Integration method.
     * <p>
     * @param dimension Spatial dimension of the Gaussian filter.
     * @param sigma     Standard deviation of the Gaussian function.
     * <p>
     * @return 2D Gaussian kernel.
     * <p>
     */
    public static final double[][] computeGaussianMatrix(int dimension, double sigma)
    {
        double center = (((double) (dimension - 1)) / 2.0d);
        double[][] kernel = new double[dimension][dimension];
        double sum = 0;

        for(double x = -center; x <= center; x++) {
            for(double y = -center; y <= center; y++) {
                kernel[(int) (x + center)][(int) (y + center)] = Math.exp(-(x * x + y * y) / (2 * sigma * sigma));
                sum += kernel[(int) (x + center)][(int) (y + center)];
            }
        }
        // normalize to 1
        for(double x = -center; x <= center; x++) {
            for(double y = -center; y <= center; y++) {
                kernel[(int) (x + center)][(int) (y + center)] = (kernel[(int) (x + center)][(int) (y + center)] / sum);
            }
        }
        return kernel;
    }

    /**
     * Direct approach of sampling the exponential functino to compute a
     * Gaussian kernel, not as accurate as using the Area Integration method.
     * <p>
     * @param dimension Spatial dimension of the Gaussian filter.
     * @param sigma     Standard deviation of the Gaussian function.
     * <p>
     * @return 1D Gaussian kernel.
     * <p>
     */
    public static final double[] computeGaussianMatrix1D(int dimension, double sigma)
    {
        double center = (((double) (dimension - 1)) / 2.0d);
        double[] kernel = new double[dimension];
        double sum = 0;

        for(double x = -center; x <= center; x++) {
            kernel[(int) (x + center)] = Math.exp(-(x * x) / (2 * sigma * sigma));
            sum += kernel[(int) (x + center)];

        }
        // normalize to 1
        for(double x = -center; x <= center; x++) {
            kernel[(int) (x + center)] = (kernel[(int) (x + center)] / sum);
        }
        return kernel;
    }

    /**
     * Computes the convolution of the filter with a signal at a discrete signal
     * position.
     * <p>
     * @param i      Coordinate of dimension 1.
     * @param j      Coordinate of dimension 2.
     * @param signal Signal to convolve.
     * <p>
     * @return Convolved value at position (i,j).
     * <p>
     * @throws JCeliacGenericException
     */
    @Override
    public double filterDiscreteDataPosition(int i, int j, double[][] signal)
            throws JCeliacGenericException
    {
        int filterWidth = this.coefficientMatrix.length;
        int filterHeight = this.coefficientMatrix[0].length;

        int startX = i - ((filterWidth - 1) / 2);
        int startY = j - ((filterHeight - 1) / 2);

        double ret = 0;
        for(int filterX = 0; filterX < filterWidth; filterX++) {
            for(int filterY = 0; filterY < filterHeight; filterY++) {
                ret += signal[startX + filterX][startY + filterY] * this.coefficientMatrix[filterX][filterY];
            }
        }
        return ret;
    }

    @Override
    /**
     * Computes the convolution of the filter with a signal at a position which
     * is not a discrete position, uses bi-linear interpolation to compute the
     * correct values.
     * <p>
     * @param i      Coordinate of dimension 1.
     * @param j      Coordinate of dimension 2.
     * @param signal Signal to convolve.
     * <p>
     * @return Convolved value at position (i,j).
     * <p>
     * @throws JCeliacGeneralException
     */
    public double filterContinuousDataPosition(double i, double j, double[][] data)
            throws JCeliacGenericException
    {
        int fi = (int) i;
        int fj = (int) j;
        int ci = (int) Math.ceil(i);
        int cj = (int) Math.ceil(j);

        double d1 = this.filterDiscreteDataPosition(fi, fj, data);
        double d2 = this.filterDiscreteDataPosition(ci, fj, data);
        double d3 = this.filterDiscreteDataPosition(fi, cj, data);
        double d4 = this.filterDiscreteDataPosition(ci, cj, data);

        double tj = j - fj;
        double ti = i - fi;

        double invti = 1 - ti;
        double invtj = 1 - tj;

        double w1 = invti * invtj;
        double w2 = ti * invtj;
        double w3 = invti * tj;
        double w4 = ti * tj;

        return w1 * d1 + w2 * d2 + w3 * d3 + w4 * d4;
    }
}
