/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.filter;

/**
 * Represents a complex number, this should only be used for proof
 * implementations as using an Object data type is bad for performance.
 * <p>
 * @author shegen
 */
public class Complex
{

    public double re;
    public double im;
    public String hint;

    public Complex(double re, double im, String hint)
    {
        this.re = re;
        this.im = im;
        this.hint = hint;
    }

    public Complex(double re, double im)
    {
        this.re = re;
        this.im = im;
    }

    public Complex add(Complex b)
    {
        return new Complex(this.re + b.re, this.im + b.im);//, this.hint+ " "+b.hint);
    }

    public Complex subtract(Complex b)
    {
        return new Complex(this.re - b.re, this.im - b.im); //, this.hint+ " "+b.hint);
    }

    public Complex mult(Complex b)
    {
        double real = (this.re * b.re) - (this.im * b.im);
        double imaginary = (this.re * b.im) + (this.im * b.re);

        return new Complex(real, imaginary);//, this.hint+ " "+b.hint);
    }

    public Complex div(Complex b)
    {
        double real = ((this.re * b.re) + (this.im * b.im)) / ((b.re * b.re) + (b.im * b.im));
        double imaginary = ((this.im * b.re) - (this.re * b.im)) / ((b.re * b.re) + (b.im * b.im));

        return new Complex(real, imaginary); //, this.hint+ " "+b.hint);
    }

    public Complex conjugate()
    {
        return new Complex(this.re, -this.im);
    }

    @Override
    public String toString()
    {
        return this.re + " + " + this.im + "i   (" + this.hint + ")";
    }

}
