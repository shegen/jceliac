/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

import jceliac.JCeliacGenericException;
import java.io.FileFilter;
import jceliac.experiment.*;
import jceliac.logging.*;
import java.io.*;

/**
 * This class reads the data inside a directory.
 * <p>
 * @author shegen
 */
public class CachedDataReader
        extends DataReader
{

    private int fileIndex = 0;
    private final File[] files;
    private DataCache dataCache;
    private boolean useCache;

    public CachedDataReader(ClassLabeledFile baseDirectory, boolean useCache)
    {
        // ignore this reader and act as finished
        if(baseDirectory == null) {
            // mark as finished
            this.fileIndex = 1;
            this.files = new File[1];
        } else {
            this.classNumber = baseDirectory.getClassNumber();
            this.className = baseDirectory.getName();
            this.files = baseDirectory.listFiles(new SupportedFileFilter());
            if(useCache) {
                dataCache = new DataCache();
            }
            this.useCache = useCache;
        }

    }

    /**
     * Cleans up the cache.
     */
    @Override
    public void cleanup()
    {
        this.purgeCache();

    }

    /**
     * Purges the cache.
     */
    private void purgeCache()
    {
        this.dataCache = null;
        if(this.useCache) {
            this.dataCache = new DataCache();
        }
    }

    @Override
    public boolean hasNext()
    {
        // index starts at 0
        return fileIndex != files.length;
    }

    public String nextFileName()
    {
        String path = files[fileIndex].getAbsolutePath();
        fileIndex++;
        return path;
    }

    @Override
    public Object next()
    {
        try {
            DataSource newSource = null;
            String path = files[fileIndex].getAbsolutePath();
            fileIndex++;
            if(path.endsWith("png") || path.endsWith("PNG") || 
               path.endsWith("jpg") || path.endsWith("JPG")) 
            {
                if(useCache) {
                    if(dataCache.hit(files[fileIndex - 1])) {
                        return dataCache.retrieve(files[fileIndex - 1]);
                    } else {
                        newSource = new ImageDataSource();
                        newSource.openDataSource(path);
                        dataCache.insert(files[fileIndex - 1], newSource);
                    }
                } else {
                    newSource = new ImageDataSource();
                    newSource.openDataSource(path);
                }
            } else if(path.endsWith("mpg") || path.endsWith("MPG")) {
                // do not use caching for movies
                newSource = new VideoDataSource();
                newSource.openDataSource(path);
            } else if(path.endsWith("vob") || path.endsWith("VOB")) {
                // do not use caching for movies
                newSource = new VideoDataSource();
                newSource.openDataSource(path);
            } else {
                Experiment.log(JCeliacLogger.LOG_INFO, "Ignoring File of unknown Type: %s.", path);
            }
            return newSource;
        }catch(JCeliacGenericException e) {
            return null;
        }
    }

    @Override
    public void reset()
    {
        this.fileIndex = 0;
    }

    @Override
    public void remove()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private class SupportedFileFilter
            implements FileFilter
    {

        @Override
        public boolean accept(File pathname)
        {
            return pathname.getName().endsWith(".png")
                   || pathname.getName().endsWith(".PNG")
                   || pathname.getName().endsWith(".Png")
                   || pathname.getName().endsWith(".mpg")
                   || pathname.getName().endsWith(".MPG")
                   || pathname.getName().endsWith(".Mpg")
                   || pathname.getName().endsWith(".vob")
                   || pathname.getName().endsWith(".VOB")
                   || pathname.getName().endsWith(".Vob")
                   || pathname.getName().endsWith(".jpg")
                   || pathname.getName().endsWith(".Jpg")
                   || pathname.getName().endsWith(".jpeg")
                   || pathname.getName().endsWith(".JPEG");
        }
    }

    public int getNumberOfFiles()
    {
        return this.files.length;
    }
}
