/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

/**
 *
 * @author shegen
 */
public class PSNRInfo
{

    public double psnr;
    public int frameID;

    public PSNRInfo(double p, int i)
    {
        this.psnr = p;
        this.frameID = i;
    }
}
