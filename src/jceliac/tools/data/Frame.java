/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

import jceliac.JCeliacGenericException;
import jceliac.tools.cache.Cacheable;
import java.util.function.Supplier;
import java.io.ObjectOutput;
import com.xuggle.xuggler.*;
import java.io.IOException;
import java.io.ObjectInput;
import java.awt.image.*;
import java.io.File;
import jceliac.tools.math.MathTools;

/**
 *
 * This class represents a frame. This is either a single image in case of
 * images or a movie frame in case of movies.
 * <p>
 * @author shegen
 */
public class Frame
        implements Cacheable
{

    private String signalIdentifier;
    private int frameIdentifier;
    private double[][] greenData;
    private double[][] redData;
    private double[][] blueData;
    private double[][] grayData;
    private double[][] luminanceData;
    private double[][] chromAData;
    private double[][] chromBData;
    private final int colorspace;
    public static final int COLORSPACE_RGB = 1;
    public static final int COLORSPACE_LAB = 2;
    private int width = -1;
    private int height = -1;
    private BufferedImage bufferedImage;
    private IVideoPicture videoPicture;

    private double grayStd = Double.NaN;
    private double greenStd = Double.NaN;
    private double redStd = Double.NaN;
    private double blueStd = Double.NaN;

    public boolean columnFirstStored = true; // default
    private File parentFile;

    public Frame(File parentFile, int frameID)
    {
        this.signalIdentifier = parentFile.getName();
        this.parentFile = parentFile;
        this.frameIdentifier = frameID;
        this.colorspace = COLORSPACE_RGB;
    }

    public Frame(File parentFile, int frameID, int colorspace)
    {
        this.signalIdentifier = parentFile.getName();
        this.parentFile = parentFile;
        this.frameIdentifier = frameID;
        this.colorspace = colorspace;
    }

    /**
     * If this is set, we do not convert the picture until we need the data this
     * is an optimization: If frames are skipped by the FrameProcessingStrategy
     * we do not need to convert them which takes up a lot of time.
     * <p>
     * @param picture VideoPicture as returned by the MPEG decoder.
     */
    public void setVideoPicture(IVideoPicture picture)
    {
        this.videoPicture = picture;
    }

    public Frame()
    {
        this.colorspace = COLORSPACE_RGB;
    }

    public Frame(int colorspace)
    {
        this.colorspace = colorspace;
    }

    public double[][] getBlueData()
    {
        return blueData;
    }

    public double[][] getGrayData()
    {
        return grayData;
    }

    /**
     * Returns a color channel,
     * @param colorChannel 1 = red, 2 = green, 3 = blue
     * @return 
     * @throws jceliac.JCeliacGenericException
     */
    public double [][] getColorChannel(int colorChannel) 
            throws JCeliacGenericException
    {
        switch(colorChannel) {
            case DataSource.CHANNEL_RED:
                return this.getRedData();
            case DataSource.CHANNEL_GREEN:
                return this.getGreenData();
            case DataSource.CHANNEL_BLUE:
                return this.getBlueData();
            default:
                throw new JCeliacGenericException("Undefined color channel: "+colorChannel);
        }
    }
    
    /**
     * Returns a color channel,
     * <p>
     * @param colorChannel 1 = red, 2 = green, 3 = blue
     * <p>
     * @param data
     * @return
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public void setColorChannel(int colorChannel, double[][] data)
            throws JCeliacGenericException
    {
        switch(colorChannel) {
            case DataSource.CHANNEL_RED:
                this.setRedData(data);
                break;
            case DataSource.CHANNEL_GREEN:
                this.setGreenData(data);
                break;
            case DataSource.CHANNEL_BLUE:
                this.setBlueData(data);
                break;
            default:
                throw new JCeliacGenericException("Undefined color channel: " + colorChannel);
        }
    }
    
    
    public void setBlueData(double[][] blueData)
    {
        this.blueStd = Double.NaN;
        this.blueData = blueData;

        if(this.columnFirstStored) {
            this.width = blueData.length;
            this.height = blueData[0].length;
        } else {
            this.width = blueData[0].length;
            this.height = blueData.length;
        }
    }

    public void setGrayData(double[][] grayData)
    {
        this.grayStd = Double.NaN;
        this.grayData = grayData;
        if(this.columnFirstStored) {
            this.width = grayData.length;
            this.height = grayData[0].length;
        } else {
            this.width = grayData[0].length;
            this.height = grayData.length;
        }
    }

    public void setGreenData(double[][] greenData)
    {
        this.greenStd = Double.NaN;
        this.greenData = greenData;

        if(this.columnFirstStored) {
            this.width = greenData.length;
            this.height = greenData[0].length;
        } else {
            this.width = greenData[0].length;
            this.height = greenData.length;
        }

    }

    public void setRedData(double[][] redData)
    {
        this.redStd = Double.NaN;
        this.redData = redData;

        if(this.columnFirstStored) {
            this.width = redData.length;
            this.height = redData[0].length;
        } else {
            this.width = redData[0].length;
            this.height = redData.length;
        }

    }

    public void setData(BufferedImage image)
    {
        this.blueStd = Double.NaN;
        this.redStd = Double.NaN;
        this.greenStd = Double.NaN;
        this.grayStd = Double.NaN;

        this.bufferedImage = image;
        this.width = image.getWidth();
        this.height = image.getHeight();

    }

    public double[][] getGreenData()
    {
        if(this.greenData == null) { // find out what kind of data is set

        }
        return greenData;
    }

    public double[][] getRedData()
    {
        return redData;
    }

    public void setParentFile(File parentFile)
    {
        this.parentFile = parentFile;
    }

    public int getFrameIdentifier()
    {
        return frameIdentifier;
    }

    public void setFrameIdentifier(int frameIdentifier)
    {
        this.frameIdentifier = frameIdentifier;
    }

    public String getSignalIdentifier()
    {
        return signalIdentifier;
    }

    public void setSignalIdentifier(String signalIdentifier)
    {
        this.signalIdentifier = signalIdentifier;
    }

    public int getWidth()
    {
        return this.width;
    }

    public int getHeight()
    {
        return this.height;
    }

    public double[][] getChromAData()
    {
        return chromAData;
    }

    public void setChromAData(double[][] chromAData)
    {
        this.chromAData = chromAData;
        this.width = chromAData.length;
        this.height = chromAData[0].length;
    }

    public double[][] getChromBData()
    {
        return chromBData;
    }

    public void setChromBData(double[][] chromBData)
    {
        this.chromBData = chromBData;
        this.width = chromBData.length;
        this.height = chromBData[0].length;
    }

    public double[][] getLuminanceData()
    {
        return luminanceData;
    }

    public void setLuminanceData(double[][] luminanceData)
    {
        this.luminanceData = luminanceData;
        this.width = luminanceData.length;
        this.height = luminanceData[0].length;
    }

    public int getColorspace()
    {
        return colorspace;
    }

    /**
     * Compares frame with another frame pixel by pixel.
     * <p>
     * @param other Frame to compare to.
     * <p>
     * @return True if frames are equal, false else.
     */
    public boolean equals(Frame other)
    {
        // first check grayscale version
        double[][] oGray = other.getGrayData();
        if(arraysEqual(oGray, this.grayData) == false) {
            return false;
        }

        // check color channels
        double[][] oRed = other.getRedData();
        if(arraysEqual(oRed, this.redData) == false) {
            return false;
        }
        double[][] oGreen = other.getGreenData();
        if(arraysEqual(oGreen, this.getGreenData()) == false) {
            return false;
        }
        double[][] oBlue = other.getBlueData();
        return arraysEqual(oBlue, this.blueData) != false;
    }

    /**
     * Tests if two frames are almost equal.
     * <p>
     * @param other Frame to compare to.
     * <p>
     * @return
     */
    public boolean almostEquals(Frame other)
    {
        // first check grayscale version
        double[][] oGray = other.getGrayData();
        if(arraysAlmostEqual(oGray, this.grayData) == false) {
            return false;
        }

        // check color channels
        double[][] oRed = other.getRedData();
        if(arraysAlmostEqual(oRed, this.redData) == false) {
            return false;
        }
        double[][] oGreen = other.getGreenData();
        if(arraysAlmostEqual(oGreen, this.getGreenData()) == false) {
            return false;
        }
        double[][] oBlue = other.getBlueData();
        return arraysAlmostEqual(oBlue, this.blueData) != false;
    }

    /**
     * Compares to arrays for equality.
     * <p>
     * @param c1 Array one.
     * @param c2 Array two.
     * <p>
     * @return True if equal, else if not.
     */
    public boolean arraysEqual(double[][] c1, double[][] c2)
    {
        for(int i = 0; i < c1.length; i++) {
            for(int j = 0; j < c1[0].length; j++) {
                if(c1[i][j] != c2[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Compares to arrays for equality with an error margin of 10.
     * <p>
     * @param c1 Array one.
     * @param c2 Array two.
     * <p>
     * @return True if equal, else if not.
     */
    public boolean arraysAlmostEqual(double[][] c1, double[][] c2)
    {
        for(int i = 0; i < c1.length; i++) {
            for(int j = 0; j < c1[0].length; j++) {
                double tmp1 = c1[i][j];
                double tmp2 = c2[i][j];

                double diff = tmp1 - tmp2;

                if(Math.abs(diff) > 10) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Converts frame to buffered image.
     * <p>
     * @return Frame as buffered image.
     */
    public BufferedImage toImage()
    {

        int[] intImage = new int[this.width * this.height];

        // this sucks, but there is no other way to convert the arrays (at least i
        // do not know)
        for(int x = 0; x < this.width; x++) {
            for(int y = 0; y < this.height; y++) {

                if(this.columnFirstStored) {
                    int greenPixel = (int) greenData[x][y];
                    int redPixel = (int) redData[x][y];
                    int bluePixel = (int) blueData[x][y];
                    intImage[y * width + x] = (int) (255 << 24 | redPixel << 16 | greenPixel << 8 | bluePixel);
                } else {
                    int greenPixel = (int) greenData[y][x];
                    int redPixel = (int) redData[y][x];
                    int bluePixel = (int) blueData[y][x];
                    intImage[y * width + x] = (int) (255 << 24 | redPixel << 16 | greenPixel << 8 | bluePixel);
                }
            }
        }
        BufferedImage bImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        bImg.setRGB(0, 0, width, height, intImage, 0, width);
        return bImg;
    }

    /**
     * Crops a frame.
     * <p>
     * @param startX X-coordinate of upper left pixel.
     * @param startY Y-coordinate of upper left pixel.
     * @param dim    Dimension of cropped frame.
     */
    public void crop(int startX, int startY, int dim)
    {
        double[][] newGrayData = new double[dim][dim];
        double[][] newRedData = new double[dim][dim];
        double[][] newGreenData = new double[dim][dim];
        double[][] newBlueData = new double[dim][dim];

        for(int j = 0; j < dim; j++) {
            System.arraycopy(this.grayData[j + startX], startY, newGrayData[j], 0, dim);
            System.arraycopy(this.greenData[j + startX], startY, newGreenData[j], 0, dim);
            System.arraycopy(this.redData[j + startX], startY, newRedData[j], 0, dim);
            System.arraycopy(this.blueData[j + startX], startY, newBlueData[j], 0, dim);
        }
        this.greenData = newGreenData;
        this.grayData = newGrayData;
        this.redData = newRedData;
        this.blueData = newBlueData;
    }
    
    /**
     * Crops a frame.
     * <p>
     * @param startX X-coordinate of upper left pixel.
     * @param startY Y-coordinate of upper left pixel.
     * @param dim1   Dimension of cropped frame.  
     * @param dim2   Dimension of cropped frame. 
     */
    public void crop(int startX, int startY, int dim1, int dim2)
    {
        double[][] newGrayData = new double[dim1][dim2];
        double[][] newRedData = new double[dim1][dim2];
        double[][] newGreenData = new double[dim1][dim2];
        double[][] newBlueData = new double[dim1][dim2];

        for(int j = 0; j < dim1; j++) {
            System.arraycopy(this.grayData[j + startX], startY, newGrayData[j], 0, dim2);
            System.arraycopy(this.greenData[j + startX], startY, newGreenData[j], 0, dim2);
            System.arraycopy(this.redData[j + startX], startY, newRedData[j], 0, dim2);
            System.arraycopy(this.blueData[j + startX], startY, newBlueData[j], 0, dim2);
        }
        this.greenData = newGreenData;
        this.grayData = newGrayData;
        this.redData = newRedData;
        this.blueData = newBlueData;
    }

    public double[][] getColoredData()
    {

        double[][] colored = new double[this.getWidth()][this.getHeight()];

        if(this.colorspace == Frame.COLORSPACE_RGB) {
            if(this.redData != null) {
                for(int i = 0; i < this.getWidth(); i++) {
                    for(int j = 0; j < this.getHeight(); j++) {
                        int red = (int) redData[i][j];
                        int green = (int) greenData[i][j];
                        int blue = (int) blueData[i][j];

                        int curPixel = (blue | (green << 8) | (red << 16) | (255 << 24));
                        colored[i][j] = (double) curPixel;
                    }
                }
            } else if(this.grayData != null) {

                for(int i = 0; i < this.getWidth(); i++) {
                    for(int j = 0; j < this.getHeight(); j++) {
                        int gray = (int) grayData[i][j];

                        int curPixel = (gray | (gray << 8) | (gray << 16) | (255 << 24));
                        colored[i][j] = (double) curPixel;
                    }
                }
            }
        } else if(this.colorspace == Frame.COLORSPACE_LAB) {
            for(int i = 0; i < this.getWidth(); i++) {
                for(int j = 0; j < this.getHeight(); j++) {
                    int l = (int) luminanceData[i][j];
                    int a = (int) chromAData[i][j];
                    int b = (int) chromBData[i][j];

                    int curPixel = (l | (a << 8) | (b << 16) | (255 << 24));
                    colored[i][j] = (double) curPixel;
                }
            }
        }

        return colored;
    }

    /**
     * Converts a Buffered imate to a frame optimized!.
     * <p>
     * @param image          BufferedImage to convert to frame.
     * @param storeByColumns Boolean indicating how the memory layout of the
     *                       frame should be.
     * <p>
     * @return The converted frame.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public static Frame convertBufferedImageToFrameOptimized(BufferedImage image, boolean storeByColumns)
            throws JCeliacGenericException
    {
        double[][] greenData;
        double[][] redData;
        double[][] blueData;
        double[][] grayData;
        Frame frame = null;

        try {
            int w = image.getWidth(null);
            int h = image.getHeight(null);

            // the documentation is not totally clear here, it looks like the 0 channel is
            // always red, does not matter if its an RGB or BGR image
            double[] red1D = image.getRaster().getSamples(0, 0, w, h, 0, (double[]) null);
            double[] green1D = image.getRaster().getSamples(0, 0, w, h, 1, (double[]) null);
            double[] blue1D = image.getRaster().getSamples(0, 0, w, h, 2, (double[]) null);

            if(storeByColumns) {
                greenData = new double[w][h];
                redData = new double[w][h];
                blueData = new double[w][h];
                grayData = new double[w][h];
            } else {
                greenData = new double[h][w];
                redData = new double[h][w];
                blueData = new double[h][w];
                grayData = new double[h][w];
            }

            frame = new Frame();
            for(int i = 0; i < w; i++) {
                for(int j = 0; j < h; j++) {
                    int index = (j * w + i);

                    int red = (int) red1D[index];
                    int green = (int) green1D[index];
                    int blue = (int) blue1D[index];

                    /* Be cautios, if you do not round you will screw up the
                     * result. This is not totaly compliant to the Matlab code,
                     * rounding errors.
                     */
                    int gray = (int) ((red * 0.298936021293776 + green * 0.587043074451121 + blue * 0.114020904255103) + 0.5);

                    /* Unfortunately we cannot use arraycopy for this because
                     * due to the type arrays are constructed we would have y,x
                     * instead of x,y and needed another transpose which
                     * destroyed the gained performance.
                     */
                    if(storeByColumns) {
                        // store by column so that data[n] gives the n-th column
                        greenData[i][j] = green1D[index];
                        redData[i][j] = red1D[index];
                        blueData[i][j] = blue1D[index];
                        grayData[i][j] = (double) gray;
                    } else { // store by rows so that data[n] gives the n-th row
                        greenData[j][i] = green1D[index];
                        redData[j][i] = red1D[index];
                        blueData[j][i] = blue1D[index];
                        grayData[j][i] = (double) gray;
                        frame.setColumnFirstStored(false);
                    }
                }
            }
            frame.setBlueData(blueData);
            frame.setGreenData(greenData);
            frame.setRedData(redData);
            frame.setGrayData(grayData);

        } catch(Exception e) {
            throw new JCeliacGenericException(e);
        }
        return frame;
    }

    /**
     * Converts a buffered image to a frame, automatically stores by columns so
     * that data[n] gives the n-th column.
     * <p>
     * @param image Buffered image to convert.
     * <p>
     * @return Converted frame.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public static Frame convertBufferedImageToFrameOptimized(BufferedImage image)
            throws JCeliacGenericException
    {
        double[][] greenData;
        double[][] redData;
        double[][] blueData;
        double[][] grayData;
        Frame frame = null;

        try {
            int w = image.getWidth(null);
            int h = image.getHeight(null);

            // the documentation is not totally clear here, it looks like the 0 channel is
            // always red, does not matter if its an RGB or BGR image
            double[] red1D, green1D, blue1D;
            if(image.getType() == BufferedImage.TYPE_BYTE_GRAY) {
                red1D = image.getRaster().getSamples(0, 0, w, h, 0, (double[]) null);
                green1D = image.getRaster().getSamples(0, 0, w, h, 0, (double[]) null);
                blue1D = image.getRaster().getSamples(0, 0, w, h, 0, (double[]) null);
            } else {
                red1D = image.getRaster().getSamples(0, 0, w, h, 0, (double[]) null);
                green1D = image.getRaster().getSamples(0, 0, w, h, 1, (double[]) null);
                blue1D = image.getRaster().getSamples(0, 0, w, h, 2, (double[]) null);
            }

            greenData = new double[w][h];
            redData = new double[w][h];
            blueData = new double[w][h];
            grayData = new double[w][h];

            frame = new Frame();
            for(int i = 0; i < w; i++) {
                for(int j = 0; j < h; j++) {
                    int index = (j * w + i);

                    int red = (int) red1D[index];
                    int green = (int) green1D[index];
                    int blue = (int) blue1D[index];

                    /* Be cautios, if you do not round you will screw up the
                     * result. This is not totaly compliant to the Matlab code,
                     * rounding errors.
                     */
                    int gray = (int) ((red * 0.298936021293776 + green * 0.587043074451121 + blue * 0.114020904255103) + 0.5);

                    /* Unfortunately we cannot use arraycopy for this because
                     * due to the type arrays are constructed we would have y,x
                     * instead of x,y and needed another transpose which
                     * destroyed the gained performance.
                     */
                    // store by column so that data[n] gives the n-th column
                    greenData[i][j] = green1D[index];
                    redData[i][j] = red1D[index];
                    blueData[i][j] = blue1D[index];
                    grayData[i][j] = (double) gray;

                }
            }
            frame.setBlueData(blueData);
            frame.setGreenData(greenData);
            frame.setRedData(redData);
            frame.setGrayData(grayData);

        } catch(Exception e) {
            throw new JCeliacGenericException(e);
        }
        return frame;
    }

    public boolean isColumnFirstStored()
    {
        return columnFirstStored;
    }

    public void setColumnFirstStored(boolean columnFirstStored)
    {
        this.columnFirstStored = columnFirstStored;
    }

    // <editor-fold defaultstate="expand" desc="Implementation of the Cacheable interface.">
    @Override
    public int getConsumedMemorySize()
    {

        int bytes = 0;
        bytes += this.blueData.length * this.blueData[0].length * Double.BYTES;
        bytes += this.redData.length * this.redData[0].length * Double.BYTES;
        bytes += this.greenData.length * this.greenData[0].length * Double.BYTES;
        if(this.grayData != null) {
            bytes += this.grayData.length * this.grayData[0].length * Double.BYTES;
        }
        if(this.luminanceData != null) {
            bytes += this.luminanceData.length * this.luminanceData[0].length * Double.BYTES;
        }
        if(this.chromAData != null) {
            bytes += this.chromAData.length * this.chromAData[0].length * Double.BYTES;
        }
        if(this.chromBData != null) {
            bytes += this.chromBData.length * this.chromBData[0].length * Double.BYTES;
        }
        // ignore the strings, they won't be too much
        return bytes;
    }

    @Override
    public void writeExternal(ObjectOutput out)
            throws IOException
    {
        out.writeObject(this.redData);
        out.writeObject(this.greenData);
        out.writeObject(this.blueData);
        out.writeObject(this.grayData);

        out.writeObject(this.luminanceData);
        out.writeObject(this.chromAData);
        out.writeObject(this.chromBData);

        out.writeObject(this.signalIdentifier);
        out.writeObject(this.frameIdentifier);
        out.writeObject(this.columnFirstStored);
        out.writeObject(this.width);
        out.writeObject(this.height);

    }

    @Override
    public void readExternal(ObjectInput in)
            throws IOException, ClassNotFoundException
    {
        this.redData = (double[][]) in.readObject();
        this.greenData = (double[][]) in.readObject();
        this.blueData = (double[][]) in.readObject();
        this.grayData = (double[][]) in.readObject();

        this.luminanceData = (double[][]) in.readObject();
        this.chromAData = (double[][]) in.readObject();
        this.chromBData = (double[][]) in.readObject();

        this.signalIdentifier = (String) in.readObject();
        this.frameIdentifier = (Integer) in.readObject();
        this.columnFirstStored = (Boolean) in.readObject();
        this.width = (Integer) in.readObject();
        this.height = (Integer) in.readObject();

    }

    @Override
    public String computeHashCode(Supplier<String> s)
    {
        return s.get() + this.signalIdentifier + this.frameIdentifier;
    }
    //</editor-fold>

    public double getRedStd()
    {
        if(Double.isNaN(this.redStd)) {
            this.redStd = MathTools.stdev(this.redData);

        }
        return this.redStd;
    }

    public double getGreenStd()
    {
        if(Double.isNaN(this.greenStd)) {
            this.greenStd = MathTools.stdev(this.greenData);
        }
        return this.greenStd;
    }

    public double getBlueStd()
    {
        if(Double.isNaN(this.blueStd)) {
            this.blueStd = MathTools.stdev(this.blueData);
        }
        return this.blueStd;
    }

    public double getGrayStd()
    {
        if(Double.isNaN(this.grayStd)) {
            this.grayStd = MathTools.stdev(this.grayData);
        }
        return this.grayStd;
    }

    public File getParentFile()
    {
        return parentFile;
    }

}
