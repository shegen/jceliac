/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

import jceliac.video.MPEG2Decoder;
import java.io.*;

/**
 * Data source for videos.
 * <p>
 * @author shegen
 */
public class VideoDataSource
        extends DataSource
{

    private String absolutePath = null;
    private MPEG2Decoder decoder = null;

    @Override
    public void closeDataSource()
    {
        this.decoder.close();
    }

    @Override
    public ContentStream createContentStream()
    {
        return new VideoContentStream(this);
    }

    @Override
    public void openDataSource(String path)
    {
        String url;

        url = "file://" + path;
        this.decoder = new MPEG2Decoder(url);
        this.decoder.init();
        File f = new File(path);
        this.absolutePath = f.getAbsolutePath();
        setSignalIdentifier(f.getName());
    }

    @Override
    public Object getData()
    {
        return this.decoder;
    }

    @Override
    public String getAbsolutePath()
    {
        return this.absolutePath;
    }

    public void setAbsolutePath(String absolutePath)
    {
        this.absolutePath = absolutePath;
    }
}
