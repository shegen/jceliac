/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

import java.net.URI;
import java.io.*;

/**
 *
 * @author shegen
 */
public class ClassLabeledFile
        extends File
{

    private int classNumber;

    public ClassLabeledFile(URI uri)
    {
        super(uri);
    }

    public ClassLabeledFile(File parent, String child)
    {
        super(parent, child);
    }

    public ClassLabeledFile(String parent, String child)
    {
        super(parent, child);
    }

    public ClassLabeledFile(String pathname)
    {
        super(pathname);
    }

    public int getClassNumber()
    {
        return classNumber;
    }

    public void setClassNumber(int classNumber)
    {
        this.classNumber = classNumber;
    }
}
