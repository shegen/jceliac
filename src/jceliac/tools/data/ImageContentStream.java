/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

import jceliac.JCeliacGenericException;
import java.awt.image.BufferedImage;
import jceliac.experiment.*;
import jceliac.logging.*;

/**
 * Content stream for single images.
 * <p>
 * @author shegen
 */
public class ImageContentStream
        extends ContentStream
{

    private Frame currentFrame;
    private boolean frameAvailable = true;

    public ImageContentStream(DataSource source)
            throws JCeliacGenericException
    {
        super(source);

        BufferedImage image = (BufferedImage) source.getData();
        if(image == null) {
            this.frameAvailable = false;
            Experiment.log(JCeliacLogger.LOG_ERROR, "Could not open DataSource (%s)!", source.getSignalIdentifier());
        } else {
            this.currentFrame = Frame.convertBufferedImageToFrameOptimized(image);
            this.currentFrame.setSignalIdentifier(this.source.getAbsolutePath());
            this.currentFrame.setParentFile(this.source.parentFile);
            this.currentFrame.setFrameIdentifier(1);
        }
    }

    @Override
    public boolean hasNext()
    {
        return this.frameAvailable;
    }

    @Override
    public Frame next()
    { // extract data from Image

        this.frameAvailable = false;
        return currentFrame;

    }
}
