/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

import jceliac.JCeliacGenericException;
import jceliac.video.MPEG2Decoder;
import jceliac.video.*;
import java.util.*;

/**
 * Content stream for videos.
 * <p>
 * @author shegen
 */
public class VideoContentStream
        extends ContentStream
{

    private final int FRAMES_PER_SEQUENCE = 150;
    private FrameSequence currentSequence;
    private final MPEG2Decoder decoder;

    public VideoContentStream(DataSource source)
    {
        super(source);
        VideoDataSource mSource = (VideoDataSource) super.source;
        this.decoder = (MPEG2Decoder) mSource.getData();

    }

    @Override
    public boolean hasNext()
    {
        if(this.currentSequence != null) {
            if(this.currentSequence.hasNext()) {
                return true;
            }
        }
        try {
            do {
                this.currentSequence = prepareNextSequence();
            } while(this.currentSequence != null && this.currentSequence.getSequenceLength() == 0);
        } catch(JCeliacGenericException e) // done
        {
            return false;
        }
        if(this.currentSequence == null) {
            return false;
        }
        return this.currentSequence.hasNext();
    }

    private FrameSequence prepareNextSequence()
            throws JCeliacGenericException
    {
        FrameSequence sequence = new FrameSequence();
        Frame nextFrame;

        // decoder was closed, this signals the end of the stream
        if(this.decoder.isIsFinished()) {
            return null; // return empty sequence
        }
        try {
            for(int count = 0; count < FRAMES_PER_SEQUENCE; count++) {
                Frame frame = this.decoder.nextFrame();
                if(frame == null) {
                    this.decoder.close(); // we are done
                    break;
                } else {
                    nextFrame = frame;
                    nextFrame.setSignalIdentifier(source.getSignalIdentifier());
                    nextFrame.setFrameIdentifier(decoder.getCurrentFrameNumber());
                    sequence.addFrame(nextFrame);
                }
            }
        } catch(Exception e) {
            throw new JCeliacGenericException(e);
        }
        sequence.pruneAndFilter();
        return sequence;
    }

    @Override
    public Frame next()
    {
        return this.currentSequence.next();
    }

    public ArrayList<Frame> next(int num) throws Exception
    {
        ArrayList<Frame> frameSequence = new ArrayList<>();
        int count = 0;

        while(count < num) { // implicitly throw Exception on out of bounds
            frameSequence.add(this.currentSequence.next());
        }
        return frameSequence;
    }
}
