/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

/**
 * Data source for frames.
 * <p>
 * @author shegen
 */
public class FrameDataSource
        extends DataSource
{

    @Override
    public void closeDataSource()
    {
    }

    @Override
    public ContentStream createContentStream()
    {
        return new FrameContentStream(this);
    }

    @Override
    public void openDataSource(String path)
    {
    }

    @Override
    public String getAbsolutePath()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
