/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

import java.io.File;
import jceliac.JCeliacGenericException;

/**
 * DataSource is used to open and read a Medium (image, video, ...). It is used
 * represents a single signal of any kind and format (e.g. a single image).
 * <p>
 * @author shegen
 */
public abstract class DataSource
{

    public final static int CHANNEL_RED = (1);
    public final static int CHANNEL_GREEN = (2);
    public final static int CHANNEL_BLUE = (3);
    public final static int CHANNEL_GRAY = (4);
    protected Object data;
    protected String signalIdentifier;
    protected File parentFile;

    // Methods
    public abstract void openDataSource(String path) throws JCeliacGenericException;

    public abstract void closeDataSource();

    public abstract ContentStream createContentStream() throws JCeliacGenericException;

    public Object getData()
    {
        return this.data;
    }

    public void setData(Object data)
    {
        this.data = data;
    }

    public String getSignalIdentifier()
    {
        return signalIdentifier;
    }

    public void setSignalIdentifier(String signalIdentifier)
    {
        this.signalIdentifier = signalIdentifier;
    }

    public abstract String getAbsolutePath();
}
