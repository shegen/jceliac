/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

import jceliac.JCeliacGenericException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.*;

/**
 * Data source for images.
 * <p>
 * @author shegen
 */
public class ImageDataSource
        extends DataSource
{

    private String absolutePath;

    public ImageDataSource()
    {
    }

    @Override
    public ContentStream createContentStream()
            throws JCeliacGenericException
    {
        return new ImageContentStream(this);
    }

    @Override
    public void closeDataSource()
    {
        // nothing to do yet
    }

    @Override
    public void openDataSource(String path)
            throws JCeliacGenericException
    {

        try {
            File imageFile = new File(path);
            BufferedImage image = ImageIO.read(imageFile);
            this.absolutePath = imageFile.getAbsolutePath();
            super.data = image;
            super.signalIdentifier = imageFile.getName();
            super.parentFile = imageFile;
        } catch(IOException e) {
            throw new JCeliacGenericException(e);
        }
    }

    @Override
    public String getAbsolutePath()
    {
        return this.absolutePath;
    }
}
