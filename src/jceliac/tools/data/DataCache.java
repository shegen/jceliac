/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

import jceliac.experiment.*;
import jceliac.logging.*;
import java.util.*;

/**
 * Base class for data cache.
 * <p>
 * @author shegen
 */
public class DataCache
{

    private final HashMap<Object, Object> cacheData;

    public DataCache()
    {
        this.cacheData = new HashMap<>();
    }

    public boolean hit(Object identifier)
    {

        if(this.cacheData.containsKey(identifier)) {
            Experiment.log(JCeliacLogger.LOG_HHDEBUG, "Cache hit!");
            return true;
        }
        return false;
    }

    public Object retrieve(Object identifier)
    {
        return this.cacheData.get(identifier);
    }

    public void insert(Object identifier, Object data)
    {
        this.cacheData.put(identifier, data);
    }
}
