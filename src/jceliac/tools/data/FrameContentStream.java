/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

/**
 * Content stream delivering frames. 
 * <p>
 * @author shegen
 */
public class FrameContentStream
        extends ContentStream
{

    public FrameContentStream(DataSource source)
    {
        super(source);
    }
    private boolean next = true;

    @Override
    public boolean hasNext()
    {
        if(this.next == true) {
            this.next = false;
            return true;
        }
        return false;
    }

    @Override
    public Frame next()
    {
        return (Frame) super.source.getData();
    }
}
