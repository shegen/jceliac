/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.tools.data;

/**
 *
 * @author shegen
 */
public final class DArray2D 
{
    private final double [] data;
    private final int sizeX;
    
    public DArray2D(int dimX, int dimY)
    {
        this.sizeX = dimX;
        this.data = new double[dimX*dimY];
    }
    
    public final double get(int x, int y)
    {
        return this.data[x+y*this.sizeX];
    }
    
    public final void set(int x, int y, double value)
    {
        this.data[x+y*this.sizeX] = value;
    }
}
