/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

import java.util.*;

/**
 * Caches processed images and handles them as if they were simply read from the
 * files. This is used for example by the WTLBP method to avoid computing the
 * wavelet decompositions for each of the used sub-methods.
 * <p>
 * @author shegen
 */
public class ProcessedDataReader
        extends DataReader
{

    private final ArrayList<Frame> frames;
    private int currentIndex;

    public ProcessedDataReader()
    {
        this.frames = new ArrayList<>();
        this.currentIndex = 0;
    }

    public void addFrame(Frame f)
    {
        this.frames.add(f);
    }

    @Override
    public boolean hasNext()
    {
        return (this.currentIndex < this.frames.size());
    }

    @Override
    public Object next()
    {
        this.currentIndex++;
        DataSource d = new FrameDataSource();
        d.setData(this.frames.get(this.currentIndex - 1));
        return d;
    }

    @Override
    public void remove()
    {
        this.frames.remove(this.currentIndex);
    }

    @Override
    public void reset()
    {
        this.currentIndex = 0;
    }

    @Override
    public void cleanup()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
