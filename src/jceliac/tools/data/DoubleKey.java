/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

// 
import java.util.Comparator;

/**
 * We use this to hash the scaleExponents, this is simple as we interpret he
 * scaleExponent as an integer by multiplication with a large number simply
 * ignoring some part of the double, this suffices our needs in this case.
 * <p>
 * @author shegen
 */
public class DoubleKey
        implements Comparator<DoubleKey>
{

    public double value;

    public DoubleKey(double value)
    {
        this.value = value;
    }

    @Override
    public int hashCode()
    {
        if(Double.isNaN(this.value)) {
            return Integer.MIN_VALUE;
        }
        int hash = (int) (this.value * 10000000);
        return hash;
    }

    @Override
    public int compare(DoubleKey k1, DoubleKey k2)
    {
        if(k1.value == k2.value) {
            return 0;
        } else if(k1.value < k2.value) {
            return -1;
        } else {
            return 1;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        }
        if(getClass() != obj.getClass()) {
            return false;
        }
        final DoubleKey other = (DoubleKey) obj;
        return Double.doubleToLongBits(this.value) == Double.doubleToLongBits(other.value);
    }
}
