/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

import jceliac.experiment.InitializeFailedException;
import java.util.*;
import java.io.*;

/**
 * Reads the contents of directory and returns files labelled by their specific
 * class (the parent directory's name).
 * <p>
 * @author shegen
 */
public class ClassLabeledDirectoryReader
{

    /**
     * This method identifies the classes distinguished by subdirectories and
     * returns the base directory of each separate class.
     * <p>
     * @param path Base path of directory containing subdirectories with data.
     * <p>
     * @return A list of class labeled files.
     * <p>
     * @throws jceliac.experiment.InitializeFailedException If data directory is
     *                                                      invalid.
     */
    public final ArrayList<ClassLabeledFile> prepareBaseDirectories(String path)
            throws InitializeFailedException
    {
        ArrayList<ClassLabeledFile> baseDirectories = new ArrayList<>();
        try {
            ClassLabeledFile parentDirectory = new ClassLabeledFile(path);
            if(parentDirectory == null) {
                throw new InitializeFailedException("Data Directory(" + path + ") does not exist!");
            }
            if(parentDirectory.isDirectory() == false) {
                throw new InitializeFailedException("Data Directory (" + path + ") is not a Directory!");
            }
            ArrayList<ClassLabeledFile> root = new ArrayList<>();
            root.add(parentDirectory);
            collectSubdirectories(root, baseDirectories);
            return baseDirectories;
        } catch(InitializeFailedException e) {
            if(e instanceof InitializeFailedException) {
                throw e;
            }
            String message = e.getMessage();
            throw new InitializeFailedException((message == null ? "Unknown Error" : message));
        }
    }
    
 
    /**
     * Collects all sub-directorise of a base directory recursively.
     * <p>
     * @param directories     Used for recursion.
     * @param baseDirectories Base directory to collect sub-directories.
     */
    private void collectSubdirectories(ArrayList<ClassLabeledFile> directories, 
                                       ArrayList<ClassLabeledFile> baseDirectories)
    {
        FileFilter directoryFilter = new DirectoryFileFilter();
        ArrayList<ClassLabeledFile> subDirectories = new ArrayList<>();

        // done if no subdirectories are left
        if(directories.isEmpty()) {
            return;
        }
        for(int index = 0; index < directories.size(); index++) {
            ClassLabeledFile currentDirectory = directories.get(index);
            File[] childs = (File[]) currentDirectory.listFiles(directoryFilter);

            if(childs.length == 0) // no subdirectories, this is the base directory of a class
            {
                baseDirectories.add(currentDirectory);
            } else {
                for(File child : childs) {
                    subDirectories.add(new ClassLabeledFile(child.toURI()));
                }
            }
        }
        collectSubdirectories(subDirectories, baseDirectories);
    }

    private class DirectoryFileFilter
            implements FileFilter
    {

        @Override
        public boolean accept(File pathname)
        {
            return (pathname.isDirectory() == true);
        }
    }
}
