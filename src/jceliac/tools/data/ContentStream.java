/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

/**
 * Provides a stream of data contents.
 * <p>
 * @author shegen
 */
public abstract class ContentStream
{

    protected DataSource source;

    public ContentStream(DataSource source)
    {
        this.source = source;
    }

    public abstract boolean hasNext();

    public abstract Frame next(); // returns a single frame of data

    public void remove()
    { // unused
    }
}
