/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.tools.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.Distances;

/**
 * Implementation of a KD-Tree, be warned, KD-Trees do not work well in high
 * dimensions. 
 * @author shegen
 */
public class KDTree 
{
    private KDTreeNode root;
    private final static int MEDIAN_RANDOM_POINTS = 1000;
    private Random rand = new Random(263134129L);
    
    private class KDTreeNode 
    {
        public double [] elementData;
        public int dimensionality;
        
        public KDTreeNode left;
        public KDTreeNode right;
        public KDTreeNode root;

        public KDTreeNode()
        {
        }

        
        public KDTreeNode(double[] elementData)
        {
            this.elementData = elementData;
            this.dimensionality = this.elementData.length;
        }

        public KDTreeNode(double[] elementData, KDTreeNode left, KDTreeNode right, KDTreeNode root)
        {
            this.elementData = elementData;
            this.dimensionality = this.elementData.length;
            this.left = left;
            this.right = right;
            this.root = root;
        }
    }
    
    public KDTree(ArrayList <double []> elements)
    {
        ArrayList <KDTreeNode> nodes = new ArrayList <>();
        for(int index = 0; index < elements.size(); index++) {
             KDTreeNode node = new KDTreeNode(elements.get(index));
             nodes.add(node);
        }
        this.root = this.buildKDTree(nodes, 0); 
    }
    
    private KDTreeNode buildKDTree(ArrayList <KDTreeNode> nodes, int depth) 
    {
        int splitDimension = depth % nodes.get(0).dimensionality;
        KDTreeNode medianNode = this.randomlySelectMedian(nodes, splitDimension);
        
        ArrayList <KDTreeNode> leftSublist = new ArrayList <>();
        ArrayList <KDTreeNode> rightSublist = new ArrayList <>(); 
        for(int i = 0; i < nodes.size(); i++) {
            KDTreeNode node = nodes.get(i);
            if(node.equals(medianNode)) {
                continue;
            }
            if(node.elementData[splitDimension] < medianNode.elementData[splitDimension]) {
                leftSublist.add(node);
            }else {
                rightSublist.add(node);
            }
        }
        
        if(false == leftSublist.isEmpty()) {
            medianNode.left = this.buildKDTree(leftSublist, depth+1);
        }
        if(false == rightSublist.isEmpty()) {
            medianNode.right = this.buildKDTree(rightSublist, depth+1);
        }
        return medianNode;
    }
    
    /**
     * Heuristic method for finding an appropriate node for the splitting plane,
     * selects a random set of points and sorts the set, then finds the median. 
     * This should guarantee a fairly balanced kd-tree with minimal effort by sorting.
     * 
     * @param nodes
     * @return 
     */
    private KDTreeNode randomlySelectMedian(ArrayList <KDTreeNode> nodes, int splitDimension)
    {
        ArrayList <KDTreeNode> randomNodes;
        if(nodes.size() < KDTree.MEDIAN_RANDOM_POINTS) {
            randomNodes = new ArrayList <>(nodes);
        }else {
            randomNodes = new ArrayList <>();
            while(randomNodes.size() < KDTree.MEDIAN_RANDOM_POINTS)
            {
                int sourceIndex = Math.abs(this.rand.nextInt()) % nodes.size();
                KDTreeNode randomNode = nodes.get(sourceIndex);
                if(randomNodes.contains(randomNode)) {
                    continue;
                }
                randomNodes.add(randomNode);
            }
        }
        Collections.sort(randomNodes, 
                         (a,b) -> {
                                if(a.elementData[splitDimension] <= b.elementData[splitDimension]) {
                                    return -1;
                                }else {
                                    return 1;
                                }
                            });
        
        int medianIndex = randomNodes.size() / 2;
        return randomNodes.get(medianIndex);
    }
    
    private double bestDistance = Double.POSITIVE_INFINITY;
    private KDTreeNode best = null;
    
    
    public double [] nearest(double [] queryPoint)
            throws JCeliacGenericException
    {
        this.bestDistance = Double.POSITIVE_INFINITY;
        this.best = null;
        this.findKNN(queryPoint, this.root, 0);
        return best.elementData;
    }
    private int distanceComputed = 0;
    

    private void findKNN(double [] queryPoint, KDTreeNode subtreeRoot, int dim) 
            throws JCeliacGenericException
    {
       boolean left = false;
       if(subtreeRoot == null) {
           return;
       }
       int axis = dim % subtreeRoot.dimensionality;
       this.distanceComputed++;
       double distance = Distances.euclideanDistance(queryPoint, subtreeRoot.elementData);
       if(distance < this.bestDistance) {
           this.bestDistance = distance;
           this.best = subtreeRoot;
       }
       if(queryPoint[axis] < subtreeRoot.elementData[axis]) {
               findKNN(queryPoint, subtreeRoot.left, dim+1);
               left = true;
       }else {
               findKNN(queryPoint, subtreeRoot.right, dim+1);
               left = false;
       }
       if(Math.abs(queryPoint[axis] - subtreeRoot.elementData[axis]) < bestDistance) {
           if(left) {
               this.findKNN(queryPoint, subtreeRoot.right, dim+1);
           }else {
               this.findKNN(queryPoint, subtreeRoot.left, dim+1);
           }
       }
    }
}
