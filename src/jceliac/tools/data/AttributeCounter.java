/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * This is used to count certain occurences of attributes of a certain
 * collection of objects.
 * <p>
 * @author shegen
 * @param <T>
 */
public class AttributeCounter<T>
{

    protected HashMap<T, Integer> counts;

    public AttributeCounter()
    {
         this.counts = new HashMap<>();
    }

    /**
     * Attribute is required to have a meaningfull implementation of hashCode
     * for hashing the object.
     * <p>
     * @param attribute
     */
    public void count(T attribute)
    {
        if(this.counts.containsKey(attribute) == false) {
            this.counts.put(attribute, 1);
        } else {
            int tmp = this.counts.get(attribute);
            this.counts.put(attribute, tmp+1);
        }
    }

    public int getCountForAttribute(T attribute)
    {
        return this.counts.get(attribute);
    }

    public int getDistinctAttributeCount()
    {
        return this.counts.keySet().size();
    }
    
    public Set <T> getDistinctAttributes()
    {
        return this.counts.keySet();
    }
    
    public T getAttribute(int index) 
    {
        Set <T> keys = this.counts.keySet();
        return new ArrayList <> (keys).get(index);
    }
}
