/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.interpolator;

/**
 * Bi-linear interpolation.
 * <p>
 * @author shegen
 */
public class BilinearInterpolator
        extends Interpolator
{

    /**
     * Interpolates data bi-linearly.
     * <p>
     * @param data Data to interpolate.
     * @param x    Position to interpolate, x.
     * @param y    Position to inerpolate, y.
     * <p>
     * @return Interpolated value at x,y.
     */
    @Override
    public double interpolate(final double[][] data, double x, double y)
    {
        int fx = (int) x;
        int fy = (int) y;
        int cx = (int) Math.ceil(x);
        int cy = (int) Math.ceil(y);

        double ty = y - fy;
        double tx = x - fx;

        double invtx = 1 - tx;
        double invty = 1 - ty;

        double w1 = invtx * invty;
        double w2 = tx * invty;
        double w3 = invtx * ty;
        double w4 = tx * ty;

        // use this only in extreme debugging cases, this method is called every time
        // Experiment.log(Logger.LOG_HHDEBUG, "getInterpoaltedPixelValue: %f * %f", w1, data[fx][fy]);
        // Experiment.log(Logger.LOG_HHDEBUG, "getInterpoaltedPixelValue: %f * %f", w2, data[cx][fy]);
        // Experiment.log(Logger.LOG_HHDEBUG, "getInterpoaltedPixelValue: %f * %f", w3, data[fx][cy]);
        //  Experiment.log(Logger.LOG_HHDEBUG, "getInterpoaltedPixelValue: %f * %f", w4, data[cx][cy]);
        double ret = w1 * data[fx][fy] + w2 * data[cx][fy] + w3 * data[fx][cy] + w4 * data[cx][cy];
        return ret;
    }

}
