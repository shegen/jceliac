/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.parser;

import jceliac.JCeliacGenericException;
import jceliac.experiment.Experiment;
import jceliac.logging.JCeliacLogger;

/**
 * Parses filenames to create Leave-One-Patient-Out information.
 * <p>
 * @author shegen
 */
public class LOPOIdentifierParser
{

    private static final String OEGD_REGEX = "([a-zA-ZüÜäÄöÖß]+_)([a-zA-ZüÜäÄöÖß]+_)*(ÖGD|OEGD|OEButton|OEnone)_(\\d+)_(\\d+)-(Q165)?-?(\\d+).*";
    private static final String OEGD_REGEX_NBI = "([a-zA-ZüÜäÄöÖß]+[_|-])([a-zA-ZüÜäÄöÖß]+[_|-])*(ÖGD|OEGD|OEButton|OEnone|Coloskopie|none)_(\\d+)_(\\d+)*";
    private static final String PIT_ARTIFICIAL_REGEX = "([0-9]+_)([0-9]+_)([0-9]+_)*(ÖGD|OEGD|OEButton)_(\\d+)_([a-zA-Z\\s]+)[-_]([a-zA-Z]+)-(\\d+).*";
    private static final String AKH_REGEX = "(Patient\\d+)-AKH(\\d+)-(\\d+).*";
    private static final String NBI_PIT_REGEX = "(Patient\\d+)(.*)";
    private static final String KTH_TIPS_REGEX = "(^[\\da-zA-Z]+)-scale_(\\d+)_im_(\\d+).*";
    

    public static final int TYPE_OEGD = 1;
    public static final int TYPE_KTH_TIPS = 2;
    public static final int TYPE_ART_PIT = 3;
    public static final int TYPE_OEGD_NBI = 4;
    public static final int TYPE_AKH = 5;
    public static final int TYPE_NBI_PIT = 6;
    
    private final RegexpParser parser;
    private int type;
    private static boolean printInfo = true;

    private LOPOIdentifierParser()
    {
        // default is OEGD
        this.parser = new RegexpParser(OEGD_REGEX);
    }

    private LOPOIdentifierParser(int type)
    {
        switch(type) {
            case TYPE_KTH_TIPS:
                this.parser = new RegexpParser(KTH_TIPS_REGEX);
                break;
            case TYPE_ART_PIT:
                this.parser = new RegexpParser(PIT_ARTIFICIAL_REGEX);
                break;
            case TYPE_OEGD_NBI:
                this.parser = new RegexpParser(OEGD_REGEX_NBI);
                break;
            case TYPE_AKH:
                this.parser = new RegexpParser(AKH_REGEX);
                break;
            case TYPE_NBI_PIT:
                this.parser = new RegexpParser(NBI_PIT_REGEX);
                break;
            case TYPE_OEGD:
            default:
                this.parser = new RegexpParser(OEGD_REGEX);
        }
        this.type = type;
    }

    /**
     * Creates parser by trying to parse filename, first correct parse is then
     * used to create a parser.
     * <p>
     * @param sampleData Filename to create parser for.
     * <p>
     * @return An according LOPO parser for the filename type.
     * <p>
     * @throws JCeliacGenericException If no LOPO parser can be found for the
     *                                 filename type.
     */
    public static LOPOIdentifierParser createParser(String sampleData)
            throws JCeliacGenericException
    {
        RegexpParser kthParser = new RegexpParser(KTH_TIPS_REGEX);
        RegexpParser oegdParser = new RegexpParser(OEGD_REGEX);
        RegexpParser pitParser = new RegexpParser(PIT_ARTIFICIAL_REGEX);
        RegexpParser nbiParser = new RegexpParser(OEGD_REGEX_NBI);
        RegexpParser akhParser = new RegexpParser(AKH_REGEX);
        RegexpParser nbiPitParser = new RegexpParser(NBI_PIT_REGEX);
        
        // remove the path from the sample Data name
        int pathEnd = sampleData.lastIndexOf("/");
        String parseData = sampleData.substring(pathEnd+1);
        
        try {
            kthParser.parse(parseData);
            if(LOPOIdentifierParser.printInfo) {
                Experiment.log(JCeliacLogger.LOG_INFO, "Creating Identifier Parser for KTH-TIPS Type Data!");
                LOPOIdentifierParser.printInfo = false;
            }
            return new LOPOIdentifierParser(TYPE_KTH_TIPS);
        } catch(JCeliacGenericException e) {

        }

        try {
            oegdParser.parse(parseData);
            if(LOPOIdentifierParser.printInfo) {
                Experiment.log(JCeliacLogger.LOG_INFO, "Creating Identifier Parser for OEGD Type Data!");
                LOPOIdentifierParser.printInfo = false;
            }
            return new LOPOIdentifierParser(TYPE_OEGD);
        } catch(JCeliacGenericException e) {

        }

        try {
            pitParser.parse(parseData);
            if(LOPOIdentifierParser.printInfo) {
                Experiment.log(JCeliacLogger.LOG_INFO, "Creating Identifier Parser for PIT Artificial Type Data!");
                LOPOIdentifierParser.printInfo = false;
            }
            return new LOPOIdentifierParser(TYPE_ART_PIT);
        } catch(JCeliacGenericException e) {

        }
        
        try {
            nbiParser.parse(parseData);
            if(LOPOIdentifierParser.printInfo) {
                Experiment.log(JCeliacLogger.LOG_INFO, "Creating Identifier Parser for NBI Data!");
                LOPOIdentifierParser.printInfo = false;
            }
            return new LOPOIdentifierParser(TYPE_OEGD_NBI);
        } catch(JCeliacGenericException e) {

        }
        
        try {
            akhParser.parse(parseData);
            if(LOPOIdentifierParser.printInfo) {
                Experiment.log(JCeliacLogger.LOG_INFO, "Creating Identifier Parser for AKH Data!");
                LOPOIdentifierParser.printInfo = false;
            }
            return new LOPOIdentifierParser(TYPE_AKH);
        } catch(JCeliacGenericException e) {

        }
        
        try {
            nbiPitParser.parse(parseData);
            if(LOPOIdentifierParser.printInfo) {
                Experiment.log(JCeliacLogger.LOG_INFO, "Creating Identifier Parser for NBI PIT Data!");
                LOPOIdentifierParser.printInfo = false;
            }
            return new LOPOIdentifierParser(TYPE_NBI_PIT);
        } catch(JCeliacGenericException e) {

        }
         
        
        throw new JCeliacGenericException("No LOPO parser for file format: "+parseData);
    }

    /**
     * Generates a unique identifier for a patient.
     * <p>
     * @param data Filename.
     * <p>
     * @return Unique identifier for patient.
     * <p>
     * @throws JCeliacGenericException
     */
    public String generateUniqueIdentifier(String data)
            throws JCeliacGenericException
    {

        String[] groups = this.parser.parse(data);
        switch(this.type) {
            case TYPE_KTH_TIPS:
                return this.generateUniqueIdentifierKTH_TIPS(groups);
            case TYPE_AKH:
                return this.generateUniqueIdentifierAKH(groups);
            case TYPE_NBI_PIT:
                return this.generateUniqueIdentifierNBIPIT(groups);
            case TYPE_OEGD:
            default:
                return this.generateUniqueIdentifierOEGD(groups);
        }
    }

    /**
     * Use the names as identifier. "Turan_Cem_ÖGD_20100112_11978-Q165-0.PNG"
     * returns groups: Turan_, Cem_, null, ÖGD_, 20100112_, 11978, Q165, 0
     * <p>
     */
    private String generateUniqueIdentifierOEGD(String[] groups)
    {
        String identifier = groups[0];
        identifier += groups[1];
        if(groups[2] != null) {
            identifier += groups[2];
        }
        return identifier;
    }

    /**
     * "44-scale_1_im_1_col.png" returns groups: 44, 1, 1 where 44 is the sample
     * number, 1 is the scale and 1 is the image number The image numbers 1,2,3
     * are frontal.
     * <p>
     */
    private String generateUniqueIdentifierKTH_TIPS(String[] groups)
    {
        // the unique identifier is sample and scale
        return groups[0] + "_" + groups[1];
    }
    
    /**
     * "Patient123-AKH12-0.png"
     * <p>
     */
    private String generateUniqueIdentifierAKH(String[] groups)
    {
        // the unique identifier is sample and scale
        return groups[0];
    }
    
   /**
     * "Patient4+21+1963+m-354-100-200-11.png"
     * <p>
     */
    private String generateUniqueIdentifierNBIPIT(String[] groups)
    {
        // the unique identifier is sample and scale
        return groups[0];
    }
    
}
