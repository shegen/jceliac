/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.parser;

import jceliac.JCeliacGenericException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Wrapper for a regexp parser.
 * <p>
 * @author shegen
 */
public class RegexpParser
{

    private final Pattern pattern;

    public RegexpParser(String regexp)
    {
        this.pattern = Pattern.compile(regexp);
    }

    public String[] parse(String data) throws JCeliacGenericException
    {
        String[] groups;
        Matcher matcher = this.pattern.matcher(data);
        boolean found = matcher.find();
        if(found) {
            groups = new String[matcher.groupCount() - 1];
            for(int i = 0; i < matcher.groupCount() - 1; i++) {
                groups[i] = matcher.group(i + 1);
            }
            return groups;
        } else {
            throw new JCeliacGenericException("Could not parse!");
        }
    }
}
