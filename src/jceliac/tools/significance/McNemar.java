/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.significance;

import umontreal.iro.lecuyer.probdist.ChiSquareDist;
import jceliac.classification.ClassificationOutcome;
import jceliac.classification.ClassificationResult;
import jceliac.JCeliacGenericException;
import jceliac.tools.arrays.*;
import java.util.ArrayList;

/**
 * McNemar test for significance.
 * <p>
 * @author shegen
 */
public class McNemar
{

    private final int[] classAssignments1;
    private final int[] classAssignments2;
    private final int[] groundtruth;
    private final int N;
    private final ChiSquareDist chiSquareDist;

    public McNemar(int[] classAssignments1, int[] classAssignments2, int[] groundtruth,
                   int N)
    {
        this.classAssignments1 = classAssignments1;
        this.classAssignments2 = classAssignments2;
        this.groundtruth = groundtruth;
        this.N = N;

        this.chiSquareDist = new ChiSquareDist(1);
    }

    public McNemar(ClassificationResult result1, ClassificationResult result2, int N)
            throws JCeliacGenericException
    {
        ArrayTools<Integer> at = new ArrayTools<>();
        int[] groundtruth1 = this.collectGroundtruth(result1);
        int[] groundtruth2 = this.collectGroundtruth(result2);

        boolean eq = at.compare(at.toObject(groundtruth1, Integer.class),
                                at.toObject(groundtruth2, Integer.class),
                                (i1, i2) -> i1 - i2);
        if(eq == false) {
            throw new JCeliacGenericException("Groundtruth mismatch");
        }
        this.groundtruth = groundtruth1;
        this.classAssignments1 = this.collectEstimatedClasses(result1);
        this.classAssignments2 = this.collectEstimatedClasses(result2);
        this.N = N;

        this.chiSquareDist = new ChiSquareDist(1);
    }

    /**
     * Creates groundtruth for classification result.
     * <p>
     * @param result Classification result.
     * <p>
     * @return Correctly labeled groundtruth.
     */
    private int[] collectGroundtruth(ClassificationResult result)
    {
        ArrayList<ClassificationOutcome> classificationOutcomes = result.getClassificationOutcomes();
        int[] gt = new int[classificationOutcomes.size()];

        for(int index = 0; index < classificationOutcomes.size(); index++) {
            ClassificationOutcome outcome = classificationOutcomes.get(index);
            gt[index] = outcome.getFeatures().getClassNumber();
        }
        return gt;
    }

    /**
     * Creates list of outcomes for classification result.
     * <p>
     * @param result Classification result.
     * <p>
     * @return Correctly labeled outcomes.
     */
    private int[] collectEstimatedClasses(ClassificationResult result)
    {
        ArrayList<ClassificationOutcome> classificationOutcomes = result.getClassificationOutcomes();
        int[] ec = new int[classificationOutcomes.size()];

        for(int index = 0; index < classificationOutcomes.size(); index++) {
            ClassificationOutcome outcome = classificationOutcomes.get(index);
            ec[index] = outcome.getEstimatedClass();
        }
        return ec;
    }

    /**
     * Get the count where ass1 was correctly classified and ass2 was
     * incorrectly classified.
     * <p>
     * @param ass1 Outcome labels of result 1.
     * @param ass2 Outcome labels of result 2.
     * <p>
     * @return Number of differences where ass1 was correctly classified and
     *         ass2 was incorrectly classified.
     */
    private int getDifferencesCount(int[] ass1, int[] ass2)
    {
        int count = 0;

        for(int i = 0; i < ass1.length; i++) {
            if(ass1[i] == this.groundtruth[i] && ass2[i] != this.groundtruth[i]) {
                count++;
            }
        }
        return count;
    }

    /**
     * Tests for significance by checking how probably the differences are
     * distributed according to a chi-squared distribution.
     * <p>
     * @param alpha Significance level.
     * <p>
     * @return True if significant, false else.
     */
    public boolean testSignificance(double alpha)
    {
        int n10 = this.getDifferencesCount(this.classAssignments1, this.classAssignments2);
        int n01 = this.getDifferencesCount(this.classAssignments2, this.classAssignments1);

        if(n10 + n01 == 0) {
            return false;
        }
        double T = Math.pow(Math.abs(n10 - n01) - 0.5, 2.0d) / (n10 + n01);
        double alphac = 1 - Math.pow((1.0d - alpha), (1.0d / (double) this.N));
        double threshold = this.chiSquareDist.inverseF(1 - alphac);
        return (T >= threshold);
    }

    /**
     * Computes the chi-square cumulative density function value at position x
     * with v degrees of freedom.
     * <p>
     * @param x Position in distribution.
     * @param v Degrees of freedom.
     * <p>
     * @return Cumulative density function value.
     * <p>
     * @throws JCeliacGenericException
     */
    public static double chi2cdf(double x, int v)
            throws JCeliacGenericException
    {
        double value = 0;
        double deltaT = 0.00001;
        for(double t = 0; t <= x; t += deltaT) {
            value += (Math.pow(t, (v - 2) / 2.0d) * Math.exp(-t / 2.0d)) / (Math.pow(2, v / 2) * gamma(v / 2)) * deltaT;
        }
        return value;
    }

    /**
     * Uses lanczos approximation to compute the gamma function.
     * <p>
     * @param param Paramter to gamma function.
     * <p>
     * @return Value of gamma function for parameter.
     */
    public static double gamma(double param)
    {
        double real = param;
        double g = 7;
        double p[] = {0.99999999999980993, 676.5203681218851, -1259.1392167224028,
                      771.32342877765313, -176.61502916214059, 12.507343278686905,
                      -0.13857109526572012, 9.9843695780195716e-6, 1.5056327351493116e-7};

        if(real < 0.5d) {
            return Math.PI / Math.sin(Math.PI * real) / gamma(1 - real);
        } else {
            real = real - 1.0d;
            double x = p[0];
            for(int i = 1; i < g + 2; i++) {
                x += p[i] / (real + i);
            }
            double t = real + g + 0.5;
            return Math.sqrt(2 * Math.PI) * Math.pow(t, real + 0.5d) * Math.exp(-t) * x;
        }
    }
}
