/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.memory;

import jceliac.experiment.Experiment;
import jceliac.logging.JCeliacLogger;

public class MemoryInfos
{

    private static long lFreeMemoryOld = MemoryInfos.getFreeMemory();
    private static long lTotalMemoryOld = MemoryInfos.getTotalMemory();
    private static long lUsedMemoryOld;
    private static long lFreeMemoryCurrent;
    private static long lTotalMemoryCurrent;
    private static long lUsedMemoryCurrent;
    private static long lFreeMemoryChange = 0;
    private static long lTotalMemoryChange = 0;
    private static long lUsedMemoryChange = 0;

    public MemoryInfos()
    {
        MemoryInfos.lUsedMemoryOld = MemoryInfos.lTotalMemoryOld - MemoryInfos.lFreeMemoryOld;
        MemoryInfos.lFreeMemoryCurrent = MemoryInfos.lFreeMemoryOld;
        MemoryInfos.lTotalMemoryCurrent = MemoryInfos.lTotalMemoryOld;
        MemoryInfos.lUsedMemoryCurrent = MemoryInfos.lUsedMemoryOld;
    }

    public static long getTotalMemory()
    {
        return Runtime.getRuntime().totalMemory();
    }

    public static long getFreeMemory()
    {
        return Runtime.getRuntime().freeMemory();
    }

    public static long getMaxMemory()
    {
        return Runtime.getRuntime().maxMemory();
    }

    public static long getUsedMemory()
    {
        return getTotalMemory() - getFreeMemory();
    }

    /**
     * Upates the current memory statistics.
     */
    private static void updateMemoryStatistics()
    {
        // get current values
        MemoryInfos.lFreeMemoryCurrent = MemoryInfos.getFreeMemory();
        MemoryInfos.lTotalMemoryCurrent = MemoryInfos.getTotalMemory();
        MemoryInfos.lUsedMemoryCurrent = MemoryInfos.lTotalMemoryCurrent - MemoryInfos.lFreeMemoryCurrent;

        // calc changes
        MemoryInfos.lFreeMemoryChange = MemoryInfos.lFreeMemoryCurrent - MemoryInfos.lFreeMemoryOld;
        MemoryInfos.lTotalMemoryChange = MemoryInfos.lTotalMemoryCurrent - MemoryInfos.lTotalMemoryOld;
        MemoryInfos.lUsedMemoryChange = MemoryInfos.lUsedMemoryCurrent - MemoryInfos.lUsedMemoryOld;

        // remember old values
        MemoryInfos.lFreeMemoryOld = MemoryInfos.lFreeMemoryCurrent;
        MemoryInfos.lTotalMemoryOld = MemoryInfos.lTotalMemoryCurrent;
        MemoryInfos.lUsedMemoryOld = MemoryInfos.lUsedMemoryCurrent;
    }

    /**
     * Updates the memory statics and returns used memory as string.
     * <p>
     * @return Used memory in KB as string.
     */
    public static String getUpdatedUsedMemoryStringMB()
    {
        MemoryInfos.updateMemoryStatistics();

        long lUsedMemoryKB = (MemoryInfos.lUsedMemoryCurrent / 1024) / 1024;

        return "Heap used: " + lUsedMemoryKB + " MB";
    }

    /**
     * Logs memory information.
     */
    public static void dumpMemoryInfos()
    {
        MemoryInfos.updateMemoryStatistics();

        Experiment.log(JCeliacLogger.LOG_INFO, "Heap used: " + MemoryInfos.getUpdatedUsedMemoryStringMB());
        Experiment.log(JCeliacLogger.LOG_HINFO, "Used memory: " + MemoryInfos.lUsedMemoryCurrent + " bytes (change: " + MemoryInfos.lUsedMemoryChange + " bytes)");
        Experiment.log(JCeliacLogger.LOG_HINFO, "Free memory: " + MemoryInfos.lFreeMemoryCurrent + " bytes (change: " + MemoryInfos.lFreeMemoryChange + " bytes)");
        Experiment.log(JCeliacLogger.LOG_HINFO, "Total memory: " + MemoryInfos.lTotalMemoryCurrent + " bytes (change: " + MemoryInfos.lTotalMemoryChange + " bytes)");
    }
}
