/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.tools.parallel;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import jceliac.JCeliacGenericException;


/**
 *
 * @author shegen
 * @param <T> Return type of the callable object. 
 */
public class ParallelTaskExecutor <T>
{
    
    protected ArrayList <Callable<T>> tasks;
    protected ExecutorCompletionService<T> completionService;
    protected ExecutorService executor;
    
    public ParallelTaskExecutor(ArrayList<Callable <T>> tasks, boolean cachedThreadPool)
    {
        this.tasks = tasks;
        if(cachedThreadPool == false) {
            this.executor = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());
        }else {
            this.executor = Executors.newCachedThreadPool();
        }
        this.completionService = new ExecutorCompletionService<>(this.executor);
    }
    
    
    public ArrayList <T> executeTasks()
            throws JCeliacGenericException
    {
        ArrayList<T> allResults = new ArrayList<>();
        ArrayList<Future<T>> futures = new ArrayList<>();
        JCeliacGenericException caughtException = null;
        int done = 0;
        int total = this.tasks.size();
        
        try {
            for(Callable <T> c : this.tasks) {
                futures.add(this.completionService.submit(c));
            }
            for(Future<T> tmp : futures) {
                try {
                    Future<T> computedFuture = this.completionService.take();
                    T result = computedFuture.get();
                    allResults.add(result);
                    if(done > 1) {
                    //    System.out.print("\33[1A\33[2K");
                    }
                    //System.out.print(++done + "/" + total+"\n");

                } catch(InterruptedException | ExecutionException e) {
                    StackTraceElement[] els = e.getStackTrace();
                    caughtException = new JCeliacGenericException(e);
                    e.printStackTrace();
                    break;
                }
            }
        } finally {
            for(Future<T> future : futures) {
                future.cancel(true);
            }
            this.executor.shutdownNow();
            if(caughtException != null) {
                throw caughtException;
            }
        }
        return allResults;
    }
}
