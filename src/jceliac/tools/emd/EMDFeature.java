package jceliac.tools.emd;

public interface EMDFeature {
    public double groundDist(EMDFeature f);
}