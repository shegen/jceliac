package jceliac.tools.emd;

/**
 * @author Telmo Menezes (telmo@telmomenezes.com)
 *
 */
public class EMDFeature2D implements EMDFeature {
    private double x;
    private double y;

    public EMDFeature2D(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    @Override
    public double groundDist(EMDFeature f) {
        EMDFeature2D f2d = (EMDFeature2D)f;
        double deltaX = x - f2d.x;
        double deltaY = y - f2d.y;
        return Math.sqrt((deltaX * deltaX) + (deltaY * deltaY));
    }
}