package jceliac.tools.emd;


/**
 * @author Telmo Menezes (telmo@telmomenezes.com)
 *
 */
public class Signature {
    private int numberOfFeatures;
    private EMDFeature[] features;
    private double[] weights;
    
    public int getNumberOfFeatures() {
        return numberOfFeatures;
    }
    
    public void setNumberOfFeatures(int numberOfFeatures) {
        this.numberOfFeatures = numberOfFeatures;
    }

    public EMDFeature[] getFeatures() {
        return features;
    }

    public void setFeatures(EMDFeature[] features) {
        this.features = features;
    }

    public double[] getWeights() {
        return weights;
    }

    public void setWeights(double[] weights) {
        this.weights = weights;
    }
}