/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.export;

import java.io.Serializable;

/**
 * A feature type that is exportable by serialization.
 * <p>
 * @author shegen
 */
public class ExportableFeature
        implements Serializable
{

    private final int classNumber;
    private final double[] featureVector;
    private final String filename;

    public ExportableFeature(int classNumber, double[] featureVector, String filename)
    {
        this.classNumber = classNumber;
        this.featureVector = featureVector;
        this.filename = filename;
    }

    public int getClassNumber()
    {
        return classNumber;
    }

    public double[] getFeatureVector()
    {
        return featureVector;
    }

    public String getFilename()
    {
        return filename;
    }
}
