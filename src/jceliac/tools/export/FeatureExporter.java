/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.export;

import java.io.BufferedWriter;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.JCeliacGenericException;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.io.File;
import java.io.FileWriter;
import java.util.Collections;
import java.util.HashMap;
import jceliac.tools.parser.LOPOIdentifierParser;

/**
 * Exporter for features, expects features to be serializable.
 * <p>
 * @author shegen
 */
public class FeatureExporter
{

    private final ArrayList<DiscriminativeFeatures> features;
    private final File directory;
    private final String experimentName;

    /**
     * Initializes the feature exporter.
     * <p>
     * @param directory      Directory to write the features to.
     * @param features       List of serializable features.
     * @param experimentName Name of experiment.
     */
    public FeatureExporter(File directory, ArrayList<DiscriminativeFeatures> features, String experimentName)
    {
        this.features = features;
        this.directory = directory;
        this.experimentName = experimentName;
    }

    /**
     * Exports features supplied in the constructor.
     * <p>
     * @throws JCeliacGenericException
     */
    public void exportFeaturesSerialized()
            throws JCeliacGenericException
    {
        ArrayList<ExportableFeature> exportableFeatures = new ArrayList<>();
        for(DiscriminativeFeatures tmpFeature : this.features) {
            FeatureVectorSubsetEncoding encoding = new FeatureVectorSubsetEncoding(tmpFeature.getFeatureVectorDimensionality());
            encoding.setAll();
            DiscriminativeFeatures tmpFeatureSubset = tmpFeature.getClonedFeatureSubset(encoding);
            double[] featureVector = tmpFeatureSubset.getAllFeatureVectorData();
            ExportableFeature feature = new ExportableFeature(tmpFeature.getClassNumber(),
                                                              featureVector,
                                                              tmpFeature.getSignalIdentifier()
            );
            exportableFeatures.add(feature);
        }
        try(FileOutputStream fileOutStream = new FileOutputStream(this.directory.getAbsolutePath() + File.separator + "features.ser");
            ObjectOutputStream objOutStream = new ObjectOutputStream(fileOutStream)) {
            objOutStream.writeObject(exportableFeatures);
        } catch(Exception e) {
            throw new JCeliacGenericException(e);
        }
    }
    
    private class PatientMapping 
    {
        public String imageName;
        public int imageID;
        public int patientID;
        public int classLabel;
    }
    public void exportFeatureCSV()
            throws JCeliacGenericException
    {
        HashMap <String, ArrayList <DiscriminativeFeatures>> patients = this.collectPatients(this.features);
        File patientMappingOutFile = new File(this.directory.getAbsolutePath() + File.separator + this.experimentName + "-PatientMapping.csv");
        File featureDataOutFile = new File(this.directory.getAbsolutePath() + File.separator + this.experimentName + "-FeatureData.csv");
        
        HashMap <String, PatientMapping> patientMapping = this.computePatientMapping(this.features);
        this.exportPatientMappingCSV(patientMapping, patientMappingOutFile);
        this.exportFeatureVectors(this.features, patientMapping, featureDataOutFile);
    }
    
    
    
    private HashMap <String, PatientMapping> computePatientMapping(ArrayList <DiscriminativeFeatures> features)
            throws JCeliacGenericException
    {
        HashMap <String, ArrayList <DiscriminativeFeatures>> patients = this.collectPatients(this.features);
        HashMap <String, PatientMapping> patientMapping = new HashMap<>();
        int patientID = 0;
        int imageID = 0;
        
        for(String patientName : patients.keySet()) {
            ArrayList <DiscriminativeFeatures> featuresPerPatient = patients.get(patientName);
            for(DiscriminativeFeatures feature : featuresPerPatient) {
                PatientMapping pMap = new PatientMapping();
                pMap.classLabel = feature.getClassNumber();
                pMap.imageID = imageID;
                pMap.patientID = patientID;
                
                File fTmp = new File(feature.getSignalIdentifier());
                pMap.imageName = fTmp.getName();
                patientMapping.put(feature.getSignalIdentifier(), pMap);
                imageID++;
            }
            patientID++;
        }
        return patientMapping;
    }
    
    /**
     * Exports a patient mapping file in CSV format.
     * The format is: "Image-Name,Image-ID,PatientID,Class-Label\r\n".
     * 
     * @throws jceliac.JCeliacGenericException
     */
    private void exportPatientMappingCSV(HashMap <String, PatientMapping> patientMapping, File outFile)
            throws JCeliacGenericException
    {
        String exportString = "";

        for(String patientName : patientMapping.keySet()) {
                PatientMapping pMap = patientMapping.get(patientName);
                
                exportString +=   pMap.imageName  + "," 
                                + pMap.imageID    + "," 
                                + pMap.patientID  + ","
                                + pMap.classLabel + "\r\n";
               
        }
        // export data
        try(FileWriter writer = new FileWriter(outFile, true);
            BufferedWriter bwriter = new BufferedWriter(writer)) {
            bwriter.write(exportString);
            
            bwriter.flush();
            bwriter.close();
        } catch(Exception e) {
            throw new JCeliacGenericException(e);
        }
        
    }
    
    
    private void exportFeatureVectors(ArrayList <DiscriminativeFeatures> features, 
                                     HashMap <String, PatientMapping> patientMapping,
                                     File outFile)
            throws JCeliacGenericException
    {
        String exportString = "";
        for(DiscriminativeFeatures tmpFeature : features) {
            PatientMapping pMap = patientMapping.get(tmpFeature.getSignalIdentifier());
            String featureVectorDataString = this.exportFeatureVectorCSV(tmpFeature);
            exportString += pMap.imageID + ",";
            exportString += featureVectorDataString + "\r\n";
        }
       
        try(FileWriter writer = new FileWriter(outFile, true);
            BufferedWriter bwriter = new BufferedWriter(writer)) {
            bwriter.write(exportString);
            
            bwriter.flush();
            bwriter.close();
        } catch(Exception e) {
            throw new JCeliacGenericException(e);
        }
    }
    
    private String exportFeatureVectorCSV(DiscriminativeFeatures feature)
            throws JCeliacGenericException
    {
        String featureVectorString = "";
        double [] featureData = feature.getAllFeatureVectorData();
        
        for(int index = 0; index < featureData.length; index++) {
            featureVectorString += String.valueOf(featureData[index]);
            if(index+1 < featureData.length) {
                featureVectorString += ",";
            }
        }
        return featureVectorString;
    }
    
    private HashMap <String, ArrayList <DiscriminativeFeatures>> collectPatients(ArrayList <DiscriminativeFeatures> features)
            throws JCeliacGenericException
    {
        HashMap<String, ArrayList<DiscriminativeFeatures>> patientMap;
        patientMap = new HashMap<>();
        
        for(DiscriminativeFeatures tmp : features) {
            LOPOIdentifierParser parser = LOPOIdentifierParser.createParser(tmp.getSignalIdentifier());

            if(parser == null) {
                throw new JCeliacGenericException("Could not create parser");
            }
            String uniqueIdentifier = parser.generateUniqueIdentifier(tmp.getSignalIdentifier());
            ArrayList<DiscriminativeFeatures> l = patientMap.get(uniqueIdentifier);
            if(l == null) {
                l = new ArrayList<>();
                l.add(tmp);
                patientMap.put(uniqueIdentifier, l);
            } else {
                l.add(tmp);
                patientMap.put(uniqueIdentifier, l);
            }
        }
        return patientMap;

    }
    

}
