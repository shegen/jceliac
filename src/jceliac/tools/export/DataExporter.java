/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.export;

import jceliac.tools.filter.scalespace.ScalespacePoint;
import jceliac.JCeliacGenericException;
import java.util.*;
import java.io.*;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.experimental.ScaleResponseFeatures;


/**
 * Exports data in a CSV fashion, is also used to export scale-space scale
 * levels and response values.
 * <p>
 * @author shegen
 */
public class DataExporter
{

    public static void exportData(double[][] data, String outfile)
            throws JCeliacGenericException
    {

        File f = new File(outfile);
        try(FileWriter writer = new FileWriter(f);
            BufferedWriter bwriter = new BufferedWriter(writer)) {
            for(double[] tmp : data) {
                for(int y = 0; y < data[0].length; y++) {
                    bwriter.write(tmp[y] + " ");
                }
                bwriter.write("\n");
            }
            bwriter.flush();
            bwriter.close();
        } catch(Exception e) {
            throw new JCeliacGenericException(e);
        }
    }
    
    public static void exportScaleResponseFeatures(ArrayList<DiscriminativeFeatures> features,
                                                   String outfile)
            throws JCeliacGenericException
    {

        File f = new File(outfile);
        try(FileWriter writer = new FileWriter(f);
            BufferedWriter bwriter = new BufferedWriter(writer)) {

            int index = 0;
            for(DiscriminativeFeatures tmpFeatures : features) {
                ArrayList<double[]> featureVectors = ((ScaleResponseFeatures) tmpFeatures).getLocalFeatureVectorData();
              
                index++;
                for(double[] data : featureVectors) {
                        bwriter.write(String.format("%d ", index));
                        bwriter.write(String.format("%d ", ((ScaleResponseFeatures) tmpFeatures).getClassNumber()));
                        bwriter.write(String.format("%d ", ((ScaleResponseFeatures) tmpFeatures).getScaleIdentifier()));
                        for(int x = 0; x < data.length; x++) {
                            String formattedData = String.format("%.2f", data[x]);
                            bwriter.write(formattedData + " ");
                        }
                        bwriter.write("\n");
                    }
                
               
            }
            bwriter.flush();
            bwriter.close();
        } catch(Exception e) {
            throw new JCeliacGenericException(e);
        }
    }

    public static synchronized void exportScalespaceIndices(ArrayList<ScalespacePoint> points, String outfile)
            throws JCeliacGenericException
    {

        File f = new File(outfile);
        try(FileWriter writer = new FileWriter(f, true);
            BufferedWriter bwriter = new BufferedWriter(writer)) {

            for(ScalespacePoint tmp : points) {
                bwriter.write(tmp.getScaleLevel() + ";\n");
            }
            bwriter.flush();
            bwriter.close();
        } catch(Exception e) {
            throw new JCeliacGenericException(e);
        }
    }

    public static synchronized void exportScalespaceValues(ArrayList<ScalespacePoint> points, String outfile)
            throws JCeliacGenericException
    {

        File f = new File(outfile);
        try(FileWriter writer = new FileWriter(f, true);
            BufferedWriter bwriter = new BufferedWriter(writer)) {

            for(ScalespacePoint tmp : points) {
                bwriter.write(tmp.getResponse() + ";\n");
            }
            bwriter.flush();
            bwriter.close();
        } catch(Exception e) {
            throw new JCeliacGenericException(e);
        }
    }

}
