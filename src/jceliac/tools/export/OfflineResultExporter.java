/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.export;

import jceliac.JCeliacGenericException;
import jceliac.classification.*;
import java.util.*;
import java.io.*;

/**
 * Exporter of results of offline experiments.
 * <p>
 * @author shegen
 */
public class OfflineResultExporter
{

    private final ArrayList<ClassificationOutcome> outcomes;
    private final String outfile;
    private final String neighborsOutfile;

    public OfflineResultExporter(ArrayList<ClassificationOutcome> outcomes,
                                 String outfile, String neighborsOutfile)
    {
        this.outcomes = outcomes;
        this.outfile = outfile;
        this.neighborsOutfile = neighborsOutfile;
    }

    public OfflineResultExporter()
    {
        this.outcomes = null;
        this.outfile = null;
        this.neighborsOutfile = null;
    }
    
    

    /**
     * Exports results supplied to the constructor, writes each outcome's signal
     * identifier, class number and estimated class. This can be used to run
     * significance tests.
     * <p>
     * @throws JCeliacGenericException
     */
    public void exportResults()
            throws JCeliacGenericException
    {
        File file = new File(this.outfile);
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {

            for(ClassificationOutcome tmpOutcome : this.outcomes) {
                String line = tmpOutcome.getFeatures().getSignalIdentifier() + " "
                              + tmpOutcome.getFeatures().getClassNumber() + " "
                              + tmpOutcome.getEstimatedClass() + "\r\n";
                writer.write(line);
            }
            writer.close();
        } catch(IOException e) {
            throw new JCeliacGenericException(e);
        }
    }

    /**
     * Serializes the entire ClassificationResult object.
     * <p>
     * @param result  Classification result to export.
     * @param outfile File to write the object to.
     * <p>
     * @throws JCeliacGenericException
     */
    public void serializeResultsFile(IteratedClassificationResult result, String outfile)
            throws JCeliacGenericException
    {
        try(FileOutputStream fileOutStream = new FileOutputStream(outfile);
            ObjectOutputStream objOutStream = new ObjectOutputStream(fileOutStream)) {
            objOutStream.writeObject(result);
        } catch(IOException e) {
            throw new JCeliacGenericException(e);
        }
    }

    /**
     * Exports the neighbors of each feature vector when using k-nn.
     * <p>
     * @param num Number of neighbors to write.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public void exportNeighbors(int num)
            throws JCeliacGenericException
    {

        ArrayList<String> writtenList = new ArrayList<>();

        File file = new File(this.neighborsOutfile);
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            for(ClassificationOutcome tmpOutcome : this.outcomes) {
                if(writtenList.contains(tmpOutcome.getFeatures().getSignalIdentifier())) {
                    continue;
                }
                ArrayList<Integer> neighbors = tmpOutcome.getNearestNeighbors();
                if(neighbors.isEmpty()) {
                    writer.close();
                    return;
                }
                String neighborString = "";
                for(int index = 0; index < num; index++) {
                    Integer kNeighbor = neighbors.get(index);
                    neighborString += kNeighbor + " ";
                }
                String line = tmpOutcome.getFeatures().getSignalIdentifier() + " "
                              + tmpOutcome.getFeatures().getClassNumber() + " "
                              + "Neighbors: " + neighborString + "\r\n";

                writer.write(line);
                writtenList.add(tmpOutcome.getFeatures().getSignalIdentifier());
            }
        } catch(IOException e) {
            throw new JCeliacGenericException(e);
        }
    }

}
