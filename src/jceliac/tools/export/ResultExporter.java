/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.tools.export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationOutcome;
import jceliac.classification.ClassificationResult;
import jceliac.classification.IteratedClassificationResult;

/**
 *
 * @author shegen
 */
public class ResultExporter
{
    private final File outfile;

    public ResultExporter(String experimentName)
    {
        String resultFilename = experimentName + ".results";
        this.outfile = new File(resultFilename);
    }
    
    public void exportResult(IteratedClassificationResult results)
            throws JCeliacGenericException
    {
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(this.outfile))) {
            for(int i = 0; i < results.getNumberOfResults(); i++) {
                ClassificationResult result = results.getSingleResult(i);
                double rate = result.getOverallClassificationRate();
                writer.write(rate+"\n");
            }
            writer.close();
        } catch(IOException e) {
            throw new JCeliacGenericException(e);
        }
    }
}
