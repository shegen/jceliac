/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.export;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.io.FileReader;
import java.util.HashMap;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jceliac.JCeliacGenericException;
import jceliac.tools.transforms.dtcwt.ComplexArray1D;
import jceliac.tools.transforms.dtcwt.ComplexArray2D;

/**
 * Imports a matrix exported from Matlab, or generally a matrix with elements
 * separated by ",".
 * <p>
 * @author shegen
 */
public class MatrixImporter
{

    public static double[][] importDoubleMatrix(File file)
            throws Exception
    {
        BufferedReader bufs = new BufferedReader(new FileReader(file));
        String line;

        HashMap<Integer, ArrayList<Double>> mat = new HashMap<>();
        int rowIndex = 0;
        int rowWidth = 0;
        while((line = bufs.readLine()) != null) {
            String[] entries = line.split(",");
            ArrayList<Double> rowData = new ArrayList<>();
            for(int i = 0; i < entries.length; i++) {
                double tmp = Double.parseDouble(entries[i]);
                rowData.add(i, tmp);
            }
            mat.put(rowIndex, rowData);
            rowWidth = rowData.size();
            rowIndex++;
        }

        // create a matrix
        double[][] matrix = new double[rowWidth][mat.size()];
        for(int i = 0; i < mat.size(); i++) {
            ArrayList<Double> row = mat.get(i);
            for(int colIndex = 0; colIndex < row.size(); colIndex++) {
                matrix[colIndex][i] = row.get(colIndex);
            }
        }
        return matrix;
    }
   
    
    public static double[] importDoubleMatrix1D(File file)
            throws Exception
    {
        BufferedReader bufs = new BufferedReader(new FileReader(file));
        String line;

        ArrayList<Double> mat = new ArrayList<>();
        while((line = bufs.readLine()) != null) {
            String[] entries = line.split(",");
            for(int i = 0; i < entries.length; i++) {
                double tmp = Double.parseDouble(entries[i]);
                mat.add(tmp);

            }
        }
        // create a matrix
        double[] matrix = new double[mat.size()];
        for(int i = 0; i < mat.size(); i++) {
            matrix[i] = mat.get(i);
        }
        return matrix;
    }
    
    public static ComplexArray2D importComplexMatrix(File file)
            throws Exception
    {

        BufferedReader bufs = new BufferedReader(new FileReader(file));
        String line;

        HashMap<Integer, ArrayList<Double>> matReal = new HashMap<>();
        HashMap<Integer, ArrayList<Double>> matImaginary = new HashMap<>();
        int rowIndex = 0;
        int rowWidth = 0;
        
        while((line = bufs.readLine()) != null) {
            String[] entries = line.split(",");
            ArrayList<Double> rowDataReal = new ArrayList<>();
            ArrayList<Double> rowDataImaginary = new ArrayList<>();
            for(int i = 0; i < entries.length; i++) {
                double [] complex = MatrixImporter.parseComplexNumberString(entries[i]);
                rowDataReal.add(i, complex[0]);
                rowDataImaginary.add(i, complex[1]);
            }
            matReal.put(rowIndex, rowDataReal);
            matImaginary.put(rowIndex, rowDataImaginary);
            
            rowWidth = rowDataReal.size();
            rowIndex++;
        }

        // create a matrix
        double[][] matrixReal = new double[rowWidth][matReal.size()];
        double[][] matrixImaginary = new double[rowWidth][matReal.size()];
        
        for(int i = 0; i < matReal.size(); i++) {
            ArrayList<Double> rowReal = matReal.get(i);
            ArrayList<Double> rowImaginary = matImaginary.get(i);
            
            for(int colIndex = 0; colIndex < rowReal.size(); colIndex++) {
                matrixReal[colIndex][i] = rowReal.get(colIndex);
                matrixImaginary[colIndex][i] = rowImaginary.get(colIndex);
            }
        }
        ComplexArray2D ret = new ComplexArray2D(matrixReal, matrixImaginary);
        return ret;
    }
    
    
    public static ComplexArray1D importComplexMatrix1D(File file)
            throws Exception
    {

        BufferedReader bufs = new BufferedReader(new FileReader(file));
        String line;

        ArrayList<Double> matReal = new ArrayList<>();
        ArrayList<Double> matImage = new ArrayList<>();
        int rowIndex = 0;
        int rowWidth = 0;
        
        while((line = bufs.readLine()) != null) {
            String[] entries = line.split(",");
            for(int i = 0; i < entries.length; i++) {
                double [] complex = MatrixImporter.parseComplexNumberString(entries[i]);
                matReal.add(complex[0]);
                matImage.add(complex[1]);
            }
        }

        // create a matrix
        double[] matrixReal = new double[matReal.size()];
        double[] matrixImaginary = new double[matImage.size()];
        
        for(int i = 0; i < matReal.size(); i++) {
            matrixReal[i] = matReal.get(i);
            matrixImaginary[i] = matImage.get(i);
        }
        ComplexArray1D ret = new ComplexArray1D(matrixReal, matrixImaginary);
        return ret;
    }
    
    private static double[] parseComplexNumberString(String complexNumber)
            throws JCeliacGenericException
    {
        String regexpComplex = "(?<REAL>(\\+|\\-)*\\d+(\\.\\d*(e(\\-|\\+)\\d+)?)?)\\s*(?<IMAGINARY>(\\+|\\-)\\s*\\d+(\\.\\d*)?)\\s*i\\s*";
        Pattern p = Pattern.compile(regexpComplex);
        Matcher m = p.matcher(complexNumber);
        
        if(m.matches() == false) {
            // this can be a complex value with zero imaginary part, dlmwrite will not write 0*i
            String regexpReal = "(\\+|\\-)*\\d+(\\.\\d*(e(\\-|\\+)\\d+)?)?";
            Pattern pReal = Pattern.compile(regexpReal);
            Matcher mReal = pReal.matcher(complexNumber);
            if(mReal.matches() == false) {
                throw new JCeliacGenericException("Neither Complex nor Real number encountered!");
            }
            double real = Double.parseDouble(complexNumber);
            return new double[] { real, 0};
        }
        String realString = m.group("REAL");
        String imaginaryString = m.group("IMAGINARY");

        double real = Double.parseDouble(realString);
        double imaginary = Double.parseDouble(imaginaryString);
        return new double[]{real, imaginary};
    }

}
