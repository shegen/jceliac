/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.arrays;

import jceliac.JCeliacGenericException;
import java.lang.reflect.Array;
import java.util.Collections;
import java.util.Comparator;
import java.util.ArrayList;
import data.MatlabArray;
import java.util.Arrays;
import java.util.function.DoublePredicate;
import java.util.function.IntPredicate;
import jceliac.tools.transforms.dtcwt.ComplexArray1D;
import jceliac.tools.transforms.dtcwt.ComplexArray2D;

/**
 * Tools for array handling.
 * <p>
 * @author shegen
 * @param <T> Type of array.
 */
public class ArrayTools<T>
{

    /**
     * Extracts an area that is valid, centerX, centerY might not be the actual
     * center of the extracted area.
     * <p>
     * <p>
     * @param data    Data to extract area from.
     * @param centerX Center x-coordinate of area.
     * @param centerY Center y-coordinate of area-
     * @param size    Size of area.
     * <p>
     * @return Extracted area as new initialized array.
     */
    public static double[][] extractAreaValid(double[][] data, int centerX, int centerY, int size)
    {
        int startX, endX;
        int startY, endY;

        startX = Math.max(0, centerX - size);
        endX = Math.min(data.length - 1, centerX + size);
        startY = Math.max(0, centerY - size);
        endY = Math.min(data[0].length - 1, centerY + size);

        double[][] extractedArea = new double[(endX - startX) + 1][(endY - startY) + 1];
        for(int i = startX, iPos = 0; i <= endX; i++, iPos++) {
            for(int j = startY, jPos = 0; j <= endY; j++, jPos++) {
                extractedArea[iPos][jPos] = data[i][j];
            }
        }
        return extractedArea;
    }

    /**
     * Extract an area centered at centerX and centerY with the given size.
     * <p>
     * @param data    Data to extract area from.
     * @param centerX Center x-coordinate of area.
     * @param centerY Center y-coordiante of area.
     * @param size    Size of area.
     * <p>
     * @return Extracted area as new initialized array.
     * <p>
     * @throws jceliac.JCeliacGenericException If the requested area crosses the
     *                                         image bounds.
     */
    public static double[][] extractAreaFull(double[][] data, int centerX, int centerY, int size)
            throws JCeliacGenericException
    {
        int startX, endX;
        int startY, endY;

        startX = centerX - size;
        endX = centerX + size;
        startY = centerY - size;
        endY = centerY + size;

        if(startX < 0 || startY < 0) {
            throw new JCeliacGenericException("Cannot extract full area, area crosses bounds");
        }
        if(endX > data.length - 1 || endY > data[0].length - 1) {
            throw new JCeliacGenericException("Cannot extract full area, area crosses bounds");
        }
        double[][] extractedArea = new double[(endX - startX) + 1][(endY - startY) + 1];
        for(int i = startX, iPos = 0; i <= endX; i++, iPos++) {
            for(int j = startY, jPos = 0; j <= endY; j++, jPos++) {
                extractedArea[iPos][jPos] = data[i][j];
            }
        }
        return extractedArea;
    }

    /**
     * Compares two double arrays with a given error margin.
     * <p>
     * @param a      Array one.
     * @param b      Array two.
     * @param margin Error margin, all errors less or equal to margin will be
     *               ignored.
     * <p>
     * @return True if a equals b, false else.
     */
    public static boolean compare(double[][] a, double[][] b, double errorMargin)
    {
        if(a.length != b.length) {
            return false;
        }
        if(a[0].length != b[0].length) {
            return false;
        }
        for(int i = 0; i < a.length; i++) {
            for(int j = 0; j < a[0].length; j++) {
                if(Math.abs(a[i][j] - b[i][j]) > errorMargin) {
                    double diff = Math.abs(a[i][j] - b[i][j]);
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Compares two double arrays with a given error margin.
     * <p>
     * @param a      Array one.
     * @param b      Array two.
     * @param margin Error margin, all errors less or equal to margin will be
     *               ignored.
     * <p>
     * @return True if a equals b, false else.
     */
    public static boolean compare(double[] a, double[] b, double margin)
    {
        if(a.length != b.length) {
            return false;
        }
        for(int i = 0; i < a.length; i++) {
            if(Math.abs(a[i] - b[i]) > margin) {
                double diff = Math.abs(a[i] - b[i]);
                return false;
            }
        }
        return true;
    }
    
    public static boolean compare(ComplexArray2D a, ComplexArray2D b, double margin)
    {
        return ArrayTools.compare(a.realValues, b.realValues, margin) &&
               ArrayTools.compare(a.imaginaryValues, b.imaginaryValues, margin);
    }
    
    public static boolean compare(ComplexArray1D a, ComplexArray1D b, double margin)
    {
        return ArrayTools.compare(a.realValues, b.realValues, margin) &&
               ArrayTools.compare(a.imaginaryValues, b.imaginaryValues, margin);
    }

    /**
     * Compares two generic arrays.
     * <p>
     * @param array1 Array one.
     * @param array2 Array two.
     * @param comp   A comparator.
     * <p>
     * @return True if a equals b, false else.
     */
    public boolean compare(T[][] array1, T[][] array2, Comparator<T> comp)
    {
        if(array1.length != array2.length) {
            return false;
        }
        if(array1[0].length != array2[0].length) {
            return false;
        }
        for(int i = 0; i < array1.length; i++) {
            for(int j = 0; j < array1[0].length; j++) {
                if(comp.compare(array1[i][j], array2[i][j]) != 0) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Compares two generic arrays.
     * <p>
     * @param array1 Array one.
     * @param array2 Array two.
     * @param comp   A comparator.
     * <p>
     * @return True if a equals b, false else.
     */
    public boolean compare(T[] array1, T[] array2, Comparator<T> comp)
    {
        if(array1.length != array2.length) {
            return false;
        }
        for(int i = 0; i < array1.length; i++) {

            if(comp.compare(array1[i], array2[i]) != 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Compares a generic array with a MatlabArray.
     * <p>
     * @param array1 Array one.
     * @param array2 Array two.
     * @param comp   A comparator.
     * <p>
     * @return True if a equals b, false else.
     */
    public boolean compare(T[][] array1, MatlabArray<T> array2, Comparator<T> comp)
    {
        for(int i = 0; i < array1.length; i++) {
            for(int j = 0; j < array1[0].length; j++) {
                if(comp.compare(array1[i][j], array2.getElementSubIndex(j + 1, i + 1)) != 0) {
                    T x1 = array1[i][j];
                    T x2 = array2.getElementSubIndex(i + 1, j + 1);
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Converts an integer array to an array of type clazz.
     * <p>
     * @param array Integer array.
     * @param clazz Array type to convert to.
     * <p>
     * @return Converted array.
     */
    public T[] toObject(int[] array, Class<T> clazz)
    {
        @SuppressWarnings("unchecked")
        T[] objArray = (T[]) Array.newInstance(clazz, array.length);
        for(int i = 0; i < array.length; i++) {
            objArray[i] = clazz.cast(array[i]);
        }
        return objArray;
    }

    /**
     * Converts a byte array to an array of type clazz.
     * <p>
     * @param array Byte array.
     * @param clazz Array type to convert to.
     * <p>
     * @return Converted array.
     */
    public T[] toObject(byte[] array, Class<T> clazz)
    {
        @SuppressWarnings("unchecked")
        T[] objArray = (T[]) Array.newInstance(clazz, array.length);
        for(int i = 0; i < array.length; i++) {
            objArray[i] = clazz.cast(array[i]);
        }
        return objArray;
    }

    /**
     * Converts a double array to an array of type clazz.
     * <p>
     * @param array Double array.
     * @param clazz Array type to convert to.
     * <p>
     * @return Converted array.
     */
    public T[] toObject(double[] array, Class<T> clazz)
    {
        @SuppressWarnings("unchecked")
        T[] objArray = (T[]) Array.newInstance(clazz, array.length);
        for(int i = 0; i < array.length; i++) {
            objArray[i] = clazz.cast(array[i]);
        }
        return objArray;
    }

    /**
     * Converts a double array to an array of type clazz.
     * <p>
     * @param array Double array.
     * @param clazz Array type to convert to.
     * <p>
     * @return Converted array.
     */
    public T[][] toObject(double[][] array, Class<T> clazz)
    {
        @SuppressWarnings("unchecked")
        T[][] objArray = (T[][]) Array.newInstance(clazz, array.length, array[0].length);
        for(int i = 0; i < array.length; i++) {
            for(int j = 0; j < array[0].length; j++) {
                objArray[i][j] = clazz.cast(array[i][j]);
            }
        }
        return objArray;
    }

    /**
     * Converts a MatlabArray to a double array.
     * <p>
     * @param array MatlabArray.
     * <p>
     * @return A converted double array.
     * <p>
     * @throws JCeliacGenericException
     */
    public static double[][] toDoubleArray(MatlabArray array)
            throws JCeliacGenericException
    {
        int[] dimensions = array.getDimensions();
        if(dimensions.length != 2) {
            throw new JCeliacGenericException("Expecting two dimensional array!");
        }
        double[][] retArray = new double[dimensions[0]][dimensions[1]];
        for(int i = 0; i < retArray.length; i++) {
            for(int j = 0; j < retArray[0].length; j++) {
                retArray[j][i] = (double) array.getElementSubIndex(i + 1, j + 1);
            }
        }
        return retArray;
    }

    /**
     * Returns the maximum value of an array.
     * <p>
     * @param array Array to search for maximum.
     * <p>
     * @return Maximum value of array.
     */
    public static double max(double[] array)
    {
        double maxValue = -Double.MAX_VALUE;
        for(int i = 0; i < array.length; i++) {
            if(array[i] > maxValue) {
                maxValue = array[i];
            }
        }
        return maxValue;
    }
    
    public static double max(ArrayList <Double> array)
    {
        double maxValue = Double.NEGATIVE_INFINITY;
        for(Double tmp : array) {
            if(tmp > maxValue) {
                maxValue = tmp;
            }
        }
        return maxValue;
    }
    
    /**
     * Returns the maximum value of an array.
     * <p>
     * @param array Array to search for maximum.
     * <p>
     * @return Maximum value of array.
     */
    public static double max(double[][] array)
    {
        double maxValue = -Double.MAX_VALUE;
        for(double[] tmp : array) {
            for(int j = 0; j < tmp.length; j++) {
                if(tmp[j] > maxValue) {
                    maxValue = tmp[j];
                }
            }
        }
        return maxValue;
    }

    /**
     * Returns the index of the maximum value of an array.
     * <p>
     * @param array Array to search for maximum.
     * <p>
     * @return Index of maximum value of array.
     */
    public static int maxIndex(double[] array)
    {
        double maxValue = -Double.MAX_VALUE;
        int maxIndex = -1;
        for(int i = 0; i < array.length; i++) {
            if(array[i] > maxValue) {
                maxValue = array[i];
                maxIndex = i;
            }
        }
        return maxIndex;
    }

    /**
     * Returns the index of the minimum value of an array.
     * <p>
     * @param array Array to search for minimum.
     * <p>
     * @return Index of minimum value of array.
     */
    public static int minIndex(double[] array)
    {
        double minValue = Double.MAX_VALUE;
        int minIndex = -1;
        for(int i = 0; i < array.length; i++) {
            if(array[i] < minValue) {
                minValue = array[i];
                minIndex = i;
            }
        }
        return minIndex;
    }

    /**
     * Returns the minimum value of an array.
     * <p>
     * @param array Array to search for minimum.
     * <p>
     * @return Minimum value of array.
     */
    public static double min(double[] array)
    {
        double minValue = Double.MAX_VALUE;
        for(int i = 0; i < array.length; i++) {
            if(array[i] < minValue) {
                minValue = array[i];
            }
        }
        return minValue;
    }
    
    public static double min(ArrayList <Double> array)
    {
        double minValue = Double.POSITIVE_INFINITY;
        for(Double tmp : array) {
            if(tmp < minValue) {
                minValue = tmp;
            }
        }
        return minValue;
    }
    
    
    public static int min(int[] array)
    {
        int minValue = Integer.MAX_VALUE;
        for(int i = 0; i < array.length; i++) {
            if(array[i] < minValue) {
                minValue = array[i];
            }
        }
        return minValue;
    }

    public static double min(double[][] array)
    {
        double minValue = Double.MAX_VALUE;
        for(double[] tmp : array) {
            for(int j = 0; j < tmp.length; j++) {
                if(tmp[j] < minValue) {
                    minValue = tmp[j];
                }
            }
        }
        return minValue;
    }

    /**
     * Computes the mean of an array.
     * <p>
     * @param array Array to compute mean.
     * <p>
     * @return Mean of array.
     */
    public static double mean(double[][] array)
    {
        double sum = 0;
        for(int i = 0; i < array.length; i++) {
            for(int j = 0; j < array[0].length; j++) {
                sum = sum + array[i][j];
            }
        }
        return sum / (array.length * array[0].length);
    }

    /**
     * Computes the mean of an array.
     * <p>
     * @param array Array to compute mean.
     * <p>
     * @return Mean of array.
     */
    public static double mean(double[] array)
    {
        double sum = 0;
        for(int i = 0; i < array.length; i++) {
            sum = sum + array[i];
        }
        return sum / (array.length);
    }

    /**
     * Computes the mean of an ArrayList.
     * <p>
     * @param array ArrayList to compute mean.
     * <p>
     * @return Mean of array.
     */
    public static double mean(ArrayList<Double> array)
    {
        double sum = 0;
        for(int i = 0; i < array.size(); i++) {
            double tmp = array.get(i);
            if(Double.isNaN(tmp)) {
                System.out.println("NaN");
            }
            if(Double.isInfinite(tmp)) {
                System.out.println("Inf");
            }
            sum = sum + array.get(i);
        }
        return sum / (array.size());
    }

    /**
     * Computes the mean of an ArrayList pf longs.
     * <p>
     * @param array ArrayList to compute mean.
     * <p>
     * @return Mean of array.
     */
    public static double meanL(ArrayList<Long> array)
    {
        double sum = 0;
        for(int i = 0; i < array.size(); i++) {
            sum = sum + array.get(i);
        }
        return sum / (array.size());
    }

    
   /**
     * Computes the sum of values of an array.
     * <p>
     * @param array Array to compute sum.
     * <p>
     * @return Sum of values in array.
     */
    public static int sum(int[][] array)
    {
        int sum = 0;
        for(int[] tmp : array) {
            for(int j = 0; j < array[0].length; j++) {
                sum = sum + tmp[j];
            }
        }
        return sum;
    }
    
    
    /**
     * Computes the sum of values of an array.
     * <p>
     * @param array Array to compute sum.
     * <p>
     * @return Sum of values in array.
     */
    public static double sum(double[][] array)
    {
        double sum = 0;
        for(int i = 0; i < array[0].length; i++) {
            for(int j = 0; j < array.length; j++) {
                sum = sum + array[j][i];
            }
        }
        return sum;
    }
    
    /**
     * Computes the absolute sum of values of an array.
     * <p>
     * @param array Array to compute sum.
     * <p>
     * @return Sum of values in array.
     */
    public static double abssum(double[][] array)
    {
        double sum = 0;
        for(double[] tmp : array) {
            for(int j = 0; j < array[0].length; j++) {
                sum = sum + Math.abs(tmp[j]);
            }
        }
        return sum;
    }

    /**
     * Computes the sum of values of an array.
     * <p>
     * @param array Array to compute sum.
     * <p>
     * @return Sum of values in array.
     */
    public static double sum(double[] array)
    {
        double sum = 0;
        for(int i = 0; i < array.length; i++) {
            sum = sum + array[i];
        }
        return sum;
    }
    
   /**
     * Computes the product of values of an array.
     * <p>
     * @param array Array to compute sum.
     * <p>
     * @return Sum of values in array.
     */
    public static int prod(int[] array)
    {
        int prod = 1;
        for(int i = 0; i < array.length; i++) {
            prod = prod * array[i];
        }
        return prod;
    }

    /**
     * Computes the sum of values of an array.
     * <p>
     * @param array Array to compute sum.
     * <p>
     * @return Sum of values in array.
     */
    public static int sum(int[] array)
    {
        int sum = 0;
        for(int i = 0; i < array.length; i++) {
            sum = sum + array[i];
        }
        return sum;
    }

    /**
     * Computes the sum of values of an ArrayList.
     * <p>
     * @param array Array to compute sum.
     * <p>
     * @return Sum of values in array.
     */
    public static double sumL(ArrayList<Long> array)
    {
        double sum = 0;
        for(int i = 0; i < array.size(); i++) {
            sum = sum + array.get(i);
        }
        return sum;
    }

    /**
     * Computes the median of values of an array.
     * <p>
     * @param array Array to compute median.
     * <p>
     * @return Median of values in array.
     */
    public static double median(double[] array)
    {
        Arrays.sort(array);
        int pos;
        if(array.length % 2 == 1) {
            pos = ((array.length + 1) / 2) - 1;
            return array[pos];
        } else {
            pos = (array.length / 2) - 1;
            return 0.5 * (array[pos] + array[pos + 1]);
        }
    }

    /**
     * Computes the median of values of an ArrayList.
     * <p>
     * @param array Array to compute median.
     * <p>
     * @return Median of values in array.
     */
    public static int median(ArrayList<Integer> array)
    {
        Collections.sort(array);
        int pos;
        if(array.size() % 2 == 1) {
            pos = ((array.size() + 1) / 2) - 1;
            return array.get(pos);
        } else {
            pos = (array.size() / 2) - 1;
            return (int) Math.floor(0.5 * (array.get(pos) + array.get(pos + 1)));
        }
    }
    
    public static double medianDouble(ArrayList<Double> array)
    {
        Collections.sort(array);
        int pos;
        if(array.size() % 2 == 1) {
            pos = ((array.size() + 1) / 2) - 1;
            return array.get(pos);
        } else {
            pos = (array.size() / 2) - 1;
            return (double) 0.5 * (array.get(pos) + array.get(pos + 1));
        }
    }

    /**
     * Computes the histogram of an ArrayList.
     * <p>
     * @param data ArrayList of values.
     * @param bins Number of bins to create the histogram.
     * <p>
     * @return Histogram of data.
     */
    public static double[] computeScaledHistogram(ArrayList<Double> data, int bins)
    {
        double[] hist = new double[bins];
        double maxValue = data.stream().max((a, b) -> a > b ? 1 : -1).get();
        double minValue = data.stream().max((a, b) -> a < b ? 1 : -1).get();

        for(int index = 0; index < data.size(); index++) {
            double tmp = (data.get(index) - minValue) / (maxValue - minValue);
            int histIndex = (int) Math.round(tmp * (bins - 1));
            hist[histIndex]++;
        }
        return hist;
    }

    
    public static double [][] abs(double [][] data)
    {
        double [][] absData = new double[data.length][data[0].length];
        for(int i = 0; i < data.length; i++) {
            for(int j = 0; j < data[0].length; j++) {
                absData[i][j] = Math.abs(data[i][j]);
            }
        }
        return absData;
    }
    
    public static double[][] max(double[][] data1, double[][] data2)
            throws JCeliacGenericException
    {
        if(data1.length != data2.length || data1[0].length != data2[0].length) {
            throw new JCeliacGenericException("Data dimensions mismatch!");
        }
        double[][] maxData = new double[data1.length][data1[0].length];
        for(int i = 0; i < data1.length; i++) {
            for(int j = 0; j < data1[0].length; j++) {
                maxData[i][j] = Math.max(data1[i][j], data2[i][j]);
            }
        }
        return maxData;
    }
    
    public static double[] reshape(double[][] data)
    {
        double[] reshapedData = new double[data.length * data[0].length];
        int index = 0;
        for(int i = 0; i < data.length; i++) {
            for(int j = 0; j < data[0].length; j++) {
                reshapedData[index] = data[i][j];
                index++;
            }
        }
        return reshapedData;
    }
    
  
    
    public static boolean any(double [][] data, DoublePredicate comp)
    {
        for(int i = 0; i < data.length; i++) {
            for(int j = 0; j < data[0].length; j++) {
                if(true == comp.test(data[i][j])) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public static boolean any(int[] data, IntPredicate comp)
    {
        for(int i = 0; i < data.length; i++) {
            if(true == comp.test(data[i])) {
                return true;
            }
        }
        return false;
    }
}
