/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.cluster;

/**
 * A data vector in k-means clustering.
 * <p>
 * @author shegen
 */
public class KMeansDataPoint
{

    public double[] dataVector;
    public int clusterCenterLabel;

    /**
     * Compute squared euclidean distance to other feature point p.
     * <p>
     * @param p Reference point to compute distance to.
     * <p>
     * @return Squared euclidean distance.
     */
    public double distanceTo(KMeansDataPoint p)
    {
        if(this.dataVector == null) {
            return Double.MAX_VALUE;
        }
        double sum = 0;
        for(int i = 0; i < dataVector.length; i++) {
            sum += (this.dataVector[i] - p.dataVector[i]) * (this.dataVector[i] - p.dataVector[i]);
        }
        return sum;
    }

  


}
