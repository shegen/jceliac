/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jceliac.tools.cluster;

/**
 *
 * @author shegen
 */
public class KMeansClusterCenter extends KMeansDataPoint
{
    public int numAssignedPoints;
    
    /**
     * Adds another data point to this data point. This is used to compute
     * average cluster center positions.
     * <p>
     * @param p A data point.
     */
    public void addVector(KMeansDataPoint p)
    {
        if(this.dataVector == null) {
            this.dataVector = new double[p.dataVector.length];
        }
        for(int i = 0; i < this.dataVector.length; i++) {
            this.dataVector[i] += p.dataVector[i];
        }
        this.numAssignedPoints++;
    }
    
   /**
     * Normalize data vector.
     */
    public void normalize()
    {
        if(this.dataVector == null) {
            return;
        }
        for(int i = 0; i < this.dataVector.length; i++) {
            this.dataVector[i] /= (double) this.numAssignedPoints;
        }
    }
}
