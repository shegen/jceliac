/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.cluster;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import jceliac.JCeliacGenericException;

/**
 * K means clustering implementation. This does not perform the onlineUpdate as
 * done in matlab. Hence it is compatbible to: kmeans(... 'OnlinePhase' ,
 * 'off');
 * <p>
 * <p>
 * <p>
 * @author shegen
 */
public class KMeansClustering
{

    private final KMeansDataPoint[] dataPoints;
    private KMeansClusterCenter[] clusterCenters;
    private final long seed = 1234567890;
    private final int numIterations;
    // randomly initialize cluster centers
    private final Random rand = new Random(this.seed);

    
    public KMeansClustering(ArrayList <double []> data, 
                            int numClusters, int numIterations)
            throws JCeliacGenericException
    {
        this.numIterations = numIterations;
        this.clusterCenters = new KMeansClusterCenter[numClusters];
        int maxTries = 10;
        
        this.dataPoints = this.aggregateData(data);

        do {
            int[] clusterCenterIndices = this.createRandomClusterCenterIndices(this.dataPoints.length, numClusters);
            for(int i = 0; i < clusterCenterIndices.length; i++) {
                this.clusterCenters[i] = new KMeansClusterCenter();
                this.clusterCenters[i].dataVector = this.dataPoints[clusterCenterIndices[i]].dataVector;
                this.clusterCenters[i].clusterCenterLabel = i;
            }
            maxTries--;
        } while(this.checkClusterInitialization() == false && maxTries > 0);
    }
    
    
    private KMeansDataPoint[] aggregateData(ArrayList<double []> data)
            throws JCeliacGenericException
    {
        if(data == null || data.isEmpty()) {
            throw new JCeliacGenericException("Cannot cluster empty list of data!");
        }

        KMeansDataPoint[] aggregatedData = new KMeansDataPoint[data.size()];
        int dstOffset = 0;
        for(double [] tmpData : data) {
            aggregatedData[dstOffset] = new KMeansDataPoint();
            aggregatedData[dstOffset].dataVector = tmpData;
            dstOffset++;
        }
        return aggregatedData;
    }
    
    public KMeansClustering(double[][] data, int numClusters, int numIterations)
    {
        this.numIterations = numIterations;
        this.clusterCenters = new KMeansClusterCenter[numClusters];
        int maxTries = 10;

        do {
            int[] clusterCenterIndices = this.createRandomClusterCenterIndices(data.length, numClusters);
            for(int i = 0; i < clusterCenterIndices.length; i++) {
                this.clusterCenters[i] = new KMeansClusterCenter();
                this.clusterCenters[i].dataVector = data[clusterCenterIndices[i]];
                this.clusterCenters[i].clusterCenterLabel = i;
            }
            maxTries--;
        } while(this.checkClusterInitialization() == false && maxTries > 0);

        this.dataPoints = new KMeansDataPoint[data.length];
        for(int i = 0; i < data.length; i++) {
            this.dataPoints[i] = new KMeansDataPoint();
            this.dataPoints[i].dataVector = data[i];
        }
    }

    public KMeansClustering(double[][] data, int numClusters, int[] seedPoints, int numIterations)
    {
        this.numIterations = numIterations;
        this.clusterCenters = new KMeansClusterCenter[numClusters];
        int maxTries = 10;

        do {
            int[] clusterCenterIndices = seedPoints;
            for(int i = 0; i < clusterCenterIndices.length; i++) {
                this.clusterCenters[i] = new KMeansClusterCenter();
                this.clusterCenters[i].dataVector = data[clusterCenterIndices[i]];
                this.clusterCenters[i].clusterCenterLabel = i;
            }
            maxTries--;
        } while(this.checkClusterInitialization() == false && maxTries > 0);

        this.dataPoints = new KMeansDataPoint[data.length];
        for(int i = 0; i < data.length; i++) {
            this.dataPoints[i] = new KMeansDataPoint();
            this.dataPoints[i].dataVector = data[i];
        }
    }

    /**
     * Identifies the closest cluster center relative to p.
     * <p>
     * @param p A point that is clustered.
     * <p>
     * @return The closest cluster center relative to p.
     */
    private KMeansClusterCenter findClosestClusterCenter(KMeansDataPoint p)
            throws JCeliacGenericException
    {
        double minDistance = Double.MAX_VALUE;
        KMeansClusterCenter bestClusterCenter = null;
        for(KMeansClusterCenter clusterCenter : this.clusterCenters) {
            if(p.dataVector == null) {
                throw new JCeliacGenericException("A data vector of cluster centers is null");
            }
            double distance = p.distanceTo(clusterCenter);
            if(distance < minDistance) {
                minDistance = distance;
                bestClusterCenter = clusterCenter;
            }
        }
        if(bestClusterCenter == null) {
            throw new JCeliacGenericException("No best cluster center found");
        }
        return bestClusterCenter;
    }

    /**
     * Creates random cluster centers for initialization.
     * <p>
     * @param dataLen     Number of data points.
     * @param numClusters Number of clusters.
     * <p>
     * @return Indices of randomly initialized cluster centers.
     */
    private int[] createRandomClusterCenterIndices(int dataLen, int numClusters)
    {
        int[] clusterIndices = new int[numClusters];
        ArrayList <Integer> availableIndices = new ArrayList <>();
        for(int i = 0; i < clusterIndices.length; i++) {
            availableIndices.add(i);
        }
        Collections.shuffle(availableIndices, rand);
        
        for(int i = 0; i < clusterIndices.length; i++) {
            clusterIndices[i] = availableIndices.get(i);
        }
        return clusterIndices;
    }

    /**
     * In some rare cases it can happen that the initialization picks two
     * cluster centers with different indices, but the same data values; This
     * method checks for such cases.
     * <p>
     */
    private boolean checkClusterInitialization()
    {
        for(int i = 0; i < this.clusterCenters.length; i++) {
            for(int j = i + 1; j < this.clusterCenters.length; j++) {
                double distance = this.clusterCenters[i].distanceTo(this.clusterCenters[j]);
                if(distance < 10e-10) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Checks if clusterIndex is contained in clusterIndices.
     * <p>
     * @param clusterIndices Array of indices.
     * @param clusterIndex   Index that is checked.
     * <p>
     * @return True if clusterIndex is contained in clusterIndices.
     */
    private boolean containsClusterCenterInitialization(int[] clusterIndices, int clusterIndex)
    {
        for(int j = 0; j < clusterIndices.length; j++) {
            if(clusterIndices[j] == clusterIndex) {
                return true;
            }
        }
        return false;
    }

    private void printStats()
    {
        for(int i = 0; i < this.clusterCenters.length; i++) {
            System.out.println("Cluster (" + i + "): " + this.clusterCenters[i].numAssignedPoints);
        }
        System.out.println("Total distance: " + this.computeTotalSumOfDistances());
        double[] clusterDistances = this.computeTotalSumOfDistancesByCluster();
        for(int i = 0; i < clusterDistances.length; i++) {
            System.out.println("Cluster (" + i + ") distance sum: " + clusterDistances[i]);
        }
    }

    /**
     * Performs a step of k-means clustering.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    protected void iterate()
            throws JCeliacGenericException

    {
        double previousTotalSumOfDistances = Double.MAX_VALUE;
        for(int iteration = 1; iteration <= this.numIterations; iteration++) {
            this.assign();
            this.update();
            // check if we converged
            double totalSumOfDistances = this.computeTotalSumOfDistances();
            if(Math.abs(totalSumOfDistances - previousTotalSumOfDistances) < 1e-10) {
                break;
            }
            previousTotalSumOfDistances = totalSumOfDistances;
        }
    }

    private void checkNumberOfClusterPoints()
    {
        return;
        /* int[] counts = new int[this.clusterCenters.length];
         * for(KMeansDataPoint dataPoint : this.dataPoints) {
         * counts[dataPoint.assignedClusterCenter.clusterCenterLabel]++;
        } */
    }

    private double computeTotalSumOfDistances()
    {
        double sum = 0;
        for(KMeansDataPoint dataPoint : this.dataPoints) {
            double distance = dataPoint.distanceTo(this.clusterCenters[dataPoint.clusterCenterLabel]);
            sum += distance;
        }
        return sum;
    }

    private double[] computeTotalSumOfDistancesByCluster()
    {
        double[] distanceSumByCluster = new double[this.clusterCenters.length];
        for(KMeansDataPoint dataPoint : this.dataPoints) {
            distanceSumByCluster[dataPoint.clusterCenterLabel]
            += dataPoint.distanceTo(this.clusterCenters[dataPoint.clusterCenterLabel]);
        }
        return distanceSumByCluster;
    }

    /**
     * Updates the cluster centers.
     */
    private void update()
    {

        this.checkNumberOfClusterPoints();

        KMeansClusterCenter[] recomputedClusterCenters = new KMeansClusterCenter[this.clusterCenters.length];
        for(int clusterCenterLabel = 0; clusterCenterLabel < this.clusterCenters.length; clusterCenterLabel++) {
            recomputedClusterCenters[clusterCenterLabel] = new KMeansClusterCenter();
            recomputedClusterCenters[clusterCenterLabel].clusterCenterLabel = clusterCenterLabel;

            for(KMeansDataPoint dataPoint : this.dataPoints) {
                if(dataPoint.clusterCenterLabel == clusterCenterLabel) {
                    // add data vector to cluster center
                    recomputedClusterCenters[clusterCenterLabel].addVector(dataPoint);
                }
            }
            if(recomputedClusterCenters[clusterCenterLabel].numAssignedPoints == 0) {
                System.out.println("Cluster center lost all points!");
                KMeansDataPoint newCenter = this.findNewCenterForCluster(clusterCenterLabel);
                recomputedClusterCenters[clusterCenterLabel].addVector(newCenter);
                
            }
            recomputedClusterCenters[clusterCenterLabel].normalize();
        }
        this.clusterCenters = recomputedClusterCenters;
    }
    
    // matlab empty action singleton
    private KMeansDataPoint findNewCenterForCluster(int label)
    {
        // compute all distances to the old cluster center 
        double maxDistance = 0;
        KMeansDataPoint candidate = null;
        
        for(KMeansDataPoint dataPoint : this.dataPoints) {
            double distance = this.clusterCenters[label].distanceTo(dataPoint);
            if(distance > maxDistance) {
                distance = maxDistance;
                candidate = dataPoint;
            }            
        }
        return candidate;
    }
            

    /**
     * Assigns the closest cluster centers to each of the points to cluster.
     * <p>
     * @throws JCeliacGenericException
     */
    private void assign()
            throws JCeliacGenericException
    {
        for(int index = 0; index < this.clusterCenters.length; index++) {
            this.clusterCenters[index].numAssignedPoints = 0;
        }
        for(int index = 0; index < this.dataPoints.length; index++) {
            KMeansDataPoint dataPoint = this.dataPoints[index];
            KMeansClusterCenter center = this.findClosestClusterCenter(dataPoint);
            dataPoint.clusterCenterLabel = center.clusterCenterLabel;
            center.numAssignedPoints++;
        }
    }

    public void cluster()
            throws JCeliacGenericException
    {
        this.iterate();
    }

    public KMeansClusterCenter[] getClusterCenters()
    {
        return clusterCenters;
    }

    public int[] getClusterLabels()
    {
        int[] labels = new int[this.dataPoints.length];
        for(int i = 0; i < this.dataPoints.length; i++) {
            labels[i] = this.dataPoints[i].clusterCenterLabel;
        }
        return labels;
    }

}
