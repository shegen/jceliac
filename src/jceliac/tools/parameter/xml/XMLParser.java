/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.parameter.xml;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;
import jceliac.experiment.*;
import jceliac.logging.*;
import org.xml.sax.*;
import java.io.*;
import jceliac.*;

/**
 * XML Parser of configuration files.
 * <p>
 * @author shegen
 */
public class XMLParser
{

    private static ConfigurationXMLHandler handler;

    public static void parse(String file, String featureExtractionMethod, String classificationMethod,
                             String optimizationMethod, String generativeModel, String validationMethod)
            throws JCeliacGenericException
    {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            XMLParser.handler = new ConfigurationXMLHandler(featureExtractionMethod, classificationMethod, 
                                                            optimizationMethod, generativeModel,
                                                            validationMethod);

            parser.parse(new File(file), XMLParser.handler);
        } catch(SAXException | ParserConfigurationException | IOException e) {
            Main.log(JCeliacLogger.LOG_ERROR, e.getMessage());
            throw new JCeliacGenericException(e);
        }
    }

    public static ExperimentParameters getExperimentParameters(int experimentNumber)
    {
        return XMLParser.handler.getExperimentParameters(experimentNumber);
    }

    public static GlobalTestConfigurationParameters getGlobalTestConfigurationParameters()
            throws XMLParseException, InitializeFailedException
    {
        return XMLParser.handler.getGlobalTestConfigurationParameters();
    }

    public static int getExperimentCount()
    {
        return XMLParser.handler.getExperimentCount();
    }

    // the main experiments are the experiments defined within the XML file
    public static int getMainExperimentCount()
    {
        return XMLParser.handler.getMainExperimentCount();
    }
}
