/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.parameter.xml;

import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.logging.JCeliacLogger;
import jceliac.experiment.*;
import java.util.ArrayList;
import jceliac.Main;


/**
 * Handles the parsing of feature extraction parameters.
 * <p>
 * @author shegen
 */
public class FeatureExtractionElementHandler
        extends XMLElementHandler<FeatureExtractionParameters>
{

    private final String method;

    public FeatureExtractionElementHandler(String method)
    {
        super("param");
        this.method = method;
    }

    @Override
    public FeatureExtractionParameters getParametersFor(ArrayList<ElementContent> localElements)
            throws XMLParseException, InitializeFailedException
    {
        FeatureExtractionParameters parameters = FeatureExtractionMethodFactory.createParametersByMethod(this.method);
        for(ElementContent current : localElements) {
            if(parameters.hasOption(current.attributeField)) {
                parameters.setOption(current.attributeField, current.attributeContent);
            } else {
                //throw new XMLParseException("Field is unknown for method (" + this.method + "): \"" + current.attributeField + "\".");
                Main.log(JCeliacLogger.LOG_WARN, "Field is unknown for method (" + this.method + "): \"" + current.attributeField + "\".");
            }
        }
        return parameters;
    }
}
