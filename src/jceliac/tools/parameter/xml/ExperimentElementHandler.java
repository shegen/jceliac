/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.parameter.xml;

import java.util.ArrayList;
import jceliac.experiment.*;

/**
 * Handles the parsing of experiment parameters.
 * <p>
 * @author shegen
 */
public class ExperimentElementHandler
        extends XMLElementHandler<ExperimentParameters>
{

    public ExperimentElementHandler()
    {
        super("param");
    }

    @Override
    public ExperimentParameters getParametersFor(ArrayList<ElementContent> localElements)
            throws XMLParseException, InitializeFailedException
    {

        ExperimentParameters parameters = new ExperimentParameters();
        for(ElementContent current : localElements) {
            if(parameters.hasOption(current.attributeField)) {
                parameters.setOption(current.attributeField, current.attributeContent);
            } else {
                throw new XMLParseException("Field is unknown for Experiment Parameters: \"" + current.attributeField + "\".");
            }
        }
        return parameters;
    }
}
