/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.parameter.xml;

import jceliac.featureOptimization.FeatureOptimizationParameters;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.classification.classificationParameters.*;
import org.xml.sax.helpers.DefaultHandler;
import jceliac.video.strategyHandler.*;
import jceliac.experiment.*;
import jceliac.logging.*;
import org.xml.sax.*;
import java.util.*;
import jceliac.*;
import jceliac.classification.model.GenerativeModelParameters;
import jceliac.validation.generic.ValidationMethodParameters;

/**
 * Handles the parsing of xml configuration files.
 * <p>
 * @author shegen
 */
public class ConfigurationXMLHandler
        extends DefaultHandler
{

    public final static String CONFIG = "TestConfiguration";
    public final static String EXPERIMENT = "Experiment";
    public final static String FEATURE_EXTRACTION = "FeatureExtractionMethod";
    public final static String CLASSIFICATION = "ClassificationMethod";
    public final static String OPTIMIZATION = "OptimizationMethod";
    public final static String VIDEO_SEQUENCE_HANDLER = "VideoSequenceHandler";
    public final static String GENERATIVE_MODEL_HANDLER = "GenerativeModel";
    public final static String VALIDATION_HANDLER = "ValidationMethod";

    private XMLElementHandler currentHandler = null;
    private ArrayList<XMLElementHandler> finishedExperimentHandlers;
    private XMLElementHandler globalParameterHandler;

    // stores the parameters by experiment
    private final ArrayList<ExperimentParameters> experimentParametersByExperiment;
    private String featureExtractionMethod = null;
    private String classificationMethod = null;
    private String optimizationMethod = null;
    private String validationMethod = null;
    private String generativeModel = null;

    private int mainExperimentCount = 0;
    private String cmdOverrideClassificationMethod;
    private String cmdOverrideFeatureExtractionMethod;
    private String cmdOverrideOptimizationMethod;
    private String cmdOverrideGenerativeModel;
    private String cmdValidationMethod;
    

    public ConfigurationXMLHandler(String featureExtractionMethod, String classificationMethod,
                                   String optimizationMethod, String generativeModel,
                                   String validationMethod)
    {
        this.experimentParametersByExperiment = new ArrayList<>();
        this.featureExtractionMethod = featureExtractionMethod;
        this.classificationMethod = classificationMethod;
        this.generativeModel = generativeModel;

        this.cmdOverrideFeatureExtractionMethod = featureExtractionMethod;
        this.cmdOverrideClassificationMethod = classificationMethod;
        this.cmdOverrideOptimizationMethod = optimizationMethod;
        this.cmdOverrideGenerativeModel = generativeModel;
        this.cmdValidationMethod = validationMethod;
    }

    public ConfigurationXMLHandler()
    {
        this.experimentParametersByExperiment = new ArrayList<>();
    }

    private void cleanup()
    {
        this.featureExtractionMethod = null;
        this.classificationMethod = null;
        this.optimizationMethod = null;
        this.validationMethod = null;
        this.generativeModel = null;
        this.currentHandler = null;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException
    {
        if(qName.compareToIgnoreCase(ConfigurationXMLHandler.CONFIG) == 0) // starts a test configuration file
        {
            this.currentHandler = new TestConfigurationElementHandler();
            this.globalParameterHandler = this.currentHandler;

            //   System.out.println("Begin of TestConfiguration found, parsing file!");
        } else if(qName.compareToIgnoreCase(ConfigurationXMLHandler.FEATURE_EXTRACTION) == 0) {
            String method = attributes.getValue("method");
            this.finishedExperimentHandlers.add(currentHandler); // add the last handler to finished

            if(this.cmdOverrideFeatureExtractionMethod != null) {
                Main.log(JCeliacLogger.LOG_INFO, "CMD Overruling Feature Extraction Method. Using %s!", this.cmdOverrideFeatureExtractionMethod);
                currentHandler = new FeatureExtractionElementHandler(this.cmdOverrideFeatureExtractionMethod);
            } else {
                this.featureExtractionMethod = method;
                currentHandler = new FeatureExtractionElementHandler(method);
            }
            return;
        } else if(qName.compareToIgnoreCase(ConfigurationXMLHandler.CLASSIFICATION) == 0) {
            String method = attributes.getValue("method");

            this.finishedExperimentHandlers.add(currentHandler); // add the last handler to finished
            if(this.cmdOverrideClassificationMethod != null) {
                Main.log(JCeliacLogger.LOG_INFO, "CMD Overruling Classification Method. Using %s!", this.cmdOverrideClassificationMethod);
                currentHandler = new ClassificationMethodElementHandler(this.cmdOverrideClassificationMethod);
            } else {
                this.classificationMethod = method;
                currentHandler = new ClassificationMethodElementHandler(method);
            }
            return;
        } else if(qName.compareToIgnoreCase(ConfigurationXMLHandler.EXPERIMENT) == 0) {
            this.mainExperimentCount++;
            // create new list of handlers for this element
            this.finishedExperimentHandlers = new ArrayList<>();
            currentHandler = new ExperimentElementHandler();
            return;
        } else if(qName.compareToIgnoreCase(ConfigurationXMLHandler.OPTIMIZATION) == 0) {
            String method = attributes.getValue("method");

            this.finishedExperimentHandlers.add(this.currentHandler); // add the last handler to finished
            if(this.cmdOverrideOptimizationMethod != null) {
                Main.log(JCeliacLogger.LOG_INFO, "CMD Overruling Optimization Method. Using %s!", this.cmdOverrideOptimizationMethod);
                currentHandler = new OptimizationMethodElementHandler(this.cmdOverrideOptimizationMethod);
            } else {
                this.optimizationMethod = method;
                currentHandler = new OptimizationMethodElementHandler(method);
            }
            return;
        } else if(qName.compareToIgnoreCase(ConfigurationXMLHandler.VIDEO_SEQUENCE_HANDLER) == 0) {
            this.finishedExperimentHandlers.add(currentHandler);
            currentHandler = new VideoSequenceHandlerElementHandler();
            return;
        } else if(qName.compareToIgnoreCase(ConfigurationXMLHandler.GENERATIVE_MODEL_HANDLER) == 0) {
            String method = attributes.getValue("method");

            this.finishedExperimentHandlers.add(this.currentHandler); // add the last handler to finished
            if(this.cmdOverrideGenerativeModel != null) {
                Main.log(JCeliacLogger.LOG_INFO, "CMD Overruling Generative Model. Using %s!", this.cmdOverrideGenerativeModel);
                currentHandler = new GenerativeModelElementHandler(this.cmdOverrideGenerativeModel);
            } else {
                this.generativeModel = method;
                currentHandler = new GenerativeModelElementHandler(method);
            }
            return;
        } else if(qName.compareToIgnoreCase(ConfigurationXMLHandler.VALIDATION_HANDLER) == 0) {
            String method = attributes.getValue("method");

            this.finishedExperimentHandlers.add(this.currentHandler); // add the last handler to finished
            if(this.cmdValidationMethod != null) {
                Main.log(JCeliacLogger.LOG_INFO, "CMD Overruling Validation strategy. Using %s!", this.cmdValidationMethod);
                currentHandler = new ValidationMethodElementHandler(this.cmdValidationMethod);
            } else {
                this.validationMethod = method;
                currentHandler = new ValidationMethodElementHandler(method);
            }
            return;
        }
        else { // this is probably handled by the video sequence handler 
            // e.g. parameters for the specific method strategies ..
            // so ignore, and let the video sequence handler decide if it can be handled
        }

        try {
            if(currentHandler != null) {
                boolean ret = currentHandler.handleElement(qName, attributes);
                if(ret == false) {
                    Main.log(JCeliacLogger.LOG_ERROR, "Could not handle element (%s)", qName);
                }
            }
        } catch(Exception e) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Configuration file is invalid (%s)!", e.getMessage());
            throw new SAXException("Cannot handle XML Element: ");
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws
            SAXException
    {
        try {
            if(qName.compareToIgnoreCase(ConfigurationXMLHandler.CONFIG) == 0) // starts a test configuration file
            {
                //          System.out.println("Begin of TestConfiguration found, parsing file!");
            }
            if(qName.compareToIgnoreCase(ConfigurationXMLHandler.FEATURE_EXTRACTION) == 0) {
                //         System.out.println("Ending feature extraction parameters!");
                return;
            }
            if(qName.compareToIgnoreCase(ConfigurationXMLHandler.CLASSIFICATION) == 0) {
                //         System.out.println("Ending classification method parameters!");
                return;
            }
            if(qName.compareToIgnoreCase(ConfigurationXMLHandler.EXPERIMENT) == 0) {
                //         System.out.println("Experiment parameters ends!");
                this.finishedExperimentHandlers.add(this.currentHandler);
                // at this point we retrieve the parameters of this experiment
                // and are able to create the experiment
                this.prepareExperiments();

                // cleanup internal data for a possible next experiment within the config
                this.cleanup();
            }
        } catch(CloneNotSupportedException e) // this will not happen
        {
            throw new XMLParseException("Clone of XML-Handler is not supported! (Serious bug!!)");
        } catch(InitializeFailedException | XMLParseException e) {
            throw new XMLParseException(e.getLocalizedMessage());
        }
    }

    /**
     * Prepares experiments creating the correct parameters for all used
     * methods.
     * <p>
     * @throws XMLParseException
     * @throws CloneNotSupportedException
     * @throws InitializeFailedException
     */
    private void prepareExperiments()
            throws XMLParseException, CloneNotSupportedException,
                   InitializeFailedException
    {
        FeatureExtractionElementHandler localFEHandler = null;
        ClassificationMethodElementHandler localCMHandler = null;
        ExperimentElementHandler localExpHandler = null;
        OptimizationMethodElementHandler localOptHandler = null;
        GenerativeModelElementHandler localGenModHandler = null;
        ValidationMethodElementHandler localVMHandler = null;
        VideoSequenceHandlerElementHandler localVSHHandler = null;
        boolean isVideoExperiment = false;

        // get the five finished handlers
        for(XMLElementHandler handler : this.finishedExperimentHandlers) {
            if(handler instanceof FeatureExtractionElementHandler) {
                localFEHandler = (FeatureExtractionElementHandler) handler;
            }
            if(handler instanceof ClassificationMethodElementHandler) {
                localCMHandler = (ClassificationMethodElementHandler) handler;
            }
            if(handler instanceof ExperimentElementHandler) {
                localExpHandler = (ExperimentElementHandler) handler;
            }
            if(handler instanceof OptimizationMethodElementHandler) {
                localOptHandler = (OptimizationMethodElementHandler) handler;
            }
            if(handler instanceof GenerativeModelElementHandler) {
                localGenModHandler = (GenerativeModelElementHandler) handler;
            }
            if(handler instanceof ValidationMethodElementHandler) {
                localVMHandler = (ValidationMethodElementHandler) handler;
            }
            if(handler instanceof VideoSequenceHandlerElementHandler) {
                localVSHHandler = (VideoSequenceHandlerElementHandler) handler;
            }
        }
        // VSH handler is optional
        if(localFEHandler == null || localCMHandler == null || localExpHandler == null
           || localOptHandler == null || localVMHandler == null) {
            String missingParameters = "";
            if(localFEHandler == null) {
                missingParameters = "FeatureExtraction";
            }
            if(localCMHandler == null) {
                missingParameters += "ClassificationMethod";
            }
            if(localExpHandler == null) {
                missingParameters += " Experiment";
            }
            if(localOptHandler == null) {
                missingParameters += " OptimizationMethod";
            }
            if(localGenModHandler == null) {
                missingParameters += " GenericModel";
            }
            if(localVMHandler == null) {
                missingParameters += " ValidationMethod";
            }

            throw new XMLParseException("Parameters are missing ( " + missingParameters + " ).");
        }
        ArrayList<FeatureExtractionParameters> fParams = localFEHandler.getAllParameters();
        ArrayList<ClassificationParameters> cParams = localCMHandler.getAllParameters();
        ArrayList<ExperimentParameters> eParams = localExpHandler.getAllParameters();
        ArrayList<FeatureOptimizationParameters> oParams = localOptHandler.getAllParameters();
        ArrayList<GenerativeModelParameters> gmParams = localGenModHandler.getAllParameters();
        ArrayList <ValidationMethodParameters> valParams = localVMHandler.getAllParameters();

        ArrayList<VideoSequenceHandlerParameters> vParams;

        if(localVSHHandler != null) {
            isVideoExperiment = true;
            vParams = localVSHHandler.getAllParameters();
        } else {
            vParams = new ArrayList<>();
            vParams.add(new VideoSequenceHandlerParameters()); // default
        }

        for(int fParamsIndex = 0; fParamsIndex < fParams.size(); fParamsIndex++) {
            for(int cParamsIndex = 0; cParamsIndex < cParams.size(); cParamsIndex++) {
                for(int oParamsIndex = 0; oParamsIndex < oParams.size(); oParamsIndex++) {
                    for(int eParamsIndex = 0; eParamsIndex < eParams.size(); eParamsIndex++) {
                        for(int gmParamsIndex = 0; gmParamsIndex < gmParams.size(); gmParamsIndex++) {
                            for(int valParamsIndex = 0; valParamsIndex < valParams.size(); valParamsIndex++) {
                                for(int vParamsIndex = 0; vParamsIndex < vParams.size(); vParamsIndex++) {

                                    FeatureExtractionParameters feParameters = (FeatureExtractionParameters) fParams.get(fParamsIndex);
                                    ClassificationParameters cmParameters = (ClassificationParameters) cParams.get(cParamsIndex);
                                    ExperimentParameters expParameters = (ExperimentParameters) eParams.get(eParamsIndex);
                                    FeatureOptimizationParameters optParameters = (FeatureOptimizationParameters) oParams.get(oParamsIndex);
                                    GenerativeModelParameters gmParameters = (GenerativeModelParameters) gmParams.get(gmParamsIndex);
                                    VideoSequenceHandlerParameters vshParameters = (VideoSequenceHandlerParameters) vParams.get(vParamsIndex);
                                    ValidationMethodParameters valParameters =  valParams.get(valParamsIndex);
                                    
                                    expParameters.setFeatureExtractionMethodName(this.featureExtractionMethod);
                                    expParameters.setClassificationMethodName(this.classificationMethod);
                                    expParameters.setOptimizationMethodName(this.optimizationMethod);
                                    expParameters.setGenerativeModelName(this.generativeModel);
                                    expParameters.setValidationMethodName(this.validationMethod);

                                    expParameters.setOptimizationMethodParameters((FeatureOptimizationParameters) optParameters.clone());
                                    expParameters.setFeatureExtractionParameters((FeatureExtractionParameters) feParameters.clone());
                                    expParameters.setClassificationMethodParameters((ClassificationParameters) cmParameters.clone());
                                    expParameters.setVideoSequenceHandlerParameters((VideoSequenceHandlerParameters) vshParameters.clone());
                                    expParameters.setGenerativeModelParameters((GenerativeModelParameters) gmParameters.clone());
                                    expParameters.setValidationMethodParameters((ValidationMethodParameters)valParameters.clone());
                                    
                                    expParameters.setVideoSequenceExperiment(isVideoExperiment);
                                    this.experimentParametersByExperiment.add((ExperimentParameters) expParameters.clone());
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException
    {
        String content = new String(ch).substring(start, start + length);
        if(this.currentHandler != null) {
            this.currentHandler.handleCharacters(content.trim());
        }
    }

    public ExperimentParameters getExperimentParameters(int experimentNumber)
    {
        return this.experimentParametersByExperiment.get(experimentNumber);
    }

    /**
     * Returns the global test configuration parameters.
     * <p>
     * @return Global test configuration parameters.
     * <p>
     * @throws XMLParseException
     * @throws InitializeFailedException
     */
    public GlobalTestConfigurationParameters getGlobalTestConfigurationParameters()
            throws XMLParseException, InitializeFailedException
    {
        @SuppressWarnings("unchecked")
        ArrayList<GlobalTestConfigurationParameters> params = this.globalParameterHandler.getAllParameters();
        if(params.size() > 1) {
            throw new XMLParseException("Multiple Global Parameter sets returned!");
        }
        // there is only 1 valid global test configuration parameter object
        return params.get(0);
    }

    public int getExperimentCount()
    {
        return this.experimentParametersByExperiment.size();
    }

    public int getMainExperimentCount()
    {
        return mainExperimentCount;
    }
}
