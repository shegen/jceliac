/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.parameter.xml;

import jceliac.featureOptimization.FeatureOptimizationParametersSBS;
import jceliac.featureOptimization.FeatureOptimizationParameters;
import jceliac.featureOptimization.FeatureOptimizationParametersNULL;
import jceliac.featureOptimization.FeatureOptimizationParametersSFS;
import java.util.ArrayList;

/**
 * Handles the parsing of optimization method parameters.
 * <p>
 * @author shegen
 */
public class OptimizationMethodElementHandler
        extends XMLElementHandler<FeatureOptimizationParameters>
{

    private final String method;

    public OptimizationMethodElementHandler(String method)
    {
        super("param");
        this.method = method;
    }

    @Override
    public FeatureOptimizationParameters getParametersFor(ArrayList<ElementContent> localElements)
            throws XMLParseException
    {

        FeatureOptimizationParameters parameters = this.createParametersByMethod();
        for(ElementContent current : localElements) {
            if(parameters.hasOption(current.attributeField)) {
                parameters.setOption(current.attributeField, current.attributeContent);
            } else {
                throw new XMLParseException("Field is unknown for method (" + this.method + "): \"" + current.attributeField + "\".");
                //Main.log(JCeliacLogger.LOG_WARN, "Field is unknown for method (" + this.method + "): \"" + current.attributeField + "\".");
            }
        }
        return parameters;
    }

    /**
     * Creates the correct default object for optimization methods.
     * <p>
     * @return Default parameter object.
     * <p>
     * @throws XMLParseException
     */
    private FeatureOptimizationParameters createParametersByMethod()
            throws XMLParseException
    {
        FeatureOptimizationParameters parameters;

        if(this.method.equalsIgnoreCase("SFS")) {
            parameters = new FeatureOptimizationParametersSFS();
            return parameters;
        }
        if(this.method.equalsIgnoreCase("SBS")) {
            parameters = new FeatureOptimizationParametersSBS();
            return parameters;
        }
        if(this.method.equalsIgnoreCase("NULL")) {
            parameters = new FeatureOptimizationParametersNULL();
            return parameters;
        } else {
            throw new XMLParseException("Feature Optimization Method (" + method + ") not supported!");
        }
    }
}
