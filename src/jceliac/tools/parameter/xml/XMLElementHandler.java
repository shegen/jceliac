/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.parameter.xml;

import jceliac.experiment.*;
import jceliac.logging.*;
import org.xml.sax.*;
import java.util.*;
import jceliac.*;

/**
 * Base class for parsing xml parameters.
 * <p>
 * @author shegen
 * @param <T> Type of parameter object created.
 */
public abstract class XMLElementHandler<T>
{

    protected String elementIdentifier;
    protected ArrayList<ElementContent> elements;
    protected ElementContent currentElement;
    protected boolean iteratingParameters = false;

    protected XMLElementHandler(String elementIdentifier)
    {
        this.elementIdentifier = elementIdentifier;
        this.elements = new ArrayList<>();
    }

    public XMLElementHandler()
    {
    }

    public boolean accept(String elementIdentifier, Attributes attributes)
    {
        return this.elementIdentifier.compareTo(elementIdentifier) == 0;
    }

    public boolean handleElement(String qName, Attributes attributes)
            throws Exception
    {
        if(this.accept(qName, attributes)) {
            this.handle(attributes);
            return true;
        } else {
            return false;
        }
    }

    // used for the nested elements of strategies
    public void register(Attributes attributes)
    {
    }

    /**
     * This class is used for defining parameter optimizations in the experiment
     * configuration.
     */
    protected class ElementContent
    {

        public String qName;
        public String attributeField;
        public String attributeContent;
        public String range;
        public String stepping;
        public String baseValue;
        public String exponentBaseValue;
        public String exponentStepping;
        public boolean isIterating;
        public boolean isExponential;

        public int getNumberOfIterations()
        {
            if(this.isIterating == false) {
                return 1;
            } else {
                double r = Double.parseDouble(this.range);           // maximum range

                if(this.isExponential) {
                    double b = Double.parseDouble(this.baseValue);
                    double expFinal = (Math.log(r) / Math.log(b)); // exponent of final value
                    double expRange = expFinal - Double.parseDouble(this.exponentBaseValue);

                    return (int) (Math.ceil(expRange / Double.parseDouble(this.exponentStepping)));

                } else {
                    double s = Double.parseDouble(this.stepping);        // stepping size
                    return (int) (Math.ceil(r / s)) + 1; // add 1 because we start with the 0-th step
                }
            }
        }

        public ElementContent getRealization(int iteration)
        {
            ElementContent realization = new ElementContent();
            realization.isIterating = false;
            realization.qName = qName;
            realization.attributeField = attributeField;

            if(this.isExponential) {
                double expStepping = Double.parseDouble(this.exponentStepping);
                double expBase = Double.parseDouble(this.exponentBaseValue);
                double b = Double.parseDouble(this.baseValue);

                double realizedExponent = expBase + (iteration * expStepping);
                double value = Math.pow(b, realizedExponent);
                realization.attributeContent = String.valueOf(value);
                return realization;

            } else {
                double s = Double.parseDouble(this.stepping);        // stepping size
                double b = Double.parseDouble(this.attributeContent);        // base
                double value = (b + s * ((double) iteration));
                realization.attributeContent = String.valueOf(value);
                return realization;
            }
        }
    }

    protected void handle(Attributes attributes)
            throws Exception
    {

        this.currentElement = new ElementContent();
        this.currentElement.attributeField = attributes.getValue("field");
        this.currentElement.range = attributes.getValue("range");
        this.currentElement.stepping = attributes.getValue("stepping");
        this.currentElement.exponentBaseValue = attributes.getValue("exponentBaseValue");
        this.currentElement.exponentStepping = attributes.getValue("exponentStepping");
        this.currentElement.baseValue = attributes.getValue("baseValue");

        if(this.currentElement.attributeField == null) {
            //      Main.log(JCeliacLogger.LOG_ERROR, "XML File: Field not set!");
            this.currentElement = null;
            return;
        }
        if(this.currentElement.range != null && this.currentElement.stepping != null) {
            this.iteratingParameters = true;
            this.currentElement.isIterating = true;
            return;
        }
        if(this.currentElement.range != null && this.currentElement.stepping == null
           && this.currentElement.exponentBaseValue != null && this.currentElement.exponentStepping != null
           && this.currentElement.baseValue != null) {
            this.iteratingParameters = true;
            this.currentElement.isIterating = true;
            this.currentElement.isExponential = true;
        }
    }

    public void handleCharacters(String content)
    {
        if(this.currentElement != null) {
            this.currentElement.attributeContent = content;
            this.elements.add(this.currentElement);
        }
        this.currentElement = null;
    }

    public boolean hasIteratingParameters()
    {
        return this.iteratingParameters;
    }

    public int getNumberOfIterations()
    {
        int prod = 1;
        for(ElementContent cur : this.elements) {
            prod *= cur.getNumberOfIterations();
        }
        return prod;
    }

    /**
     * Creates parameters for a set of XML elements.
     * <p>
     * @param localElements XML elements defining the parameters.
     * <p>
     * @return Created parameters object.
     * <p>
     * @throws XMLParseException
     * @throws InitializeFailedException
     */
    public abstract T getParametersFor(ArrayList<ElementContent> localElements)
            throws XMLParseException, InitializeFailedException;

    /**
     * Creates a list of parameters.
     * <p>
     * @return List of created parameters object.
     * <p>
     * @throws XMLParseException
     * @throws InitializeFailedException
     */
    public ArrayList<T> getAllParameters()
            throws XMLParseException, InitializeFailedException
    {
        ArrayList<ElementContent> iteratedParameters;
        ArrayList<ElementContent> nonIteratedParameters;

        // count number of iterating parameters
        int prodIterations = 1;
        for(ElementContent current : this.elements) {
            if(current.isIterating) {
                prodIterations *= current.getNumberOfIterations();

            }
        }
        // setup two lists of parameters by type
        iteratedParameters = new ArrayList<>();
        nonIteratedParameters = new ArrayList<>();
        for(ElementContent current : this.elements) {
            if(current.isIterating) {
                iteratedParameters.add(current);
            } else {
                nonIteratedParameters.add(current);
            }
        }
        ArrayList<ArrayList<ElementContent>> iteratedRealizations = new ArrayList<>();
        for(ElementContent currentIt : iteratedParameters) {
            iteratedRealizations.add(getAllRealizations(currentIt));
        }
        ArrayList<ArrayList<ElementContent>> combinedParameters;
        combinedParameters = combineLists(iteratedRealizations, 0);
        if(prodIterations != combinedParameters.size()) {
            Main.log(JCeliacLogger.LOG_ERROR, "Number of combined methods is wrong!");
        }

        ArrayList<T> parameters = new ArrayList<>();
        for(int index = 0; index < combinedParameters.size(); index++) {
            ArrayList<ElementContent> newList = new ArrayList<>();
            ArrayList<ElementContent> combinedRealizations = combinedParameters.get(index);
            newList.addAll(nonIteratedParameters);
            newList.addAll(newList.size(), combinedRealizations);

            T param = this.getParametersFor(newList);
            parameters.add(param);
        }
        return parameters;
    }

    /**
     * Maps the iterated element to a non iterated element.
     */
    private ArrayList<ElementContent> getAllRealizations(ElementContent element)
    {
        ArrayList<ElementContent> realizations = new ArrayList<>();

        int iterations = element.getNumberOfIterations();
        for(int i = 0; i < iterations; i++) {
            ElementContent real = element.getRealization(i);
            realizations.add(real);
        }
        return realizations;
    }

    /**
     * Combines lists creating all possible permutations of parameters for
     * optimizing parameters.
     * <p>
     * @param lists Lists used.
     * @param step  Paraemter used for recursion.
     * <p>
     * @return All permutations of lists.
     */
    private ArrayList<ArrayList<ElementContent>> combineLists(ArrayList<ArrayList<ElementContent>> lists, int step)
    {
        if(step == lists.size()) {
            ArrayList<ArrayList<ElementContent>> result = new ArrayList<>();
            result.add(new ArrayList<>());
            return result;
        }

        ArrayList<ArrayList<ElementContent>> result = new ArrayList<>();
        ArrayList<ArrayList<ElementContent>> recursive = combineLists(lists, step + 1);

        for(int j = 0; j < lists.get(step).size(); j++) {
            for(int k = 0; k < recursive.size(); k++) {
                ArrayList<ElementContent> newList = new ArrayList<>();
                for(ElementContent content : recursive.get(k)) {
                    newList.add(content);
                }
                newList.add(lists.get(step).get(j));
                result.add(newList);
            }
        }
        return result;
    }
}
