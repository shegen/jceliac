/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.parameter.xml;

import jceliac.classification.classificationParameters.*;
import java.util.ArrayList;
import jceliac.experiment.*;
import jceliac.classification.kernelSVM.ClassificationParametersKernelSVM;

/**
 * Handles classification method parameter elements.
 * <p>
 * @author shegen
 */
public class ClassificationMethodElementHandler
        extends XMLElementHandler<ClassificationParameters>
{

    private final String method;

    public ClassificationMethodElementHandler(String method)
    {
        super("param");
        this.method = method;
    }

    @Override
    public ClassificationParameters getParametersFor(ArrayList<ElementContent> localElements)
            throws XMLParseException, InitializeFailedException
    {
        ClassificationParameters parameters = ClassificationMethodFactory.createParametersByMethod(this.method);
        for(ElementContent current : localElements) {
            if(parameters.hasOption(current.attributeField)) {
                parameters.setOption(current.attributeField, current.attributeContent);
            } else {
                throw new XMLParseException("Configuration parameter (" + current.attributeField + ") is invalid!");
            }
        }
        return parameters;
    }

    
}
