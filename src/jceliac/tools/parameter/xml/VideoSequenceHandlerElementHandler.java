/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.parameter.xml;

import jceliac.video.strategies.AbstractOutcomeCombinationStrategy.EOutcomeCombinationStrategy;
import jceliac.video.strategies.frameProcessing.FrameProcessingStrategyParametersDownsampling;
import jceliac.video.strategies.AbstractFeatureExtractionStrategy.EFeatureExtractionStrategy;
import jceliac.video.strategies.AbstractOutcomeProcessingStrategy.EOutcomeProcessingStrategy;
import jceliac.video.strategies.AbstractFrameProcessingStrategy.EFrameProcessingStrategy;
import jceliac.video.strategies.AbstractClassificationStrategy.EClassificationStrategy;
import jceliac.video.strategies.AbstractSegmentationStrategy.ESegmentationStrategy;
import jceliac.video.strategies.outcomeCombination.*;
import jceliac.experiment.InitializeFailedException;
import jceliac.video.strategies.featureExtraction.*;
import jceliac.video.strategies.outcomeProcessing.*;
import jceliac.video.strategies.classification.*;
import jceliac.video.strategies.segmentation.*;
import jceliac.video.strategies.parameters.*;
import jceliac.video.strategyHandler.*;
import org.xml.sax.Attributes;
import java.util.*;
import jceliac.Main;
import jceliac.logging.JCeliacLogger;

/**
 * Handles all the video sequence parameter parsing.
 * <p>
 * @author shegen
 */
public class VideoSequenceHandlerElementHandler
        extends XMLElementHandler<VideoSequenceHandlerParameters>
{

    public VideoSequenceHandlerElementHandler()
    {
        super("");
        this.classificationStrategyHandler = new ClassificationStrategyElementHandler();
        this.featureExtractionStrategyHandler = new FeatureExtractionStrategyElementHandler();
        this.frameProcessingStrategyHandler = new FrameProcessingStrategyElementHandler();
        this.outcomeCombinationStrategyHandler = new OutcomeCombinationStrategyElementHandler();
        this.segmentationStrategyHandler = new SegmentationStrategyElementHandler();
        this.outcomeProcessingStrategyHandler = new OutcomeProcessingStrategyElementHandler();

    }
    public final String featureExtractionStrategyQualifier = "FeatureExtractionStrategy";
    public final String classificationStrategyQualifier = "ClassificationStrategy";
    public final String frameProcessingStrategyQualifier = "FrameProcessingStrategy";
    public final String outcomeCombinationStrategyQualifier = "OutcomeCombinationStrategy";
    public final String segmentationStrategyQualifier = "SegmentationStrategy";
    public final String outcomeProcessingStrategyQualifier = "OutcomeProcessingStrategy";

    private ClassificationStrategyElementHandler classificationStrategyHandler;
    private FeatureExtractionStrategyElementHandler featureExtractionStrategyHandler;
    private FrameProcessingStrategyElementHandler frameProcessingStrategyHandler;
    private OutcomeCombinationStrategyElementHandler outcomeCombinationStrategyHandler;
    private SegmentationStrategyElementHandler segmentationStrategyHandler;
    private OutcomeProcessingStrategyElementHandler outcomeProcessingStrategyHandler;

    private XMLElementHandler currentHandler;

    private final ArrayList<Object> parameterList = new ArrayList<>();

    @Override
    @SuppressWarnings("unchecked")
    public VideoSequenceHandlerParameters getParametersFor(ArrayList<ElementContent> localElements)
            throws XMLParseException, InitializeFailedException
    {
        if(this.currentHandler != null) {
            parameterList.add(this.currentHandler.getParametersFor(localElements));
        }

        VideoSequenceHandlerParameters parameters = new VideoSequenceHandlerParameters();
        for(ElementContent current : localElements) {
            if(parameters.hasOption(current.attributeField)) {
                parameters.setOption(current.attributeField, current.attributeContent);
            } else {
                throw new XMLParseException("Field is unknown for Video Sequence Handler Parameters: \"" + current.attributeField + "\".");
            }
        }
        return parameters;
    }

    @Override
    public boolean accept(String elementIdentifier, Attributes attributes)
    {

        if(this.elementIdentifier.compareTo(elementIdentifier) == 0) {
            this.currentHandler = null;
            return true;
        } else if(elementIdentifier.compareTo(this.classificationStrategyQualifier) == 0) {
            this.currentHandler = this.classificationStrategyHandler;
            this.currentHandler.register(attributes);
            return true;
        } else if(elementIdentifier.compareTo(this.featureExtractionStrategyQualifier) == 0) {
            this.currentHandler = this.featureExtractionStrategyHandler;
            this.currentHandler.register(attributes);
            return true;
        } else if(elementIdentifier.compareTo(this.frameProcessingStrategyQualifier) == 0) {
            this.currentHandler = this.frameProcessingStrategyHandler;
            this.currentHandler.register(attributes);
            return true;
        } else if(elementIdentifier.compareTo(this.outcomeCombinationStrategyQualifier) == 0) {
            this.currentHandler = this.outcomeCombinationStrategyHandler;
            this.currentHandler.register(attributes);
            return true;
        } else if(elementIdentifier.compareTo(this.segmentationStrategyQualifier) == 0) {
            this.currentHandler = this.segmentationStrategyHandler;
            this.currentHandler.register(attributes);
            return true;
        } else if(elementIdentifier.compareTo(this.outcomeProcessingStrategyQualifier) == 0) {
            this.currentHandler = this.outcomeProcessingStrategyHandler;
            this.currentHandler.register(attributes);
            return true;
        } else {
            try {
                if(this.currentHandler != null) {
                    boolean ret = this.currentHandler.accept(elementIdentifier, attributes);
                    if(ret == true) {
                        this.currentHandler.handle(attributes);
                    }
                    return ret;
                } else {
                    return false;
                }
            } catch(Exception e) {
                Main.logException(JCeliacLogger.LOG_ERROR, e);
            }
            return false;
        }
    }

    @Override
    public void handleCharacters(String content)
    {
        if(this.currentHandler != null) {
            this.currentHandler.handleCharacters(content);
        } else {
            super.handleCharacters(content);
        }
    }

    /**
     * Creates the parameters for each strategy based on the XML parse.
     * <p>
     * @return List of parameters.
     * <p>
     * @throws jceliac.tools.parameter.xml.XMLParseException
     * @throws jceliac.experiment.InitializeFailedException
     */
    @Override
    public ArrayList<VideoSequenceHandlerParameters> getAllParameters() throws
            XMLParseException, InitializeFailedException
    {
        ArrayList<FeatureExtractionStrategyParameters> featureExtractionStrategyParameters
                                                       = this.featureExtractionStrategyHandler.getAllParameters();
        ArrayList<SegmentationStrategyParameters> segmentationStrategyParameters
                                                  = this.segmentationStrategyHandler.getAllParameters();
        ArrayList<ClassificationStrategyParameters> classificationStrategyParameters
                                                    = this.classificationStrategyHandler.getAllParameters();
        ArrayList<OutcomeCombinationStrategyParameters> outcomeCombinationStrategyParameters
                                                        = this.outcomeCombinationStrategyHandler.getAllParameters();
        ArrayList<FrameProcessingStrategyParameters> frameProcessingStrategyParameters
                                                     = this.frameProcessingStrategyHandler.getAllParameters();
        ArrayList<OutcomeProcessingStrategyParameters> outcomeProcessingStrategyParameters
                                                       = this.outcomeProcessingStrategyHandler.getAllParameters();

        // we currently do not support iterated parameters for strategies
        if(featureExtractionStrategyParameters.size() > 1 || segmentationStrategyParameters.size() > 1
           || classificationStrategyParameters.size() > 1 || outcomeCombinationStrategyParameters.size() > 1
           || frameProcessingStrategyParameters.size() > 1) {
            throw new XMLParseException("Optimizing Parameters are currently not supported for"
                                        + " strategy parameters!");
        }

        ArrayList<VideoSequenceHandlerParameters> videoSequenceHandlerParameters = super.getAllParameters();
        if(videoSequenceHandlerParameters.size() > 1) {
            throw new XMLParseException("Optimizing Parameters are currently not supported for"
                                        + " strategy parameters!");
        }
        // get first iteration of parameters, we do not support iterated parameters here
        VideoSequenceHandlerParameters tmp = (VideoSequenceHandlerParameters) videoSequenceHandlerParameters.get(0);
        tmp.setClassificationParameters((ClassificationStrategyParameters) classificationStrategyParameters.get(0));
        tmp.setFeatureExtractionParameters((FeatureExtractionStrategyParameters) featureExtractionStrategyParameters.get(0));
        tmp.setFrameProcessingParameters((FrameProcessingStrategyParameters) frameProcessingStrategyParameters.get(0));
        tmp.setOutcomeCombinationParameters((OutcomeCombinationStrategyParameters) outcomeCombinationStrategyParameters.get(0));
        tmp.setSegmentationParameters((SegmentationStrategyParameters) segmentationStrategyParameters.get(0));
        tmp.setOutcomeProcessingParameters((OutcomeProcessingStrategyParameters) outcomeProcessingStrategyParameters.get(0));
        videoSequenceHandlerParameters.set(0, tmp);
        return videoSequenceHandlerParameters;
    }
}

/**
 * Handles the parsing of the classification strategy.
 * <p>
 * @author shegen
 */
class ClassificationStrategyElementHandler
        extends XMLElementHandler<ClassificationStrategyParameters>
{

    private String strategy = "Default";

    @Override
    public ClassificationStrategyParameters getParametersFor(ArrayList<ElementContent> localElements)
            throws XMLParseException
    {
        ClassificationStrategyParameters parameters = createParametersByStrategy();
        for(ElementContent current : localElements) {
            if(parameters.hasOption(current.attributeField)) {
                parameters.setOption(current.attributeField, current.attributeContent);
            } else {
                throw new XMLParseException("Field is unknown for strategy (" + this.strategy + "): \"" + current.attributeField + "\".");
            }
        }
        return parameters;
    }

    private ClassificationStrategyParameters createParametersByStrategy() throws
            XMLParseException
    {
        EClassificationStrategy strat = EClassificationStrategy.valueOf(this.strategy);
        switch(strat) {
            case Null:
                return new ClassificationStrategyParametersNull();
            case SeparateFeatures:
            case Default:
                return new ClassificationStrategyParametersSeparateFeatures();
        }
        throw new XMLParseException("Unknown Classification Strategy!");
    }

    @Override
    public void register(Attributes attributes)
    {
        this.strategy = attributes.getValue("strategy");
    }

    public ClassificationStrategyElementHandler()
    {
        super("param");
    }
}

/**
 * Handles the parsing of the feature extraction strategy.
 * <p>
 * @author shegen
 */
class FeatureExtractionStrategyElementHandler
        extends XMLElementHandler<FeatureExtractionStrategyParameters>
{

    private String strategy = "Default";

    @Override
    public FeatureExtractionStrategyParameters getParametersFor(ArrayList<ElementContent> localElements)
            throws XMLParseException
    {
        FeatureExtractionStrategyParameters parameters = createParametersByStrategy();
        for(ElementContent current : localElements) {
            if(parameters.hasOption(current.attributeField)) {
                parameters.setOption(current.attributeField, current.attributeContent);
            } else {
                throw new XMLParseException("Field is unknown for strategy (" + this.strategy + "): \"" + current.attributeField + "\".");
            }
        }
        return parameters;
    }

    private FeatureExtractionStrategyParameters createParametersByStrategy()
            throws XMLParseException
    {
        EFeatureExtractionStrategy strat = EFeatureExtractionStrategy.valueOf(this.strategy);
        switch(strat) {
            case SeparateFramesSeparateSegments:
            case Default:
                return new FeatureExtractionStrategyParametersSFSS();
        }
        throw new XMLParseException("Unknown Feature Extraction Strategy!");
    }

    @Override
    public void register(Attributes attributes)
    {
        this.strategy = attributes.getValue("strategy");
    }

    public FeatureExtractionStrategyElementHandler()
    {
        super("param");
    }
}

/**
 * Handles the parsing of the frame processing strategy.
 * <p>
 * @author shegen
 */
class FrameProcessingStrategyElementHandler
        extends XMLElementHandler<FrameProcessingStrategyParameters>
{

    private String strategy = "Default";

    @Override
    public FrameProcessingStrategyParameters getParametersFor(ArrayList<ElementContent> localElements)
            throws XMLParseException
    {
        FrameProcessingStrategyParameters parameters = createParametersByStrategy();
        for(ElementContent current : localElements) {
            if(parameters.hasOption(current.attributeField)) {
                parameters.setOption(current.attributeField, current.attributeContent);
            } else {
                throw new XMLParseException("Field is unknown for strategy (" + this.strategy + "): \"" + current.attributeField + "\".");
            }
        }
        return parameters;
    }

    private FrameProcessingStrategyParameters createParametersByStrategy()
            throws XMLParseException
    {
        EFrameProcessingStrategy strat = EFrameProcessingStrategy.valueOf(this.strategy);
        switch(strat) {
            case Downsample:  // default parameters
            case Default:
                return new FrameProcessingStrategyParametersDownsampling();

        }
        throw new XMLParseException("Unknown Frame Processing Strategy!");
    }

    @Override
    public void register(Attributes attributes)
    {
        this.strategy = attributes.getValue("strategy");
    }

    public FrameProcessingStrategyElementHandler()
    {
        super("param");
    }
}

/**
 * Handles the parsing of the outcome combination strategy.
 * <p>
 * @author shegen
 */
class OutcomeCombinationStrategyElementHandler
        extends XMLElementHandler<OutcomeCombinationStrategyParameters>
{

    private String strategy = "Default";

    @Override
    public OutcomeCombinationStrategyParameters getParametersFor(ArrayList<ElementContent> localElements)
            throws XMLParseException
    {
        OutcomeCombinationStrategyParameters parameters = createParametersByStrategy();
        for(ElementContent current : localElements) {
            if(parameters.hasOption(current.attributeField)) {
                parameters.setOption(current.attributeField, current.attributeContent);
            } else {
                throw new XMLParseException("Field is unknown for strategy (" + this.strategy + "): \"" + current.attributeField + "\".");
            }
        }
        return parameters;
    }

    private OutcomeCombinationStrategyParameters createParametersByStrategy()
            throws XMLParseException
    {
        EOutcomeCombinationStrategy strat = EOutcomeCombinationStrategy.valueOf(this.strategy);
        switch(strat) {
            case MajorityVote:  // default parameters
            case Default:
                return new OutcomeCombinationStrategyParametersMajorityVote();
            case Null:
                return new OutcomeCombinationStrategyParametersNull();

        }
        throw new XMLParseException("Unknown Outcome Combination Strategy!");
    }

    @Override
    public void register(Attributes attributes)
    {
        this.strategy = attributes.getValue("strategy");
    }

    public OutcomeCombinationStrategyElementHandler()
    {
        super("param");
    }

}

/**
 * Handles the parsing for the segmentation strategy.
 * <p>
 * @author shegen
 */
class SegmentationStrategyElementHandler
        extends XMLElementHandler<SegmentationStrategyParameters>
{

    private String strategy = "Default";

    @Override
    public SegmentationStrategyParameters getParametersFor(ArrayList<ElementContent> localElements)
            throws XMLParseException
    {
        SegmentationStrategyParameters parameters = createParametersByStrategy();
        for(ElementContent current : localElements) {
            if(parameters.hasOption(current.attributeField)) {
                parameters.setOption(current.attributeField, current.attributeContent);
            } else {
                throw new XMLParseException("Field is unknown for strategy (" + this.strategy + "): \"" + current.attributeField + "\".");
            }
        }
        return parameters;
    }

    private SegmentationStrategyParameters createParametersByStrategy() throws
            XMLParseException
    {
        ESegmentationStrategy strat = ESegmentationStrategy.valueOf(this.strategy);
        switch(strat) {

            case Default:
                return new SegmentationStrategyParametersSingleSegment();

        }
        throw new XMLParseException("Unknown Segmentation Strategy!");
    }

    @Override
    public void register(Attributes attributes)
    {
        this.strategy = attributes.getValue("strategy");
    }

    public SegmentationStrategyElementHandler()
    {
        super("param");
    }
}

/**
 * Handles the parsing of the outcome processing strategy.
 * <p>
 * @author shegen
 */
class OutcomeProcessingStrategyElementHandler
        extends XMLElementHandler<OutcomeProcessingStrategyParameters>
{

    private String strategy = "Default";

    @Override
    public OutcomeProcessingStrategyParameters getParametersFor(ArrayList<ElementContent> localElements)
            throws XMLParseException
    {
        OutcomeProcessingStrategyParameters parameters = createParametersByStrategy();
        for(ElementContent current : localElements) {
            if(parameters.hasOption(current.attributeField)) {
                parameters.setOption(current.attributeField, current.attributeContent);
            } else {
                throw new XMLParseException("Field is unknown for strategy (" + this.strategy + "): \"" + current.attributeField + "\".");
            }
        }
        return parameters;
    }

    private OutcomeProcessingStrategyParameters createParametersByStrategy()
            throws XMLParseException
    {
        EOutcomeProcessingStrategy strat = EOutcomeProcessingStrategy.valueOf(this.strategy);
        switch(strat) {
            case DisplayFeatures:
                return new OutcomeProcessingStrategyParametersDisplayFeatures();
            case Null:
            case Default:
                return new OutcomeProcessingStrategyParametersNull();
        }
        throw new XMLParseException("Unknown Outcome Processing Strategy!");
    }

    @Override
    public void register(Attributes attributes)
    {
        this.strategy = attributes.getValue("strategy");
    }

    public OutcomeProcessingStrategyElementHandler()
    {
        super("param");
    }
}
