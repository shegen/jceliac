/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.parameter.xml;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jceliac.JCeliacGenericException;
import jceliac.experiment.InitializeFailedException;
import jceliac.validation.generic.ValidationMethodFactory;
import jceliac.validation.generic.ValidationMethodParameters;
/**
 *
 * @author shegen
 */
public class ValidationMethodElementHandler
        extends XMLElementHandler <ValidationMethodParameters>
{

    private final String method;

    public ValidationMethodElementHandler(String method)
    {
        super("param");
        this.method = method;
    }

    @Override
    public ValidationMethodParameters getParametersFor(ArrayList<ElementContent> localElements)
            throws XMLParseException, InitializeFailedException
    {
        ValidationMethodParameters parameters;
        try {
            parameters = this.createParametersByMethod();
        } catch(JCeliacGenericException ex) {
            throw new InitializeFailedException("Cannot create parameters for validation method ("+this.method+").");
        }
        for(ElementContent current : localElements) {
            if(parameters.hasOption(current.attributeField)) {
                parameters.setOption(current.attributeField, current.attributeContent);
            } else {
                throw new XMLParseException("Configuration parameter (" + current.attributeField + ") is invalid!");
            }
        }
        return parameters;
    }

    /**
     * Creates default classification method parameters based on method type.
     * <p>
     * @return Default classification method parameters.
     * <p>
     * @throws jceliac.tools.parameter.xml.XMLParseException
     * @throws jceliac.JCeliacGenericException
     */
    public ValidationMethodParameters createParametersByMethod() 
            throws XMLParseException, JCeliacGenericException
    {
        return ValidationMethodFactory.createParametersByMethod(this.method);
    }

}
