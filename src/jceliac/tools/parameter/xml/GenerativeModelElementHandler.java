/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.parameter.xml;

import java.util.ArrayList;
import jceliac.Main;
import jceliac.classification.model.GenerativeModelFactory;
import jceliac.classification.model.GenerativeModelParameters;
import jceliac.experiment.InitializeFailedException;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
public class GenerativeModelElementHandler 
    extends XMLElementHandler<GenerativeModelParameters>
{

    private final String method;

    public GenerativeModelElementHandler(String method)
    {
        super("param");
        this.method = method;
    }
    
    @Override
    public GenerativeModelParameters getParametersFor(ArrayList<ElementContent> localElements)
            throws XMLParseException, InitializeFailedException
    {
        GenerativeModelParameters parameters = GenerativeModelFactory.createParametersByMethod(this.method);
        for(ElementContent current : localElements) {
            if(parameters.hasOption(current.attributeField)) {
                parameters.setOption(current.attributeField, current.attributeContent);
            } else {
                //throw new XMLParseException("Field is unknown for method (" + this.method + "): \"" + current.attributeField + "\".");
                Main.log(JCeliacLogger.LOG_WARN, "Field is unknown for method (" + this.method + "): \"" + current.attributeField + "\".");
            }
        }
        return parameters;
    }
    
  
    
}
