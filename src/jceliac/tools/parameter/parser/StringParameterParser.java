/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.parameter.parser;

import jceliac.featureOptimization.FeatureOptimizationParametersSBS;
import jceliac.featureOptimization.FeatureOptimizationParametersNULL;
import jceliac.featureOptimization.FeatureOptimizationParametersSFS;
import jceliac.featureExtraction.COOCLBP.FeatureExtractionParametersCOOCLBP;
import jceliac.featureExtraction.WTLBP.FeatureExtractionParametersWTLBP;
import jceliac.featureExtraction.LBPC.FeatureExtractionParametersLBPC;
import jceliac.featureExtraction.ELBP.FeatureExtractionParametersELBP;
import jceliac.featureExtraction.ELTP.FeatureExtractionParametersELTP;
import jceliac.featureExtraction.LBP.FeatureExtractionParametersLBP;
import jceliac.featureExtraction.LTP.FeatureExtractionParametersLTP;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureOptimization.FeatureOptimizationParameters;
import jceliac.classification.classificationParameters.ClassificationParametersSVM;
import jceliac.classification.classificationParameters.ClassificationParametersKNN;
import jceliac.featureExtraction.FeatureExtractionMethod.EFeatureExtractionMethod;
import jceliac.classification.classificationParameters.ClassificationParameters;
import jceliac.classification.EClassificationMethod;
import jceliac.experiment.InitializeFailedException;
import jceliac.JCeliacGenericException;
import jceliac.Main;
import jceliac.featureOptimization.FeatureOptimizationMethod.EFeatureOptimizationMethod;
import jceliac.logging.JCeliacLogger;

/**
 * This implements a Matlab compatible form of parsing the parameter strings.
 * e.g.:
 * "o_uniform_patterns:1;o_use_color:1;o_minscale:1;o_maxscale:3;o_bin_base:4;
 * o_redo_test_name:ELS_WTLBP_BULB_2C"
 * <p>
 * @author shegen
 */
public class StringParameterParser
        extends ParameterParser
{

    @Override
    public ClassificationParameters parseClassificationMethodParameters(Object o, EClassificationMethod m)
            throws JCeliacGenericException
    {
        String paramString = (String) o;
        return doParseClassificationParams(paramString, m);
    }

    @Override
    public FeatureExtractionParameters parseFeatureExtractionParameters(Object o, EFeatureExtractionMethod m)
            throws JCeliacGenericException
    {
        String paramString = (String) o;
        return doParseFeatureExtractionParams(paramString, m);
    }

    /**
     * Creates default optimization parameters, does not use user supplied
     * strings.
     * <p>
     * @param o null.
     * @param m Optimization parameters.
     * <p>
     * @return Default optimization parameters.
     * <p>
     * @throws JCeliacGenericException
     */
    @Override
    public FeatureOptimizationParameters parseOptimizationMethodParameters(Object o, 
                                                                    EFeatureOptimizationMethod m)
            throws JCeliacGenericException
    {
        // create default
        switch(m) {
            case NULL:
                return new FeatureOptimizationParametersNULL();

            case SFS:
                return new FeatureOptimizationParametersSFS();

            case SBS:
                return new FeatureOptimizationParametersSBS();
        }
        throw new InitializeFailedException("Unknown Optimization Method");
    }

    /**
     * Parses user specified classification parameter string.
     * <p>
     * @param paramString User supplied parameter string.
     * @param m           Classification method type.
     * <p>
     * @return Classification method parameter object.
     * <p>
     * @throws JCeliacGenericException
     */
    private ClassificationParameters doParseClassificationParams(String paramString,
                                                                 EClassificationMethod m)
            throws JCeliacGenericException
    {
        String[] splitString;
        ClassificationParameters parameters;

        switch(m) {
            case KNN:
                parameters = new ClassificationParametersKNN();
                break;

            case KERNEL_SVM:
                parameters = new ClassificationParametersSVM();
                break;

            default:
                throw new JCeliacGenericException("Unknown Feature Extraction Method!");

        }
        if(paramString == null) //  unused
        {
            return parameters;
        }
        splitString = paramString.split("\\:"); // index 2*i = option, 2*i+1 = value

        for(int i = 0; i < splitString.length; i += 2) {
            String option = splitString[i];
            String value = null;

            if(splitString.length < (i + 1)) {
                value = splitString[i + 1];
            }

            if(value != null) {
                if(parameters.hasOption(option)) {

                    parameters.setOption(option, value);
                } else {
                    Main.log(JCeliacLogger.LOG_WARN, "WARNING: Unknown option encountered: " + option);
                    Main.log(JCeliacLogger.LOG_WARN, "Ignoring this option!");
                }
            } else {
                Main.log(JCeliacLogger.LOG_WARN, "WARNING: Empty option encountered: " + option);
                Main.log(JCeliacLogger.LOG_WARN, "Ignoring this option!");
            }

        }
        return parameters;

    }

    /**
     * Parses user specified feature extraction parameters string.
     * <p>
     * @param o String with parameters.
     * @param m Feature extraction method type.
     * <p>
     * @return Feature extraction method parameter object.
     * <p>
     * @throws JCeliacGenericException
     */
    private FeatureExtractionParameters doParseFeatureExtractionParams(String paramString,
                                                                       EFeatureExtractionMethod m)
            throws JCeliacGenericException
    {

        String[] splitString;
        FeatureExtractionParameters parameters;

        switch(m) {
            case LBP:
                parameters = new FeatureExtractionParametersLBP();
                break;

            case LBPC:
                parameters = new FeatureExtractionParametersLBPC();
                break;

            case ELBP:
                parameters = new FeatureExtractionParametersELBP();
                break;

            case LTP:
                parameters = new FeatureExtractionParametersLTP();
                break;

            case ELTP:
                parameters = new FeatureExtractionParametersELTP();
                break;

            case WTLBP:
                parameters = new FeatureExtractionParametersWTLBP();
                break;

            case COOCLBP:
                parameters = new FeatureExtractionParametersCOOCLBP();
                break;

            case COOCFLBP:
                parameters = new FeatureExtractionParametersCOOCLBP();
                break;

            default:
                throw new JCeliacGenericException("Unknown Feature Extraction Method!");

        }
        if(paramString == null) //  unused
        {
            return parameters;
        }
        splitString = paramString.split("\\:"); // index 2*i = option, 2*i+1 = value

        for(int i = 0; i < splitString.length; i += 2) {
            String option = splitString[i];
            String value = null;

            if(splitString.length > (i + 1)) {
                value = splitString[i + 1];
            }

            if(value != null) {
                if(parameters.hasOption(option)) {

                    parameters.setOption(option, value);
                } else {
                    Main.log(JCeliacLogger.LOG_WARN, "WARNING: Unknown option encountered: " + option);
                    Main.log(JCeliacLogger.LOG_WARN, "Ignoring this option!");
                }
            } else {
                Main.log(JCeliacLogger.LOG_WARN, "WARNING: Empty option encountered: " + option);
                Main.log(JCeliacLogger.LOG_WARN, "Ignoring this option!");
            }

        }
        return parameters;
    }
}
