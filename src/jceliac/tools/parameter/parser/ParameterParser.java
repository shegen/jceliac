/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.parameter.parser;

import jceliac.featureOptimization.FeatureOptimizationParameters;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.FeatureExtractionMethod.EFeatureExtractionMethod;
import jceliac.classification.classificationParameters.ClassificationParameters;
import jceliac.classification.EClassificationMethod;
import jceliac.JCeliacGenericException;
import jceliac.featureOptimization.FeatureOptimizationMethod.EFeatureOptimizationMethod;

/**
 * Base class for parsing user supplied parameters.
 * <p>
 * @author shegen
 */
public abstract class ParameterParser
{

    public abstract FeatureExtractionParameters parseFeatureExtractionParameters(Object o, EFeatureExtractionMethod m)
            throws JCeliacGenericException;

    public abstract ClassificationParameters parseClassificationMethodParameters(Object o, EClassificationMethod m)
            throws JCeliacGenericException;

    public abstract FeatureOptimizationParameters parseOptimizationMethodParameters(Object o, EFeatureOptimizationMethod m)
            throws JCeliacGenericException;
}
