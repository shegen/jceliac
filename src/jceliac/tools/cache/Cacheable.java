/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.cache;

import java.util.function.Supplier;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.Externalizable;
import java.io.File;
import jceliac.JCeliacGenericException;

/**
 *
 * @author shegen
 */
public interface Cacheable
        extends Externalizable
{

    default public String computeHashCode(Supplier<String> s)
    {
        return s.get();
    }

    default public void readFromDisk(File outFile)
            throws JCeliacGenericException
    {
        try(ObjectInputStream objInStream = new ObjectInputStream(new FileInputStream(outFile))) {
            this.readExternal(objInStream);
        } catch(Exception e) {
            throw new JCeliacGenericException(e);
        }
    }

    default public void writeToDisk(File outFile)
            throws JCeliacGenericException
    {
        try(ObjectOutputStream objOutStream = new ObjectOutputStream(new FileOutputStream(outFile))) {
            this.writeExternal(objOutStream);
        } catch(Exception e) {
            throw new JCeliacGenericException(e);
        }
    }

    public int getConsumedMemorySize();
}
