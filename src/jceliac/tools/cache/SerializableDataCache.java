/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.cache;

import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Arrays;
import java.util.Set;
import java.io.File;
import jceliac.JCeliacGenericException;

/**
 * This class is used to cache filtered images or data in general. The reason is
 * that in some cases the filtering does not change between separate runs during
 * experimentation but takes a long time to compute. We use the cache to speed
 * up experimentation.
 * <p>
 * @author shegen
 */
public class SerializableDataCache
{

    // consumption in bytes
    private int MAX_MEMORY_CONSUMPTION = 1024 * 1024 * 500; // 500 MB
    // store where each feature is stored
    private HashMap<String, File> cachedDataFiles = new HashMap<>();
    private HashMap<String, Cacheable> cachedData = new HashMap<>();
    private final int cacheID;
    private final Class<?> type;
    private final String parentDirectory;

    @SuppressWarnings("unchecked")
    public SerializableDataCache(String parentDirectory, int cacheID, Class<?> type)
            throws JCeliacGenericException
    {
        this.parentDirectory = parentDirectory;
        this.cacheID = cacheID;
        this.type = type;

        // make sure the type implements the cacheable interface
        Class<?>[] interfaces = type.getInterfaces();
        boolean implementsCacheable = Arrays.asList(interfaces).parallelStream()
                .anyMatch((a) -> a.getName().compareTo("jceliac.tools.cache.Cacheable") == 0);

        boolean superclassImplementsCacheable = Arrays.asList(type.getSuperclass().getInterfaces()).parallelStream()
                .anyMatch((a) -> a.getName().compareTo("jceliac.tools.cache.Cacheable") == 0);
        if(implementsCacheable == false && superclassImplementsCacheable == false) {
            throw new JCeliacGenericException("Cached Class Type must implement the Chacheable Interface!");
        }

        // find previously cached data
        try(ObjectInputStream objIn
                              = new ObjectInputStream(
                new FileInputStream(
                        new File(this.parentDirectory + File.separatorChar + this.cacheID + ".ser")))) {
                    this.cachedDataFiles = (HashMap<String, File>) objIn.readObject();
                } catch(Exception e) {
                    throw new JCeliacGenericException(e);
                }

    }

    public void purge()
    {
        this.cachedData = new HashMap<>();
        this.cachedDataFiles = new HashMap<>();

    }

    /**
     * Stores all cached data to the disk and also stores the cache file
     * hashmap.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public void close()
            throws JCeliacGenericException
    {
        // store all in memory cache to the disc
        Set<String> keys = this.cachedData.keySet();

        for(String key : keys) {
            Cacheable c = this.cachedData.get(key);
            this.writeCachedData(c, key);
        }

        try(ObjectOutputStream objOut = new ObjectOutputStream(
                new FileOutputStream(
                        new File(this.parentDirectory + File.separatorChar + this.cacheID + ".ser")))) {
                    objOut.writeObject(this.cachedDataFiles);
                } catch(Exception e) {
                    throw new JCeliacGenericException(e);
                }
    }

    public void setMaxMemoryConsumption(int max)
    {
        this.MAX_MEMORY_CONSUMPTION = max;
    }

    /**
     * Used to check if data is cached.
     * <p>
     * @param cacheHashCode Hash code of data.
     * <p>
     * @return True if data with hash code exists, false else.
     */
    public synchronized boolean containsData(String cacheHashCode)
    {
        return this.cachedData.containsKey(cacheHashCode)
               || this.cachedDataFiles.containsKey(cacheHashCode);
    }

    /**
     * Caches data.
     * <p>
     * @param data Cacheable data.
     * @param s    Used to compute hash code.
     * <p>
     * @return Hash code of hashed data.
     * <p>
     * @throws JCeliacGenericException
     */
    public synchronized String cacheData(Cacheable data, Supplier<String> s)
            throws JCeliacGenericException
    {
        String hashCode = data.computeHashCode(s);
        if(this.cachedData.containsKey(hashCode) || this.cachedDataFiles.containsKey(hashCode)) {
            throw new JCeliacGenericException("Allready cached a value for that key, possible collision!");
        }
        this.cachedData.put(hashCode, data);
        Set<String> keys = this.cachedData.keySet();
        Iterator<String> it = keys.iterator();

        while(this.getMemoryConsumption() > MAX_MEMORY_CONSUMPTION && it.hasNext()) {
            String key = it.next();
            this.writeCachedData(this.cachedData.get(key), key);
            this.cachedData.remove(key);
        }
        return hashCode;
    }

    /**
     * Retrieves data from cache.
     * <p>
     * @param cacheHashCode Hash code of data.
     * <p>
     * @return Cached data if exists.
     * <p>
     * @throws JCeliacGenericException If no data with hash code exists.
     */
    public synchronized Cacheable retrieveData(String cacheHashCode)
            throws JCeliacGenericException
    {
        if(this.cachedData.containsKey(cacheHashCode)) {
            return this.cachedData.get(cacheHashCode);
        }
        // try to load from fs, figure out if this exists
        if(this.cachedDataFiles.containsKey(cacheHashCode) == false) {
            throw new JCeliacGenericException("Data is not Cached!");
        }
        File serFile = this.cachedDataFiles.get(cacheHashCode);
        return this.loadFromDisk(serFile);
    }

    /**
     * Returns the consumed memory by the cache.
     * <p>
     * @return Consumed memory.
     */
    private int getMemoryConsumption()
    {
        // compute total memory consumption of all cached elements
        int totalMemoryConsumption
            = this.cachedData.values().parallelStream().map(a -> a.getConsumedMemorySize())
                .collect(Collectors.toList()).parallelStream()
                .reduce(0, (a, b) -> a + b).intValue();
        return totalMemoryConsumption;
    }

    /**
     * Writes cached data to disk.
     * <p>
     * @param cData    Cache.
     * @param hashCode
     * <p>
     * @throws JCeliacGenericException
     */
    private void writeCachedData(Cacheable cData, String hashCode)
            throws JCeliacGenericException
    {
        File outFile = new File(this.parentDirectory + File.separatorChar + this.cacheID + "-" + hashCode + ".ser");
        cData.writeToDisk(outFile);
        this.cachedDataFiles.put(hashCode, outFile);
    }

    /**
     * Loads cachaed data from disk.
     * <p>
     * @param serFile File of cache.
     * <p>
     * @return Cacheable object of cached data.
     * <p>
     * @throws JCeliacGenericException
     */
    private Cacheable loadFromDisk(File serFile)
            throws JCeliacGenericException
    {
        try {
            Object t = type.newInstance();
            ((Cacheable) t).readFromDisk(serFile);
            return ((Cacheable) t);
        } catch(IllegalAccessException | InstantiationException | JCeliacGenericException e) {
            throw new JCeliacGenericException(e);
        }

    }
}
