/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at> 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.tools.math;

import org.apache.commons.math3.stat.regression.SimpleRegression;
import jceliac.JCeliacGenericException;

/**
 * Implementation of matlabs polyfitSlope. 
 * 
 * @author shegen
 */
public class PolyFit
{

    public static double fitSlope(double [] x, double [] y)
            throws JCeliacGenericException
    {
        if(x.length != y.length) {
            throw new JCeliacGenericException("PolyFit: Data dimensions mismatch!");
        }
        SimpleRegression fitter = new SimpleRegression();
        for(int index = 0; index < x.length; index++) {
            fitter.addData(x[index], y[index]);
        }
        double slope = fitter.getSlope();
        return slope;
    }
     
    public static double [] fitSlopeAndIntercept(double [] x, double [] y)
            throws JCeliacGenericException
    {
        if(x.length != y.length) {
            throw new JCeliacGenericException("PolyFit: Data dimensions mismatch!");
        }
        SimpleRegression fitter = new SimpleRegression();
        for(int index = 0; index < x.length; index++) {
            fitter.addData(x[index], y[index]);
        }
        double slope = fitter.getSlope();
        double intercept = fitter.getIntercept();
        return new double [] {slope, intercept};
    }
    
   
    private static double [][] createVandermondeMatrix(double [] x, int n) 
    {
        double [][] V = new double[x.length][n+1];
        for(int j = 0; j < V.length; j++) {
            V[j][0] = 1;
        }
        for(int i = 0; i < x.length; i++) {
            for(int j = 1; j < V[0].length; j++) {
                V[i][j] = V[i][j-1] * x[i];
            }
        }
        return V;   
    }
    
    
}
