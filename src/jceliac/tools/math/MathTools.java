/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.math;

import java.util.*;
import jceliac.JCeliacGenericException;
import jceliac.experiment.Experiment;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.transforms.dtcwt.ComplexArray2D;

/**
 * Collection of math tools.
 * <p>
 * @author shegen
 */
public class MathTools
{

    /**
     * Computes the standard deviation of a list of doubles.
     * <p>
     * @param list List of doubles.
     * <p>
     * @return Standard deviation of list.
     */
    public static double stdev(ArrayList<Double> list)
    {
        double m = 0;
        for(Double d : list) {
            m += d;
        }
        m = (m / list.size());

        double std = 0;
        for(Double d : list) {
            std += ((d - m) * (d - m));
        }
        return Math.sqrt(std / (list.size() - 1));
    }

    /**
     * Computes the standard deviation of a list of longs.
     * <p>
     * @param list List of doubles.
     * <p>
     * @return Standard deviation of list.
     */
    public static double stdevL(ArrayList<Long> list)
    {
        double m = 0;
        for(Long d : list) {
            m += d;
        }
        m = (m / list.size());

        double std = 0;
        for(Long d : list) {
            std += ((d - m) * (d - m));
        }
        return Math.sqrt(std / (list.size() - 1));
    }

    /**
     * Computes the standard deviation of a double array.
     * <p>
     * @param data Double array.
     * <p>
     * @return Standard deviation of double array.
     */
    public static double stdev(double[] data)
    {
        double m = 0;
        for(int i = 0; i < data.length; i++) {
            m += data[i];
        }
        m = (m / data.length);

        double std = 0;
        for(int i = 0; i < data.length; i++) {
            double tmp = data[i];
            std += ((tmp - m) * (tmp - m));
        }
        return Math.sqrt(std / (data.length - 1));
    }

    /**
     * Computes the standard deviation of a 2D double array.
     * <p>
     * @param data 2D Double array.
     * <p>
     * @return Standard deviation of double array.
     */
    public static double stdev(double[][] data)
    {

        double mean = 0;
        double count = 0;
        double sum = 0;
        for(double[] tmp : data) {
            for(int y = 0; y < data[0].length; y++) {
                mean += tmp[y];
                count++;
            }
        }
        mean = (mean / count);
        for(double[] tmpData : data) {
            for(int y = 0; y < data[0].length; y++) {
                double tmp = tmpData[y];
                sum += (tmp - mean) * (tmp - mean);
            }
        }
        double stdev = Math.sqrt(sum / (count - 1));
        return stdev;
    }
    
   /**
     * Computes the absolute standard deviation of a 2D double array.
     * <p>
     * @param data 2D Double array.
     * <p>
     * @return Standard deviation of double array.
     */
    public static double absStdev(double[][] data)
    {

        double mean = 0;
        double count = 0;
        double sum = 0;
        for(double[] tmp : data) {
            for(int y = 0; y < data[0].length; y++) {
                mean += Math.abs(tmp[y]);
                count++;
            }
        }
        mean = (mean / count);
        for(double[] tmpData : data) {
            for(int y = 0; y < data[0].length; y++) {
                double tmp = Math.abs(tmpData[y]);
                sum += (tmp - mean) * (tmp - mean);
            }
        }
        double stdev = Math.sqrt(sum / (count - 1));
        return stdev;
    }

    /**
     * Computes the mean of a 2D double array.
     * <p>
     * @param data 2D double array.
     * <p>
     * @return Mean of data.
     */
    public static double mean(double[][] data)
    {
        double sum = 0;
        for(double[] tmp : data) {
            for(int y = 0; y < data[0].length; y++) {
                sum += tmp[y];
            }
        }
        return sum / (data.length * data[0].length);
    }
    
   /**
     * Computes the absolute mean of a 2D double array.
     * <p>
     * @param data 2D double array.
     * <p>
     * @return Mean of data.
     */
    public static double absMean(double[][] data)
    {
        double sum = 0;
        for(double[] tmp : data) {
            for(int y = 0; y < data[0].length; y++) {
                sum += Math.abs(tmp[y]);
            }
        }
        return sum / (data.length * data[0].length);
    }

    /**
     * Computes the mean of a 1D double array.
     * <p>
     * @param data 2D double array.
     * <p>
     * @return Mean of data.
     */
    public static double mean(double[] data)
    {
        double sum = 0;

        for(int x = 0; x < data.length; x++) {
            sum += data[x];
        }
        return sum / (data.length);
    }

    /**
     * Converts the values in double array to the absolute value.
     * <p>
     * @param data 2D double array.
     */
    public static void abs(double[][] data)
    {
        for(double[] tmp : data) {
            for(int y = 0; y < data[0].length; y++) {
                tmp[y] = Math.abs(tmp[y]);
            }
        }
    }

    /**
     * Computes the maximum value of a double array.
     * <p>
     * @param data Double array.
     * <p>
     * @return Maximum value of data.
     */
    public static double max(double[] data)
    {
        double max = Double.MIN_VALUE;
        for(int index = 0; index < data.length; index++) {
            if(data[index] > max) {
                max = data[index];
            }
        }
        return max;
    }

    /**
     * Computes the cantor tuple of two integers.
     * <p>
     * @param x Integer one.
     * @param y Integer two.
     * <p>
     * @return Cantor tuple representation of one and two.
     */
    public static long cantorTupel(long x, long y)
    {
        long xd, yd;

        xd =  x;
        yd =  y;
        return (long) (0.5 * (xd + yd) * (xd + yd + 1) + yd);
    }

    /**
     * Computes the trace of a squared matrix.
     * <p>
     * @param matrix Squared matrix.
     * <p>
     * @return Trace of matrix.
     */
    public static double trace(double[][] matrix)
    {
        double trace = 0;
        for(int i = 0; i < matrix.length; i++) {
            trace = matrix[i][i] + matrix[i][i];
        }
        return trace;
    }

    /**
     * Computes the determinant of a 2x2 matrix.
     * <p>
     * @param matrix 2x2 matrix.
     * <p>
     * @return Determinant of matrix.
     */
    public static double determinant(double[][] matrix)
    {
        double det;
        det = matrix[0][0] * matrix[1][1] - (matrix[0][1] * matrix[1][0]);
        return det;
    }

    /**
     * Computes the median of a double array.
     * <p>
     * @param data Double array of data.
     * <p>
     * @return Median of data.
     */
    public static double median(double[] data)
    {
        double[] sortedData = new double[data.length];

        System.arraycopy(data, 0, sortedData, 0, data.length);
        Arrays.sort(sortedData);
        int n = sortedData.length;
        double median;

        if((n % 2) == 0) { // even length
            median = 0.5 * (sortedData[n / 2] + sortedData[(n / 2) + 1]);
        } else {
            median = sortedData[(n + 1) / 2];
        }
        return median;
    }

    /**
     * Computes the angle between two vectors.
     * <p>
     * @param vec1 Vector one.
     * @param vec2 Vector two.
     * <p>
     * @return Angle in radiant.
     */
    public static double vecangle(double[] vec1, double[] vec2)
    {
        double len1 = Math.sqrt(vec1[0] * vec1[0] + vec1[1] * vec1[1]);
        double len2 = Math.sqrt(vec2[0] * vec2[0] + vec2[1] * vec2[1]);

        return Math.acos((vec1[0] * vec2[0] + vec1[1] * vec2[1]) / (len1 * len2));
    }

    /**
     * Implementation of the inverse error function following Mike Giles in
     * "Approximating the erfinv function".
     * <p>
     * @param x Argument x.
     * <p>
     * @return erfinv(x).
     */
    public static double erfinv(double x)
    {
        double w, p;

        w = -Math.log((1.0f - x) * (1.0f + x));
        if(w < 5.0f) {
            w = w - 2.5f;
            p = 2.81022636e-08f;
            p = 3.43273939e-07f + p * w;
            p = -3.5233877e-06f + p * w;
            p = -4.39150654e-06f + p * w;
            p = 0.00021858087f + p * w;
            p = -0.00125372503f + p * w;
            p = -0.00417768164f + p * w;
            p = 0.246640727f + p * w;
            p = 1.50140941f + p * w;
        } else {
            w = Math.sqrt(w) - 3.0f;
            p = -0.000200214257f;
            p = 0.000100950558f + p * w;
            p = 0.00134934322f + p * w;
            p = -0.00367342844f + p * w;
            p = 0.00573950773f + p * w;
            p = -0.0076224613f + p * w;
            p = 0.00943887047f + p * w;
            p = 1.00167406f + p * w;
            p = 2.83297682f + p * w;
        }
        return p * x;
    }

    /**
     * This is a fast approximation to the erf() function following Abramowitz
     * and Stegun with a maximum error of 1.5*10^-7. This should be accurate
     * enough for our needs.
     * <p>
     * @param x Argument to erf.
     * <p>
     * @return erf(x).
     */
    public static double erf(double x)
    {
        // erf is a odd function the later approximation is only valid for x >= 0
        if(x < 0) {
            return -erf(-x);
        }
        double p = 0.3275911;
        double a1 = 0.254829592;
        double a2 = -0.284496736;
        double a3 = 1.421413741;
        double a4 = -1.453152027;
        double a5 = 1.061405429;

        double t = 1.0d / (1.0d + p * x);
        return 1.0d - (a1 * t + a2 * t * t + a3 * t * t * t + a4 * t * t * t * t + a5 * t * t * t * t * t) * Math.exp(-(x * x));
    }

    /**
     * This is an approximation of the circumference of an ellipse, given the
     * length of both axis.
     * <p>
     * @param a Axis a.
     * @param b Axis b.
     * <p>
     * @return Circumference of ellipse.
     */
    public static double ellipseCircumference(double a, double b)
    {
        return Math.PI * (3 * (a + b) - Math.sqrt((3 * a + b) * (a + 3 * b)));
    }
    
    
    public static double [][] complexAbs(ComplexArray2D array)
    {
        double [][] absValues = new double[array.realValues.length][array.realValues[0].length];
        for(int i = 0; i < array.realValues.length; i++) {
            for(int j = 0; j < array.realValues[0].length; j++) {
                double value = array.realValues[i][j] * array.realValues[i][j] + 
                               array.imaginaryValues[i][j] * array.imaginaryValues[i][j];
                absValues[i][j] = Math.sqrt(value);
            }
        }
        return absValues;
    }
    
    /**
     * Computes entropy of data. 
     * 
     * @param data 
     * @return 
     */
    public static double computeEntropy(double[][] data)
    {
        double entropy = 0;
        for(int i = 0; i < data.length; i++) {
            for(int j = 0; j < data[0].length; j++) {
                if(Double.isNaN(data[i][j])) {
                    continue;
                }
                double tmp = data[i][j] * data[i][j];
                entropy += tmp * Math.log(tmp);
            }
        }
        return -entropy;
    }
    
    /**
     * Computes entropy of data. 
     * 
     * @param data 
     * @return 
     */
    public static double computeEntropy(double[] data)
    {
        double entropy = 0;
        for(int i = 0; i < data.length; i++) {
            if(Double.isNaN(data[i])) {
                continue;
            }
            double tmp = data[i] * data[i];
            entropy += tmp * Math.log(tmp);
        }
        return -entropy;
    }
    
    
    
    /**
     * Computes the second derivative of a signal.
     * <p>
     * @param signal    2D Signal to compute the derivative.
     * @param dimension Dimension to compute the derivative, 1 is the first
     *                  array dimension, 2 is the second array dimension.
     * <p>
     * @return Second derivative of a signal with respect to the dimension.
     * <p>
     * @throws JCeliacGenericException
     */
    public static double[][] derivativeOrder2(double[][] signal, int dimension)
            throws JCeliacGenericException
    {
        double[][] derivatives = new double[signal.length][signal[0].length];
        int end;

        if(dimension == 1) { // x - direction
            end = signal.length - 1;
            for(int j = 0; j < signal[0].length; j++) {
                // 2nd order forward
                derivatives[0][j] = (signal[2][j] - 2.0d * signal[1][j] + signal[0][j]);
                for(int i = 1; i < signal.length - 1; i++) {
                    derivatives[i][j] =  (signal[i + 1][j] - 2.0d * signal[i][j] + signal[i - 1][j]); 
                }
                derivatives[end][j] = (signal[end - 2][j] - 2.0d * signal[end - 1][j] + signal[end][j]);
            }

        } else if(dimension == 2) { // y-direction
            end = signal[0].length - 1;
            for(int i = 0; i < signal.length; i++) {
                derivatives[i][0] = (signal[i][2] - 2.0d * signal[i][1] + signal[i][0]);
                for(int j = 1; j < signal[0].length - 1; j++) {
                    derivatives[i][j] = (signal[i][j + 1] - 2.0d * signal[i][j] + signal[i][j - 1]);
                }
                derivatives[i][end] = (signal[i][end - 2] - 2.0d * signal[i][end - 1] + signal[i][end]);
            }
        } else {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Undefined dimension for differentiation!");
            throw new JCeliacGenericException("Undefined dimension for differentiation!");
        }
        return derivatives;
    }

    /**
     * Computes the first derivative of a signal.
     * <p>
     * @param signal    2D Signal to compute the derivative.
     * @param dimension Dimension to compute the derivative, 1 is the first
     *                  array dimension, 2 is the second array dimension.
     * <p>
     * @return Second derivative of a signal with respect to the dimension.
     * <p>
     * @throws JCeliacGenericException
     */
    public static double[][] derivativeOrder1(double[][] signal, int dimension)
            throws JCeliacGenericException
    {
        double[][] derivatives = new double[signal.length][signal[0].length];
        int end;

        if(dimension == 1) { // x-direction
            end = signal.length - 1;
            for(int j = 0; j < signal[0].length; j++) {
                // use forward differences on boundaries
                derivatives[0][j] = (signal[1][j] - signal[0][j]);
                for(int i = 1; i < signal.length - 1; i++) {
                    // take centered differences
                    derivatives[i][j] = 0.5 * (signal[i + 1][j] - signal[i - 1][j]);
                }
                derivatives[end][j] = (signal[end][j] - signal[end-1][j]);
            }

        } else if(dimension == 2) { // y-direction
            end = signal[0].length - 1;
            for(int i = 0; i < signal.length; i++) {
                 // use forward differences on boundaries
                derivatives[i][0] = (signal[i][1] - signal[i][0]);
                for(int j = 1; j < signal[0].length - 1; j++) {
                    // take centered differences
                    derivatives[i][j] = 0.5 * (signal[i][j + 1] - signal[i][j - 1]);
                }
                derivatives[i][end] = (signal[i][end] - signal[i][end-1]);
            }
        } else {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Undefined dimension for differentiation!");
            throw new JCeliacGenericException("Undefined dimension for differentiation!");
        }
        return derivatives;
    }

    /**
     * Computes the first derivative of a signal.
     * <p>
     * @param signal 1D Signal to compute the derivative.
     * <p>
     * @return Second derivative of a signal with respect to the dimension.
     */
    public static double[] derivativeOrder1(double[] signal)
    {
        double[] derivatives = new double[signal.length];
        int end;

        end = signal.length - 1;

        derivatives[0] = (signal[1] - signal[0]);
        for(int i = 1; i < signal.length - 1; i++) {
            derivatives[i] = 0.5 * (signal[i + 1] - signal[i - 1]);
        }
        derivatives[end] = (signal[end] - signal[end-1]);

        return derivatives;
    }

}
