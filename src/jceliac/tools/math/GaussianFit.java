/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.math;

import org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer;
import org.apache.commons.math3.fitting.GaussianFitter.ParameterGuesser;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.ConvergenceException;
import org.apache.commons.math3.fitting.WeightedObservedPoint;
import org.apache.commons.math3.analysis.function.Gaussian;
import org.apache.commons.math3.fitting.GaussianFitter;
import java.util.ArrayList;

/**
 * Fits a Gaussian function using a least squares estimate to a given set of
 * data points.
 * <p>
 * @author shegen
 */
public class GaussianFit
{

    private final double[] yData;
    private final double[] xData;

    private ArrayList<Double> preparedDataX;
    private ArrayList<Double> preparedDataY;

    public GaussianFit(double[] yData)
    {
        this.yData = yData;
        // craete indices from 1 to yData.length
        this.xData = new double[yData.length];
        for(int i = 0; i < yData.length; i++) {
            this.xData[i] = i + 1;
        }
    }

    public GaussianFit(double[] xData, double[] yData)
    {
        this.xData = xData;
        this.yData = yData;
    }

    /**
     * Fits a Gaussian function to the given data.
     * <p>
     * @return Array of parameters, ret[0] = norm, ret[1] = mean, ret[2] =
     *         sigma.
     * <p>
     * @throws ConvergenceException
     */
    public double[] fit()
            throws ConvergenceException
    {
        this.prepareData();
        if(this.preparedDataY.size() < 3) {
            throw new ConvergenceException();
        }
        GaussianFitter fitter = new GaussianFitter(new LevenbergMarquardtOptimizer());
        for(int index = 0; index < this.preparedDataX.size(); index++) {
            fitter.addObservedPoint(this.preparedDataX.get(index), this.preparedDataY.get(index));
        }
        double[] parameters = fitter.fit();
        return parameters;
    }

    /**
     * Fits a Gaussian function to the given data, giving up after a number of
     * iterations to avoid unbound computational time.
     * <p>
     * @param maxEval Specifies the maximum number of iterations.
     * <p>
     * @return Array of parameters, ret[0] = norm, ret[1] = mean, ret[2] =
     *         sigma.
     * <p>
     * @throws ConvergenceException
     */
    public double[] fitBounded(int maxEval)
    {

        this.prepareData();
        if(this.preparedDataY.size() < 3) {
            throw new ConvergenceException();
        }
        GaussianFitter fitter = new GaussianFitter(new LevenbergMarquardtOptimizer());
        for(int index = 0; index < this.preparedDataX.size(); index++) {
            fitter.addObservedPoint(this.preparedDataX.get(index), this.preparedDataY.get(index));
        }

        final Gaussian.Parametric f = new Gaussian.Parametric()
        {
            @Override
            public double value(double x, double... p)
            {
                double v = Double.POSITIVE_INFINITY;
                try {
                    v = super.value(x, p);
                } catch(NotStrictlyPositiveException e) { // NOPMD
                    // Do nothing.
                }
                return v;
            }

            @Override
            public double[] gradient(double x, double... p)
            {
                double[] v = {Double.POSITIVE_INFINITY,
                              Double.POSITIVE_INFINITY,
                              Double.POSITIVE_INFINITY};
                try {
                    v = super.gradient(x, p);
                } catch(NotStrictlyPositiveException e) { // NOPMD
                    // Do nothing.
                }
                return v;
            }
        };
        WeightedObservedPoint[] points = fitter.getObservations();
        ParameterGuesser parameterGuesser = new ParameterGuesser(points);
        double[] guess = parameterGuesser.guess();
        double[] parameters = fitter.fit(maxEval, f, guess);
        return parameters;
    }

    /**
     * Prepares the data for fitting.
     */
    private void prepareData()
    {
        this.preparedDataY = new ArrayList<>();
        this.preparedDataX = new ArrayList<>();
        for(int i = 0; i < this.yData.length; i++) {
            if(this.yData[i] != 0) {
                this.preparedDataX.add(this.xData[i]);
                this.preparedDataY.add(this.yData[i]);
            }

        }
    }
}
