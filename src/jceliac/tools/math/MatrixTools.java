/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.math;

import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.BlockRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import java.util.AbstractMap.SimpleEntry;
import jceliac.JCeliacGenericException;

/**
 * Provides tools to handle math with matrices.
 * <p>
 * @author shegen
 */
public class MatrixTools
{

    /**
     * Eigenvalue decomposition of a 2x2 matrix.
     * <p>
     * @param a11 Matrix element 1,1.
     * @param a12 Matrix element 1,2.
     * @param a21 Matrix element 2,1.
     * @param a22 Matrix element 2,2.
     * <p>
     * @return Two eigenvalues.
     */
    public static double[] eigenvalues(double a11, double a12, double a21, double a22)
    {
        double lambda1 = 0.5 * (a11 + a22 + Math.sqrt((a11 * a11) + 2 * a11 * a22 + (a22 * a22) + 4 * a12 * a21 - 4 * a11 * a22));
        double lambda2 = 0.5 * (a11 + a22 - Math.sqrt((a11 * a11) + 2 * a11 * a22 + (a22 * a22) + 4 * a12 * a21 - 4 * a11 * a22));

        double[] ret = new double[2];
        ret[0] = lambda1;
        ret[1] = lambda2;
        return ret;
    }

    /**
     * Computes the inverse of a 2x2 matrix.
     * <p>
     * @param a11 Matrix element 1,1.
     * @param a12 Matrix element 1,2.
     * @param a21 Matrix element 2,1.
     * @param a22 Matrix element 2,2.
     * <p>
     * @return Inverse of matrix as a 2x2 double array.
     */
    public static double[][] inverse(double a11, double a12, double a21, double a22)
    {
        double detinverse = 1 / (a11 * a22 - (a21 * a12));
        double[][] invmatrix = new double[2][2];
        invmatrix[0][0] = a22 * detinverse;
        invmatrix[0][1] = -a12 * detinverse;
        invmatrix[1][0] = -a21 * detinverse;
        invmatrix[1][1] = a11 * detinverse;
        return invmatrix;
    }

    /**
     * To conform to the matlab implementation we have to compute the square
     * root for which every eigenvalue has a nonnegativ real part.
     * <p>
     * @param a11 Matrix element 1,1.
     * @param a12 Matrix element 1,2.
     * @param a21 Matrix element 2,1.
     * @param a22 Matrix element 2,2.
     * <p>
     * @return Square root of a matrix.
     * <p>
     * @throws jceliac.JCeliacGenericException If matrix is singular or inverse
     *                                         is complex.
     */
    public static double[][] squareRoot(double a11, double a12, double a21, double a22)
            throws JCeliacGenericException
    {
        double trace = a11 + a22;
        double det = a11 * a22 - (a21 * a12);

        if(trace <= 0) { // we do not allow complex 
            throw new JCeliacGenericException("Trace is <= 0, complex Matrix elements not supported!");
        }
        if(det < 0) { // we do not allow complex 
            throw new JCeliacGenericException("Determinant is < 0, complex Matrix elements not supported!");
        }

        double s = Math.sqrt(det);
        double t = Math.sqrt(trace + 2 * s);

        double r11 = (1 / t) * (a11 + s);
        double r12 = (1 / t) * a12;
        double r21 = (1 / t) * a21;
        double r22 = (1 / t) * (a22 + s);

        double[][] ret = new double[2][2];
        ret[0][0] = r11;
        ret[0][1] = r12;
        ret[1][0] = r21;
        ret[1][1] = r22;
        return ret;
    }

    /**
     * Computes determinant of a matrix.
     * <p>
     * @param a11 Matrix element 1,1.
     * @param a12 Matrix element 1,2.
     * @param a21 Matrix element 2,1.
     * @param a22 Matrix element 2,2.
     * <p>
     * @return Determinant of the matrix.
     */
    public static double determinant(double a11, double a12, double a21, double a22)
    {
        return a11 * a22 - (a12 * a21);
    }

    /**
     * Computes determinant of a matrix.
     * <p>
     * @param A 2x2 matrix.
     * <p>
     * @return Determinant of the matrix.
     */
    public static double determinant(double[][] A)
    {
        return A[0][0] * A[1][1] - (A[0][1] * A[1][0]);
    }

    /**
     * Computes eigenvectors of a matrix.
     * <p>
     * @param A Matrix.
     * <p>
     * @return Two eigenvectors of A.
     */
    public static RealVector[] eigenvectors(double[][] A)
    {
        RealMatrix matrix = new BlockRealMatrix(A);
        @SuppressWarnings("deprecation")
        EigenDecomposition eigenDecomp = new EigenDecomposition(matrix, 0);
        RealVector vec1 = eigenDecomp.getEigenvector(0);
        RealVector vec2 = eigenDecomp.getEigenvector(1);

        return new RealVector[]{vec1, vec2};
    }

    /**
     * Computes eigenvalues of a matrix.
     * <p>
     * @param A Matrix.
     * <p>
     * @return Eigenvalues of A.
     */
    public static double[] eigenvalues(double[][] A)
    {
        RealMatrix matrix = new BlockRealMatrix(A);
        @SuppressWarnings("deprecation")
        EigenDecomposition eigenDecomp = new EigenDecomposition(matrix, 0);
        RealMatrix val = eigenDecomp.getD();
        double[] eigenvalues = new double[2];
        eigenvalues[0] = val.getEntry(0, 0);
        eigenvalues[1] = val.getEntry(1, 1);
        return eigenvalues;
    }

    /**
     * Returns eigenvectors and eigenvalues of a 2x2 matrix.
     * <p>
     * @param A Matrix.
     * <p>
     * @return Eigenvectors and eigenvalues of a 2x2 matrix.
     */
    public static SimpleEntry<RealVector[], double[]> eigensolution(double[][] A)
    {
        double b = -A[0][0] - A[1][1];
        double a = 1;
        double c = A[0][0] * A[1][1] - A[0][1] * A[1][0];

        // compute eigenvalues solving for det(A-lambdaI) = 0
        double lambda1 = (-b + Math.sqrt(b * b - 4 * a * c)) / (2 * a);
        double lambda2 = (-b - Math.sqrt(b * b - 4 * a * c)) / (2 * a);

        // compute eigenvectors by calculating the relationship of the components of the eigenvectors,
        // replacing the first component with 1! we can do this because we unitize the vectors anyway!
        double eigenvector1Comp2 = (lambda1 - A[0][0]) / A[0][1];
        double eigenvector1Norm = (Math.sqrt(1 + eigenvector1Comp2 * eigenvector1Comp2));

        double eigenvector1Comp1 = 1 / eigenvector1Norm;
        eigenvector1Comp2 = eigenvector1Comp2 / eigenvector1Norm;

        double eigenvector2Comp2 = (lambda2 - A[0][0]) / A[0][1];
        double eigenvector2Norm = (Math.sqrt(1 + eigenvector2Comp2 * eigenvector2Comp2));

        double eigenvector2Comp1 = 1 / eigenvector2Norm;
        eigenvector2Comp2 = eigenvector2Comp2 / eigenvector2Norm;

        RealVector eigenvector1 = new ArrayRealVector(new double[]{eigenvector1Comp1, eigenvector1Comp2});
        RealVector eigenvector2 = new ArrayRealVector(new double[]{eigenvector2Comp1, eigenvector2Comp2});

        SimpleEntry<RealVector[], double[]> ret = new SimpleEntry<>(new RealVector[]{eigenvector1, eigenvector2},
                                                                    new double[]{lambda1, lambda2});
        return ret;

    }
}
