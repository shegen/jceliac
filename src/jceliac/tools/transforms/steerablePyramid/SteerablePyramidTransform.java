/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.tools.transforms.steerablePyramid;

import jceliac.JCeliacGenericException;
import jceliac.tools.filter.DataFilter2D;

/**
 * Steerable pyramid composition, based on matlab code by Gabriel Peyre. 
 * 
 * @author shegen
 */
public class SteerablePyramidTransform
{
    private SteerablePyramidDecomposition decomposition;
    
    public void transformForward(double [][] dataChannel, int minimumScale, int numOrientations) 
            throws JCeliacGenericException
    {
        int maximumScale = (int)((Math.log(dataChannel.length) / Math.log(2.0d))-1);
        if(maximumScale - minimumScale + 1 > 5) {
            throw new JCeliacGenericException("Cannot construct pyramid higher than 5 levels");
        }
        int numBands = maximumScale - minimumScale + 1;
        SPFilters filters;
        switch(numOrientations) {
            case 1:
                filters = SteerablePyramidFilterFactory.createSPFilters(SteerablePyramidFilterFactory.ESteerablePyramidFilters.SP0);
                break;
            case 2:
                filters = SteerablePyramidFilterFactory.createSPFilters(SteerablePyramidFilterFactory.ESteerablePyramidFilters.SP1);
                break;
            case 4:
                filters = SteerablePyramidFilterFactory.createSPFilters(SteerablePyramidFilterFactory.ESteerablePyramidFilters.SP3);
                break;
            case 6:
                filters = SteerablePyramidFilterFactory.createSPFilters(SteerablePyramidFilterFactory.ESteerablePyramidFilters.SP5);
                break;
            default:
                throw new JCeliacGenericException("The number of orientations should be 1,2,4 or 6.");
        }
        this.decomposition = this.buildSteerablePyramid(dataChannel, numBands, filters);
    }

    public SteerablePyramidDecomposition getDecomposition()
    {
        return decomposition;
    }
    
    private SteerablePyramidDecomposition buildSteerablePyramid(double [][] dataChannel, 
                                                                int height, SPFilters filters)
            throws JCeliacGenericException
    {
        
        int maxPyramidHeight = this.computeMaximumPyramidHeight(dataChannel.length, dataChannel[0].length, 
                                                                filters.getLoFilter().length, 
                                                                filters.getLoFilter()[0].length);
        if(height > maxPyramidHeight) {
            throw new JCeliacGenericException("Cannot build pyramid higher than "+ maxPyramidHeight + " levels.");
        }
        SteerablePyramidDecomposition pyramid = new SteerablePyramidDecomposition();
        double [][] hi0Response = DataFilter2D.correlateMirror(dataChannel, filters.getHi0Filter());
        double [][] lo0Response = DataFilter2D.correlateMirror(dataChannel, filters.getLo0Filter());
        
        pyramid.setHighpassSubband(hi0Response);
        int curHeight = height;
        double [][] lowpassResponse = lo0Response;
        int curScale = 0;
        while(curHeight > 0)
        {
            int bFilterSize = (int)Math.round(Math.sqrt(filters.getLengthOfBFilters()));
            for(int bFilterIndex = 0; bFilterIndex < filters.getNumberOfBFilters(); bFilterIndex++) {
                double [][] bfilter = filters.getbFilter(bFilterIndex, bFilterSize, bFilterSize);
                double [][] bandResponse = DataFilter2D.correlateMirror(lowpassResponse, bfilter);
                pyramid.setBandpassSubband(bandResponse, curScale, bFilterIndex);
            }
            lowpassResponse = DataFilter2D.correlateMirror(lowpassResponse, filters.getLoFilter());
            lowpassResponse = this.scaleDown(lowpassResponse);
            curHeight--;
            curScale++;
        }
        pyramid.setLowpassSubband(lowpassResponse);
        return pyramid;
    }
    
    // removes all odd data values
    private double [][] scaleDown(double [][] data)
    {
        double [][] scaledData = new double[data.length/2][data[0].length/2];
        for(int i = 0; i < scaledData.length; i++) {
            for(int j = 0; j < scaledData[0].length; j++) {
                scaledData[i][j] = data[i*2][j*2];
            }
        }
        return scaledData;
    }
    
    private int computeMaximumPyramidHeight(int imageSizeX, int imageSizeY, int filterSizeX, int filterSizeY)
    {
        int newFilterSizeX = filterSizeX;
        int newFilterSizeY = filterSizeY;
        
        int newImageSizeX = imageSizeX;
        int newImageSizeY = imageSizeY;
        
        // check if it's a 1D filter
        if(filterSizeX == 1 || filterSizeY == 1) {
            newFilterSizeX = filterSizeX;
            newFilterSizeY = filterSizeX;
        }
        if(imageSizeX < filterSizeX || imageSizeY < filterSizeY) {
            return 0;
        }
        return 1 + this.computeMaximumPyramidHeight((int)Math.floor(newImageSizeX/2), (int)Math.floor(newImageSizeY/2), 
                                                    newFilterSizeX, newFilterSizeY);  
    } 
}
