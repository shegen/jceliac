/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */
package jceliac.tools.transforms.steerablePyramid;

import jceliac.JCeliacGenericException;

/**
 *
 * @author shegen
 */
public class SPFilters
{

    protected double[][] lo0Filter = null;
    protected double[][] loFilter = null;
    protected double[][] hi0Filter = null;
    protected double[][] bFilter = null;
    protected double[][] mtxFilter = null;
    protected double[][] harmonics = null;

    public double[][] getLo0Filter()
    {
        return lo0Filter;
    }

    public double[][] getLoFilter()
    {
        return loFilter;
    }

    public double[][] getHi0Filter()
    {
        return hi0Filter;
    }

    
    public int getLengthOfBFilters()
    {
        return this.bFilter.length;
    }
    
    public int getNumberOfBFilters()
    {
        return this.bFilter[0].length;
    }
    
    public double[][] getbFilter(int filterIndex, int dimX, int dimY)
            throws JCeliacGenericException
    {
        return this.reshape(this.bFilter, filterIndex, dimX, dimY);
    }

    public double[][] getMtxFilter()
    {
        return mtxFilter;
    }

    public double[][] getHarmonics()
    {
        return harmonics;
    }
    
      // reshape column wise 
    private double [][] reshape(double [][] data, int filterIndex, int dimX, int dimY)
            throws JCeliacGenericException
    {
        if(dimX*dimY != data.length)
        {
            throw new JCeliacGenericException("Reshaped data is supposed to have "
                                            + "the same dimension as the original");
        }
        double [][] reshapedData = new double[dimX][dimY];
        for(int linearIndex = 0; linearIndex < data.length; linearIndex++) {
            // reshape column wise
            int y = linearIndex % dimY;
            int x = linearIndex / dimY;
            reshapedData[x][y] = data[linearIndex][filterIndex];
        }
        return reshapedData; 
    }
}
