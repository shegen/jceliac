/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.tools.transforms.steerablePyramid;

import java.util.HashMap;

/**
 *
 * @author shegen
 */
public class SteerablePyramidDecomposition
{
    
    
    private HashMap <Integer, HashMap <Integer, double [][]>> bandpassSubbands;
    private double [][] highpassSubband;
    private double [][] lowpassSubband;

    @SuppressWarnings("unchecked")
    public SteerablePyramidDecomposition()
    {
        this.bandpassSubbands = new HashMap <> ();
    }
    
    public void setLowpassSubband(double [][] data)
    {
        this.lowpassSubband = data; 
    }
    
    public void setHighpassSubband(double [][] data) {
        this.highpassSubband = data;
    }
    
    public void setBandpassSubband(double [][] data, int scale, int orientation)
    {
        if(this.bandpassSubbands.containsKey(scale) == false) {
            HashMap <Integer, double[][]> orientationHashMap = new HashMap <>();
            orientationHashMap.put(orientation, data);
            this.bandpassSubbands.put(scale, orientationHashMap);
        }else {
            HashMap <Integer, double[][]> orientationHashMap = this.bandpassSubbands.get(scale);
            orientationHashMap.put(orientation, data);
        }
    }
    
    public double [][] getBandpassSubband(int scale, int orientation)
    {
        return this.bandpassSubbands.get(scale).get(orientation);
    }

    public double[][] getHighpassSubband()
    {
        return highpassSubband;
    }

    public double[][] getLowpassSubband()
    {
        return lowpassSubband;
    }
}
