/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */
package jceliac.tools.transforms.steerablePyramid;

/**
 *
 * @author shegen
 */
public class SteerablePyramidFilterFactory
{

    public static enum ESteerablePyramidFilters
    {

        SP0,
        SP1,
        SP3,
        SP5;
    };

    public static SPFilters createSPFilters(ESteerablePyramidFilters filterType)
    {
        switch(filterType) {
            case SP0:
                return new SP0Filters();
            case SP1:
                return new SP1Filters();
            case SP3:
                return new SP3Filters();
            case SP5:
                return new SP5Filters();
            default:
                return null;
        }
    }

}
