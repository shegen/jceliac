/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.transforms.wavelet;

import jceliac.tools.transforms.wavelet.WaveletDecomposition;
import java.util.ArrayList;
import java.util.function.BiPredicate;
import jceliac.tools.filter.*;
import jceliac.experiment.*;
import jceliac.tools.data.*;
import jceliac.logging.*;
import jceliac.*;

/**
 * Implements the wavelet transform.
 * <p>
 * @author shegen
 */
public class WaveletTransform
{

    public static enum EExtensionType
    {

        SYMMETRIC_HALFPOINT
    }

    public static enum EFilterBankType
    {

        CDF97 // cohen daubechies fauveau 9/7 tap filter (JPEG2000)
    }
    private final EFilterBankType filterBankType;
    private final int COLUMN = 1;
    private final int ROW = 2;
    private double[][] lowpassFilter;
    private double[][] highpassFilter;
    private double[][] TlowpassFilter;
    private double[][] ThighpassFilter;
    private WaveletDecomposition waveletDecompositionRed;
    private WaveletDecomposition waveletDecompositionGreen;
    private WaveletDecomposition waveletDecompositionBlue;
    private WaveletDecomposition waveletDecompositionGray;

    public WaveletTransform(EFilterBankType filterBankType)
    {
        this.filterBankType = filterBankType;

        this.createFilterBank();
    }

    /**
     * Returns wavele decomposition based on color channel.
     * <p>
     * @param channel Color channel.
     * <p>
     * @return Wavelet decomposed data.
     * <p>
     * @throws JCeliacGenericException
     */
    public WaveletDecomposition getWaveletDecompositionTree(int channel)
            throws JCeliacGenericException
    {
        switch(channel) {
            case DataSource.CHANNEL_BLUE:
                return this.waveletDecompositionBlue;
            case DataSource.CHANNEL_GREEN:
                return this.waveletDecompositionGreen;
            case DataSource.CHANNEL_RED:
                return this.waveletDecompositionRed;
            case DataSource.CHANNEL_GRAY:
                return this.waveletDecompositionGray;

            default:
                throw new JCeliacGenericException("No such color channel");
        }
    }

    /**
     * Performs a forward wavelet transform, return downscaled version of signal
     * for the next scale.
     * <p>
     * @param signal  Signal to transform.
     * @param scale   Scale of transform.
     * @param channel Color channel that is transformed.
     * <p>
     * @return Signal mapped by the scaling function.
     */
    private double[][] doTransformForward(double[][] signal, int scale, int channel)
    {

        int lf = lowpassFilter.length;
        int[] sx = new int[]{signal.length, signal[0].length};

        int sizeEXT = lf - 1; // extend signal by the length of the filter-1
        int[] last = new int[]{sx[0] + lf - 1, sx[1] + lf - 1};
        int[] first = new int[]{1, 1};

        double[][] extendedSignal = extendSignal(signal, sizeEXT, COLUMN);

        double[][] convolved = DataFilter2D.convolveValid(extendedSignal, lowpassFilter);
        double[][] a = downscaleConvolve(convolved, TlowpassFilter, sizeEXT, first, last);
        double[][] h = downscaleConvolve(convolved, ThighpassFilter, sizeEXT, first, last);

        convolved = DataFilter2D.convolveValid(extendedSignal, highpassFilter);
        double[][] v = downscaleConvolve(convolved, TlowpassFilter, sizeEXT, first, last);
        double[][] d = downscaleConvolve(convolved, ThighpassFilter, sizeEXT, first, last);

        switch(channel) {
            case DataSource.CHANNEL_BLUE:

                this.waveletDecompositionBlue.setApproximationSubband(a, scale);
                this.waveletDecompositionBlue.setHorizontalDetailSubband(h, scale);
                this.waveletDecompositionBlue.setVerticalDetailSubband(v, scale);
                this.waveletDecompositionBlue.setDiagonalDetailSubband(d, scale);
                break;

            case DataSource.CHANNEL_RED:
                this.waveletDecompositionRed.setApproximationSubband(a, scale);
                this.waveletDecompositionRed.setHorizontalDetailSubband(h, scale);
                this.waveletDecompositionRed.setVerticalDetailSubband(v, scale);
                this.waveletDecompositionRed.setDiagonalDetailSubband(d, scale);
                break;

            case DataSource.CHANNEL_GREEN:
                this.waveletDecompositionGreen.setApproximationSubband(a, scale);
                this.waveletDecompositionGreen.setHorizontalDetailSubband(h, scale);
                this.waveletDecompositionGreen.setVerticalDetailSubband(v, scale);
                this.waveletDecompositionGreen.setDiagonalDetailSubband(d, scale);
                break;

            case DataSource.CHANNEL_GRAY:
                this.waveletDecompositionGray.setApproximationSubband(a, scale);
                this.waveletDecompositionGray.setHorizontalDetailSubband(h, scale);
                this.waveletDecompositionGray.setVerticalDetailSubband(v, scale);
                this.waveletDecompositionGray.setDiagonalDetailSubband(d, scale);
                break;

        }
        return a;
    }

    /**
     * Wavelet transforms a signal.
     * <p>
     * @param signal  Signal to transform.
     * @param scales  Number of scales to use.
     * @param channel Color channel that is transformed.
     * <p>
     * @throws JCeliacGenericException
     */
    public void transformForwardSingleChannel(double[][] signal, int scales, int channel)
            throws JCeliacGenericException
    {

        if((Math.log(signal.length) / Math.log(2)) < scales) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Wavelet Decomposition depth is too high for signal length!");
            throw new JCeliacGenericException("Wavelet Decomposition depth is too high for signal length");
        }

        switch(channel) {
            case DataSource.CHANNEL_BLUE:
                this.waveletDecompositionBlue = new WaveletDecomposition(scales);
                break;
            case DataSource.CHANNEL_GREEN:
                this.waveletDecompositionGreen = new WaveletDecomposition(scales);
                break;
            case DataSource.CHANNEL_RED:
                this.waveletDecompositionRed = new WaveletDecomposition(scales);
                break;
            case DataSource.CHANNEL_GRAY:
                this.waveletDecompositionGray = new WaveletDecomposition(scales);
                break;
        }

        double[][] approx = signal;
        for(int scale = 1; scale <= scales; scale++) {
            approx = doTransformForward(approx, scale, channel);
        }
    }

    /**
     * Wavelet transforms a signal with multiple color channel at once.
     * <p>
     * @param content  Frame that is wavelet transformed.
     * @param scales   Number of scales to use.
     * @param channels Bitmap of color channels to use for transformation.
     * <p>
     * @throws JCeliacGenericException
     */
    public void transformForwardMultiChannel(Frame content, int scales, int channels)
            throws JCeliacGenericException
    {
        // transform for all marked channels
        if((channels & DataSource.CHANNEL_BLUE) != 0) {
            transformForwardSingleChannel(content.getBlueData(), scales, DataSource.CHANNEL_BLUE);
        }
        if((channels & DataSource.CHANNEL_RED) != 0) {
            transformForwardSingleChannel(content.getRedData(), scales, DataSource.CHANNEL_RED);
        }
        if((channels & DataSource.CHANNEL_GREEN) != 0) {
            transformForwardSingleChannel(content.getGreenData(), scales, DataSource.CHANNEL_GREEN);
        }
        if((channels & DataSource.CHANNEL_GRAY) != 0) {
            transformForwardSingleChannel(content.getGrayData(), scales, DataSource.CHANNEL_GRAY);
        }
    }

    /**
     * Scaling function of the wavelet transform.
     * <p>
     * @param signal     Signal that is transformed.
     * @param filter     Scaling filter that is used.
     * @param extendSize Size of extended signal.
     * @param first
     * @param last <p>
     * @return Downscaled signal.
     */
    private double[][] downscaleConvolve(double[][] signal, double[][] filter,
                                         int extendSize, int[] first, int[] last)
    {

        // downscale and extend up
        double[][] tmp = downscale(signal, first[0], last[0], 'y');
        tmp = extendSignal(tmp, extendSize, ROW);
        tmp = DataFilter2D.convolveValid(tmp, filter);
        return downscale(tmp, first[1], last[1], 'x');

    }

    /**
     * Scales the signal down.
     * <p>
     * @param signal Signal to scale.
     * @param start  Start index of the signal.
     * @param end    End index of the signal.
     * @param dim    Dimension to scale along.
     * <p>
     * @return Downscaled signal.
     */
    private double[][] downscale(double[][] signal, int start, int end, char dim)
    {
        double[][] scaledSignal;

        if(dim == 'y') {
            // downscale signal by a factor of 2 
            scaledSignal = new double[(end - start + 1) / 2][signal[0].length];
            for(int colIndex = 0; colIndex < signal[0].length; colIndex++) {
                for(int index = start, scaledIndex = 0; index < end; index += 2, scaledIndex++) {
                    scaledSignal[scaledIndex][colIndex] = signal[index][colIndex];
                }
            }
            return scaledSignal;
        }

        if(dim == 'x') {
            // downscale signal by a factor of 2
            scaledSignal = new double[signal.length][(end - start + 1) / 2];
            for(int rowIndex = 0; rowIndex < signal.length; rowIndex++) {
                for(int index = start, scaledIndex = 0; index < end; index += 2, scaledIndex++) {
                    scaledSignal[rowIndex][scaledIndex] = signal[rowIndex][index];
                }
            }
            return scaledSignal;
        }

        return null;

    }

    /**
     * Creates a wavelet filter bank.
     */
    private void createFilterBank()
    {
        switch(this.filterBankType) {
            case CDF97:
                this.lowpassFilter = new double[][]{{0.0267}, {-0.0168}, {-0.0782}, {0.2668}, {0.6029}, {0.2668}, {-0.0782}, {-0.0168}, {0.0267}};
                this.highpassFilter = new double[][]{{0.0912}, {-0.0575}, {-0.5912}, {1.1150}, {-0.5912}, {-0.0575}, {0.0912}};

                this.TlowpassFilter = new double[][]{{0.0267, -0.0168, -0.0782, 0.2668, 0.6029, 0.2668, -0.0782, -0.0168, 0.0267}};
                this.ThighpassFilter = new double[][]{{0.0912, -0.0575, -0.5912, 1.1150, -0.5912, -0.0575, 0.0912}};

                break;

            default:
                Main.log(JCeliacLogger.LOG_ERROR, "Unknown Wavelet Filter Type!");
        }
    }

    // 
    /**
     * Extensd a signal accordingly. 'b' = 1, 'n' = 0.
     * <p>
     * @param signal     Signal that is extended.
     * @param extendSize Size to extend to.
     * @param dimension  Dimension of signal that is extended.
     * <p>
     * @return Extended signal.
     */
    private double[][] extendSignal(double[][] signal, int extendSize, int dimension)
    {

        double[][] extendedSignal = null;

        int[] indices;
        switch(dimension) {
            case 1:
                indices = this.getSymIndices(signal.length, extendSize);
                extendedSignal = this.extendColumns(signal, indices);
                break;
            case 2:
                indices = this.getSymIndices(signal[0].length, extendSize);
                extendedSignal = this.extendRows(signal, indices);
        }
        return extendedSignal;
    }

    private int[] getSymIndices(int signalLength, int extendSize)
    {
        int[] indices;

        indices = new int[signalLength + (extendSize * 2)];

        int globalIndex = 0;
        /* [lf:-1:1, 1:lx, lx:-1:lx-lf+1]
         */
        for(int index = extendSize - 1; index >= 0; index--) {
            indices[globalIndex] = index;
            globalIndex++;
        }
        for(int index = 0; index < signalLength; index++) {
            indices[globalIndex] = index;
            globalIndex++;
        }
        for(int index = signalLength - 1; index >= signalLength - extendSize; index--) {
            indices[globalIndex] = index;
            globalIndex++;
        }
        if(signalLength < extendSize) {
            for(int i = 0; i < indices.length; i++) {
                if(indices[i] < 0) {
                    indices[i] = -1-indices[i];
                }
            }
            Integer [] J = find(indices, signalLength, (a,b) -> a >= b);
            while(any(J)) {
                for(int j : J) {
                    indices[j] = 2*signalLength -indices[j]-1;
                }
                Integer [] K = find(indices, 0, (a,b) -> a < b);
                for(int k : K) {
                    indices[k] = 1-indices[k];
                }
                J = find(indices, signalLength, (a,b) -> a >= b);
            }
        }
        return indices;
    }

    private static Integer [] find(int [] data, double value, 
                                            BiPredicate <Integer, Double> t) 
    {
        ArrayList <Integer> retIndices = new ArrayList <>();
        for(int i = 0; i < data.length; i++) {
            boolean r = t.test(data[i], value);
            if(r) {
                retIndices.add(i);
            }
        }
        Integer [] retArray = retIndices.toArray(new Integer[retIndices.size()]);
        return retArray;
    }
    
    private static int [] logical(int [] data, double value, 
                                      BiPredicate <Integer, Double> t) 
    {
        int [] l = new int[data.length];
        for(int i = 0; i < data.length; i++) {
            boolean r = t.test(data[i], value);
            if(r) {
                l[i] = 1;
            }
        }
        return l;
    }
    
    private static boolean any(Integer[] data)
    {
        for(int i = 0; i < data.length; i++) {
            if(data[i] != 0) {
                return true;
            }
        }
        return false;
    }

    private double[][] extendRows(double[][] signal, int[] indices)
    {
        double[][] extendedSignal = new double[signal.length][indices.length];

        for(int rowIndex = 0; rowIndex < extendedSignal.length; rowIndex++) {
            for(int colIndex = 0; colIndex < indices.length; colIndex++) {
                extendedSignal[rowIndex][colIndex] = signal[rowIndex][indices[colIndex]];
            }
        }
        return extendedSignal;
    }

    private double[][] extendColumns(double[][] signal, int[] indices)
    {
        double[][] extendedSignal = new double[indices.length][signal[0].length];

        for(int rowIndex = 0; rowIndex < indices.length; rowIndex++) {
            System.arraycopy(signal[indices[rowIndex]], 0, extendedSignal[rowIndex], 0, signal[0].length);
        }
        return extendedSignal;
    }
}
