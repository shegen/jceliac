/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.transforms.wavelet;

import java.util.*;

/**
 * Stores the data of a wavelet decomposition.
 *
 * @author shegen
 */
public class WaveletDecomposition {

    private final ArrayList<double[][]> approximationSubbands;  // LL
    private final ArrayList<double[][]> horizontalDetailSubbands;
    private final ArrayList<double[][]> verticalDetailSubbands;
    private final ArrayList<double[][]> diagonalDetailSubbands; //HH

    public WaveletDecomposition(int scales) {
        this.approximationSubbands = new ArrayList<>();
        this.approximationSubbands.ensureCapacity(scales);

        this.horizontalDetailSubbands = new ArrayList<>();
        this.horizontalDetailSubbands.ensureCapacity(scales);

        this.verticalDetailSubbands = new ArrayList<>();
        this.verticalDetailSubbands.ensureCapacity(scales);

        this.diagonalDetailSubbands = new ArrayList<>();
        this.diagonalDetailSubbands.ensureCapacity(scales);

        for (int i = 0; i < scales; i++) {
            this.approximationSubbands.add(null);
            this.verticalDetailSubbands.add(null);
            this.horizontalDetailSubbands.add(null);
            this.diagonalDetailSubbands.add(null);
        }

    }

    public double[][] getApproximationSubband(int scale) {
        return this.approximationSubbands.get(scale - 1);
    }

    public double[][] getVerticalDetailSubband(int scale) {
        return this.verticalDetailSubbands.get(scale - 1);
    }

    public double[][] getHorizontalDetailSubband(int scale) {
        return this.horizontalDetailSubbands.get(scale - 1);
    }

    public double[][] getDiagonalDetailSubband(int scale) {
        return this.diagonalDetailSubbands.get(scale - 1);
    }

    public void setApproximationSubband(double[][] subband, int scale) {
        this.approximationSubbands.set(scale - 1, subband);
    }

    public void setVerticalDetailSubband(double[][] subband, int scale) {
        this.verticalDetailSubbands.set(scale - 1, subband);
    }

    public void setHorizontalDetailSubband(double[][] subband, int scale) {
        this.horizontalDetailSubbands.set(scale - 1, subband);
    }

    public void setDiagonalDetailSubband(double[][] subband, int scale) {
        this.diagonalDetailSubbands.set(scale - 1, subband);
    }
}
