/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.transforms.logpolar;

import jceliac.JCeliacGenericException;
import jceliac.tools.interpolator.BilinearInterpolator;
import jceliac.tools.interpolator.Interpolator;

/**
 * Implements the log polar transform, with fixed radii and scales such that R =
 * S = image width, expects a quadratic image.
 * <p>
 * @author shegen
 */
public class LogpolarTransform
{

    public static double[][] transformLogPolar(double[][] input)
            throws JCeliacGenericException
    {
        if(input.length != input[0].length) {
            throw new JCeliacGenericException("Only square sized data supported in LogPolar transform!");
        }
        double[][] p = new double[input.length][input[0].length];
        double[][] transformed = new double[input.length][input[0].length];
        Interpolator interp = new BilinearInterpolator();

        int S, R, N;
        S = input[0].length;
        R = S;
        N = S;

        for(int a = 0; a < S; a++) {
            for(int r = 0; r < Math.floor(N / 2.0d); r++) {
                double indY = Math.floor(N / 2.0d) - r * Math.sin((2 * Math.PI * a) / (double) S);
                double indX = Math.floor(N / 2.0d) + r * Math.cos((2 * Math.PI * a) / (double) S);
                double val = interp.interpolate(input, indX, indY);
                p[r][a] = val;
            }
        }
        for(int i = 0; i < S; i++) {
            for(int j = 0; j < N; j++) {

                double nom = Math.log(j + 2) / Math.log(2.0d);
                double denom = Math.log(R + 2) / Math.log(2.0d);
                int ind = (int) Math.floor((nom / denom) * Math.floor(N / 2.0d));
                transformed[j][i] = p[ind][i];
            }
        }
        return transformed;
    }
}
