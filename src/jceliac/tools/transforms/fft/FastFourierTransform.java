/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.transforms.fft;

import jceliac.tools.filter.Complex;
import jceliac.tools.transforms.dtcwt.ComplexArray1D;
import jceliac.tools.transforms.dtcwt.ComplexArray2D;

/**
 * Implementation of the Fast Fourier Transform.
 * <p>
 * @author shegen
 */
public class FastFourierTransform
{

    protected static int[] precomputedIndices128;
    protected static int[] precomputedIndices256;
    protected int[] precomputedIndices;

    protected double[][] sineLookup;
    protected double[][] cosineLookup;
    protected static double[][] sineLookup256;
    protected static double[][] cosineLookup256;

    static {
        FastFourierTransform.precomputedIndices128 = FastFourierTransform.computeIndices((int) 128);
        FastFourierTransform.precomputedIndices256 = FastFourierTransform.computeIndices((int) 256);
        FastFourierTransform.cosineLookup256 = FastFourierTransform.computeTwiddleFactorsCosine(256);
        FastFourierTransform.sineLookup256 = FastFourierTransform.computeTwiddleFactorsSine(256);
    }

    public FastFourierTransform(int size)
    {
        switch(size) {
            case 128:
                this.precomputedIndices = FastFourierTransform.precomputedIndices128;
                break;
            case 256:
                this.precomputedIndices = FastFourierTransform.precomputedIndices256;
                break;
            default:
                if((size & - size) == size) { // check if power of two
                    this.precomputedIndices = FastFourierTransform.computeIndices(size);
                }
                break;
        }
        switch(size) {
            case 256:
                this.sineLookup = FastFourierTransform.sineLookup256;
                this.cosineLookup = FastFourierTransform.cosineLookup256;
                break;
            default:
                if((size & - size) == size) { // check if power of two
                    this.sineLookup = FastFourierTransform.computeTwiddleFactorsSine(size);
                    this.cosineLookup = FastFourierTransform.computeTwiddleFactorsCosine(size);
                }
        }
    }

    /**
     * This method computes the indices of the even/odd splitting that is
     * usually achieved by the recursion. We assign each point an increasing
     * number and treat the binary representation by interpreting the number
     * changing the MSB to the LSB. This gives us the final arrangement of the
     * "recursion".
     * <p>
     * @param size Size of signal.
     * <p>
     * @return Indices of the even/odd splitting.
     */
    protected static int[] computeIndices(int size)
    {
        int numBits = (int) (Math.log(size) / Math.log(2));
        int[] indicesArray = new int[2 << (numBits - 1)];

        for(int i = 0; i < indicesArray.length; i++) {
            indicesArray[i] = computeIndexFor(i, numBits);
        }
        return indicesArray;
    }

    /**
     * Compute the number when treating the MSB as LSB in the binary
     * representation.
     * <p>
     * @param i
     * @param numBits
     * <p>
     * @return
     */
    private static int computeIndexFor(int i, int numBits)
    {
        int index = 0;
        for(int bitPos = 0; bitPos < numBits; bitPos++) {
            index += ((i & (1 << bitPos)) >> bitPos) << (numBits - 1 - bitPos);
        }
        return index;
    }

    /**
     * Compute the "twiddle" factors which are the the values of the sines and
     * cosines, e^(i*k) = cos k + i sin k we compute W^k_N = e^-(i*2*pi*k/N)
     * with N being the number of samples.
     * <p>
     * @param size Size of signal.
     * <p>
     * @return Sine twiddle factors.
     */
    private static double[][] computeTwiddleFactorsSine(int size)
    {
        int N = (int) (Math.log(size) / Math.log(2)); // compute the number of layers
        double[][] factors = new double[N][]; // compute the twiddle factors for each layer
        
        for(int layerIndex = 1; layerIndex < N; layerIndex++) {
            // compute elements for the sections of this layer
            int elementsPerSection = (size / (1 << (N - layerIndex)));
            factors[layerIndex] = new double[elementsPerSection];

            // compute the factors
            for(int k = 0; k < elementsPerSection; k++) {
                factors[layerIndex][k] = Math.sin((-Math.PI * k) / elementsPerSection);
            }
        }
        return factors;
    }

    /**
     * Compute the "twiddle" factors for the cosine.
     * <p>
     * @param size Size of signal.
     * <p>
     * @return Cosine twiddle factors.
     */
    private static double[][] computeTwiddleFactorsCosine(int size)
    {
        // compute the number of layers
        int N = (int) (Math.log(size) / Math.log(2));

        // compute the twiddle factors for each layer
        double[][] factors = new double[N][];

        for(int layerIndex = 1; layerIndex < N; layerIndex++) {
            // compute elements for the sections of this layer
            int elementsPerSection = (size / (1 << (N - layerIndex)));
            factors[layerIndex] = new double[elementsPerSection];

            // compute the factors
            for(int k = 0; k < elementsPerSection; k++) {

                factors[layerIndex][k] = Math.cos((-Math.PI * k) / elementsPerSection);
            }
        }
        return factors;
    }

    /**
     * This is the fast non-recursive implementation of the FFT using the
     * Complex object.
     * @param signal signal to transformFFT.
     * @return Fourier transformFFTed signal.
     */
    protected Complex[] _transform(Complex[] signal)
    {
        int N = (int) (Math.log(signal.length) / Math.log(2));
        Complex[] ret = new Complex[signal.length];

        for(int i = 0; i < signal.length - 1; i += 2) {
            int ind1 = precomputedIndices[i];
            int ind2 = precomputedIndices[i + 1];

            Complex t = new Complex(1, 0, "e"); // cos(0), sin(0)
            Complex g = signal[ind1];
            Complex u = signal[ind2];

            // put at position k in the section
            Complex ck = g.add(u.mult(t));
            // put at position k+n in the section (n is the length of the section)
            Complex ckn = g.subtract(u.mult(t));
            ret[i] = ck;
            ret[i + 1] = ckn;
        }

        for(int layer = 1; layer < N; layer++) {
            // for each section in a layer
            for(int section = 0; section < (1 << (N - layer)) - 1; section += 2) {
                // for each element in a section
                for(int element = 0; element < (signal.length / (1 << (N - layer))); element++) {
                    int elementsPerSection = (signal.length / (1 << (N - layer)));
                    int ind2 = (section + 1) * elementsPerSection + element;
                    int ind1 = (section) * elementsPerSection + element;
                    int k = element;

                    /* this was initially (-2*Math.PI*K) /
                     * (elementsPerSection*2) but obviously this is (-Math.PI*k)
                     * / (elementsPerSection)
                     */
                    Complex t = new Complex(Math.cos((-Math.PI * k) / (elementsPerSection)),
                                            Math.sin((-Math.PI * k) / (elementsPerSection)), "e");
                    Complex g = ret[ind1];
                    Complex u = ret[ind2];
                    Complex ck = g.add(u.mult(t));
                    Complex ckn = g.subtract(u.mult(t));

                    ret[ind1] = ck;
                    ret[ind2] = ckn;
                }
            }
        }
        return ret;
    }

    /**
     * This is the fast non-recursive implementation of the FFT using double
     * arrays for speedup.
     * @param signal signal to transformFFT.
     * @return Fourier transformFFTed signal.
     */
    protected double[][] transformFFT(double[][] signal)
    {
        int N = (int) (Math.log(signal.length) / Math.log(2));
        double[][] ret = new double[signal.length][2];

        for(int i = 0; i < signal.length - 1; i += 2) {
            int ind1 = this.precomputedIndices[i];
            int ind2 = this.precomputedIndices[i + 1];

            double greal = signal[ind1][0];
            double gimage = signal[ind1][1];
            double ureal = signal[ind2][0];
            double uimage = signal[ind2][1];

            // coefficiencts ck and ckn imaginary and real parts
            double ckreal, cknreal, ckimage, cknimage;
            double tmpreal, tmpimage;

            tmpreal = ureal;   //ureal * 1 - gimage * 0;
            tmpimage = uimage; //uimage * 1 + greal *0;

            // next add g and the result of t*u
            ckreal = greal + tmpreal;
            ckimage = gimage + tmpimage;
            ret[i][0] = ckreal;
            ret[i][1] = ckimage;

            // subtract: g-(u*t)
            cknreal = greal - tmpreal;
            cknimage = gimage - tmpimage;
            ret[i + 1][0] = cknreal;
            ret[i + 1][1] = cknimage;
        }

        for(int layer = 1; layer < N; layer++) {
            // for each section in a layer
            for(int section = 0; section < (1 << (N - layer)) - 1; section += 2) {
                // for each element in a section
                int elementsPerSection = (signal.length / (1 << (N - layer)));
                int ind2base = (section + 1) * elementsPerSection;
                int ind1base = section * elementsPerSection;

                for(int element = 0; element < elementsPerSection; element++) {
                    int ind2 = ind2base + element;
                    int ind1 = ind1base + element;

                    /* this was initially (-2*Math.PI*K) /
                     * (elementsPerSection*2) but obviously this is (-Math.PI*k)
                     * / (elementsPerSection)
                     */
                    double tr = Math.cos((-Math.PI * element) / (elementsPerSection));
                    double ti = Math.sin((-Math.PI * element) / (elementsPerSection));
                    double treal = this.cosineLookup[layer][element];
                    double timage = this.sineLookup[layer][element];

                    double greal = ret[ind1][0];
                    double gimage = ret[ind1][1];
                    double ureal = ret[ind2][0];
                    double uimage = ret[ind2][1];

                    // coefficiencts ck and ckn imaginary and real parts
                    double ckreal, ckimage, cknreal, cknimage;
                    double tmpimage, tmpreal;

                    // compute ck, first multiply t and u
                    tmpreal = (ureal * treal - uimage * timage);
                    tmpimage = (uimage * treal + ureal * timage);

                    // next add g and the result of t*u
                    ckreal = greal + tmpreal;
                    ckimage = gimage + tmpimage;

                    // compute ckn, reuse the u*t stored in [tmpreal,tmpimage]
                    // and subtract that from g
                    cknreal = greal - tmpreal;
                    cknimage = gimage - tmpimage;

                    ret[ind1][0] = ckreal;
                    ret[ind1][1] = ckimage;
                    ret[ind2][0] = cknreal;
                    ret[ind2][1] = cknimage;
                }
            }
        }
        return ret;
    }

    /**
     * This is the fast non-recursive implementation of the FFT using two 1D
 arrays for transformFFTation.
     * <p>
     * @param signalReal      Real part of signal.
     * @param signalImaginary Imaginary part of image.
     * <p>
     * @return Fourier transformFFTed signal.
     */
    protected ComplexArray1D _transformFullOptimizedFFT(double[] signalReal, double[] signalImaginary)
    {
        int N = (int) (Math.log(signalReal.length) / Math.log(2));
        double[] retReal = new double[signalReal.length];
        double[] retImage = new double[signalReal.length];

        for(int i = 0; i < signalReal.length - 1; i += 2) {
            int ind1 = precomputedIndices[i];
            int ind2 = precomputedIndices[i + 1];

            double greal = signalReal[ind1];
            double gimage = signalImaginary[ind1];
            double ureal = signalReal[ind2];
            double uimage = signalImaginary[ind2];

            // coefficiencts ck and ckn imaginary and real parts
            double ckreal, cknreal, ckimage, cknimage;
            double tmpreal, tmpimage;

            tmpreal = ureal; //ureal * 1 - gimage * 0;
            tmpimage = uimage; //uimage * 1 + greal *0;

            // next add g and the result of t*u
            ckreal = greal + tmpreal;
            ckimage = gimage + tmpimage;
            retReal[i] = ckreal;
            retImage[i] = ckimage;

            // subtract: g-(u*t)
            cknreal = greal - tmpreal;
            cknimage = gimage - tmpimage;
            retReal[i + 1] = cknreal;
            retImage[i + 1] = cknimage;
        }

        for(int layer = 1; layer < N; layer++) {
            // for each section in a layer
            for(int section = 0; section < (1 << (N - layer)) - 1; section += 2) {
                // for each element in a section
                int elementsPerSection = (signalReal.length / (1 << (N - layer)));
                int ind2base = (section + 1) * elementsPerSection;
                int ind1base = section * elementsPerSection;

                for(int element = 0; element < elementsPerSection; element++) {
                    int ind2 = ind2base + element;
                    int ind1 = ind1base + element;

                    /* this was initially (-2*Math.PI*K) /
                     * (elementsPerSection*2) but obviously this is (-Math.PI*k)
                     * / (elementsPerSection)
                     */
                    double treal = this.cosineLookup[layer][element];
                    double timage = this.sineLookup[layer][element];

                    double greal = retReal[ind1];
                    double gimage = retImage[ind1];
                    double ureal = retReal[ind2];
                    double uimage = retImage[ind2];

                    // coefficiencts ck and ckn imaginary and real parts
                    double ckreal, ckimage, cknreal, cknimage;
                    double tmpimage, tmpreal;

                    // compute ck, first multiply t and u
                    tmpreal = (ureal * treal - uimage * timage);
                    tmpimage = (uimage * treal + ureal * timage);

                    // next add g and the result of t*u
                    ckreal = greal + tmpreal;
                    ckimage = gimage + tmpimage;

                    // compute ckn, reuse the u*t stored in [tmpreal,tmpimage]
                    // and subtract that from g
                    cknreal = greal - tmpreal;
                    cknimage = gimage - tmpimage;

                    retReal[ind1] = ckreal;
                    retImage[ind1] = ckimage;
                    retReal[ind2] = cknreal;
                    retImage[ind2] = cknimage;
                }
            }
        }
        Object[] ret = new Object[2];
        ret[0] = retReal;
        ret[1] = retImage;
        return new ComplexArray1D(retReal, retImage);
    }

    // transformFFT inverse, return double which is effectively only the real part 
    // in case of our application
    /**
     *
     * @param data
     * <p>
     * @return
     */
    public double[] _transformInverseFFT(Complex[] data)
    {
        Complex[] ret = new Complex[data.length];

        // compute conjugate and transformFFT
        for(int i = 0; i < data.length; i++) {
            ret[i] = data[i].conjugate();
        }
        ret = this._transform(ret);

        // compute conjugate
        for(int i = 0; i < data.length; i++) {
            ret[i] = ret[i].conjugate();
        }

        double[] retDouble = new double[data.length];
        for(int i = 0; i < data.length; i++) {
            ret[i] = ret[i].div(new Complex(data.length, 0));
            retDouble[i] = ret[i].re;
        }
        return retDouble;
    }

    /**
     * This is the fast non-recursive implementation of the inverse FFT using a
     * 2D double array to represent a complex signal.
     * <p>
     * @param signal 2D array representing complex signal.
     * <p>
     * @return Real part of the inverse transformFFTed signal.
     */
    public double[] transformInverse(double[][] signal)
    {
        double[][] ret = new double[signal.length][2];

        // compute conjugate and transformFFT
        for(int i = 0; i < signal.length; i++) {
            ret[i][0] = signal[i][0];
            ret[i][1] = -signal[i][1];
        }
        ret = this.transformFFT(ret);

        // compute conjugate
        for(int i = 0; i < signal.length; i++) {
            //ret[i][0] = ret[i][0];
            ret[i][1] = -ret[i][1];
        }

        double[] retDouble = new double[signal.length];
        for(int i = 0; i < signal.length; i++) {
            // normalize by divide by length
            // (ret[i][0] * signal.length + ret[i][1] * 0) / (signal.length*signal.length)+0);
            retDouble[i] = ret[i][0] * signal.length / (signal.length * signal.length);
        }
        return retDouble;
    }

    /**
     * This is the fast non-recursive implementation of the inverse FFT using
     * two 1D double array to represent a complex signal.
     * <p>
     * @param signalReal      Real part of signal.
     * @param signalImaginary Imaginary part of signal.
     * <p>
     * @return Real part of the inverse transformFFTed signal.
     */
    public double[] transformInverseFullOptimized(double[] signalReal, double[] signalImaginary)
    {
        double[] retReal = new double[signalReal.length];
        double[] retImage = new double[signalReal.length];

        // compute conjugate and transformFFT
        for(int i = 0; i < signalReal.length; i++) {
            retReal[i] = signalReal[i];
            retImage[i] = -signalImaginary[i];
        }
        ComplexArray1D ret;
        ret = this._transformFullOptimizedFFT(retReal, retImage);

        retReal = (double[]) ret.realValues;
        retImage = (double[]) ret.imaginaryValues;

        // compute conjugate
        for(int i = 0; i < signalReal.length; i++) {
            //retReal[i] = retReal[i];
            retImage[i] = -retImage[i];
        }

        double[] retDouble = new double[signalReal.length];
        for(int i = 0; i < signalReal.length; i++) {
            // normalize by divide by length
            // (ret[i][0] * data.length + ret[i][1] * 0) / (data.length*data.length)+0);
            retDouble[i] = retReal[i] * signalReal.length / (signalReal.length * signalReal.length);
        }
        return retDouble;
    }

    private Complex[] _transform(double[] data)
    {
        return this._transform(this._makeComplex(data));
    }

    public ComplexArray1D transformFullOptimized(double[] data)
    {
        if(this.precomputedIndices == null) { // compute direct DFT and hope the signal isn't very long
            return this._transformDFT(data);
        }
        return this._transformFullOptimizedFFT(data, new double[data.length]);
    }

    public double [][] transform(double[] data)
    {
        if(this.precomputedIndices == null) { // compute direct DFT and hope the signal isn't very long
            return this.transformDFT(data);
        }
        return this.transformFFT(this.makeComplex(data));
    }

    /**
     * This is the recursive variant used for double checking the results.
     * <p>
     * @param signal Signal to transformFFT.
 <p>
     * @return Transformed complex signal.
     */
    private Complex[] _transformRecursivelyFFT(double[] signal)
    {
        return this._transformRecursivelyFFT(this._makeComplex(signal));
    }

    /**
     * This is the recursive variant used for double checking the results.
     * <p>
     * @param signal Signal to transformFFT.
     *   <p>
     * @return Transformed complex signal represented as 2D double array.
     */
    private double[][] transformRecursively(double[] signal)
    {
        return this.transformRecursivelyFFT(this.makeComplex(signal));
    }

    /**
     * This is the recursive variant used for double checking the results.
     * <p>
     * @param signal Signal to transformFFT.
        <p>
     * @return Transformed complex signal.
     */
    private Complex[] _transformRecursivelyFFT(Complex[] signal)
    {
        if(signal.length == 1) {
            return signal; // the FT of a single point is the point itself
        }
        Complex[] evenData = _transformRecursivelyFFT(even(signal));
        Complex[] oddData = _transformRecursivelyFFT(odd(signal));

        Complex[] ret = new Complex[evenData.length * 2];
        int n = evenData.length;
        for(int k = 0; k < n; k++) {
            Complex t = new Complex(Math.cos((-Math.PI * k) / (evenData.length)), Math.sin((-Math.PI * k) / (evenData.length)), "e");
            Complex g = evenData[k];
            Complex u = oddData[k];

            Complex ck = g.add(u.mult(t));
            Complex ckn = g.subtract(u.mult(t));
            ckn.hint = ckn.hint + " c";
            ret[k] = ck;
            ret[k + n] = ckn;
        }
        return ret;
    }

    /**
     * This is the recursive variant used for double checking the results.
     * <p>
     * @param signal Signal to transformFFT.
 <p>
     * @return Transformed complex signal.
     */
    private double[][] transformRecursivelyFFT(double[][] signal)
    {
        if(signal.length == 1) { // the FT of a single point is the point itself
            return signal;
        }
        double[][] evenData = transformRecursivelyFFT(even(signal));
        double[][] oddData = transformRecursivelyFFT(odd(signal));

        double[][] ret = new double[evenData.length * 2][2];
        int n = evenData.length;
        for(int k = 0; k < n; k++) {
            double treal = Math.cos((-Math.PI * k) / (evenData.length));
            double timage = Math.sin((-Math.PI * k) / (evenData.length));

            double greal = evenData[k][0];
            double gimage = evenData[k][1];
            double ureal = oddData[k][0];
            double uimage = oddData[k][1];

            // coefficiencts ck and ckn imaginary and real parts
            double ckreal, ckimage, cknreal, cknimage;
            double tmpimage, tmpreal;

            // compute ck, first multiply t and u
            tmpreal = (ureal * treal - uimage * timage);
            tmpimage = (uimage * treal + ureal * timage);
            // next add g and the result of t*u
            ckreal = greal + tmpreal;
            ckimage = gimage + tmpimage;
            ret[k][0] = ckreal;
            ret[k][1] = ckimage;

            // compute ckn, reuse the u*t stored in [tmpreal,tmpimage]
            // and subtract that from g
            cknreal = greal - tmpreal;
            cknimage = gimage - tmpimage;

            ret[k + n][0] = cknreal;
            ret[k + n][1] = cknimage;
        }
        return ret;
    }

    /**
     * This is the recursive variant used for double checking the results.
     * <p>
     * @param signal Signal to transformFFT.
 <p>
     * @return Transformed real part of signal.
     */
    private double[] _transformInverseRecursivelyFFT(Complex[] signal)
    {
        Complex[] ret = new Complex[signal.length];

        // compute conjugate and transformFFT
        for(int i = 0; i < signal.length; i++) {
            ret[i] = signal[i].conjugate();
        }
        ret = this._transformRecursivelyFFT(ret);

        // compute conjugate
        for(int i = 0; i < signal.length; i++) {
            ret[i] = ret[i].conjugate();
        }

        double[] retDouble = new double[signal.length];
        for(int i = 0; i < signal.length; i++) {
            ret[i] = ret[i].div(new Complex(signal.length, 0));
            retDouble[i] = ret[i].re;
        }
        return retDouble;
    }

    /**
     * This is the recursive variant used for double checking the results.
     * <p>
     * @param signal Signal to transformFFT.
 <p>
     * @return Transformed real part of signal.
     */
    private double[] transformInverseRecursivelyFFT(double[][] signal)
    {
        double[][] ret = new double[signal.length][2];

        // compute conjugate and transformFFT
        for(int i = 0; i < signal.length; i++) {
            ret[i][0] = signal[i][0];
            ret[i][1] = -signal[i][1];
        }
        ret = this.transformRecursivelyFFT(ret);

        // compute conjugate
        for(int i = 0; i < signal.length; i++) {
            //ret[i][0] = ret[i][0];
            ret[i][1] = -ret[i][1];
        }

        double[] retDouble = new double[signal.length];
        for(int i = 0; i < signal.length; i++) {
            // normalize by divide by length
            // (ret[i][0] * signal.length + ret[i][1] * 0) / (signal.length*signal.length)+0);
            retDouble[i] = ret[i][0] * signal.length / (signal.length * signal.length);
        }
        return retDouble;
    }

    /**
     * Converts a real valued signal to a complex signal by adding an imaginary
     * part of i*0.
     * <p>
     * @param signal Real valued signal.
     * <p>
     * @return Complex valued signal.
     */
    protected Complex[] _makeComplex(double[] signal)
    {
        Complex[] cArray = new Complex[signal.length];

        for(int i = 0; i < signal.length; i++) {
            cArray[i] = new Complex(signal[i], 0, "" + i);
        }
        return cArray;
    }

    /**
     * Converts a real valued signal to a complex signal by adding an imaginary
     * part of i*0.
     * <p>
     * @param signal Real valued signal.
     * <p>
     * @return Complex valued signal.
     */
    protected double[][] makeComplex(double[] signal)
    {
        double[][] cArray = new double[signal.length][2];

        for(int i = 0; i < signal.length; i++) {
            cArray[i][0] = signal[i];
        }
        return cArray;
    }

    /**
     * Returns a new array with the even indexed signal values.
     * <p>
     * @param signal Signal to extract even values.
     * <p>
     * @return New array of values at even indices in signal.
     */
    protected Complex[] even(Complex[] signal)
    {
        Complex[] evenData = new Complex[signal.length / 2];
        for(int i = 0; i < signal.length; i += 2) {
            evenData[i / 2] = signal[i];
        }
        return evenData;
    }

    /**
     * Returns a new array with the odd indexed signal values.
     * <p>
     * @param signal Signal to extract odd values.
     * <p>
     * @return New array of values at odd indices in signal.
     */
    protected Complex[] odd(Complex[] signal)
    {
        Complex[] oddData = new Complex[signal.length / 2];
        for(int i = 1; i < signal.length; i += 2) {
            oddData[(i - 1) / 2] = signal[i];
        }
        return oddData;
    }

    /**
     * Returns a new array with the even indexed signal values.
     * <p>
     * @param signal Signal to extract even values.
     * <p>
     * @return New array of values at even indices in signal.
     */
    protected double[][] even(double[][] signal)
    {
        double[][] evenData = new double[signal.length / 2][2];
        for(int i = 0; i < signal.length; i += 2) {
            evenData[i / 2] = signal[i];
        }
        return evenData;
    }

    /**
     * Returns a new array with the odd indexed signal values.
     * <p>
     * @param signal Signal to extract odd values.
     * <p>
     * @return New array of values at odd indices in signal.
     */
    protected double[][] odd(double[][] signal)
    {
        double[][] oddData = new double[signal.length / 2][2];
        for(int i = 1; i < signal.length; i += 2) {
            oddData[(i - 1) / 2] = signal[i];
        }
        return oddData;
    }
    
    /**
     * Computes the direct dft for signals not a power of two.
     * This is not very efficient, but we expect only small signals to use this
     * method, hence it is reasonable to do it that way. 
     * 
     * @param realSignal Small signal which a length of not a power of two.
     * @return Transformed signal. 
     */
    private ComplexArray1D _transformDFT(double [] realSignal)
    {
        int N = realSignal.length;
     
        double [] realCoeffs = new double[N];
        double [] imagCoeffs = new double[N];
        
        for(int k = 0; k < N; k++) {
            double realCoeff = 0;
            double imagCoeff = 0;
            for(int n = 0; n < N; n++) {
                realCoeff += realSignal[n] * Math.cos(2*Math.PI*k*n/N);
                imagCoeff += realSignal[n] * Math.sin(2*Math.PI*k*n/N);
            }
            realCoeffs[k] = realCoeff;
            imagCoeffs[k] = -imagCoeff;
        }
        return new ComplexArray1D(realCoeffs, imagCoeffs);
    }
    
        /**
     * Computes the direct dft for signals not a power of two.
     * This is not very efficient, but we expect only small signals to use this
     * method, hence it is reasonable to do it that way. 
     * 
     * @param realSignal Small signal which a length of not a power of two.
     * @return Transformed signal. 
     */
    private double [][] transformDFT(double [] realSignal)
    {
        int N = realSignal.length;
     
        double [][] transformedSignal = new double[N][2];
        
        for(int k = 0; k < N; k++) {
            double realCoeff = 0;
            double imagCoeff = 0;
            for(int n = 0; n < N; n++) {
                realCoeff += realSignal[n] * Math.cos(2*Math.PI*k*n/N);
                imagCoeff += realSignal[n] * Math.sin(2*Math.PI*k*n/N);
            }
            transformedSignal[k][0] = realCoeff;
            transformedSignal[k][1] = -imagCoeff;
        }
        return transformedSignal;
    }

}
