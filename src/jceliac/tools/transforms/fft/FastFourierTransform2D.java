/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.transforms.fft;

import jceliac.tools.filter.Complex;

/**
 * Implementation of the 2D FFT.
 * <p>
 * @author shegen
 */
public class FastFourierTransform2D
        extends FastFourierTransform
{

    // currently only quadratic images with a size of a power of 2 are supported
    public FastFourierTransform2D(int size)
    {
        super(size);
    }

    /**
     * Converts a real valued two dimensional signal to a complex signal by
     * adding an imaginary part of i*0.
     * <p>
     * @param signal Real valued signal.
     * <p>
     * @return Complex valued signal.
     */
    public Complex[][] _makeComplex(double[][] signal)
    {
        Complex[][] cArray = new Complex[signal.length][signal[0].length];

        for(int i = 0; i < signal.length; i++) {
            for(int j = 0; j < signal[0].length; j++) {
                cArray[i][j] = new Complex(signal[i][j], 0, "");
            }
        }
        return cArray;
    }

    /**
     * Converts a real valued two dimensional signal to a complex signal by
     * adding an imaginary part of i*0.
     * <p>
     * @param signal Real valued signal.
     * <p>
     * @return Complex valued signal.
     */
    public double[][][] makeComplex(double[][] signal)
    {
        double[][][] cArray = new double[signal.length][signal[0].length][2];

        for(int i = 0; i < signal.length; i++) {
            for(int j = 0; j < signal[0].length; j++) {
                cArray[i][j][0] = signal[i][j];
            }
        }
        return cArray;
    }

    /**
     * The 2D-FFT is pretty much the same as the 1D-FFT,he basic difference is
 that we first tranform the columns, and then transformFFT the rows (using
 the prior transformFFTed coefficients).
     * <p>
     * <p>
     * @param signal Complex signal to transformFFT.
 <p>
     * @return Complex transformFFTed signal.
     */
    public Complex[][] _transform(Complex[][] signal)
    {
        Complex[][] transformed = this._transformColumns(signal);
        return this._transformRows(transformed);

    }

    /**
     * The 2D-FFT is pretty much the same as the 1D-FFT,he basic difference is
 that we first tranform the columns, and then transformFFT the rows (using
 the prior transformFFTed coefficients).
     * <p>
     * <p>
     * @param signal Complex signal to transformFFT.
 <p>
     * @return Complex transformFFTed signal.
     */
    public double[][][] transform(double[][][] signal)
    {
        double[][][] transformed = this.transformColumns(signal);
        return this.transformRows(transformed);
    }

    /**
     * Inverse 2D FFT of a complex signal.
     * <p>
     * @param signal Complex signal.
     * <p>
     * @return Real part of the inversly transformFFTed signal.
     */
    public double[][] _transformInverse(Complex[][] signal)
    {
        Complex[][] conjugatedData = new Complex[signal.length][signal[0].length];
        /* conjugate all */
        for(int i = 0; i < signal.length; i++) {
            for(int j = 0; j < signal[0].length; j++) {
                conjugatedData[i][j] = signal[i][j].conjugate();
            }
        }
        Complex[][] transformed = this._transform(conjugatedData);
        for(Complex[] tmp : transformed) {
            for(int j = 0; j < transformed[0].length; j++) {
                tmp[j] = tmp[j].conjugate();
            }
        }
        /* divide */
        double[][] realData = new double[signal.length][signal[0].length];
        for(int i = 0; i < transformed.length; i++) {
            for(int j = 0; j < transformed[0].length; j++) {
                Complex tmp = transformed[i][j].div(new Complex(signal.length * signal.length, 0));
                realData[i][j] = tmp.re;
            }
        }
        return realData;
    }

    /**
     * Inverse 2D FFT of a complex signal.
     * <p>
     * @param signal Complex signal.
     * <p>
     * @return Real part of the inversly transformFFTed signal.
     */
    public double[][] transformInverse(double[][][] signal)
    {
        double[][][] conjugatedData = new double[signal.length][signal[0].length][2];
        /* conjugate all */
        for(int i = 0; i < signal.length; i++) {
            for(int j = 0; j < signal[0].length; j++) {
                conjugatedData[i][j][0] = signal[i][j][0];
                conjugatedData[i][j][1] = -signal[i][j][1];
            }
        }
        double[][][] transformed = this.transform(conjugatedData);
        for(double[][] tmp : transformed) {
            for(int j = 0; j < transformed[0].length; j++) {
                //transformed[i][j][0] = transformed[i][j][0];
                tmp[j][1] = -tmp[j][1];
            }
        }
        /* divide */
        double[][] realData = new double[signal.length][signal[0].length];
        for(int i = 0; i < transformed.length; i++) {
            for(int j = 0; j < transformed[0].length; j++) {

                /*
                 * Complex tmp = transformed[i][j].div(new
                 * Complex(signal.length*signal.length,0)); we divide but are
                 * only interested in the real part, remember that
                 * transformed[i][j][0] * signal.length*signal.length +
                 * transformed[i][j][1]*0 /
                 * signal.length*signal.length*signal.length*signal.length
                 */
                realData[i][j] = ((transformed[i][j][0]))
                                 / (signal.length * signal.length);
            }
        }
        return realData;
    }

    /**
     * Transformation of the columns of a complex 2D signal.
     * <p>
     * @param signal Complex signal.
     * <p>
     * @return Real part of the inversly transformFFTed signal.
     */
    protected Complex[][] _transformColumns(Complex[][] signal)
    {
        Complex[][] transformed = new Complex[signal.length][signal[0].length];

        for(int colIndex = 0; colIndex < signal.length; colIndex++) {

            transformed[colIndex] = this._transform(signal[colIndex]);
        }
        return transformed;
    }

    /**
     * Transformation of the columns of a complex 2D signal.
     * <p>
     * @param signal Complex signal.
     * <p>
     * @return Real part of the inversly transformFFTed signal.
     */
    protected double[][][] transformColumns(double[][][] signal)
    {
        double[][][] transformed = new double[signal.length][signal[0].length][2];

        for(int colIndex = 0; colIndex < signal.length; colIndex++) {
            transformed[colIndex] = this.transformFFT(signal[colIndex]);
        }
        return transformed;
    }

    /**
     * Transformation of the rows of a complex 2D signal.
     * <p>
     * @param signal Complex signal.
     * <p>
     * @return Real part of the inversly transformFFTed signal.
     */
    protected Complex[][] _transformRows(Complex[][] signal)
    {
        Complex[][] transformed = new Complex[signal.length][signal[0].length];

        for(int rowIndex = 0; rowIndex < signal[0].length; rowIndex++) {
            Complex[] row = new Complex[signal.length];

            for(int i = 0; i < signal.length; i++) {
                row[i] = signal[i][rowIndex];
            }
            Complex[] transformedRow = this._transform(row);
            for(int i = 0; i < transformedRow.length; i++) {
                transformed[i][rowIndex] = transformedRow[i];
            }
        }
        return transformed;
    }

    /**
     * Transformation of the rows of a complex 2D signal.
     * <p>
     * @param signal Complex signal.
     * <p>
     * @return Real part of the inversly transformFFTed signal.
     */
    protected double[][][] transformRows(double[][][] signal)
    {
        double[][][] transformed = new double[signal.length][signal[0].length][2];

        for(int rowIndex = 0; rowIndex < signal[0].length; rowIndex++) {
            double[][] row = new double[signal.length][2];

            for(int i = 0; i < signal.length; i++) {
                row[i] = signal[i][rowIndex];
            }
            double[][] transformedRow = this.transformFFT(row);
            for(int i = 0; i < transformedRow.length; i++) {
                transformed[i][rowIndex] = transformedRow[i];
            }
        }
        return transformed;
    }

}
