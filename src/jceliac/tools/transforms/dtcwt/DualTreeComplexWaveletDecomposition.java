/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.tools.transforms.dtcwt;

import java.util.HashMap;

/**
 *
 * @author shegen
 */
public class DualTreeComplexWaveletDecomposition
{
    // keys are scale and subband index, both starting with 1
    private final HashMap <Integer, HashMap <Integer, ComplexArray2D>> complexSubbandData;
    private final HashMap <Integer, double [][]> realLowpassSubbands;

    public DualTreeComplexWaveletDecomposition()
    {
        this.complexSubbandData = new HashMap<>();
        this.realLowpassSubbands = new HashMap <>();
    }
    
    public void setComplexSubband(int scaleLevel, int subbandIndex, ComplexArray2D data)
    {
        HashMap <Integer, ComplexArray2D> subbandsAtScale;
        if(this.complexSubbandData.containsKey(scaleLevel) == false) {
            subbandsAtScale = new HashMap<>();
        }else {
            subbandsAtScale = this.complexSubbandData.get(scaleLevel);
        }
        subbandsAtScale.put(subbandIndex, data);
        this.complexSubbandData.put(scaleLevel, subbandsAtScale);
    }
    
    /**
     * Creates a deep copy of the lowpass subband. 
     * 
     * @param lowpassSubband Real lowpass subband that is stored. 
     * @param scaleLevel Scale level of lowpass subband. 
     */
    public void setRealLowpassSubband(int scaleLevel, double [][] lowpassSubband)
    {
        double [][] copy = new double[lowpassSubband.length][lowpassSubband.length];
        for(int i = 0; i < copy.length; i++) {
            System.arraycopy(lowpassSubband[i], 0, copy[i], 0, copy.length);
        }
        this.realLowpassSubbands.put(scaleLevel, copy);
    }
    
    public ComplexArray2D getComplexSubband(int scaleLevel, int subbandIndex) {
        return this.complexSubbandData.get(scaleLevel).get(subbandIndex);
    }

    public double [][] getRealLowpassSubband(int scaleLevel) {
        return this.realLowpassSubbands.get(scaleLevel);
    }
    
    
}
