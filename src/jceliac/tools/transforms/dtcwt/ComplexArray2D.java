/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.tools.transforms.dtcwt;

/**
 *
 * @author shegen
 */
public class ComplexArray2D 
{
    public double [][] realValues;
    public double [][] imaginaryValues;

    public ComplexArray2D(double[][] realValue, double[][] imaginaryValues)
    {
        this.realValues = realValue;
        this.imaginaryValues = imaginaryValues;
    } 
    
    public double [][] abs() 
    {
        double [][] absValues = new double[this.realValues.length][this.realValues[0].length];
        for(int i = 0; i < absValues.length; i++) {
            for(int j = 0; j < absValues[0].length; j++) {
                absValues[i][j] = Math.sqrt(this.realValues[i][j] * this.realValues[i][j] +
                                            this.imaginaryValues[i][j]  * this.imaginaryValues[i][j]);
            }
        }
        return absValues;
    }
}
