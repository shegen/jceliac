/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.transforms.dtcwt;

import jceliac.tools.transforms.dtcwt.filter.BiorthogonalFilter;
import jceliac.tools.transforms.dtcwt.filter.QShiftFilter;
import jceliac.tools.filter.DataFilter2D;
import jceliac.JCeliacGenericException;
import java.util.function.BiPredicate;
import java.util.ArrayList;

/**
 * This is a port of Kingsbury's matlab code, some stuff is written to 
 * resemble the matlab style, so this might not very well readble.
 * <p>
 * @author shegen
 */
public class DualTreeComplexWaveletTransform
{
    private final BiorthogonalFilter biort;
    private final QShiftFilter qshift;
    private DualTreeComplexWaveletDecomposition decompositionTree;

    public DualTreeComplexWaveletTransform(BiorthogonalFilter biort, QShiftFilter qshift)
    {
        this.biort = biort;
        this.qshift = qshift;
    }

    public void transformForward(double[][] dataChannel, int nlevels)
            throws JCeliacGenericException
    {
        this.decompositionTree = new DualTreeComplexWaveletDecomposition();

        double [][] extendedData = this.extendDataDivisible2(dataChannel);
       
        double[][] LoLo = null;
        if(nlevels >= 1) {
            // Do odd top-level filters on cols.   
            double[][] Lo = this.colfilter(extendedData, this.biort.h0o(), true);
            double[][] Hi = this.colfilter(extendedData, this.biort.h1o(), true);

            LoLo = this.colfilter(Lo, this.biort.h0o(), false);
            ComplexArray2D[] horizontalPair = this.quadsToComplex(colfilter(Hi, this.biort.h0o(), false));
            ComplexArray2D[] verticalPair = this.quadsToComplex(colfilter(Lo, this.biort.h1o(), false));
            ComplexArray2D[] diagonalPair = this.quadsToComplex(colfilter(Hi, this.biort.h1o(), false));

            // store data
            this.decompositionTree.setRealLowpassSubband(1, LoLo);
            this.decompositionTree.setComplexSubband(1, 1, horizontalPair[0]);
            this.decompositionTree.setComplexSubband(1, 6, horizontalPair[1]);
            this.decompositionTree.setComplexSubband(1, 3, verticalPair[0]);
            this.decompositionTree.setComplexSubband(1, 4, verticalPair[1]);
            this.decompositionTree.setComplexSubband(1, 2, diagonalPair[0]);
            this.decompositionTree.setComplexSubband(1, 5, diagonalPair[1]);

        }
        if(nlevels >= 2) {
            for(int level = 2; level <= nlevels; level++) {
                LoLo = this.extendDataDivisible4(LoLo);
                
                double[][] Lo = this.coldfilt(LoLo, this.qshift.h0b(), this.qshift.h0a(), true);
                double[][] Hi = this.coldfilt(LoLo, this.qshift.h1b(), this.qshift.h1a(), true);

                LoLo = this.coldfilt(Lo, this.qshift.h0b(), this.qshift.h0a(), false);
                ComplexArray2D[] horizontalPair = this.quadsToComplex(coldfilt(Hi, this.qshift.h0b(), this.qshift.h0a(), false));
                ComplexArray2D[] verticalPair = this.quadsToComplex(coldfilt(Lo, this.qshift.h1b(), this.qshift.h1a(), false));
                ComplexArray2D[] diagonalPair = this.quadsToComplex(coldfilt(Hi, this.qshift.h1b(), this.qshift.h1a(), false));
                
                // store data
                this.decompositionTree.setRealLowpassSubband(level, LoLo);
                this.decompositionTree.setComplexSubband(level, 1, horizontalPair[0]);
                this.decompositionTree.setComplexSubband(level, 6, horizontalPair[1]);
                this.decompositionTree.setComplexSubband(level, 3, verticalPair[0]);
                this.decompositionTree.setComplexSubband(level, 4, verticalPair[1]);
                this.decompositionTree.setComplexSubband(level, 2, diagonalPair[0]);
                this.decompositionTree.setComplexSubband(level, 5, diagonalPair[1]);
            }
        }
    }

    public DualTreeComplexWaveletDecomposition getDecompositionTree()
    {
        return decompositionTree;
    }

    private double[][] coldfilt(double[][] x, double[] ha, double[] hb, boolean column)
            throws JCeliacGenericException
    {
        if(ha.length != hb.length) {
            throw new JCeliacGenericException("Lengths of ha and hb must be the same!");
        }
        if(ha.length % 2 != 0 || hb.length % 2 != 0) {
            throw new JCeliacGenericException("Lengths of ha and hb must be even!");
        }
        int m, c, r;
        if(column) {
            m = ha.length;
            c = x.length;
            r = x[0].length;
        } else {
            m = ha.length;
            r = x.length;
            c = x[0].length;
        }

        // floor is the same as fix for non negative values, and h.length will be >= 0
        int m2 = (int) Math.floor(m / 2.0d);

        int[] indices = new int[r + m - (1 - m) + 1];
        int index = 0;
        for(int i = 1 - m; i <= r + m; i++) {
            indices[index] = i;
            index++;
        }
        //  Set up vector for symmetric extension of X with repeated end samples.
        indices = reflect(indices, 0.5d, r + 0.5d); // Use 'reflect' so d < m works OK.
        // Select odd and even samples from ha and hb.
        // note that even and odd are named as in matlab allthough the indices
        // in java are actually starting with 0 and odd is therefore even.
        double[] hao = new double[(int) (ha.length / 2.0d)];
        double[] hae = new double[(int) (ha.length / 2.0d)];
        double[] hbo = new double[(int) (ha.length / 2.0d)];
        double[] hbe = new double[(int) (ha.length / 2.0d)];

        for(int i = 0; i < (m / 2); i++) {
            hao[i] = ha[i * 2];
            hbo[i] = hb[i * 2];
            hae[i] = ha[(i * 2) + 1];
            hbe[i] = hb[(i * 2) + 1];
        }
        int[] t = new int[(int) ((((r + 2 * m - 2) - 6) / 4) + 1)];
        int[] tMinus1 = new int[t.length];
        int[] tMinus2 = new int[t.length];
        int[] tMinus3 = new int[t.length];

        index = 0;
        for(int v = 6; v <= (r + 2 * m - 2); v += 4) {
            t[index] = v;
            tMinus1[index] = v - 1;
            tMinus2[index] = v - 2;
            tMinus3[index] = v - 3;
            index++;
        }
        int r2 = (int) (r / 2.0d);
        double[][] Y = new double[c][r2];
        int[] s1 = new int[r2 / 2];
        int[] s2 = new int[r2 / 2];

        if(arrayMultSum(ha, hb) > 0) {
            index = 0;
            for(int i = 1; i < r2; i += 2) {
                s1[index] = i;
                s2[index] = i + 1;
                index++;
            }
        } else {
            index = 0;
            for(int i = 1; i < r2; i += 2) {
                s2[index] = i;
                s1[index] = i + 1;
                index++;
            }
        }
        double[][] subSignal1 = extractIndicesFromSignal(x, extractInnerIndices(indices, tMinus1), column);
        double[][] subSignal2 = extractIndicesFromSignal(x, extractInnerIndices(indices, tMinus3), column);
        double[][] subSignal3 = extractIndicesFromSignal(x, extractInnerIndices(indices, t), column);
        double[][] subSignal4 = extractIndicesFromSignal(x, extractInnerIndices(indices, tMinus2), column);

        if(column) {
            double[][] Ys1 = arraySum(DataFilter2D.convolveValidSeparable(subSignal1, hao, false, 1),
                                      DataFilter2D.convolveValidSeparable(subSignal2, hae, false, 1));
            double[][] Ys2 = arraySum(DataFilter2D.convolveValidSeparable(subSignal3, hbo, false, 1),
                                      DataFilter2D.convolveValidSeparable(subSignal4, hbe, false, 1));

            double[][] Ys = new double[Ys1.length][Ys1[0].length + Ys2[0].length];
            for(int i = 0; i < Ys1.length; i++) {
                for(int j = 0; j < Ys1[0].length; j++) {
                    Ys[i][s1[j] - 1] = Ys1[i][j];
                    Ys[i][s2[j] - 1] = Ys2[i][j];
                }
            }
            return Ys;
        } else {
            double[][] Ys1 = arraySum(DataFilter2D.convolveValidSeparable(subSignal1, hao, false, 0),
                                      DataFilter2D.convolveValidSeparable(subSignal2, hae, false, 0));
            double[][] Ys2 = arraySum(DataFilter2D.convolveValidSeparable(subSignal3, hbo, false, 0),
                                      DataFilter2D.convolveValidSeparable(subSignal4, hbe, false, 0));

            double[][] Ys = new double[Ys1.length + Ys2.length][Ys1[0].length];
            for(int i = 0; i < Ys1.length; i++) {
                for(int j = 0; j < Ys1[0].length; j++) {
                    Ys[s1[i] - 1][j] = Ys1[i][j];
                    Ys[s2[i] - 1][j] = Ys2[i][j];
                }
            }
            return Ys;
        }
    }

    private double[][] arraySum(double[][] array1, double[][] array2)
    {
        double[][] arraySum = new double[array1.length][array1[0].length];
        for(int i = 0; i < array1.length; i++) {
            for(int j = 0; j < array1[0].length; j++) {
                arraySum[i][j] = array1[i][j] + array2[i][j];
            }
        }
        return arraySum;
    }

    private double arrayMultSum(double[] array1, double[] array2)
    {
        double sum = 0;
        for(int i = 0; i < array1.length; i++) {
            sum += array1[i] * array2[i];
        }
        return sum;
    }

    /*
     * Filter the columns of image X using filter vector h, without decimation.
     * If length(h) is odd, each output sample is aligned with each input sample
     * and Y is the same size as X. If length(h) is even, each output sample is
     * aligned with the mid point of each pair of input samples, and size(Y) =
     * size(X) + [1 0];
     */
    private double[][] colfilter(double[][] x, double[] h, boolean column)
            throws JCeliacGenericException
    {
        int c = x.length;
        int r = x[0].length;
        int m = h.length;

        // floor is the same as fix for non negative values, and h.length will be >= 0
        int m2 = (int) Math.floor(h.length / 2.0d);
        if(any(x)) {
            int[] indices = new int[r + m2 - (1 - m2) + 1];
            int index = 0;
            for(int i = 1 - m2; i <= r + m2; i++) {
                indices[index] = i;
                index++;
            }
            // Symmetrically extend with repeat of end samples.
            indices = reflect(indices, 0.5d, r + 0.5d);
            // Perform filtering on the columns of the extended matrix X(xe,:), keeping
            // only the 'valid' output samples, so Y is the same size as X if m is odd. 
            double[][] XExtended = extractIndicesFromSignal(x, indices, column);
            double[][] Y;
            if(column) {
                Y = DataFilter2D.convolveValidSeparable(XExtended, h, false, 1);
            } else {
                Y = DataFilter2D.convolveValidSeparable(XExtended, h, false, 0);
            }
            return Y;
        } else {
            return new double[c][r + 1 - (m % 2)];
        }
    }

    private double[][] extractIndicesFromSignal(double[][] x, int[] indices, boolean column)
    {
        double[][] XExtended;

        if(column) {
            XExtended = new double[x.length][indices.length];
            for(int i = 0; i < XExtended.length; i++) {
                for(int j = 0; j < XExtended[0].length; j++) {
                    int index = indices[j];
                    XExtended[i][j] = x[i][index - 1];
                }
            }
        } else {
            XExtended = new double[indices.length][x[0].length];
            for(int i = 0; i < indices.length; i++) {
                for(int j = 0; j < XExtended[0].length; j++) {
                    int index = indices[i];
                    XExtended[i][j] = x[index - 1][j];
                }
            }
        }
        return XExtended;
    }

    private int[] extractInnerIndices(int[] outerIndices, int[] innerIndices)
    {
        int[] retIndices = new int[innerIndices.length];
        for(int i = 0; i < innerIndices.length; i++) {
            retIndices[i] = outerIndices[innerIndices[i] - 1];
        }
        return retIndices;
    }

    /*
     * Reflect the values in matrix x about the scalar values minx and maxx.
     * Hence a vector x containing a long linearly increasing series is
     * converted into a waveform which ramps linearly up and down between minx
     * and maxx. If x contains integers and minx and maxx are (integers + 0.5),
     * the ramps will have repeated max and min samples.
     */
    private int[] reflect(int[] x, double minx, double maxx)
    {
        int[] y = new int[x.length];
        System.arraycopy(x, 0, y, 0, y.length);

        Integer[] t = find(y, maxx, (a, b) -> a > b);
        for(Integer index : t) {
            y[index] = (int) (2.0d * maxx - y[index]);
        }
        t = find(y, minx, (a, b) -> a < b);
        while(t.length > 0) {
            for(Integer index : t) {
                y[index] = (int) (2.0d * minx - y[index]);
            }
            t = find(y, maxx, (a, b) -> a > b);
            if(t.length > 0) {
                for(Integer index : t) {
                    y[index] = (int) (2.0d * maxx - y[index]);
                }
            }
            t = find(y, minx, (a, b) -> a < b);
        }
        return y;
    }

    private Integer[] find(int[] data, double value,
                                  BiPredicate<Integer, Double> t)
    {
        ArrayList<Integer> retIndices = new ArrayList<>();
        for(int i = 0; i < data.length; i++) {
            boolean r = t.test(data[i], value);
            if(r) {
                retIndices.add(i);
            }
        }
        Integer[] retArray = retIndices.toArray(new Integer[retIndices.size()]);
        return retArray;
    }

    private boolean any(double[][] data)
    {
        for(int i = 0; i < data.length; i++) {
            for(int j = 0; j < data[0].length; j++) {
                if(data[i][j] != 0) {
                    return true;
                }
            }
        }
        return false;
    }
    
    private double [][] extendRows(double [][] data, boolean extendTwo) 
    {
        double [][] extendedSignal;
        int indexOffset = 0;
        
        if(extendTwo == false) {
            extendedSignal = new double[data.length][data[0].length+1];
            // duplicate bottom row
            for(int i = 0; i < data.length; i++) {
                extendedSignal[i][extendedSignal[0].length-1] = data[i][data[0].length-1];
            }
            
        }else {
            indexOffset = 1;
            extendedSignal = new double[data.length][data[0].length+2];
            // duplicate front and bottom row
            for(int i = 0; i < data.length; i++) {
                extendedSignal[i][extendedSignal[0].length-1] = data[i][data[0].length-1];
                extendedSignal[i][0] = data[i][0];
            }
        }
        // copy the rest of the data
        for(int i = 0; i < data.length; i++) {
            System.arraycopy(data[i], 0, extendedSignal[i], indexOffset, data[0].length);
        }
        return extendedSignal;    
    }
    
    private double [][] extendColumns(double [][] data, boolean extendTwo) 
    {
        double [][] extendedSignal;
        int indexOffset;
        
        if(extendTwo == false) {
            indexOffset = 0;
            extendedSignal = new double[data.length+1][data[0].length];
            System.arraycopy(data[data.length-1], 0, extendedSignal[extendedSignal.length-1], 0, extendedSignal[0].length);
            
        }else {
            indexOffset = 1;
            extendedSignal = new double[data.length+2][data[0].length];
            System.arraycopy(data[0], 0, extendedSignal[0], 0, extendedSignal[0].length);
            System.arraycopy(data[data.length-1], 0, extendedSignal[extendedSignal.length-1], 0, extendedSignal[extendedSignal.length-1].length);
            
        }
        // copy the rest of the data
        for(int i = 0; i < data.length; i++) {
            System.arraycopy(data[i], 0, extendedSignal[i+indexOffset], 0, data[0].length);
        }
        return extendedSignal;    
    }
    
    private double [][] extendDataDivisible2(double [][] data) 
    {
        double [][] extendedSignal;
        boolean extendRow = false;
        boolean extendCol = false;
        
        if(data.length % 2 == 0 && data[0].length % 2 == 0) {
            return data;
        }
        if(data.length % 2 == 1)
        {
            extendCol = true;
        }
        if(data[0].length % 2 == 1) {
            extendRow = true;
        }
        
        if(extendRow) {
            extendedSignal = this.extendRows(data, false);
            if(extendCol) {
                extendedSignal = this.extendColumns(extendedSignal, false);
            }
        }else if(extendCol) {
            extendedSignal = this.extendColumns(data, false);
        }else {
            extendedSignal = data;
        }
        return extendedSignal;
    }
    
    private double [][] extendDataDivisible4(double [][] data) 
    {
        double [][] extendedSignal;
        boolean extendRow = false;
        boolean extendCol = false;
        
        if(data.length % 4 == 0 && data[0].length % 4 == 0) {
            return data;
        }
        if(data.length % 4 != 0)
        {
            extendCol = true;
        }
        if(data[0].length % 4 != 0) {
            extendRow = true;
        }
        
        if(extendRow) {
            extendedSignal = this.extendRows(data, true);
            if(extendCol) {
                extendedSignal = this.extendColumns(extendedSignal, true);
            }
        }else if(extendCol) {
            extendedSignal = this.extendColumns(data, true);
        }else {
            extendedSignal = data;
        }
        return extendedSignal;
    }
        
    // Convert from quads in y to complex numbers in z.
    private ComplexArray2D[] quadsToComplex(double[][] y)
    {
        double jReal = Math.sqrt(0.5);
        double jImage = Math.sqrt(0.5);

        double[][] pReal = new double[y.length / 2][y[0].length / 2];
        double[][] pImage = new double[y.length / 2][y[0].length / 2];
        double[][] qReal = new double[y.length / 2][y[0].length / 2];
        double[][] qImage = new double[y.length / 2][y[0].length / 2];

        for(int ind1 = 1; ind1 <= y.length; ind1 += 2) {
            for(int ind2 = 1; ind2 <= y[0].length; ind2 += 2) {
                pReal[(ind1 - 1) / 2][(ind2 - 1) / 2] = y[ind1 - 1][ind2 - 1] * jReal;
                pImage[(ind1 - 1) / 2][(ind2 - 1) / 2] = y[ind1][ind2 - 1] * jImage;

                qReal[(ind1 - 1) / 2][(ind2 - 1) / 2] = y[ind1][ind2] * jReal;
                qImage[(ind1 - 1) / 2][(ind2 - 1) / 2] = -y[ind1 - 1][ind2] * jImage;
            }
        }
        double[][][] zReal = new double[2][pReal.length][pReal[0].length];
        double[][][] zImage = new double[2][pReal.length][pReal[0].length];
        for(int i = 0; i < pReal.length; i++) {
            for(int j = 0; j < pReal[0].length; j++) {
                zReal[0][i][j] = pReal[i][j] - qReal[i][j];
                zReal[1][i][j] = pReal[i][j] + qReal[i][j];

                zImage[0][i][j] = pImage[i][j] - qImage[i][j];
                zImage[1][i][j] = pImage[i][j] + qImage[i][j];
            }
        }
        ComplexArray2D[] z = new ComplexArray2D[2];
        z[0] = new ComplexArray2D(zReal[0], zImage[0]);
        z[1] = new ComplexArray2D(zReal[1], zImage[1]);
        return z;

    }
}
