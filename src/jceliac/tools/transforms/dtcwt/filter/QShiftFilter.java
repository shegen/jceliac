/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.transforms.dtcwt.filter;

/**
 *
 * @author shegen
 */
public abstract class QShiftFilter
{
    public abstract double [] g0a();
    public abstract double [] g0b();
    public abstract double [] g1a();
    public abstract double [] g1b();
    public abstract double [] h0a();
    public abstract double [] h0b();
    public abstract double [] h1a();
    public abstract double [] h1b();  
}
