/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.transforms.dtcwt.filter;

/**
 * Filter used by the dtcwt features. 
 * 
 * @author shegen
 */
public class QShiftFilter_QShift_B
        extends QShiftFilter
{

    private final double[] h0a = {0.0032531428, -0.0038832120, 0.0346603468,
                                  -0.0388728013, -0.1172038877, 0.2752953847,
                                  0.7561456439, 0.5688104207, 0.0118660920,
                                  -0.1067118047, 0.0238253848, 0.0170252239,
                                  -0.0054394759, -0.0045568956};

    private final double[] h0b = {-0.0045568956, -0.0054394759, 0.0170252239,
                                  0.0238253848, -0.1067118047, 0.0118660920,
                                  0.5688104207, 0.7561456439, 0.2752953847,
                                  -0.1172038877, -0.0388728013, 0.0346603468,
                                  -0.0038832120, 0.0032531428};

    private final double[] h1a = {-0.0045568956, 0.0054394759, 0.0170252239,
                                  -0.0238253848, -0.1067118047, -0.0118660920,
                                  0.5688104207, -0.7561456439, 0.2752953847,
                                  0.1172038877, -0.0388728013, -0.0346603468,
                                  -0.0038832120, -0.0032531428};

    private final double[] h1b = {-0.0032531428, -0.0038832120, -0.0346603468,
                                  -0.0388728013, 0.1172038877, 0.2752953847,
                                  -0.7561456439, 0.5688104207, -0.0118660920,
                                  -0.1067118047, -0.0238253848, 0.0170252239,
                                  0.0054394759, -0.0045568956};

    @Override
    public double[] g0a()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double[] g0b()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double[] g1a()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double[] g1b()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double[] h0a()
    {
        return this.h0a;
    }

    @Override
    public double[] h0b()
    {
        return this.h0b;
    }

    @Override
    public double[] h1a()
    {
        return this.h1a;
    }

    @Override
    public double[] h1b()
    {
        return this.h1b;
    }

}
