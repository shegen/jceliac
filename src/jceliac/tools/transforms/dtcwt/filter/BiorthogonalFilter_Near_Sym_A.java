/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.transforms.dtcwt.filter;

/**
 *
 * @author shegen
 */
public class BiorthogonalFilter_Near_Sym_A
        extends BiorthogonalFilter
{

    private final double[] h0o = {-0.05, 0.25, 0.6, 0.25, -0.05};

    private final double[] h1o = {0.0107142857, -0.0535714286, -0.2607142857,
                                  0.6071428571, -0.2607142857, -0.0535714286,
                                  0.0107142857};

    @Override
    public double[] g0o()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double[] g1o()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double[] h0o()
    {
        return this.h0o;
    }

    @Override
    public double[] h1o()
    {
        return this.h1o;
    }

}
