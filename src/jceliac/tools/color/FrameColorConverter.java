/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.tools.color;

import jceliac.tools.data.*;

/**
 * Converts the color of a frame.
 * <p>
 * @author shegen
 */
public class FrameColorConverter
{

    /**
     * Converts a frame from RGB to LAB color space.
     * <p>
     * Note: This is not compatible to matlab applycform() Maximum value of L is
     * 100!
     * <p>
     * @param rgbFrame Frame in RGB colorspace.
     * <p>
     * @return New frame in LAB colorspace.
     * <p>
     */
    public static Frame convertRBGtoLAB(Frame rgbFrame)
    {
        ColorSpaceConverter converter = new ColorSpaceConverter();

        double[][] redChannel = rgbFrame.getRedData();
        double[][] greenChannel = rgbFrame.getGreenData();
        double[][] blueChannel = rgbFrame.getBlueData();

        // next convert XYZ to LAB
        double[][] luminance = new double[rgbFrame.getWidth()][rgbFrame.getHeight()];
        double[][] chromA = new double[rgbFrame.getWidth()][rgbFrame.getHeight()];
        double[][] chromB = new double[rgbFrame.getWidth()][rgbFrame.getHeight()];

        for(int i = 0; i < rgbFrame.getWidth(); i++) {
            for(int j = 0; j < rgbFrame.getHeight(); j++) {
                double[] result = converter.RGBtoLAB((int) redChannel[i][j], (int) greenChannel[i][j],
                                                     (int) blueChannel[i][j]);

                double l = result[0];
                double a = result[1];
                double b = result[2];

                luminance[i][j] = l;
                chromA[i][j] = a;
                chromB[i][j] = b;
            }
        }
        Frame labFrame = new Frame(Frame.COLORSPACE_LAB);
        labFrame.setSignalIdentifier(rgbFrame.getSignalIdentifier());
        labFrame.setFrameIdentifier(rgbFrame.getFrameIdentifier());
        labFrame.setLuminanceData(luminance);
        labFrame.setChromAData(chromA);
        labFrame.setChromBData(chromB);
        return labFrame;
    }

    /**
     * Converts a frame from LAB to RGB color space.
     * <p>
     * Note: This is not compatible to matlab applycform() Maximum value of L is
     * 100!
     * <p>
     * <p>
     * @param labFrame Frame in LAB colorspace.
     * <p>
     * @return New frame in RGB colorspace.
     */
    public static Frame convertLABtoRGB(Frame labFrame)
    {
        ColorSpaceConverter converter = new ColorSpaceConverter();

        double[][] luminance = labFrame.getLuminanceData();
        double[][] chromA = labFrame.getChromAData();
        double[][] chromB = labFrame.getChromBData();

        // next convert XYZ to LAB
        double[][] red = new double[labFrame.getWidth()][labFrame.getHeight()];
        double[][] green = new double[labFrame.getWidth()][labFrame.getHeight()];
        double[][] blue = new double[labFrame.getWidth()][labFrame.getHeight()];
        double[][] gray = new double[labFrame.getWidth()][labFrame.getHeight()];

        for(int i = 0; i < labFrame.getWidth(); i++) {
            for(int j = 0; j < labFrame.getHeight(); j++) {
                int[] result = converter.LABtoRGB((int) luminance[i][j], (int) chromA[i][j],
                                                  (int) chromB[i][j]);

                double r = (double) result[0];
                double g = (double) result[1];
                double b = (double) result[2];
                int gr = (int) Math.round((r * 0.2989 + g * 0.5870 + b * 0.1140));

                red[i][j] = r;
                green[i][j] = g;
                blue[i][j] = b;
                gray[i][j] = (double) gr;
            }
        }
        Frame rgbFrame = new Frame(Frame.COLORSPACE_RGB);
        rgbFrame.setSignalIdentifier(labFrame.getSignalIdentifier());
        rgbFrame.setFrameIdentifier(labFrame.getFrameIdentifier());
        rgbFrame.setRedData(red);
        rgbFrame.setGreenData(green);
        rgbFrame.setBlueData(blue);
        rgbFrame.setGrayData(gray);
        return rgbFrame;
    }
}
