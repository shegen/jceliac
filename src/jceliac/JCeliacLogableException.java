/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jceliac;

/**
 * Basic exception for all exceptions used in this application.
 * <p>
 * This class stores the name of the throwing method.
 * <p>
 * @author shegen
 */
public class JCeliacLogableException
        extends JCeliacGenericException
{

    protected String generatingMethod;
    private static final long serialVersionUID = 1L;

    public JCeliacLogableException(String string)
    {
        super(string);
        this.generatingMethod = new Throwable().fillInStackTrace().getStackTrace()[2].getMethodName();
    }

    public JCeliacLogableException()
    {
        super();
        this.generatingMethod = new Throwable().fillInStackTrace().getStackTrace()[2].getMethodName();
    }

    /**
     * Overwrite getMessage() to indicate the throwing method.
     * <p>
     * @return the message string
     */
    @Override
    public String getMessage()
    {
        return super.getMessage() + " (Occured in :-> " + generatingMethod + "())";
    }

}
