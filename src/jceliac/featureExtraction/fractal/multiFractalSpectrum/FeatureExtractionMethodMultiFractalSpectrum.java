/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.fractal.multiFractalSpectrum;

import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.AbstractFeatureExtractionThread;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.featureExtraction.FeatureExtractionThread;
import jceliac.experiment.IThreadedExperiment;
import jceliac.featureExtraction.StableCode;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.tools.filter.DataFilter2D;
import jceliac.tools.arrays.ArrayTools;
import jceliac.JCeliacGenericException;
import jceliac.tools.data.DataReader;
import jceliac.tools.math.PolyFit;
import jceliac.tools.data.Frame;
import java.util.ArrayList;
import java.util.HashMap;
import jceliac.featureExtraction.FixedFeatureVector;
import jceliac.tools.math.MathTools;

/**
 * This implements the multi fractal spectrum methods as discussed in paper
 * "Viewpoint invariant texture description using fractal analysis", the Java
 * code is ported from matlab based on an implementation of Georg Wimmer,
 * gwimmer@cosy.sbg.ac.at.
 * <p>
 * <p>
 * @author shegen
 */
@StableCode(
        comment = "This code is trusted and computes the same feature vectors as gwimmer's matlab code.",
        date = "27.01.2014",
        lastModified = "07.02.2014")
public class FeatureExtractionMethodMultiFractalSpectrum
        extends FeatureExtractionMethod
{

    protected FeatureExtractionParametersMultiFractalSpectrum myParameters;
    protected HashMap<Integer, double[]> gaussFilters = new HashMap<>();
    protected HashMap<Integer, double[][]> circularMasks = new HashMap<>();
    protected final double[][] diagFilter1 = {{0, 0, 0.5}, {0, 0, 0}, {-0.5, 0, 0}};
    protected final double[][] diagFilter2 = {{0.5, 0, 0}, {0, 0, 0}, {0, 0, -0.5}};

    public FeatureExtractionMethodMultiFractalSpectrum(FeatureExtractionParameters param)
    {
        super(param);
        this.myParameters = (FeatureExtractionParametersMultiFractalSpectrum) param;
    }

    @Override
    public void initialize()
            throws JCeliacGenericException
    {
        // compute gaussians used to filter image
        for(int r = 1; r <= 8; r++) {
            double[] gaussFilter = this.computeGaussFilter(r, this.myParameters.getSigma());
            double[][] circularMask = this.computeCircularMask(r);

            this.gaussFilters.put(r, gaussFilter);
            this.circularMasks.put(r, circularMask);
        } 
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {
        MultiFractalSpectrumFeatures features = new MultiFractalSpectrumFeatures(this.myParameters);
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(1);
        if(this.myParameters.isUseColor() == true)
        {
            for(Integer colorChannel : this.myParameters.getColorChannels()) {
                double[] featureData;
                if(this.myParameters.useAllMeasures()) {
                    featureData = this.computeFeaturesAllMeasures(content.getColorChannel(colorChannel));
                } else {
                    featureData = this.computeFeatures(content.getColorChannel(colorChannel));
                }
                features.addAbstractFeature(new FixedFeatureVector(featureData));
            }
        } else {
            double[] featuresGray;
            if(this.myParameters.useAllMeasures()) {
                featuresGray = this.computeFeaturesAllMeasures(content.getGrayData());
            } else {
                featuresGray = this.computeFeatures(content.getGrayData());
            }
            features.addAbstractFeature(new FixedFeatureVector(featuresGray));
        }
        return features;
    }

    
    
    
    
    protected double [] computeFeaturesLaplacianMeasure(double [][] data) 
            throws JCeliacGenericException
    {
        double[][][] laplacianMeasures = new double[this.myParameters.getNumRadii()][][];
        int rlogLen = (int)Math.round(Math.log(Math.max(data.length, data[0].length)) 
                                             / Math.log(2.0d));
        double [] rlog = new double[rlogLen+1];
        for(int i = 1; i <= rlogLen+1; i++) {
            rlog[i - 1] = Math.log(i);
        }
        
        for(int radius = 1; radius <= this.myParameters.getNumRadii(); radius++) {
            double[] gaussFilter = this.gaussFilters.get(radius);
            double[][] circularMask = this.circularMasks.get(radius);
            double[][] dataFiltered = DataFilter2D.convolve(data, gaussFilter, DataFilter2D.BORDER_MIRROR);

            /* Compute responses of measurement function mu using the Laplacian:
             * To be compatible with the matlab implementation we use the second
             * order derivative twice instead of computing the approximation for
             * the second order derivative directly.
             */
            double[][] dx = MathTools.derivativeOrder1(dataFiltered, 1);
            double[][] dxdx = MathTools.derivativeOrder1(dx, 1);

            double[][] dy = MathTools.derivativeOrder1(dataFiltered, 2);
            double[][] dydy = MathTools.derivativeOrder1(dy, 2);

            double[][] laplacian = new double[dataFiltered.length][dataFiltered[0].length];
            for(int i = 0; i < laplacian.length; i++) {
                for(int j = 0; j < laplacian[0].length; j++) {
                    laplacian[i][j] = Math.abs(dxdx[i][j] + dydy[i][j]);
                }
            }
            double[][] laplacianMeasure = DataFilter2D.convolve(laplacian, circularMask, DataFilter2D.BORDER_MIRROR);
            laplacianMeasures[radius - 1] = laplacianMeasure;
        }
        
        double[][] slopePerPixelLaplacian = this.computeSlopePerPixel(laplacianMeasures);
        double[][][] binaryFractalLaplacian = this.computeBinaryMatrix(slopePerPixelLaplacian);
        double[] fractalDimensionsLaplacian = new double[binaryFractalLaplacian.length];

        for(int i = 0; i < fractalDimensionsLaplacian.length; i++) {
            if(ArrayTools.sum(binaryFractalLaplacian[i]) > 0) {
                BoxCountResult result = BoxCount.count(binaryFractalLaplacian[i]);
                double slope = PolyFit.fitSlope(rlog, result.getMinusLogN());
                
                fractalDimensionsLaplacian[i] = slope;
            }
        }
        return fractalDimensionsLaplacian;
    }
    
    
    
    protected double[] computeFeaturesDifferentialMeasure(double[][] data)
            throws JCeliacGenericException
    {
        double[][][] differentialMeasures = new double[this.myParameters.getNumRadii()][][];
        int rlogLen = (int)Math.round(Math.log(Math.max(data.length, data[0].length)) 
                                             / Math.log(2.0d));
        double [] rlog = new double[rlogLen+1];
        for(int i = 1; i <= rlogLen+1; i++) {
            rlog[i - 1] = Math.log(i);
        }
        
        for(int radius = 1; radius <= this.myParameters.getNumRadii(); radius++) {
            double[] gaussFilter = this.gaussFilters.get(radius);
            double[][] circularMask = this.circularMasks.get(radius);
            double[][] dataFiltered = DataFilter2D.convolve(data, gaussFilter, DataFilter2D.BORDER_MIRROR);
            
            /* Compute responses of measurement function mu using the Laplacian:
             * To be compatible with the matlab implementation we use the second
             * order derivative twice instead of computing the approximation for
             * the second order derivative directly.
             */
            double[][] dx = MathTools.derivativeOrder1(dataFiltered, 1);
            double[][] dy = MathTools.derivativeOrder1(dataFiltered, 2);
          
            /*
             * Compute the respones of the measuremeant function mu using the
             * four differential operators.
             */
            double[][] diagFilterResponse1 = DataFilter2D.convolve(dataFiltered, this.diagFilter1, DataFilter2D.BORDER_MIRROR);
            double[][] diagFilterResponse2 = DataFilter2D.convolve(dataFiltered, this.diagFilter2, DataFilter2D.BORDER_MIRROR);
            double[][] diffResponse = new double[dataFiltered.length][dataFiltered[0].length];
            for(int i = 0; i < dx.length; i++) {
                for(int j = 0; j < dx[0].length; j++) {
                    diffResponse[i][j] = Math.sqrt(
                            dx[i][j] * dx[i][j] + dy[i][j] * dy[i][j]
                            + diagFilterResponse1[i][j] * diagFilterResponse1[i][j]
                            + diagFilterResponse2[i][j] * diagFilterResponse2[i][j]
                    );
                }

            }
            double[][] differentialMeasure
                       = DataFilter2D.convolve(diffResponse, circularMask, DataFilter2D.BORDER_MIRROR);
            differentialMeasures[radius - 1] = differentialMeasure;
        }
        double[][] slopePerPixelDifferential = this.computeSlopePerPixel(differentialMeasures);
        double[][][] binaryFractalDifferential = this.computeBinaryMatrix(slopePerPixelDifferential);
        double[] fractalDimensionsDifferential = new double[binaryFractalDifferential.length];
       

        for(int i = 0; i < fractalDimensionsDifferential.length; i++) {
            if(ArrayTools.sum(binaryFractalDifferential[i]) > 0) {
                BoxCountResult result = BoxCount.count(binaryFractalDifferential[i]);
                double slope = PolyFit.fitSlope(rlog, result.getMinusLogN());
                fractalDimensionsDifferential[i] = slope;
            }
        }
        return fractalDimensionsDifferential;
    }

    
    
     protected double[] computeFeaturesNormalMeasure(double[][] data)
            throws JCeliacGenericException
    {

        double[][][] normalMeasures = new double[this.myParameters.getNumRadii()][][];
        int rlogLen = (int)Math.round(Math.log(Math.max(data.length, data[0].length)) 
                                             / Math.log(2.0d));
        double [] rlog = new double[rlogLen+1];
        for(int i = 1; i <= rlogLen+1; i++) {
            rlog[i - 1] = Math.log(i);
        }

        for(int radius = 1; radius <= this.myParameters.getNumRadii(); radius++) {
            double[] gaussFilter = this.gaussFilters.get(radius);
            double[][] circularMask = this.circularMasks.get(radius);
            double[][] dataFiltered = DataFilter2D.convolve(data, gaussFilter, DataFilter2D.BORDER_MIRROR);

            double[][] normalMeasure
                       = DataFilter2D.convolve(dataFiltered, circularMask, DataFilter2D.BORDER_MIRROR);
            normalMeasures[radius - 1] = normalMeasure;

        }

        double[][] slopePerPixelNormal = this.computeSlopePerPixel(normalMeasures);
        double[][][] binaryFractalNormal = this.computeBinaryMatrixNormal(slopePerPixelNormal);
        double[] fractalDimensionsNormal = new double[binaryFractalNormal.length];

        for(int i = 0; i < fractalDimensionsNormal.length; i++) {
            if(ArrayTools.sum(binaryFractalNormal[i]) > 0) {
                BoxCountResult result = BoxCount.count(binaryFractalNormal[i]);
                double slope = PolyFit.fitSlope(rlog, result.getMinusLogN());
                fractalDimensionsNormal[i] = slope;
            }
        }
        return fractalDimensionsNormal;
    }
     
     
    protected double [] computeFeatures(double [][] data)
            throws JCeliacGenericException
    {
        ArrayList <double []> featureData = new ArrayList <>();
        int featureLength = 0;
        
        if(this.myParameters.isUseMeasureLaplacian()) {
            double [] rawFeatures = this.computeFeaturesLaplacianMeasure(data);
            featureData.add(rawFeatures);
            featureLength += rawFeatures.length;
        }
        if(this.myParameters.isUseMeasureDifferential()) {
            double [] rawFeatures = this.computeFeaturesDifferentialMeasure(data);
            featureData.add(rawFeatures);
            featureLength += rawFeatures.length;
        }
        if(this.myParameters.isUseMeasureNormal()) {
            double [] rawFeatures = this.computeFeaturesNormalMeasure(data);
            featureData.add(rawFeatures);
            featureLength += rawFeatures.length;
        }
        double[] combinedFeatures = new double[featureLength];
        
        int pos = 0;
        for(int i = 0; i < featureData.size(); i++) {
            System.arraycopy(featureData.get(i), 0, combinedFeatures, pos, featureData.get(i).length);
            pos += featureData.get(i).length;        
        }
        return combinedFeatures;   
    }
     
     
    /**
     * Performs the actual computation of features for a single color channel.
     * <p>
     * @param data Raw image data.
     * <p>
     * @return Feature vector as double array.
     * <p>
     * @throws JCeliacGenericException
     */
    protected double[] computeFeaturesAllMeasures(double[][] data)
            throws JCeliacGenericException
    {

        double[][][] laplacianMeasures = new double[this.myParameters.getNumRadii()][][];
        double[][][] differentialMeasures = new double[this.myParameters.getNumRadii()][][];
        double[][][] normalMeasures = new double[this.myParameters.getNumRadii()][][];

        int rlogLen = (int)Math.round(Math.log(Math.max(data.length, data[0].length)) / Math.log(2.0d));
        double [] rlog = new double[rlogLen+1];
        for(int i = 1; i <= rlogLen+1; i++) {
            rlog[i - 1] = Math.log(i);
        }
        
        
        for(int radius = 1; radius <= this.myParameters.getNumRadii(); radius++) {
            double[] gaussFilter = this.gaussFilters.get(radius);
            double[][] circularMask = this.circularMasks.get(radius);
            double[][] dataFiltered = DataFilter2D.convolve(data, gaussFilter, DataFilter2D.BORDER_MIRROR);

            /* Compute responses of measurement function mu using the Laplacian:
             * To be compatible with the matlab implementation we use the second
             * order derivative twice instead of computing the approximation for
             * the second order derivative directly.
             */
            double[][] dx = MathTools.derivativeOrder1(dataFiltered, 1);
            double[][] dxdx = MathTools.derivativeOrder1(dx, 1);

            double[][] dy = MathTools.derivativeOrder1(dataFiltered, 2);
            double[][] dydy = MathTools.derivativeOrder1(dy, 2);

            double[][] laplacian = new double[data.length][data[0].length];
            for(int i = 0; i < laplacian.length; i++) {
                for(int j = 0; j < laplacian[0].length; j++) {
                    laplacian[i][j] = Math.abs(dxdx[i][j] + dydy[i][j]);
                }
            }
            double[][] laplacianMeasure = DataFilter2D.convolve(laplacian, circularMask, DataFilter2D.BORDER_MIRROR);
            laplacianMeasures[radius - 1] = laplacianMeasure;

            /*
             * Compute the respones of the measuremeant function mu using the
             * four differential operators.
             */
            double[][] diagFilterResponse1 = DataFilter2D.convolve(dataFiltered, this.diagFilter1, DataFilter2D.BORDER_MIRROR);
            double[][] diagFilterResponse2 = DataFilter2D.convolve(dataFiltered, this.diagFilter2, DataFilter2D.BORDER_MIRROR);
            double[][] diffResponse = new double[data.length][data[0].length];
            for(int i = 0; i < laplacian.length; i++) {
                for(int j = 0; j < laplacian[0].length; j++) {
                    diffResponse[i][j] = Math.sqrt(
                            dx[i][j] * dx[i][j] + dy[i][j] * dy[i][j]
                            + diagFilterResponse1[i][j] * diagFilterResponse1[i][j]
                            + diagFilterResponse2[i][j] * diagFilterResponse2[i][j]
                    );
                }

            }
            double[][] differentialMeasure
                       = DataFilter2D.convolve(diffResponse, circularMask, DataFilter2D.BORDER_MIRROR);
            differentialMeasures[radius - 1] = differentialMeasure;

            double[][] normalMeasure
                       = DataFilter2D.convolve(dataFiltered, circularMask, DataFilter2D.BORDER_MIRROR);
            normalMeasures[radius - 1] = normalMeasure;

        }
        double[][] slopePerPixelLaplacian = this.computeSlopePerPixel(laplacianMeasures);
        double[][] slopePerPixelDifferential = this.computeSlopePerPixel(differentialMeasures);
        double[][] slopePerPixelNormal = this.computeSlopePerPixel(normalMeasures);
        // ok up to here

        double[][][] binaryFractalLaplacian = this.computeBinaryMatrix(slopePerPixelLaplacian);
        double[][][] binaryFractalDifferential = this.computeBinaryMatrix(slopePerPixelDifferential);
        double[][][] binaryFractalNormal = this.computeBinaryMatrixNormal(slopePerPixelNormal);

        double[] fractalDimensionsLaplace = new double[binaryFractalLaplacian.length];
        double[] fractalDimensionsDifferential = new double[binaryFractalDifferential.length];
        double[] fractalDimensionsNormal = new double[binaryFractalNormal.length];

        for(int i = 0; i < fractalDimensionsLaplace.length; i++) {
            if(ArrayTools.sum(binaryFractalLaplacian[i]) > 0) {
                BoxCountResult result = BoxCount.count(binaryFractalLaplacian[i]);
                double slope = PolyFit.fitSlope(rlog, result.getMinusLogN());
                
                fractalDimensionsLaplace[i] = slope;
            }
        }
        for(int i = 0; i < fractalDimensionsDifferential.length; i++) {
            if(ArrayTools.sum(binaryFractalDifferential[i]) > 0) {
                BoxCountResult result = BoxCount.count(binaryFractalDifferential[i]);
                double slope = PolyFit.fitSlope(rlog, result.getMinusLogN());
                fractalDimensionsDifferential[i] = slope;
            }
        }
        for(int i = 0; i < fractalDimensionsNormal.length; i++) {
            double s = ArrayTools.sum(binaryFractalNormal[i]);
            if(ArrayTools.sum(binaryFractalNormal[i]) > 0) {
                BoxCountResult result = BoxCount.count(binaryFractalNormal[i]);
                double slope = PolyFit.fitSlope(rlog, result.getMinusLogN());
                fractalDimensionsNormal[i] = slope;
            }
        }
        double[] combinedFeatures = new double[fractalDimensionsLaplace.length
                                               + fractalDimensionsDifferential.length
                                               + fractalDimensionsNormal.length];
        System.arraycopy(fractalDimensionsLaplace, 0, combinedFeatures, 0, fractalDimensionsLaplace.length);
        System.arraycopy(fractalDimensionsDifferential, 0, combinedFeatures, fractalDimensionsLaplace.length, fractalDimensionsDifferential.length);
        System.arraycopy(fractalDimensionsNormal, 0, combinedFeatures, fractalDimensionsLaplace.length + fractalDimensionsDifferential.length, fractalDimensionsNormal.length);
        return combinedFeatures;
    }

    /**
     * Computes the binary image from local slopes to compute global fractal
     * dimension with box counting.
     * <p>
     * @param slopePerPixel Slope of fractal response by pixel.
     * <p>
     * @return Thresholded binary images.
     */
    private double[][][] computeBinaryMatrix(double[][] slopePerPixel)
    {
        double[][][] binarySlopes = new double[this.myParameters.getSteps()][][];
        double stepsize = this.myParameters.getStepmax() / (this.myParameters.getSteps() - 1);

        int index = 0;
        for(double i = 0; i <= this.myParameters.getStepmax(); i += stepsize, index++) {
            binarySlopes[index] = new double[slopePerPixel.length][slopePerPixel[0].length];

            for(int x = 0; x < slopePerPixel.length; x++) {
                for(int y = 0; y < slopePerPixel[0].length; y++) {
                    if(i <= slopePerPixel[x][y] && slopePerPixel[x][y] < (i + stepsize)) {
                        binarySlopes[index][x][y] = 1;
                    }
                }
            }
        }
        return binarySlopes;
    }

    /**
     * Georgs uses a slightly different thresholding for the normal response.
     * <p>
     * @param slopePerPixel Slope of fractal response by pixel.
     * <p>
     * @return Thresholded binary images.
     */
    private double[][][] computeBinaryMatrixNormal(double[][] slopePerPixel)
    {
        double[][][] binarySlopes = new double[this.myParameters.getSteps()][][];
        double stepsize = this.myParameters.getStepmax() / (this.myParameters.getSteps() - 1);
        double stepmax = this.myParameters.getStepmax();
        int index = 0;
        for(double i = 0; i <= this.myParameters.getStepmax(); i += stepsize, index++) {
            binarySlopes[index] = new double[slopePerPixel.length][slopePerPixel[0].length];

            for(int x = 0; x < slopePerPixel.length; x++) {
                for(int y = 0; y < slopePerPixel[0].length; y++) {
                    if((i + stepmax / 2.0d) <= slopePerPixel[x][y]
                       && slopePerPixel[x][y] < (i + stepsize + stepmax / 2.0d)) {
                        binarySlopes[index][x][y] = 1;
                    }
                }
            }
        }
        return binarySlopes;
    }

    /**
     * Computes the slope per pixel by linear fitSlopeting, this is the local fractal
 dimension.
     * <p>
     * @param measureResponses Responses of a specific mu-function.
     * <p>
     * @return Slopes per pixel.
     * <p>
     * @throws JCeliacGenericException
     */
    protected double[][] computeSlopePerPixel(double[][][] measureResponses)
            throws JCeliacGenericException
    {
        double[][] slopePerPixel = new double[measureResponses[0].length][measureResponses[0][0].length];
        int rlogLen = measureResponses.length;
        double [] rlog = new double[rlogLen];
        for(int i = 1; i <= rlogLen; i++) {
            rlog[i - 1] = Math.log(i);
        } 
        
        // prepare data by computing log
        for(int i = 0; i < measureResponses[0].length; i++) {
            for(int j = 0; j < measureResponses[0][0].length; j++) {

                double[] values = new double[measureResponses.length];
                for(int r = 0; r < measureResponses.length; r++) {
                    double tmp = measureResponses[r][i][j];
                    double logValue = tmp > 0 ? Math.log(tmp) : 0;
                    values[r] = logValue;
                }
                double slope = PolyFit.fitSlope(rlog, values);
                slopePerPixel[i][j] = slope;
            }
        }
        return slopePerPixel;
    }

    /**
     * Computes a mask of circular shaped used to compute the fractal
     * dimensions.
     * <p>
     * @param r Radius.
     * @return Circluar filter mask.
     */
    protected double[][] computeCircularMask(int r)
    {
        double[][] rmask = new double[2 * r - 1][2 * r - 1];
        for(int i = 1; i <= 2 * r - 1; i++) {
            for(int j = 0; j <= 2 * r - 1; j++) {
                if(Math.sqrt((i - r) * (i - r) + (j - r) * (j - r)) <= r - 1) {
                    rmask[i - 1][j - 1] = 1.0d;
                }
            }
        }
        return rmask;
    }

    /**
     * Computes the gauss filter with r used as variance and sigma used as fixed
     * parameter.
     * <p>
     * @param r     Radius used to compute gauss filter, also used as standard
     *              deviation of Gaussian.
     * @param sigma Parameter used to scale variance.
     * <p>
     * @return Separable Gaussian filter.
     */
    protected double[] computeGaussFilter(int r, double sigma)
    {
        int dimension = 2 * r - 1;
        double center = (((double) (dimension - 1)) / 2.0d);
        double[] kernel = new double[dimension];
        double sum = 0;

        for(double x = -center; x <= center; x++) {
            /* Please note that in "Viewpoint invariant texture description
             * using fractal analysis" the authors explicitly compute Gr using
             * r^2 to multiply the variance. This is intended.
             */
            kernel[(int) (x + center)] = Math.exp(-(x * x) / (2 * sigma * sigma * r * r));
            sum += kernel[(int) (x + center)];

        }
        // normalize to 1
        for(double x = -center; x <= center; x++) {
            kernel[(int) (x + center)] = (kernel[(int) (x + center)] / sum);
        }
        return kernel;
    }

    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass) throws
            JCeliacGenericException
    {

    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {

    }

    @Override
    public AbstractFeatureExtractionThread createFeatureExtractionThread(IThreadedExperiment exp, Frame content, int classIndex, boolean isOneClassTrainingClass, boolean isTraining)
    {
        return new FeatureExtractionThread(exp, this, content, classIndex, isOneClassTrainingClass, isTraining);
    }

}
