/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.fractal.multiFractalSpectrum;

/**
 *
 * @author shegen
 */
public class BoxCountResult
{

    public int[] n;
    public int[] r;

    public BoxCountResult(int[] n, int[] r)
    {
        this.n = n;
        this.r = r;
    }

    public double [] getMinusLogN()
    {
        double [] logN = new double[this.n.length];
        for(int i = 0; i < this.n.length; i++) {
            if(this.n[i] == 0) {
                logN[i] = 0;
            }else {
                logN[i] = -Math.log(this.n[i]);
            }
        }
        return logN;
    }
    
}
