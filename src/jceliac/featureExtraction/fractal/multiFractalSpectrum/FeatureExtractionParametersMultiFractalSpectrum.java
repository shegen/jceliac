/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.fractal.multiFractalSpectrum;

import jceliac.experiment.Experiment;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersMultiFractalSpectrum 
    extends FeatureExtractionParameters
{

    public boolean useMeasureLaplacian = true;
    public boolean useMeasureDifferential = true;
    public boolean useMeasureNormal = true;
    
    protected double sigma = 2d;
    protected int numRadii = 8;
    protected double stepmax = 2.42;
    protected int steps = 26;

    public FeatureExtractionParametersMultiFractalSpectrum()
    {
    }
    
    @Override
    public void report()
    {
        super.report();
        Experiment.log(JCeliacLogger.LOG_INFO, "Laplacian Measure: " + this.useMeasureLaplacian);
        Experiment.log(JCeliacLogger.LOG_INFO, "Differential Measure: " + this.useMeasureDifferential);
        Experiment.log(JCeliacLogger.LOG_INFO, "Normal Measure: " + this.useMeasureNormal);
        
        Experiment.log(JCeliacLogger.LOG_INFO, "Sigma: " + this.sigma);
        Experiment.log(JCeliacLogger.LOG_INFO, "Number of Radii: " + this.numRadii);
        Experiment.log(JCeliacLogger.LOG_INFO, "Stepmax: " + this.stepmax);
        Experiment.log(JCeliacLogger.LOG_INFO, "Steps: " + this.steps);   
    }

    @Override
    public String getMethodName()
    {
        return "Multi Fractal Spectrum";
    }

    public double getSigma()
    {
        return this.sigma;
    }

    public int getNumRadii()
    {
        return this.numRadii;
    }

    public double getStepmax()
    {
        return this.stepmax;
    }

    public int getSteps()
    {
        return steps;
    }

    public boolean isUseMeasureLaplacian()
    {
        return useMeasureLaplacian;
    }

    public boolean isUseMeasureDifferential()
    {
        return useMeasureDifferential;
    }

    public boolean isUseMeasureNormal()
    {
        return useMeasureNormal;
    }

    public boolean useAllMeasures()
    {
        return this.useMeasureLaplacian && this.useMeasureDifferential && this.useMeasureNormal;
    }

    
    
    
    
    
    
    
}
