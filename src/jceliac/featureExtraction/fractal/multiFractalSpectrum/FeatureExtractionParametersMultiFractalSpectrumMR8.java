/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.fractal.multiFractalSpectrum;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersMultiFractalSpectrumMR8
    extends FeatureExtractionParametersMultiFractalSpectrum

{
    // in the paper only 5 filter responses are used, it is not clear why
    // this seems overfitted; we hence give the option to use the entire mr8 response
    protected boolean useEntireMR8Response = false;

    public FeatureExtractionParametersMultiFractalSpectrumMR8()
    {
        this.useMeasureDifferential = false;
        this.useMeasureLaplacian = false;
    }
    
    
    @Override
    public void report()
    {
        super.report();
    }

    @Override
    public String getMethodName()
    {
        return "Multi Fractal Spectrum MR8";
    }

    @Override
    public boolean isUseMeasureLaplacian()
    {
        return false;
    }
    
    @Override
    public boolean isUseMeasureDifferential()
    {
        return false;
    }
    
    @Override
    public boolean isUseMeasureNormal()
    {
        return true;
    }
    
    @Override
    public boolean useAllMeasures()
    {
        return false;
    }

    public boolean isUseEntireMR8Response()
    {
        return this.useEntireMR8Response;
    }
}
