/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.featureExtraction.fractal.multiFractalSpectrum;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.classification.kernelSVM.SVMFeatureVector;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;

/**
 *
 * @author shegen
 */
public class MultiFractalSpectrumMR8Features
    extends DiscriminativeFeatures
{
    private ArrayList <double [][]> localRawFeatureData;
    
    public MultiFractalSpectrumMR8Features(FeatureExtractionParameters parameters)
    {
        super(parameters);
    }

    public MultiFractalSpectrumMR8Features()
    {
        super();
    }

    @Override
    public DiscriminativeFeatures cloneFeatureSubset(FeatureVectorSubsetEncoding subset)
            throws JCeliacGenericException
    {
        MultiFractalSpectrumMR8Features featureSubset = new MultiFractalSpectrumMR8Features(super.featureExtractionParameters);
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
        featureSubset.localRawFeatureData = this.localRawFeatureData;
        
        return featureSubset;
    }

    @Override
    public DiscriminativeFeatures cloneEmptyFeature()
    {
        MultiFractalSpectrumMR8Features featureSubset = new MultiFractalSpectrumMR8Features(super.featureExtractionParameters);
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
        featureSubset.localRawFeatureData = this.localRawFeatureData;
        
        return featureSubset;
    }
 
    @Override
    public int getFeatureVectorDimensionality()
    {
        return 1;
    }

    @Override
    public double doDistanceTo(DiscriminativeFeatures them)
            throws JCeliacGenericException
    {
        throw new JCeliacGenericException("Feature is not supposed to be used before generative model mapping!");
    }

    public synchronized void setLocalFeatureVectorList(ArrayList <double [][]> localRawFeatureData)
    {
         this.localRawFeatureData = localRawFeatureData;
    }

    @Override
    public synchronized ArrayList<double []> getLocalFeatureVectorData()
    {
        return this.createFeatureVectorsFromRawData(this.localRawFeatureData);
    }
    
    private ArrayList <double []> createFeatureVectorsFromRawData(ArrayList <double [][]> rawFeatureList)
    {
        ArrayList <double []> lVec = new ArrayList <>();
        
        for(int listIndex = 0; listIndex < rawFeatureList.size(); listIndex++) 
        {
            double [][] rawFeatures = rawFeatureList.get(listIndex);
            for(int featureIndex = 0; featureIndex < rawFeatures[0].length; featureIndex++) 
            {
                double [] featureData = new double[rawFeatures.length];
                for(int filterIndex = 0; filterIndex < featureData.length; filterIndex++) {
                    featureData[filterIndex] = rawFeatures[filterIndex][featureIndex];
                }
                lVec.add(featureData);
            }
        }
        return lVec;
    }
}
