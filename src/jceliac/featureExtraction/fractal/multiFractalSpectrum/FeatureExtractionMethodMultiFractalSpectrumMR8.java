/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.featureExtraction.fractal.multiFractalSpectrum;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.Frame;
import jceliac.tools.filter.DataFilter2D;
import jceliac.tools.filter.MR8Filter;

/**
 * Implements the feature presented in "Locally Invariant Fractal Features for 
 * Statistical Texture Classification" based on matlab code by gwimmer. 
 * This implements only the fractal dimension feature (D), the fractal length feature (L)
 * is not implemented as it is not bi-Lipschitz invariant. 
 * 
 * @author shegen
 */
public class FeatureExtractionMethodMultiFractalSpectrumMR8
    extends FeatureExtractionMethodMultiFractalSpectrum
{

    protected MR8Filter mr8Filter;
    protected final FeatureExtractionParametersMultiFractalSpectrumMR8 myParameters;
    
    public FeatureExtractionMethodMultiFractalSpectrumMR8(FeatureExtractionParameters param)
    {
        super(param);
        this.myParameters = (FeatureExtractionParametersMultiFractalSpectrumMR8) param;
    }
    
    
   @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {     
        MultiFractalSpectrumMR8Features features = new MultiFractalSpectrumMR8Features(this.myParameters);
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(1);
        if(this.myParameters.isUseColor() == true) 
        {
            ArrayList <double [][]> combinedFeatures = new ArrayList <>();
            for(Integer colorChannel : this.myParameters.getColorChannels()) {
                double [][] featureData = this.computeLocalFeatures(content.getColorChannel(colorChannel));
                combinedFeatures.add(featureData);
            }
            features.setLocalFeatureVectorList(combinedFeatures);
        } else {
            ArrayList <double [][]> localFeatures = new ArrayList <>();
            double [][] featuresGray = this.computeLocalFeatures(content.getGrayData());
            localFeatures.add(featuresGray);
            features.setLocalFeatureVectorList(localFeatures);
        }
        return features;
    }

    protected double [][] computeLocalFeatures(double [][] data)
            throws JCeliacGenericException
    {
        /* Currently only the normal measure is implemented, but 
         * I probably implement the SOBEL measure of gwimmer later, hence
         * use this layer of abstraction.
         */
        if(this.myParameters.isUseMeasureNormal()) {
            double [][] rawFeatures = this.computeMR8FeaturesNormalMeasure(data);
            return rawFeatures;
        }else {
            throw new JCeliacGenericException("Measure type not supported!");
        }
    }
    

    
    protected double [][] computeMR8FeaturesNormalMeasure(double[][] data)
            throws JCeliacGenericException
    {
        double[][][] normalMeasures = new double[this.myParameters.getNumRadii()][][];
        int rlogLen = (int)Math.round(Math.log(Math.max(data.length, data[0].length)) 
                                             / Math.log(2.0d));
        double [] rlog = new double[rlogLen+1];
        for(int i = 1; i <= rlogLen+1; i++) {
            rlog[i - 1] = Math.log(i);
        }
        
        double [][][] mr8Response = this.mr8Filter.computeMR8FilterResponse2D(data);
        /* The authors of the paper use the absolute values of the maximum
         * filter responses to compute the mu-measure, we hence take the
         * absolute of the mr8 response data. Also no gaussian filter is applied
         * in the paper, this is in contrast to the MFS feature.
         */
        for(int i = 0; i < mr8Response.length; i++) {
            mr8Response[i] = ArrayTools.abs(mr8Response[i]);
        }
        double [][] slopesPerPixelNormalByFilter;
        if(this.myParameters.useEntireMR8Response) {
            slopesPerPixelNormalByFilter = new double[mr8Response.length][];
            for(int responseIndex = 0; responseIndex < mr8Response.length; responseIndex++) {
                for(int radius = 1; radius <= this.myParameters.getNumRadii(); radius++) {
                    double[][] circularMask = super.circularMasks.get(radius);
                    double[][] normalMeasure
                               = DataFilter2D.convolve(mr8Response[responseIndex], circularMask, DataFilter2D.BORDER_MIRROR);
                    normalMeasures[radius - 1] = normalMeasure;
                }
            double[][] slopePerPixelNormal = this.computeSlopePerPixel(normalMeasures);
            slopesPerPixelNormalByFilter[responseIndex] = ArrayTools.reshape(slopePerPixelNormal);
            }
            
        }else {
            // the 5 MR8 responses used in the original paper
            int [] responseIndices = {1,2,3,5,7};
            slopesPerPixelNormalByFilter = new double[responseIndices.length][]; 
            int slopesIndex = 0;
            for(int index = 0; index < responseIndices.length; index++) {
                int responseIndex = responseIndices[index];
                for(int radius = 1; radius <= this.myParameters.getNumRadii(); radius++) {
                    double[][] circularMask = super.circularMasks.get(radius);
                    double[][] normalMeasure
                               = DataFilter2D.convolve(mr8Response[responseIndex], circularMask, DataFilter2D.BORDER_MIRROR);
                    normalMeasures[radius - 1] = normalMeasure;
                }
                double[][] slopePerPixelNormal = this.computeSlopePerPixel(normalMeasures);
                slopesPerPixelNormalByFilter[slopesIndex] = ArrayTools.reshape(slopePerPixelNormal);
                slopesIndex++;
            }
        }
        return slopesPerPixelNormalByFilter;
    }

    @Override
    public void initialize() throws JCeliacGenericException
    {
        super.initialize();
        this.mr8Filter = new MR8Filter();
    } 
}
