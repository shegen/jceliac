/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.fractal.multiFractalSpectrum;

import org.apache.commons.lang3.ArrayUtils;
import jceliac.tools.arrays.ArrayTools;


/**
 * Implements the box count method, based on a matlab implementation of F Moisy.
 * This is used in the fractal dimension feature extraction methods.
 * <p>
 * <p>
 * @author shegen
 */
public class BoxCount
{

    /**
     * Converts the data to an array containing 1 and 0.
     * <p>
     * @param data1D Data used to create logical.
     * <p>
     * @return Array of 1 and 0, 1 if value at index was > 0, 0 else.
     */
    private static int[] createLogical(double[] data1D)
    {
        int[] logical = new int[data1D.length];
        for(int i = 0; i < data1D.length; i++) {
            if(data1D[i] > 0) {
                logical[i] = 1;
            }
        }
        return logical;
    }

    /**
     * Converts the data to an array containing 1 and 0.
     * <p>
     * @param data2D Data used to create logical.
     * <p>
     * @return Array of 1 and 0, 1 if value at index was > 0, 0 else.
     */
    private static int[][] createLogical(double[][] data2D)
    {
        int[][] logical = new int[data2D.length][data2D[0].length];
        for(int i = 0; i < data2D.length; i++) {
            for(int j = 0; j < data2D[0].length; j++) {
                if(data2D[i][j] > 0) {
                    logical[i][j] = 1;
                }
            }
        }
        return logical;
    }

    /**
     * Performs the box count of a 1D array.
     * <p>
     * @param data1D One dimensional array.
     * <p>
     * @return Box count result.
     */
    public static BoxCountResult count(double[] data1D)
    {

        int[] c = BoxCount.createLogical(data1D);
        int width = c.length; // largest size of the box
        double p = Math.log(width) / Math.log(2.0d);  // number of generations

        /*
         * Remap the array if the dimensions are not all equal or if the are not
         * a power of two.
         */
        if(p != Math.floor(p)) {
            p = Math.ceil(p);
            width = (int) Math.pow(2, p);
            int[] mz = new int[width];
            System.arraycopy(c, 0, mz, 0, c.length);
            c = mz;
        }
        int[] n = new int[(int) p + 1];

        /*
         * Performs the 1D-Boxcount.
         */
        n[(int) p] = (int) ArrayTools.sum(c);
        for(int g = (int) p - 1; g >= 0; g--) {
            double siz = Math.pow(2.0d, p - g);
            int siz2 = (int) Math.round(siz / 2.0d);
            for(int i = 1; i <= width - siz + 1; i += siz) {
                if(c[i - 1] > 0 || c[i + siz2 - 1] > 0) {
                    c[i - 1] = 1;
                } else {
                    c[i - 1] = 0;
                }
            }
            int csum = 0;
            for(int i = 1; i <= width - siz + 1; i += siz) {
                csum += c[i - 1];
            }
            n[g] = csum;
        }
        /*
         * Reverse n for matlab compliance.
         */
        ArrayUtils.reverse(n);
        int[] r = new int[(int) p + 1];
        for(int i = 0; i <= p; i++) {
            r[i] = (int) Math.pow(2.0d, i);
        }
        return new BoxCountResult(n, r);
    }

    /**
     * Performs the box count of a 2D array.
     * <p>
     * @param data2D One dimensional array.
     * <p>
     * @return Box count result.
     */
    public static BoxCountResult count(double[][] data2D)
    {

        int[][] c = BoxCount.createLogical(data2D);
        int width = Math.max(c.length, c[0].length); // largest size of the box
        double p = Math.log(width) / Math.log(2.0d);  // number of generations

        /*
         * Remap the array if the dimensions are not all equal or if the are not
         * a power of two.
         */
        if(p != Math.floor(p)) {
            p = Math.ceil(p);
            width = (int) Math.pow(2, p);
            int[][] mz = new int[width][width];
            for(int i = 0; i < c.length; i++) {
                System.arraycopy(c[i], 0, mz[i], 0, c[0].length);
            }
            c = mz;
        }
        int[] n = new int[(int) p + 1];

        /*
         * Performs the 2D-Boxcount.
         */
        n[(int) p] = (int) ArrayTools.sum(c);
        for(int g = (int) p - 1; g >= 0; g--) {
            double siz = Math.pow(2.0d, p - g);
            int siz2 = (int) Math.round(siz / 2.0d);
            for(int i = 1; i <= width - siz + 1; i += siz) {
                for(int j = 1; j <= width - siz + 1; j += siz) {
                
                    if(c[i - 1][j - 1] > 0 || 
                       c[i + siz2 - 1][j - 1] > 0 || 
                       c[i - 1][j + siz2 - 1] > 0 || 
                       c[i + siz2 - 1][j + siz2 - 1] > 0) 
                    {
                        c[i - 1][j - 1] = 1;
                    } else {
                        c[i - 1][j - 1] = 0;
                    }
                }
            }
            int csum = 0;
            for(int i = 1; i <= width - siz + 1; i += siz) {
                for(int j = 1; j <= width - siz + 1; j += siz) {
                    csum += c[i - 1][j - 1];
                }
            }
            n[g] = csum;
        }
        /*
         * Reverse n for matlab compliance.
         */
        ArrayUtils.reverse(n);
        int[] r = new int[(int) p + 1];
        for(int i = 0; i <= p; i++) {
            r[i] = (int) Math.pow(2.0d, i);
        }
        return new BoxCountResult(n, r);
    }

}
