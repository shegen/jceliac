/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction;

import java.io.Serializable;
import jceliac.experiment.*;
import jceliac.logging.JCeliacLogger;

/**
 * Base class for all feature extraction parameters.
 * <p>
 * @author shegen
 */
public abstract class FeatureExtractionParameters
        extends AbstractParameters
        implements Cloneable, Serializable
{
    protected boolean useColor = false;
    protected int [] colorChannels = new int []{1,2,3};
    
    public boolean isUseColor()
    {
        return useColor;
    }

    public int[] getColorChannels()
    {
        return colorChannels;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }

    public void report() 
    {
        Experiment.log(JCeliacLogger.LOG_INFO, "Feature Extraction Parameters: ");
        Experiment.log(JCeliacLogger.LOG_INFO, "Feature Extraction Method: %s", this.getMethodName());
        Experiment.log(JCeliacLogger.LOG_INFO, "Color: %s", this.useColor ? "true" : "false");
    }

    public abstract String getMethodName();
}
