/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.featureExtraction.dominantScale;

import jceliac.experiment.Experiment;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersDominantScale
    extends FeatureExtractionParameters
{
    // default values following the paper is 2, we use 3
    protected int numberOfScales = 2;
    // the decomposition supports 1,2,4 and 6 orientations, the paper wants 3 this seems stupid
    protected int numberOfOrientations = 4;  
    
    // The paper uses the energy sum of all coefficients to determine dominant scale,
    // but it's not clear if this is reasonable due to different numbers of subband-coefficient 
    // across scales. If this is set to true, the mean of the energy will be used instead
    protected boolean useEnergyMean = false; 

    @Override
    public String getMethodName()
    {
        return "Dominant Scale";
    }

    @Override
    public void report()
    {
        super.report(); 
        Experiment.log(JCeliacLogger.LOG_INFO, "Number of Scales: %d", this.numberOfScales);
        Experiment.log(JCeliacLogger.LOG_INFO, "Number of Orientations: %d", this.numberOfOrientations);
    }
}
