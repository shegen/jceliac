/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */
package jceliac.featureExtraction.dominantScale;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.FixedFeatureVector;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.StandardEuclideanFeatures;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.Frame;
import jceliac.tools.math.MathTools;
import jceliac.tools.transforms.steerablePyramid.SteerablePyramidDecomposition;
import jceliac.tools.transforms.steerablePyramid.SteerablePyramidTransform;

/**
 * Implements the features described in "Rotation-Invariant and Scale-Invariant
 * Steerable Pyramid Decomposition for Texture Image Retrieval" based on the
 * matlab code of gwimmer.
 * <p>
 * @author shegen
 */
public class FeatureExtractionMethodDominantScale
        extends FeatureExtractionMethod
{

    protected FeatureExtractionParametersDominantScale myParameters;

    public FeatureExtractionMethodDominantScale(FeatureExtractionParameters param)
    {
        super(param);
        this.myParameters = (FeatureExtractionParametersDominantScale) param;
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {
        StandardEuclideanFeatures features = new StandardEuclideanFeatures();

        if(this.myParameters.isUseColor()) {
            for(Integer colorChannel : this.myParameters.getColorChannels()) {
                FixedFeatureVector featureData = this.computeFeatures(content.getColorChannel(colorChannel));
                features.addAbstractFeature(featureData);
            }
        } else {
            FixedFeatureVector featureDataGray = this.computeFeatures(content.getGrayData());
            features.addAbstractFeature(featureDataGray);
        }
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(1);
        return features;
    }

    public FixedFeatureVector computeFeatures(double[][] data)
            throws JCeliacGenericException
    {
        int minScale = this.computeMinimumScale(data);
        SteerablePyramidTransform transform = new SteerablePyramidTransform();
        transform.transformForward(data, minScale, this.myParameters.numberOfOrientations);
        SteerablePyramidDecomposition decomposition = transform.getDecomposition();

        double[][] featureVectorMean = new double[this.myParameters.numberOfScales][this.myParameters.numberOfOrientations];
        double[][] featureVectorStd = new double[this.myParameters.numberOfScales][this.myParameters.numberOfOrientations];

        for(int scaleIndex = 0; scaleIndex < this.myParameters.numberOfScales; scaleIndex++) {
            for(int orientationIndex = 0; orientationIndex < this.myParameters.numberOfOrientations;
                orientationIndex++) {
                featureVectorMean[scaleIndex][orientationIndex] = MathTools.absMean(decomposition.getBandpassSubband(scaleIndex, orientationIndex));
                featureVectorStd[scaleIndex][orientationIndex] = MathTools.absStdev(decomposition.getBandpassSubband(scaleIndex, orientationIndex));
            }
        }
        // reorder feature vectors by dominant scale and orientation
        int[] dominantScaleAndOrientation = this.computeDominantScaleAndOrientation(decomposition);
        double[][] dominantFeatureVectorMean = new double[this.myParameters.numberOfScales][this.myParameters.numberOfOrientations];
        double[][] dominantFeatureVectorStd = new double[this.myParameters.numberOfScales][this.myParameters.numberOfOrientations];

        for(int i = 0; i < featureVectorMean.length; i++) {
            for(int j = 0; j < featureVectorMean[0].length; j++) {

                int scaleIndex = (dominantScaleAndOrientation[0] + i) % this.myParameters.numberOfScales;
                int orientationIndex = (dominantScaleAndOrientation[1] + j) % this.myParameters.numberOfOrientations;

                dominantFeatureVectorMean[i][j] = featureVectorMean[scaleIndex][orientationIndex];
                dominantFeatureVectorStd[i][j] = featureVectorStd[scaleIndex][orientationIndex];
            }
        }
        double[] meanVec = reshapeFeatures(dominantFeatureVectorMean);
        double[] stdVec = reshapeFeatures(dominantFeatureVectorStd);
        double[] featureData = new double[meanVec.length + stdVec.length];
        System.arraycopy(meanVec, 0, featureData, 0, meanVec.length);
        System.arraycopy(stdVec, 0, featureData, meanVec.length, stdVec.length);

        return new FixedFeatureVector(featureData);
    }

    private double[] reshapeFeatures(double[][] data)
    {
        double[] reshapedData = new double[data.length * data[0].length];
        int index = 0;
        for(int j = 0; j < data[0].length; j++) {
            for(int i = 0; i < data.length; i++) {
                reshapedData[index] = data[i][j];
                index++;
            }
        }
        return reshapedData;
    }

    private int computeNumberOfPossibleScales(double[][] data)
    {
        int minDim = Math.min(data.length, data[0].length);
        double h = Math.log(minDim) / Math.log(2.0d);
        return (int) h;
    }

    private int computeMinimumScale(double[][] data)
    {
        int numScales = this.computeNumberOfPossibleScales(data);
        return (numScales - this.myParameters.numberOfScales);
    }

    /**
     * Computes the dominant scales and orientations.
     * <p>
     * @param decomposition index 0: dominant scale, index 1: dominant
     *                      orientation
     * <p>
     * @return
     */
    private int[] computeDominantScaleAndOrientation(SteerablePyramidDecomposition decomposition)
    {
        double[] energyAcrossScales = new double[this.myParameters.numberOfScales];
        double[] energyAcrossOrientations = new double[this.myParameters.numberOfOrientations];

        for(int orientationIndex = 0; orientationIndex < this.myParameters.numberOfOrientations; orientationIndex++) {
            for(int scaleIndex = 0; scaleIndex < this.myParameters.numberOfScales; scaleIndex++) {
                double[][] tmpResponse = decomposition.getBandpassSubband(scaleIndex, orientationIndex);
                double tmpEnergy = this.computeEnergy(tmpResponse);

                energyAcrossScales[scaleIndex] += tmpEnergy;
                energyAcrossOrientations[orientationIndex] += tmpEnergy;
            }
        }
        // find maximum scale
        int dominantScale = ArrayTools.maxIndex(energyAcrossScales);
        int dominantOrientation = ArrayTools.maxIndex(energyAcrossOrientations);
        return new int[]{dominantScale, dominantOrientation};
    }

    private double computeEnergy(double[][] subbandData)
    {
        double energy = 0;

        for(int i = 0; i < subbandData.length; i++) {
            for(int j = 0; j < subbandData[0].length; j++) {
                energy += Math.abs(subbandData[i][j]);
            }
        }
        if(this.myParameters.useEnergyMean) {
            energy = energy / (subbandData.length * subbandData[0].length);
        }
        return energy;
    }

    // <editor-fold defaultstate="collapsed" desc="Unused initializers and parameter estimator methods.">
    @Override
    public void initialize() throws JCeliacGenericException
    {
        // nothing to do
    }

    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass)
            throws JCeliacGenericException
    {
        // nothing to do
    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {
        // nothing to do
    }
    // </editor-fold>
}
