/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.temporalFeatures;

import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.featureExtraction.AbstractFeatureExtractionThread;
import jceliac.experiment.IThreadedExperiment;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.JCeliacGenericException;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.Frame;
import java.util.ArrayList;
import jceliac.featureExtraction.FixedFeatureVector;
import jceliac.featureExtraction.StableCode;
import jceliac.tools.math.*;

/**
 *
 * @author shegen
 */
@StableCode(comment = "code is trusted",
            date =  "20.01.2014",
            lastModified = "20.01.2014")
public class FeatureExtractionMethodTemporalStatistics
        extends FeatureExtractionMethod
{

    protected FeatureExtractionParametersTemporalStatistics myParameters;

    public FeatureExtractionMethodTemporalStatistics(FeatureExtractionParameters param)
    {
        super(param);
        this.myParameters = (FeatureExtractionParametersTemporalStatistics) param;
    }

    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass)
    {
        // does nothing
    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {
        // does nothing
    }

    @Override
    public void initialize()
    {
        // does nothing
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content) throws
            JCeliacGenericException
    {
        TemporalStatisticFeatures features = new TemporalStatisticFeatures(this.myParameters);

        if(myParameters.isUseColor()) {
            double[][] blue = content.getBlueData();
            double[][] red = content.getRedData();
            double[][] green = content.getGreenData();

            double[] stdBlue = new double[1];
            double[] stdRed = new double[1];
            double[] stdGreen = new double[1];

            stdBlue[0] = MathTools.stdev(blue);
            stdRed[0] = MathTools.stdev(red);
            stdGreen[0] = MathTools.stdev(green);

            features.addAbstractFeature(new FixedFeatureVector(stdBlue));
            features.addAbstractFeature(new FixedFeatureVector(stdRed));
            features.addAbstractFeature(new FixedFeatureVector(stdGreen));
        } else {
            double[][] gray = content.getGrayData();
            double[] stdGray = new double[1];

            stdGray[0] = MathTools.stdev(gray);
            features.addAbstractFeature(new FixedFeatureVector(stdGray));
        }
        features.setSignalIdentifier(content.getSignalIdentifier());
        return features;
    }

    @Override
    public AbstractFeatureExtractionThread createFeatureExtractionThread(IThreadedExperiment exp,
                                                                 Frame content, int classNumber,
                                                                 boolean isOneClassTrainingClass, boolean isTraining)
    {
        FeatureExtractionThreadTemporalStatistics thread
                                                  = new FeatureExtractionThreadTemporalStatistics(exp, this, content,
                                                                                                  classNumber, isOneClassTrainingClass,
                                                                                                  isTraining);
        return thread;
    }

}
