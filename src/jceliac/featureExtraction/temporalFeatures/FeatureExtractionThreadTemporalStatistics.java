/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.temporalFeatures;

import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.*;
import jceliac.tools.data.*;
import jceliac.experiment.*;

/**
 *
 * @author shegen
 */
public class FeatureExtractionThreadTemporalStatistics
        extends AbstractFeatureExtractionThread
{

    public FeatureExtractionThreadTemporalStatistics(IThreadedExperiment experiment,
                                                     FeatureExtractionMethod parentMethod, Frame content,
                                                     int classNumber, boolean isOneClassTrainingClass, boolean isTraining)
    {
        super(experiment, parentMethod, content, classNumber, isOneClassTrainingClass, isTraining);
    }

    @Override
    public synchronized DiscriminativeFeatures extractFeatures(Frame content, boolean isTraining)
            throws JCeliacGenericException
    {
        // this was implemented thread safe. if not, implement the thread safe
        // version here
        DiscriminativeFeatures f = super.parentMethod.extractFeatures(content);
        // this computes the combined histograms, do this within the treath to speedup the entire process
        // later threads would have to block and wait for others (within the mapDistance method)

        //  at this point all data was prepared(), dispose tha redundant info for more memory
        return f;
    }

}
