/*
 *  JCeliac Copyright (C) 2010-2011 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.temporalFeatures;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.classification.kernelSVM.SVMFeatureVector;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.FixedFeatureVector;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;

/**
 *
 * @author shegen
 */
public class TemporalStatisticFeatures extends DiscriminativeFeatures {

    public TemporalStatisticFeatures(FeatureExtractionParameters parameters) 
    {
        super(parameters);
    }

    public TemporalStatisticFeatures()
    {
        super();
    }

    @Override
    public DiscriminativeFeatures cloneFeatureSubset(FeatureVectorSubsetEncoding subset)
            throws JCeliacGenericException
    {
        TemporalStatisticFeatures featureSubset = new TemporalStatisticFeatures(super.featureExtractionParameters);
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
        
        double [] rawFeatureCopy = this.getFeatureVectorSubsetData(subset);
        FixedFeatureVector vector = new FixedFeatureVector();
        vector.setData(rawFeatureCopy);
        featureSubset.addAbstractFeature(vector);
        return featureSubset;
    }

    @Override
    public DiscriminativeFeatures cloneEmptyFeature()
    {
        TemporalStatisticFeatures featureSubset = new TemporalStatisticFeatures(super.featureExtractionParameters);
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;

        return featureSubset;
    }
    
    @Override
    public double doDistanceTo(DiscriminativeFeatures them)
            throws JCeliacGenericException 
    {
        double[] they = them.getAllFeatureVectorData();
        double[] us = this.getAllFeatureVectorData();

        double distance = 0;
        for (int i = 0; i < they.length; i++) {
            distance += (they[i] - us[i]) * (they[i] - us[i]);
        }
        return Math.sqrt(distance);
    }

    @Override
    public ArrayList<double[]> getLocalFeatureVectorData()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    

}
