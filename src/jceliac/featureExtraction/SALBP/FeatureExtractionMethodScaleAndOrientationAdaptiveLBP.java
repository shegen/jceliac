/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.SALBP;

import jceliac.featureExtraction.RotationAdaptiveLBPFeatures;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ScalespaceScaleAndShapeEstimator;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.OrientationEstimation;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ScaleEstimation;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.tools.filter.scalespace.LaplacianScalespaceRepresentation;
import jceliac.featureExtraction.affineScaleLBP.EllipticPointLookup;
import jceliac.tools.filter.scalespace.ScalespaceRepresentation;
import jceliac.featureExtraction.LBP.FeatureExtractionMethodLBP;
import jceliac.featureExtraction.affineScaleLBP.EllipticPoint;
import jceliac.tools.filter.scalespace.ScalespaceParameters;
import jceliac.experiment.FeatureExtractionMethodFactory;
import jceliac.featureExtraction.affineScaleLBP.Ellipse;
import jceliac.tools.interpolator.BilinearInterpolator;
import jceliac.tools.parameter.xml.XMLParseException;
import jceliac.featureExtraction.LBP.LBPHistogram;
import jceliac.featureExtraction.LBP.LBPFeatures;
import jceliac.tools.interpolator.Interpolator;
import jceliac.featureExtraction.StableCode;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.JCeliacGenericException;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.Frame;
import java.util.ArrayList;
import java.util.HashMap;
import jceliac.featureExtraction.affineScaleLBPScaleBias.AffineFilteredNeighbor;
import jceliac.featureExtraction.affineScaleLBPScaleBias.IAffineAdaptiveFeatureExtractionMethod;
import jceliac.featureExtraction.affineScaleLBPScaleBias.LBPShapeBias;

/**
 * The implementation of the scale and orientation adaptive LBP framework, this
 * has been used for ICPR experiments.
 * <p>
 * @author shegen
 */
@StableCode(comment = "Code is trusted",
            date = "20.01.2014",
            lastModified = "20.01.2014")
public class FeatureExtractionMethodScaleAndOrientationAdaptiveLBP
        extends FeatureExtractionMethodLBP
{

    private final FeatureExtractionParametersScaleAndOrientationAdaptiveLBP myParameters;
    private final HashMap<Integer, ArrayList <LBPScaleBias>> scaleBiasesPerClass;
    private final ScalespaceParameters scalespaceParameters;
    private final EllipticPointLookup ellipseLookupTable;
    private HashMap<Integer, ArrayList <ScaleEstimation>> scaleEstimationPerClass;
    private final HashMap<Integer, LBPScaleBias> uniqueScaleBiases;

    public FeatureExtractionMethodScaleAndOrientationAdaptiveLBP(
            FeatureExtractionParameters param)
            throws JCeliacGenericException
    {
        super(param);
        this.ellipseLookupTable = EllipticPointLookup.create();
        /* The scalespace is constructed such that the middle scale
         * sqrt(2)^0*c*sqrt(2) is 3 pixels wide. We define the scale bias to be
         * 3 pixels wide.
         */
        this.scalespaceParameters = new ScalespaceParameters(Math.sqrt(2.0d),
                                                             -4,
                                                             8,
                                                             0.25,
                                                             2.1214);

        // compute cos and sin lookup tables for scalespace paramters
        this.myParameters = (FeatureExtractionParametersScaleAndOrientationAdaptiveLBP) param;
        this.scaleBiasesPerClass = new HashMap<>();
        this.uniqueScaleBiases = new HashMap<>();
    }

    @Override
    public DiscriminativeFeatures extractTrainingFeatures(Frame content, int classIndex)
            throws JCeliacGenericException
    {

        LBPScaleFilteredDataSupplier filteredDataSupplier;
        FeatureExtractionParameters p;
        try {
            p = FeatureExtractionMethodFactory.createParametersByMethod(this.myParameters.getExtractionMethod().getStringValue());
        } catch(XMLParseException e) {
            throw new JCeliacGenericException(e);
        }
        IAffineAdaptiveFeatureExtractionMethod subMethod = (IAffineAdaptiveFeatureExtractionMethod) FeatureExtractionMethodFactory.createFeatureExtractionMethod(this.myParameters.getExtractionMethod(), p);
        filteredDataSupplier = new LBPScaleFilteredDataSupplier(content.getGrayData());

        // now estimate the scales of the input data
        ScalespaceRepresentation scaleRep = new LaplacianScalespaceRepresentation(content.getGrayData(), this.scalespaceParameters);
        scaleRep.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_SUM);
        ScaleEstimation localScaleEstimate = new ScaleEstimation(this.scalespaceParameters,
                                                                 scaleRep.getNormalizedLaplacianDataSupplier(),
                                                                 content.getWidth(), content.getHeight());
        localScaleEstimate.estimateGlobalScale();
        scaleRep.computeMaxima();

        ArrayList<ScaleEstimation> classScaleEstimations = this.scaleEstimationPerClass.get(classIndex);
        
        OrientationEstimation localOrientationEstimate = new OrientationEstimation();
        localOrientationEstimate.estimateGlobalOrientation(scaleRep,localScaleEstimate.getEstimatedScaleLevel());
        
        ArrayList <LBPScaleBias> scaleBiases = this.scaleBiasesPerClass.get(classIndex);
        // find correct scale bias for the given image, e.e. the scale bias with closest scale
        LBPScaleBias bestScaleBias = this.identifyBestScaleBiasForTraining(localScaleEstimate, scaleBiases);
        
        double midScaleRadius = 3.0d;
        double lowerScaleRadius = 1.5d;
        double upperScaleRadius = 4.5d;

        AffineScaleLBPBiasedFeatures features = 
            new AffineScaleLBPBiasedFeatures(this.myParameters.useFeatureScaleSelection);
        LBPScaleBias localBias = new LBPScaleBias(this.scalespaceParameters, bestScaleBias);

        LBPShapeBias dummyShapeBias = new LBPShapeBias(1, 1, 1);
        AffineLBPFeatures tmp = new AffineLBPFeatures(super.parameters, localScaleEstimate, localBias);
        tmp.setSignalIdentifier(content.getSignalIdentifier());
        tmp.setFrameNumber(content.getFrameIdentifier());

        localBias.setLBP_BASE_RADIUS(lowerScaleRadius);

        RotationAdaptiveLBPFeatures rotFeatures = new RotationAdaptiveLBPFeatures(this.myParameters,
                                                                                    localScaleEstimate,
                                                                                    localBias);
        double rotationSigma = localOrientationEstimate.getOrientationSigma();
        if(rotationSigma > 20) {
            rotationSigma = 0;
        }
        double off = rotationSigma / 20;
        for(double angleOff = -off; angleOff <= off; angleOff += 0.0873) {

            localBias.setLBP_BASE_RADIUS(lowerScaleRadius);
            LBPHistogram histogram1
                         = this.performScaleAndOrientationAdaptiveFeatureExtraction(subMethod,
                                                                                    filteredDataSupplier,
                                                                                    localBias,
                                                                                    localScaleEstimate,
                                                                                    localOrientationEstimate,
                                                                                    this.ellipseLookupTable,
                                                                                    angleOff);

            localBias.setLBP_BASE_RADIUS(midScaleRadius);
            LBPHistogram histogram2 = this.performScaleAndOrientationAdaptiveFeatureExtraction(subMethod,
                                                                                               filteredDataSupplier,
                                                                                               localBias,
                                                                                               localScaleEstimate,
                                                                                               localOrientationEstimate,
                                                                                               this.ellipseLookupTable,
                                                                                               angleOff);

            localBias.setLBP_BASE_RADIUS(upperScaleRadius);
            LBPHistogram histogram3 = this.performScaleAndOrientationAdaptiveFeatureExtraction(subMethod,
                                                                                               filteredDataSupplier,
                                                                                               localBias,
                                                                                               localScaleEstimate,
                                                                                               localOrientationEstimate,
                                                                                               this.ellipseLookupTable,
                                                                                               angleOff);

            LBPFeatures featureForAngleOffset = new LBPFeatures(this.myParameters);
            featureForAngleOffset.addAbstractFeature(histogram1);
            featureForAngleOffset.addAbstractFeature(histogram2);
            featureForAngleOffset.addAbstractFeature(histogram3);
            featureForAngleOffset.setSignalIdentifier(tmp.getSignalIdentifier());

            rotFeatures.addRotatedFeature(featureForAngleOffset);
            rotFeatures.setSignalIdentifier(tmp.getSignalIdentifier());
        }
        features.addFeature(rotFeatures);
        features.setSignalIdentifier(tmp.getSignalIdentifier());
        features.setFrameNumber(1);
        return features;
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {

        LBPScaleFilteredDataSupplier filteredDataSupplier;
        FeatureExtractionParameters p;
        try {
            p = FeatureExtractionMethodFactory.createParametersByMethod(this.myParameters.getExtractionMethod().getStringValue());
        } catch(XMLParseException e) {
            throw new JCeliacGenericException(e);
        }
        IAffineAdaptiveFeatureExtractionMethod subMethod = (IAffineAdaptiveFeatureExtractionMethod) FeatureExtractionMethodFactory.createFeatureExtractionMethod(this.myParameters.getExtractionMethod(), p);
        filteredDataSupplier = new LBPScaleFilteredDataSupplier(content.getGrayData());

        LBPShapeBias dummyShapeBias = new LBPShapeBias(1, 1, 1);
        // now estimate the scales of the input data
        ScalespaceRepresentation scaleRep = new LaplacianScalespaceRepresentation(content.getGrayData(), this.scalespaceParameters);
        scaleRep.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_SUM);
        ScaleEstimation localScaleEstimate = new ScaleEstimation(this.scalespaceParameters,
                                                                 scaleRep.getNormalizedLaplacianDataSupplier(),
                                                                 content.getWidth(), content.getHeight());
        localScaleEstimate.estimateGlobalScale();
        scaleRep.computeMaxima();
        OrientationEstimation localOrientationEstimate = new OrientationEstimation();
        localOrientationEstimate.estimateGlobalOrientation(scaleRep,localScaleEstimate.getEstimatedScaleLevel());

        ArrayList<LBPScaleBias> scaleBiases = new ArrayList<>();
        for(Integer key : this.uniqueScaleBiases.keySet()) {
            LBPScaleBias tmp = this.uniqueScaleBiases.get(key);
            if(tmp != null) {
                scaleBiases.add(tmp);
            }
        }
        AffineScaleLBPBiasedFeatures features = 
                new AffineScaleLBPBiasedFeatures(this.myParameters.useFeatureScaleSelection);
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());

        double midScaleRadius = 3.0d;
        double lowerScaleRadius = 1.5d;
        double upperScaleRadius = 4.5d;

        for(LBPScaleBias curScaleBias : scaleBiases) {
            if(this.isReasonableScaleDifference(curScaleBias, localScaleEstimate) == false) {
                continue;
            }
            LBPScaleBias localBias = new LBPScaleBias(this.scalespaceParameters, curScaleBias);
            RotationAdaptiveLBPFeatures rotFeatures = new RotationAdaptiveLBPFeatures(this.myParameters,
                                                                                        localScaleEstimate,
                                                                                        localBias);
            double rotationSigma = localOrientationEstimate.getOrientationSigma();
            if(rotationSigma > 20) {
                rotationSigma = 0;
            }
            double off = rotationSigma / 20;
            for(double angleOff = -off; angleOff <= off; angleOff += 0.0873) {
                localBias.setLBP_BASE_RADIUS(lowerScaleRadius);
                LBPHistogram histogram1
                             = this.performScaleAndOrientationAdaptiveFeatureExtraction(subMethod,
                                                                                        filteredDataSupplier,
                                                                                        localBias,
                                                                                        localScaleEstimate,
                                                                                        localOrientationEstimate,
                                                                                        this.ellipseLookupTable,
                                                                                        angleOff);

                localBias.setLBP_BASE_RADIUS(midScaleRadius);
                LBPHistogram histogram2 = this.performScaleAndOrientationAdaptiveFeatureExtraction(subMethod,
                                                                                                   filteredDataSupplier,
                                                                                                   localBias,
                                                                                                   localScaleEstimate,
                                                                                                   localOrientationEstimate,
                                                                                                   this.ellipseLookupTable,
                                                                                                   angleOff);

                localBias.setLBP_BASE_RADIUS(upperScaleRadius);
                LBPHistogram histogram3 = this.performScaleAndOrientationAdaptiveFeatureExtraction(subMethod,
                                                                                                   filteredDataSupplier,
                                                                                                   localBias,
                                                                                                   localScaleEstimate,
                                                                                                   localOrientationEstimate,
                                                                                                   this.ellipseLookupTable,
                                                                                                   angleOff);

                LBPFeatures featureForAngleOffset = new LBPFeatures(this.myParameters);
                featureForAngleOffset.addAbstractFeature(histogram1);
                featureForAngleOffset.addAbstractFeature(histogram2);
                featureForAngleOffset.addAbstractFeature(histogram3);
                featureForAngleOffset.setSignalIdentifier(content.getSignalIdentifier());

                rotFeatures.addRotatedFeature(featureForAngleOffset);
                rotFeatures.setSignalIdentifier(content.getSignalIdentifier());
            }
            features.addFeature(rotFeatures);
            features.setSignalIdentifier(content.getSignalIdentifier());
            features.setFrameNumber(1);
        }
        return features;
    }

    /**
     * Validates if the scale difference between the scale bias and locally
     * estimated scale is in a resonable range.
     * <p>
     * @param scaleBias       Scale bias.
     * @param localEstimation Scale estimation.
     * <p>
     * @return True if the scale difference is reasonable, false if it doesn't
     *         make sense.
     */
    public boolean isReasonableScaleDifference(LBPScaleBias scaleBias, ScaleEstimation localEstimation)
    {

        int dominantClassScale = scaleBias.getClassScaleEstimation().getEstimatedScaleLevel();
        int localScale = localEstimation.getEstimatedScaleLevel();

        double classSigma = this.scalespaceParameters.getSigmaForScaleLevel(dominantClassScale);
        double localSigma = this.scalespaceParameters.getSigmaForScaleLevel(localScale);

        boolean ret = ((Math.max(classSigma, localSigma) / Math.min(classSigma, localSigma)) > 2.5);
        return ret;

    }

    
    /**
     * Extracts the patterns scale adaptively. 
     * 
     * @param subMethod The LBP based method used to compute the actual patterns. 
     * @param filteredDataSupplier Scale-adaptively filtered data. 
     * @param scaleBias Scale bias to use. 
     * @param localScaleEstimation Scale estimation for current image. 
     * @param localOrientationEstimation Orientation estimation for current image. 
     * @param ellipticPointLookup  Lookup table for neighbor points. 
     * @param angleOffset Angle intervall size to use for orientation etimation error compensation. 
     * @return Scale adaptively extracted LBP features. 
     * @throws JCeliacGenericException 
     */
    public LBPHistogram performScaleAndOrientationAdaptiveFeatureExtraction(
            IAffineAdaptiveFeatureExtractionMethod subMethod,
            LBPScaleFilteredDataSupplier filteredDataSupplier,
            LBPScaleBias scaleBias,
            ScaleEstimation localScaleEstimation,
            OrientationEstimation localOrientationEstimation,
            EllipticPointLookup ellipticPointLookup,
            double angleOffset)
            throws JCeliacGenericException
    {
        int allPossiblyComputedPatterns = 0;
        int computedPatterns = 0;

        BilinearInterpolator interpolator = new BilinearInterpolator();
        AffineFilteredNeighbor center = new AffineFilteredNeighbor();
        AffineFilteredNeighbor[] neighbors = new AffineFilteredNeighbor[8];
        for(int i = 0; i < neighbors.length; i++) {
            neighbors[i] = new AffineFilteredNeighbor();
        }
        Ellipse ell = new Ellipse();
        LBPHistogram histogram = subMethod.initializeHistogram();

        // create elliptic pionts once and recycle to speedup 
        EllipticPoint[] points = new EllipticPoint[8];
        for(int i = 0; i < points.length; i++) {
            points[i] = new EllipticPoint();
        }

        for(int scaleLevel = 0;
            scaleLevel < scaleBias.getScalespaceParameters().getNumberOfScales();
            scaleLevel++) {

            double confidence = localScaleEstimation.getConfidenceForScale(scaleLevel);
            if(confidence < 0.99) {
                continue;
            }
            double lbpRadius = scaleBias.getLBPRadiusForScaleLevel(scaleLevel);
            double c = (lbpRadius / 0.94) - lbpRadius;
            ell.a = lbpRadius;
            ell.b = lbpRadius + c;
            ell.phi = Math.PI * 2 + localOrientationEstimation.getDominantOrientation() + angleOffset;
            EllipticPoint[] neighborPoints = this.ellipseLookupTable.lookupOld(0, 0, ell.a, ell.b, ell.phi);

            // this will deliver the correctly filtered data for the lbp radius
            double[][] data = filteredDataSupplier.retrieveLBPScaleFilteredDataForLBPRadius(lbpRadius);
            double stdev = filteredDataSupplier.getStdevForFilteredDataForLBPRadius(lbpRadius);

            for(int x = 2; x < data.length - 2; x++) {
                for(int y = 2; y < data[0].length - 2; y++) {
                    allPossiblyComputedPatterns++;
                    try {

                        this.collectNeighbors(interpolator, data, x, y, neighborPoints, center, neighbors);
                        subMethod.computeAffineAdaptivePattern(histogram, center, neighbors, stdev, confidence);
                        computedPatterns++;
                    } catch(ArrayIndexOutOfBoundsException e) {
                    }
                }
            }
        }
        histogram.setComputedPatternRatio((double) computedPatterns / (double) allPossiblyComputedPatterns);
        subMethod.finalizeHistogram(histogram);
        return histogram;
    }

    /**
     * Collects neighbors to supply them to the subMethod for computing
     * patterns.
     * <p>
     * @param interp    Interpolator to use.
     * @param data      Scale-adaptively filtered data.
     * @param x         Center x-coordinate for pattern.
     * @param y         Center y-coordinate for pattern.
     * @param points    Neighbor point positions.
     * @param center    Pre-allocated object for center.
     * @param neighbors Pre-allocatd array for neighbors.
     */
    private void collectNeighbors(Interpolator interp, double[][] data, int x, int y, EllipticPoint[] points,
                                  AffineFilteredNeighbor center, AffineFilteredNeighbor[] neighbors)
    {

        center.value = data[x][y];
        center.distanceToCenter = 0;

        for(int p = 0; p < 8; p++) {
            double value = interp.interpolate(data, points[p].x + x, points[p].y + y);
            neighbors[p].value = value;
            neighbors[p].position = p;
        }
    }
    
    /**
     * Estimates the scales per class. 
     * 
     * @param readersByClass Readers for all classes. 
     * @throws JCeliacGenericException 
     */
    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass)
            throws JCeliacGenericException
    {

        // estimate scales for training textures
        ScalespaceScaleAndShapeEstimator scaleAndShapeEstimator = new ScalespaceScaleAndShapeEstimator(this.scalespaceParameters);
        scaleAndShapeEstimator.estimateScalesByClass(readersByClass);

        this.scaleEstimationPerClass = scaleAndShapeEstimator.reduceScaleEstimations(this.myParameters.isAllowMultipleScalesPerClass());
        this.prepareScaleBiases();
    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {
        // nothing to do
    }
    
       
    private LBPScaleBias identifyBestScaleBiasForTraining(ScaleEstimation scaleEstimation,
                                                          ArrayList <LBPScaleBias> scaleBiasesOfClass)
    {
        LBPScaleBias bestBias = null;
        int closest = Integer.MAX_VALUE;
        
        for(LBPScaleBias scaleBias : scaleBiasesOfClass) {
            int sbLevel = scaleBias.getClassScaleEstimation().getEstimatedScaleLevel();
            int estLevel = scaleEstimation.getEstimatedScaleLevel();
            
            int diff = Math.abs(sbLevel - estLevel);
            if(diff < closest) {
                closest = diff;
                bestBias = scaleBias;
            }
        }
        return bestBias;
    }
    
    private void prepareScaleBiases()
    {
        for(int classIndex : this.scaleEstimationPerClass.keySet()) 
        {
            ArrayList <ScaleEstimation> classDominantScales = 
                            this.scaleEstimationPerClass.get(classIndex);
            ArrayList <LBPScaleBias> scaleBiasesOfClass = new ArrayList <>();
            
            for(ScaleEstimation curScale : classDominantScales) 
            {
                int scaleBiasScale = curScale.getEstimatedScaleLevel();

                LBPScaleBias scaleBias;
                if(this.uniqueScaleBiases.containsKey(scaleBiasScale) == false) {
                    scaleBias = new LBPScaleBias(this.scalespaceParameters, scaleBiasScale,
                                                          classIndex, curScale);
                    this.uniqueScaleBiases.put(scaleBiasScale, scaleBias);
                }else {
                    scaleBias = this.uniqueScaleBiases.get(scaleBiasScale);
                }
                scaleBiasesOfClass.add(scaleBias);
            }
            this.scaleBiasesPerClass.put(classIndex, scaleBiasesOfClass);
        }
    }
    
}
