/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.SALBP;

import java.util.ArrayList;
import java.util.HashMap;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ScaleEstimation;
import jceliac.featureExtraction.LBP.LBPHistogram;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.affineScaleLBP.EllipticPointLookup;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.OrientationEstimation;
import jceliac.tools.data.Frame;

/**
 * Interface implemented by LBP-subMethods for scale adaptive framework.
 * <p>
 * @author shegen
 */
public interface IScaleAdaptiveFeatureExtractionMethod
{

    public abstract LBPHistogram performScaleAdaptiveFeatureExtraction(LBPScaleFilteredDataSupplier dataSupplier,
                                                                       LBPScaleBias scaleBias,
                                                                       ScaleEstimation localScaleEstimation,
                                                                       LBPNeighborLookup lookup)
            throws JCeliacGenericException;
   
    public abstract ArrayList <LBPHistogram>
                    performScaleAndOrientationAdaptiveFeatureExtraction(LBPScaleFilteredDataSupplier dataSupplier,
                                                                        LBPScaleBias scaleBias,
                                                                        ScaleEstimation localScaleEstimation,
                                                                        OrientationEstimation localOrientationEstimation,
                                                                        LBPNeighborLookup lookup,
                                                                        double orientationDelta, 
                                                                        double orientationSteps,
                                                                        EllipticPointLookup ellipticLookup)
    throws JCeliacGenericException;
    

    /**
     * Prepare the data, perform any sort of pre-filtering if needed.
     * <p>
     * @param content Current image frame.
     * <p>
     * @return Prepared filtered data supplier.
     */
    default public LBPScaleFilteredDataSupplier prepareDataSupplier(Frame content)
    {
        return new LBPScaleFilteredDataSupplier(content.getGrayData());
    }
    
    /**
     * Stores parameters for the scale adaptive extraction in the used extraction method. 
     * @param params 
     */
    public abstract void setScaleAdaptiveParameters(FeatureExtractionParametersScaleAdaptiveLBP params);

}
