/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.SALBP;

import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ScaleEstimation;
import jceliac.tools.filter.scalespace.ScalespaceParameters;

/**
 * This represents a scale bias for scale adaptive LBP, this is also known as
 * trained base scale.
 * <p>
 * @author shegen
 */
public class LBPScaleBias
{

    private final ScalespaceParameters scalespaceParameters;
    private double[] lbpRadii;
    private double LBP_BASE_RADIUS = 3d;
    private int id;
    private double stdevAtScale;
    private int scaleBiasBaseScale;
    public boolean isDefaultScaleBias = false;

    private ScaleEstimation classScaleEstimation;

    private static int idCounter = 0;
    public double classConfidence;

    public synchronized static int retrieveUniqueID()
    {
        int currentID = LBPScaleBias.idCounter;
        LBPScaleBias.idCounter++;
        return currentID;
    }

    public LBPScaleBias(ScalespaceParameters parameters, LBPScaleBias scaleBias)
    {
        this.scalespaceParameters = parameters;
        this.scaleBiasBaseScale = scaleBias.scaleBiasBaseScale;
        this.id = scaleBias.id;
        this.classScaleEstimation = scaleBias.classScaleEstimation;

        this.computeLBPRadii();
    }

    public LBPScaleBias(ScalespaceParameters parameters, int scaleBiasBaseScale,
                        int id, ScaleEstimation classScaleEstimation)
    {
        this.scalespaceParameters = parameters;
        this.scaleBiasBaseScale = scaleBiasBaseScale;
        this.id = id;
        this.classScaleEstimation = classScaleEstimation;

        this.computeLBPRadii();
    }

    /**
     * Set's the LBP base radius for the estimated scale.
     * <p>
     * @param LBP_BASE_RADIUS LBP base radius.
     */
    public void setLBP_BASE_RADIUS(double LBP_BASE_RADIUS)
    {
        this.LBP_BASE_RADIUS = LBP_BASE_RADIUS;
        this.computeLBPRadii();
    }

    /**
     * Computes the lbp radii for each scale level based on the scale estimation
     * supplied in the constructor.
     */
    private void computeLBPRadii()
    {
        this.lbpRadii = new double[this.scalespaceParameters.getNumberOfScales()];

        double dominantScaleRadius = this.scalespaceParameters.getSigmaForScaleLevel(this.scaleBiasBaseScale) * Math.sqrt(2.0d);

        for(int scaleLevel = 0;
            scaleLevel < this.scalespaceParameters.getNumberOfScales();
            scaleLevel++) {
            double scaleLevelRadius = this.scalespaceParameters.getSigmaForScaleLevel(scaleLevel) * Math.sqrt(2.0d);
            double scaleFactor = scaleLevelRadius / dominantScaleRadius;

            // scale the LBP radius 
            this.lbpRadii[scaleLevel] = LBP_BASE_RADIUS * scaleFactor;
        }
    }

    public final double getLBPRadiusForScaleLevel(int scaleLevel)
    {
        return this.lbpRadii[scaleLevel];
    }

    public int getId()
    {
        return id;
    }

    public double getStdevAtScale()
    {
        return stdevAtScale;
    }

    public void setStdevAtScale(double stdevAtScale)
    {
        this.stdevAtScale = stdevAtScale;
    }

    public int getScaleBiasBaseScale()
    {
        return scaleBiasBaseScale;
    }

    public ScaleEstimation getClassScaleEstimation()
    {
        return classScaleEstimation;
    }

    public ScalespaceParameters getScalespaceParameters()
    {
        return scalespaceParameters;
    }

}
