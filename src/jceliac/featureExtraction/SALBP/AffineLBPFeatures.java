/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.SALBP;


import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.FixedFeatureVector;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.LBP.LBPFeatures;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ScaleEstimation;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;

/**
 * Affine LBP features with scale-bias and shape-bias. 
 * Only features with matching scale and shape-bias are comparable in the classifier. 
 * 
 * @author shegen
 */
public class AffineLBPFeatures
        extends LBPFeatures
{

    // scale and shape information
    protected ScaleEstimation scaleEstimation = null;
    protected LBPScaleBias scaleBias = null;
    // indicates that this feature can be used for training a classifier, we need this distinction to perform CV 
    // because training features and evaluation features are treated differently for this method
    public boolean isTrainingFeature = false; 
    
    public AffineLBPFeatures(FeatureExtractionParameters parameters,
                             ScaleEstimation scaleEstimation, LBPScaleBias scaleBias)
    {
        super(parameters);
        this.scaleEstimation = scaleEstimation;
        this.scaleBias = scaleBias;
        //this.orientedFeatureVectors = new HashMap<>();
    }

    public AffineLBPFeatures(ScaleEstimation scaleEstimation, LBPScaleBias scaleBias)
    {
        this.scaleEstimation = scaleEstimation;
        this.scaleBias = scaleBias;
        //this.orientedFeatureVectors = new HashMap<>();
    }

    public AffineLBPFeatures(FeatureExtractionParameters parameters)
    {
        super(parameters);
    }

    public AffineLBPFeatures()
    {
        super();
    }

    @Override
    public DiscriminativeFeatures cloneEmptyFeature()
    {
        AffineLBPFeatures featureSubset = new AffineLBPFeatures(super.featureExtractionParameters);
        featureSubset.isTrainingFeature = this.isTrainingFeature;
        featureSubset.scaleBias = this.scaleBias;
        featureSubset.scaleEstimation = this.scaleEstimation;
        featureSubset.featureVectors = this.featureVectors;
        
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
        
        return featureSubset;
    }

    @Override
    public DiscriminativeFeatures cloneFeatureSubset(FeatureVectorSubsetEncoding subset)
            throws JCeliacGenericException
    {
        AffineLBPFeatures featureSubset = new AffineLBPFeatures(super.featureExtractionParameters);
        featureSubset.isTrainingFeature = this.isTrainingFeature;
        featureSubset.scaleBias = this.scaleBias;
        featureSubset.scaleEstimation = this.scaleEstimation;
        featureSubset.featureVectors = this.featureVectors;
        
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
        
        double [] rawFeatureCopy = this.getFeatureVectorSubsetData(subset);
        FixedFeatureVector vector = new FixedFeatureVector();
        vector.setData(rawFeatureCopy);
        featureSubset.addAbstractFeature(vector);
        return featureSubset;
    }
    
    public ScaleEstimation getScaleEstimation()
    {
        return scaleEstimation;
    }

    public LBPScaleBias getScaleBias()
    {
        return scaleBias;
    }

    public boolean comparableTo(AffineLBPFeatures f)
    {
        if(this.scaleBias == null) {
            return true;
        }
        if(f.scaleBias.getId() == this.scaleBias.getId()) {
            return true;
        }
        return false;
    }

    public boolean isTrainingFeature()
    {
        return isTrainingFeature;
    }

    public void setIsTrainingFeature(boolean isTrainingFeature)
    {
        this.isTrainingFeature = isTrainingFeature;
    }
    
    public boolean isEmpty() 
            throws JCeliacGenericException
    {
        return false;
    }
}
