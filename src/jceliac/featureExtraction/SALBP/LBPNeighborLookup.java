/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.featureExtraction.SALBP;

/**
 *
 * @author shegen
 */
public class LBPNeighborLookup
{
    public double [] cosineLookup;
    public double [] sineLookup;

    public double [][] orientedCosineLookup;
    public double [][] orientedSineLookup;
    
    
    // compute lookup for all possible lbp radii, without rotation
    public LBPNeighborLookup(int numberOfNeighbors)
    {
        this.computeTrigLookup(numberOfNeighbors);
        this.computeOrientatedTrigLookup(numberOfNeighbors);
    }
    
    // compute lookup for all possible lbp radii, with orientations
    public LBPNeighborLookup(int numberOfNeighbors, boolean blub)
    {
        this.computeOrientatedTrigLookup(numberOfNeighbors);
    }
    
    private void computeTrigLookup(int numberOfNeighbors)
    {
        this.cosineLookup = new double[numberOfNeighbors];
        this.sineLookup = new double[numberOfNeighbors];
        
        double p = (Math.PI) / 4.0d;
        for(int n = 0; n < numberOfNeighbors; n++) {
            this.cosineLookup[n] = Math.cos((2.0d * Math.PI * (double) (n + 1)) / (double) 8);
            this.sineLookup[n] = Math.sin((2.0d * Math.PI * (double) (n + 1)) / (double) 8);
        }
    }
    
    private void computeOrientatedTrigLookup(int numberOfNeighbors)
    {
        this.orientedCosineLookup = new double[360][numberOfNeighbors];
        this.orientedSineLookup = new double[360][numberOfNeighbors];
        
        double step = (1.0d / 180.0d) * Math.PI;
        for(int orientation = 0; orientation < 360; orientation ++) 
        {
            double orientationAngle = step * orientation;
            for(int n = 0; n < numberOfNeighbors; n++) {
                this.orientedCosineLookup[orientation][n] = Math.cos(orientationAngle + ((2.0d * Math.PI * (double) (n + 1)) / (double) 8));
                this.orientedSineLookup[orientation][n] = Math.sin(orientationAngle + ((2.0d * Math.PI * (double) (n + 1)) / (double) 8));
            }
        }
    }
    
    public void lookup(double radius, double [][] neighbors)
    {
        for(int n = 0; n < this.cosineLookup.length; n++) {
            double coordX = radius * this.cosineLookup[n];
            double coordY = radius * this.sineLookup[n];
            neighbors[n][0] = coordX;
            neighbors[n][1] = coordY;
        }
    } 
    
    public void lookup(double radius, double orientation, double [][] neighbors)
    {
        double step = (1.0d / 180.0d) * Math.PI;
        int orientationIndex = (int)Math.round((orientation % (2*Math.PI)) / step);
        if(orientationIndex == 360) orientationIndex = 0;
        
        for(int n = 0; n < this.orientedCosineLookup[0].length; n++) {
            double coordX = radius * this.orientedCosineLookup[orientationIndex][n];
            double coordY = radius * this.orientedSineLookup[orientationIndex][n];
            neighbors[n][0] = coordX;
            neighbors[n][1] = coordY;
        }
    } 
}
