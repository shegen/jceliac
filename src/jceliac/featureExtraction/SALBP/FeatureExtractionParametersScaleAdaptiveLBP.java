/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.SALBP;

import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.LBP.FeatureExtractionParametersLBP;
import jceliac.featureExtraction.FeatureExtractionMethod.EFeatureExtractionMethod;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersScaleAdaptiveLBP
        extends FeatureExtractionParametersLBP
{
    
    // minimum level of confidence at scale used to extract pattern at scale
    protected double minConfidenceLevel = 1.0d; // was 0.9 for ICASSP and 1.0 for ICPR, we should stick with 1.0 for performance
    protected String extractionMethod = "LBP";

    
    protected boolean adaptOrientation = false;
    
    /*
     * If set to "true" this will split the samples from a class to several
     * sub-classes according to their scale in scale-space. This should be used
     * if the training classes have multiple different scales.
     */
    protected final boolean allowMultipleScalesPerClass = false;
    // this has not been tested and is currently enforced
    protected final boolean useFeatureScaleSelection = true;
   
    protected String featureMode = "SELECTED_SCALE_BIAS";

    protected double scalespaceStep = 0.25d;
    
    public static enum EFeatureMode {
        // the ICPR/ICASSP mode, features are compared by the scale bias and the best distance is picked
        // this requires some additional code for feature comparison and can not easily be used in SVM,
        // however it is faster because the training data computation is not as complex
        SELECTED_SCALE_BIAS,
        // a new variation in that features at all scale biases are also computed in the training
        // and all features are concatenated in a sorted order (by scale bias) this also works
        // for SVM
        ALL_SCALE_BIAS;
        
        public static FeatureExtractionParametersScaleAdaptiveLBP.EFeatureMode valueOfIgnoreCase(String s)
                throws JCeliacGenericException
        {
            for(FeatureExtractionParametersScaleAdaptiveLBP.EFeatureMode type : 
                FeatureExtractionParametersScaleAdaptiveLBP.EFeatureMode.values()) 
            {
                if(type.name().equalsIgnoreCase(s)) {
                    return type;
                }
            }
            throw new JCeliacGenericException("Unknown SALBP Feature Mode.");
        }
     };
    
    
    @Override
    public String getMethodName()
    {
        return "SALBP (" + this.extractionMethod + ")";
    }

    public EFeatureExtractionMethod getExtractionMethod()
    {
        return EFeatureExtractionMethod.mapStringToValue(this.extractionMethod);
    }

    public void setExtractionMethod(String extractionMethod)
    {
        this.extractionMethod = extractionMethod;
    }

    public boolean isAllowMultipleScalesPerClass()
    {
        return allowMultipleScalesPerClass;
    }
    public double getMinConfidenceLevel()
    {
        return minConfidenceLevel;
    }

    public EFeatureMode getFeatureMode()
            throws JCeliacGenericException
    {
        return EFeatureMode.valueOfIgnoreCase(this.featureMode);
    }

    public void setFeatureMode(String featureMode)
    {
        this.featureMode = featureMode;
    }

    public boolean useFeatureScaleSelection()
    {
        return useFeatureScaleSelection;
    }

    public boolean isAdaptOrientation()
    {
        return adaptOrientation;
    }

    public void setAdaptOrientation(boolean adaptOrientation)
    {
        this.adaptOrientation = adaptOrientation;
    }

    public double getScalespaceStep()
    {
        return scalespaceStep;
    }

    public void setScalespaceStep(double scalespaceStep)
    {
        this.scalespaceStep = scalespaceStep;
    }
   
    @Override
    public void report()
    {
        super.report();
    }
}
