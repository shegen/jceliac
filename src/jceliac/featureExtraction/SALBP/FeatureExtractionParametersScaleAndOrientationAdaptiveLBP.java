/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.SALBP;

import jceliac.featureExtraction.LBP.FeatureExtractionParametersLBP;
import jceliac.featureExtraction.FeatureExtractionMethod;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersScaleAndOrientationAdaptiveLBP
        extends FeatureExtractionParametersLBP
{

    protected String extractionMethod = "LBP";

    /*
     * If set to "true" this will split the samples from a class to several
     * sub-classes according to their scale in scale-space. This should be used
     * if the training classes have multiple different scales.
     */
    protected boolean allowMultipleScalesPerClass = false;
    protected boolean useFeatureScaleSelection = true;
    
    
    @Override
    public String getMethodName()
    {
        return "SOALBP (" + this.extractionMethod + ")";
    }

    public FeatureExtractionMethod.EFeatureExtractionMethod getExtractionMethod()
    {
        return FeatureExtractionMethod.EFeatureExtractionMethod.mapStringToValue(this.extractionMethod);
    }

    public void setExtractionMethod(String extractionMethod)
    {
        this.extractionMethod = extractionMethod;
    }

    public boolean isAllowMultipleScalesPerClass()
    {
        return allowMultipleScalesPerClass;
    }

    public void setAllowMultipleScalesPerClass(boolean allowMultipleScalesPerClass)
    {
        this.allowMultipleScalesPerClass = allowMultipleScalesPerClass;
    }

    @Override
    public void report()
    {
        super.report();
    }
    
    

}
