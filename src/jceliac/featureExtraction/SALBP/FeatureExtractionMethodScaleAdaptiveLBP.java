/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.SALBP;

import jceliac.featureExtraction.RotationAdaptiveLBPFeatures;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ScalespaceScaleAndShapeEstimator;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ScaleEstimation;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.tools.filter.scalespace.LaplacianScalespaceRepresentation;
import jceliac.featureExtraction.LBP.FeatureExtractionMethodLBP;
import jceliac.tools.filter.scalespace.ScalespaceRepresentation;
import jceliac.tools.filter.scalespace.ScalespaceParameters;
import jceliac.experiment.FeatureExtractionMethodFactory;
import jceliac.tools.parameter.xml.XMLParseException;
import jceliac.featureExtraction.LBP.LBPHistogram;
import jceliac.featureExtraction.StableCode;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.JCeliacGenericException;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.data.DataReader;
import jceliac.experiment.Experiment;
import jceliac.tools.data.Frame;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import jceliac.featureExtraction.LBP.LBPFeatures;
import jceliac.featureExtraction.LBP.LBPMultiscale;
import jceliac.featureExtraction.affineScaleLBP.EllipticPointLookup;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.OrientationEstimation;
import jceliac.tools.timer.PerformanceTimer;

/**
 * This implements the scale adaptive LBP based framework, hence also SALTP and
 * SAFLBP, this was used for ICASSP experiments and is stable code. XXX:
 * Supports only grayscale atm.
 * <p>
 * @author shegen
 */
@StableCode(comment = "Code is trusted",
            date = "20.01.2014",
            lastModified = "20.01.2014")
public class FeatureExtractionMethodScaleAdaptiveLBP
        extends FeatureExtractionMethodLBP
{
 
    private final static double LOWER_RADIUS = 1.5;
    private final static double MID_RADIUS = 3;
    private final static double UPPER_RADIUS = 4.5;
    
    // the mean error of the orientation estimation is 10-15°, we therefore step in 10° 
    private final static double ORIENTATION_STEP = (10.0d / 180.0d)*Math.PI;
    private EllipticPointLookup ellipseLookupTable;
    private final FeatureExtractionParametersScaleAdaptiveLBP myParameters;
    private final HashMap<Integer, ArrayList<LBPScaleBias>> scaleBiasesPerClass;
    private final HashMap<Integer, LBPScaleBias> uniqueScaleBiases;
    private ScalespaceParameters scalespaceParameters;
    private final LBPNeighborLookup neighborLookupTable;

    private HashMap<Integer, ArrayList<ScaleEstimation>> scaleEstimationPerClass;
    private LBPScaleBias defaultScaleBias1;
    private LBPScaleBias defaultScaleBias2;
    private LBPScaleBias defaultScaleBias3;

    private ScaleEstimation defaultScaleEstimation;
    

    public FeatureExtractionMethodScaleAdaptiveLBP(FeatureExtractionParameters param)
            throws JCeliacGenericException
    {
        super(param);
        this.myParameters = (FeatureExtractionParametersScaleAdaptiveLBP) param;
        
       // this.ellipseLookupTable = EllipticPointLookup.create();
        this.neighborLookupTable = new LBPNeighborLookup(8);
        double step = this.myParameters.getScalespaceStep();

        /* The scalespace is constructed such that the middle scale
         * sqrt(2)^0*c*sqrt(2) is 3 pixels wide. We define the scale bias to be
         * 3 pixels wide.
         */
        this.scalespaceParameters = new ScalespaceParameters(Math.sqrt(2.0d),
                                                             -4,
                                                             8,
                                                             step,
                                                             2.1214);
        
        // compute cos and sin lookup tables for scalespace paramters
        this.scaleBiasesPerClass = new HashMap<>();
        this.uniqueScaleBiases = new HashMap<>();
    }

    @Override
    @SuppressWarnings("UnusedAssignment")
    public DiscriminativeFeatures extractTrainingFeatures(Frame content, int classIndex)
            throws JCeliacGenericException
    {  
        if(this.myParameters.isAdaptOrientation()) {
            return this.extractSOALBPTrainingFeaturesSSB(content, classIndex);
        }else {
            return this.extractSALBPTrainingFeaturesSSB(content, classIndex);
        }
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {
        if(this.myParameters.isAdaptOrientation()) {
            return this.extractSOALBPFeaturesSSB(content);
        }else {
            return this.extractSALBPFeaturesSSB(content);  
        }
    }

      
    public DiscriminativeFeatures extractSALBPTrainingFeaturesSSB(Frame content, int classIndex)
            throws JCeliacGenericException
    {

        LBPScaleFilteredDataSupplier filteredDataSupplier;
        FeatureExtractionParameters p;
        try {
            p = FeatureExtractionMethodFactory.createParametersByMethod(this.myParameters.getExtractionMethod().getStringValue());
        } catch(XMLParseException e) {
            throw new JCeliacGenericException(e);
        }
        IScaleAdaptiveFeatureExtractionMethod subMethod = (IScaleAdaptiveFeatureExtractionMethod) FeatureExtractionMethodFactory.createFeatureExtractionMethod(this.myParameters.getExtractionMethod(), p);
        subMethod.setScaleAdaptiveParameters(this.myParameters);
        filteredDataSupplier = subMethod.prepareDataSupplier(content);

        // now estimate the scales of the input data
        ScalespaceRepresentation scaleRep = new LaplacianScalespaceRepresentation(content.getGrayData(), this.scalespaceParameters);
        scaleRep.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_ABSOLUTE_SUM);
        ScaleEstimation localScaleEstimate = new ScaleEstimation(this.scalespaceParameters,
                                                                 scaleRep.getNormalizedLaplacianDataSupplier(),
                                                                 content.getWidth(), content.getHeight());
        localScaleEstimate.estimateGlobalScale();
        ArrayList<LBPScaleBias> scaleBiases
                                = this.identifyBestScaleBiasForTraining(localScaleEstimate, this.scaleBiasesPerClass.get(classIndex));

        AffineScaleLBPBiasedFeatures features
                                     = new AffineScaleLBPBiasedFeatures(this.myParameters.useFeatureScaleSelection());
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());
        
        OrientationEstimation defaultOrientation = new OrientationEstimation();
        defaultOrientation.setDominantOrientation(Double.NaN);
        for(LBPScaleBias curScaleBias : scaleBiases) 
        {
            if(localScaleEstimate.isValid() == false) {
                break;
                
            }
            this.extractOrientationCompensatedFeaturesForScaleBias(content, filteredDataSupplier, curScaleBias, 
                                           localScaleEstimate, defaultOrientation, 
                                           0, 1, subMethod, features);
        }
       this.extractScaleAndOrientationAdaptiveFallbackFeatures(content,
                                                                subMethod,
                                                                filteredDataSupplier,
                                                                features,
                                                                defaultOrientation,
                                                                0,
                                                                1,
                                                                1);
        return features;
    }
    
    public DiscriminativeFeatures extractSALBPFeaturesSSB(Frame content)
            throws JCeliacGenericException
    {
        PerformanceTimer allTimer = new PerformanceTimer();
        allTimer.startTimer();
        
        LBPScaleFilteredDataSupplier filteredDataSupplier;
        FeatureExtractionParameters p;
        try {
            p = FeatureExtractionMethodFactory.createParametersByMethod(this.myParameters.getExtractionMethod().getStringValue());
        } catch(XMLParseException e) {
            throw new JCeliacGenericException(e);
        }
        IScaleAdaptiveFeatureExtractionMethod subMethod = (IScaleAdaptiveFeatureExtractionMethod) FeatureExtractionMethodFactory.createFeatureExtractionMethod(this.myParameters.getExtractionMethod(), p);
        subMethod.setScaleAdaptiveParameters(this.myParameters);
        filteredDataSupplier = subMethod.prepareDataSupplier(content);

        PerformanceTimer scalespaceTimer = new PerformanceTimer();
        scalespaceTimer.startTimer();
        
        // now estimate the scales of the input data
        ScalespaceRepresentation scaleRep = new LaplacianScalespaceRepresentation(content.getGrayData(), this.scalespaceParameters);
        scaleRep.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_ABSOLUTE_SUM);
        ScaleEstimation localScaleEstimate = new ScaleEstimation(this.scalespaceParameters,
                                                                 scaleRep.getNormalizedLaplacianDataSupplier(),
                                                                 content.getWidth(), content.getHeight());

        localScaleEstimate.estimateGlobalScale();
        scalespaceTimer.stopTimer();
          
        PerformanceTimer orientationTimer = new PerformanceTimer();
        orientationTimer.startTimer();   
        
        orientationTimer.stopTimer();
         
        ArrayList<LBPScaleBias> scaleBiases = new ArrayList<>();
        for(Integer key : this.uniqueScaleBiases.keySet()) {
            LBPScaleBias tmp = this.uniqueScaleBiases.get(key);
            if(tmp != null) {
                scaleBiases.add(tmp);
            }
        }
        AffineScaleLBPBiasedFeatures features
                                     = new AffineScaleLBPBiasedFeatures(this.myParameters.useFeatureScaleSelection());
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());

        PerformanceTimer lbpTimer = new PerformanceTimer();
        lbpTimer.startTimer();
        
        OrientationEstimation defaultOrientation = new OrientationEstimation();
        defaultOrientation.setDominantOrientation(Double.NaN);
        
        for(LBPScaleBias curScaleBias : scaleBiases) {
            if(localScaleEstimate.isValid() == false) {
                break;
            }
            this.extractOrientationCompensatedFeaturesForScaleBias(content, filteredDataSupplier, curScaleBias, 
                                           localScaleEstimate, defaultOrientation, 
                                           0, 1, subMethod, features);  
        }
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(1);
        
        this.extractScaleAndOrientationAdaptiveFallbackFeatures(content,
                                                                subMethod,
                                                                filteredDataSupplier,
                                                                features,
                                                                defaultOrientation,
                                                                0,
                                                                1,
                                                                1);
        lbpTimer.stopTimer();
        allTimer.stopTimer();
        features.totalComputationTime = allTimer.getSeconds();
        features.featureComputationTime = lbpTimer.getSeconds();
        features.scalespaceAndEstimationTime = scalespaceTimer.getSeconds();
        features.orientationTime = orientationTimer.getSeconds();
        
        return features;

    }
    
    /**
     * Feature Extraction for SELECTED_SCALE_BIAS mode, used in ICPR and ICASSP.
     * <p>
     * @param content
     * @param classIndex
     * <p>
     * @return
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    //<editor-fold desc="old salbp RC3">
 /*   public DiscriminativeFeatures extractTrainingFeaturesSSB(Frame content, int classIndex)
            throws JCeliacGenericException
    {
        LBPScaleFilteredDataSupplier filteredDataSupplier;
        FeatureExtractionParameters p;
        try {
            p = FeatureExtractionMethodFactory.createParametersByMethod(this.myParameters.getExtractionMethod().getStringValue());
        } catch(XMLParseException e) {
            throw new JCeliacGenericException(e);
        }
        IScaleAdaptiveFeatureExtractionMethod subMethod = (IScaleAdaptiveFeatureExtractionMethod) FeatureExtractionMethodFactory.createFeatureExtractionMethod(this.myParameters.getExtractionMethod(), p);
        subMethod.setScaleAdaptiveParameters(this.myParameters);
        filteredDataSupplier = subMethod.prepareDataSupplier(content);

        // now estimate the scales of the input data
        ScalespaceRepresentation scaleRep = new LaplacianScalespaceRepresentation(content.getGrayData(), this.scalespaceParameters);
        scaleRep.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_ABSOLUTE_SUM);
        ScaleEstimation localScaleEstimate = new ScaleEstimation(this.scalespaceParameters,
                                                                 scaleRep.getNormalizedLaplacianDataSupplier(),
                                                                 content.getWidth(), content.getHeight());

        localScaleEstimate.estimateGlobalScale();
        ArrayList<LBPScaleBias> scaleBiases
                                = this.identifyBestScaleBiasForTraining(localScaleEstimate, this.scaleBiasesPerClass.get(classIndex));

        AffineScaleLBPBiasedFeatures features
                                     = new AffineScaleLBPBiasedFeatures(this.myParameters.useFeatureScaleSelection());
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());


        for(LBPScaleBias curScaleBias : scaleBiases) {
            if(localScaleEstimate.isValid() == false) {
                break;
            }
            AffineLBPFeatures tmp = new AffineLBPFeatures(super.parameters,
                                                          localScaleEstimate, curScaleBias);
            tmp.setSignalIdentifier(content.getSignalIdentifier());
            tmp.setFrameNumber(content.getFrameIdentifier());
            LBPScaleBias localBias = new LBPScaleBias(this.scalespaceParameters, curScaleBias);

            LBPHistogram histogramLower, histogramMid, histogramUpper;
            localBias.setLBP_BASE_RADIUS(LOWER_RADIUS);
            histogramLower = subMethod.performScaleAdaptiveFeatureExtraction(filteredDataSupplier,
                                                                             localBias,
                                                                             localScaleEstimate,
                                                                             this.neighborLookupTable);

            localBias.setLBP_BASE_RADIUS(MID_RADIUS);
            histogramMid = subMethod.performScaleAdaptiveFeatureExtraction(filteredDataSupplier,
                                                                           localBias,
                                                                           localScaleEstimate,
                                                                           this.neighborLookupTable);

            localBias.setLBP_BASE_RADIUS(UPPER_RADIUS);
            histogramUpper = subMethod.performScaleAdaptiveFeatureExtraction(filteredDataSupplier,
                                                                             localBias,
                                                                             localScaleEstimate,
                                                                             this.neighborLookupTable);
            tmp.addAbstractFeature(histogramLower);
            tmp.addAbstractFeature(histogramMid);
            tmp.addAbstractFeature(histogramUpper);
            features.addFeature(tmp);
               
        }
       features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(1);
        this.extractScaleAdaptiveFallbackFeatures(content,
                                                  subMethod,
                                                  filteredDataSupplier,
                                                  features,
                                                  1);*/
       /* this.extractScaleAdaptiveFallbackFeatures(content,
                                                  subMethod,
                                                  filteredDataSupplier,
                                                  features,
                                                  2);*/
     /*   return features;
    }*/
    //</editor-fold>

        /**
     * Feature Extraction for SELECTED_SCALE_BIAS mode, used in ICPR and ICASSP.
     * <p>
     * @param content
     * <p>
     * @return
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    //<editor-fold desc="older salbp (RC3+)">
 /*   public DiscriminativeFeatures extractFeaturesSSB(Frame content)
            throws JCeliacGenericException
    {
        PerformanceTimer allTimer = new PerformanceTimer();
        allTimer.startTimer();
        
        LBPScaleFilteredDataSupplier filteredDataSupplier;
        FeatureExtractionParameters p;
        try {
            p = FeatureExtractionMethodFactory.createParametersByMethod(this.myParameters.getExtractionMethod().getStringValue());
        } catch(XMLParseException e) {
            throw new JCeliacGenericException(e);
        }
        IScaleAdaptiveFeatureExtractionMethod subMethod = (IScaleAdaptiveFeatureExtractionMethod) FeatureExtractionMethodFactory.createFeatureExtractionMethod(this.myParameters.getExtractionMethod(), p);
        subMethod.setScaleAdaptiveParameters(this.myParameters);
        filteredDataSupplier = subMethod.prepareDataSupplier(content);
        
        PerformanceTimer scalespaceAndEstimation = new PerformanceTimer();
        scalespaceAndEstimation.startTimer();
        
        // now estimate the scales of the input data
        ScalespaceRepresentation scaleRep = new LaplacianScalespaceRepresentation(content.getGrayData(), this.scalespaceParameters);
        scaleRep.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_ABSOLUTE_SUM);
        ScaleEstimation localScaleEstimate = new ScaleEstimation(this.scalespaceParameters,
                                                                 scaleRep.getNormalizedLaplacianDataSupplier(),
                                                                 content.getWidth(), content.getHeight());

        localScaleEstimate.estimateGlobalScale();
        scalespaceAndEstimation.stopTimer();
        
        PerformanceTimer lbpTimer = new PerformanceTimer();
        lbpTimer.startTimer();
        ArrayList<LBPScaleBias> scaleBiases = new ArrayList<>();
        for(Integer key : this.uniqueScaleBiases.keySet()) {
            LBPScaleBias tmp = this.uniqueScaleBiases.get(key);
            if(tmp != null) {
                scaleBiases.add(tmp);
            }
        }
        AffineScaleLBPBiasedFeatures features
                                     = new AffineScaleLBPBiasedFeatures(this.myParameters.useFeatureScaleSelection());
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());

        for(LBPScaleBias curScaleBias : scaleBiases) {
            if(localScaleEstimate.isValid() == false) {
                break;
            }
            AffineLBPFeatures tmp = new AffineLBPFeatures(super.parameters,
                                                          localScaleEstimate, curScaleBias);
            tmp.setSignalIdentifier(content.getSignalIdentifier());
            tmp.setFrameNumber(content.getFrameIdentifier());

            LBPScaleBias localBias = new LBPScaleBias(this.scalespaceParameters, curScaleBias);
            LBPHistogram histogramLower, histogramMid, histogramUpper;
                localBias.setLBP_BASE_RADIUS(LOWER_RADIUS);
                histogramLower = subMethod.performScaleAdaptiveFeatureExtraction(filteredDataSupplier,
                                                                                 localBias,
                                                                                 localScaleEstimate,
                                                                                 this.neighborLookupTable);

                localBias.setLBP_BASE_RADIUS(MID_RADIUS);
                histogramMid = subMethod.performScaleAdaptiveFeatureExtraction(filteredDataSupplier,
                                                                               localBias,
                                                                               localScaleEstimate,
                                                                               this.neighborLookupTable);

                localBias.setLBP_BASE_RADIUS(UPPER_RADIUS);
                histogramUpper = subMethod.performScaleAdaptiveFeatureExtraction(filteredDataSupplier,
                                                                                 localBias,
                                                                                 localScaleEstimate,
                                                                                 this.neighborLookupTable);
                tmp.addAbstractFeature(histogramLower);
                tmp.addAbstractFeature(histogramMid);
                tmp.addAbstractFeature(histogramUpper);
                features.addFeature(tmp);
        }
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(1);
        this.extractScaleAdaptiveFallbackFeatures(content,
                                                  subMethod,
                                                  filteredDataSupplier,
                                                  features,
                                                  1);
      /*  this.extractScaleAdaptiveFallbackFeatures(content,
                                                  subMethod,
                                                  filteredDataSupplier,
                                                  features,
                                                  2);*/
       /* lbpTimer.stopTimer();
        allTimer.stopTimer();
        
        features.scalespaceAndEstimationTime = scalespaceAndEstimation.getSeconds();
        features.featureComputationTime = lbpTimer.getSeconds();
        features.totalComputationTime = allTimer.getSeconds();
        return features;
    }*/
    //</editor-fold>
    
    private final ConcurrentHashMap <String, OrientationEstimation> orientationsPerTrainingImage = 
                                        new  ConcurrentHashMap<>();

    
    private void extractOrientationCompensatedFeaturesForScaleBias(Frame content,
                                           LBPScaleFilteredDataSupplier filteredDataSupplier,
                                           LBPScaleBias curScaleBias,
                                           ScaleEstimation localScaleEstimate,
                                           OrientationEstimation orientationEstimation,
                                           double orientationDelta,
                                           double orientationStep,
                                           IScaleAdaptiveFeatureExtractionMethod subMethod,
                                           AffineScaleLBPBiasedFeatures features)
            throws JCeliacGenericException
    {

        LBPScaleBias localBias = new LBPScaleBias(this.scalespaceParameters, curScaleBias);

        localBias.setLBP_BASE_RADIUS(LOWER_RADIUS);
        ArrayList<LBPHistogram> orientedFeaturesLower
                                = subMethod.performScaleAndOrientationAdaptiveFeatureExtraction(filteredDataSupplier,
                                                                                                localBias,
                                                                                                localScaleEstimate,
                                                                                                orientationEstimation,
                                                                                                this.neighborLookupTable,
                                                                                                orientationDelta,
                                                                                                orientationStep,
                                                                                                this.ellipseLookupTable);

        localBias.setLBP_BASE_RADIUS(MID_RADIUS);
        ArrayList<LBPHistogram> orientedFeaturesMid
                                = subMethod.performScaleAndOrientationAdaptiveFeatureExtraction(filteredDataSupplier,
                                                                                                localBias,
                                                                                                localScaleEstimate,
                                                                                                orientationEstimation,
                                                                                                this.neighborLookupTable,
                                                                                                orientationDelta,
                                                                                                orientationStep,
                                                                                                this.ellipseLookupTable);

        localBias.setLBP_BASE_RADIUS(UPPER_RADIUS);
        ArrayList<LBPHistogram> orientedFeaturesUpper
                                = subMethod.performScaleAndOrientationAdaptiveFeatureExtraction(filteredDataSupplier,
                                                                                                localBias,
                                                                                                localScaleEstimate,
                                                                                                orientationEstimation,
                                                                                                this.neighborLookupTable,
                                                                                                orientationDelta,
                                                                                                orientationStep,
                                                                                                this.ellipseLookupTable);

        RotationAdaptiveLBPFeatures rotFeatures = new RotationAdaptiveLBPFeatures(this.myParameters,
                                                                                  localScaleEstimate,
                                                                                  localBias);
        rotFeatures.setSignalIdentifier(content.getSignalIdentifier());
        for(int index = 0; index < orientedFeaturesLower.size(); index++) {
            LBPFeatures featureForAngleOffset = new LBPFeatures(this.myParameters);

            featureForAngleOffset.addAbstractFeature(orientedFeaturesLower.get(index));
            featureForAngleOffset.addAbstractFeature(orientedFeaturesMid.get(index));
            featureForAngleOffset.addAbstractFeature(orientedFeaturesUpper.get(index));
            featureForAngleOffset.setSignalIdentifier(content.getSignalIdentifier());

            rotFeatures.addRotatedFeature(featureForAngleOffset);
        }
        features.addFeature(rotFeatures);
    }
    
    
    public DiscriminativeFeatures extractSOALBPTrainingFeaturesSSB(Frame content, int classIndex)
            throws JCeliacGenericException
    {

        LBPScaleFilteredDataSupplier filteredDataSupplier;
        FeatureExtractionParameters p;
        try {
            p = FeatureExtractionMethodFactory.createParametersByMethod(this.myParameters.getExtractionMethod().getStringValue());
        } catch(XMLParseException e) {
            throw new JCeliacGenericException(e);
        }
        IScaleAdaptiveFeatureExtractionMethod subMethod = (IScaleAdaptiveFeatureExtractionMethod) FeatureExtractionMethodFactory.createFeatureExtractionMethod(this.myParameters.getExtractionMethod(), p);
        subMethod.setScaleAdaptiveParameters(this.myParameters);
        filteredDataSupplier = subMethod.prepareDataSupplier(content);

        // now estimate the scales of the input data
        ScalespaceRepresentation scaleRep = new LaplacianScalespaceRepresentation(content.getGrayData(), this.scalespaceParameters);
        scaleRep.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_ABSOLUTE_SUM);
        ScaleEstimation localScaleEstimate = new ScaleEstimation(this.scalespaceParameters,
                                                                 scaleRep.getNormalizedLaplacianDataSupplier(),
                                                                 content.getWidth(), content.getHeight());
        localScaleEstimate.estimateGlobalScale();
      
        OrientationEstimation localOrientationEstimate = null;
        if(this.myParameters.isAdaptOrientation()) 
        {
            localOrientationEstimate = new OrientationEstimation();
            if(localScaleEstimate.isValid() == false) {
                localOrientationEstimate.estimateGlobalOrientation(scaleRep,
                                                                   localScaleEstimate.getEstimatedScaleLevel());
            }else {
                localOrientationEstimate.estimateGlobalOrientation(scaleRep,
                                                                   this.defaultScaleEstimation.getEstimatedScaleLevel());
            }
        }
        ArrayList<LBPScaleBias> scaleBiases
                                = this.identifyBestScaleBiasForTraining(localScaleEstimate, this.scaleBiasesPerClass.get(classIndex));

        AffineScaleLBPBiasedFeatures features
                                     = new AffineScaleLBPBiasedFeatures(this.myParameters.useFeatureScaleSelection());
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());
        
        OrientationEstimation defaultOrientation = new OrientationEstimation();
        defaultOrientation.setDominantOrientation(Double.NaN);
        for(LBPScaleBias curScaleBias : scaleBiases) 
        {
            if(localScaleEstimate.isValid() == false) {
                break;
            }
          // double orientationDelta = localOrientationEstimate.getOrientationDelta();
           double orientationDelta = 2*ORIENTATION_STEP;
           this.extractOrientationCompensatedFeaturesForScaleBias(content, filteredDataSupplier, curScaleBias, 
                                           localScaleEstimate, localOrientationEstimate, 
                                           orientationDelta, ORIENTATION_STEP, subMethod, features);
            
        }
        //double orientationDelta = localOrientationEstimate.getOrientationDelta();
        double orientationDelta = 2*ORIENTATION_STEP;
        this.extractScaleAndOrientationAdaptiveFallbackFeatures(content,
                                                                subMethod,
                                                                filteredDataSupplier,
                                                                features,
                                                                localOrientationEstimate,
                                                                orientationDelta,
                                                                ORIENTATION_STEP,
                                                                2);
        
        return features;
    }
    

        
  
    
    public DiscriminativeFeatures extractSOALBPFeaturesSSB(Frame content)
            throws JCeliacGenericException
    {
        PerformanceTimer allTimer = new PerformanceTimer();
        allTimer.startTimer();
        
        LBPScaleFilteredDataSupplier filteredDataSupplier;
        FeatureExtractionParameters p;
        try {
            p = FeatureExtractionMethodFactory.createParametersByMethod(this.myParameters.getExtractionMethod().getStringValue());
        } catch(XMLParseException e) {
            throw new JCeliacGenericException(e);
        }
        IScaleAdaptiveFeatureExtractionMethod subMethod = (IScaleAdaptiveFeatureExtractionMethod) FeatureExtractionMethodFactory.createFeatureExtractionMethod(this.myParameters.getExtractionMethod(), p);
        subMethod.setScaleAdaptiveParameters(this.myParameters);
        filteredDataSupplier = subMethod.prepareDataSupplier(content);

        PerformanceTimer scalespaceTimer = new PerformanceTimer();
        scalespaceTimer.startTimer();
        
        // now estimate the scales of the input data
        ScalespaceRepresentation scaleRep = new LaplacianScalespaceRepresentation(content.getGrayData(), this.scalespaceParameters);
        scaleRep.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_ABSOLUTE_SUM);
        ScaleEstimation localScaleEstimate = new ScaleEstimation(this.scalespaceParameters,
                                                                 scaleRep.getNormalizedLaplacianDataSupplier(),
                                                                 content.getWidth(), content.getHeight());

        localScaleEstimate.estimateGlobalScale();
        scalespaceTimer.stopTimer();
          
        PerformanceTimer orientationTimer = new PerformanceTimer();
        orientationTimer.startTimer();   
        
        OrientationEstimation localOrientationEstimate = null;
        if(this.myParameters.isAdaptOrientation()) 
        {
            localOrientationEstimate = new OrientationEstimation();
            if(localScaleEstimate.isValid() == false) {
                localOrientationEstimate.estimateGlobalOrientation(scaleRep,
                                                                   localScaleEstimate.getEstimatedScaleLevel());
            }else {
                localOrientationEstimate.estimateGlobalOrientation(scaleRep,
                                                                   this.defaultScaleEstimation.getEstimatedScaleLevel());
            }
        }
        orientationTimer.stopTimer();
         
        ArrayList<LBPScaleBias> scaleBiases = new ArrayList<>();
        for(Integer key : this.uniqueScaleBiases.keySet()) {
            LBPScaleBias tmp = this.uniqueScaleBiases.get(key);
            if(tmp != null) {
                scaleBiases.add(tmp);
            }
        }
        AffineScaleLBPBiasedFeatures features
                                     = new AffineScaleLBPBiasedFeatures(this.myParameters.useFeatureScaleSelection());
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());

        PerformanceTimer lbpTimer = new PerformanceTimer();
        lbpTimer.startTimer();
        
        OrientationEstimation defaultOrientation = new OrientationEstimation();
        defaultOrientation.setDominantOrientation(Double.NaN);
        
        for(LBPScaleBias curScaleBias : scaleBiases) {
            if(localScaleEstimate.isValid() == false) {
               break;
            }
           // double orientationDelta = localOrientationEstimate.getOrientationDelta();
            double orientationDelta = 2*ORIENTATION_STEP;
            this.extractOrientationCompensatedFeaturesForScaleBias(content, filteredDataSupplier, curScaleBias, 
                                           localScaleEstimate, localOrientationEstimate, 
                                           orientationDelta, ORIENTATION_STEP, subMethod, features);
        }
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(1);
        
       // double orientationDelta = localOrientationEstimate.getOrientationDelta();
        double orientationDelta = 2*ORIENTATION_STEP;
        this.extractScaleAndOrientationAdaptiveFallbackFeatures(content,
                                                                subMethod,
                                                                filteredDataSupplier,
                                                                features,
                                                                localOrientationEstimate,
                                                                orientationDelta,
                                                                ORIENTATION_STEP,
                                                                2);
        lbpTimer.stopTimer();
        allTimer.stopTimer();
        features.totalComputationTime = allTimer.getSeconds();
        features.featureComputationTime = lbpTimer.getSeconds();
        features.scalespaceAndEstimationTime = scalespaceTimer.getSeconds();
        features.orientationTime = orientationTimer.getSeconds();
        
        return features;

    }
    
      private String fixFilename(String filename)
    {
        String fixedFilename = "";
        int scaleIndex = filename.lastIndexOf("-scale_");
        int index = filename.lastIndexOf("/");
        String tmp = filename.substring(index+1,scaleIndex);
       
        fixedFilename = tmp + filename.substring(scaleIndex+9);
        return fixedFilename;
    }
    private ArrayList <Double> errors = new ArrayList();

    public void extractScaleAndOrientationAdaptiveFallbackFeatures(Frame content,
                                                                   IScaleAdaptiveFeatureExtractionMethod subMethod,
                                                                   LBPScaleFilteredDataSupplier filteredDataSupplier,
                                                                   AffineScaleLBPBiasedFeatures features,
                                                                   OrientationEstimation localOrientationEstimation,
                                                                   double orientationDelta,
                                                                   double orientationStep,
                                                                   int scale)
            throws JCeliacGenericException
    {
        
        
        LBPScaleBias referenceScaleBias = null;
        switch(scale) {
            case 1: referenceScaleBias = this.defaultScaleBias1; break;
            case 2: referenceScaleBias = this.defaultScaleBias2; break;
            case 3: referenceScaleBias = this.defaultScaleBias3; break;
        }

        LBPScaleBias localBias1 = new LBPScaleBias(this.scalespaceParameters, referenceScaleBias);
        localBias1.isDefaultScaleBias = true;
        localBias1.setLBP_BASE_RADIUS(LBPMultiscale.getRadiusForScale(scale));

        ArrayList<LBPHistogram> rotatedFeatures
        = subMethod.performScaleAndOrientationAdaptiveFeatureExtraction(filteredDataSupplier,
                                                                        localBias1,
                                                                        this.defaultScaleEstimation,
                                                                        localOrientationEstimation,
                                                                        this.neighborLookupTable,
                                                                        orientationDelta,
                                                                        orientationStep,
                                                                        this.ellipseLookupTable);
        RotationAdaptiveLBPFeatures rotFeatures = new RotationAdaptiveLBPFeatures(this.myParameters,
                                                                                  this.defaultScaleEstimation,
                                                                                  localBias1);
        rotFeatures.setSignalIdentifier(content.getSignalIdentifier());
        for(int index = 0; index < rotatedFeatures.size(); index++) {
            LBPFeatures featureForAngleOffset = new LBPFeatures(this.myParameters);

            switch(scale) {
                case 1:
                    featureForAngleOffset.addAbstractFeature(rotatedFeatures.get(index));
                    featureForAngleOffset.addAbstractFeature(new LBPHistogram(8));
                    featureForAngleOffset.addAbstractFeature(new LBPHistogram(8));
                    break;
                case 2:
                    featureForAngleOffset.addAbstractFeature(new LBPHistogram(8));
                    featureForAngleOffset.addAbstractFeature(rotatedFeatures.get(index));
                    featureForAngleOffset.addAbstractFeature(new LBPHistogram(8));
                    break;
                case 3:
                    featureForAngleOffset.addAbstractFeature(new LBPHistogram(8));
                    featureForAngleOffset.addAbstractFeature(new LBPHistogram(8));
                    featureForAngleOffset.addAbstractFeature(rotatedFeatures.get(index));
                    break;
            }
            featureForAngleOffset.setSignalIdentifier(content.getSignalIdentifier());
            rotFeatures.addRotatedFeature(featureForAngleOffset);
        }
        features.addFeature(rotFeatures);
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(1);
    }
        
    public void extractScaleAdaptiveFallbackFeatures(Frame content,
                                        IScaleAdaptiveFeatureExtractionMethod subMethod,
                                        LBPScaleFilteredDataSupplier filteredDataSupplier,
                                        AffineScaleLBPBiasedFeatures features,
                                        int scale)
            throws JCeliacGenericException
    {
        // first fallback
        AffineLBPFeatures tmp = null;

        LBPHistogram histogram = new LBPHistogram();
        double[] emptyData1 = new double[256];
        histogram.setData(emptyData1);

        LBPHistogram histogramEmpty1 = new LBPHistogram();
        double[] emptyData2 = new double[256];
        histogramEmpty1.setData(emptyData2);

        LBPHistogram histogramEmpty2 = new LBPHistogram();
        double[] emptyData3 = new double[256];
        histogramEmpty2.setData(emptyData3);

        LBPScaleBias referenceScaleBias = null;
        switch(scale) {
            case 1: referenceScaleBias = this.defaultScaleBias1; break;
            case 2: referenceScaleBias = this.defaultScaleBias2; break;
            case 3: referenceScaleBias = this.defaultScaleBias3; break;
        }
        
        
        tmp = new AffineLBPFeatures(super.parameters, this.defaultScaleEstimation, referenceScaleBias);
        LBPScaleBias localBias1 = new LBPScaleBias(this.scalespaceParameters, referenceScaleBias);
        localBias1.isDefaultScaleBias = true;
        localBias1.setLBP_BASE_RADIUS(LBPMultiscale.getRadiusForScale(scale));

        histogram = subMethod.performScaleAdaptiveFeatureExtraction(filteredDataSupplier,
                                                                     localBias1,
                                                                     this.defaultScaleEstimation,
                                                                     this.neighborLookupTable);
        
        switch(scale) {
            case 1:
                tmp.addAbstractFeature(histogram);
                tmp.addAbstractFeature(histogramEmpty1);
                tmp.addAbstractFeature(histogramEmpty2);
                break;
            case 2:
                tmp.addAbstractFeature(histogramEmpty1);
                tmp.addAbstractFeature(histogram);
                tmp.addAbstractFeature(histogramEmpty2);
                break;
            case 3:
                tmp.addAbstractFeature(histogramEmpty1);
                tmp.addAbstractFeature(histogramEmpty2);
                tmp.addAbstractFeature(histogram);
                break;  
        }
        
        tmp.setSignalIdentifier(content.getSignalIdentifier());
        tmp.setFrameNumber(content.getFrameIdentifier());
        features.addFeature(tmp);
    }

    private ArrayList<LBPScaleBias> identifyBestScaleBiasForTraining(ScaleEstimation scaleEstimation,
                                                                     ArrayList<LBPScaleBias> scaleBiasesOfClass)
    {
        if(scaleEstimation.isValid() == false) {
            return scaleBiasesOfClass;
        }
        int estLevel = scaleEstimation.getEstimatedScaleLevel();
        ArrayList<LBPScaleBias> bestScaleBiases = new ArrayList<>();
        int closest = Integer.MAX_VALUE;

        for(LBPScaleBias scaleBias : scaleBiasesOfClass) {
            int sbLevel = scaleBias.getClassScaleEstimation().getEstimatedScaleLevel();

            int diff = Math.abs(sbLevel - estLevel);
            if(diff < closest) {
                closest = diff;
            }
        }
        for(LBPScaleBias scaleBias : scaleBiasesOfClass) {
            int sbLevel = scaleBias.getClassScaleEstimation().getEstimatedScaleLevel();
            int diff = Math.abs(sbLevel - estLevel);
            if(diff <= closest) {
                bestScaleBiases.add(scaleBias);
            }
        }
        return bestScaleBiases;
    }

    private void prepareScaleBiases()
    {
        for(int classIndex : this.scaleEstimationPerClass.keySet()) {
            ArrayList<ScaleEstimation> classDominantScales
                                       = this.scaleEstimationPerClass.get(classIndex);
            ArrayList<LBPScaleBias> scaleBiasesOfClass = new ArrayList<>();

            for(ScaleEstimation curScale : classDominantScales) {
                int scaleBiasScale = curScale.getEstimatedScaleLevel();
                LBPScaleBias scaleBias;
                if(this.uniqueScaleBiases.containsKey(scaleBiasScale) == false) {
                    scaleBias = new LBPScaleBias(this.scalespaceParameters, scaleBiasScale,
                                                 LBPScaleBias.retrieveUniqueID(), curScale);
                    scaleBias.classConfidence = curScale.scaleBiasConfidence;
                    this.uniqueScaleBiases.put(scaleBiasScale, scaleBias);
                } else {
                    scaleBias = this.uniqueScaleBiases.get(scaleBiasScale);
                }
                scaleBiasesOfClass.add(scaleBias);
            }
            this.scaleBiasesPerClass.put(classIndex, scaleBiasesOfClass);
        }
        this.defaultScaleEstimation = new ScaleEstimation(this.scalespaceParameters, this.scalespaceParameters.getNumberOfScales() / 2);
        this.defaultScaleBias1 = new LBPScaleBias(this.scalespaceParameters, this.scalespaceParameters.getNumberOfScales() / 2,
                                                  LBPScaleBias.retrieveUniqueID(), this.defaultScaleEstimation);
        this.defaultScaleBias1.isDefaultScaleBias = true;
        this.defaultScaleBias2 = new LBPScaleBias(this.scalespaceParameters, this.scalespaceParameters.getNumberOfScales() / 2,
                                                  LBPScaleBias.retrieveUniqueID(), this.defaultScaleEstimation);
        this.defaultScaleBias2.isDefaultScaleBias = true;
        this.defaultScaleBias3 = new LBPScaleBias(this.scalespaceParameters, this.scalespaceParameters.getNumberOfScales() / 2,
                                                  LBPScaleBias.retrieveUniqueID(), this.defaultScaleEstimation);
        this.defaultScaleBias3.isDefaultScaleBias = true;
    }

    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass)
            throws JCeliacGenericException
    {
        Experiment.log(JCeliacLogger.LOG_INFO, "Estimating training class scales using %d scale levels.",
                       this.scalespaceParameters.getNumberOfScales());
        PerformanceTimer timer = new PerformanceTimer();
        timer.startTimer();
        // estimate scales for training textures
        ScalespaceScaleAndShapeEstimator scaleAndShapeEstimator = new ScalespaceScaleAndShapeEstimator(this.scalespaceParameters);
        scaleAndShapeEstimator.estimateScalesByClass(readersByClass);

        this.scaleEstimationPerClass = scaleAndShapeEstimator.reduceScaleEstimations(this.myParameters.isAllowMultipleScalesPerClass());
        this.prepareScaleBiases();
        timer.stopTimer();
        Experiment.log(JCeliacLogger.LOG_INFO, "Scales of images in %d classes estimated in %.2f seconds.",
                       this.scaleBiasesPerClass.size(), timer.getSeconds());
    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {
        // nothing to do
    }
    
    // used for performance experiments to add a known number of scale biases
    public void createArtificialScaleBiases(int numScaleBiases)
    {
        this.uniqueScaleBiases.clear();
        for(int i = 0; i < numScaleBiases; i++) {
            ScaleEstimation scaleEstimation = new ScaleEstimation(this.scalespaceParameters, i);
            LBPScaleBias scaleBias = new LBPScaleBias(this.scalespaceParameters, i,
                                                 LBPScaleBias.retrieveUniqueID(), scaleEstimation);
            
            this.uniqueScaleBiases.put(i, scaleBias);
        }
        this.defaultScaleEstimation = new ScaleEstimation(this.scalespaceParameters, this.scalespaceParameters.getNumberOfScales() / 2);
        this.defaultScaleBias1 = new LBPScaleBias(this.scalespaceParameters, this.scalespaceParameters.getNumberOfScales() / 2,
                                                  LBPScaleBias.retrieveUniqueID(), this.defaultScaleEstimation);
        this.defaultScaleBias1.isDefaultScaleBias = true;
        this.defaultScaleBias2 = new LBPScaleBias(this.scalespaceParameters, this.scalespaceParameters.getNumberOfScales() / 2,
                                                  LBPScaleBias.retrieveUniqueID(), this.defaultScaleEstimation);
        this.defaultScaleBias2.isDefaultScaleBias = true;
        this.defaultScaleBias3 = new LBPScaleBias(this.scalespaceParameters, this.scalespaceParameters.getNumberOfScales() / 2,
                                                  LBPScaleBias.retrieveUniqueID(), this.defaultScaleEstimation);
        this.defaultScaleBias3.isDefaultScaleBias = true;
    }
    
    // used for performance experiment
    public void __overrideScalespaceParameters(ScalespaceParameters scalespaceParameters)
    {
        this.scalespaceParameters = scalespaceParameters;
    }
}
