/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.SALBP;


import jceliac.featureOptimization.FeatureVectorSubsetEncoding;
import jceliac.featureExtraction.LBP.LBPFeatures;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.JCeliacGenericException;
import java.util.ArrayList;
import java.util.Random;
import jceliac.classification.kernelSVM.SVMFeatureVector;
import jceliac.featureExtraction.affineScaleLBPScaleBias.DistanceStats;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ScaleEstimation;

/**
 * This feature abstraction collects multiple AffineLBPDiscriminativeFeatures, the best
 * matching pair of features with correct scale-bias defines the distance
 * between two AffineScaleLBPBiasedDiscriminativeFeatures.
 * <p>
 * <p>
 * @author shegen
 */
public class AffineScaleLBPBiasedFeatures
        extends LBPFeatures
{

    private final ArrayList<AffineLBPFeatures> features;
    private final static Random rand = new Random(123456789);
    public boolean isTrainingFeature; 
    private boolean useFeatureScaleSelection;
    public boolean invalidTrainingFeature = false;
   
    public static ArrayList <DistanceStats> distanceStats = new ArrayList <>();
    
    public AffineScaleLBPBiasedFeatures(boolean useFeatureScaleSelection)
    {
        super();
        this.features = new ArrayList<>();
        this.useFeatureScaleSelection = useFeatureScaleSelection;
    }
    

    @Override
    public DiscriminativeFeatures cloneFeatureSubset(FeatureVectorSubsetEncoding subset)
            throws JCeliacGenericException
    {
        AffineScaleLBPBiasedFeatures featureSubset = 
            new AffineScaleLBPBiasedFeatures(this.useFeatureScaleSelection);
        
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
        
        // copy the entire feature data
        FeatureVectorSubsetEncoding all = new FeatureVectorSubsetEncoding(subset.bitCount());
        all.setAll();
        for(AffineLBPFeatures fTmp : this.features) {
            featureSubset.addFeature((AffineLBPFeatures)fTmp.cloneFeatureSubset(all));
        }
        return featureSubset;
    }

    @Override
    public DiscriminativeFeatures cloneEmptyFeature()
    {
        AffineScaleLBPBiasedFeatures featureSubset = 
                    new AffineScaleLBPBiasedFeatures(this.useFeatureScaleSelection);
        
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
        
        return featureSubset;
    }
    
    public void addFeature(AffineLBPFeatures feature)
    {
        this.features.add(feature);
    }

    public ArrayList<AffineLBPFeatures> getFeatures()
    {
        return this.features;
    }

    public void flagAsEvaluationFeature()
    {
        this.isTrainingFeature = false;
    }

    @Override
    public void flagAsTrainingFeature()
    {
        this.isTrainingFeature = true;
    }

    @Override
    public int getFeatureVectorDimensionality()
    {
        return this.features.get(0).getFeatureVectorDimensionality();
    }

    @Override
    public double distanceTo(DiscriminativeFeatures them)
            throws JCeliacGenericException
    {
        double distanceEllipse = this.computeDistances(them);
        return distanceEllipse;
    }

    
    private FeatureVectorSubsetEncoding computeBestSubsetForScaleDifference(AffineLBPFeatures feature1,
                                                                            AffineLBPFeatures feature2)
            throws JCeliacGenericException 
    {
        // create a copy of scale biases to avoid race condition when setting the base scale below!
        LBPScaleBias feature1ScaleBias = new LBPScaleBias(feature1.getScaleEstimation().getScalespaceParameters(), feature1.getScaleBias());
        LBPScaleBias feature2ScaleBias = new LBPScaleBias(feature2.getScaleEstimation().getScalespaceParameters(), feature2.getScaleBias());
        
        ScaleEstimation feature1ScaleEstimation = feature1.getScaleEstimation();
        ScaleEstimation feature2ScaleEstimation = feature2.getScaleEstimation();

        int scale1 = feature1.getScaleEstimation().getEstimatedScaleLevel();
        int scale2 = feature2.getScaleEstimation().getEstimatedScaleLevel();

        // find the best base radius for the scale bias such that the smallest sigma is
        // used for both features
        // XXX: this should not be hard coded later
        double[] baseScaleRadii = new double[]{1.5, 3, 4.5};

        
        //double lowerScaleRadius = LBPMultiscale.getRadiusForScale(1);
        //double midScaleRadius = LBPMultiscale.getRadiusForScale(2);
        //double upperScaleRadius = LBPMultiscale.getRadiusForScale(3);
        
        
        
      //  double[] baseScaleRadii = new double[]{lowerScaleRadius, midScaleRadius, upperScaleRadius};
        
        
        
        FeatureVectorSubsetEncoding encoding = new FeatureVectorSubsetEncoding(3);
        for (int i = 0; i < baseScaleRadii.length; i++) {
            feature1ScaleBias.setLBP_BASE_RADIUS(baseScaleRadii[i]);
            feature2ScaleBias.setLBP_BASE_RADIUS(baseScaleRadii[i]);
            double lbpRadius1 = feature1ScaleBias.getLBPRadiusForScaleLevel(feature1ScaleEstimation.getEstimatedScaleLevel());
            double lbpRadius2 = feature2ScaleBias.getLBPRadiusForScaleLevel(feature2ScaleEstimation.getEstimatedScaleLevel());
           
            if(lbpRadius1 < 1 || lbpRadius2 < 1) {
                continue;
            }
            double ratio = Math.max(lbpRadius1, lbpRadius2) / Math.min(lbpRadius1, lbpRadius2);
            if(ratio > 3) {
                continue;
            }
            double maxRadius = Math.max(lbpRadius1, lbpRadius2);
            double maxAllowedRadius = baseScaleRadii[2]; 
            maxAllowedRadius = maxAllowedRadius + 0.25*maxAllowedRadius; 
            if(maxRadius > maxAllowedRadius) {
                continue;
            }
            encoding.setFeatureIndex(i);
        }
        if(encoding.bitCount() == 0) {
            return null;
        }
        if(encoding.allSet()) {
            encoding.clearFeatureIndex(2);
        }
        return encoding;
    }

    
    private static synchronized void addDistanceStats(DistanceStats stats) 
    {
        AffineScaleLBPBiasedFeatures.distanceStats.add(stats);
    }

    /**
     * Finds the pair of sub-features with matching scale-bias with the smallest
     * distance and returns that distance.
     * <p>
     * @param them   Other AffineScaleLBPBiasedDiscriminativeFeatures collection.
     * @param subset Feature subset, currently unused.
     * @param type   Distance type.
     * <p>
     * @return Smallest distance between all pairs of sub-features.
     * <p>
     * @throws JCeliacGenericException
     */
    private double computeDistances(DiscriminativeFeatures them)
            throws JCeliacGenericException
    {
        ArrayList <Double> candidates = new ArrayList <>();
        AffineScaleLBPBiasedFeatures themCasted = (AffineScaleLBPBiasedFeatures) them;

        double bestDistance = Double.NaN;

        // find best match within the scale bias options of the other feature
        ArrayList<AffineLBPFeatures> myFeatures = this.getFeatures();
        ArrayList<AffineLBPFeatures> themFeatures = themCasted.getFeatures();

        if(myFeatures == null || themFeatures == null) {
            return Double.POSITIVE_INFINITY;
        }

        // find best match between shared scales
        for(int i = 0; i < themFeatures.size(); i++) {
            for(int j = 0; j < myFeatures.size(); j++) {
                AffineLBPFeatures myFeature = myFeatures.get(j);
                AffineLBPFeatures themFeature = themFeatures.get(i);
                if(myFeature == null || themFeature == null) {
                    throw new JCeliacGenericException("Severe issue can not find matching scale biases between a pair of features!");
                }
                if(myFeature.comparableTo(themFeature) == false) {
                    continue;
                }
                AffineLBPFeatures myFeatureTmp, themFeatureTmp;
                double distance;
                
                if(this.useFeatureScaleSelection) {
                    FeatureVectorSubsetEncoding subsetEncoding = this.computeBestSubsetForScaleDifference(myFeature, themFeature);
                    if(subsetEncoding == null) {
                        continue;
                    }
                    myFeatureTmp = (AffineLBPFeatures)myFeature.getClonedFeatureSubset(subsetEncoding);
                    themFeatureTmp = (AffineLBPFeatures)themFeature.getClonedFeatureSubset(subsetEncoding);
                }else {
                    FeatureVectorSubsetEncoding subsetEncoding = new FeatureVectorSubsetEncoding(3);
                    subsetEncoding.setFeatureIndex(1);
                    
                    myFeatureTmp = (AffineLBPFeatures)myFeature.getClonedFeatureSubset(subsetEncoding);
                    themFeatureTmp = (AffineLBPFeatures)themFeature.getClonedFeatureSubset(subsetEncoding);
                }
                distance = myFeatureTmp.doDistanceTo(themFeatureTmp);
                candidates.add(distance);
                if(distance < bestDistance || Double.isNaN(bestDistance)) {
                    bestDistance = distance;
                }

            }
        }
        // if there was no valid neighbor, create a random distance which is bigger than 
        // all possible valid distances of the histogram interstection;
        // do this to avoid unwanted classification results due to ordering of the LabeledDistance map
        if(Double.isNaN(bestDistance)) {
        //    System.out.println("No valid neighbor, NaN!");
            bestDistance = Double.POSITIVE_INFINITY;
            return Double.POSITIVE_INFINITY;
        }
        if(candidates.isEmpty()) {
            return Double.POSITIVE_INFINITY;
        }
        return bestDistance;
       
    }

    private int computeSVMFeatureVectorDimension()
            throws JCeliacGenericException
    {
        int len = 0;
        for(AffineLBPFeatures tmpFeature : this.features) {
                len += tmpFeature.getAllFeatureVectorData().length;
        } 
        throw new JCeliacGenericException("check code, remove scale biases!");
       // return len;
    }
    
    @Override
    public SVMFeatureVector[] getNormalizedFeaturesForSVM()
            throws JCeliacGenericException
    {
        int svmDim = this.computeSVMFeatureVectorDimension();
        double [] rawSVMData = new double[svmDim];
        
          // fill the feature vector based on the scale biases
        int lenPerScaleBias = this.features.get(0).getAllFeatureVectorData().length;

        // fill raw data with random value
        double [] pad = new double[lenPerScaleBias];
        double sum = 0;
        for(int n = 0; n < pad.length; n++) {
            pad[n] = Math.random();
            sum += pad[n];
        }
        for(int n = 0; n < pad.length; n++) {
            pad[n] = pad[n] / sum;
        }
        throw new JCeliacGenericException("check code!");
      /*
        int globalN = 0;
        for(Integer scaleBiasKey : this.scaleBiases.keySet()) {
            ArrayList <LBPScaleBias> scaleBiasesOfClass = this.scaleBiases.get(scaleBiasKey);
            for(LBPScaleBias curScaleBias : scaleBiasesOfClass) {
                for(AffineLBPFeatures tmpFeature : this.features) 
                {
                    if(tmpFeature.getScaleBias().getId() == curScaleBias.getId()) {
                        double [] rawData = tmpFeature.getAllFeatureVectorData();

                        for(int n = 0; n < rawData.length; n++) {
                            rawSVMData[globalN  + n] = rawData[n];
                        }
                    }
                }
            }
            globalN += lenPerScaleBias;
        }
        */
      //  this.normalizeLinear(rawSVMData);
    /*    SVMFeatureVector[] svmVec = new SVMFeatureVector[svmDim];
        for(int n = 0; n < svmVec.length; n++) {
            svmVec[n] = new SVMFeatureVector();
            svmVec[n].index = n+1;
            svmVec[n].value = rawSVMData[n];
        }
        return svmVec;*/
    }
}
