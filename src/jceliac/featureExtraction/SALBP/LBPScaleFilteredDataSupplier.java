/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.SALBP;

import jceliac.tools.filter.LowpassFilter;
import jceliac.JCeliacGenericException;
import jceliac.tools.math.MathTools;
import jceliac.tools.data.DoubleKey;
import java.util.HashMap;

/**
 * Computes an adaptively filtered version of the data dependant on the
 * requested lbp radius.
 * <p>
 * @author shegen
 */
public class LBPScaleFilteredDataSupplier
{

    private final HashMap<DoubleKey, double[][]> scaleFilteredData;
    private final HashMap<DoubleKey, Double> scaleFilteredDataStandardDeviations;
    private final double[][] originalData;

    private int cacheHits = 0;
    private int cacheMisses = 0;
    private int totalQueries = 0;

    public LBPScaleFilteredDataSupplier(double[][] data)
    {
        this.scaleFilteredDataStandardDeviations = new HashMap<>();
        this.scaleFilteredData = new HashMap<>();
        this.originalData = data;
    }

    /**
     * Returns the correctly filtered data for a requested lbp radius, if
     * requested before the cached version is returned, if not a the data is
     * filtered accordingly.
     * <p>
     * @param lbpRadius Radius of the LBP.
     * <p>
     * @return Accordingly filtered data.
     * <p>
     * @throws JCeliacGenericException
     */
    public double[][] retrieveLBPScaleFilteredDataForLBPRadius(double lbpRadius)
            throws JCeliacGenericException
    {
        this.totalQueries++;

        DoubleKey key = new DoubleKey(lbpRadius);
        if(this.scaleFilteredData.containsKey(key)) {
            this.cacheHits++;
            return this.scaleFilteredData.get(key);
        } else {
            this.cacheMisses++;
            double[][] filteredData = this.computeLBPScaleFilteredData(lbpRadius);
            this.scaleFilteredData.put(key, filteredData);
            return filteredData;
        }
    }

    /**
     * Returns the standard deviation for data filtered with a certain LBP
     * radius.
     * <p>
     * @param lbpRadius Radius of the LBP.
     * <p>
     * @return Standard deviation of filtered data.
     * <p>
     * @throws JCeliacGenericException
     */
    public double getStdevForFilteredDataForLBPRadius(double lbpRadius)
            throws JCeliacGenericException
    {
        DoubleKey key = new DoubleKey(lbpRadius);
        if(this.scaleFilteredDataStandardDeviations.containsKey(key)) {
            return this.scaleFilteredDataStandardDeviations.get(key);
        } else {
            double[][] filteredData = this.scaleFilteredData.get(key);
            double stdev = MathTools.stdev(filteredData);
            this.scaleFilteredDataStandardDeviations.put(key, stdev);
            return stdev;
        }
    }
    
    private double originalDataStdev = -1;
    
    public double getStdevForUnfilteredData()
            throws JCeliacGenericException
    {
        if(this.originalDataStdev == -1) {
            this.originalDataStdev = MathTools.stdev(this.originalData);
        }
        return this.originalDataStdev;
    }
    

    /**
     * Filters the data such that the support area of each sample is equal to
     * the size of a pixel at scale 1,5.
     * <p>
     * @param lbpRadius Radius of LBP.
     * <p>
     * @throws JCeliacGenericException
     */
    private double[][] computeLBPScaleFilteredData(double lbpRadius)
            throws JCeliacGenericException
    {
        double sigma = computeSigmaForLBPRadius(lbpRadius);
        // find out most efficient filter width and filter the data
        int optimalFilterWidth = LowpassFilter.computeOptimalFilterSize(127, sigma);
        LowpassFilter filter = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, optimalFilterWidth, sigma);
        // filter separable, fft is not useful here because sigmas are small
        double[][] filteredData = filter.filterData(this.originalData);
        return filteredData;
    }

    /**
     * Computes the sigma for a Gaussian filter for a certain LBP radius, such
     * that 99 percent of the mass of the Gaussian is covered within the circle
     * corresonding to the sample radius for an LBP with radius lbpRadius.
     * <p>
     * @param lbpRadius Radius of LBP.
     * <p>
     * @return Sigma used for computing the Gaussian filter.
     */
    public static double computeSigmaForLBPRadius(double lbpRadius)
    {
        double lbpCircumference = 2 * lbpRadius * Math.PI;
        // find radius of sample points such that they area adjacent on a circle
        double sampleDiameter = lbpCircumference / (8.0d);
        // we now have the sample radius, which is interpreted as: a pixel in the 
        // image should have the size of the circle represented by the sampleRadius
        // after filtering

        // compute the sigma for a gaussian such that 99 percent of the area is covered 
        // within the circle with sampleRadius, so this means for a small sample radius
        // the gaussian will be just so small that it's the center pixel, for larger
        // sample radii it will be larger accomodating to scaling
        double sampleRadius = (sampleDiameter / 2);
        double sigma = sampleRadius / (Math.sqrt(2.0d) * MathTools.erfinv(0.99d));
        return sigma;
    }

    /**
     * Computes the sample radius for an LBP with radius lbpRadius.
     * <p>
     * @param lbpRadius Radius of LBP.
     * <p>
     * @return Sample radius of LBP.
     */
    public static double computeSampleRadiusForLBPRadius(double lbpRadius)
    {
        double lbpCircumference = 2 * lbpRadius * Math.PI;
        double sampleRadius = lbpCircumference / (8.0d);
        return sampleRadius;
    }

    /**
     * Prints statistics about the cache efficiency.
     */
    public void printDataSupplierStatistics()
    {
        System.out.println("Total Queries: " + this.totalQueries);
        System.out.println("Cache Hits: " + this.cacheHits);
        System.out.println("Cache Misses: " + this.cacheMisses);
    }

}
