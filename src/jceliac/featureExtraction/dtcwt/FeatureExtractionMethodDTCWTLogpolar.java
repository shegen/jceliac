/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.dtcwt;

import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FixedFeatureVector;
import jceliac.featureExtraction.StandardEuclideanFeatures;
import jceliac.tools.data.Frame;
import jceliac.tools.transforms.dtcwt.DualTreeComplexWaveletTransform;
import jceliac.tools.transforms.logpolar.LogpolarTransform;

/**
 *
 * @author shegen
 */
public class FeatureExtractionMethodDTCWTLogpolar
        extends FeatureExtractionMethodDTCWT

{

    protected FeatureExtractionParametersDTCWTLogpolar myParameters;

    public FeatureExtractionMethodDTCWTLogpolar(FeatureExtractionParameters param)
    {
        super(param);
        this.myParameters = (FeatureExtractionParametersDTCWTLogpolar) param;
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {

        StandardEuclideanFeatures features = new StandardEuclideanFeatures();

        if(this.myParameters.isUseColor()) {
            for(Integer colorChannel : this.myParameters.getColorChannels()) {
                DualTreeComplexWaveletTransform transform = new DualTreeComplexWaveletTransform(this.biort, this.qshift);
                double[][] dataLogpolar = LogpolarTransform.transformLogPolar(content.getColorChannel(colorChannel));
                transform.transformForward(dataLogpolar, this.myParameters.getNumScales());
                double[] featureData = (double[]) this.computeFeatures(transform.getDecompositionTree());
                features.addAbstractFeature(new FixedFeatureVector(featureData));
            }
        } else {
            double[][] grayDataLogpolar = LogpolarTransform.transformLogPolar(content.getGrayData());
            DualTreeComplexWaveletTransform transform = new DualTreeComplexWaveletTransform(this.biort, this.qshift);
            transform.transformForward(grayDataLogpolar, this.myParameters.getNumScales());
            double[] featureData = (double[]) this.computeFeatures(transform.getDecompositionTree());
            features.addAbstractFeature(new FixedFeatureVector(featureData));
        }
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(1);
        return features;
    }

}
