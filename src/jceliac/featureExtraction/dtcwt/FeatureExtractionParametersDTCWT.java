/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.featureExtraction.dtcwt;

import jceliac.experiment.Experiment;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersDTCWT extends FeatureExtractionParameters
{
    protected String featureType = "energy";

    public enum EDTCWTFeatureType {
        ENERGY,
        ENTROPY;
        
        public static EDTCWTFeatureType valueOfIgnoreCase(String s)
        {
            for(EDTCWTFeatureType type : EDTCWTFeatureType.values()) {
               if(type.name().equalsIgnoreCase(s)) {
                   return type;
               }
            }
            Experiment.log(JCeliacLogger.LOG_WARN, "Unknown DTCWT Feature Type, using ENERGY!");
            return ENERGY;
        }
    };
    protected int numScales = 6;

    public int getNumScales()
    {
        return numScales;
    }
    
    public EDTCWTFeatureType getDTCWTFeatureType()
    {
        return EDTCWTFeatureType.valueOfIgnoreCase(this.featureType);
    }
  
    
    @Override
    public void report()
    {
        super.report();
        Experiment.log(JCeliacLogger.LOG_INFO, "Number of scales: " + this.numScales);
        Experiment.log(JCeliacLogger.LOG_INFO, "Feature Type: " + this.featureType);
    }

    @Override
    public String getMethodName()
    {
        return "DTCWT Classic";
    }
}
