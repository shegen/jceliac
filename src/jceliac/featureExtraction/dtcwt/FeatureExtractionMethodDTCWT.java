/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.dtcwt;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.StandardEuclideanFeatures;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.FixedFeatureVector;
import jceliac.featureExtraction.StableCode;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.Frame;
import jceliac.tools.transforms.dtcwt.DualTreeComplexWaveletDecomposition;
import jceliac.tools.transforms.dtcwt.DualTreeComplexWaveletTransform;
import jceliac.tools.transforms.dtcwt.filter.BiorthogonalFilter;
import jceliac.tools.transforms.dtcwt.filter.BiorthogonalFilter_Near_Sym_A;
import jceliac.tools.transforms.dtcwt.filter.QShiftFilter;
import jceliac.tools.transforms.dtcwt.filter.QShiftFilter_QShift_B;
import jceliac.tools.math.MathTools;

/**
 *
 * @author shegen
 */
@StableCode(
        comment = "This code is trusted and computes the same feature vectors as gwimmer's matlab code.",
        date = "06.02.2014",
        lastModified = "06.02.2014")
public class FeatureExtractionMethodDTCWT
        extends FeatureExtractionMethod
{

    protected FeatureExtractionParametersDTCWT myParameters;
    protected BiorthogonalFilter biort = new BiorthogonalFilter_Near_Sym_A();
    protected QShiftFilter qshift = new QShiftFilter_QShift_B();

    public FeatureExtractionMethodDTCWT(FeatureExtractionParameters param)
    {
        super(param);
        this.myParameters = (FeatureExtractionParametersDTCWT) param;
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {
        StandardEuclideanFeatures features = new StandardEuclideanFeatures();

        if(this.myParameters.isUseColor()) {
            for(Integer colorChannel : this.myParameters.getColorChannels()) {
                DualTreeComplexWaveletTransform transform = new DualTreeComplexWaveletTransform(this.biort, this.qshift);
                transform.transformForward(content.getColorChannel(colorChannel), this.myParameters.getNumScales());
                double[] featureData = (double[]) this.computeFeatures(transform.getDecompositionTree());
                features.addAbstractFeature(new FixedFeatureVector(featureData));
            }
        } else {
            double[][] data = content.getGrayData();
            DualTreeComplexWaveletTransform transform = new DualTreeComplexWaveletTransform(this.biort, this.qshift);
            transform.transformForward(data, this.myParameters.getNumScales());
            double[] featureData = (double[]) this.computeFeatures(transform.getDecompositionTree());
            features.addAbstractFeature(new FixedFeatureVector(featureData));

        }
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(1);
        return features;
    }

    protected double[] computeFeatures(DualTreeComplexWaveletDecomposition decompositionTree)
    {
        double[] featureData = null;
        int index = 0;
        for(int scaleLevel = 1; scaleLevel <= this.myParameters.getNumScales(); scaleLevel++) {
            for(int subbandIndex = 1; subbandIndex <= 6; subbandIndex++) {
                double[][] subbandCoefficients = MathTools.complexAbs(decompositionTree.getComplexSubband(scaleLevel, subbandIndex));

                switch(this.myParameters.getDTCWTFeatureType()) {
                    case ENERGY:
                        if(featureData == null) {
                            featureData = new double[this.myParameters.getNumScales() * 6 * 2];
                        }
                        double mean = ArrayTools.mean(subbandCoefficients);
                        double std = MathTools.stdev(subbandCoefficients);
                        featureData[index] = mean;
                        index++;
                        featureData[index] = std;
                        index++;
                        break;

                    case ENTROPY:
                        if(featureData == null) {
                            featureData = new double[this.myParameters.getNumScales() * 6];
                        }
                        double entropy = MathTools.computeEntropy(subbandCoefficients);
                        featureData[index] = entropy;
                        index++;
                        break;
                }
            }
        }
        return featureData;
    }

    /**
     * This is called separately for training and test features. 
     * 
     * @param features
     * @return 
     * @throws JCeliacGenericException 
     */
    @Override
    public ArrayList <DiscriminativeFeatures> normalizeFeatures(ArrayList<DiscriminativeFeatures> features) 
            throws JCeliacGenericException
    {
        if(features.isEmpty()) {
            return features;
        }
        // the n-th feature vector entry of each feature builds a column 
        // vector and is normalized
        double [][] rawFeatures = new double[features.size()][];
        
        for(int featureIndex = 0; featureIndex < features.size(); featureIndex++) 
        {
            StandardEuclideanFeatures castedTmpFeatures = (StandardEuclideanFeatures) features.get(featureIndex);
            double [] rawFeatureData = castedTmpFeatures.getAllFeatureVectorData();
            rawFeatures[featureIndex] = rawFeatureData;
        }
        
        double [] nthFeatureColumn = new double[features.size()];
        for(int n = 0; n < rawFeatures[0].length; n++) {
            for(int featureIndex = 0; featureIndex < rawFeatures.length; featureIndex++) {
                nthFeatureColumn[featureIndex] = rawFeatures[featureIndex][n];
            }
            // compute mean and std of column
            double colMean = MathTools.mean(nthFeatureColumn);
            double colStd = MathTools.stdev(nthFeatureColumn);
            for(int i = 0; i < nthFeatureColumn.length; i++) {
                rawFeatures[i][n] =  (rawFeatures[i][n] - colMean) / colStd;
            }
        }
        ArrayList<DiscriminativeFeatures> normalizedFeatures = new ArrayList <>();
        // store normalized features back
        for(int featureIndex = 0; featureIndex < features.size(); featureIndex++) 
        {
            FixedFeatureVector vector = new FixedFeatureVector();
            vector.setData(rawFeatures[featureIndex]);
            StandardEuclideanFeatures normalizedFeature = (StandardEuclideanFeatures) features.get(featureIndex).cloneEmptyFeature();
            normalizedFeature.addAbstractFeature(vector);
            normalizedFeatures.add(normalizedFeature);
        }
        return normalizedFeatures; 
    }

    
    protected void normalizeDTCWTFeatureData(FixedFeatureVector vector)
            throws JCeliacGenericException
    {
        double [] featureData = vector.getFeatureData();
        double mean = MathTools.mean(featureData);
        double std = MathTools.stdev(featureData);
        
        double [] normalizedFeatureData = new double[featureData.length];
        for(int i = 0; i < featureData.length; i++) {
            normalizedFeatureData[i] = (featureData[i] - mean) / std;
        }
        vector.setData(normalizedFeatureData);
    }
    
    @Override
    public void initialize() throws JCeliacGenericException
    {

    }

    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass) throws
            JCeliacGenericException
    {

    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {

    }

}
