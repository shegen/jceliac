/*
 * 
 JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at> 
 The source code of this software is not yet made available to the open source 
 community. If you obtained this source code without permission of the author 
 please remove all the source files.

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 * Each line should be prefixed with  * 
 */

package jceliac.featureExtraction.dtcwt;

import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.StableCode;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.math.MathTools;

/**
 * Implementation of 
 * "Query by example using invariant features from the double dyadic dual-tree complex
 * wavelet transform." 
 * based on the matlab code of gwimmer/rkwitt. 
 * 
 * @author shegen
 */
@StableCode(
        comment = "This code is trusted and computes the same feature vectors as gwimmer's matlab code.",
        date = "07.01.2014",
        lastModified = "07.01.2014")
public class FeatureExtractionMethodD3TCWTCyclicShift extends FeatureExtractionMethodD3TCWTLocal
{
    protected final FeatureExtractionParametersD3TCWTCyclicShift myParameters;
    public FeatureExtractionMethodD3TCWTCyclicShift(FeatureExtractionParameters param)
    {
        super(param);
        this.myParameters = (FeatureExtractionParametersD3TCWTCyclicShift) param;
    }
    
    @Override
    protected double [] computeFeatures(double [][] data)
            throws JCeliacGenericException
    {
        double [][][] combinedSubbandCoefficients = super.computeCombinedDTCWTSubbands(data);
        double [][][] shiftedSubbandCoefficients = this.alignAllCoefficientsCyclicShift(combinedSubbandCoefficients);
        return this.computeFeatureVector(shiftedSubbandCoefficients);
        
    }
    
    protected double [] computeFeatureVector(double[][][] shiftedCoeffs)
    {
        double[] featureData = null;
        int index = 0;

        for(int scaleLevel = 0; scaleLevel < shiftedCoeffs.length; scaleLevel++) {
            for(int subbandIndex = 0; subbandIndex < 6; subbandIndex++) {
                
                double[] subbandCoefficients = shiftedCoeffs[scaleLevel][subbandIndex];
                switch(this.myParameters.getDTCWTFeatureType()) {
                    case ENERGY:
                        if(featureData == null) {
                            featureData = new double[shiftedCoeffs.length * 6 * 2];
                        }
                        double mean = ArrayTools.mean(subbandCoefficients);
                        double std = MathTools.stdev(subbandCoefficients);
                        featureData[index] = mean;
                        index++;
                        featureData[index] = std;
                        index++;
                        break;

                    case ENTROPY:
                        if(featureData == null) {
                            featureData = new double[shiftedCoeffs.length * 6];
                        }
                        double entropy = MathTools.computeEntropy(subbandCoefficients);
                        featureData[index] = entropy;
                        index++;
                        break;
                }
            }
        }
        return featureData;
    }
    
    private  double [][][]  alignAllCoefficientsCyclicShift(double [][][] combinedSubbandCoefficients)
            throws JCeliacGenericException
    {
        double [][][] shiftedSubbandCoefficients = 
        new double[combinedSubbandCoefficients.length][combinedSubbandCoefficients[0].length][combinedSubbandCoefficients[0][0].length];
        
        for(int k = 0; k < combinedSubbandCoefficients[0][0].length; k++) {
            double [][] coeffOverDims = this.extractCoefficientDimension(combinedSubbandCoefficients, k);
            double [][] alignedCoeffs = null;
            
            if(this.myParameters.isAlignScaleDimension() == false &&
               this.myParameters.isAlignOrientationDimension() == false)
            {
                throw new JCeliacGenericException("Cyclic Shifting is not reasonable without any cyclic shift!");
            }
            
            if(this.myParameters.isAlignScaleDimension() && 
               this.myParameters.isAlignOrientationDimension()) {
                alignedCoeffs = this.alignScaleByCyclicShift(coeffOverDims);
                alignedCoeffs = this.alignOrientationByCyclicShift(alignedCoeffs);
            }
            else if(this.myParameters.isAlignScaleDimension() &&
                    this.myParameters.isAlignOrientationDimension() == false) {
                 alignedCoeffs = this.alignScaleByCyclicShift(coeffOverDims);
            }
             else if(this.myParameters.isAlignScaleDimension() == false &&
                    this.myParameters.isAlignOrientationDimension()) {
                 alignedCoeffs = this.alignOrientationByCyclicShift(coeffOverDims);
            }
            
            
            for(int i = 0; i < alignedCoeffs.length; i++) {
                for(int j = 0; j < alignedCoeffs[0].length; j++) {
                    // dimensions were swapped in extractCoefficientDimension, swap it back now
                    shiftedSubbandCoefficients[j][i][k] = alignedCoeffs[i][j];
                }
            }
        }
        return shiftedSubbandCoefficients;
    }
    
    /**
     * Performs the cyclic shift in the scale dimension to align the features.
     * 
     * @return 
     */
    private double [][] alignScaleByCyclicShift(double [][] coeffOverScales)
            throws JCeliacGenericException
    {
        double [][] filter = new double[coeffOverScales.length][coeffOverScales[0].length];
        double [][] coeffsSquared = new double[coeffOverScales.length][coeffOverScales[0].length];
        
        // compute squared coefficient responses
        for(int i = 0; i < coeffOverScales.length; i++) {
            for(int j = 0; j < coeffOverScales[0].length; j++) {
                coeffsSquared[i][j] = coeffOverScales[i][j] * coeffOverScales[i][j];
            }
        }
        // compute filter
        for(int i = 0; i < coeffOverScales.length; i++) {
            for(int j = 0; j < coeffOverScales[0].length; j++) {
                filter[i][j] = 1.0d - j*(1.0d / filter.length);
            }
        }
        double [][] correlatedCoeffs = this.correlateCircular(coeffsSquared, filter);
        
        /* Extract a single column to sort and reorder the coefficients. 
         * The matlab code does a reordering here, I don't know why and
         * I think it's not needed, but I also do it to keep in sync with
         * the matlab code.
         */
        double [] correlatedColumn = new double[correlatedCoeffs[0].length];
        for(int i = 2; i < correlatedCoeffs[0].length; i++) {
                correlatedColumn[i-2] = correlatedCoeffs[0][i];
        }
        for(int i = 1; i >= 0; i--) {
            correlatedColumn[correlatedCoeffs.length-2+i] = correlatedCoeffs[0][i];
        }
        // search for max index, to realing the coefficients
        int maxIndex = ArrayTools.maxIndex(correlatedColumn);
        int [] reorderingIndices = new int[correlatedCoeffs[0].length];
        int index = 0;
        for(int i = maxIndex; i < reorderingIndices.length; i++) {
            reorderingIndices[index] = i;
            index++;
        }
         for(int i = 0; i < maxIndex; i++) {
            reorderingIndices[index] = i;
            index++;
        }
        double [][] shiftedCoefficients = new double[coeffOverScales.length][coeffOverScales[0].length];      
        for(int i = 0; i < coeffOverScales.length; i++) {
            for(int oldIndex = 0; oldIndex < reorderingIndices.length; oldIndex++) {
                int newIndex = reorderingIndices[oldIndex];
                shiftedCoefficients[i][oldIndex] = coeffOverScales[i][newIndex];
            }
        }
        return shiftedCoefficients;
    }
    
    
        /**
     * Performs the cyclic shift in the scale dimension to align the features.
     * 
     * @return 
     */
    private double [][] alignOrientationByCyclicShift(double [][] coeffOverScales)
            throws JCeliacGenericException
    {
        double [][] filter = new double[coeffOverScales.length][coeffOverScales[0].length];
        double [][] coeffsSquared = new double[coeffOverScales.length][coeffOverScales[0].length];
        
        // compute squared coefficient responses
        for(int i = 0; i < coeffOverScales.length; i++) {
            for(int j = 0; j < coeffOverScales[0].length; j++) {
                coeffsSquared[i][j] = coeffOverScales[i][j] * coeffOverScales[i][j];
            }
        }
        // compute filter
        for(int i = 0; i < coeffOverScales.length; i++) {
            for(int j = 0; j < coeffOverScales[0].length; j++) {
                filter[i][j] = 1.0d - i*(1.0d / filter.length);
            }
        }
        double [][] correlatedCoeffs = this.correlateCircular(coeffsSquared, filter);
        
        /* Extract a single column to sort and reorder the coefficients. 
         * The matlab code does a reordering here, I don't know why and
         * I think it's not needed, but I also do it to keep in sync with
         * the matlab code.
         */
        double [] correlatedColumn = new double[correlatedCoeffs.length];
        for(int i = 2; i < correlatedCoeffs.length; i++) {
                correlatedColumn[i-2] = correlatedCoeffs[i][0];
        }
        for(int i = 1; i >= 0; i--) {
            correlatedColumn[correlatedCoeffs.length-2+i] = correlatedCoeffs[i][0];
        }
        // search for max index, to re-align the coefficients
        int maxIndex = ArrayTools.maxIndex(correlatedColumn);
        int [] reorderingIndices = new int[correlatedCoeffs.length];
        int index = 0;
        for(int i = maxIndex; i < reorderingIndices.length; i++) {
            reorderingIndices[index] = i;
            index++;
        }
         for(int i = 0; i < maxIndex; i++) {
            reorderingIndices[index] = i;
            index++;
        }
        double [][] shiftedCoefficients = new double[coeffOverScales.length][coeffOverScales[0].length];      
        for(int i = 0; i < coeffOverScales[0].length; i++) {
            for(int oldIndex = 0; oldIndex < reorderingIndices.length; oldIndex++) {
                int newIndex = reorderingIndices[oldIndex];
                shiftedCoefficients[oldIndex][i] = coeffOverScales[newIndex][i];
            }
        }
        return shiftedCoefficients;
    }
    
    private double[][] correlateCircular(double[][] signal, double[][] filter)
            throws JCeliacGenericException
    {

        if(signal.length < filter.length) {
            throw new JCeliacGenericException("Filter length > signal length not supported!");
        }

        if(signal[0].length < filter.length) {
            throw new JCeliacGenericException("Filter length > signal length not supported!");
        }

        double[][] outArray = new double[signal.length][signal[0].length];
        int deltaX = filter.length / 2;
        int deltaY = filter[0].length / 2;

        if(filter.length % 2 == 0) {
            deltaX = deltaX - 1;
        }
        if(filter[0].length % 2 == 0) {
            deltaY = deltaY - 1;
        }
        // rows

        for(int x = 0; x < signal.length; x++) {
            for(int y = 0; y < signal[0].length; y++) {
                // convolve
                double convSum = 0;
                for(int i = 0; i < filter.length; i++) {
                    for(int j = 0; j < filter[0].length; j++) {
                        
                        int signalX = (x - deltaX + i);
                        int signalY = (y - deltaY + j);
                        
                        if(signalX < 0) {
                            signalX = signal.length + signalX ;
                        }
                        if(signalX >= signal.length) {
                            signalX = signalX - signal.length;
                        }
                        
                        if(signalY < 0) {
                            signalY = signal[0].length + signalY;
                        }
                        if(signalY >= signal[0].length) {
                            signalY = signalY - signal[0].length;
                        }
                        convSum += filter[i][j] * signal[signalX][signalY];
                    }
                }
                outArray[x][y] = convSum;

            }

        }
        return outArray;
    }

    
    /**
     * Dimensions are scale, subband, coefficient index
     * @param data
     * @return 
     */
    private double [][] extractCoefficientDimension(double [][][] data, int coeffIndex) 
    {
        double [][] coeffs = new double[data[0].length][data.length];
        for(int i = 0; i < data.length; i++) {
            for(int j = 0; j < data[0].length; j++) {
                coeffs[j][i] = data[i][j][coeffIndex];
            }
        }
        return coeffs;
    }

    

}
