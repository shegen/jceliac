/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.dtcwt;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FixedFeatureVector;
import jceliac.featureExtraction.StableCode;
import jceliac.featureExtraction.StandardEuclideanFeatures;
import static jceliac.featureExtraction.dtcwt.FeatureExtractionParametersD3TCWTLocal.ED3TCWTTransformType.DFT;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.Frame;
import jceliac.tools.filter.ImageResize;
import jceliac.tools.math.MathTools;
import jceliac.tools.transforms.dtcwt.ComplexArray2D;
import jceliac.tools.transforms.dtcwt.DualTreeComplexWaveletDecomposition;
import jceliac.tools.transforms.dtcwt.DualTreeComplexWaveletTransform;
import jceliac.tools.transforms.dtcwt.filter.BiorthogonalFilter;
import jceliac.tools.transforms.dtcwt.filter.BiorthogonalFilter_Near_Sym_A;
import jceliac.tools.transforms.dtcwt.filter.QShiftFilter;
import jceliac.tools.transforms.dtcwt.filter.QShiftFilter_QShift_B;
import jceliac.tools.transforms.fft.FastFourierTransform;

/**
 *
 * @author shegen
 */
@StableCode(
        comment = "This code is trusted and computes the same feature vectors as gwimmer's matlab code.",
        date = "06.02.2014",
        lastModified = "06.02.2014")
public class FeatureExtractionMethodD3TCWTLocal
        extends FeatureExtractionMethod
{
    
    protected BiorthogonalFilter biort = new BiorthogonalFilter_Near_Sym_A();
    protected QShiftFilter qshift = new QShiftFilter_QShift_B();
    protected FeatureExtractionParametersD3TCWTLocal myParameters;
    
    public FeatureExtractionMethodD3TCWTLocal(FeatureExtractionParameters param)
    {
        super(param);
        this.myParameters = (FeatureExtractionParametersD3TCWTLocal)param;
    }

    
    @Override
    public DiscriminativeFeatures extractFeatures(Frame content) 
            throws JCeliacGenericException
    {
       StandardEuclideanFeatures features = new StandardEuclideanFeatures();
       features.setSignalIdentifier(content.getSignalIdentifier());
       features.setFrameNumber(1);
       
       if(this.myParameters.isUseColor()) {
           for(Integer colorChannel : this.myParameters.getColorChannels()) {
               double [] featureData = this.computeFeatures(content.getColorChannel(colorChannel));
               features.addAbstractFeature(new FixedFeatureVector(featureData));
           }
       }else {
           double [] featureDataGray = this.computeFeatures(content.getGrayData());
           features.addAbstractFeature(new FixedFeatureVector(featureDataGray));
       }
       return features;
    }
    
    protected double [][] manipulateFeatures(double [][][] coefficients)
            throws JCeliacGenericException
    {
        double [][] manipulatedCoefficients;
        double [][] reshapedCoefficientsPerScale = new double[this.myParameters.getNumScales()*2][];
        
        for(int scaleLevel = 1; scaleLevel <= coefficients.length; scaleLevel++) {
            for(int subbandIndex = 1; subbandIndex <= 6; subbandIndex++) {
                double [] coefficientsAtScale = this.reshapeCoefficientsAtScaleDimension(coefficients, scaleLevel);
                reshapedCoefficientsPerScale[scaleLevel-1] = coefficientsAtScale;
            } 
        }
        switch(this.myParameters.getCoefficientManipulationTransform()) {
            case DFT:
                manipulatedCoefficients = this.manipulateFeaturesDFT(reshapedCoefficientsPerScale);
                break;
                
            default:
                throw new JCeliacGenericException("Feature manipulation transform type not supported: "+this.myParameters.coefficientManipulationTransform);     
        }
        return manipulatedCoefficients;
    }
    
    
    protected double [][][] computeCombinedDTCWTSubbands(double [][] data) 
            throws JCeliacGenericException
    {
        DualTreeComplexWaveletTransform dtcwtTransform = 
                    new DualTreeComplexWaveletTransform(this.biort, this.qshift);
        dtcwtTransform.transformForward(data, this.myParameters.getNumScales());
        DualTreeComplexWaveletDecomposition decompositionTree = dtcwtTransform.getDecompositionTree();
        
        int maxSubbandSize = dtcwtTransform.getDecompositionTree().getComplexSubband(1, 1).realValues.length;
        ImageResize resizer = new ImageResize(ImageResize.EInterpolationType.NEAREST);
        double [][][] combinedSubbands = new double[this.myParameters.getNumScales()*2][6][];
        
        // compute and arrange coefficients dyadic scales
        for(int scaleLevel = 1; scaleLevel <= this.myParameters.getNumScales(); scaleLevel++) {
            for(int subbandIndex = 1; subbandIndex <= 6; subbandIndex++) {
                ComplexArray2D subbandData = decompositionTree.getComplexSubband(scaleLevel, subbandIndex);
                double [][] absoluteSubbandData = subbandData.abs();
                absoluteSubbandData = resizer.resizeImage(absoluteSubbandData, maxSubbandSize, maxSubbandSize);
                
                double [] reshapedAbsoluteSubbandData = this.reshapeAndComplexAbs(absoluteSubbandData);
                combinedSubbands[(2*scaleLevel-1)-1][subbandIndex-1] = reshapedAbsoluteSubbandData;
            }
        }
        // compute and arrange coefficients between dyadic scales
        double scaleFactor = 1.0d / Math.sqrt(2.0d);
        double [][] resizedData = resizer.resizeImage(data, scaleFactor);
        dtcwtTransform.transformForward(resizedData, this.myParameters.getNumScales());
        decompositionTree = dtcwtTransform.getDecompositionTree();
        
        for(int scaleLevel = 1; scaleLevel <= this.myParameters.getNumScales(); scaleLevel++) {
            for(int subbandIndex = 1; subbandIndex <= 6; subbandIndex++) {
                ComplexArray2D subbandData = decompositionTree.getComplexSubband(scaleLevel, subbandIndex);
                double [][] absoluteSubbandData = subbandData.abs();
                absoluteSubbandData = resizer.resizeImage(absoluteSubbandData, maxSubbandSize, maxSubbandSize);
                
                double [] reshapedAbsoluteSubbandData = this.reshapeAndComplexAbs(absoluteSubbandData);
                combinedSubbands[(2*scaleLevel)-1][subbandIndex-1] = reshapedAbsoluteSubbandData;
            }
        }
        return combinedSubbands;
        
    }
    
    
    protected double [] computeFeatures(double [][] data) 
            throws JCeliacGenericException
    {
        double [][][] coeffsDFT = this.computeCombinedDTCWTSubbands(data);
        double [][] manipulatedFeatures = this.manipulateFeatures(coeffsDFT);
        return this.computeFeatureVector(manipulatedFeatures);  
    }
  
    protected double [] computeFeatureVector(double[][] manipulatedCoeffs)
    {
        double[] featureData = null;
        int index = 0;

        for(int scaleLevel = 0; scaleLevel < manipulatedCoeffs.length; scaleLevel++) {
            for(int subbandIndex = 0; subbandIndex < 6; subbandIndex++) {
                int startIndex = (int) (subbandIndex * manipulatedCoeffs[0].length / 6.0);
                int endIndex = (int) ((subbandIndex + 1) * manipulatedCoeffs[0].length / 6.0);

                double[] subbandCoefficients = new double[endIndex - startIndex];
                System.arraycopy(manipulatedCoeffs[scaleLevel], startIndex, subbandCoefficients, 0, endIndex - startIndex);

                switch(this.myParameters.getDTCWTFeatureType()) {
                    case ENERGY:
                        if(featureData == null) {
                            featureData = new double[manipulatedCoeffs.length * 6 * 2];
                        }
                        double mean = ArrayTools.mean(subbandCoefficients);
                        double std = MathTools.stdev(subbandCoefficients);
                        featureData[index] = mean;
                        index++;
                        featureData[index] = std;
                        index++;
                        break;

                    case ENTROPY:
                        if(featureData == null) {
                            featureData = new double[manipulatedCoeffs.length * 6];
                        }
                        double entropy = MathTools.computeEntropy(subbandCoefficients);
                        featureData[index] = entropy;
                        index++;
                        break;
                }
            }
        }
        return featureData;
    }
    
    
    protected double[][] manipulateFeaturesDFT(double[][] allCoeffsAtScales)
    {
        double[][] manipulatedCoeffs = new double[(int)(allCoeffsAtScales.length/2.0d)+1][allCoeffsAtScales[0].length];

        FastFourierTransform dft = new FastFourierTransform(allCoeffsAtScales.length); // fft falls back to dft for 6 values
        for(int k = 0; k < allCoeffsAtScales[0].length; k++) {
            
            double[] coeffOverScales = new double[allCoeffsAtScales.length];
            for(int scaleLevel = 0; scaleLevel < allCoeffsAtScales.length; scaleLevel++) {
                coeffOverScales[scaleLevel] = allCoeffsAtScales[scaleLevel][k];
            }
            double[] coeffs = dft.transformFullOptimized(coeffOverScales).abs();
            for(int index = 0; index < (coeffs.length / 2.0d) + 1; index++) {
                    manipulatedCoeffs[index][k] = coeffs[index];
            }
        }
        return manipulatedCoeffs;
    }

    protected double [] reshapeAndComplexAbs(double [][] subbandData)
    {
        double [] retData = new double[subbandData.length * subbandData[0].length];
        int index = 0;
        for(int i = 0; i < subbandData.length; i++) {
            for(int j = 0; j < subbandData[0].length; j++) {
                retData[index] = subbandData[i][j];
                index++;
            }
        }
        return retData;
    }
    
    protected double[] reshapeCoefficientsAtScaleDimension(double[][][] subbandData, int scaleLevel)
    {
        double[] reshapedArray = new double[subbandData[scaleLevel-1].length * subbandData[scaleLevel-1][0].length];
        int index = 0;
        for(int coeffIndex = 0; coeffIndex < subbandData[scaleLevel-1][0].length; coeffIndex++) {
            for(int subbandIndex = 0; subbandIndex < subbandData[scaleLevel-1].length; subbandIndex++) {
                reshapedArray[index] = subbandData[scaleLevel-1][subbandIndex][coeffIndex];
                index++;
            }
        }
        return reshapedArray;
    }
    
    
     /**
     * This is called separately for training and test features. 
     * 
     * @param features
     * @return 
     * @throws JCeliacGenericException 
     */
    @Override
    public ArrayList<DiscriminativeFeatures> 
            normalizeFeatures(ArrayList<DiscriminativeFeatures> features) 
                        throws JCeliacGenericException
    {
        // the n-th feature vector entry of each feature builds a column 
        // vector and is normalized
        double [][] rawFeatures = new double[features.size()][];
        
        for(int featureIndex = 0; featureIndex < features.size(); featureIndex++) 
        {
            StandardEuclideanFeatures castedTmpFeatures = (StandardEuclideanFeatures) features.get(featureIndex);
            double [] rawFeatureData = castedTmpFeatures.getAllFeatureVectorData();
            rawFeatures[featureIndex] = rawFeatureData;
        }
        
        double [] nthFeatureColumn = new double[features.size()];
        for(int n = 0; n < rawFeatures[0].length; n++) {
            for(int featureIndex = 0; featureIndex < rawFeatures.length; featureIndex++) {
                nthFeatureColumn[featureIndex] = rawFeatures[featureIndex][n];
            }
            // compute mean and std of column
            double colMean = MathTools.mean(nthFeatureColumn);
            double colStd = MathTools.stdev(nthFeatureColumn);
            for(int i = 0; i < nthFeatureColumn.length; i++) {
                rawFeatures[i][n] =  (rawFeatures[i][n] - colMean) / colStd;
            }
        }
        ArrayList <DiscriminativeFeatures> normalizedFeatures = new ArrayList <>();
        // store normalized features back
        for(int featureIndex = 0; featureIndex < features.size(); featureIndex++) 
        {
            FixedFeatureVector vector = new FixedFeatureVector();
            vector.setData(rawFeatures[featureIndex]);
            
            StandardEuclideanFeatures castedTmpFeatures = (StandardEuclideanFeatures)features.get(featureIndex).cloneEmptyFeature();
            castedTmpFeatures.addAbstractFeature(vector);
            normalizedFeatures.add(castedTmpFeatures);
        }
        return normalizedFeatures;
    }
    
    @Override
    public void initialize() 
            throws JCeliacGenericException
    {
       
    }

    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass) 
            throws JCeliacGenericException
    {
       
    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {
       
    }

}
