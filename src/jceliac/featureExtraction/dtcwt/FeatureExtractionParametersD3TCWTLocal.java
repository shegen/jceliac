/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.featureExtraction.dtcwt;

import jceliac.experiment.Experiment;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersD3TCWTLocal extends FeatureExtractionParametersDTCWT
{
    protected String coefficientManipulationTransform = "DFT";

    public FeatureExtractionParametersD3TCWTLocal()
    {
        this.numScales = 6;
    }
    
    public enum ED3TCWTTransformType {
        DFT;
        
        public static ED3TCWTTransformType valueOfIgnoreCase(String s)
        {
            for(ED3TCWTTransformType type : ED3TCWTTransformType.values()) {
               if(type.name().equalsIgnoreCase(s)) {
                   return type;
               }
            }
            Experiment.log(JCeliacLogger.LOG_WARN, "Unknown D3TCWT Transform Type, using DFT!");
            return DFT;
        }
    };

    public ED3TCWTTransformType getCoefficientManipulationTransform()
    {
        return ED3TCWTTransformType.valueOfIgnoreCase(this.coefficientManipulationTransform);
    }
    
    @Override
    public void report()
    {
        super.report();
        Experiment.log(JCeliacLogger.LOG_INFO, "Coefficient Transform: " + this.coefficientManipulationTransform);
    }

    @Override
    public String getMethodName()
    {
        return "D3TCWT Local";
    }  
}
