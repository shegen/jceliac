/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.dtcwt;

import jceliac.experiment.Experiment;
import jceliac.featureExtraction.StableCode;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
@StableCode(
        comment = "This code is trusted and computes the same feature vectors as gwimmer's matlab code.",
        date = "10.02.2014",
        lastModified = "10.02.2014")
public class FeatureExtractionParametersD3TCWTCyclicShift 
    extends FeatureExtractionParametersD3TCWTLocal
{
    protected boolean alignScaleDimension = true;
    protected boolean alignOrientationDimension = false;

    public boolean isAlignScaleDimension()
    {
        return alignScaleDimension;
    }

    public boolean isAlignOrientationDimension()
    {
        return alignOrientationDimension;
    }
    
    
    public FeatureExtractionParametersD3TCWTCyclicShift()
    {
    }

    @Override
    public void report()
    {
        super.report(); 
        Experiment.log(JCeliacLogger.LOG_INFO, "Align Scale Dimension: " + this.alignScaleDimension);
        Experiment.log(JCeliacLogger.LOG_INFO, "Align Orientation Dimension: " + this.alignOrientationDimension);
    }

    
    @Override
    public String getMethodName()
    {
        return "D3TCWT Cyclic Shift";
    }
    
    
}
