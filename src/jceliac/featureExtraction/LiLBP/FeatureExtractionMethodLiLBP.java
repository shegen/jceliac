/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.featureExtraction.LiLBP;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.LBP.LBPFeatures;
import jceliac.featureExtraction.LBP.LBPHistogram;
import jceliac.featureExtraction.SALBP.LBPNeighborLookup;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.Frame;
import jceliac.tools.filter.scalespace.LaplacianScalespaceRepresentation;
import jceliac.tools.filter.scalespace.ScaleLevelIndexedDataSupplier;
import jceliac.tools.filter.scalespace.ScalespaceParameters;
import jceliac.tools.filter.scalespace.ScalespaceRepresentation;

/**
 *
 * @author shegen
 */
public class FeatureExtractionMethodLiLBP 
    extends FeatureExtractionMethod
{
    protected LBPNeighborLookup neighborLookupTable;
    protected ScalespaceParameters scalespaceParameters;
    private final FeatureExtractionParametersLiLBP myParameters;
    
    public FeatureExtractionMethodLiLBP(FeatureExtractionParameters param)
    {
       super(param);
        
       /*
        * The authors build the scalespace with fixes sigmas (1,2,...N).
        * Their best value was 14, which we use here. 
        */
        double [] sigmas = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
        this.scalespaceParameters = new ScalespaceParameters(sigmas);
        
        this.neighborLookupTable = new LBPNeighborLookup(8);
        this.myParameters = (FeatureExtractionParametersLiLBP) param;
    }
    
    

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content) 
            throws JCeliacGenericException
    {
        double [][] localRadii = this.computeLocalRadii(content.getGrayData());
        LBPHistogram hist = this.computeLBPHistogram(localRadii, content.getGrayData(), 8);
        
        LBPFeatures features = new LBPFeatures(this.myParameters);
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(0);
        features.addAbstractFeature(hist);
        return features;
    }
    
    private LBPHistogram computeLBPHistogram(double [][] localRadii, double [][] data, int neighbors)
    {
        double[] hist = new double[1 << neighbors];
        int shiftMax = neighbors- 1;
        
        double[][] neighborCoords = new double[8][2];
        for(int x = 0; x < data.length; x++) {
            for(int y = 0; y < data[0].length; y++) {

                double center = (double) data[x][y];
                int pattern = 0;
                double radius = localRadii[x][y];
                
                this.neighborLookupTable.lookup(radius, neighborCoords);
                try {
                    for(int p = 0; p < neighbors; p++) {

                        double value = this.getInterpolatedPixelValueFast(data, x+neighborCoords[p][0], y-neighborCoords[p][1]);
                        if(value >= center) {
                            // this is in reverse order from the matlab thingy to speed up things
                            pattern |= (1 << (shiftMax - p));
                        }
                    }
                    hist[pattern] = hist[pattern] + 1.0d;
                }catch(ArrayIndexOutOfBoundsException e) {}
            }
        }
        LBPHistogram lbpHistogram = new LBPHistogram();
        lbpHistogram.setData(hist);
        return lbpHistogram;
    }
    
    
    /**
     * The paper states:
     * <p>
     * "The optimal scale of pixel (x,y) with the maximum response, denoated as
     * lambda(x,y), controls the neighboring region size of this pixel for
     * describing LBP feature."
     * <p>
     * I.e.: The maxmimum response over all scales determines the LBP radius at
     * this pixel, we therefore compute the LBP radius as sqrt(2)*sigma_i for
     * each pixel with maximum response at sigma_i.
     * <p>
     * @return
     */
    private double[][] computeLocalRadii(double[][] imageData)
            throws JCeliacGenericException
    {
        ScalespaceRepresentation scaleRep
                                 = new LaplacianScalespaceRepresentation(imageData,
                                                                         this.scalespaceParameters);

        /* The authores write, they detect the local maximum following [28] 
         * I suppose this is the normalized LoG response following the paper.
         */
        scaleRep.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_SUM);
        ScaleLevelIndexedDataSupplier<double[][]> normlaplacians = scaleRep.getNormalizedLaplacianDataSupplier();

        double[][] maxResponses = new double[imageData.length][imageData[0].length];
        int[][] maxResponseLevels = new int[imageData.length][imageData[0].length];

        // initialize maxResponses
        for(int i = 0; i < maxResponses.length; i++) {
            for(int j = 0; j < maxResponses.length; j++) {
                maxResponses[i][j] = Double.NEGATIVE_INFINITY;
            }
        }
        // figure out max responses for each pixel
        for(int scaleLevel = 0; scaleLevel < normlaplacians.getScaleCount(); scaleLevel++) {
            double[][] responsesAtScale = normlaplacians.getData(scaleLevel);
            for(int i = 0; i < imageData.length; i++) {
                for(int j = 0; j < imageData[0].length; j++) {
                    if(responsesAtScale[i][j] > maxResponses[i][j]) {
                        maxResponses[i][j] = responsesAtScale[i][j];
                        maxResponseLevels[i][j] = scaleLevel;
                    }
                }
            }
        }
        // compute radii
        double[][] localRadii = new double[imageData.length][imageData[0].length];
        for(int i = 0; i < maxResponses.length; i++) {
            for(int j = 0; j < maxResponses.length; j++) {
                // the paper does not explicitly mention this computation but 
                // they always say optimal scale, hence it it probably the sigma not the radius at sigma
                localRadii[i][j] = this.scalespaceParameters.getSigmaForScaleLevel(maxResponseLevels[i][j]);
            }
        }
        return localRadii;
    }
    
    @Override
    public void initialize() throws JCeliacGenericException
    {
    }

    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass) 
            throws JCeliacGenericException
    {
    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {
    }
    
    
     /**
     * This is a lot faster than the matlab compliant version; It was tested
     * with all images we have, no differences to the compliant Java
     * implementation. This performs bi-linear interpolation.
     * <p>
     * <p>
     * @param data Raw image data to interpolate on.
     * @param x    Coordinate x.
     * @param y    Coordiante y.
     * <p>
     * @return Interpolated value.
     */
    protected double getInterpolatedPixelValueFast(final double[][] data, double x, double y)
    {

        int fx = (int) x;
        int fy = (int) y;
        int cx = (int) Math.ceil(x);
        int cy = (int) Math.ceil(y);

        double ty = y - fy;
        double tx = x - fx;

        double invtx = 1 - tx;
        double invty = 1 - ty;

        double w1 = invtx * invty;
        double w2 = tx * invty;
        double w3 = invtx * ty;
        double w4 = tx * ty;

        // use this only in extreme debugging cases, this method is called every time
        // Experiment.log(Logger.LOG_HHDEBUG, "getInterpoaltedPixelValue: %f * %f", w1, data[fx][fy]);
        // Experiment.log(Logger.LOG_HHDEBUG, "getInterpoaltedPixelValue: %f * %f", w2, data[cx][fy]);
        // Experiment.log(Logger.LOG_HHDEBUG, "getInterpoaltedPixelValue: %f * %f", w3, data[fx][cy]);
        //  Experiment.log(Logger.LOG_HHDEBUG, "getInterpoaltedPixelValue: %f * %f", w4, data[cx][cy]);
        double ret = w1 * data[fx][fy] + w2 * data[cx][fy] + w3 * data[fx][cy] + w4 * data[cx][cy];
        return ret;
    }
    
}
