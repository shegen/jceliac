/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.featureExtraction;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.TreeSet;
import jceliac.JCeliacGenericException;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;

/**
 *
 * @author shegen
 */
public class FeatureCollection 
    implements Iterable <DiscriminativeFeatures>, Comparable<FeatureCollection>
{
    private final ArrayList <DiscriminativeFeatures> features;
    private final String featureCollectionID;
    private final long sortSeed = 492835905L;
    
    public synchronized static String generateUniqueID(ArrayList <DiscriminativeFeatures> features) 
            throws JCeliacGenericException 
            
    {
        String idString = "";
        for(DiscriminativeFeatures feature : features) {
            idString += feature.getFeatureID()+";";
        }
        return idString;
    }
    
    public static FeatureCollection createClonedFeatureCollection(ArrayList<DiscriminativeFeatures> features,
                                                                        FeatureVectorSubsetEncoding subsetEncoding)
            throws JCeliacGenericException
    {
        return new FeatureCollection(features, subsetEncoding);
    }
    
    public static FeatureCollection createClonedFeatureCollection(ArrayList<DiscriminativeFeatures> features) 
            throws JCeliacGenericException
    {
       return new FeatureCollection(features);
    }
    
     public static FeatureCollection createClonedFeatureCollection(FeatureCollection features,
                                                                   FeatureVectorSubsetEncoding subsetEncoding)
            throws JCeliacGenericException
    {
        return new FeatureCollection(features.getFeatures(), subsetEncoding);
    }
    
    public static FeatureCollection createClonedFeatureCollection(FeatureCollection features) 
            throws JCeliacGenericException
    {
       return new FeatureCollection(features.getFeatures());
    }


    @SuppressWarnings("unchecked")
    protected FeatureCollection(ArrayList<DiscriminativeFeatures> features) 
            throws JCeliacGenericException
    {
        this.features = new  ArrayList <>();
        if(features == null) {
            this.featureCollectionID = "empty"; // indicate empty
            return;
        }
        for(DiscriminativeFeatures tmp : features) {
            this.features.add(tmp);
        }
        this.featureCollectionID = FeatureCollection.generateUniqueID(this.features);
        // ensure a random permutation of the stored features to avoid any probability
        // of introducting an unwanted bias due to bugs in the code
        java.util.Collections.shuffle(this.features, new Random(this.sortSeed));
    }
    
    @SuppressWarnings("unchecked")
    protected FeatureCollection(ArrayList<DiscriminativeFeatures> features,
                             FeatureVectorSubsetEncoding subset) 
            throws JCeliacGenericException
    {
        this.features = new ArrayList <>();
        for(DiscriminativeFeatures tmp : features) {
            this.features.add(tmp.cloneFeatureSubset(subset));
        }
        this.featureCollectionID = FeatureCollection.generateUniqueID(this.features);
        // ensure a random permutation of the stored features to avoid any probability
        // of introducting an unwanted bias due to bugs in the code
        java.util.Collections.shuffle(this.features, new Random(this.sortSeed));
    }
    
    
    
    public static FeatureCollection combine(FeatureCollection[] folds)
            throws JCeliacGenericException
    {
        ArrayList<DiscriminativeFeatures> combinedFeatures = new ArrayList<>();
        for(int i = 0; i < folds.length; i++) {
            combinedFeatures.addAll(folds[i].getFeatures());
        }
        return new FeatureCollection(combinedFeatures);
    }
 
    
    public static FeatureCollection combineExcept(FeatureCollection[] folds,
                                                  int exceptInded)
            throws JCeliacGenericException
    {
        ArrayList<DiscriminativeFeatures> combinedFeatures = new ArrayList<>();
        for(int i = 0; i < folds.length; i++) {
            if(i != exceptInded) {
                combinedFeatures.addAll(folds[i].getFeatures());
            }
        }
        return new FeatureCollection(combinedFeatures);
    }
        
    
    public int size()
    {
        return this.features.size();
    }
    
    public boolean isEmpty()
    {
        return (this.features == null || this.features.isEmpty());
    }

    public String getFeatureCollectionID()
    {
        return featureCollectionID;
    }

    @Override
    public Iterator<DiscriminativeFeatures> iterator()
    {
        return this.features.iterator();
    }   
    
    public DiscriminativeFeatures get(int index) 
    {
        return this.features.get(index);
    }

    public ArrayList<DiscriminativeFeatures> getFeatures()
    {
        return features;
    }

    @Override
    public int compareTo(FeatureCollection o)
    {
        return o.featureCollectionID.compareTo(this.featureCollectionID);
    }
    
    public TreeSet <Long> getFeatureIDs()
    { 
        TreeSet <Long> featureIDs = new TreeSet<>();
        for(DiscriminativeFeatures feature : this.features) {
            featureIDs.add(feature.getFeatureID());
        }
        return featureIDs;
    }
    
    public void flagAsTrainingFeatures() {
        for (DiscriminativeFeatures tmp : this.features) {
            tmp.flagAsTrainingFeature();
        }
    }
    
    public void flagAsEvaluationFeatures() {
        for (DiscriminativeFeatures tmp : this.features) {
            tmp.flagAsEvaluationFeature();
        }
    }
    
    
    
    
}
