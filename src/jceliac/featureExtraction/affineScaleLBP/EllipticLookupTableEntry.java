/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBP;

import java.io.Serializable;

/**
 * Lookup table entry for points distributed at equal arc-distances on an
 * ellipse.
 * <p>
 * @author shegen
 */
public class EllipticLookupTableEntry
        implements Serializable, Cloneable
{

    public EllipticPoint[][] pointsByAngle;
    public double a;
    public double b;
    public double distanceToCenter;

    public EllipticLookupTableEntry(double a, double b)
    {
        this.a = a;
        this.b = b;
        this.pointsByAngle = new EllipticPoint[360][];
        this.distanceToCenter = Math.sqrt(a * a + b * b);
    }

    public void addPointForAngle(int angleIndex, EllipticPoint[] points)
    {
        this.pointsByAngle[angleIndex] = points;
    }
}
