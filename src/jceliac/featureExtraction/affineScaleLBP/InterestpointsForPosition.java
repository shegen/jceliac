/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBP;

import jceliac.tools.filter.scalespace.SecondMomentMatrix;
import jceliac.tools.filter.scalespace.ScalespacePoint;
import java.util.ArrayList;

public class InterestpointsForPosition
{

    public int x;
    public int y;
    public ScalespacePoint maximumInterestPoint = null;
    public ArrayList<ScalespacePoint> interestpoints;

    public InterestpointsForPosition(int x, int y)
    {
        this.x = x;
        this.y = y;
        this.interestpoints = new ArrayList<>();
    }

    public void addInterestpoint(ScalespacePoint point)
    {
        if(this.interestpoints.contains(point) == false) {
            this.interestpoints.add(point);
            if(this.maximumInterestPoint == null || point.getResponse() > this.maximumInterestPoint.getResponse()) {
                this.maximumInterestPoint = point;
            }
        }
    }

    public SecondMomentMatrix getSecondMomentMatrix(int index)
    {
        return this.interestpoints.get(index).getSecondMomentMatrix();
    }

    public double getDetectionSigma(int index)
    {
        return this.interestpoints.get(index).getSigma();
    }

    public int getInterestpointCount()
    {
        return this.interestpoints.size();
    }

    public ScalespacePoint getMaximumPoint()
    {
        return this.maximumInterestPoint;
    }

    public double getMaximumResponse()
    {
        return this.maximumInterestPoint.getResponse();
    }

    public double getMeanValue()
    {
        double mean = 0;
        for(ScalespacePoint tmp : this.interestpoints) {
            mean += tmp.getResponse();
        }
        mean = mean / this.interestpoints.size();
        return mean;
    }

    public double getStdValue()
    {
        double mean = this.getMeanValue();
        double std = 0;
        for(ScalespacePoint tmp : this.interestpoints) {
            std += (tmp.getResponse() - mean) * (tmp.getResponse() - mean);
        }
        return Math.sqrt(std / this.interestpoints.size());
    }
}
