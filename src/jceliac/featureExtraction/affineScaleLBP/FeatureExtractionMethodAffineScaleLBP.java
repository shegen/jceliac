/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBP;

import jceliac.featureExtraction.affineScaleLBPScaleBias.FeatureExtractionParametersScaleLBP;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.FeatureExtractionMethod.EFeatureExtractionMethod;
import jceliac.tools.filter.scalespace.LaplacianScalespaceRepresentation;
import jceliac.tools.filter.scalespace.ScaleLevelIndexedDataSupplier;
import jceliac.tools.filter.scalespace.ScalespaceStructureTensor;
import jceliac.tools.filter.scalespace.ScalespaceRepresentation;
import jceliac.featureExtraction.LBP.FeatureExtractionMethodLBP;
import jceliac.tools.filter.scalespace.ScalespaceParameters;
import jceliac.tools.filter.scalespace.SecondMomentMatrix;
import jceliac.tools.filter.scalespace.ScalespacePoint;
import jceliac.tools.filter.scalespace.StructureTensor;
import jceliac.featureExtraction.ExperimentalCode;
import jceliac.featureExtraction.LBP.LBPHistogram;
import jceliac.featureExtraction.LTP.LTPHistogram;
import jceliac.featureExtraction.LBP.LBPFeatures;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.tools.filter.LowpassFilter;
import jceliac.tools.data.ContentStream;
import jceliac.JCeliacGenericException;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.DataSource;
import jceliac.experiment.Experiment;
import jceliac.tools.math.MathTools;
import jceliac.tools.data.Frame;
import java.util.ArrayList;

/**
 * Affine Scale LBP this implements the affine and scale invariant LBP but is
 * still experimental and not trusted. 
 * This is one of the early versions of the affine scale lbp and mainly 
 * kept for documentation purposes, incomplete and buggy do not use directly.
 * 
 * <p>
 * @author shegen
 */
@ExperimentalCode(comment = "Code is experimental and not trusted, use mainly of documentation!",
                  date = "17.01.2014",
                  lastModified = "17.01.2014")
public class FeatureExtractionMethodAffineScaleLBP
        extends FeatureExtractionMethodLBP
{

    private final FeatureExtractionParametersScaleLBP myParameters;
    private final int IGNORE_BORDER = 2;
    private final ScalespaceParameters scalespaceParameters;
    private EllipticPointLookup lookup;
    private final boolean useLBP = false;
    private int frameWidth, frameHeight;

    @Override
    public void initialize()
            throws JCeliacGenericException
    {
        // precompute the lookup table for points on an ellipse
        this.lookup = EllipticPointLookup.create();
    }

    public FeatureExtractionMethodAffineScaleLBP(FeatureExtractionParameters param)
    {
        super(param);
        // this is just for convenience
        this.myParameters = (FeatureExtractionParametersScaleLBP) super.parameters;
        this.scalespaceParameters = new ScalespaceParameters(Math.sqrt(2.0d), 0, 10, 1, 1);
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {
        ScaleLevelIndexedDataSupplier<Frame> prefilteredFrames;

        LBPFeatures features = new LBPFeatures(this.myParameters);
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());

        this.frameHeight = content.getHeight();
        this.frameWidth = content.getWidth();

        ScaleDistribution redScaleDistribution = new ScaleDistribution(this.scalespaceParameters, content.getWidth());
        ScaleDistribution greenScaleDistribution = new ScaleDistribution(this.scalespaceParameters, content.getWidth());
        ScaleDistribution blueScaleDistribution = new ScaleDistribution(this.scalespaceParameters, content.getWidth());

        ScalespaceRepresentation scaleRepRed;
        ScalespaceRepresentation scaleRepGreen;
        ScalespaceRepresentation scaleRepBlue;

        scaleRepRed = new LaplacianScalespaceRepresentation(content.getRedData(), this.scalespaceParameters);
        scaleRepRed.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_ABSOLUTE_SUM);
        scaleRepRed.computeMaxima();

        redScaleDistribution.addInterestpointsToDistribution(scaleRepRed.getMaximas());
        redScaleDistribution.computeScaleDistribution();

        scaleRepGreen = new LaplacianScalespaceRepresentation(content.getGreenData(), this.scalespaceParameters);
        scaleRepGreen.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_ABSOLUTE_SUM);
        scaleRepGreen.computeMaxima();

        scaleRepBlue = new LaplacianScalespaceRepresentation(content.getBlueData(), this.scalespaceParameters);
        scaleRepBlue.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_ABSOLUTE_SUM);
        scaleRepBlue.computeMaxima();

        ScalespaceStructureTensor structureTensorsRed;
        ScalespaceStructureTensor structureTensorsGreen;
        ScalespaceStructureTensor structureTensorsBlue;

        structureTensorsRed = new ScalespaceStructureTensor(scaleRepRed);
//        structureTensorsRed.computeStructureTensors(scaleRepRed.getMaximas());

        structureTensorsGreen = new ScalespaceStructureTensor(scaleRepGreen);
  //      structureTensorsGreen.computeStructureTensors(scaleRepGreen.getMaximas());

        structureTensorsBlue = new ScalespaceStructureTensor(scaleRepBlue);
    //    structureTensorsBlue.computeStructureTensors(scaleRepBlue.getMaximas());

        prefilteredFrames = this.precomputeFilteredFramesGaussianFilter(content, scalespaceParameters, 8);

        LBPHistogram redHist = this.extractFeatures(content, prefilteredFrames, scaleRepRed, structureTensorsRed, 1);
        LBPHistogram greenHist = this.extractFeatures(content, prefilteredFrames, scaleRepGreen, structureTensorsGreen, 2);
        LBPHistogram blueHist = this.extractFeatures(content, prefilteredFrames, scaleRepBlue, structureTensorsBlue, 3);

        features.addAbstractFeature(redHist);
        features.addAbstractFeature(blueHist);
        features.addAbstractFeature(greenHist);

        return features;
    }

    private LBPHistogram extractFeatures(Frame frame, ScaleLevelIndexedDataSupplier<Frame> scaledFrames,
                                         ScalespaceRepresentation scaleRep, ScalespaceStructureTensor tensor,
                                         int colorChannel)
            throws JCeliacGenericException
    {
        int neighbors = 8;
        double[] histLBP = null;
        double[] histLTPUpper = null;
        double[] histLTPLower = null;
        EllipticPoint[] points = new EllipticPoint[8];

        if(this.useLBP) {
            histLBP = new double[getNumberOfBins(neighbors)];
        } else {
            histLTPUpper = new double[getNumberOfBins(neighbors)];
            histLTPLower = new double[getNumberOfBins(neighbors)];

        }
        int margin = IGNORE_BORDER;

        double[][] data = null;
        switch(colorChannel) {
            case 1:
                data = frame.getRedData();
                break;
            case 2:
                data = frame.getGreenData();
                break;
            case 3:
                data = frame.getBlueData();
                break;
        }
        InterestpointsForPosition[][] intPointsPos
                                      = collectInterestPointsForAllPositions3DMaximaEllipses(data, scaleRep, tensor);

        StructureTensor structureTensor = new StructureTensor(data, 1.5d / Math.sqrt(2.0d));
        structureTensor.prepareData();

        for(int x = margin; x < this.frameWidth - margin; x++) {
            for(int y = margin; y < this.frameHeight - margin; y++) {

                // get the correct rescaled frame for this pixel
                InterestpointsForPosition ip = intPointsPos[x][y];
                if(ip == null) { // add new dummy point with standard radius 1.5 pixel
                    ip = new InterestpointsForPosition(x, y);
                    ScalespacePoint p = new ScalespacePoint(-1, -1, x, y, this.scalespaceParameters);
                    p.setSecondMomentMatrix(structureTensor.getTensor(x, y));
                    p.setSigma(0);
                    p.setScaleExponent(Double.NaN); // invalid value
                    ip.addInterestpoint(p);
                    p.setParent(p);
                    p.setChildren(8);
                    p.setScaleConfidence(1.0d);
                    continue;
                }

                for(ScalespacePoint currentInterestPoint : ip.interestpoints) {
                    if(currentInterestPoint.getScaleConfidence() == 0) {
                        continue;
                    }
                    SecondMomentMatrix smm = currentInterestPoint.getSecondMomentMatrix();
                    double sigma = currentInterestPoint.getSigma();

                    try {
                        Ellipse ell;
                        double radius;
                        try {
                            if(sigma == 0) {
                                radius = 1.5d;
                                ell = new Ellipse(smm, radius, Ellipse.CIRCUMFERENCE_NORMALIZATION_MODE);
                            } else {
                                radius = currentInterestPoint.getSigma() * Math.sqrt(2.0d);
                                ell = new Ellipse(smm, radius, Ellipse.CIRCUMFERENCE_NORMALIZATION_MODE);
                            }
                        } catch(org.apache.commons.math3.exception.MaxCountExceededException e) {
                            continue; // this happens if a smm is a null matrix, in this case the pattern will be 255, ignore it
                        }
                        int scaleLevel = currentInterestPoint.getScaleLevel();
                        if(ell.a < 0.5 || ell.b < 0.5) {
                            continue;
                        }
                        Frame scaledFrame;
                        if(Double.isNaN(scaleLevel)) {
                            scaledFrame = frame;
                        } else {
                            scaledFrame = scaledFrames.getData(scaleLevel);
                        }
                        double[][] scaledData = null;
                        switch(colorChannel) {
                            case 1:
                                scaledData = scaledFrame.getRedData();
                                break;
                            case 2:
                                scaledData = scaledFrame.getGreenData();
                                break;
                            case 3:
                                scaledData = scaledFrame.getBlueData();
                                break;
                            default:
                                throw new JCeliacGenericException("color channel not supported");
                        }

                        // check if the LBP can be computed in this area, assuming a radius of 1.5 -> margin = ceil(radius) = 2
                        if(x - 2 < 0 || x + 2 >= scaledData.length || y - 2 < 0 || y + 2 >= scaledData[0].length) {
                            continue;
                        }
                        double center = (double) scaledData[x][y];

                        this.lookup.lookup(points, x, y, ell.a, ell.b, ell.phi);
                        if(this.useLBP) {
                            int[] patterns = this.extractLBP(center, points, scaledData);

                            // weight ehe patterns such that each interestpoint adds the same amount to the histogram 
                            // no matter of the the size of the sigma of the interestpoint
                            histLBP[patterns[0]] = histLBP[patterns[0]] + (1.0d * (currentInterestPoint.getScaleConfidence()));
                            if(patterns[0] != patterns[1]) {
                                histLBP[patterns[1]] = histLBP[patterns[1]] + (1.0d * (currentInterestPoint.getScaleConfidence()));
                            }
                        } else {
                            double threshold;
                            double stdev = MathTools.stdev(scaledData);

                            if(stdev >= myParameters.getThresholdBeta()) {
                                threshold = myParameters.getThresholdBeta() + myParameters.getThresholdAlpha() * stdev;
                            } else {
                                threshold = myParameters.getThresholdBeta() - myParameters.getThresholdAlpha() * stdev;
                            }
                            int[][] patterns = this.extractLTP(center, threshold, points, scaledData);

                            // weight ehe patterns such that each interestpoint adds the same amount to the histogram 
                            // no matter of the the size of the sigma of the interestpoint
                            histLTPLower[patterns[0][0]] = histLTPLower[patterns[0][0]] + (1.0d * (currentInterestPoint.getScaleConfidence() / currentInterestPoint.getParent().getChildren()));
                            histLTPUpper[patterns[0][1]] = histLTPUpper[patterns[0][1]] + (1.0d * (currentInterestPoint.getScaleConfidence() / currentInterestPoint.getParent().getChildren()));

                            if(patterns[0][0] != patterns[1][0] && patterns[0][1] != patterns[1][1]) {
                                histLTPLower[patterns[1][0]] = histLTPLower[patterns[1][0]] + (1.0d * (currentInterestPoint.getScaleConfidence() / currentInterestPoint.getParent().getChildren()));
                                histLTPUpper[patterns[1][1]] = histLTPUpper[patterns[1][1]] + (1.0d * (currentInterestPoint.getScaleConfidence() / currentInterestPoint.getParent().getChildren()));
                            }
                        }
                    } catch(ArrayIndexOutOfBoundsException e) { // ignored
                    }
                }
            }
        }
        if(this.useLBP) {
            LBPHistogram lbpHistogram = new LBPHistogram();
            lbpHistogram.setData(histLBP);
            return lbpHistogram;
        } else {
            LTPHistogram ltpHistogram = new LTPHistogram();
            histLTPLower[0] = 0;
            histLTPUpper[0] = 0;
            ltpHistogram.setDataLower(histLTPLower);
            ltpHistogram.setDataUpper(histLTPUpper);
            return ltpHistogram;
        }

    }

    private int[] extractLBP(double center, EllipticPoint[] points, double[][] scaledData)
    {
        int pattern = 0;
        int shiftMax = points.length - 1;

        for(int p = 0; p < points.length; p++) {
            double neighbor = 0;
            if(points[p].x < scaledData.length - 1 && points[p].y < scaledData[0].length - 1
               && points[p].x >= 0 && points[p].y >= 0) {
                neighbor = this.getInterpolatedPixelValueFast(scaledData, points[p].x, points[p].y);
            } else {
                throw new ArrayIndexOutOfBoundsException();
            }
            if((neighbor) >= center) {
                // this is in reverse order from the matlab thingy to speed up things
                pattern |= (1 << (shiftMax - p));
            }
        }
        int alternatePattern = (pattern & 0xF0) >> 4 | (pattern & 0xF) << 4;
        int[] ret = {pattern, alternatePattern};
        return ret;
    }

    private int[][] extractLTP(double center, double threshold, EllipticPoint[] points,
                               double[][] scaledData)
    {
        int shiftMax = points.length - 1;
        int patternUpper = 0;
        int patternLower = 0;

        for(int p = 0; p < points.length; p++) {
            double neighbor = 0;
            if(points[p].x < scaledData.length - 1 && points[p].y < scaledData[0].length - 1
               && points[p].x >= 0 && points[p].y >= 0) {
                neighbor = this.getInterpolatedPixelValueFast(scaledData, points[p].x, points[p].y);
            } else {
                throw new ArrayIndexOutOfBoundsException();
            }
            if(neighbor >= (center + threshold)) {
                // this is in reverse order from the matlab thingy to speed up things
                patternUpper |= (1 << (shiftMax - p));
            } else if(neighbor <= (center - threshold)) {
                // this is in reverse order from the matlab thingy to speed up things
                patternLower |= (1 << (shiftMax - p));
            } else {
                // zero
            }
        }
        int alternatePatternLower = (patternLower & 0xF0) >> 4 | (patternLower & 0xF) << 4;
        int alternatePatternUpper = (patternUpper & 0xF0) >> 4 | (patternUpper & 0xF) << 4;
        int[][] ret = {{patternLower, patternUpper}, {alternatePatternLower, alternatePatternUpper}};
        return ret;
    }

    private double[] computeDistances(EllipticPoint[] points, double x, double y)
    {
        double[] distances = new double[points.length];

        for(int i = 0; i < distances.length; i++) {
            double xd = (points[i].x - x) * (points[i].x - x);
            double yd = (points[i].y - y) * (points[i].y - y);
            distances[i] = Math.sqrt(xd + yd) - 1.5d;
        }
        return distances;
    }

    private ScaleLevelIndexedDataSupplier<Frame>
            precomputeFilteredFramesGaussianFilter(Frame frame, ScalespaceParameters params,
                                                   int neighborsCount)
            throws JCeliacGenericException
    {
        ScaleLevelIndexedDataSupplier<Frame> frameDataSupplier = new ScaleLevelIndexedDataSupplier<>(params);

        for(int scaleLevel = 0;
            scaleLevel < params.getNumberOfScales();
            scaleLevel++) {
            double detectionSigma = params.getSigmaForScaleLevel(scaleLevel);
            double detectionRadius = detectionSigma * Math.sqrt(2.0d);
            double detectionCircumference = 2 * detectionRadius * Math.PI;
            double filterRadius = detectionCircumference / (2.0d * (double) neighborsCount);

            int filterWidth = (int) Math.round(filterRadius) * 2;
            if(filterWidth % 2 == 0) {
                filterWidth++;
            }
            if(filterWidth == 1) {
                frameDataSupplier.setData(scaleLevel, frame);
                continue;
            }
                // compute the sigma for a gaussian filter with dimension filterWidth such 
            // that X% of the area are covered between -filterRadius and +filterRadius
            double sigma = (filterRadius) / (Math.sqrt(2.0d) * MathTools.erfinv(0.9d));

            LowpassFilter gaussianFilter = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, filterWidth, sigma);
            Frame filteredFrame = gaussianFilter.filterData(frame);
            frameDataSupplier.setData(scaleLevel, filteredFrame);
        }
        return frameDataSupplier;
    }

    private InterestpointsForPosition[][]
            collectInterestPointsForAllPositions3DMaximaEllipses(double[][] data,
                                                                 ScalespaceRepresentation scaleRep,
                                                                 ScalespaceStructureTensor tensors)
            throws JCeliacGenericException
    {
        InterestpointsForPosition[][] intPointsForData = new InterestpointsForPosition[data.length][data[0].length];
        if(scaleRep == null) {
            return intPointsForData;
        }

        ArrayList<ScalespacePoint> maximas = scaleRep.getMaximas();
        try {
            for(ScalespacePoint tmp : maximas) {
                SecondMomentMatrix smm = tensors.getStructureTensor(tmp.getCoordX(), tmp.getCoordY(), tmp.getScaleLevel());
                double radius = tmp.getSigma() * Math.sqrt(2.0d);
                Ellipse ell = new Ellipse(smm, radius, Ellipse.CIRCUMFERENCE_NORMALIZATION_MODE);
                this.fillEllipseModified(intPointsForData, tmp, ell, scaleRep, tensors);
            }
        } catch(org.apache.commons.math3.exception.MaxCountExceededException e) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Eigenvalue Decomposition did not converge! Ignoring Interestpoint!");
        }

        // select the best scales for each pixel  position
        for(int x = 0; x < intPointsForData.length; x++) {
            for(int y = 0; y < intPointsForData[0].length; y++) {
                InterestpointsForPosition tmp = intPointsForData[x][y];
                if(tmp != null) {
                    double mean = tmp.getMeanValue();
                    double std = tmp.getStdValue();

                    for(ScalespacePoint tmpPoint : tmp.interestpoints) {
                        double maxResponse = tmp.getMaximumResponse();
                        double tmpConfidence = tmpPoint.getScaleConfidence();
                        double tmpResponse = tmpPoint.getResponse();

                        tmpPoint.setScaleConfidence((tmpResponse / maxResponse) * tmpConfidence);
                    }
                }
            }
        }
        return intPointsForData;
    }

    private void fillEllipseModified(InterestpointsForPosition[][] intPointsForData, ScalespacePoint interestPoint,
                                     Ellipse ell, ScalespaceRepresentation scaleRep, ScalespaceStructureTensor tensors)
            throws JCeliacGenericException
    {
        EllipticPoint[] foci = ell.getFoci(interestPoint.getCoordX(), interestPoint.getCoordY());
        int major;

        int x0 = interestPoint.getCoordX();
        int y0 = interestPoint.getCoordY();
        
        // rotate the foci 
        foci = EllipticPoint.rotatePoints(foci, x0, y0, ell.phi);
        major = (int) (Math.max(ell.a, ell.b) + 1); // round up
        double minor = Math.max(ell.a, ell.b);
        for(int offX = -major; offX < major; offX++) {
            for(int offY = -major; offY < major; offY++) {
                int x = x0 + offX;
                int y = y0 + offY;

                if(x < 0 || x >= intPointsForData.length
                   || y < 0 || y >= intPointsForData[0].length) {
                    continue;
                }
                double distance1 = Math.sqrt(((x - foci[0].x) * (x - foci[0].x)) + ((y - foci[0].y) * (y - foci[0].y)));
                double distance2 = Math.sqrt(((x - foci[1].x) * (x - foci[1].x)) + ((y - foci[1].y) * (y - foci[1].y)));
                if(distance1 + distance2 <= 2 * major) {
                    if(intPointsForData[x][y] == null) {
                        intPointsForData[x][y] = new InterestpointsForPosition(x0, y0);
                    }

                    ScalespacePoint newInterestPoint = new ScalespacePoint(interestPoint.getScaleLevel(), interestPoint.getResponse(), x, y, scaleRep.getScalespaceParameters());
                    newInterestPoint.setParent(interestPoint);
                    newInterestPoint.setSecondMomentMatrix(tensors.getStructureTensor(interestPoint.getCoordX(), interestPoint.getCoordY(), interestPoint.getScaleLevel()));
                    newInterestPoint.setSigma(interestPoint.getSigma());
                    // compute scale confidence
                    // double radius = interestPoint.getSigma() * Math.sqrt(2.0d);
                    // copmute the confidence such that the scale confidence is 1 at distance 0 
                    // and 0 at distance of the radius as a linear function:
                    // y = 1 - (1 / radius) * x
                    double distance = Math.sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0));
                    // double confidence = 1 - (1/major) * distance;

                    double sigma = Math.sqrt(-((minor) * (minor)) / (2 * Math.log(0.5)));
                    double confidence = Math.exp(-(distance * distance) / (2 * (sigma * sigma)));

                    if(confidence > 0) {
                        newInterestPoint.setScaleConfidence(confidence);
                        intPointsForData[x][y].addInterestpoint(newInterestPoint);
                        int children = interestPoint.getChildren();
                        interestPoint.setChildren(children + 1);
                    }
                }
            }
        }
    }

    private void fillEllipse(InterestpointsForPosition[][] intPointsForData, ScalespacePoint interestPoint,
                             Ellipse ell, ScalespaceRepresentation scaleRep, ScalespaceStructureTensor tensors)
            throws JCeliacGenericException
    {
        EllipticPoint[] foci = ell.getFoci(interestPoint.getCoordX(), interestPoint.getCoordY());
        int major;

        int x0 = interestPoint.getCoordX();
        int y0 = interestPoint.getCoordY();

        // rotate the foci 
        foci = EllipticPoint.rotatePoints(foci, x0, y0, ell.phi);
        major = (int) (Math.max(ell.a, ell.b) + 1); // round up
        double minor = Math.max(ell.a, ell.b);
        for(int offX = -major; offX < major; offX++) {
            for(int offY = -major; offY < major; offY++) {
                int x = x0 + offX;
                int y = y0 + offY;

                if(x < 0 || x >= intPointsForData.length
                   || y < 0 || y >= intPointsForData[0].length) {
                    continue;
                }
                double distance1 = Math.sqrt(((x - foci[0].x) * (x - foci[0].x)) + ((y - foci[0].y) * (y - foci[0].y)));
                double distance2 = Math.sqrt(((x - foci[1].x) * (x - foci[1].x)) + ((y - foci[1].y) * (y - foci[1].y)));
                if(distance1 + distance2 <= 2 * major) {
                    if(intPointsForData[x][y] == null) {
                        intPointsForData[x][y] = new InterestpointsForPosition(x0, y0);
                    }

                    ScalespacePoint newInterestPoint = new ScalespacePoint(interestPoint.getScaleLevel(), interestPoint.getResponse(), x, y, scaleRep.getScalespaceParameters());
                    newInterestPoint.setParent(interestPoint);
                    newInterestPoint.setSecondMomentMatrix(tensors.getStructureTensor(x, y, interestPoint.getScaleLevel()));
                    newInterestPoint.setSigma(interestPoint.getSigma());
                    // compute scale confidence
                    // double radius = interestPoint.getSigma() * Math.sqrt(2.0d);
                    // copmute the confidence such that the scale confidence is 1 at distance 0 
                    // and 0 at distance of the radius as a linear function:
                    // y = 1 - (1 / radius) * x
                    double distance = Math.sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0));
                    // double confidence = 1 - (1/major) * distance;

                    double sigma = Math.sqrt(-((minor) * (minor)) / (2 * Math.log(0.5)));
                    double confidence = Math.exp(-(distance * distance) / (2 * (sigma * sigma)));

                    if(confidence > 0) {
                        newInterestPoint.setScaleConfidence(confidence);
                        intPointsForData[x][y].addInterestpoint(newInterestPoint);
                        int children = interestPoint.getChildren();
                        interestPoint.setChildren(children + 1);
                    }
                }
            }
        }
    }

    /**
     * This estimates the thresholds used within the LTP operator. The
     * thresholds are calculated as: stdev(content) > beta => beta + alpha *
     * stdev(content); stdev(content) < beta => beta + alpha * stdev(content);
     * The value alpha is a weighting factor and is parametrizeable. The value
     * beta is estimated by this method as the standard deviation of all images
     * (pixel intensity deviation).
     *
     * @param dataReaders
     * @throws jceliac.JCeliacGenericException
     */
    @Override
    public void estimateParameters(ArrayList<DataReader> dataReaders)
            throws JCeliacGenericException
    {
        double meanSum = 0;
        double mean;
        double count = 0;
        double sum = 0;

        Experiment.log(JCeliacLogger.LOG_INFO, "Estimating LTP Parameters (Treshold Beta).");

        for(int classNum = 0; classNum < dataReaders.size(); classNum++) {
            DataReader current = dataReaders.get(classNum);
            while(current.hasNext()) {
                DataSource currentFile = (DataSource) current.next();
                ContentStream cstream = currentFile.createContentStream();

                while(cstream.hasNext()) {
                    Frame content = (Frame) cstream.next();
                    double[][] data = content.getGrayData();

                    for(int x = 0; x < data.length; x++) {
                        for(int y = 0; y < data[0].length; y++) {
                            meanSum += data[x][y];
                            count++;
                        }
                    }
                }
            }
            current.reset();
        }
        mean = (meanSum / count);

        for(int classNum = 0; classNum < dataReaders.size(); classNum++) {
            DataReader current = dataReaders.get(classNum);
            while(current.hasNext()) {
                DataSource currentFile = (DataSource) current.next();
                ContentStream cstream = currentFile.createContentStream();

                while(cstream.hasNext()) {
                    Frame content = (Frame) cstream.next();
                    double[][] data = content.getGrayData();
                    for(int x = 0; x < data.length; x++) {
                        for(int y = 0; y < data[0].length; y++) {
                            double tmp = data[x][y];
                            sum += (tmp - mean) * (tmp - mean);
                        }
                    }

                }
            }
            current.reset();
        }
        double stdev = Math.sqrt(sum / (count - 1)); // compliant to matlab std(X)
        Experiment.log(JCeliacLogger.LOG_INFO, "Grayscale Pixel Standard Deviation: %f.", stdev);
        this.myParameters.setThresholdBeta(Math.sqrt(stdev));
    }

    @Override
    public void cleanup()
    {

    }
}
