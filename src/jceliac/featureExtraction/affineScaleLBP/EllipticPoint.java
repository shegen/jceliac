/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBP;

import java.io.Serializable;

/**
 * A point on an ellipse.
 * <p>
 * @author shegen
 */
public class EllipticPoint
        implements Serializable, Cloneable
{

    public double x;
    public double y;
    public int position;
    public double distanceToCenter = -1;

    public EllipticPoint(double x, double y)
    {
        this.x = x;
        this.y = y;
        this.distanceToCenter = Math.sqrt(x * x + y * y);
    }

    public EllipticPoint(double x, double y, int position)
    {
        this.x = x;
        this.y = y;
        this.position = position;
        this.distanceToCenter = Math.sqrt(x * x + y * y);
    }

    public EllipticPoint()
    {
    }

    /**
     * Rotate points lying on axis aligned ellipses by phi radiants.
     * <p>
     * @param points Points on ellipse.
     * @param x0     Center of ellipse.
     * @param y0     Center of ellispe.
     * @param phi    Rotation of the main-axis.
     * <p>
     * @return A new array of rotated elliptic points.
     */
    public static EllipticPoint[] rotatePoints(EllipticPoint[] points, double x0, double y0, double phi)
    {
        EllipticPoint[] rotatedPoints = new EllipticPoint[points.length];
        for(int i = 0; i < points.length; i++) {
            double x = points[i].x - x0;
            double y = points[i].y - y0;

            double xRotated = x * Math.cos(phi) - y * Math.sin(phi);
            double yRotated = x * Math.sin(phi) + y * Math.cos(phi);
            x = xRotated + x0;
            y = yRotated + y0;
            EllipticPoint rotatedPoint = new EllipticPoint(x, y, points[i].position);
            rotatedPoints[i] = rotatedPoint;
        }
        return rotatedPoints;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        super.clone();
        EllipticPoint clonedPoint = new EllipticPoint(this.x, this.y, this.position);
        return clonedPoint;
    }

}
