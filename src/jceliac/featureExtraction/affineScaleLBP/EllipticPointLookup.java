/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBP;

import jceliac.JCeliacGenericException;
import java.io.FileNotFoundException;
import jceliac.experiment.Experiment;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.math.MathTools;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

/**
 * Lookup table for points distributed at equal arc-distances on an ellipse.
 * <p>
 * @author shegen
 */
public class EllipticPointLookup
{

    private static final double ERROR_THRESHOLD = 0.1;
    private static final double ISOTROPY_THRESHOLD = 0.95; // unused
    private static final double MAX_RATIO = 50;
    private static final double MIN_RATIO = 0.01;
    // we use this to multiply the ratios with to get integers to use as keys for the hashmap
    private static final double RATIO_KEY_MULTIPLIER = 1.0d / MIN_RATIO;
    private static final String LOOKUP_TABLE_FILE = "Elliptic-Coordinates-Lookup.ser";

    private static EllipticPointLookup instance = null;

    private HashMap<Integer, EllipticLookupTableEntry> lookupTable;

    public synchronized static EllipticPointLookup create()
            throws JCeliacGenericException
    {
        if(EllipticPointLookup.instance == null) {
            EllipticPointLookup.instance = new EllipticPointLookup();
        }
        return EllipticPointLookup.instance;
    }

    // enforce singleton to improve performance
    private EllipticPointLookup()
            throws JCeliacGenericException
    {
        this.loadLookupTable();
    }

    /**
     * Loads the lookup table from a serialized file, if the file does not
     * exist, a new lookup table is computed and serialized.
     * <p>
     * @throws JCeliacGenericException
     */
    @SuppressWarnings("unchecked")
    private void loadLookupTable()
            throws JCeliacGenericException
    {

        try {
            ObjectInputStream objIn;
            try(FileInputStream fileIn = new FileInputStream(LOOKUP_TABLE_FILE)) {
                HashMap<Integer, EllipticLookupTableEntry> lt;
                objIn = new ObjectInputStream(fileIn);
                lt = (HashMap<Integer, EllipticLookupTableEntry>) objIn.readObject();
                this.lookupTable = lt;
            }
            objIn.close();
        } catch(FileNotFoundException e) {
            // computing lookup table
            Experiment.log(JCeliacLogger.LOG_INFO, "Could not find lookup table file, computing lookup table!");
            this.computeLookupTable();
        } catch(IOException | ClassNotFoundException e) {
            Experiment.log(JCeliacLogger.LOG_INFO, "Could not deserialize lookup table: %s", e.getLocalizedMessage());
            throw new JCeliacGenericException(e);
        }
    }

    private void computeLookupTable()
            throws JCeliacGenericException
    {
        this.lookupTable = new HashMap<>();
        for(double ratio = 0.01; ratio < MAX_RATIO; ratio += 0.01) {
            // increase the scale of ellipses with small ratios to improve the 
            // accuracy of the numerical integration
            double scaleup = 100;
            if(ratio > 1) {
                scaleup = 10;
            } else if(ratio > 10) {
                scaleup = 1;
            }

            // create an ellipse with the corresponding axis-ratio
            double a = ratio * scaleup;
            double b = scaleup;

            EllipticPoint[] points = this.distributePointsOnEllipse(0, 0, a, b, 0);
            this.scalePoints(points, 1 / scaleup);
            EllipticLookupTableEntry entry = new EllipticLookupTableEntry(a / scaleup, b / scaleup);

            // compute rotations in steps of 1 degree
            for(int angle = 0; angle < 360; angle++) {
                double anglerad = ((double) angle / 180.0d) * Math.PI;
                // check if we are close to a circle
                if(Math.min(a, b) / Math.max(a, b) > ISOTROPY_THRESHOLD) {
                    anglerad = 0;
                }
                EllipticPoint[] rotatedPoints = EllipticPoint.rotatePoints(points, 0, 0, anglerad);
                //  this.fixPointPositions(rotatedPoints);
                rotatedPoints = this.reoderPointsByIndex(rotatedPoints);
                entry.addPointForAngle(angle, rotatedPoints);
            }
            lookupTable.put((int) (ratio * RATIO_KEY_MULTIPLIER), entry);
            System.out.println("Lookup Entry for ratio: " + ((int) (ratio * RATIO_KEY_MULTIPLIER)) + " computed!");
        }
        // Serialize object and store
        try {
            try(FileOutputStream fileOutputStream = new FileOutputStream(LOOKUP_TABLE_FILE);
                ObjectOutputStream objOutputStream = new ObjectOutputStream(fileOutputStream)) {
                objOutputStream.writeObject(this.lookupTable);
                Experiment.log(JCeliacLogger.LOG_INFO, "Serialized Lookup Table as: %s", LOOKUP_TABLE_FILE);
            }
        } catch(IOException e) {
            Experiment.log(JCeliacLogger.LOG_INFO, "Serialized Lookup Table failed: %s", e.getLocalizedMessage());
            throw new JCeliacGenericException(e);
        }
    }

    // used for debugging
    public EllipticLookupTableEntry computeLookupTableEntry(double ratio)
            throws JCeliacGenericException
    {
        // increase the scale of ellipses with small ratios to improve the 
        // accuracy of the numerical integration
        double scaleup = 100;
        if(ratio > 1) {
            scaleup = 10;
        } else if(ratio > 10) {
            scaleup = 1;
        }
        // create an ellipse with the corresponding axis-ratio
        double a = ratio * scaleup;
        double b = scaleup;
        EllipticPoint[] points = this.distributePointsOnEllipse(0, 0, a, b, 0);
        this.scalePoints(points, 1 / scaleup);
        EllipticLookupTableEntry entry = new EllipticLookupTableEntry(a / scaleup, b / scaleup);

        // compute rotations in steps of 1 degree
        for(int angle = 0; angle < 360; angle++) {

            double anglerad = ((double) angle / 180.0d) * Math.PI;
            // check if we are close to a circle
            if(Math.min(a, b) / Math.max(a, b) > ISOTROPY_THRESHOLD) {
                anglerad = 0;
            }
            EllipticPoint[] rotatedPoints = EllipticPoint.rotatePoints(points, 0, 0, anglerad);
            //this.fixPointPositions(rotatedPoints);
            rotatedPoints = this.reoderPointsByIndex(rotatedPoints);
            entry.addPointForAngle(angle, rotatedPoints);
        }
        return entry;
    }

    // normalize points
    private void scalePoints(EllipticPoint[] points, double factor)
    {
        for(EllipticPoint point : points) {
            point.x = point.x * factor;
            point.y = point.y * factor;
        }
    }

    /**
     * Computes arc length on an ellipse. Parameters: x1 = startpoint on
     * ellipse, x2 = endpoint on ellipse. Computes the arc length of an ellipse
     * within two points x1 and x2. The length of the arc at a point (i.e. at a
     * very small scale) on the ellipse is the length of the hypothenusis of a
     * rectuangluar square with kathedes of dy/dx and dx/dx considering the
     * paramatric form of an ellipse as x^2/a^2 + y^2/b^2 = 1. Hence the length
     * of this hypothenusis is sqrt(1+(dy/dx)^2) and further the length of the
     * entire arc the sums of these hypothenusis between points x1 and x2, which
     * is the integral of sqrt(1+(dy/dx)^2) dx with the limits x1 and x2. The
     * general differential of the ellipse is: dy/dx = -xr / (R*sqrt(R^2-x^2)).
     * We could use a numerical method for integration like Simpson's rule, but
     * I am lazy and simply pick a small dx and use addition. This should
     * suffice our needs. The parameters are R = the (x-axis) and r = the
     * (y-axis). The ellipse is assumed to be in canonical form.
     * <p>
     * I have also implemented this in in matlab in arclen.m
     * <p>
     * @param point1 Startpoint on ellipse.
     * @param point2 Endpoint on ellipse.
     * @param a      Ellipse axis length.
     * @param b      Ellipse axis length.
     * <p>
     * @return Arc length between point1 and point2.
     */
    public double computeArcLength(double point1, double point2, double a, double b)
    {
        int steps = 10000;
        double dx = (point2 - point1) / steps;
        double length = 0;
        int step = 0;

        for(double s = 0; step < steps; s += dx, step++) {
            double x = point1 + s;
            double numerator = -(x * b);
            double denominator = (a * Math.sqrt((a * a) - (x * x)));
            if(denominator == 0) {
                continue;
            }
            double dydx = numerator / denominator;
            dydx = dydx * dydx;
            length += Math.sqrt(1 + dydx) * dx;
        }
        return length;
    }

    /**
     * Distribute n equidistant points on ellipse, regarding the arc length. We
     * set 4 points on the intersection points with the 2 main axes and
     * distribute the n-4 other points within the 4 quadrants of the ellipse.
     * <p>
     * XXX: Currently this works only for 8 points, when using more than 8 the
     * indexing (the ordering) of the points is incorrect!
     * <p>
     * The angle can not be negative!
     * <p>
     * @param x0  Center of ellipse x-coordinate.
     * @param y0  Center of ellipse y-coordinate.
     * @param a   Length of axis a.
     * @param b   Length of axis b.
     * @param phi Rotation of main axis ccw.
     * <p>
     * @return Points on ellipse.
     */
    public EllipticPoint[] distributePointsOnEllipse(double x0, double y0, double a, double b, double phi)
    {
        int pointsperquadrant = (8 - 4) / 4;
        double c = MathTools.ellipseCircumference(a, b);
        double quadrantlen = c / 4.0d;

        if(phi < 0) {
            System.out.println("Negative angle in distrubtePointsOnEllipse! This should not happen!");
            System.exit(0);
        }
        double distancesOnQuadrant = quadrantlen / (pointsperquadrant + 1);
        EllipticPoint[] points = new EllipticPoint[8];

        // support points are along at the intersection of the axis and the ellipse
        points[0] = new EllipticPoint(x0, y0 + b);
        points[1] = new EllipticPoint(x0 - a, y0);
        points[2] = new EllipticPoint(x0, y0 - b);
        points[3] = new EllipticPoint(x0 + a, y0);

        // we use an indexing of points such that the point with positive offset 
        // from the center along the longer axis is the first in the LBP pattern.
        // we then go counter-clockwise; if the ellipse is close enough to a circle
        // we always use the point on the "positive" y-axis (below the center in the image)
        int[] supportIndices = new int[]{1, 3, 5, 7};

        // check if the isotropy of the blob is close to a circle
        if((Math.min(a, b) / Math.max(a, b)) > ISOTROPY_THRESHOLD) {

        } else {
            if(a > b) { // the x-axis is the dominant (longer) axis
                supportIndices = new int[]{3, 5, 7, 1};
            }
        } // else nothing to do, indices are allready correct

        for(int i = 0; i < pointsperquadrant; i++) {
            double deltaX = this.findPointOnEllipse((i + 1) * distancesOnQuadrant, a, b);
            // compute y from deltaX
            double deltaY = Math.sqrt((b * b) - (((b * b) * (deltaX * deltaX)) / (a * a)));

            // now use the ellipse's symmetries 
            points[4] = new EllipticPoint(x0 + deltaX, y0 + deltaY); // quadrant 4
            points[4].position = supportIndices[3] + 1;

            points[5] = new EllipticPoint(x0 + deltaX, y0 - deltaY); // quadrant 1
            points[5].position = supportIndices[2] + 1;

            points[6] = new EllipticPoint(x0 - deltaX, y0 + deltaY); // quadrant 3
            points[6].position = supportIndices[0] + 1;

            points[7] = new EllipticPoint(x0 - deltaX, y0 - deltaY); // quadrant 2
            points[7].position = supportIndices[1] + 1;
        }
        for(int i = 0; i < 4; i++) {
            points[i].position = supportIndices[i];
        }
        if(phi != 0) { // rotation changes the start indices
            // round the angle to 1 degree, to match the lookup table
            int angleDegRounded = (int) (Math.round((phi / Math.PI) * 180d));
            double angleRad = (((double) angleDegRounded) / 180.0d) * Math.PI;
            EllipticPoint[] rotatedPoints = EllipticPoint.rotatePoints(points, x0, y0, angleRad);

            // this.fixPointPositions(rotatedPoints);
            EllipticPoint[] reorderedPoints = this.reoderPointsByIndex(rotatedPoints);
            return reorderedPoints;

        } else {
            return this.reoderPointsByIndex(points);

        }

    }

    /**
     * Reorders point in array such that they are sorted by index.
     * <p>
     * @param points Array of elliptic points.
     * <p>
     * @return Array of elliptic points sorted by index.
     */
    private EllipticPoint[] reoderPointsByIndex(EllipticPoint[] points)
    {
        EllipticPoint[] reorderedPoints = new EllipticPoint[points.length];
        for(EllipticPoint point : points) {
            int index = point.position;
            reorderedPoints[index - 1] = point;
        }
        return reorderedPoints;
    }

    /**
     * Looks for a point on the ellipse such that the length of the between the
     * point and the center is the target length. This is done using some kind
     * of binary sarch by halfing the intervals to refine the results
     * <p>
     * @param targetLength Target length on arc.
     * @param a            Ellipse axis a.
     * @param b            Ellipse axis b.
     * <p>
     * @return The offset on the x-axis from the origin of the ellipse.
     */
    private double findPointOnEllipse(double targetLength, double a, double b)
    {
        double currentPosition = a / (3.0d / 2.0d);
        double length = this.computeArcLength(0, currentPosition, a, b);

        double error = targetLength - length;
        while(Math.abs(error) > ERROR_THRESHOLD) {
            if(error > 0) {
                currentPosition = currentPosition + (a - currentPosition) / 2;
            } else {
                currentPosition = currentPosition - (a - currentPosition) / 2;
            }
            // we always find a point for one quadrant of the ellipse and use 
            // the ellipse symmetries to compute the other points, therefore
            // the left border is 0
            length = this.computeArcLength(0, currentPosition, a, b);
            error = targetLength - length;
        }
        return currentPosition;
    }

    private int shiftPosition(int pos, int startpos, int numpoints)
    {
        return ((pos + (numpoints - startpos)) % numpoints) + 1;
    }

    /**
     * Currently unused.
     * <p>
     * @param points
     */
    private void fixPointPositions(EllipticPoint[] points)
    {
        // fix the start point such that the first point is indeed the point on the positive side of the major axis
        int numpoints = points.length;
        int pointsperquadrant = (numpoints - 4) / 4;
        EllipticPoint[] startpoints = new EllipticPoint[2];
        int alternateStartPosition = 1 + (pointsperquadrant * 2) + 2;

        for(EllipticPoint point : points) {
            if(point.position == 1) {
                startpoints[0] = point;
            }
            if(point.position == alternateStartPosition) {
                startpoints[1] = point;
            }
        }
        // check if the y-positions are basically the same (within 0.1 pixels) if so use the positive x-axis
        if(Math.abs(startpoints[1].y - startpoints[0].y) < 0.1) {
            if(startpoints[1].x > startpoints[0].x) {
                for(EllipticPoint point : points) {
                    int position = this.shiftPosition(point.position, alternateStartPosition, numpoints);
                    point.position = position;
                }
            }
        }// shegen: added else
        else if(startpoints[1].y > startpoints[0].y) {
            for(EllipticPoint point : points) {
                int position = this.shiftPosition(point.position, alternateStartPosition, numpoints);
                point.position = position;
            }
        }
    }

    /**
     * Lookup points for an ellipse.
     * <p>
     * @param clonedPoints This reference will be used to store the elliptic
     *                     points.
     * @param x0           Ellipse origin x-coordinate.
     * @param y0           Ellipse origin y-coordinate.
     * @param a            Ellipse axis length a.
     * @param b            Ellispe axis length b.
     * @param phi          Rotation of main axis ccw.
     * <p>
     * @throws JCeliacGenericException
     */
    public void lookup(EllipticPoint[] clonedPoints, double x0, double y0, double a, double b, double phi)
            throws JCeliacGenericException
    {
        if(Double.isNaN(a) || Double.isNaN(b)) {
            Experiment.log(JCeliacLogger.LOG_WARN, "NaN encoutnered in Ellipse axis length!");
            throw new JCeliacGenericException();
        }
        if(a == 0 || b == 0) {
            //      Experiment.log(JCeliacLogger.LOG_WARN, "Zero Ellipse Axis encountered!");
            throw new JCeliacGenericException("Zero Ellipse Axis encountered!");

        }
        if(phi < 0) {
            System.out.println("Negative angle in lookup! This should not happen!");
            System.exit(0);
        }

        double ratio = a / b;
        int key = (int) (ratio * RATIO_KEY_MULTIPLIER);
        EllipticLookupTableEntry entry = this.lookupTable.get(key);
        // XXX: this should probably be synchronized
        if(entry == null) {
            Experiment.log(JCeliacLogger.LOG_WARN, "No lookup table entry found for a = %f, b = %f, key = %d, computing and adding to lookup table!", a, b, key);
            entry = this.computeLookupTableEntry(ratio);
            this.lookupTable.put((int) (ratio * RATIO_KEY_MULTIPLIER), entry);
        }
        // scale the points
        double scalefactor = a / entry.a;
        int angleIndex = ((int) Math.round((phi / Math.PI) * 180)) % 360;
        EllipticPoint[] points = entry.pointsByAngle[angleIndex];

        for(int i = 0; i < points.length; i++) {
            clonedPoints[i].x = points[i].x * scalefactor;
            clonedPoints[i].y = points[i].y * scalefactor;
            clonedPoints[i].position = points[i].position;
            clonedPoints[i].distanceToCenter = points[i].distanceToCenter * scalefactor;
        }
        // translate into center of ellipse
        for(int i = 0; i < points.length; i++) {
            // add x0 and y0
            clonedPoints[i].x += x0;
            clonedPoints[i].y += y0;
        }
    }
    
    
    

    /**
     * Lookup points for an ellipse. Due to the allocation of elliptic points
     * array this is slower then the new lookup().
     * <p>
     * @param x0  Ellipse origin x-coordinate.
     * @param y0  Ellipse origin y-coordinate.
     * @param a   Ellipse axis length a.
     * @param b   Ellispe axis length b.
     * @param phi Rotation of main axis ccw.
     * <p>
     * @return Array with points on an ellipse.
     * <p>
     * @throws JCeliacGenericException
     */
    public EllipticPoint[] lookupOld(double x0, double y0, double a, double b, double phi)
            throws JCeliacGenericException
    {
        if(phi < 0) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Negative angle in lookup! This should not happen!");
            throw new JCeliacGenericException("Negative angle in lookup! This should not happen!");
        }

        double ratio = a / b;
        int key = (int) (ratio * RATIO_KEY_MULTIPLIER);
        EllipticLookupTableEntry entry = this.lookupTable.get(key);
        // XXX: this should probably be synchronized
        if(entry == null) {
            Experiment.log(JCeliacLogger.LOG_WARN, "No lookup table entry found for a = %f, b = %f, key = %d, computing and adding to lookup table!", a, b, key);
            entry = this.computeLookupTableEntry(ratio);
            this.lookupTable.put((int) (ratio * RATIO_KEY_MULTIPLIER), entry);
        }
        // scale the points
        double scalefactor = a / entry.a;
        int angleIndex = ((int) Math.round((phi / Math.PI) * 180)) % 360;
        EllipticPoint[] points = entry.pointsByAngle[angleIndex];
        EllipticPoint[] clonedPoints = new EllipticPoint[points.length];

        for(int i = 0; i < points.length; i++) {
            clonedPoints[i] = new EllipticPoint();
            clonedPoints[i].x = points[i].x * scalefactor;
            clonedPoints[i].y = points[i].y * scalefactor;
            clonedPoints[i].position = points[i].position;
            clonedPoints[i].distanceToCenter = points[i].distanceToCenter * scalefactor;
        }
        // translate into center of ellipse
        for(int i = 0; i < points.length; i++) {
            // add x0 and y0
            clonedPoints[i].x += x0;
            clonedPoints[i].y += y0;
        }
        return clonedPoints;
    }

}
