/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBP;

import jceliac.tools.filter.scalespace.ScalespaceParameters;
import jceliac.tools.filter.scalespace.ScalespacePoint;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Represents the distribution of scales of an image.
 * <p>
 * @author shegen
 */
public class ScaleDistribution
{

    private final ScalespaceParameters scalespaceParameters;
    private final ArrayList<ScalespacePoint> allInterestPoints;
    private final HashMap<Integer, Integer> scaleCounts;
    private final HashMap<Integer, Double> scaleProbabilities;

    private final int imageDimension;

    public ScaleDistribution(ScalespaceParameters scalespaceParameters, int imageDimension)
    {
        this.scalespaceParameters = scalespaceParameters;
        this.allInterestPoints = new ArrayList<>();
        this.scaleCounts = new HashMap<>();
        this.scaleProbabilities = new HashMap<>();
        this.imageDimension = imageDimension;

    }

    public void addInterestpointsToDistribution(ArrayList<ScalespacePoint> ips)
    {
        this.allInterestPoints.addAll(ips);
    }

    /**
     * Computes the distribution of scales.
     * <p>
     */
    public void computeScaleDistribution()
    {
        int allCount = 0;
        for(ScalespacePoint tmp : this.allInterestPoints) {
            int scaleLevel = tmp.getScaleLevel();
            if(this.scaleCounts.containsKey(scaleLevel) == false) {
                this.scaleCounts.put(scaleLevel, 1);
            } else {
                Integer count = this.scaleCounts.get(scaleLevel);
                this.scaleCounts.put(scaleLevel, count + 1);
            }
            allCount++;
        }
        // compute all counts
        Set<Integer> keySet = this.scaleCounts.keySet();
        for(Integer key : keySet) {
            Integer count = this.scaleCounts.get(key);
            this.scaleProbabilities.put(key, (double) count / (double) allCount);
        }
    }

    /**
     * Normalizes the scale distribution.
     * <p>
     * there can not be the same number of blobs with larger radius due to the
     * fixed image size, we normalize this by weighting the sigma of the blob
     * into the distribution.
     * <p>
     * <p>
     * @param frameCount Number of frames used to compute the distribution.
     */
    public void computeNormalizedDistribution(double frameCount)
    {
        int allCount = 0;
        for(ScalespacePoint tmp : this.allInterestPoints) {
            int scaleLevel = tmp.getScaleLevel();
            if(this.scaleCounts.containsKey(scaleLevel) == false) {
                this.scaleCounts.put(scaleLevel, 1);
            } else {
                Integer count = this.scaleCounts.get(scaleLevel);
                this.scaleCounts.put(scaleLevel, count + 1);
            }
        }
        this.normalizeDistribution(frameCount);
        for(Integer key : this.scaleCounts.keySet()) {
            allCount += this.scaleCounts.get(key);
        }
        // compute all counts
        Set<Integer> keySet = this.scaleCounts.keySet();
        for(Integer key : keySet) {
            Integer count = this.scaleCounts.get(key);
            this.scaleProbabilities.put(key, (double) count / (double) allCount);
        }
    }

    /**
     * Normalize the distribution.
     * <p>
     * @param frameCount Number of frames used to compute the distribution.
     */
    private void normalizeDistribution(double frameCount)
    {
        // find out the number of maxima possible at the smallest scale
        double smallestScaleRadius = this.scalespaceParameters.getSigmaForScaleLevel(0) * Math.sqrt(2.0d);
        double smallestArea = smallestScaleRadius * smallestScaleRadius * Math.PI;
        double numOfPossibleMaxima = (((double) this.imageDimension * (double) this.imageDimension) * frameCount) / smallestArea;

        for(Integer scaleLevel : this.scaleCounts.keySet()) {
            double curScaleRadius = this.scalespaceParameters.getSigmaForScaleLevel(scaleLevel) * Math.sqrt(2.0d);
            double curNumOfPossibleMaxima = (((double) this.imageDimension * (double) this.imageDimension) * frameCount) / (curScaleRadius * curScaleRadius * Math.PI);
            double normFactor = numOfPossibleMaxima / curNumOfPossibleMaxima;
            int curNumMaxima = this.scaleCounts.get(scaleLevel);
            this.scaleCounts.put(scaleLevel, (int) Math.round(((double) curNumMaxima) * normFactor));
        }
    }

    /**
     * Get the probability of a scale level to be present.
     * <p>
     * @param scaleLevel Scale level in scale-space.
     * <p>
     * @return Probability of scale level.
     */
    public double getProbability(int scaleLevel)
    {
        if(this.scaleProbabilities.containsKey(scaleLevel) == false) {
            return 0;
        }
        return this.scaleProbabilities.get(scaleLevel);
    }

    public HashMap<Integer, Double> getDistribution()
    {
        return this.scaleProbabilities;
    }

    /**
     * Compute the dominant scale level, which is the scale with the highest
     * probability.
     * <p>
     * @return Scale level with highest probability.
     */
    public int getDominantScaleLevel()
    {
        double maxProb = 0;
        int dominantScaleLevel = -100;

        for(Integer scaleLevel : this.scaleProbabilities.keySet()) {
            double prob = this.scaleProbabilities.get(scaleLevel);
            if(prob > maxProb) {
                maxProb = prob;
                dominantScaleLevel = scaleLevel;
            }
        }
        return dominantScaleLevel;
    }
}
