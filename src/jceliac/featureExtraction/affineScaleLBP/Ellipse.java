/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBP;

import jceliac.tools.filter.scalespace.SecondMomentMatrix;
import org.apache.commons.math3.linear.RealVector;
import jceliac.JCeliacGenericException;
import jceliac.tools.math.MatrixTools;
import jceliac.tools.math.MathTools;
import java.util.Map.Entry;

/**
 *
 * @author shegen
 */
public class Ellipse
{

    public double a;
    public double b;
    public double phi; // this will always be the angle between localA and the x-axis

    private RealVector[] storedEigenvectors;
    private double[] storedEigenvalues;

    public static final int AREA_NORMALIZATION_MODE = 1;
    public static final int CIRCUMFERENCE_NORMALIZATION_MODE = 2;
    public static final int CIRCUMFERENCE_INCREASE_ISOTROPY = 3;
    public static final int AXIS_SUM_NORMALIZATION_MODE = 4;
    public static final int NO_NORMALIZATION = 0;

    SecondMomentMatrix storedSmm;

    //  public static HashMap <Integer,Double> acosLookup;
    public static double[] acosLookupFast;

    static {
        Ellipse.acosLookupFast = new double[2000];

        for(double arg = -1; arg <= 1; arg += 0.001) {
            Ellipse.acosLookupFast[(int) (1000.0d * arg + 1000.0d)] = Math.acos(arg);
        }
    }

    public Ellipse()
    {
    }

    public Ellipse(double a, double b, double phi)
    {
        this.a = a;
        this.b = b;
        this.phi = phi;
    }

    // see also smm2ell.m 
    public Ellipse(SecondMomentMatrix matrix, double radius, int normalizationMode)
            throws JCeliacGenericException
    {

        this.storedSmm = matrix;

        /* DO NOT INVERT! */
        double[][] m = new double[2][2];
        m[0][0] = matrix.getA();
        m[0][1] = matrix.getB();
        m[1][0] = matrix.getC();
        m[1][1] = matrix.getD();

        if(normalizationMode == AREA_NORMALIZATION_MODE) {
            double sqrtdet = Math.sqrt(MatrixTools.determinant(m));

            // the inverse of localA 2x2 matrix is 1/det * [c -localB; -localB localA], division by the sqrt of the det of the inverse
            // the determinant cancels out and the normalization will be sqrt(localA*c-localB^2);
            // this leaves an ellipse with the area of PI
            m[0][0] /= sqrtdet;
            m[0][1] /= sqrtdet;
            m[1][0] /= sqrtdet;
            m[1][1] /= sqrtdet;

            // scale the ellipse to have the same area as localA circle with the radius; by using this
            // we ensure that the support of the ellipse is the same as the support of the blob!
            m[0][0] *= radius;
            m[0][1] *= radius;
            m[1][0] *= radius;
            m[1][1] *= radius;

            /* double [] eigenvalues = MatrixTools.eigenvalues(m); RealVector []
             * eigenvectors = MatrixTools.eigenvectors(m);
             */
            Entry<RealVector[], double[]> e = MatrixTools.eigensolution(m);
            double[] eigenvalues = e.getValue();
            RealVector[] eigenvectors = e.getKey();

            double aVal = eigenvalues[0];
            double bVal = eigenvalues[1];

            // pick the first eigenvectors to have the rotation of the first axis, this is correct
            // as the parametric form of the ellipse implicitly is based on the rotation of the first argument (localA)
            // note that we can optimize the computation of the angle with the x-axis:
            // the inner product will only use the x-component of the vector, also the vector [1,0] is of unit size
            //  eigenvectors[0].unitize(); eigensolution is based on unitized vectors anyway
            double phiVal = Ellipse.acosLookupFast[(int) (1000.0d * eigenvectors[0].getEntry(0) + 1000.0d)];

            // the arcus cosine is ambiguous between [0;2*pi] therefore make sure that 
            // vectors in the 3-rd and 4-th quadrant are in angles ccw from the positive x-axis
            if(eigenvectors[0].getEntry(1) < 0) {
                phiVal = (2 * (Math.PI) - phiVal);
            }
            this.a = aVal;
            this.b = bVal;
            this.phi = phiVal;

        } else if(normalizationMode == CIRCUMFERENCE_NORMALIZATION_MODE) {
            Entry<RealVector[], double[]> e = MatrixTools.eigensolution(m);
            double[] eigenvalues = e.getValue();
            RealVector[] eigenvectors = e.getKey();

            this.storedEigenvectors = eigenvectors;
            this.storedEigenvalues = eigenvalues;

            RealVector majorEigenvector;
            majorEigenvector = eigenvectors[0];

            double localA = eigenvalues[0];
            double localB = eigenvalues[1];

            double circlec = 2 * Math.PI * radius;

            // solve the equation 2*radius*pi = circumference of ellipse approximation for localA*c, localB*c
            // for localA constant scaling c
            double commonTerm = (3 * (localA + localB) * radius + Math.sqrt((3 * localA + localB) * (localA + 3 * localB) * (radius * radius)));
            double c1 = (2 * radius * radius) / commonTerm;
            double c2 = commonTerm / (3 * (localA * localA) + 4 * localA * localB + 3 * (localB * localB));

            double scaleConstant;
            double ellc1 = MathTools.ellipseCircumference(localA * c1, localB * c1);
            if(Math.abs(circlec - ellc1) < 0.0001) {
                scaleConstant = c1;
            } else {
                scaleConstant = c2;
            }
            this.a = localA * scaleConstant;
            this.b = localB * scaleConstant;

            // round the ellipse axis lengths to 3 digits to improve lookup table efficiency
            this.a = (int) (this.a * 1000.0d);
            this.a = this.a / 1000.0d;

            this.b = (int) (this.b * 1000.0d);
            this.b = this.b / 1000.0d;

            // pick the first eigenvectors to have the rotation of the first axis, this is correct
            // as the parametric form of the ellipse implicitly is based on the rotation of the first argument (localA)
            // note that we can optimize the computation of the angle with the x-axis:
            // the inner product will only use the x-component of the vector, also the vector [1,0] is of unit size
            majorEigenvector.unitize();
            //double phiVal2 = Math.acos(majorEigenvector.getEntry(0));
            // acos was too slow (600 ms per 128x128 image) we therefore use localA lookup
            // double phiVal2 = Ellipse.acosLookup.get((int)(eigenvectors[0].getEntry(0)*1000.0d));
            int index = (int) (1000.0d * majorEigenvector.getEntry(0) + 1000.0d);
            double phiVal;
            if(index < Ellipse.acosLookupFast.length) {
                phiVal = Ellipse.acosLookupFast[index];
            } else {
                phiVal = Math.acos(majorEigenvector.getEntry(0));
            }

            // the arcus cosine is ambiguous between [0;2*pi] therefore make sure that 
            // vectors in the 3-rd and 4-th quadrant are in angles ccw from the positive x-axis
            if(majorEigenvector.getEntry(1) < 0) {
                phiVal = (2 * (Math.PI) - phiVal);
            }
            this.phi = phiVal;
        } else if(normalizationMode == NO_NORMALIZATION) {
            Entry<RealVector[], double[]> e = MatrixTools.eigensolution(m);
            double[] eigenvalues = e.getValue();
            RealVector[] eigenvectors = e.getKey();

            double localA = eigenvalues[0];
            double localB = eigenvalues[1];

            this.a = localA;
            this.b = localB;

            // pick the first eigenvectors to have the rotation of the first axis, this is correct
            // as the parametric form of the ellipse implicitly is based on the rotation of the first argument (localA)
            // note that we can optimize the computation of the angle with the x-axis:
            // the inner product will only use the x-component of the vector, also the vector [1,0] is of unit size
            eigenvectors[0].unitize();
            // double phiVal = Math.acos(eigenvectors[0].getEntry(0));
            // acos was too slow (600 ms per 128x128 image) we therefore use localA lookup
            //double phiVal = Ellipse.acosLookup.get((int)(eigenvectors[0].getEntry(0)*1000.0d));
            int index = (int) (1000.0d * eigenvectors[0].getEntry(0) + 1000.0d);
            double phiVal;
            if(index < Ellipse.acosLookupFast.length) {
                phiVal = Ellipse.acosLookupFast[index];
            } else {
                phiVal = Math.acos(eigenvectors[0].getEntry(0));
            }

            // the arcus cosine is ambiguous between [0;2*pi] therefore make sure that 
            // vectors in the 3-rd and 4-th quadrant are in angles ccw from the positive x-axis
            if(eigenvectors[0].getEntry(1) < 0) {
                phiVal = (2 * (Math.PI) - phiVal);
            }
            this.phi = phiVal;
        } else if(normalizationMode == AXIS_SUM_NORMALIZATION_MODE) {

            Entry<RealVector[], double[]> e = MatrixTools.eigensolution(m);
            double[] eigenvalues = e.getValue();
            RealVector[] eigenvectors = e.getKey();

            double localA = eigenvalues[0];
            double localB = eigenvalues[1];

            double scaleConstant = (2 * radius) / (localA + localB);
            this.a = localA * scaleConstant;
            this.b = localB * scaleConstant;

            eigenvectors[0].unitize();
            // double phiVal = Math.acos(eigenvectors[0].getEntry(0));
            // acos was too slow (600 ms per 128x128 image) we therefore use localA lookup
            //double phiVal = Ellipse.acosLookup.get((int)(eigenvectors[0].getEntry(0)*1000.0d));
            int index = (int) (1000.0d * eigenvectors[0].getEntry(0) + 1000.0d);
            double phiVal;
            if(index < Ellipse.acosLookupFast.length) {
                phiVal = Ellipse.acosLookupFast[index];
            } else {
                phiVal = Math.acos(eigenvectors[0].getEntry(0));
            }

            // the arcus cosine is ambiguous between [0;2*pi] therefore make sure that 
            // vectors in the 3-rd and 4-th quadrant are in angles ccw from the positive x-axis
            if(eigenvectors[0].getEntry(1) < 0) {
                phiVal = (2 * (Math.PI) - phiVal);
            }
            this.phi = phiVal;
        } 
        else {
            throw new JCeliacGenericException("Unknown Normalization Mode!");
        }
    }

    /**
     * Computes the LBP start point of the ellipse, used for debugging.
     * <p>
     * @return The x,y coorindates of the start point.
     */
    public double[] computeLBPStartPoint()
    {
        double startx, starty;

        if(this.storedEigenvectors == null) {
            return new double[]{0, 0};
        }
        if(this.a > this.b) {
            startx = this.a * this.storedEigenvectors[0].getEntry(0);
            starty = this.a * this.storedEigenvectors[0].getEntry(1);
        } else {
            startx = this.b * this.storedEigenvectors[1].getEntry(0);
            starty = this.b * this.storedEigenvectors[1].getEntry(1);
        }

        return new double[]{startx, starty};
    }

    /**
     * Normalize the ellipse. This expects sane values for a and b.
     * <p>
     * @param radius            The reference radius to normalize to.
     * @param normalizationMode The mode of normalizeation.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public void normalize(double radius, int normalizationMode)
            throws JCeliacGenericException
    {
        if(normalizationMode == CIRCUMFERENCE_NORMALIZATION_MODE) {
            double circlec = 2 * Math.PI * radius;

            // solve the equation 2*radius*pi = circumference of ellipse approximation for localA*c, localB*c
            // for localA constant scaling c
            double commonTerm = (3 * (a + b) * radius + Math.sqrt((3 * a + b) * (a + 3 * b) * (radius * radius)));
            double c1 = (2 * radius * radius) / commonTerm;
            double c2 = commonTerm / (3 * (a * a) + 4 * a * b + 3 * (b * b));

            double scaleConstant;
            double ellc1 = MathTools.ellipseCircumference(a * c1, b * c1);
            if(Math.abs(circlec - ellc1) < 0.0001) {
                scaleConstant = c1;
            } else {
                scaleConstant = c2;
            }
            this.a = a * scaleConstant;
            this.b = b * scaleConstant;

            // round the ellipse axis lengths to 3 digits to improve lookup table efficiency
            this.a = (int) (this.a * 1000.0d);
            this.a = this.a / 1000.0d;

            this.b = (int) (this.b * 1000.0d);
            this.b = this.b / 1000.0d;
        } else {
            throw new JCeliacGenericException("Normalization Mode not supported!");
        }
    }

    /**
     * Computes the foci of the ellipse.
     * <p>
     * @param x0 Center x-coordinates.
     * @param y0 Center y-coordinates.
     * <p>
     * @return The foci as elliptic points.
     */
    public EllipticPoint[] getFoci(int x0, int y0)
    {
        double max = Math.max(this.a, this.b);
        double min = Math.min(this.a, this.b);

        double focusDistance = Math.sqrt(max * max - min * min);
        EllipticPoint focus1 = new EllipticPoint();
        EllipticPoint focus2 = new EllipticPoint();
        if(this.a > this.b) {
            focus1.x = x0 + focusDistance;
            focus1.y = y0 + 0;
            focus2.x = x0 - focusDistance;
            focus2.y = y0 + 0;
        } else {
            focus1.x = x0 + 0;
            focus1.y = y0 + focusDistance;
            focus2.x = x0 + 0;
            focus2.y = y0 - focusDistance;
        }
        EllipticPoint[] ret = {focus1, focus2};
        return ret;
    }

    public double area()
    {
        return (this.a * this.b * Math.PI);
    }

    /**
     * Sanity check for debugging.
     * <p>
     * @param points Point on of an ellipse.
     * <p>
     * @return True if sane, false if not.
     */
    public static boolean validateDistributedPoints(EllipticPoint[] points)
    {
        double maxDistance = Double.MIN_VALUE;
        double minDistance = Double.MAX_VALUE;

        for(EllipticPoint point : points) {
            if(point.distanceToCenter > maxDistance) {
                maxDistance = point.distanceToCenter;
            }
            if(point.distanceToCenter < minDistance) {
                minDistance = point.distanceToCenter;
            }
        }

        if(Math.abs(points[0].distanceToCenter - maxDistance) > 0.001) {
            return false;
        }
        if(Math.abs(points[4].distanceToCenter - maxDistance) > 0.001) {
            return false;
        }
        if(Math.abs(points[2].distanceToCenter - minDistance) > 0.001) {
            return false;
        }
        if(Math.abs(points[6].distanceToCenter - minDistance) > 0.001) {
            return false;
        }
        if(Math.abs(points[1].distanceToCenter - points[7].distanceToCenter) > 0.001) {
            return false;
        }
        if(Math.abs(points[1].distanceToCenter - points[5].distanceToCenter) > 0.001) {
            return false;
        }
        if(Math.abs(points[1].distanceToCenter - points[3].distanceToCenter) > 0.001) {
            return false;
        }
        if(Math.abs(points[3].distanceToCenter - points[5].distanceToCenter) > 0.001) {
            return false;
        }
        if(Math.abs(points[3].distanceToCenter - points[7].distanceToCenter) > 0.001) {
            return false;
        }
        return Math.abs(points[5].distanceToCenter - points[7].distanceToCenter) <= 0.001;
    }

    /**
     * Computes an isotropy measure. This is the ratio of axes.
     * <p>
     * @return Isotropy measure, 0 is maximum isotropy 1 is maximum anisotropy.
     */
    public double getIsotropyMeasure()
    {
        double Q = 1.0d - (Math.min(this.a, this.b) / Math.max(this.a, this.b));
        return Q;
    }

    /**
     * Increases isotropy of an ellipse using a semi linear mapping. This is
     * part of experimental code.
     * <p>
     * @param radius Reference radius.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public void increaseIsotropy(double radius)
            throws JCeliacGenericException
    {
        double isotropy = (Math.min(this.a, this.b) / Math.max(this.a, this.b));
        double mappedIsotropy = (isotropy * 0.5) + 0.5;

        double k = (Math.max(this.a, this.b) * mappedIsotropy) / Math.min(this.a, this.b);
        if(this.a < this.b) {
            this.a *= k;
        } else {
            this.b *= k;
        }
        this.normalize(radius, Ellipse.CIRCUMFERENCE_NORMALIZATION_MODE);
    }
}
