/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction;

import jceliac.classification.kernelSVM.SVMFeatureVector;
import jceliac.JCeliacGenericException;
import jceliac.featureOptimization.*;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import jceliac.experiment.*;
import jceliac.logging.*;
import jceliac.tools.arrays.ArrayTools;

/**
 * This implements the high level feature abstraction.
 * <p>
 * @author shegen
 */
public abstract class DiscriminativeFeatures
        implements Comparable<DiscriminativeFeatures>, Externalizable
{
    // this uniquely identifies the feature based on content
    protected long featureID = -1;
    
    // Info about extracted signal, e.g. Image name
    protected String signalIdentifier;
    protected int frameNumber;

    // the class this feature was assigned to
    protected int classNumber;
    protected String classIdentifier;

    protected FeatureExtractionParameters featureExtractionParameters;

    // contains the actual feature data, the specific class should know how
    // to handle it, this can be extended for special use by specific feature mechanisms
    protected ArrayList <AbstractFeatureVector> featureVectors = new ArrayList <>();
    
    // local feature vectors, either by pixel or by window or something else, this is
    // used by the generative models to train models 
    protected ArrayList <FixedFeatureVector> localFeatureVectors; 
                 
    protected double [] allRawFeatureData; 
    
    private final ConcurrentHashMap<Integer, double[]> subsetCache = new ConcurrentHashMap <>();
    private final int MAX_CACHE_SIZE = 1;
    
    private static long NEXT_UNIQUE_ID = 0;
    
    
    public double featureComputationTime = -1;
    public double scalespaceAndEstimationTime = -1;
    public double totalComputationTime = -1;
    public double orientationTime = -1;
    
    public synchronized static long retrieveUniqueID()
    {
        long ret = DiscriminativeFeatures.NEXT_UNIQUE_ID;
        DiscriminativeFeatures.NEXT_UNIQUE_ID++;
        return ret;
    }
    
    
    public DiscriminativeFeatures(FeatureExtractionParameters parameters)
    {
        this.featureExtractionParameters = parameters;
        this.featureID = DiscriminativeFeatures.retrieveUniqueID();
    }

   /**
     * Needed by Externalizable, do not initialize feature ID, these
     * features are not useable and can only be exported. 
     * <p>
     */
    public DiscriminativeFeatures()
    {
        this.featureID = DiscriminativeFeatures.retrieveUniqueID();
        this.featureExtractionParameters = null;
    }
    
    public long getFeatureID()
    {
        if(this.featureID == -1) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Invalid feature ID, featuer is not properly initialized!");
        }
        return this.featureID;
    }

    public String getSignalIdentifier()
    {
        return this.signalIdentifier;
    }

    public void setSignalIdentifier(String absolutePath)
    {
        this.signalIdentifier = absolutePath;
    }

    public void setFrameNumber(int num)
    {
        this.frameNumber = num;
    }

    public int getFrameNumber()
    {
        return this.frameNumber;
    }

    public int getClassNumber()
    {
        return classNumber;
    }

    public void setClassNumber(int classNumber)
    {
        this.classNumber = classNumber;
    }

    public String getClassIdentifier()
    {
        return classIdentifier;
    }

    public void setClassIdentifier(String classIdentifier)
    {
        this.classIdentifier = classIdentifier;
    }

    
    /**
     * Creates a copy of this feature but without any feature vector data,
     * only the meta data are copied, the feature data is then set later, 
     * this is used for the generative model feature mappings. 
     * 
     * @return 
     */
    public abstract DiscriminativeFeatures cloneEmptyFeature();
    
    /**
     * Copies the current feature and returns it using only the requested
     * subset of the feature vector, this is supposed to be thread safe
     * as compared to the old mechanism using setCurrentDynamicFixedFeatureVectorSubset().
     * 
     * @param subset
     * @return 
     * @throws jceliac.JCeliacGenericException 
     */
    public synchronized DiscriminativeFeatures getClonedFeatureSubset(FeatureVectorSubsetEncoding subset) 
              throws JCeliacGenericException 
    {
        if(subset.allSet()) {
            return this;
        }
        return this.cloneFeatureSubset(subset);
    }
    
    protected abstract DiscriminativeFeatures cloneFeatureSubset(FeatureVectorSubsetEncoding subset) 
              throws JCeliacGenericException;
    
    
    public int getFeatureVectorDimensionality() 
    {
        return this.featureVectors.size();
    }
    
    /**
     * Returns the raw feature data as copy. 
     * 
     * @return
     * @throws JCeliacGenericException 
     */
    public synchronized double [] getAllFeatureVectorData()
            throws JCeliacGenericException
    {
        if(this.allRawFeatureData != null) {
            return this.allRawFeatureData;
        }
        // build feature Vector
        int len = 0; // it's possible that single features have different dimensionality
        for(int index = 0; index < this.featureVectors.size(); index++) {
            len += this.featureVectors.get(index).getFeatureData().length;
        }
        double[] localArray = new double[len];
        int pos = 0;
        for(int index = 0; index < this.featureVectors.size(); index++) {
            double[] feature = this.featureVectors.get(index).getFeatureData();
            System.arraycopy(feature, 0, localArray, pos, feature.length);
            pos += feature.length;
        }
        this.allRawFeatureData = localArray;
        return localArray;
    }
    
   /**
     * Returns the requested feature subset as a copy of the data. 
     * <p>
     * @param subset Feature subset.
     * <p>
     * @return Raw feature vector data.
     * <p>
     * @throws JCeliacGenericException
     */
     protected synchronized double [] getFeatureVectorSubsetData(FeatureVectorSubsetEncoding subset)
            throws JCeliacGenericException
    {
        FeatureVectorSubsetEncoding localSubset = subset;
        if(localSubset == null) { // return the entire feature vector
            localSubset = new FeatureVectorSubsetEncoding(this.getFeatureVectorDimensionality());
            localSubset.setAll();
        }
        if(this.subsetCache.containsKey(localSubset.hashCode())) {
            double[] cachedHist = this.subsetCache.get(localSubset.hashCode());
            return cachedHist;
        }
        // build feature Vector
        int len = 0; // it's possible that single features have different dimensionality
        for(int index = 0; index < this.featureVectors.size(); index++) {
            if(localSubset.isSet(index)) {
                len += this.featureVectors.get(index).getFeatureData().length;
            }
        }
        // bad feature subset requested
        if(len == 0) { 
             throw new JCeliacGenericException("Bad Feature Subset Requested!");
        }
        double[] localArray = new double[len];
        int pos = 0;
        for(int index = 0; index < this.featureVectors.size(); index++) {
            if(localSubset.isSet(index)) {
                double[] feature = this.featureVectors.get(index).getFeatureData();
                System.arraycopy(feature, 0, localArray, pos, feature.length);
                pos += feature.length;
            }
        }
        // if the cache has size put it in the cache, else clear the cache
        if(this.subsetCache.size() > MAX_CACHE_SIZE) {
            this.subsetCache.clear();
        }
        double[] clonedArray = localArray.clone();
        this.subsetCache.put(localSubset.hashCode(), clonedArray);
        return localArray;
    }
    
    
    /**
     * Computes distance to other feature vector.
     * <p>
     * @param them         Other feature vector
     * <p>
     * @return Distance to other feature vector.
     * <p>
     * @throws JCeliacGenericException
     */
    public double distanceTo(DiscriminativeFeatures them)
            throws JCeliacGenericException
    {
        // make sure the distance to this item is never computed, never
        // use two frames from the same movie for classifying each other also!
        if(them.signalIdentifier.equalsIgnoreCase(this.signalIdentifier)) {
            //Experiment.log(JCeliacLogger.LOG_WARN, "Warning! OVERFITTING! Same feature marked as test and train feature! (%s;%s)", this.getSignalIdentifier(), them.getSignalIdentifier());
            double distance = this.doDistanceTo(them);
            return distance;
        } else {
            return doDistanceTo(them);
        }
    }

    /**
     * Actual implementation of the distance computation, some methods (like
     * SALBP and SOALBP use a different distance computation).
     * <p>
     * @param them   Other feature vector.
     * <p>
     * @return Distance to other feature vector.
     * <p>
     * @throws JCeliacGenericException
     */
    public abstract double doDistanceTo(DiscriminativeFeatures them)
            throws JCeliacGenericException;
    

    public synchronized  ArrayList <AbstractFeatureVector> getFeatureVectors()
    {
        return this.featureVectors;
    }
    
   /* This type of feature is based on standard discriminative feature but
    * is able to supply local feature vectors.This is used to train a generative 
    * model like Bag-of-Words.The local feature semantic is dependant on the 
    * specific feature type. 
    */
    public synchronized ArrayList <FixedFeatureVector> getLocalFeatureVectors() 
    {
        return this.localFeatureVectors;
    }
    
    public abstract ArrayList <double []> getLocalFeatureVectorData();
    
    /**
     * Adds an abstract feature (depending on the implementation) to this set of
     * DiscriminativeFeatures. This can be a histogram or an entire feature vector.
     * <p>
     * @param feature Feature data.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    
    public synchronized void addAbstractFeature(AbstractFeatureVector feature) throws
            JCeliacGenericException 
    {
        // this changes the feature content, hence acquire a new feature ID
        this.featureID = DiscriminativeFeatures.retrieveUniqueID();
        this.featureVectors.add(feature);
    }

    
    public synchronized void setLocalFeatureVectors(ArrayList <FixedFeatureVector> localFeatureVectors)
    {
        // this changes the feature content, hence acquire a new feature ID
        this.featureID = DiscriminativeFeatures.retrieveUniqueID();
        this.localFeatureVectors = localFeatureVectors;
    }
    
    public void flagAsTrainingFeature()
    {  
    }
    
    public void flagAsEvaluationFeature()
    {
    }
    
    /**
     * Compares two features, used for sorting features, this does not take
     * feature content into consideration!
     * <p>
     * @param o Other features.
     * <p>
     * @return 0 if no identifier is set
     */
    @Override
    public int compareTo(DiscriminativeFeatures o)
    {
        DiscriminativeFeatures them = o;
        String theirUniqueIdentifier = them.getSignalIdentifier();

        String uniqueIdentifier = this.signalIdentifier;
        // XXX this might not work with movies! because frames have the same signalID
        if(uniqueIdentifier == null || theirUniqueIdentifier == null) {
            Experiment.log(JCeliacLogger.LOG_WARN, "Identifier NOT set, in Feature! Sorting might be unreliable!");
            return 0;
        }
        return uniqueIdentifier.compareTo(theirUniqueIdentifier);

    }
    
    // normalize features linearly between 0 and 1
    protected void normalizeLinear(double [] data)
    {
        double min, max;

        max = ArrayTools.max(data);
        min = ArrayTools.min(data);
        double width = (max - min);
        double scaleFactor = (1.0d / width); 

        for(int y = 0; y < data.length; y++) {
            double k = data[y];
            double normalized = (k - min) * scaleFactor;
            data[y] = normalized;
        }
    }
    
    public synchronized SVMFeatureVector[] getNormalizedFeaturesForSVM()
            throws JCeliacGenericException
    {
        double[] data = this.getAllFeatureVectorData();
        this.normalizeLinear(data);
      
        SVMFeatureVector[] vector = new SVMFeatureVector[data.length];

        for(int index = 0; index < data.length; index++) {
            SVMFeatureVector cur = new SVMFeatureVector();
            cur.index = index + 1;
            cur.value = data[index];
            vector[index] = cur;
        }
        return vector;
    }

    
    /**
     * Implementation of the Externalizable interface write, only data needed by
     * McNemar Test is exported!
     * <p>
     * @param out
     * <p>
     * @throws IOException
     */
    @Override
    public void writeExternal(ObjectOutput out) throws IOException
    {
        out.writeObject(this.classNumber);
    }

    /**
     * Implementation of the Externalizable interface read
     * <p>
     * @param in
     * <p>
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void readExternal(ObjectInput in) throws IOException,
                                                    ClassNotFoundException
    {
        this.classNumber = (Integer) in.readObject();
    }
}
