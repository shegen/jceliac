/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.featureExtraction.corticalModel;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.Distances;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FixedFeatureVector;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;

/**
 *
 * @author shegen
 */
public class ICMFeatures 
    extends DiscriminativeFeatures 
{

    public ICMFeatures(FeatureExtractionParameters parameters)
    {
        super(parameters);
    }

    public ICMFeatures()
    {
        super();
    }

    @Override
    public int getFeatureVectorDimensionality()
    {
        return 1; // do not hamper with feature vector
    }

    @Override
    public double doDistanceTo(DiscriminativeFeatures them)
            throws JCeliacGenericException
    {
        return Distances.euclideanDistance(this.getAllFeatureVectorData(),
                                           them.getAllFeatureVectorData());
    }

    @Override
    public DiscriminativeFeatures cloneFeatureSubset(FeatureVectorSubsetEncoding subset)
            throws JCeliacGenericException
    {
        ICMFeatures featureSubset = new ICMFeatures();
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
        
        double [] rawFeatureCopy = this.getFeatureVectorSubsetData(subset);
        FixedFeatureVector vector = new FixedFeatureVector();
        vector.setData(rawFeatureCopy);
        featureSubset.addAbstractFeature(vector);
        return featureSubset;
    }

    @Override
    public DiscriminativeFeatures cloneEmptyFeature()
    {
        ICMFeatures featureSubset = new ICMFeatures();
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
        
        return featureSubset;
    }

    @Override
    public ArrayList<double[]> getLocalFeatureVectorData()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
