/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.corticalModel;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.StableCode;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.FixedFeatureVector;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.Frame;
import jceliac.tools.filter.DataFilter2D;
import jceliac.tools.math.MathTools;

/**
 *
 * @author shegen
 */
@StableCode(
        comment = "This code is ported based on gwimmers matlab implementation.",
        date = "29.01.2014",
        lastModified = "29.01.2014")
public class FeatureExtractionMethodCorticalModel
        extends FeatureExtractionMethod
{

    protected FeatureExtractionParametersCorticalModel myParameters;

    public FeatureExtractionMethodCorticalModel(FeatureExtractionParameters param)
    {
        super(param);
        this.myParameters = (FeatureExtractionParametersCorticalModel) param;
    }

    @Override
    public void initialize() throws JCeliacGenericException
    {
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {
        switch(this.myParameters.getModelType()) {
            case ICM:
                return this.extractFeaturesICM(content);
            case SCM:
                return this.extractFeaturesSCM(content);
            default:
                throw new JCeliacGenericException("Unknown ICM model type.");
        }
    }

    protected DiscriminativeFeatures extractFeaturesICM(Frame content)
            throws JCeliacGenericException
    {
        ArrayList<double[][]> icmRet;

        if(this.myParameters.isUseColor() == true) {
            icmRet = new ArrayList <>();
            for(Integer colorChannel : this.myParameters.getColorChannels()) {
                double[][] dataNormalized = this.normalizeChannel(content.getColorChannel(colorChannel));
                ArrayList<double[][]> ret = this.computeICM(dataNormalized);
                icmRet.addAll(ret);
            }
        } else {
            double[][] grayDataNormalized = this.normalizeChannel(content.getGrayData());
            icmRet = this.computeICM(grayDataNormalized);
        }
        double[] featureData = new double[icmRet.size()];
        int featureIndex = 0;
        for(int i = 0; i < icmRet.size(); i++) {
            double[][] binaryICMData = icmRet.get(i);
            /* The mean is (ones / number of pixel), this is the probability of
             * a pixel being 1.
             */
            double probability1 = ArrayTools.mean(binaryICMData);
            double probability0 = 1 - probability1;
            if(probability1 < 1e-16 || Math.abs(probability1 - 1.0d) < 1e-16) {
                featureData[i] = 0;
                continue;
            }
            // compute entropy
            double entropy = -probability1 * (Math.log(probability1) / Math.log(2.0d))
                             - probability0 * (Math.log(probability0) / Math.log(2.0d));
            
            featureData[featureIndex] = entropy;
            featureIndex++;
        }
        FixedFeatureVector featureVector = new FixedFeatureVector();
        featureVector.setData(featureData);
        ICMFeatures features = new ICMFeatures();
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(1);
        features.addAbstractFeature(featureVector);
        return features;
    }
    
    
    
        protected DiscriminativeFeatures extractFeaturesSCM(Frame content)
            throws JCeliacGenericException
    {
        ArrayList<double[][]> scmRet;

        if(this.myParameters.isUseColor() == true) {
            scmRet = new ArrayList <>();
            for(Integer colorChannel : this.myParameters.getColorChannels()) {
                double[][] dataNormalized = this.normalizeChannel(content.getColorChannel(colorChannel));
                ArrayList<double[][]> ret = this.computeSCM(dataNormalized);
                scmRet.addAll(ret);
            }
        } else {
            double[][] grayDataNormalized = this.normalizeChannel(content.getGrayData());
            scmRet = this.computeSCM(grayDataNormalized);
        }
        double[] featureDataEntropy = new double[scmRet.size()];
        double[] featureDataResidual = new double[scmRet.size()];
        double[] featureDataStd = new double[scmRet.size()];
        
        for(int i = 0; i < scmRet.size(); i++) {
            double[][] binarySCMData = scmRet.get(i);
            /* The mean is (ones / number of pixel), this is the probability of
             * a pixel being 1.
             */
            double probability1 = ArrayTools.mean(binarySCMData);
            double probability0 = 1 - probability1;
            if(probability1 < 1e-16 || Math.abs(probability1 - 1.0d) < 1e-16) {
                featureDataEntropy[i] = 0;
                
            }else {
                // compute entropy
                double entropy = -probability1 * (Math.log(probability1) / Math.log(2.0d))
                                 -probability0 * (Math.log(probability0) / Math.log(2.0d));
                featureDataEntropy[i] = entropy;
            }
            // compute average residual
            double mean = probability1;
            double resid = 0;
            for(int x = 0; x < binarySCMData.length;x++) {
                for(int y = 0; y < binarySCMData[0].length; y++) {
                    resid += Math.abs(binarySCMData[x][y] - mean);
                }
            }
            featureDataResidual[i] = resid;
            
            // compute standardDeviation
            double standardDeviation = MathTools.stdev(binarySCMData);
            featureDataStd[i] = standardDeviation;
        }
        // normalize features and concatenate
        double sumEntropy = ArrayTools.sum(featureDataEntropy);
        double sumResidual = ArrayTools.sum(featureDataResidual);
        double sumStd = ArrayTools.sum(featureDataStd);
        
        double [] featureDataConcat = new double[scmRet.size()*3];
        for(int i = 0; i < scmRet.size(); i++) {
            featureDataConcat[i*3] = featureDataEntropy[i] / sumEntropy;
            featureDataConcat[(i*3)+1] = featureDataResidual[i] / sumResidual;
            featureDataConcat[(i*3)+2] = featureDataStd[i] / sumStd;
        }
        FixedFeatureVector featureVector = new FixedFeatureVector();
        featureVector.setData(featureDataConcat);
        ICMFeatures features = new ICMFeatures();
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(1);
        features.addAbstractFeature(featureVector);
        return features;
    }

    /**
     * Normalize to zero mean, and unit variance.
     * <p>
     * @param data Image data to normalize.
     * <p>
     * @return Normalized copy of data.
     */
    private double[][] normalizeChannel(double[][] data)
    {
        double dataMean = ArrayTools.mean(data);
        double dataStd = MathTools.stdev(data);

        double[][] normalizedData = new double[data.length][data[0].length];
        for(int i = 0; i < normalizedData.length; i++) {
            for(int j = 0; j < normalizedData[0].length; j++) {
                normalizedData[i][j] = (data[i][j] - dataMean) / dataStd;
            }
        }
        return normalizedData;
    }

    /**
     * Implements the ICM from: "Pulse coupled neural networks and one-class
     * support vector machines for geometry invariant texture retrieval".
     * <p>
     * @param input Raw image data.
     * <p>
     * @return Binary neuron outputs.
     * <p>
     * @throws JCeliacGenericException
     */
    private ArrayList<double[][]> computeICM(double[][] input)
            throws JCeliacGenericException
    {
        double[][] M = {{0.5, 1.0, 0.5}, {1, 0, 1}, {0.5, 1.0, 0.5}};
        double[][] F = new double[input.length][input[0].length];
        double[][] Y = new double[input.length][input[0].length];
        double[][] theta = new double[input.length][input[0].length];

        ArrayList<double[][]> binaryOutput = new ArrayList<>();

        double thetainit = this.myParameters.getThetainit();
        for(int i = 0; i < theta.length; i++) {
            for(int j = 0; j < theta[0].length; j++) {
                theta[i][j] = thetainit;
            }
        }

        double f = this.myParameters.getF();
        double g = this.myParameters.getG();
        double h = this.myParameters.getH();

        for(int iteration = 0;
            iteration < this.myParameters.getNumberOfIterations();
            iteration++) {
            double[][] W = DataFilter2D.convolve(Y, M, DataFilter2D.BORDER_MIRROR);
            for(int i = 0; i < F.length; i++) {
                for(int j = 0; j < F[0].length; j++) {
                    F[i][j] = f * F[i][j] + W[i][j] + input[i][j];
                    theta[i][j] = g * theta[i][j] + h * Y[i][j];
                    Y[i][j] = F[i][j] > theta[i][j] ? 1 : 0;
                }
            }
            double[][] outputCopy = new double[Y.length][Y[0].length];
            for(int k = 0; k < outputCopy.length; k++) {
                System.arraycopy(Y[k], 0, outputCopy[k], 0, outputCopy[k].length);
            }
            binaryOutput.add(outputCopy);
        }
        return binaryOutput;
    }

    
   /**
     * Implements the SCM from: 
     * "New Spiking Cortical Model for Invariant Texture Retrieval and Image Processing".
     * <p>
     * @param input Raw image data.
     * <p>
     * @return Binary neuron outputs.
     * <p>
     * @throws JCeliacGenericException
     */
    private ArrayList<double[][]> computeSCM(double[][] input)
            throws JCeliacGenericException
    {
        double[][] M = {{0.1091, 0.1409, 0.1091}, {0.1409, 0, 0.1409}, {0.1091, 0.1409, 0.1091}};
        
        double[][] F = new double[input.length][input[0].length];
        double[][] Y = new double[input.length][input[0].length];
        double[][] theta = new double[input.length][input[0].length]; // initialized as 0 here

        ArrayList<double[][]> binaryOutput = new ArrayList<>();

        double f = this.myParameters.getF();
        double g = this.myParameters.getG();
        double h = this.myParameters.getH();

        for(int iteration = 0;
            iteration < this.myParameters.getNumberOfIterations();
            iteration++) {
            double[][] W = DataFilter2D.convolve(Y, M, DataFilter2D.BORDER_MIRROR);
            for(int i = 0; i < F.length; i++) {
                for(int j = 0; j < F[0].length; j++) {
                    F[i][j] = f * F[i][j] + input[i][j] * W[i][j] + input[i][j];
                    theta[i][j] = g * theta[i][j] + h * Y[i][j];
                    Y[i][j] = F[i][j] > theta[i][j] ? 1 : 0;
                }
            }
            double[][] outputCopy = new double[Y.length][Y[0].length];
            for(int k = 0; k < outputCopy.length; k++) {
                System.arraycopy(Y[k], 0, outputCopy[k], 0, outputCopy[k].length);
            }
            binaryOutput.add(outputCopy);
        }
        return binaryOutput;
    }
    
    
    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass)
            throws JCeliacGenericException
    {

    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {

    }
}
