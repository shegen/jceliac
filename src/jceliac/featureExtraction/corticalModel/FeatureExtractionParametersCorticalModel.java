/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.featureExtraction.corticalModel;

import jceliac.experiment.Experiment;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersCorticalModel extends FeatureExtractionParameters
{

    // either ICM or SCM
    protected String modelType = "ICM";
    
    /* Initial value of threshold, couldn't find anything in the paper using 
     * georgs value used in the implementation.
     */ 
    protected double thetainit = 0.5d; 

    /**
     * Number of iterations to compute the firings of the neurons, 
     * this is equal to the dimension of the feature vector, default value
     * in the paper (there it is called N) is 37. 
     */
    protected int numberOfIterations = 37;
    
    protected double f = 0.9; 
    protected double g = 0.8; // weight for ICM, value from paper
    protected double h = 20; // weight for ICM, value from paper

    
    public FeatureExtractionParametersCorticalModel(ECorticalModelType type)
    {
        this.modelType = type.name();
        switch(type) {
            case ICM:
                this.f = 0.9;// weight for ICM, value from paper
                this.g = 0.8;// weight for ICM, value from paper
                this.h = 20;// weight for ICM, value from paper
                this.thetainit = 0.5d;
                this.numberOfIterations = 37;
                break;
            case SCM:
                this.f = 0.2;
                this.g = 0.9;
                this.h = 20;
                this.numberOfIterations = 37;
                break;
        }
        
    }
    
    public enum ECorticalModelType {
        ICM,
        SCM
    };

    public void setModelType(String modelType)
    {
        this.modelType = modelType;
    }

    public ECorticalModelType getModelType()
    {
        return ECorticalModelType.valueOf(this.modelType);
    }

    public double getThetainit()
    {
        return thetainit;
    }

    public int getNumberOfIterations()
    {
        return numberOfIterations;
    }

    public double getF()
    {
        return f;
    }

    public double getG()
    {
        return g;
    }

    public double getH()
    {
        return h;
    }
    
    @Override
    public void report()
    {
        super.report();
        Experiment.log(JCeliacLogger.LOG_INFO, "Theta-init: "+ this.thetainit);
        Experiment.log(JCeliacLogger.LOG_INFO, "f: " + this.f);
        Experiment.log(JCeliacLogger.LOG_INFO, "g: " + this.g);
        Experiment.log(JCeliacLogger.LOG_INFO, "h: " + this.h);
        Experiment.log(JCeliacLogger.LOG_INFO, "Iterations: " + this.numberOfIterations);
    }

    @Override
    public String getMethodName()
    {
        return this.modelType;
    }
    
    
    
    
}
