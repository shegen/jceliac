/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction;

import java.util.concurrent.Callable;
import jceliac.JCeliacGenericException;
import jceliac.tools.data.Frame;
import jceliac.experiment.*;

/**
 *
 * @author shegen
 */
public abstract class AbstractFeatureExtractionThread
        implements Callable<DiscriminativeFeatures>
{

    protected FeatureExtractionMethod parentMethod;
    protected Frame content;
    protected DiscriminativeFeatures extractedFeatures;
    protected int classNumber;
    protected IThreadedExperiment experiment;
    protected boolean isOneClassTrainingClass;
    protected boolean isTraining;

    public AbstractFeatureExtractionThread(IThreadedExperiment experiment,
                                   FeatureExtractionMethod parentMethod,
                                   Frame content, int classNumber, boolean isOneClassTrainingClass,
                                   boolean isTraining)
    {
        this.parentMethod = parentMethod;
        this.content = content;
        this.classNumber = classNumber;
        this.experiment = experiment;
        this.isOneClassTrainingClass = isOneClassTrainingClass;
        this.isTraining = isTraining;

    }

    public abstract DiscriminativeFeatures extractFeatures(Frame content, boolean isTraining)
            throws JCeliacGenericException;

    public DiscriminativeFeatures getExtractedFeatures()
    {
        this.extractedFeatures.setClassNumber(this.classNumber);
        return this.extractedFeatures;
    }

    /**
     * Performs the feature extraction in a separate thread, able to throw
     * exceptions to the main thread.
     * <p>
     * @return Extracted features.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    @Override
    public DiscriminativeFeatures call()
            throws JCeliacGenericException
    {

        this.extractedFeatures = this.extractFeatures(this.content, isTraining);
        this.extractedFeatures.setClassNumber(this.classNumber);
        return this.extractedFeatures;
    }

}
