/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.LBP;

import jceliac.featureExtraction.SALBP.FeatureExtractionParametersScaleAdaptiveLBP;
import jceliac.featureExtraction.SALBP.LBPNeighborLookup;
import jceliac.featureExtraction.affineScaleLBPScaleBias.IAffineAdaptiveFeatureExtractionMethod;
import jceliac.featureExtraction.SALBP.IScaleAdaptiveFeatureExtractionMethod;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ScaleEstimation;
import jceliac.featureExtraction.SALBP.LBPScaleFilteredDataSupplier;
import jceliac.featureExtraction.affineScaleLBPScaleBias.AffineFilteredNeighbor;
import jceliac.featureExtraction.SALBP.LBPScaleBias;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.*;
import jceliac.tools.filter.*;
import jceliac.experiment.*;
import jceliac.tools.data.*;
import jceliac.logging.*;
import java.util.*;
import jceliac.featureExtraction.affineScaleLBP.EllipticPoint;
import jceliac.featureExtraction.affineScaleLBP.EllipticPointLookup;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.OrientationEstimation;

/**
 * Computes the standard Local Binary Patterns features.
 * <p>
 * @author shegen
 */
@StableCode(comment = "This code is trusted.",
            date = "16.01.2014",
            lastModified = "16.01.2014")
public class FeatureExtractionMethodLBP
        extends FeatureExtractionMethod
        implements IScaleAdaptiveFeatureExtractionMethod,
                   IAffineAdaptiveFeatureExtractionMethod
{

    private final FeatureExtractionParametersLBP myParameters;
    protected double[][] sinLUT;
    protected double[][] cosLUT;

    protected FeatureExtractionParametersScaleAdaptiveLBP myParametersSA;
    
    public FeatureExtractionMethodLBP(FeatureExtractionParameters param)
    {
        super(param);
        // this is just for convenience
        this.myParameters = (FeatureExtractionParametersLBP) super.parameters;
    }

    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass)
            throws JCeliacGenericException
    {
        // nothing to do for LBP
    }

    /**
     * Initialize lookup tables for neighbor positions.
     * @throws jceliac.JCeliacGenericException
     */
    @Override
    public void initialize()
            throws JCeliacGenericException
    {
        this.sinLUT = new double[this.myParameters.getMaxScale()][this.myParameters.getNumberOfNeighbors()];
        this.cosLUT = new double[this.myParameters.getMaxScale()][this.myParameters.getNumberOfNeighbors()];

        for(int scale = 1; scale <= this.myParameters.getMaxScale(); scale++) {
            double radiusForScale = LBPMultiscale.getRadiusForScale(scale);
            for(int i = 0; i < this.myParameters.getNumberOfNeighbors(); i++) {
                /*
                 * We start with neighbor numer 1, this is because that's how it
                 * was described within my master thesis! Actually 0 should be
                 * ok too but does not neccessarily give the same results!
                 */
                this.sinLUT[scale - 1][i] = radiusForScale * Math.sin((2.0d * Math.PI * (double) (i + 1))
                                                                      / (double) this.myParameters.getNumberOfNeighbors());
                this.cosLUT[scale - 1][i] = radiusForScale * Math.cos((2.0d * Math.PI * (double) (i + 1))
                                                                      / (double) myParameters.getNumberOfNeighbors());
            }
        }

    }

    /**
     * Extract both training and evaluation features.
     * <p>
     * @param content Frame structure representing an image.
     * <p>
     * @return LBP features.
     * <p>
     * @throws JCeliacGenericException
     */
    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {
        Experiment.log(JCeliacLogger.LOG_HINFO, "Extracting LBP-Features from SignalID: %s, FrameID: %d.",
                       content.getSignalIdentifier(), content.getFrameIdentifier());

        // create DiscriminativeFeatures structure once for each input sample
        LBPFeatures features = new LBPFeatures(this.myParameters);
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());

        for(int currentScale = this.myParameters.getMinScale(); currentScale <= this.myParameters.getMaxScale();
            currentScale++) {
            if(this.myParameters.isUseColor()) {
                // Prepare content for scale.

                for(Integer colorChannel : this.myParameters.getColorChannels()) {
                    LBPHistogram hist = this.computeDistribution(content.getColorChannel(colorChannel), 
                                                                 currentScale);
                    hist.setColorChannel(colorChannel);
                    hist.setScale(currentScale);
                    features.addAbstractFeature(hist);
                
                }
            } else {
                LBPHistogram histGray = this.computeDistribution(content.getGrayData(), currentScale);
                histGray.setColorChannel(DataSource.CHANNEL_GRAY);
                histGray.setScale(currentScale);
                features.addAbstractFeature(histGray);
            }
        }
        return features;
    }

    /**
     * Computes the number of bins for a certain number of LBP-neighbors.
     * <p>
     * @param neighbors Number of specified LBP-neighbors.
     * <p>
     * @return Number of bins in a LBP histogram wth the corresponding number of
     *         neighbors.
     */
    protected int getNumberOfBins(int neighbors)
    {
        return 1 << neighbors;
    }

    /**
     * Computes the LBP patterns and builds the histogram for a given LBP-scale
     * and LBP-radius.
     * <p>
     * @param data      Raw image data after pre-processing and filtering.
     * @param neighbors Number of LBP-neighbors to use.
     * @param radius    LBP-radius to use.
     * @param margin    The margin to use at the image borders.
     * @param scale     The LBP-scale to use.
     * <p>
     * @return Computed LBP-Histogram.
     */
    protected LBPHistogram computeHistogram(double[][] data, int neighbors, double radius, int margin, int scale)
    {
        double[] hist = new double[getNumberOfBins(neighbors)];

        int shiftMax = neighbors - 1;
        for(int x = margin; x < data.length - margin; x++) {
            for(int y = margin; y < data[0].length - margin; y++) {

                // THE MATLAB CODE WAS BUGGED, this is correct!
                double center = (double) data[x][y];
                int pattern = 0;

                for(int p = 0; p < neighbors; p++) {
                    double neighbor = getNeighbor(data, x, y, p, scale);

                    if(neighbor >= center) {
                        // this is in reverse order from the matlab thingy to speed up things
                        pattern |= (1 << (shiftMax - p));
                    }
                }
                /* reorder pattern to comply with the other implementations,
                 * freaking hokus pokus!! pick top 3 bit bits from msb
                 */
                int tmp_h = pattern & 0xE0; // 224
                // pick bottom 5 bits from lsb
                int tmp_l = pattern & 0x1F; // 31;
                int pattern_fixed = (tmp_h >> 5) | (tmp_l << 3);

                hist[pattern_fixed] = hist[pattern_fixed] + 1.0d;
            }
        }
        LBPHistogram lbpHistogram = new LBPHistogram();
        lbpHistogram.setData(hist);
        return lbpHistogram;
    }

    /**
     * Compute LBPHistogram by calling computeHistogram based on color channel
     * and scale, performs filtering and pre-processing prior to the computation
     * of the histogram.
     * <p>
     * @param dataChannel
     * @param scale        LBP scale.
     * <p>
     * @return <p>
     * @throws JCeliacGenericException
     */
    protected LBPHistogram computeDistribution(double [][] dataChannel, int scale)
            throws JCeliacGenericException
    {
        LBPHistogram lbpHistogram;
        double radius = LBPMultiscale.getRadiusForScale(scale);
        int neighbors = LBPMultiscale.getNeighborsForScale(scale);
        int margin = (int) Math.ceil(radius + 1);

        DataFilter2D filter = LBPMultiscale.getFilterForScale(scale);
        double [][] data = filter.filterData(dataChannel);
        lbpHistogram = this.computeHistogram(data, neighbors, radius, margin, scale);
        return lbpHistogram;                   
    }

    /**
     * Return a neighbor by interpolation.
     * <p>
     * @param data  Raw image data.
     * @param x     Coordinate x.
     * @param y     Coordinate y.
     * @param p     Neighbor number.
     * @param scale LBP-scale.
     * <p>
     * @return Interpolated neighbor value.
     */
    protected double getNeighbor(double[][] data, int x, int y, int p, int scale)
    {
        double nx, ny;

        // Use the LUTs, scale 1 is index 0
        nx = (double) x +  this.cosLUT[scale - 1][p];
        ny = (double) y -  this.sinLUT[scale - 1][p];
        return getInterpolatedPixelValueFast(data, nx, ny);

    }

    /**
     * This is a lot faster than the matlab compliant version; It was tested
     * with all images we have, no differences to the compliant Java
     * implementation. This performs bi-linear interpolation.
     * <p>
     * <p>
     * @param data Raw image data to interpolate on.
     * @param x    Coordinate x.
     * @param y    Coordiante y.
     * <p>
     * @return Interpolated value.
     */
    protected double getInterpolatedPixelValueFast(final double[][] data, double x, double y)
    {

        int fx = (int) x;
        int fy = (int) y;
        int cx = (int) (x+1);
        int cy = (int) (y+1);;

        double ty = y - fy;
        double tx = x - fx;

        double invtx = 1 - tx;
        double invty = 1 - ty;

        double w1 = invtx * invty;
        double w2 = tx * invty;
        double w3 = invtx * ty;
        double w4 = tx * ty;

        // use this only in extreme debugging cases, this method is called every time
        // Experiment.log(Logger.LOG_HHDEBUG, "getInterpoaltedPixelValue: %f * %f", w1, data[fx][fy]);
        // Experiment.log(Logger.LOG_HHDEBUG, "getInterpoaltedPixelValue: %f * %f", w2, data[cx][fy]);
        // Experiment.log(Logger.LOG_HHDEBUG, "getInterpoaltedPixelValue: %f * %f", w3, data[fx][cy]);
        //  Experiment.log(Logger.LOG_HHDEBUG, "getInterpoaltedPixelValue: %f * %f", w4, data[cx][cy]);
        double ret = w1 * data[fx][fy] + w2 * data[cx][fy] + w3 * data[fx][cy] + w4 * data[cx][cy];
        return ret;
    }

    /**
     * This performs nearest-neighbor interpolation.
     * <p>
     * @param data Raw image data to interpolate on.
     * @param x    Coordinate x.
     * @param y    Coordiante y.
     * <p>
     * @return Interpolated value.
     */
    protected double getNNInterpolatedPixelValue(final double[][] data, double x, double y)
    {
        int rx = (int) Math.round(x);
        int ry = (int) Math.round(y);

        double ret = data[rx][ry];
        return ret;
    }

    /**
     * Creates the feature extraction thread which then calls the
     * FeatureExtractionMethod's extractDiscriminativeFeatures() and extractTrainingDiscriminativeFeatures()
     * methods.
     * <p>
     * @param exp                     The experiment object to call
                                extractDiscriminativeFeatures() and
                                extractTrainingDiscriminativeFeatures() from (this).
     * @param content                 Frame data to extract features from.
     * @param classNumber             Class number of frame, used for
                                extractTrainingDiscriminativeFeatures() for some
                                methods.
     * @param isOneClassTrainingClass Indicates to the ONE_CLASS SVM that the
     *                                supplied frame data is in the training
     *                                class.
     * @param isTraining              Indicates wether the supplied frame data
     *                                is part of the training set.
     * <p>
     * @return
     */
    @Override
    public AbstractFeatureExtractionThread createFeatureExtractionThread(IThreadedExperiment exp,
                                                                 Frame content,
                                                                 int classNumber,
                                                                 boolean isOneClassTrainingClass,
                                                                 boolean isTraining)
    {
        FeatureExtractionThreadLBP lbpThread = new FeatureExtractionThreadLBP(exp, this, content,
                                                                              classNumber, isOneClassTrainingClass,
                                                                              isTraining);
        return lbpThread;
    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {
    }

    
    
    @Override
    public LBPHistogram performScaleAdaptiveFeatureExtraction(LBPScaleFilteredDataSupplier filteredDataSupplier,
                                                              LBPScaleBias scaleBias,
                                                              ScaleEstimation localScaleEstimation,
                                                              LBPNeighborLookup neighborLookup)
            throws JCeliacGenericException
    {

        return this.performGlobalScaleAdaptiveFeatureExtraction(filteredDataSupplier, scaleBias,
                                                                localScaleEstimation, neighborLookup);
    }

    public LBPHistogram performGlobalScaleAdaptiveFeatureExtraction(LBPScaleFilteredDataSupplier filteredDataSupplier,
                                                                    LBPScaleBias scaleBias,
                                                                    ScaleEstimation localScaleEstimation,
                                                                    LBPNeighborLookup neighborLookup)
            throws JCeliacGenericException
    {
        LBPHistogram histogram = new LBPHistogram();
        double[] histLBP = new double[getNumberOfBins(8)];
        int shiftMax = 8 - 1;
        double[][] neighborCoords = new double[8][2];

        int scaleLevel = localScaleEstimation.getEstimatedScaleLevel();
        if(false == localScaleEstimation.isValid()) {
            // fall back to LBP
            scaleLevel = scaleBias.getScaleBiasBaseScale();
        }
        double lbpRadius = scaleBias.getLBPRadiusForScaleLevel(scaleLevel);
        // do not compute for radii < 1.5
        if(lbpRadius < 1) {
            histogram.setData(histLBP);
            histogram.setComputedPatternRatio(0);
            return histogram;
        }
        // this will deliver the correctly filtered data for the lbp radius
        double[][] data = filteredDataSupplier.retrieveLBPScaleFilteredDataForLBPRadius(lbpRadius);
        double stdev = filteredDataSupplier.getStdevForFilteredDataForLBPRadius(lbpRadius);
        double T = Math.sqrt(stdev);
 
        neighborLookup.lookup(lbpRadius, neighborCoords);
        int margin =  (int)Math.ceil(2*lbpRadius);
        for(int x = margin; x < data.length - margin; x++) {
            for(int y = margin; y < data[0].length - margin; y++) {

                double [] neighborValues = new double[8];
                try {
                    this.extractScaleAdaptiveLBP(data, x, y, neighborCoords, neighborValues);
                    double center = data[x][y];
                   
                    int lbpPattern = 0;
                    for(int p = 0; p < 8; p++) {
                        if(neighborValues[p] >= (center+T)) {
                            // this is in reverse order from the matlab thingy to speed up things
                            lbpPattern |= (1 << (shiftMax - p));
                        }
                    }
                    // weight ehe patterns such that each interestpoint adds the same amount to the histogram 
                    // no matter of the the size of the sigma of the interestpoint
                    histLBP[lbpPattern] = histLBP[lbpPattern] + 1.0d;
                } catch(ArrayIndexOutOfBoundsException e) {
                }
            }
        }
        histLBP[0] = 0;
        histogram.setData(histLBP);
        return histogram;
    }

    @Override
    public ArrayList<LBPHistogram>
            performScaleAndOrientationAdaptiveFeatureExtraction(LBPScaleFilteredDataSupplier dataSupplier,
                    LBPScaleBias scaleBias,
                    ScaleEstimation localScaleEstimation,
                    OrientationEstimation localOrientationEstimation,
                    LBPNeighborLookup lookup,
                    double orientationDelta,
                    double orientationSteps,
                    EllipticPointLookup ellipticLookup)
            throws JCeliacGenericException 
    {
        ArrayList<LBPHistogram> featuresByOrientation = new ArrayList<>();
        LBPHistogram histogram;
        int shiftMax = 8 - 1;
        double[][] neighborCoords = new double[8][2];
        double[] histLBP;

        int scaleLevel = localScaleEstimation.getEstimatedScaleLevel();
        if (false == localScaleEstimation.isValid()) {
            // fall back to LBP
            scaleLevel = scaleBias.getScaleBiasBaseScale();
        }
        double lbpRadius = scaleBias.getLBPRadiusForScaleLevel(scaleLevel);

        // do not compute for radii < 1
        if (lbpRadius < 1) {
            for (double delta = -orientationDelta;
                    delta <= orientationDelta;
                    delta += orientationSteps) {
                featuresByOrientation.add(new LBPHistogram(8));
                return featuresByOrientation;
            }
        }

        // this will deliver the correctly filtered data for the lbp radius
        double[][] data = dataSupplier.retrieveLBPScaleFilteredDataForLBPRadius(lbpRadius);
        double stdev = dataSupplier.getStdevForFilteredDataForLBPRadius(lbpRadius);
        double T = Math.sqrt(stdev);

        histLBP = new double[1 << 8];
        for (double delta = -orientationDelta; delta <= orientationDelta; delta += orientationSteps) {
            
            double confidence;
            
            double orientation = Math.PI * 4 - localOrientationEstimation.getDominantOrientation() + delta;
            if(Double.isNaN(localOrientationEstimation.getDominantOrientation())) {
                lookup.lookup(lbpRadius, neighborCoords);
                confidence = 1;
            } else {
                lookup.lookup(lbpRadius, orientation, neighborCoords);
                confidence = 1.0d; //localOrientationEstimation.getConfidenceForOrientation(delta);
            }

            double[] neighborValues = new double[8];
            int margin = (int) Math.ceil(2 * lbpRadius);

            for (int x = margin; x < data.length - margin; x++) {
                for (int y = margin; y < data[0].length - margin; y++) {

                    this.extractScaleAdaptiveLBP(data, x, y, neighborCoords, neighborValues);
                    double center = data[x][y];

                    int lbpPattern = 0;
                    for (int p = 0; p < 8; p++) {
                        if (neighborValues[p] >= (center + T)) {
                            // this is in reverse order from the matlab thingy to speed up things
                            lbpPattern |= (1 << (shiftMax - p));
                        }
                    }
                    // weight ehe patterns such that each interestpoint adds the same amount to the histogram 
                    // no matter of the the size of the sigma of the interestpoint
                    histLBP[lbpPattern] = histLBP[lbpPattern] + confidence;
                    int alternatePattern = (lbpPattern & 0xF0) >> 4 | (lbpPattern & 0xF) << 4;
                    if (alternatePattern != lbpPattern) {
                        histLBP[alternatePattern] += confidence;
                    }
                }

            }
            
        }
        histLBP[0] = 0;
            histogram = new LBPHistogram();
            histogram.setData(histLBP);
            featuresByOrientation.add(histogram);
        return featuresByOrientation;
    }
     
    /**
     * Performs the actual computation of scale-adaptive patterns.
     * <p>
     * @param filteredData Scale-adaptively filtered data.
     * @param x            Coordinate x.
     * @param y            Coordinate y.
     * @param stdev        Standard deviation of filtered data.
     * @param points       Neighbor positions.
     * <p>
     * @return LBP pattern at x,y.
     * <p>
     */
    protected void extractScaleAdaptiveLBP(double[][] filteredData, int x, int y,
                                                double [][] points, double [] neighborValues)
    {
        for(int p = 0; p < 8; p++) {
            neighborValues[p] = this.getInterpolatedPixelValueFast(filteredData, x+points[p][0], y-points[p][1]);
        }
    }

     protected double [] extractScaleAdaptiveLBP(double[][] filteredData, int x, int y,
                                                EllipticPoint [] points)
    {

        int shiftMax = 8 - 1;
        int pattern = 0;

        double center = filteredData[x][y];
        double [] neighbors = new double[8];
        
        for(int p = 0; p < 8; p++) {
            double neighbor = 0;

            if(points[p].x < filteredData.length - 1 && points[p].y < filteredData[0].length - 1
               && points[p].x >= 0 && points[p].y >= 0) {
                neighbor = this.getInterpolatedPixelValueFast(filteredData, points[p].x, points[p].y);
                neighbors[p] = neighbor;
            } else {
                throw new ArrayIndexOutOfBoundsException();
            }
        }
        return neighbors;
    }
    
    /**
     * Initialize the correct histogram type for an external calling scale
     * adaptive method based on this method.
     * <p>
     * @return Initialized histogram of LBPHistogram type.
     */
    @Override
    public LBPHistogram initializeHistogram()
    {
        LBPHistogram hist = new LBPHistogram();
        hist.setData(new double[256]);
        return hist;
    }

    /**
     * Finalize the histogram for an external calling scale adaptive method
     * based on this method.
     * <p>
     * @param histogram Bin 0 is set to 0.
     */
    @Override
    public void finalizeHistogram(LBPHistogram histogram)
    {
        double[] histData = histogram.getRawData();
        histData[0] = 0;
    }

    /**
     * Computes affine adaptive patterns, called by external methods such as
     * SOALBP.
     * <p>
     * @param hist         Externally initialized LBP-Histogram, will be updated
     *                     by this method.
     * @param center       Object describing the center pixel.
     * @param neighbors    Array of Objects describing the neighbor pixels.
     * @param stdev        Standard deviation of corresponding scale-filtered
     *                     image data.
     * @param contribution Contribution of pattern based no scale-estimation
     *                     reliability.
     */
    @Override
    public void computeAffineAdaptivePattern(LBPHistogram hist, AffineFilteredNeighbor center,
                                             AffineFilteredNeighbor[] neighbors,
                                             double stdev, double contribution)
    {
        double[] histData = hist.getRawData();
        int shiftMax = 7;
        int pattern = 0;

        double centerValue = center.value;
        for(int p = 0; p < neighbors.length; p++) {
            double neighborValue = neighbors[p].value;

            if(neighborValue >= centerValue) {
                // this is in reverse order from the matlab thingy to speed up things
                pattern |= (1 << (shiftMax - p));
            } else {
                // zero
            }
        }
        int alternatePattern = (pattern & 0xF0) >> 4 | (pattern & 0xF) << 4;
        histData[pattern] += contribution;
        if(alternatePattern != pattern) {
            histData[alternatePattern] += contribution;
        }
    }

    @Override
    public void setScaleAdaptiveParameters(FeatureExtractionParametersScaleAdaptiveLBP params)
    {
       this.myParametersSA = params;
    }
    
    

}
