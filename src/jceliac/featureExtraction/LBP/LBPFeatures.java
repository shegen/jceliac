/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.LBP;

import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.JCeliacGenericException;
import jceliac.tools.data.DataSource;
import jceliac.featureOptimization.*;
import jceliac.featureExtraction.*;
import jceliac.tools.filter.*;
import java.util.*;
import jceliac.classification.kernelSVM.SVMFeatureVector;
import jceliac.tools.arrays.ArrayTools;


/**
 * LBP specific features.
 * <p>
 * @author shegen
 */
public class LBPFeatures
        extends DiscriminativeFeatures
{
    protected FeatureExtractionParametersLBP myParameters;
    protected ArrayList<LBPHistogram> histograms;
    protected LBPFeatureOptimizationMode optimizationMode = LBPFeatureOptimizationMode.HISTOGRAMS;

    /**
     * Defines the feature optimization mode, currently not used.
     */
    public static enum LBPFeatureOptimizationMode
    {

        /**
         * Treat entire histograms as feature vector entries.
         */
        HISTOGRAMS,
        /**
         * Treat single histogram bins as feature vector entries.
         */
        BINS
    }

    public LBPFeatures(FeatureExtractionParameters parameters)
    {
        super(parameters);
        this.myParameters = (FeatureExtractionParametersLBP) parameters;
        this.histograms = new ArrayList<>();
    }

    /**
     * Used by Externalizable.
     * <p>
     */
    public LBPFeatures()
    {
        super();
    }
    
    @Override
    public DiscriminativeFeatures cloneFeatureSubset(FeatureVectorSubsetEncoding subset)
            throws JCeliacGenericException
    {
        LBPFeatures featureSubset = new LBPFeatures(super.featureExtractionParameters);
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
        
        for(int i = 0; i < subset.bitCount(); i++) {
            if(subset.isSet(i)) {
                featureSubset.featureVectors.add(this.featureVectors.get(i));
            }
        }
        return featureSubset;
    }

    @Override
    public DiscriminativeFeatures cloneEmptyFeature()
    {
        LBPFeatures featureSubset = new LBPFeatures(super.featureExtractionParameters);
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
        
        return featureSubset;
    }
    
    @Override
    public double doDistanceTo(DiscriminativeFeatures them)
            throws JCeliacGenericException
    {
        double [] myHist = this.getAllFeatureVectorData();
        double[] fHist = them.getAllFeatureVectorData();
        
        double distance = Distances.histogramIntersectAsDistance(myHist, fHist);
        return distance;
    }

    @Override
    public synchronized double[] getAllFeatureVectorData() 
            throws JCeliacGenericException
    {
        double [] rawHistData = super.getAllFeatureVectorData();
        // normalize to 1
        double sum = ArrayTools.sum(this.allRawFeatureData);
        if(sum > 0) {
            for(int i = 0; i < this.allRawFeatureData.length; i++) {
                rawHistData[i] = rawHistData[i] / sum;
            }
        }
        return rawHistData;
    }
        
    /**
     * Add abstract feature by retrieving the raw data from the specific
     * LBPHistograms.
     * <p>
     * @param o LBPHistogram
     * <p>
     * @throws JCeliacGenericException
     */
    @Override
    public void addAbstractFeature(AbstractFeatureVector o)
            throws JCeliacGenericException
    {
            if(o instanceof LBPHistogram)
            {
                LBPHistogram histogram = (LBPHistogram) o;
                if(this.myParameters.isUseUniformPatterns() && 
                   this.myParameters.isUseRotationalInvariance()) 
                {
                    FixedFeatureVector vec = new FixedFeatureVector(histogram.getRotatedUniformData());
                    this.featureVectors.add(vec);
                } else if(this.myParameters.isUseRotationalInvariance()) {
                    FixedFeatureVector vec = new FixedFeatureVector(histogram.getRotatedData());
                    this.featureVectors.add(vec);
                } else if(this.myParameters.isUseUniformPatterns()) {
                    FixedFeatureVector vec = new FixedFeatureVector(histogram.getUniformData());
                    this.featureVectors.add(vec);
                } else if(this.myParameters.isUseSubUniformPatterns()) {
                    FixedFeatureVector vec = new FixedFeatureVector(histogram.getSubUniformPatterns());
                    this.featureVectors.add(vec);
                } else {
                    FixedFeatureVector vec = new FixedFeatureVector(histogram.getPreparedData());
                    this.featureVectors.add(vec);
                }
                this.histograms.add(histogram);
                histogram.disposeData(); // data not needed anymore
            }else {
                this.allRawFeatureData = o.getFeatureData();
            }
        
    }

    /**
     * Returns a string representing the used scales of a certain feature
     * subset.
     * <p>
     * @param subset Feature subset.
     * <p>
     * @return String representation of scales in a feature subset.
     */
    public String getScaleString(FeatureVectorSubsetEncoding subset)
    {
        String scaleString = "";

        for(int i = 0; i < histograms.size(); i++) {
            if(subset.isSet(i)) {
                scaleString += String.valueOf(histograms.get(i).getScale()) + ";";
            }
        }
        return scaleString;
    }

    /**
     * Returns a string representing the used color channels of a certain
     * feature subset.
     * <p>
     * @param subset Feature subset.
     * <p>
     * @return String representation of color channels in a feature subset.
     */
    public String getColorString(FeatureVectorSubsetEncoding subset)
    {
        String colorString = "";

        for(int i = 0; i < histograms.size(); i++) {
            if(subset.isSet(i)) {
                switch(histograms.get(i).getScale()) {
                    case DataSource.CHANNEL_RED:
                        colorString += "R;";
                        break;
                    case DataSource.CHANNEL_GREEN:
                        colorString += "G;";
                        break;
                    case DataSource.CHANNEL_BLUE:
                        colorString += "B;";
                        break;
                    default:
                        colorString += "-;";
                        break;
                }
            }
        }
        return colorString;
    }

    /**
     * Returns a string representing the used sobel orientations of a certain
     * feature subset.
     * <p>
     * @param subset Feature subset.
     * <p>
     * @return String representation of sobel orientations in a feature subset.
     */
    public String getSubbandString(FeatureVectorSubsetEncoding subset)
    {
        String sbString = "";

        for(int i = 0; i < histograms.size(); i++) {
            if(subset.isSet(i)) {
                switch(histograms.get(i).getSubband()) {
                    case SobelFilter.ORIENTATION_DIAGONAL:
                        sbString += "D;";
                        break;
                    case SobelFilter.ORIENTATION_HORIZONTAL:
                        sbString += "H;";
                        break;
                    case SobelFilter.ORIENTATION_VERTICAL:
                        sbString += "V;";
                        break;
                    case SobelFilter.ORIENTATION_MAGNITUDE:
                        sbString += "M;";
                        break;
                    default:
                        sbString += "-;";
                        break;
                }
            }
        }
        return sbString;
    }

    /**
     * Returns a string representing the used wavelet subbands of a certain
     * feature subset.
     * <p>
     * @param subset Feature subset.
     * <p>
     * @return String representation of wavelet subbands in a feature subset.
     */
    public String getWaveletSubbandString(FeatureVectorSubsetEncoding subset)
    {
        String sbString = "";

        for(int i = 0; i < histograms.size(); i++) {
            if(subset.isSet(i)) {
                switch(histograms.get(i).getSubband()) {
                    default:
                        sbString += "-;";
                        break;
                }
            }
        }
        return sbString;
    }

    /**
     * Returns a string representing the used wavelet scales of a certain
     * feature subset.
     * <p>
     * @param subset Feature subset.
     * <p>
     * @return String representation of wavelet scales in a feature subset.
     */
    public String getWaveletScaleString(FeatureVectorSubsetEncoding subset)
    {
        String sbString = "";

        for(int i = 0; i < histograms.size(); i++) {
            if(subset.isSet(i)) {
                switch(histograms.get(i).getSubband()) {
                    default:
                        sbString += "-;";
                        break;
                }
            }
        }
        return sbString;
    }
    
    public LBPFeatureOptimizationMode getOptimizationMode()
    {
        return optimizationMode;
    }

    public void setOptimizationMode(LBPFeatureOptimizationMode optimizationMode)
    {
        this.optimizationMode = optimizationMode;
    }

    public void setFeatureExtractionParameters(FeatureExtractionParametersLBP myParameters)
    {
        this.myParameters = myParameters;
    }

    @Override
    public ArrayList getLocalFeatureVectors()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<double[]> getLocalFeatureVectorData()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * LBP Features require a different type of normalization, do not scale between 0 and
     * 1 because it's histograms using intersection kernels. 
     * 
     * @return
     * @throws JCeliacGenericException 
     */
    @Override
    public synchronized SVMFeatureVector[] getNormalizedFeaturesForSVM()
            throws JCeliacGenericException
    {
        double[] data = this.getAllFeatureVectorData();
        this.normalizeLinear(data);
        SVMFeatureVector[] vector = new SVMFeatureVector[data.length];

        for(int index = 0; index < data.length; index++) {
            SVMFeatureVector cur = new SVMFeatureVector();
            cur.index = index + 1;
            cur.value = data[index];
            vector[index] = cur;
        }
        return vector;
    }
    
    

}
