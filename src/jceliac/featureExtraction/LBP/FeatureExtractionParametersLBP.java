/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.LBP;

import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.experiment.*;
import jceliac.logging.*;

/**
 * Parameter options for Local Binary Patterns (LBP).
 * <p>
 * @author shegen
 */
public class FeatureExtractionParametersLBP
        extends FeatureExtractionParameters
{

    /**
     * Defines whether to use Uniform Patterns
     */
    protected boolean useUniformPatterns = false;

    /*
     * Rotational invariance LBP style.
     */
    protected boolean useRotationalInvariance = false;

    /**
     * Minimum LBP Scale to use. Use scales >= 1. Default is 1.
     */
    protected int minScale = 1;          // scale has the meaning of Ojala et al. 2002

    /**
     * Maximum LBP Scale to use. Use scales >= minScale && <= 5. Default is 3.
     */
    protected int maxScale = 3;

    /**
     * Number of neighbors to use. Not supported yet. Default is 8.
     */
    protected int numberOfNeighbors = 8;      // number of neighbors to be used, defaults to 8

    
    protected boolean useSubUniformPatterns = false;
    
    public boolean getUseUniformPatterns()
    {
        return useUniformPatterns;
    }
    
    public int getMaxScale()
    {
        return maxScale;
    }

    public int getMinScale()
    {
        return minScale;
    }

    public int getNumberOfNeighbors()
    {
        return numberOfNeighbors;
    }

    public void setNumberOfNeighbors(int numberOfNeighbors)
    {
        this.numberOfNeighbors = numberOfNeighbors;
        throw new UnsupportedOperationException("Not supported yet.");

    }

    public FeatureExtractionParametersLBP()
    {
    }

    public boolean isUseUniformPatterns()
    {
        return useUniformPatterns;
    }

    public void setUseUniformPatterns(boolean useUniformPatterns)
    {
        this.useUniformPatterns = useUniformPatterns;
    }

    public boolean isUseSubUniformPatterns()
    {
        return useSubUniformPatterns;
    }

    public void setUseSubUniformPatterns(boolean useSubUniformPatterns)
    {
        this.useSubUniformPatterns = useSubUniformPatterns;
    }
    
    @Override
    public void report()
    {
        super.report();
        Experiment.log(JCeliacLogger.LOG_INFO, "Uniform-Patterns: %s", this.useUniformPatterns ? "true" : "false");
        Experiment.log(JCeliacLogger.LOG_INFO, "Neighbors: %d", this.numberOfNeighbors);
        Experiment.log(JCeliacLogger.LOG_INFO, "Minimum Scale: %d", this.minScale);
        Experiment.log(JCeliacLogger.LOG_INFO, "Maximum Scale: %d", this.maxScale);
    }

    public void setMaxScale(int maxScale)
    {
        this.maxScale = maxScale;
    }

    public void setMinScale(int minScale)
    {
        this.minScale = minScale;
    }

    public void setUseColor(boolean useColor)
    {
        this.useColor = useColor;
    }

    public boolean isUseRotationalInvariance()
    {
        return useRotationalInvariance;
    }

    public void setUseRotationalInvariance(boolean useRotationalInvariance)
    {
        this.useRotationalInvariance = useRotationalInvariance;
    }

    @Override
    public String getMethodName()
    {
        return "LBP";
    }
}
