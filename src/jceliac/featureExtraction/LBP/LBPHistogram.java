/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.LBP;

import jceliac.experiment.*;
import jceliac.logging.*;
import jceliac.*;
import jceliac.featureExtraction.AbstractFeatureVector;
import jceliac.tools.arrays.ArrayTools;

/**
 * LBPHistogram holding the raw histogram data.
 * <p>
 * @author shegen
 */
public class LBPHistogram extends AbstractFeatureVector
        implements IComputeOnRetrieval
{

    protected double[] data;
    protected double[] uniformData = null;
    protected double[] rotatedData = null;
    protected double[] rotatedUniformData = null;
    protected double[] subUniformData = null;
    
    protected int histogramDimension = -1;
    protected int scale;
    protected int colorChannel;
    protected int subband;
    
    protected int[] rotinvLUT;
    protected static int[] rotationLUT8;
    private static boolean[] uniformLUT;
    private static int [] transitionLUT;

    protected double computedPatternRatio = 0;

    static {
        // precompute lookup table for rotational invariant histograms
        LBPHistogram.rotationLUT8 = LBPHistogram.computeRotationLUT(8);
        LBPHistogram.prepareUniformLUT(256);
    }

    public LBPHistogram()
    {
    }

    /** 
     * Initialize an empty usable LBP histogram. 
     * 
     * @param neighbors 
     */
    public LBPHistogram(int neighbors)
    {
        this.data = new double[1 << neighbors];
    }
    
    
    
    
    /**
     * Computes the rotational invariance lookup table.
     * <p>
     * @param n Number of neighbors.
     * <p>
     * @return Lookup table.
     */
    protected static int[] computeRotationLUT(int n)
    {
        int[] map = new int[1 << n];
        for(int lbpCode = 0; lbpCode < (1 << n); lbpCode++) {
            int rotatedCode = rotateLBP(lbpCode, n);
            map[lbpCode] = rotatedCode;
        }
        return map;
    }

    /**
     * Performs the rotation invariant encoding be finding the smallest LBP
     * value interpreted as a number by circular shifting the pattern to the
     * rigth.
     * <p>
     * @param lbp LBP pattern.
     * @param n   Number of neighbors.
     * <p>
     * @return Rotation invariant encoding for LBP pattern.
     * <p>
     */
    private static int rotateLBP(int lbp, int n)
    {
        int minLBP = Integer.MAX_VALUE;
        for(int i = 0; i < n; i++) {
            int tmp = Integer.rotateRight(lbp, i);

            int upperbyte = (tmp & 0xff000000) >>> 24;
            int lowerbyte = tmp & 0xff;
            int rotLBP = upperbyte | lowerbyte;

            if(rotLBP < minLBP) {
                minLBP = rotLBP;
            }
        }
        return minLBP;
    }

    public synchronized static void prepareUniformLUT(int bins)
    {
        if(LBPHistogram.uniformLUT == null) {
            LBPHistogram.computeUniformLUT(bins);
        }
    }

    public static boolean[] getUniformLUT(int neighbors)
    {
        if(LBPHistogram.uniformLUT != null) {
            return LBPHistogram.uniformLUT;
        } else {
            prepareUniformLUT(neighbors);
            return LBPHistogram.uniformLUT;
        }

    }

    /**
     * Data that are not needed after they were retrieved are disposed to mark
     * them for garbage collection.
     */
    public void disposeData()
    {
        this.data = null;
        this.uniformData = null;
    }

    /**
     * Returns number of uniform patterns.
     * <p>
     * @param bins Number of histogram bins.
     * <p>
     * @return Number of uniform patterns.
     * <p>
     * @throws JCeliacLogableException If unsupported number of bins is
     *                                 supplied.
     */
    public static int getUniformCount(int bins)
            throws JCeliacLogableException
    {

        switch(bins) {
            case 256:
                return 58;
            case 1024:
                return 92;
            case 4096:
                return 134;
        }
        throw new JCeliacLogableException("Uniform Patterns not Supported with this type of Histogram!");
    }

    /**
     * Compute the uniform lookup table.
     * <p>
     * @param binCount Number of bins per histogram.
     */
    protected static void computeUniformLUT(int binCount)
    {
        int maxValue = binCount;
        int neighbors = (int) (Math.log(binCount) / Math.log(2));
        boolean[] LUT = new boolean[maxValue];
        LBPHistogram.transitionLUT = new int[maxValue];
        
        for(int i = 0; i < maxValue; i++) {
            int t = LBPHistogram.numTransitions(i, neighbors);
            LBPHistogram.transitionLUT[i] = t;
            LUT[i] = t <= 2;
        }
        LBPHistogram.uniformLUT = LUT;
    }

    /**
     * Compute the uniform pattern count for a certain number of neighbors.
     * <p>
     * @param neighbors Number of neighbors.
     * <p>
     * @return Returns number of uniform patterns.
     * <p>
     */
    protected static int getUniformPatternCount(int neighbors)
    {
        switch(neighbors) {
            case 8:
                return 58;
            case 10:
                return 92;
            case 12:
                return 134;
            default:
                Experiment.log(JCeliacLogger.LOG_ERROR, "getUniformPatternCount: Not implemented for that neighbor count yet! ");
                return 0;
        }
    }

    /**
     * Checks the number of bit transitions. Verified with 8-bit patterns (58
     * uniform patterns).
     * <p>
     * @param lbp       LBP pattern.
     * @param neighbors Number of neighbors.
     * <p>
     * @return Number of transitions between 0 and 1.
     */
    protected static int numTransitions(int lbp, int neighbors)
    {

        double exp;
        if(neighbors == 256) {
            exp = 8;
        } else {
            exp = Math.log(neighbors) / Math.log(2);
        }
        if(exp < (int) exp) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Number of neighbors is not a power of two! Cannot extract uniform patterns!");
            return -1;
        }

        int last = -1;
        int transitions = 0;

        // the first and last bit are interpreted circularly in the case of transitions
        int lastBit = (lbp & (1 << (neighbors - 1))) >> (neighbors - 1);
        int firstBit = lbp & 1;
        if(lastBit != firstBit) {
            transitions++;
        }
        for(int shift = 0; shift < neighbors; shift++) {
            int tmp = (lbp & (1 << shift)) >> shift;
            if(last == -1) {
                last = tmp;
            } else if(tmp != last) {
                transitions++;
                last = tmp;
            }
        }
        return transitions;
    }

    public static int getNumberOfNeighbors(int bins)
    {
        return (int) (Math.log((double) bins) / Math.log(2.0d));
    }

    public int getColorChannel()
    {
        return colorChannel;
    }

    public void setColorChannel(int colorChannel)
    {
        this.colorChannel = colorChannel;
    }

    public int getScale()
    {
        return scale;
    }

    public void setScale(int scale)
    {
        this.scale = scale;
    }

    public int getSubband()
    {
        return subband;
    }

    public void setSubband(int subband)
    {
        this.subband = subband;
    }

    public final double[] getRawData()
    {
        return this.data;
    }

    /**
     * Retrieves the prepared (normalized) data.
     * <p>
     * @return The prepared histogram data.
     */
    public final double[] getPreparedData()
    {
        this.prepare();  // prepares the data (assembles histograms or such things)
        return data;
    }

    public int getHistogramDimension()
    {
        if(this.histogramDimension == -1) {
            //Experiment.log(JCeliacLogger.LOG_ERROR, "Histogram dimension queried but not set!"); 
            return 0;
        }
        return this.histogramDimension;
    }

    /**
     * Computes a histogram of uniform patterns and returns it.
     * <p>
     * @return Double histogram of uniform LBP patterns.
     * <p>
     * @throws JCeliacLogableException
     */
    public double[] getUniformData()
            throws JCeliacLogableException
    {
        if(this.uniformData != null) {
            return this.uniformData;
        }

        this.prepare();
        // access to data is save from here
        LBPHistogram.prepareUniformLUT(this.data.length);
        this.uniformData = new double[this.data.length];

        double nonuniformSum = 0;
        for(int index = 0; index < this.getHistogramDimension(); index++) {
            if(LBPHistogram.uniformLUT[index] == true) {
                // access to data is save after prepare()
                this.uniformData[index] = this.data[index];
            } else {
                nonuniformSum += this.data[index];
            }
        }
        this.uniformData[9] = nonuniformSum;
        this.uniformData = normalize(this.uniformData);
        return this.uniformData;
    }

    /**
     * Computes a histogram of rotation invariant encoded patterns and returns
     * it.
     * <p>
     * @return Double histogram of LBP patterns with rotation invariant
     *         encoding.
     * <p>
     * @throws JCeliacLogableException
     */
    public double[] getRotatedData()
            throws JCeliacLogableException
    {
        if(this.rotatedData != null) {
            return this.rotatedData;
        }

        this.prepare();
        // access to data is save from here

        int neighbors = LBPHistogram.getNumberOfNeighbors(this.data.length);
        if(neighbors == 8) {
            this.rotinvLUT = LBPHistogram.rotationLUT8;
        } else {
            this.rotinvLUT = LBPHistogram.computeRotationLUT(neighbors);
        }
        this.rotatedData = new double[this.data.length];

        for(int index = 0; index < this.getHistogramDimension(); index++) {
            int rotatedCode = this.rotinvLUT[index];
            this.rotatedData[rotatedCode] += this.data[index];
        }
        this.rotatedData = normalize(this.rotatedData);
        return this.rotatedData;
    }

    /**
     * Computes a histogram of uniform rotation invariantly encoded patterns and
     * returns it.
     * <p>
     * @return Double histogram of uniform LBP patterns with rotation invariant
     *         encoding.
     * <p>
     * @throws JCeliacLogableException
     */
    public double[] getRotatedUniformData()
            throws JCeliacLogableException
    {
        if(this.rotatedUniformData != null) {
            return this.rotatedUniformData;
        }

        this.prepare();
        // access to data is save from here

        double[] rotatedTmp = new double[this.data.length];

        prepareUniformLUT(rotatedTmp.length);
        for(int index = 0; index < this.getHistogramDimension(); index++) {
            if(LBPHistogram.uniformLUT[index] == true) {
                // access to data is save after prepare()
                rotatedTmp[index] = this.data[index];
            }
        }
        int neighbors = getNumberOfNeighbors(this.data.length);
        if(neighbors == 8) {
            this.rotinvLUT = LBPHistogram.rotationLUT8;
        } else {
            this.rotinvLUT = LBPHistogram.computeRotationLUT(neighbors);
        }
        this.rotatedUniformData = new double[this.data.length];
        for(int index = 0; index < this.getHistogramDimension(); index++) {
            int rotatedCode = this.rotinvLUT[index];
            this.rotatedUniformData[rotatedCode] += rotatedTmp[index];
        }
        return this.rotatedUniformData;
    }
    
    /**
     * Provides subuniform patterns following Li et al. in 
     * Scale- and Rotation-Invariant Local Binary Pattern Using 
     * Scale-Adaptive Texton and Subuniform-Based Circular Shift. 
     * 
     * @return 
     */
    public double [] getSubUniformPatterns()
            throws JCeliacGenericException
    {
        if(this.subUniformData != null) {
            return this.subUniformData;
        }
 
        // first dimension (M) identifies the uniform pattern type (number of 1s)
        // second dimension (W) identifies the uniform pattern orientation
        double [][] subUniformData2D = new double[7][8];
        double nonUniformCount = 0;
        double allZeroCount = 0;
        double allOneCount =0 ;
        
        int [] mCounts = new int[7];
        for(int bin = 0; bin < this.data.length; bin++) 
        {
            // check if pattern is uniform
            if(LBPHistogram.uniformLUT[bin] == false) {
                nonUniformCount += this.data[bin];
                continue;
            }
            int bitCount = Integer.bitCount(bin);
            if(bitCount == 0) {
                allZeroCount += this.data[bin];
            }else if(bitCount == 8) {
                allOneCount += this.data[bin];
            }else {
                int M,W;
                // uniform pattern with U = 2
                M = bitCount;
                W = mCounts[M-1];
                subUniformData2D[M-1][W] = this.data[bin];
                mCounts[M-1]++;
            }
        }
        this.subUniformData = new double[58];
        this.subUniformData[0] = allZeroCount;
        this.subUniformData[57] = allOneCount;
        
        // rotate the subUniformData circularly such that the dominant W is first
        for(int i = 0; i < 7; i++) {
            double [] alignedHist = this.alignByDominantBin(subUniformData2D[i]);
            System.arraycopy(alignedHist, 0, this.subUniformData, 1 + i*8, 8);
        } 
        return this.subUniformData;
    }
    
    private double [] alignByDominantBin(double [] hist) 
    {
        // find largest bin
        int dominantBin = ArrayTools.maxIndex(hist);
        double [] alignedHist = new double[hist.length];
        
        for(int off = 0; off < hist.length; off++) {
            alignedHist[off] = hist[(dominantBin+off) % hist.length];
        }
        return alignedHist;
    }
    
   /**
     * Returns the first position of a transition from 1 to 0 in a clockwise
     * manner. 
     * 
     * @param bin
     * @return 
     */
    public int pos(int bin)
            throws JCeliacGenericException
    {
        for(int p = 1; p < 8; p++) {
            int oneMask = 1 << p;
            int zeroMask = 1 << (p - 1);

            if((bin & oneMask) == oneMask && (bin & zeroMask) == 0) {
                return p;
            }
        }
        throw new JCeliacGenericException("No 1-0 transition found, is U = 2?!");
    }
    

    private String toStringFormatted()
    {
        String out = "";
        for(int i = 0; i < data.length; i++) {
            out += "bin-" + i + ":" + data[i] + " ";
            if(((i + 1) % 10) == 0) {
                out += "\n\n";
            }
        }
        return out;
    }

    private String toStringBasic()
    {
        String out = "";
        for(int i = 0; i < data.length; i++) {
            out += data[i] + "\n";
        }
        return out;
    }

    @Override
    public String toString()
    {
        return toStringBasic();
    }

    /**
     * Prepare the histogram, in this case only normalization is performed.
     * <p>
     */
    @Override
    public synchronized void prepare()
    {
        this.data = normalize(this.data);
    }

    /**
     * Normalization of a histogram.
     * <p>
     * @param hist Histogram.
     * <p>
     * @return Normalized histogram.
     */
    protected synchronized double[] normalize(double[] hist)
    {
        double sum = 0;
        double[] nhist = new double[hist.length];
        for(int index = 0; index < hist.length; index++) {
            sum += hist[index];
        }
        if(sum == 0) // nasty bug, forgot this took me 2 days to find :/
        {
            return hist;
        }
        for(int index = 0; index < hist.length; index++) {
            nhist[index] = (hist[index] / sum);
        }
        return nhist;
    }

    /**
     * Merges two LBP histograms by first normalizing them and then building a
     * new average histogram of both.
     * <p>
     * @param one Histogram one.
     * @param two Histogram two.
     * <p>
     * @return Merged LBP histogram.
     * <p>
     * @throws JCeliacGenericException
     */
    public static LBPHistogram mergeHistograms(LBPHistogram one, LBPHistogram two)
            throws JCeliacGenericException
    {
        double[] oneData = one.getPreparedData();
        double[] twoData = two.getPreparedData();

        if(oneData.length != twoData.length) {
            throw new JCeliacGenericException("Histograms of different sizes can not be merged!");
        }

        // make sure both are normalized, this is invariant to multiple calls
        oneData = one.normalize(oneData);
        twoData = two.normalize(twoData);

        // merge
        for(int i = 0; i < oneData.length; i++) {
            twoData[i] += oneData[i];
            twoData[i] /= 2;
        }
        LBPHistogram hist = new LBPHistogram();
        hist.setData(twoData);
        return hist;
    }

    /**
     * Checks if a histogram is empty (all zeroes).
     * <p>
     * @return True if the histogram is empty, false else.
     */
    public boolean isEmpty()
    {
        double[] d = this.getPreparedData();
        for(int i = 0; i < d.length; i++) {
            if(d[i] > 0) {
                return false;
            }
        }
        return true;
    }

    public double getComputedPatternRatio()
    {
        return computedPatternRatio;
    }

    public void setComputedPatternRatio(double computedPatternRatio)
    {
        this.computedPatternRatio = computedPatternRatio;
    }

    @Override
    public void setData(double[] data)
    {
        this.data = data;
        this.histogramDimension = this.data.length;
    }


    @Override
    public double[] getFeatureData() throws JCeliacGenericException
    {
        return this.data;
    }
}
