/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.LBP;

import jceliac.tools.filter.LowpassFilter.EFilterType;
import jceliac.JCeliacGenericException;
import jceliac.tools.filter.*;
import java.lang.Math.*;

/**
 * This implements the lbp multiscale approach.
 * <p>
 * @author shegen
 */
public class LBPMultiscale
{

    private final static int[] NEIGHBORS = {8, 8, 8, 8, 8};

    /**
     * Computes the correct filter for LBP-scale.
     * <p>
     * @param scale LBP-scale.
     * <p>
     * @return Corresponding data filter for scale.
     * <p>
     * @throws JCeliacGenericException
     */
    public static DataFilter2D getFilterForScale(int scale)
            throws JCeliacGenericException
    {
        if(scale == 1) {
            // scale 1 uses no filtering
            return new NullFilter();
        }
        int filterDimension = calculateFilterDimension(scale);
        double sigma = computeSigmaForScale(scale);
        DataFilter2D filter = new LowpassFilter(EFilterType.FILTER_GAUSSIAN, filterDimension, sigma);
        return filter;
    }

    /**
     * Computes the filter sigma for an LBP-scale.
     * <p>
     * @param scale LBP scale.
     * <p>
     * @return Corresponding sigma for scale with 95% of the mass of the inner
     *         radius.
     */
    private static double computeSigmaForScale(int scale)
    {
        double rn = LBPMultiscale.getRadiusForScale(scale);
        double rnminus1 = LBPMultiscale.getRadiusForScale(scale - 1);

        return (rn - rnminus1) / (2.0d * Math.sqrt(-2.0d * Math.log(1d - 0.95d)));

    }

    /**
     * Computes the inner radius for an LBP-scale.
     * <p>
     * @param scale LBP-scale.
     * <p>
     * @return Inner radius for LBP-scale.
     */
    public static double getRadiusForScale(int scale)
    {
        if(scale <= 1) {
            return 1.5;
        }
        return 0.5 * (calculateOuterRadiusForScale(scale)
                      + calculateOuterRadiusForScale(scale - 1));

    }

    /**
     * Computes the outer radius for an LBP-scale.
     * <p>
     * @param scale LBP-scale.
     * <p>
     * @return Outer radius for LBP-scale.
     */
    private static double calculateOuterRadiusForScale(int scale)
    {
        double tmp;

        if(scale <= 1) {
            return 1.5;
        }
        double neighbors;
        if(scale-1 >= NEIGHBORS.length) {
            neighbors = 8;
        }else {
            neighbors = NEIGHBORS[scale - 1];
        }
        tmp = Math.sin(Math.PI / neighbors);
        return ((2 / (1 - tmp)) - 1) * calculateOuterRadiusForScale(scale - 1);

    }

    /**
     * Calculates the dimension of the filter.
     * <p>
     * @param scale LBP-scale.
     * <p>
     * @return Filter dimension.
     */
    private static int calculateFilterDimension(int scale)
    {
        double w = 0.5 * (calculateOuterRadiusForScale(scale)
                          - calculateOuterRadiusForScale(scale - 1));

        double ret = (2 * Math.ceil(w)) + 1;
        return (int) ret;
    }

    public static int getNeighborsForScale(int scale)
    {
        if(scale-1 >= NEIGHBORS.length) {
            return 0;
        }
        return NEIGHBORS[scale - 1];
    }

}
