/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.LBP;

import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.*;
import jceliac.tools.data.*;
import jceliac.experiment.*;
import jceliac.tools.timer.PerformanceTimer;


/**
 * Feature extraction thread basis for all multi-threaded LBP methods. 
 * 
 * @author shegen
 */
public class FeatureExtractionThreadLBP extends AbstractFeatureExtractionThread {

    public FeatureExtractionThreadLBP(IThreadedExperiment experiment,
            FeatureExtractionMethod parentMethod,
            Frame content, int classNumber, boolean isOneClassTrainingClass,
            boolean isTraining) {
        super(experiment, parentMethod, content, classNumber, isOneClassTrainingClass, isTraining);
    }

    /**
     * Called by feature extraction thread, calls the parentMethod's extractTrainingDiscriminativeFeatures()
 and extractDiscriminativeFeatures() methods. 
     * 
     * @param content
     * @param isTraining
     * @return
     * @throws JCeliacGenericException 
     */
    @Override
    public DiscriminativeFeatures extractFeatures(Frame content, boolean isTraining)
            throws JCeliacGenericException 
    {
        
        // this was implemented thread safe. if not, implement the thread safe
        // version here
        DiscriminativeFeatures f;
        if (isTraining) {
            f = super.parentMethod.extractTrainingFeatures(content, this.classNumber);
        } else {
            f = super.parentMethod.extractFeatures(content);
        }
        // this computes the combined histograms, do this within the treath to speedup the entire process
        // later threads would have to block and wait for others (within the mapDistance method)
        //  at this point all data was prepared(), dispose tha redundant info for more memory
        return f;
    }

}
