/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.EFLBP;

import jceliac.experiment.*;
import jceliac.featureExtraction.FLBP.FeatureExtractionParametersFLBP;
import jceliac.logging.*;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersEFLBP
        extends FeatureExtractionParametersFLBP
{

    public static enum EMembershipFunction
    {

        LINEAR,
        LOGISTIC
    }

    public FeatureExtractionParametersEFLBP()
    {
    }

    @Override
    public void report()
    {
        super.report();
        Experiment.log(JCeliacLogger.LOG_INFO, "Membership Function: %s", this.membershipFunction);
    }

    @Override
    public String getMethodName()
    {
        return "EFLBP";
    }
}
