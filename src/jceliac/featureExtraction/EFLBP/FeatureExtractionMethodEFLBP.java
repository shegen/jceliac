/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.EFLBP;

import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.StableCode;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.FLBP.*;
import jceliac.featureExtraction.LBP.*;
import jceliac.tools.filter.*;
import jceliac.tools.data.*;
import jceliac.experiment.*;
import jceliac.logging.*;

/**
 * Extended Fuzzy Local Binary Patterns.
 * <p>
 * @author shegen
 */
@StableCode(date = "16.01.2014", lastModified = "16.01.2014")
public class FeatureExtractionMethodEFLBP
        extends FeatureExtractionMethodFLBP
{

    protected FeatureExtractionParametersEFLBP myParameters;

    public FeatureExtractionMethodEFLBP(FeatureExtractionParameters param)
    {

        super(param);
        // this is just for convenience
        this.myParameters = (FeatureExtractionParametersEFLBP) super.parameters;
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {
        Experiment.log(JCeliacLogger.LOG_HINFO, "Extracting EFLBP-Features from SignalID: %s, FrameID: %d.",
                       content.getSignalIdentifier(), content.getFrameIdentifier());

        // create DiscriminativeFeatures structure once for each input sample
        LBPFeatures features = new LBPFeatures(this.myParameters);
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());

        for(int currentScale = myParameters.getMinScale(); currentScale <= myParameters.getMaxScale();
            currentScale++) {
            if(myParameters.isUseColor()) {

                // Prepare content for scale.
                for(Integer colorChannel : this.myParameters.getColorChannels()) {
                    LBPHistogram histVertical = this.computeDistribution(content.getColorChannel(colorChannel),
                                                                         currentScale, SobelFilter.ORIENTATION_VERTICAL);
                    histVertical.setColorChannel(colorChannel);
                    histVertical.setScale(currentScale);
                    features.addAbstractFeature(histVertical);

                    LBPHistogram histHorizontal = this.computeDistribution(content.getColorChannel(colorChannel),
                                                                           currentScale, SobelFilter.ORIENTATION_HORIZONTAL);
                    histHorizontal.setColorChannel(colorChannel);
                    histHorizontal.setScale(currentScale);
                    features.addAbstractFeature(histHorizontal);

                    LBPHistogram histDiagonal = this.computeDistribution(content.getColorChannel(colorChannel),
                                                                         currentScale, SobelFilter.ORIENTATION_DIAGONAL);
                    histDiagonal.setColorChannel(colorChannel);
                    histDiagonal.setScale(currentScale);
                    features.addAbstractFeature(histDiagonal);
                }

            } else {
                LBPHistogram histGrayVertical = this.computeDistribution(content.getGrayData(), currentScale, SobelFilter.ORIENTATION_VERTICAL);
                histGrayVertical.setColorChannel(DataSource.CHANNEL_GRAY);
                histGrayVertical.setScale(currentScale);
                features.addAbstractFeature(histGrayVertical);

                LBPHistogram histGrayDiagonal = this.computeDistribution(content.getGrayData(), currentScale, SobelFilter.ORIENTATION_DIAGONAL);
                histGrayDiagonal.setColorChannel(DataSource.CHANNEL_GRAY);
                histGrayDiagonal.setScale(currentScale);
                features.addAbstractFeature(histGrayDiagonal);

                LBPHistogram histGrayHorizontal = this.computeDistribution(content.getGrayData(), currentScale, SobelFilter.ORIENTATION_HORIZONTAL);
                histGrayHorizontal.setColorChannel(DataSource.CHANNEL_GRAY);
                histGrayHorizontal.setScale(currentScale);
                features.addAbstractFeature(histGrayHorizontal);

            }
        }
        return features;

    }

    protected LBPHistogram computeDistribution(double[][] dataChannel, int scale, int filterOrientation)
            throws JCeliacGenericException
    {
        LBPHistogram elbpHistogram;

        int neighbors = LBPMultiscale.getNeighborsForScale(scale);
        double radius = LBPMultiscale.getRadiusForScale(scale);
        int margin = (int) Math.ceil(radius);

        DataFilter2D sobelFilter = new SobelFilter(filterOrientation);
        DataFilter2D multiScaleFilter = LBPMultiscale.getFilterForScale(scale);

        double[][] dataSobeled;
        dataSobeled = sobelFilter.filterData(dataChannel);
        dataSobeled = multiScaleFilter.filterData(dataSobeled);
        elbpHistogram = super.computeHistogram(dataSobeled, neighbors, radius, margin, scale);
        elbpHistogram.setSubband(filterOrientation);

        return elbpHistogram;
    }

}
