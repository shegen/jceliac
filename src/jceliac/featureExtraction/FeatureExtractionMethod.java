/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction;

import java.lang.annotation.Annotation;
import jceliac.experiment.FeatureExtractionMethodFactory.*;
import jceliac.JCeliacGenericException;
import jceliac.tools.data.DataReader;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.data.Frame;
import jceliac.experiment.*;
import java.util.*;

/**
 * Basic abstraction for feature extraction methods.
 * <p>
 * @author shegen
 */
public abstract class FeatureExtractionMethod
{

    /**
     * All implemented feature extraction methods.
     */
    public static enum EFeatureExtractionMethod
    {

        /**
         * Local Binary Patterns - Stable.
         */
        LBP("LBP"),
        /**
         * Local Ternary Patterns - Stable.
         */
        LTP("LTP"),
        /**
         * Extended Local Binary Patterns - Stable.
         */
        ELBP("ELBP"),
        /**
         * Extended Local Ternary Patterns - Stable.
         */
        ELTP("ELTP"),
        /**
         * LBP with contrast measure - Stable.
         */
        LBPC("LBPC"),
        /**
         * Wavelet Transformed Local Binary Patterns - Stable.
         */
        WTLBP("WTLBP"),
        /**
         * Fuzzy Local Binary Patterns - Stable.
         */
        FLBP("FLBP"),
        /**
         * Extended Fuzzy Local Binary Patterns - Stable.
         */
        EFLBP("EFLBP"),
        /**
         * Coocurrence Local Binary Patterns - Experimental.
         */
        COOCLBP("COOCLBP"),
        /**
         * Coocurrence Fuzzy Local Binary Patterns - Experimental.
         */
        COOCFLBP("COOCFLBP"),
        /**
         * Temporal Statistics - Stable.
         */
        TEMPORAL_STATISTICS("TEMPORAL_STATISTICS"),
        /**
         * Affine Scale LBP - Experimental.
         */
        AFFINESCALELBP("AFFINESCALELBP"),
        /**
         * Affine Scale LBP with Scale Prediction - Experimental.
         */
        AFFINESCALELBPSP("AFFINESCALELBPSP"),
        /**
         * Scale Adaptive LBP with Scale Bias - Early Experimental Version of
         * SALBP.
         */
        SCALEBIAS("SCALEBIAS"),
        /**
         * Scale Invariant LBP (ICASSP 2014) - Stable.
         */
        SALBP("SALBP"),
        /**
         * Scale Invariant LTP (ICASSP 2014) - Stable.
         */
        SALTP("SALTP"),
        /**
         * Scale Invariant FLBP (ICASSP 2014) - Stable.
         */
        SAFLBP("SAFLBP"),
        /**
         * Affine Adaptive LBP - Experimental.
         */
        AALBP("AALBP"),
        /**
         * Scale and Rotation Invariant LBP (ICPR 2014) - Stable.
         */
        SOALBP("SOALBP"),
        /**
         * Scale and Rotation Invariant LTP (ICPR 2014) - Stable.
         */
        SOALTP("SOALTP"),
        /**
         * Scale and Rotation Invariant FLBP (ICPR 2014) - Stable.
         */
        SOAFLBP("SOAFLBP"),
        /**
         * Multi Fractal Spectrum - Stable. 
         */
        MULTI_FRACTAL_SPECTRUM("MULTI_FRACTAL_SPECTRUM"),
         /**
         * Multi Fractal Spectrum using MR8 filters and Bag of Visual Words - Stable. 
         */
        MULTI_FRACTAL_SPECTRUM_MR8("MULTI_FRACTAL_SPECTRUM_MR8"),
        /**
         * Intersecting cortical model - Stable.
         */
        ICM("ICM"),
        /**
         * Spiking cortical model - Stable. 
         */
        SCM("SCM"),
        /**
         * Dual Tree Complex Wavelet Transform, classic features - Stable.
         */
        DTCWT_CLASSIC("DTCWT_CLASSIC"),
        /**
         * Double Dual Tree Complex Wavelet Transform features - Stable.
         */
        D3TCWT_LOCAL("D3TCWT_LOCAL"),
        /**
         * Double Dual Tree Complex Wavelet Transform features with cyclic shifts - Stable.
         */
        D3TCWT_CYCLIC_SHIFT("D3TCWT_CYCLIC_SHIT"),
        /**
         * Dual Tree Transform on Logpolar transformed data, same features as classic DTCWT. 
         */
        DTCWT_LOGPOLAR("DTCWT_LOGPOLAR"),
        /**
         * Contrast Feature. 
         */
        CONTRAST("CONTRAST"),
        /**
         * Histogram of Gradient features.
         */
        HOG("HOG"),
        /**
         * Edge features. 
         */
        ECM("ECM"),
        /**
         * Dominant Scale.
         */
        DOMINANT_SCALE("DOMINANT_SCALE"),
        /**
         * Experimental.
         */
        SCALE_RESPONSE("SCALE_RESPONSE"),
        /**
         * Scale adaptive LBP following Li et al. 
         */
        LI_LBP("LI_LBP"),
        /**
         * Dense SIFT not usable! Just implemented for performance test. 
         */
        DSIFT("DSIFT"),
        /**
         * Affine Invariant Regions.
         */
        AFFINE_REGIONS("AFFINE_REGIONS");
        
        protected String stringValue = "";

        private EFeatureExtractionMethod(String stringValue)
        {
            this.stringValue = stringValue;
        }

        public static EFeatureExtractionMethod valueOfIgnoreCase(String stringValue)
                throws JCeliacGenericException
        {
            for(EFeatureExtractionMethod value : EFeatureExtractionMethod.values()) {
                if(value.name().equalsIgnoreCase(stringValue)) {
                    return value;
                }
            }
            throw new JCeliacGenericException("Unknown EFeatureExtractionMethod: " + stringValue);
        }

        public static EFeatureExtractionMethod mapStringToValue(String stringValue)
        {
            return EFeatureExtractionMethod.valueOf(stringValue);
        }

        public String getStringValue()
        {
            return stringValue;
        }

    };

    protected FeatureExtractionParameters parameters;

    public FeatureExtractionMethod(FeatureExtractionParameters param)
    {
        this.parameters = param;
    }
    
    public void logAnnotation()
    {
       
        Annotation [] a = this.getClass().getAnnotations();
        ExperimentalCode [] experimentalAnnotation = this.getClass().getAnnotationsByType(ExperimentalCode.class);
        StableCode [] stableAnnotation = this.getClass().getAnnotationsByType(StableCode.class);
        
        if(experimentalAnnotation.length > 0) {
            Experiment.log(JCeliacLogger.LOG_WARN, "Feature Extraction Code marked as EXPERIMENTAL: %s", experimentalAnnotation[0].comment());
        }
        
        if(stableAnnotation.length > 0) {
            Experiment.log(JCeliacLogger.LOG_INFO, "Feature Extraction Code marked as STABLE: %s", stableAnnotation[0].comment());
        }
    }

    public AbstractFeatureExtractionThread 
        createFeatureExtractionThread(IThreadedExperiment exp, Frame content, 
                                      int classIndex, boolean isOneClassTrainingClass, 
                                      boolean isTraining)
    {
         return new FeatureExtractionThread(exp, this, content, classIndex, isOneClassTrainingClass, isTraining);
    }


    public abstract DiscriminativeFeatures extractFeatures(Frame content) throws
            JCeliacGenericException;

    
    /**
     * This should be overwritten if a method needs to normalize the features 
     * according to other feature vectors such like DTCWT. 
     * 
     * @param features
     * @return Normalized Features
     * @throws JCeliacGenericException 
     */
    public ArrayList <DiscriminativeFeatures> normalizeFeatures(ArrayList <DiscriminativeFeatures> features) 
            throws JCeliacGenericException
    {
        return features;
    }
    
    
    /**
     * Computes training features with known class index, ignored by most
     * methods. For most methods this must not be changed, but some methods may
     * want to treat training features a bit differently, be careful here, this
     * might introduce some unwanted bias to the results if not used carefully!
     * <p>
     * @param content    Frame data.
     * @param classIndex Class index of frame.
     * <p>
     * @return Extracted DiscriminativeFeatures.
 <p>
     * @throws JCeliacGenericException
     */
    public DiscriminativeFeatures extractTrainingFeatures(Frame content, int classIndex)
            throws JCeliacGenericException
    {
        return extractFeatures(content);
    }

    /**
     * Extracts DiscriminativeFeatures from a sequence of frames.
     * <p>
     * @param sequence List of frames.
     * <p>
     * @return List of features.
     * <p>
     * @throws JCeliacGenericException
     */
    public ArrayList<DiscriminativeFeatures> extractFeaturesFromSequence(ArrayList<Frame> sequence)
            throws JCeliacGenericException
    {
        Experiment.log(JCeliacLogger.LOG_HINFO, "Using Standard Frame by Frame Feature Extraction Implementation!");
        ArrayList<DiscriminativeFeatures> features = new ArrayList<>();
        for(Frame tmpFrame : sequence) {
            DiscriminativeFeatures tmp = this.extractFeatures(tmpFrame);
            features.add(tmp);
        }
        return features;
    }

    /**
     * Sets up lookup tables and other things specific to the method.
     * @throws jceliac.JCeliacGenericException
     */
    public abstract void initialize() throws JCeliacGenericException;

    /**
     * This is used to estimate parameters of the image set, such as thresholds.
     * <p>
     * @param readersByClass Data reader for all classes.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public abstract void estimateParameters(ArrayList<DataReader> readersByClass)
            throws JCeliacGenericException;

    /**
     * This is used to estimate parameters iteratively frame by frame.
     * <p>
     * @param nextFrame Next input frame to improve estimation with.
     */
    public abstract void estimateParametersIterative(Frame nextFrame);

    public void report()
    {
        parameters.report();
    }

    public FeatureExtractionParameters getMethodParameters()
    {
        return parameters;
    }

    public void cleanup()
    {

    }

}
