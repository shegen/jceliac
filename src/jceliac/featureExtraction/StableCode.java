/*
 * 
 JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at> 
 The source code of this software is not yet made available to the open source 
 community. If you obtained this source code without permission of the author 
 please remove all the source files.

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 * Each line should be prefixed with  * 
 */
package jceliac.featureExtraction;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author shegen
 */
@Documented
@Retention(value=RetentionPolicy.RUNTIME) // needed to query them at runtime for subclass
public @interface StableCode
{

    String date();
    String comment() default "Code is stable and tested.";
    String lastModified() default "N/A";
}
