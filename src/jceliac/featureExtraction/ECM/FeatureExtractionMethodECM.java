/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.ECM;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.AbstractFeatureVector;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FixedFeatureVector;
import jceliac.featureExtraction.StandardEuclideanFeatures;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.Frame;
import jceliac.tools.filter.DataFilter2D;

/**
 *
 * @author shegen
 */
public class FeatureExtractionMethodECM
        extends FeatureExtractionMethod
{

    protected ArrayList<double[][]> edgeFilters;
    protected int [][] directionVector;
    protected FeatureExtractionParametersECM myParameters;
    
    public FeatureExtractionMethodECM(FeatureExtractionParameters param)
    {
        super(param);
        this.myParameters = (FeatureExtractionParametersECM) param;
    }
    
    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {
        StandardEuclideanFeatures features = new StandardEuclideanFeatures(this.myParameters);
        if(this.myParameters.isUseColor()) 
        {
            for(Integer colorChannel : this.myParameters.getColorChannels()) {
                 AbstractFeatureVector vec = this.computeFeatures(content.getColorChannel(colorChannel));
                 features.addAbstractFeature(vec);
            }
        }else {
            AbstractFeatureVector vecGray = this.computeFeatures(content.getGrayData());
            features.addAbstractFeature(vecGray);
        }
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());
        return features;
    }
    
    public FixedFeatureVector computeFeatures(double [][] data) {
        
        double [][][] allEdgeData = new double[this.edgeFilters.size()][][];
        for(int i = 0; i < this.edgeFilters.size(); i++) {
            double [][] edgeData = DataFilter2D.correlateMirror(data, this.edgeFilters.get(i));
            allEdgeData[i] = edgeData;
        }
        
        double [][] maxEdgeData = new double[data.length][data[0].length];
        double [][] maxEdgeIndices = new double[data.length][data[0].length];
        this.getMaxEdgeAndIndex(maxEdgeData, maxEdgeIndices, allEdgeData);
        
        double threshold = 0.25 * ArrayTools.max(maxEdgeData);
        double [][] edgeMask = this.computeEdgeMask(maxEdgeData, maxEdgeIndices, threshold);
        
        double [] rawFeatureData = new double[64 * this.myParameters.distances.length];
        int rawFeatureIndex = 0;
        for(int distanceIndex = 0; distanceIndex < this.myParameters.distances.length; 
            distanceIndex++) {
            int distance = this.myParameters.distances[distanceIndex];
            
            double [][] glcmsSum = new double[8][8];
            for(int i = 0; i < this.directionVector.length; i++) {
                int distY = distance * this.directionVector[i][0];
                int distX = distance * this.directionVector[i][1];
                double [][] glcms = this.computeGraylevelCoocurrenceMatrix(edgeMask, distX, distY);
                glcmsSum = this.addArrays(glcms, glcmsSum);
            }
            this.scalarMult(glcmsSum, 1.0d/this.directionVector.length);
            double [] reshapedGlcmsSum = this.reshape(glcmsSum);
            System.arraycopy(reshapedGlcmsSum, 0, rawFeatureData, rawFeatureIndex, reshapedGlcmsSum.length);
            rawFeatureIndex += reshapedGlcmsSum.length;
        }
        FixedFeatureVector vec = new FixedFeatureVector();
        vec.setData(rawFeatureData);
        return vec;
    }

    
    private double [] reshape(double [][] array) 
    {
        double [] reshapedArray = new double[array.length * array[0].length];
        int index = 0;
        for(int i = 0; i < array.length; i++) {
            for(int j = 0; j < array[0].length; j++) {
                reshapedArray[index] = array[i][j];
                index++;
            }
        }
        return reshapedArray;
    }
    private void scalarMult(double [][] array, double scalar) {
        for(int i = 0; i < array.length; i++) {
            for(int j = 0; j < array[0].length; j++) {
               array[i][j] = array[i][j] * scalar;
            }
        }
    }
    
    private double [][] addArrays(double [][] array1, double [][] array2) 
    {
        double [][] ret = new double[array1.length][array1[0].length];
        for(int i = 0; i < array1.length; i++) {
            for(int j = 0; j < array1[0].length; j++) {
                ret[i][j] = array1[i][j] + array2[i][j];
            }
        }
        return ret;
    }
    
    private double[][] computeGraylevelCoocurrenceMatrix(double[][] data, int deltaX, int deltaY)
    {
        double[][] coocMatrix = new double[this.edgeFilters.size()+1][this.edgeFilters.size()+1];

        for(int p = 0; p < data.length; p++) {
            for(int q = 0; q < data[0].length; q++) {
                // GRAYCOMATRIX ignores border pixels, if the corresponding neighbors
                // defined by 'Offset' fall outside the image boundaries.
                if(p + deltaX >= data.length || p + deltaX < 0
                   || q + deltaY >= data[0].length || q + deltaY < 0) {
                    continue;
                }
                int value1 = (int) data[p][q];
                int value2 = (int) data[p + deltaX][q + deltaY];
                coocMatrix[value1][value2]++;
            }
        }
        // return 8x8 matrix
        double [][] ret = new double[coocMatrix.length-1][coocMatrix[0].length-1];
        for(int i=1;i< coocMatrix.length; i++) {
            for(int j=1; j < coocMatrix[0].length; j++) {
                ret[i-1][j-1] = coocMatrix[i][j];
            }
        }
        return ret;
    }
    
    private void getMaxEdgeAndIndex(double [][] maxEdgeData, double [][] maxEdgeIndices, 
                                   double [][][] allEdgeData)
    {
       
        for(int i = 0; i < allEdgeData[0].length; i++) {
            for(int j = 0; j < allEdgeData[0][0].length; j++) {
                double maxValue = Double.MIN_VALUE;
                int maxIndex = 0;
                for(int k = 0; k < allEdgeData.length; k++) {
                    if(allEdgeData[k][i][j] > maxValue) {
                        maxValue = allEdgeData[k][i][j];
                        maxIndex = k;
                    }
                }
                maxEdgeData[i][j] = maxValue;
                maxEdgeIndices[i][j] = maxIndex;
            }
        }
    }
    
    private double [][] computeEdgeMask(double [][] maxEdgeData, double [][] maxEdgeIndices, double threshold)
    {
        double [][] edgeMask = new double[maxEdgeData.length][maxEdgeData[0].length];
        for(int i = 0; i < edgeMask.length; i++) {
            for(int j = 0; j < edgeMask[0].length; j++) {
                if(maxEdgeData[i][j] >= threshold) {
                    edgeMask[i][j] = maxEdgeIndices[i][j] + 1;
                }
            }
        }
        return edgeMask;
    }
    
    @Override
    public void initialize()
            throws JCeliacGenericException
    {
        this.edgeFilters = new ArrayList<>();

        this.edgeFilters.add(new double[][]{{1, 0, -1}, {2, 0, -2}, {1, 0, -1}});
        this.edgeFilters.add(new double[][]{{2, 1, 0}, {1, 0, -1}, {0, -1, -2}});
        this.edgeFilters.add(new double[][]{{1, 2, 1}, {0, 0, 0}, {-1, -2, -1}});
        this.edgeFilters.add(new double[][]{{0, 1, 2}, {-1, 0, 1}, {-2, -1, 0}});
        this.edgeFilters.add(new double[][]{{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}});
        this.edgeFilters.add(new double[][]{{-2, -1, 0}, {-1, 0, 1}, {0, 1, 2}});
        this.edgeFilters.add(new double[][]{{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}});
        this.edgeFilters.add(new double[][]{{0, -1, -2}, {1, 0, -1}, {2, 1, 0}});

        this.directionVector = new int[][]{{-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, 
                                              {0, 1}, {1, -1}, {1, 0}, {1, 1}};
    }

    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass)
            throws JCeliacGenericException
    {
    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {
    }

}
