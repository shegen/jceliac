/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.LBPC;

import jceliac.featureExtraction.LBP.*;
import java.util.*;
import jceliac.*;

/**
 * Histogram of LBPC features, they use an additional dimension for the
 * contrast.
 * <p>
 * @author shegen
 */
public class LBPCHistogram
        extends LBPHistogram
{

    private final ArrayList<LBPHistogram> histograms;
    private final int contrastBinCount;

    public LBPCHistogram(int contrastBinCount)
    {
        this.contrastBinCount = contrastBinCount;
        this.histograms = new ArrayList<>();

    }

    public void setData(double[] hist, int contrastBin)
    {
        LBPHistogram histogram = new LBPHistogram();
        histogram.setData(hist);
        this.histograms.add(contrastBin, histogram);
    }

    /**
     * Assembles the histogram by copying all stored histograms into a single
     * histogram.
     */
    protected void assembleHistogram()
    {
        int histLength = this.histograms.get(0).getHistogramDimension();
        this.data = new double[this.contrastBinCount * histLength];
        this.histogramDimension = this.data.length;

        for(int index = 0; index < this.histograms.size(); index++) {
            LBPHistogram hist = this.histograms.get(index);
            System.arraycopy(hist.getPreparedData(), 0, this.data, index * histLength, histLength);
        }
    }

    @Override
    public int getHistogramDimension()
    {
        return (this.contrastBinCount * this.histograms.get(0).getHistogramDimension());
    }

    /**
     * Assembles and normalizes the histogram data.
     */
    @Override
    public synchronized void prepare()
    {
        if(this.data == null) {
            this.assembleHistogram();
            this.data = normalize(this.data);
        }
    }

    @Override
    public double[] getUniformData()
            throws JCeliacLogableException
    {
        if(this.uniformData != null) {
            return this.uniformData;
        }

        int uniformIndex = 0;
        int neighbors = (int) (Math.log(this.histograms.get(0).getPreparedData().length) / Math.log(2));
        int histLen = this.histograms.get(0).getPreparedData().length;
        this.uniformData = new double[histLen * this.contrastBinCount];

        for(LBPHistogram hist : this.histograms) {
            // the prepare() call is done inside this method, we do not need it in here
            double[] unifData = hist.getUniformData();
            System.arraycopy(unifData, 0, this.uniformData, uniformIndex * unifData.length, unifData.length);
            uniformIndex++;
        }
        this.uniformData = normalize(this.uniformData);
        return this.uniformData;
    }

    @Override
    public void disposeData()
    {
        for(LBPHistogram hist : this.histograms) {
            hist.disposeData();
        }
    }

}
