/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.LBPC;

import jceliac.featureExtraction.LBP.FeatureExtractionParametersLBP;
import jceliac.experiment.*;
import jceliac.logging.*;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersLBPC
        extends FeatureExtractionParametersLBP
{

    /**
     * The number of contrast bins influences the binning of the 2-dimensional
     * histogram. Default is 6.
     */
    protected int contrastBins = 6;

    public int getContrastBins()
    {
        return contrastBins;
    }

    public void setContrastBins(int contrastBins)
    {
        this.contrastBins = contrastBins;
    }

    @Override
    public String getMethodName()
    {
        return "LBPC";
    }

    @Override
    public void report()
    {
        super.report();
        Experiment.log(JCeliacLogger.LOG_INFO, "Contrast-Bins: %d", contrastBins);
    }

}
