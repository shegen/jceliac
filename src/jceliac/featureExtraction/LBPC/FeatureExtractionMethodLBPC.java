/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.LBPC;

import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.StableCode;
import jceliac.featureExtraction.LBP.*;
import java.util.ArrayList;
import jceliac.experiment.*;
import jceliac.tools.data.*;
import jceliac.tools.math.*;
import jceliac.logging.*;
import java.util.*;
import jceliac.JCeliacGenericException;

/**
 * Local Binary Patterns with additional contrast measure.
 * <p>
 * @author shegen
 */
@StableCode(comment = "This code is trusted.",
            date = "16.01.2014",
            lastModified = "16.01.2014")
public class FeatureExtractionMethodLBPC
        extends FeatureExtractionMethodLBP
{

    private final FeatureExtractionParametersLBPC myParameters;
    private double[] empiricalDistributionFunction;

    private final IterativeVariables iterativeVariables;

    public FeatureExtractionMethodLBPC(FeatureExtractionParameters param)
    {
        super(param);
        this.myParameters = (FeatureExtractionParametersLBPC) param;
        this.iterativeVariables = new IterativeVariables(this.myParameters.getContrastBins());
    }

    /**
     * Used for estimating the parameters iteratively.
     */
    private class IterativeVariables
    {

        public double[] empFunc;
        public int count;

        public IterativeVariables(int contrastBins)
        {
            this.empFunc = new double[contrastBins];
        }

    }

    private int lookupContrastBin(double contrast)
    {
        for(int i = 0; i < this.empiricalDistributionFunction.length; i++) {
            // this implicitly assumes that contrast is >= empiricalDistributionFunction[i-1] !!
            if(contrast < this.empiricalDistributionFunction[i]) {
                return i;
            }
        }
        // this can happen because we build the average of the percentiles, return max bin
        return myParameters.getContrastBins() - 1;
    }

    @Override
    protected LBPHistogram computeHistogram(double[][] data, int neighbors, double radius, int margin, int scale)
    {
        // we let the LBPCHistogram class handle the histogram, this makes extracting
        // uniform patterns easier
        double[][] hist = new double[myParameters.getContrastBins()][getNumberOfBins(neighbors)];
        double std = this.calculateContrastStdev(data, scale);

        int shiftMax = neighbors - 1;
        for(int x = margin; x < data.length - margin; x++) {
            for(int y = margin; y < data[0].length - margin; y++) {

                // THE MATLAB CODE WAS BUGGED, this is correct!
                double center = (double) data[x][y];
                int pattern = 0;
                double contrast = calculateContrastForPattern(data, x, y, scale, neighbors);

                for(int p = 0; p < neighbors; p++) {
                    double neighbor = getNeighbor(data, x, y, p, scale);

                    if(neighbor >= center) {
                        // this is in reverse order from the matlab thingy to speed up things
                        pattern |= (1 << (shiftMax - p));
                    }
                }
                /* reorder pattern to comply with the other implementations,
                 * freaking hokus pokus!! pick top 3 bit bits from msb
                 */
                int tmp_h = pattern & 0xE0; // 224
                // pick bottom 5 bits from lsb
                int tmp_l = pattern & 0x1F; // 31;
                int pattern_fixed = (tmp_h >> 5) | (tmp_l << 3);

                int contrastBin = lookupContrastBin((contrast / std));
                hist[contrastBin][pattern_fixed] = hist[contrastBin][pattern_fixed] + 1.0d;
            }
        }
        LBPCHistogram lbpcHistogram = new LBPCHistogram(myParameters.getContrastBins());
        for(int cIndex = 0; cIndex < myParameters.getContrastBins(); cIndex++) {
            lbpcHistogram.setData(hist[cIndex], cIndex);
        }
        return lbpcHistogram;
    }

    /**
     * Estimates the empirical distribution of contrast to find a good binning
     * of contrast instead of using an equi-spaced binning.
     * <p>
     * @param dataReaders Data readers for all images.
     * @throws jceliac.JCeliacGenericException
     */
    @Override
    public void estimateParameters(ArrayList<DataReader> dataReaders)
            throws JCeliacGenericException
    {
        double[] empFunc = new double[myParameters.getContrastBins()];
        int imgCount = 0;
        Experiment.log(JCeliacLogger.LOG_INFO, "Estimating LBPC Parameters (empirical contrast distribution function).");

        for(int classNum = 0; classNum < dataReaders.size(); classNum++) {
            DataReader current = dataReaders.get(classNum);
            while(current.hasNext()) {
                DataSource currentFile = (DataSource) current.next();
                ContentStream cstream = currentFile.createContentStream();

                while(cstream.hasNext()) {
                    Frame content = (Frame) cstream.next();
                    this.estimateParametersIterative(content);
                    double[][] data = content.getGrayData();
                    double[] normContrast = calculateNormalizedContrast(data);
                    double[] tmpFunc = estimateEmpiricalDistribution(normContrast, myParameters.getContrastBins());

                    for(int i = 0; i < empFunc.length; i++) {
                        empFunc[i] += tmpFunc[i];
                    }
                    imgCount++;
                }
            }
            current.reset();
        }
        for(int i = 0; i < empFunc.length; i++) {
            empFunc[i] = (empFunc[i] / imgCount);
        }
        this.empiricalDistributionFunction = empFunc;

        // print out the empirical distribution function
        Experiment.log(JCeliacLogger.LOG_INFO, "Empirical Distribution Function using %d contrast-bins estimated (based on %d images)!", myParameters.getContrastBins(), imgCount);
        for(int i = 0; i < empFunc.length; i++) {
            Experiment.log(JCeliacLogger.LOG_INFO, "LBPC - Bin-(%d): %f", i, this.empiricalDistributionFunction[i]);
        }
    }

    /**
     * Estimates the empirical distribution of contrast iteratively to find a
     * good binning of contrast instead of using an equi-spaced binning.
     * <p>
     * @param content Frame data.
     */
    @Override
    public void estimateParametersIterative(Frame content)
    {

        double[][] data = content.getGrayData();
        double[] normContrast = calculateNormalizedContrast(data);
        double[] tmpFunc = estimateEmpiricalDistribution(normContrast, this.myParameters.getContrastBins());

        for(int i = 0; i < this.iterativeVariables.empFunc.length; i++) {
            this.iterativeVariables.empFunc[i] += tmpFunc[i];
        }
        this.iterativeVariables.count++;

        double[] curFunc = new double[this.myParameters.getContrastBins()];
        for(int i = 0; i < curFunc.length; i++) {
            curFunc[i] = (this.iterativeVariables.empFunc[i] / this.iterativeVariables.count);
        }
        this.empiricalDistributionFunction = curFunc;
    }

    /**
     * Estimates the empirical distribution. This approximates percentiles, it's
     * not exactly the n-th percentile.
     * <p>
     * @param contrastValues Contrast values.
     * @param numSupport     Number of support points.
     * <p>
     * @return Estimated distribution.
     */
    private double[] estimateEmpiricalDistribution(double[] contrastValues, int numSupport)
    {
        double percentileStepSize = (double) ((double) contrastValues.length / (double) numSupport);

        if(numSupport > 100) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Number of support points too large for estimating distribution function!");
            return null;
        }
        if(numSupport <= 0) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Number of support points too small for estimating distribution function!");
            return null;
        }
        double[] empFunc = new double[numSupport];
        for(int index = 0; index < numSupport; index++) {
            empFunc[index] = contrastValues[(int) (percentileStepSize * (index + 1)) - 1];
        }
        return empFunc;
    }

    /**
     * Calculates the contrast for a single pattern.
     * <p>
     * @param data          Raw image data.
     * @param x             Coordinate x.
     * @param y             Coordinate y.
     * @param scale         LBP-scale.
     * @param neighborCount Number of neighbors.
     * <p>
     * @return Contrast of the pattern at (x,y).
     */
    private double calculateContrastForPattern(double[][] data, int x, int y, int scale, int neighborCount)
    {
        double[] narray = new double[neighborCount];
        double m = 0;

        for(int p = 0; p < neighborCount; p++) {
            double neighbor = getNeighbor(data, x, y, p, scale);
            narray[p] = neighbor;
            m += neighbor;
        }
        m = (m / neighborCount);
        double contrast = 0;

        for(int p = 0; p < neighborCount; p++) {
            contrast += ((narray[p] - m) * (narray[p] - m));
        }
        return (contrast / neighborCount);
    }

    /**
     * Calculates the normalized contrast using division through the standard
     * deviation.
     * <p>
     * @param data Image data.
     * <p>
     * @return Normalized global contrast.
     */
    private double[] calculateNormalizedContrast(double[][] data)
    {
        int neighbors = LBPMultiscale.getNeighborsForScale(1);
        int margin = (int) Math.ceil(LBPMultiscale.getRadiusForScale(1));
        double[] contrastValues = new double[(data.length - (2 * margin)) * (data[0].length - (2 * margin))];

        int contrastIndex = 0;
        for(int x = margin; x < data.length - margin; x++) {
            for(int y = margin; y < data[0].length - margin; y++) {
                double contrast = this.calculateContrastForPattern(data, x, y, 1, neighbors);
                contrastValues[contrastIndex] = contrast;
                contrastIndex++;
            }
        }
        double contrastStd = MathTools.stdev(contrastValues);
        for(int i = 0; i < contrastValues.length; i++) {
            contrastValues[i] = (contrastValues[i] / contrastStd);
        }
        Arrays.sort(contrastValues);
        return contrastValues;
    }

    /**
     * Calculates the standard deviation of contrasts.
     * <p>
     * @param data Image data.
     * <p>
     * @return Standard deviation of contrasts.
     */
    private double calculateContrastStdev(double[][] data, int scale)
    {
        int neighbors = LBPMultiscale.getNeighborsForScale(scale);
        int margin = (int) Math.ceil(LBPMultiscale.getRadiusForScale(scale));
        double[] contrastValues = new double[(data.length - (2 * margin)) * (data[0].length - (2 * margin))];

        int contrastIndex = 0;
        for(int x = margin; x < data.length - margin; x++) {
            for(int y = margin; y < data[0].length - margin; y++) {
                double contrast = calculateContrastForPattern(data, x, y, scale, neighbors);
                contrastValues[contrastIndex] = contrast;
                contrastIndex++;
            }
        }
        double contrastStd = MathTools.stdev(contrastValues);
        return contrastStd;
    }

}
