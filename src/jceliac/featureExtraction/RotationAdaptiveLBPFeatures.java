/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction;

import java.util.ArrayList;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ScaleEstimation;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.LBP.LBPFeatures;
import jceliac.featureExtraction.SALBP.AffineLBPFeatures;
import jceliac.featureExtraction.SALBP.LBPScaleBias;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;
import jceliac.tools.arrays.ArrayTools;

/**
 * Abstraction for rotation adaptive LBP features, holds a number of LBP
 features and matches the best pair between two RotationAdaptiveLBPDiscriminativeFeatures.
 * <p>
 * @author shegen
 */
public class RotationAdaptiveLBPFeatures
        extends AffineLBPFeatures
{

    private int featureID = 0;
    protected ArrayList <LBPFeatures> rotatedFeatures = new ArrayList<>();
    private boolean isTrainingFeature = false;
    
    public RotationAdaptiveLBPFeatures(FeatureExtractionParameters parameters)
    {
        super(parameters);
    }

    public RotationAdaptiveLBPFeatures(FeatureExtractionParameters parameters,
                                       ScaleEstimation scaleEstimation,
                                       LBPScaleBias scaleBias)
    {
        super(parameters, scaleEstimation, scaleBias);
    }

    public RotationAdaptiveLBPFeatures(ScaleEstimation scaleEstimation,
                                       LBPScaleBias scaleBias)
    {
        super(scaleEstimation, scaleBias);
    }

    @Override
    public DiscriminativeFeatures cloneFeatureSubset(FeatureVectorSubsetEncoding subset)
            throws JCeliacGenericException
    {
        RotationAdaptiveLBPFeatures featureSubset = new RotationAdaptiveLBPFeatures(super.featureExtractionParameters);
        featureSubset.isTrainingFeature = this.isTrainingFeature;
        featureSubset.scaleBias = this.scaleBias;
        featureSubset.scaleEstimation = this.scaleEstimation;
        
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
        
        ArrayList <LBPFeatures> clonedFeatures = new ArrayList <>();
        for(LBPFeatures tmp : this.rotatedFeatures) {
            clonedFeatures.add((LBPFeatures)tmp.cloneFeatureSubset(subset));
        }
        featureSubset.rotatedFeatures = clonedFeatures;
        return featureSubset; 
    }

    public void addRotatedFeature(LBPFeatures features)
    {
        this.rotatedFeatures.add(features);
        this.featureID++;
    }

    public void flagAsTestFeature()
    {
        this.isTrainingFeature = false;
    }

    public void flagAsTrainingFeature()
    {
        this.isTrainingFeature = true;
    }

    @Override
    public int getFeatureVectorDimensionality()
    {
        return this.rotatedFeatures.get(0).getFeatureVectorDimensionality();
    }

    
    /**
     * Computes all distances between paris of sub-features of two
     * RotationAdaptiveLBPDiscriminativeFeatures and returns the distance with the best match.
     * <p>
     * @param them   Other features.
     * @param subset Feature subset.
     * @param type   Distance type.
     * <p>
     * @return The best distance between a pair of sub-features.
     * <p>
     * @throws JCeliacGenericException
     */
    @Override
    public double doDistanceTo(DiscriminativeFeatures them)
            throws JCeliacGenericException
    {
        RotationAdaptiveLBPFeatures themCasted = (RotationAdaptiveLBPFeatures) them;
        double bestDistance = Double.NaN;
        for(int i = 0; i < this.rotatedFeatures.size(); i++) {
            for(int j = 0; j < themCasted.rotatedFeatures.size(); j++) {
                DiscriminativeFeatures fi = this.rotatedFeatures.get(i);
                DiscriminativeFeatures fj = themCasted.rotatedFeatures.get(j);
                // XXX: maybe I broke this by not using the subsets anymore?!
                double distance = fi.distanceTo(fj);
                if(Double.isNaN(bestDistance) || distance < bestDistance) {
                    bestDistance = distance;
                }
            }
        }
        if(Double.isNaN(bestDistance)) {
            throw new JCeliacGenericException("Best distance between faetures is NaN!");
        }
        return bestDistance;
    }

    @Override
    public void setSignalIdentifier(String identifier)
    {
        super.setSignalIdentifier(identifier);
        this.rotatedFeatures.stream().forEach((a) -> a.setSignalIdentifier(identifier));
    }

    @Override
    public void setFrameNumber(int identifier)
    {
        super.setFrameNumber(identifier);
        this.rotatedFeatures.stream().forEach((a) -> a.setFrameNumber(identifier));
    }
    
    public boolean isEmpty()
            throws JCeliacGenericException
    {
        for(LBPFeatures ftmp : this.rotatedFeatures) {
            if(ArrayTools.sum(ftmp.getAllFeatureVectorData()) == 0) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isSane()
            throws JCeliacGenericException
    {
         for(LBPFeatures ftmp : this.rotatedFeatures) {
             if(ftmp == null && ftmp.getAllFeatureVectorData() != null) {
                 return false;
             }
         }
         if(this.rotatedFeatures.size() < 1) {
             return false;
         }
         return true;
    }

}
