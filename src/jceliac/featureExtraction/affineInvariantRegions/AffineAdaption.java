/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jceliac.featureExtraction.affineInvariantRegions;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jceliac.JCeliacGenericException;
import jceliac.gui.ImageViewerFrame;
import jceliac.tools.data.DataSource;
import jceliac.tools.filter.scalespace.ScalespacePoint;
import jceliac.tools.interpolator.BilinearInterpolator;
import jceliac.tools.interpolator.Interpolator;
import jceliac.tools.math.MatrixTools;

/**
 *
 * @author sebi
 */
public class AffineAdaption {

    private double [][] afftransMatrix;
    private int centerX, centerY;
    private double [][] imageData;
    private int size;
    
    public AffineAdaption(double [][] imageData, ScalespacePoint ip) 
    {
        this.afftransMatrix = ip.getSecondMomentMatrix().getAsNormalizedArray(ip.getSigma());
        this.centerX = ip.getCoordX();
        this.centerY = ip.getCoordY();
        this.imageData = imageData;
        this.size = (int)Math.round(ip.getSigma() * Math.sqrt(2.0d) * 2); 
    }

    public double [][] getAffineNormalizedData() 
            throws JCeliacGenericException
    {
        double [][] data = this.extractArea(this.imageData, this.centerX, this.centerY, this.size+5);
        double [][] normalized =  this.performAffineNormalization(data, afftransMatrix, data.length/2.0d, data[0].length/2.0d);
        return this.extractArea(normalized, this.size+5, this.size+5, this.size);
    }
    
    private boolean [][] mask;
    
    public boolean [][] getMask()
    {
        return this.mask;
    }
    private double[][] performAffineNormalization(double[][] imageData, double[][] afftransMatrix,
                                                  double centeri, double centerj)
            throws JCeliacGenericException 
    {
        Interpolator interp = new BilinearInterpolator();
        double[][] mtrans = MatrixTools.squareRoot(afftransMatrix[0][0], afftransMatrix[0][1],
                                                   afftransMatrix[1][0], afftransMatrix[1][1]);
        double[][] transformedImage = new double[imageData.length][imageData[0].length];
        this.mask = new boolean[transformedImage.length][transformedImage[0].length];
        
        for (int i = 0; i < imageData.length; i++) {
            for (int j = 0; j < imageData[0].length; j++) {
               // checked the ordering or i and j, its ok
               double transformedPosi = (mtrans[1][0] * (i - centeri) + mtrans[1][1] * (j - centerj) + centerj);
               double transformedPosj = (mtrans[0][0] * (i - centeri) + mtrans[0][1] * (j - centerj) + centeri);
               
               boolean valid = true;
                if (transformedPosi < 0 || transformedPosj < 0 ||
                    transformedPosi >= imageData.length-1 ||    transformedPosj >= imageData[0].length-1 ) 
                {
                   valid = false;
                }
                if(transformedPosi < 0) {
                    transformedPosi = 0;
                }
                if (transformedPosj < 0) {
                    transformedPosj = 0;
                }
                if (transformedPosi >= imageData.length-1) {
                    transformedPosi = imageData.length -1;
                }
                if (transformedPosj >= imageData[0].length-1) {
                    transformedPosj = imageData[0].length-1;
                }
                this.mask[i][j] = valid;
                transformedImage[i][j] = interp.interpolate(imageData, transformedPosi, transformedPosj);
               
            }
        }
        return transformedImage;
    }
    
    private double[][] extractArea(double[][] data, int centerX, int centerY, int width) 
    {
        int startX, endX;
        int startY, endY;

        startX = Math.max(0, centerX - width);
        endX = Math.min(data.length - 1, centerX + width);
        startY = Math.max(0, centerY - width);
        endY = Math.min(data[0].length - 1, centerY + width);

        double[][] extractedArea = new double[(endX - startX) + 1][(endY - startY) + 1];
        for (int i = startX, iPos = 0; i <= endX; i++, iPos++) {
            for (int j = startY, jPos = 0; j <= endY; j++, jPos++) {
                extractedArea[iPos][jPos] = data[i][j];
            }
        }
        return extractedArea;
    }

}

