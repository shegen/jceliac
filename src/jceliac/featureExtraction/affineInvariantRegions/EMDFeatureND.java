/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jceliac.featureExtraction.affineInvariantRegions;

import jceliac.tools.emd.EMDFeature;

/**
 *
 * @author sebi
 */
public class EMDFeatureND implements EMDFeature
{
    protected double [] data;

    public EMDFeatureND(double[] data) {
        this.data = data;
    }

    @Override
    public double groundDist(EMDFeature f) 
    {
        EMDFeatureND o = (EMDFeatureND) f;
        double sum = 0;
        for(int i = 0; i < data.length; i++) {
            sum += (this.data[i] - o.data[i]) * (this.data[i] - o.data[i]);
        }
        return sum;
    }
    
    
}
