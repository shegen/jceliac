/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jceliac.featureExtraction.affineInvariantRegions;

import jceliac.featureExtraction.FeatureExtractionParameters;

/**
 *
 * @author sebi
 */
public class FeatureExtractionParametersAffineInvariantRegions 
    extends FeatureExtractionParameters
{

    @Override
    public String getMethodName() 
    {
        return "Affine Invariant Regions";
    }

    
    
}
