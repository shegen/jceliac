/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jceliac.featureExtraction.affineInvariantRegions;

import java.util.ArrayList;

/**
 *
 * @author sebi
 */
public class AgglomerativeClustering 
{
    
    private ArrayList <double[][]> spinImages;

    public AgglomerativeClustering(ArrayList<double[][]> spinImages) {
        this.spinImages = spinImages;
    }
    
    private double [][] computeProximityMatrix(ArrayList <Cluster> clusters)
    {
        double [][] proximityMatrix = new double[clusters.size()][clusters.size()];
        for(int i = 0; i < clusters.size(); i++) {
            for(int j = 0; j < clusters.size(); j++) {
                proximityMatrix[i][j] = clusters.get(i).distanceTo(clusters.get(j));                                                     
            }
        }
        return proximityMatrix;
    }
    
    private double computeSumOfSquaredDiffernces(double [][] spinImage1, double [][] spinImage2)
    {
        double ssd = 0;
         for(int i = 0; i < this.spinImages.size(); i++) {
            for(int j = 0; j < this.spinImages.size(); j++) {
                if(i == j) {
                    ssd = Double.POSITIVE_INFINITY;
                }else {
                    ssd += (spinImage1[i][j] - spinImage2[i][j]) * (spinImage1[i][j] - spinImage2[i][j]);
                }
            }
        }
        return ssd;
    }
    
    private int[] findMostSimilarPair(double [][] proximityMatrix) {
        double minValue = Double.POSITIVE_INFINITY;
        
        int minI = -1;
        int minJ = -1;
        for(int i = 0; i < this.spinImages.size(); i++) {
            for(int j = 0; j < this.spinImages.size(); j++) {
                if(proximityMatrix[i][j] < minValue) {
                    minValue = proximityMatrix[i][j];
                    minI = i;
                    minJ = j;
                }
            }
        }
        return new int[] {minI,minJ};
    }
    
    
    public void cluster()
    {
        int m = 0;
        // initialize clusters
        ArrayList <Cluster> allClusters = new ArrayList <>();
        for(double [][] spinImage : this.spinImages) {
            Cluster c = new Cluster(spinImage);
            allClusters.add(c);
        }
        // compute proximity Matrix
        double [][] proximityMatrix = this.computeProximityMatrix(allClusters);
        int [] simIndices = this.findMostSimilarPair(proximityMatrix);
        
    }
    
    private class Cluster 
    {
        private ArrayList <double[][]> spinImages = new ArrayList<>();

        public Cluster(double [][] initialSpinImage) {
            this.spinImages.add(initialSpinImage);
        }
        public void addToCluster(double [][] spinImage)
        {
            this.spinImages.add(spinImage);
        }
         
        public double distanceTo(Cluster o) 
        {
            // d[(k), (r,s)] = min d[(k),(r)], d[(k),(s)].
            // i.e. minimum of all pairwise distances between this and o
            double min = Double.POSITIVE_INFINITY;
            for(int i = 0; i < this.spinImages.size(); i++) {
                for(int j = 0; j < o.spinImages.size(); j++) {
                    double ssd = AgglomerativeClustering.this.computeSumOfSquaredDiffernces(this.spinImages.get(i),
                                                                                            o.spinImages.get(j));
                    if(ssd < min) {
                        min = ssd;
                    }
                }
            }
            return min;
        }
        
        
    }
}
