/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jceliac.featureExtraction.affineInvariantRegions;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.FixedFeatureVector;
import jceliac.featureExtraction.StandardEuclideanFeatures;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;
import jceliac.tools.emd.EMDFeature;
import jceliac.tools.emd.JFastEMD;
import jceliac.tools.emd.Signature;

/**
 *
 * @author sebi
 */
public class AffineRegionDescriptor 
    extends DiscriminativeFeatures
{
    private final EMDFeatureND [] medoids;
    private final double [] weights;
    
    public AffineRegionDescriptor(FeatureExtractionParameters parameters,
                                  EMDFeatureND [] medoids,
                                  double [] weights) 
    {
        super(parameters);
        this.medoids = medoids;
        this.weights = weights;
    }

    @Override
    public DiscriminativeFeatures cloneFeatureSubset(FeatureVectorSubsetEncoding subset)
            throws JCeliacGenericException
    {
        AffineRegionDescriptor featureSubset = new AffineRegionDescriptor(super.featureExtractionParameters,
                                                                          this.medoids,
                                                                          this.weights);
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
        
        return featureSubset;
    }

    @Override
    public DiscriminativeFeatures cloneEmptyFeature()
    {
         throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double doDistanceTo(DiscriminativeFeatures o) 
            throws JCeliacGenericException 
    {
        AffineRegionDescriptor them = (AffineRegionDescriptor) o;
        // compute EMD between two features
        Signature signature1 = new Signature();
        signature1.setNumberOfFeatures(this.medoids.length);
        signature1.setFeatures(this.medoids);
        signature1.setWeights(this.weights);
        
        Signature signature2 = new Signature();
        signature2.setNumberOfFeatures(them.medoids.length);
        signature2.setFeatures(them.medoids);
        signature2.setWeights(them.weights);
        
        double dist = JFastEMD.distance(signature1, signature2, -1);
        return dist;
   }
    

    @Override
    public ArrayList<double[]> getLocalFeatureVectorData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
