/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jceliac.featureExtraction.affineInvariantRegions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.StandardEuclideanFeatures;
import jceliac.gui.ImageViewerFrame;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.cluster.KMeansClusterCenter;
import jceliac.tools.cluster.KMeansClustering;
import jceliac.tools.cluster.KMeansDataPoint;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.DataSource;
import jceliac.tools.data.Frame;
import jceliac.tools.filter.scalespace.LaplacianScalespaceRepresentation;
import jceliac.tools.filter.scalespace.ScalespaceParameters;
import jceliac.tools.filter.scalespace.ScalespacePoint;
import jceliac.tools.filter.scalespace.ScalespaceRepresentation;
import jceliac.tools.filter.scalespace.ScalespaceStructureTensor;
import jceliac.tools.filter.scalespace.SecondMomentMatrix;

/**
 * A sparse texture representation using affine-invariant regions. 
 * 
 * 
 * @author sebi
 */
public class FeatureExtractionMethodAffineInvariantRegions 
    extends FeatureExtractionMethod
{

    
    protected ScalespaceParameters scalespaceParameters;
    
    public FeatureExtractionMethodAffineInvariantRegions(FeatureExtractionParameters param) 
    {
        super(param);
        this.scalespaceParameters = new ScalespaceParameters(Math.sqrt(2.0d),
                                                             -4,
                                                             4,
                                                             0.5 ,
                                                             2.1214);
        
        
    }

    
    
    
    @Override
    public DiscriminativeFeatures extractFeatures(Frame content) 
            throws JCeliacGenericException 
    {
        ScalespaceRepresentation lapScaleSpace = 
                new LaplacianScalespaceRepresentation(content.getGrayData(), this.scalespaceParameters);
        lapScaleSpace.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_ABSOLUTE_SUM);
        lapScaleSpace.computeMaxima();
        
        ScalespaceStructureTensor tensors = new ScalespaceStructureTensor(lapScaleSpace);
        tensors.computeStructureTensorsAtAllScales();
        ArrayList <ScalespacePoint> interestPoints = lapScaleSpace.getNLargestMaxima(800);
        tensors.computeStructureTensorsForInterestpoints(interestPoints);
        
        ArrayList <double []> spinImages1D = new ArrayList <>();
        for(ScalespacePoint ip : interestPoints) {
            try {
                AffineAdaption adaption = new AffineAdaption(content.getGrayData(), ip);
                double [][] normalizedRegion = adaption.getAffineNormalizedData();
                double [][] spinImage = this.computeSpinImage(normalizedRegion, adaption.getMask());

                spinImages1D.add(ArrayTools.reshape(spinImage));
            }catch(JCeliacGenericException e) {
                // ignore, this will be due to complex matrices in the invert
            }
        } 
        KMeansClustering kmeans = null;
        kmeans = new KMeansClustering(spinImages1D, Math.min(15,spinImages1D.size()), 100);
        kmeans.cluster();
       
        
        // build decriptor as the {(medoid, ui)} medoid = the most centrally located element in the i-th cluster
        // ui = size of the cluster divided by the total number of descriptors in the image. 
        KMeansClusterCenter[] centers = kmeans.getClusterCenters();
        // in case of kmeans the center itself is the medoid
        EMDFeatureND[] medoids = new EMDFeatureND[centers.length];
        double [] weights = new double[centers.length];
        for(int i = 0; i < centers.length; i++) {
            medoids[i] = new EMDFeatureND(centers[i].dataVector);
            weights[i] = centers[i].numAssignedPoints / (double)spinImages1D.size();
        }
        DiscriminativeFeatures features = new AffineRegionDescriptor(this.parameters,
                                                                     medoids,
                                                                     weights);
        features.setSignalIdentifier(content.getSignalIdentifier());
        return features;
        
    }
    
      // normalize features linearly between 0 and 1
    protected void normalizeLinear(double [][] data)
    {
        double min, max;

        max = ArrayTools.max(data);
        min = ArrayTools.min(data);
        double width = (max - min);
        if(width == 0) {
            width = 1;
        }
        double scaleFactor = (1.0d / width); 

        for(int i = 0; i < data.length; i++) {
            for(int j = 0; j < data[0].length; j++) {
                double k = data[i][j];
                double normalized = (k - min) * scaleFactor;
                data[i][j] = normalized;
            }
        }
    }
    
    private double [][] computeSpinImage(double [][] region, boolean [][] mask)
    {
        // normalize range of intensity function of region between [0;1]
        this.normalizeLinear(region);
            
        double DIM = 10;
        double [][] spinImage = new double[(int)DIM][(int)DIM]; // size 20 used in CVPR; paper 10 in PAMI
        double maxRadius = region.length;
        double center = region.length / 2.0d;
        for(int x = 0; x < region.length; x++) {
            for(int y = 0; y < region[0].length; y++) {
                if(mask[x][y] == false) {
                    continue;
                }
                double distance = Math.sqrt( (center - x) * (center - x) + (center - y) * (center -y));
                double normD = distance / maxRadius;
                double intensity = region[x][y];
                
                if(normD > 1) {
                    continue;
                }
                
               // soft contribution usually, but do not know values for alpha and beta
                double alpha = 1 / Math.sqrt(2);
                double beta = 1 / Math.sqrt(2);
                for(double i = 0; i < DIM; i++) {
                    for(double d = 0; d < DIM; d++) {
                   
                        double exp = -((Math.abs(normD - (d/DIM)) * (normD - (d/DIM)) / (2*alpha*alpha)) ) -
                                      ((intensity-(i/DIM)) * (intensity-(i/DIM))) / (2*beta*beta);
                        double contribution = Math.exp(exp);
                 //       spinImage[(int)i][(int)d] += contribution;
                    }
                }
                int sX = (int)Math.round(normD*DIM);
                int sY = (int)Math.round(intensity*DIM);
                
                if(sX >= (int)DIM) sX = (int)DIM-1;
                if(sY >= (int)DIM) sY = (int)DIM-1;
                        
                spinImage[sX][sY] += 1.0d; //contribution;
            }
        }
        // normalize spin image to zero mean and unit frobenius norm
        this.normalizeZeroMeanUnitFrobenius(spinImage);
          
        return spinImage;
    }
    
    
    private void normalizeZeroMeanUnitFrobenius(double [][] spinImage) {
        // compute mean
        double mean = ArrayTools.mean(spinImage);
        for(int i = 0; i < spinImage.length; i++) {
            for(int j = 0; j < spinImage[0].length; j++) {
                spinImage[i][j] = (spinImage[i][j] - mean);
            }
        }
        // we stopped normalizing by the frobenius norm because the results were all worse
        
        // compute frobenius
        /*double fNorm = 0;
        for(int i = 0; i < spinImage.length; i++) {
            for(int j = 0; j < spinImage[0].length; j++) {
                fNorm += spinImage[i][j] * spinImage[i][j];
            }
        }
        fNorm = Math.sqrt(fNorm);
        for(int i = 0; i < spinImage.length; i++) {
            for(int j = 0; j < spinImage[0].length; j++) {
                spinImage[i][j] = (spinImage[i][j] / fNorm);
            }
        }*/
    }
    

    @Override
    public void initialize() 
            throws JCeliacGenericException 
    {
        
    }

    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass) 
            throws JCeliacGenericException 
    {
      
    }

    @Override
    public void estimateParametersIterative(Frame nextFrame) {
       
    }
    
}
