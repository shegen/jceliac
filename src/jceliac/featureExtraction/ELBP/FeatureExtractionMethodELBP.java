/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.ELBP;

import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.LBP.*;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.*;
import jceliac.tools.filter.*;
import jceliac.experiment.*;
import jceliac.tools.data.*;
import jceliac.logging.*;

/**
 * This is used to extract the features from a single input.
 * <p>
 * @author shegen
 */
@StableCode(date = "16.01.2014", lastModified = "16.01.2014")
public class FeatureExtractionMethodELBP
        extends FeatureExtractionMethodLBP
{

    private final FeatureExtractionParametersELBP myParameters;

    public FeatureExtractionMethodELBP(FeatureExtractionParameters param)
    {
        super(param);

        // this is just for convenience
        this.myParameters = (FeatureExtractionParametersELBP) super.parameters;

    }

    /* Extract features using the parameters given to the
     * FeatureExtractionMethod.
     */
    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {

        Experiment.log(JCeliacLogger.LOG_HINFO, "Extracting LBP-Features from SignalID: %s, FrameID: %d.",
                       content.getSignalIdentifier(), content.getFrameIdentifier());

        // create DiscriminativeFeatures structure once for each input sample
        LBPFeatures features = new LBPFeatures(this.myParameters);
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());

        for(int currentScale = myParameters.getMinScale(); currentScale <= myParameters.getMaxScale(); currentScale++) {

            if(myParameters.isUseColor()) {
                // Prepare content for scale.

                if(myParameters.isSobelTypeSubbands()) {
                    for(Integer colorChannel : this.myParameters.getColorChannels()) {
                        LBPHistogram histVert = this.computeDistribution(content.getColorChannel(colorChannel), currentScale, SobelFilter.ORIENTATION_VERTICAL);
                        histVert.setColorChannel(colorChannel);
                        histVert.setScale(currentScale);
                        features.addAbstractFeature(histVert);

                        LBPHistogram histHoriz = this.computeDistribution(content.getColorChannel(colorChannel), currentScale, SobelFilter.ORIENTATION_HORIZONTAL);
                        histHoriz.setColorChannel(colorChannel);
                        histHoriz.setScale(currentScale);
                        features.addAbstractFeature(histHoriz);

                        LBPHistogram histDiagonal = this.computeDistribution(content.getColorChannel(colorChannel), currentScale, SobelFilter.ORIENTATION_DIAGONAL);
                        histDiagonal.setColorChannel(colorChannel);
                        histDiagonal.setScale(currentScale);
                        features.addAbstractFeature(histDiagonal);

                    }
                } else // use magnitude 
                {
                    for(Integer colorChannel : this.myParameters.getColorChannels()) {
                        LBPHistogram histMag = this.computeDistribution(content.getColorChannel(colorChannel),
                                                                        currentScale, SobelFilter.ORIENTATION_MAGNITUDE);
                        histMag.setColorChannel(colorChannel);
                        histMag.setScale(currentScale);
                        features.addAbstractFeature(histMag);
                    }
                }

            } else { // grayscale

                if(myParameters.isSobelTypeSubbands()) {
                    LBPHistogram histGrayVert = this.computeDistribution(content.getGrayData(),
                                                                         currentScale, SobelFilter.ORIENTATION_VERTICAL);
                    LBPHistogram histGrayHoriz = this.computeDistribution(content.getGrayData(), currentScale, SobelFilter.ORIENTATION_HORIZONTAL);
                    LBPHistogram histGrayDiagonal = this.computeDistribution(content.getGrayData(), currentScale, SobelFilter.ORIENTATION_DIAGONAL);

                    histGrayVert.setColorChannel(DataSource.CHANNEL_GRAY);
                    histGrayVert.setScale(currentScale);
                    features.addAbstractFeature(histGrayVert);

                    histGrayHoriz.setColorChannel(DataSource.CHANNEL_GRAY);
                    histGrayHoriz.setScale(currentScale);
                    features.addAbstractFeature(histGrayHoriz);

                    histGrayDiagonal.setColorChannel(DataSource.CHANNEL_GRAY);
                    histGrayDiagonal.setScale(currentScale);
                    features.addAbstractFeature(histGrayDiagonal);
                } else { // magnitude
                    LBPHistogram histGrayMag = this.computeDistribution(content.getGrayData(), currentScale, SobelFilter.ORIENTATION_MAGNITUDE);

                    histGrayMag.setColorChannel(DataSource.CHANNEL_GRAY);
                    histGrayMag.setScale(currentScale);
                    features.addAbstractFeature(histGrayMag);
                }
            }
        }
        return features;
    }

    protected LBPHistogram computeDistribution(double[][] dataChannel, int scale, int filterOrientation)
            throws JCeliacGenericException
    {
        LBPHistogram elbpHistogram;
        int neighbors = LBPMultiscale.getNeighborsForScale(scale);
        double radius = LBPMultiscale.getRadiusForScale(scale);
        int margin = (int) Math.ceil(radius);

        DataFilter2D sobelFilter = new SobelFilter(filterOrientation);
        DataFilter2D multiScaleFilter = LBPMultiscale.getFilterForScale(scale);

        double[][] dataSobeled;
        dataSobeled = sobelFilter.filterData(dataChannel);
        dataSobeled = multiScaleFilter.filterData(dataSobeled);

        elbpHistogram = this.computeHistogram(dataSobeled, neighbors, radius, margin, scale);
        elbpHistogram.setSubband(filterOrientation);

        return elbpHistogram;
    }
}
