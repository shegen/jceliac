/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.ELBP;

import jceliac.experiment.Experiment;
import jceliac.featureExtraction.LBP.FeatureExtractionParametersLBP;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersELBP
        extends FeatureExtractionParametersLBP
{

    /**
     * If set to true, sub-bands are used else the gradient magnitude is used
     */
    protected boolean sobelSubbandMode = false;

    public boolean isSobelTypeSubbands()
    {
        return sobelSubbandMode;
    }

    public void setSobelTypeSubbands(boolean sobelTypeSubbands)
    {
        this.sobelSubbandMode = sobelTypeSubbands;
    }

    public FeatureExtractionParametersELBP()
    {
    }

    @Override
    public void report()
    {
        super.report(); 
        Experiment.log(JCeliacLogger.LOG_INFO, "Subband Type: "+ (this.sobelSubbandMode ? "Sobel" : "Magnitude"));
    }

    
    
    @Override
    public String getMethodName()
    {
        return "ELBP";
    }
}
