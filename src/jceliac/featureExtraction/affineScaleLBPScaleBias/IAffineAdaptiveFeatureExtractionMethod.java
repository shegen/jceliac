/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBPScaleBias;

import jceliac.featureExtraction.SALBP.LBPScaleFilteredDataSupplier;
import jceliac.featureExtraction.affineScaleLBP.EllipticPointLookup;
import jceliac.featureExtraction.LBP.LBPHistogram;
import jceliac.tools.data.Frame;

/**
 * Interface that is implemented by LBP-subMethods for the affine adaptive
 * framework.
 * <p>
 * @author shegen
 */
public interface IAffineAdaptiveFeatureExtractionMethod
{

    default public LBPAffineFilteredDataSupplier prepareAffineAdaptiveDataSupplier(Frame content, EllipticPointLookup lookup)
    {
        LBPScaleFilteredDataSupplier supplier = new LBPScaleFilteredDataSupplier(content.getGrayData());
        return new LBPAffineFilteredDataSupplier(content.getGrayData(), supplier, lookup);
    }

    public void computeAffineAdaptivePattern(LBPHistogram hist, AffineFilteredNeighbor center, AffineFilteredNeighbor[] neighbors, double stdev, double contribution);

    public LBPHistogram initializeHistogram();

    public void finalizeHistogram(LBPHistogram histogram);

}
