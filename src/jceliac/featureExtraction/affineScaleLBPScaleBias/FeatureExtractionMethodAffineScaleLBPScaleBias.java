/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBPScaleBias;

import jceliac.featureExtraction.SALBP.AffineLBPFeatures;
import jceliac.featureExtraction.SALBP.LBPScaleFilteredDataSupplier;
import jceliac.featureExtraction.SALBP.LBPScaleBias;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ScalespaceScaleAndShapeEstimator;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.OrientationEstimation;
import jceliac.featureExtraction.LTP.FeatureExtractionParametersLTP;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ScaleEstimation;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.tools.filter.scalespace.LaplacianScalespaceRepresentation;
import jceliac.tools.filter.scalespace.ScaleLevelIndexedDataSupplier;
import jceliac.featureExtraction.affineScaleLBP.EllipticPointLookup;
import jceliac.tools.filter.scalespace.ScalespaceStructureTensor;
import jceliac.featureExtraction.LBP.FeatureExtractionMethodLBP;
import jceliac.tools.filter.scalespace.ScalespaceRepresentation;
import jceliac.featureExtraction.affineScaleLBP.EllipticPoint;
import jceliac.tools.filter.scalespace.ScalespaceParameters;
import jceliac.tools.filter.scalespace.SecondMomentMatrix;
import jceliac.featureExtraction.affineScaleLBP.Ellipse;
import jceliac.tools.filter.scalespace.ScalespacePoint;
import jceliac.featureExtraction.LBP.LBPHistogram;
import jceliac.featureExtraction.LTP.LTPHistogram;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.JCeliacGenericException;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.Frame;
import java.util.ArrayList;
import java.util.HashMap;
import jceliac.experiment.Experiment;
import jceliac.featureExtraction.ExperimentalCode;
import jceliac.logging.JCeliacLogger;

/**
 * Implementation of the first version using scale bias, probably still some
 * issues but should not be much different from the new implementations in SALBP
 * and SALTB.
 * <p>
 * <p>
 * @author shegen
 */
@ExperimentalCode(
        comment = "Experimental, do not use! Instead use SALBP and SALTP.",
        date = "20.01.2014",
        lastModified = "20.01.2014")
public class FeatureExtractionMethodAffineScaleLBPScaleBias
        extends FeatureExtractionMethodLBP
{

    private final FeatureExtractionParametersLTP myParameters;
    private final EllipticPointLookup ellipseLookupTable;
    private final ScalespaceParameters scalespaceParameters;
    private int imageWidth;
    private int imageHeight;

    private final HashMap<Integer, LBPScaleBias> scaleBiasPerClass = new HashMap<>();
    private HashMap<Integer, ScaleEstimation> scaleEstimationPerClass;


    public FeatureExtractionMethodAffineScaleLBPScaleBias(FeatureExtractionParameters param)
            throws JCeliacGenericException
    {
        super(param);
        this.ellipseLookupTable = EllipticPointLookup.create();
        /* The scalespace is constructed such that the middle scale
         * sqrt(2)^0*c*sqrt(2) is 3 pixels wide. We define the scale bias to be
         * 3 pixels wide.
         */
        this.scalespaceParameters = new ScalespaceParameters(Math.sqrt(2.0d),
                                                             -4,
                                                             8,
                                                             0.25,
                                                             2.1214);
        this.myParameters = (FeatureExtractionParametersLTP) param;
    }

    private void prepareScaleBiases()
    {
        for(int classIndex : this.scaleEstimationPerClass.keySet()) {
            ScaleEstimation classScaleEstimation = this.scaleEstimationPerClass.get(classIndex);
            int scaleBiasScale = classScaleEstimation.getEstimatedScaleLevel();

            if(this.scaleBiasPerClass.containsKey(classIndex) == false) {
                LBPScaleBias scaleBias = new LBPScaleBias(this.scalespaceParameters, scaleBiasScale,
                                                          classIndex, classScaleEstimation);
                this.scaleBiasPerClass.put(classIndex, scaleBias);
            }
        }
    }
    
    @Override
    @SuppressWarnings("UnusedAssignment")
    public DiscriminativeFeatures extractTrainingFeatures(Frame content, int classIndex)
            throws JCeliacGenericException
    {
 
        LBPScaleFilteredDataSupplier filteredDataSupplier
                                     = new LBPScaleFilteredDataSupplier(content.getGrayData());

        // now estimate the scales of the input data
        ScalespaceRepresentation scaleRep = new LaplacianScalespaceRepresentation(content.getGrayData(), this.scalespaceParameters);
        scaleRep.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_SUM);
        ScaleEstimation localScaleEstimate = new ScaleEstimation(this.scalespaceParameters,
                                                                 scaleRep.getNormalizedLaplacianDataSupplier(),
                                                                 content.getWidth(), content.getHeight());
        localScaleEstimate.estimateGlobalScale();
        int dominantScale = localScaleEstimate.getEstimatedScaleLevel();
        
        ScalespaceStructureTensor structureTensor = new ScalespaceStructureTensor(scaleRep);
        ScaleLevelIndexedDataSupplier<ArrayList<ScalespacePoint>> ip1D = scaleRep.computeMaxima1D();
        scaleRep.computeMaxima();

        structureTensor.computeStructureTensors(ip1D);
        ScaleEstimation classScaleEstimation = this.scaleEstimationPerClass.get(classIndex);
        int scaleBiasScale = classScaleEstimation.getEstimatedScaleLevel();
        
        OrientationEstimation localOrientationEstimation = new OrientationEstimation();
        localOrientationEstimation.estimateGlobalOrientation(scaleRep, localScaleEstimate.getEstimatedScaleLevel());

        LBPScaleBias scaleBias = this.scaleBiasPerClass.get(classIndex);

        double[] measureMid = new double[this.scalespaceParameters.getNumberOfScales()];
        double midScaleRadius = 3.0d;
        double lowerScaleRadius = 1.5d;
        double upperScaleRadius = 4.5d;

        measureMid[dominantScale] = 1;
        this.imageHeight = content.getHeight();
        this.imageWidth = content.getWidth();

    //    AffineScaleLBPBiasedFeatures features = new AffineScaleLBPBiasedFeatures(this.scaleBiasPerClass);
        LBPScaleBias localBias = new LBPScaleBias(this.scalespaceParameters, scaleBias);

        AffineLBPFeatures tmp = new AffineLBPFeatures(super.parameters, localScaleEstimate, localBias);
        tmp.setSignalIdentifier(content.getSignalIdentifier());
        tmp.setFrameNumber(content.getFrameIdentifier());

        localBias.setLBP_BASE_RADIUS(lowerScaleRadius);
        LBPHistogram histogramLower = this.performFeatureExtractionLTP(filteredDataSupplier,
                                                                               localBias,
                                                                               localScaleEstimate,
                                                                               structureTensor,
                                                                               localOrientationEstimation,
                                                                               true);

        localBias.setLBP_BASE_RADIUS(midScaleRadius);
        LBPHistogram histogramMid = this.performFeatureExtractionLTP(filteredDataSupplier,
                                                                             localBias,
                                                                             localScaleEstimate,
                                                                             structureTensor,
                                                                             localOrientationEstimation,
                                                                             true);

        localBias.setLBP_BASE_RADIUS(upperScaleRadius);
        LBPHistogram histogramUpper = this.performFeatureExtractionLTP(filteredDataSupplier,
                                                                               localBias,
                                                                               localScaleEstimate,
                                                                               structureTensor,
                                                                               localOrientationEstimation,
                                                                               true);

        if(histogramLower.isEmpty()
           || histogramMid.isEmpty()
           || histogramUpper.isEmpty()) {
            Experiment.log(JCeliacLogger.LOG_WARN, "Empty histogram!");
        }

        tmp.addAbstractFeature(histogramLower);
        tmp.addAbstractFeature(histogramMid);
        tmp.addAbstractFeature(histogramUpper);

//        features.addFeature(tmp);
 //       features.setSignalIdentifier(tmp.getSignalIdentifier());
 //       features.setFrameNumber(1);

   //     return features;
        return null;
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {
        LBPScaleFilteredDataSupplier filteredDataSupplier
                                     = new LBPScaleFilteredDataSupplier(content.getGrayData());

        ScalespaceRepresentation scaleRep = new LaplacianScalespaceRepresentation(content.getGrayData(), this.scalespaceParameters);
        scaleRep.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_SUM);
        ScaleEstimation localScaleEstimate = new ScaleEstimation(this.scalespaceParameters,
                                                                 scaleRep.getNormalizedLaplacianDataSupplier(),
                                                                 content.getWidth(), content.getHeight());
        localScaleEstimate.estimateGlobalScale();
        ScalespaceStructureTensor structureTensor = new ScalespaceStructureTensor(scaleRep);
        ScaleLevelIndexedDataSupplier<ArrayList<ScalespacePoint>> ip1D = scaleRep.computeMaxima1D();
        scaleRep.computeMaxima();

        structureTensor.computeStructureTensors(ip1D);
        
        OrientationEstimation localOrientationEstimation  = new OrientationEstimation();
        localOrientationEstimation.estimateGlobalOrientation(scaleRep,localScaleEstimate.getEstimatedScaleLevel());

        ArrayList<LBPScaleBias> scaleBiases = new ArrayList<>();
        for(int i = 0; i < this.scaleBiasPerClass.size(); i++) {
            LBPScaleBias tmp = this.scaleBiasPerClass.get(i);
            if(tmp != null) {
                scaleBiases.add(tmp);
            }
        }

        this.imageHeight = content.getHeight();
        this.imageWidth = content.getWidth();

      //  AffineScaleLBPBiasedFeatures features = new AffineScaleLBPBiasedFeatures(this.scaleBiasPerClass);
//       features.setSignalIdentifier(content.getSignalIdentifier());
 //       features.setFrameNumber(content.getFrameIdentifier());

        double midScaleRadius = 3.0d;
        double lowerScaleRadius = 1.5d;
        double upperScaleRadius = 4.5d;

        for(LBPScaleBias curScaleBias : scaleBiases) {

                AffineLBPFeatures tmp = new AffineLBPFeatures(super.parameters, localScaleEstimate, curScaleBias);
                tmp.setSignalIdentifier(content.getSignalIdentifier());
                tmp.setFrameNumber(content.getFrameIdentifier());

                LBPScaleBias localBias = new LBPScaleBias(this.scalespaceParameters, curScaleBias);
                localBias.setLBP_BASE_RADIUS(lowerScaleRadius);

                LBPHistogram histogramLower = this.performFeatureExtractionLTP(filteredDataSupplier,
                                                                                       localBias,
                                                                                       localScaleEstimate,
                                                                                       structureTensor,
                                                                                       localOrientationEstimation,
                                                                                       false);

                localBias.setLBP_BASE_RADIUS(midScaleRadius);
                LBPHistogram histogramMid = this.performFeatureExtractionLTP(filteredDataSupplier,
                                                                                     localBias,
                                                                                     localScaleEstimate,
                                                                                     structureTensor,
                                                                                     localOrientationEstimation,
                                                                                     false);

                localBias.setLBP_BASE_RADIUS(upperScaleRadius);
                LBPHistogram histogramUpper = this.performFeatureExtractionLTP(filteredDataSupplier,
                                                                                       localBias,
                                                                                       localScaleEstimate,
                                                                                       structureTensor,
                                                                                       localOrientationEstimation,
                                                                                       false);

                if(histogramLower.isEmpty()
                   || histogramMid.isEmpty()
                   || histogramUpper.isEmpty()) {
                    Experiment.log(JCeliacLogger.LOG_WARN, "Empty histogram!");
                }

                tmp.addAbstractFeature(histogramLower);
                tmp.addAbstractFeature(histogramMid);
                tmp.addAbstractFeature(histogramUpper);
        
      /*          features.addFeature(tmp);
                features.setSignalIdentifier(tmp.getSignalIdentifier());
                features.setFrameNumber(1);

            }
       return features;*/
    }
                return null;
    }

    private LTPHistogram performFeatureExtractionLTP(LBPScaleFilteredDataSupplier filteredDataSupplier,
                                                     LBPScaleBias scaleBias,
                                                     ScaleEstimation localScaleEstimation,
                                                     ScalespaceStructureTensor structureTensor,
                                                     OrientationEstimation localOrientationEstimation,
                                                     boolean train)
            throws JCeliacGenericException
    {
        LTPHistogram histogram = new LTPHistogram();
        double[] histLTPUpper = new double[getNumberOfBins(8)];
        double[] histLTPLower = new double[getNumberOfBins(8)];

        // create elliptic pionts once and recycle to speedup 
        EllipticPoint[] points = new EllipticPoint[8];
        for(int i = 0; i < points.length; i++) {
            points[i] = new EllipticPoint();
        }

        for(int scaleLevel = 0;
            scaleLevel < this.scalespaceParameters.getNumberOfScales();
            scaleLevel++) {

            double confidence = localScaleEstimation.getConfidenceForScale(scaleLevel);
            if(confidence < 0.9) {
                continue;
            }
            double lbpRadius = scaleBias.getLBPRadiusForScaleLevel(scaleLevel);

            // this will deliver the correctly filtered data for the lbp radius
            double[][] data = filteredDataSupplier.retrieveLBPScaleFilteredDataForLBPRadius(lbpRadius);
            double stdev = filteredDataSupplier.getStdevForFilteredDataForLBPRadius(lbpRadius);
            for(int x = 2; x < this.imageWidth - 2; x++) {
                for(int y = 2; y < this.imageHeight - 2; y++) {

                    //   SecondMomentMatrix smm = structureTensor.getStructureTensor(x, y, Math.round(scaleLevel/2));
                    //   Ellipse ellipse = new Ellipse(smm, lbpRadius, Ellipse.CIRCUMFERENCE_NORMALIZATION_MODE);
                    //   ellipse.increaseIsotropy(lbpRadius);
                    //   shapeBias.modifyShape(ellipse, lbpRadius);
                    Ellipse ellipse = new Ellipse();

                    //if (ellipse.getIsotropyMeasure() > 0.9) {
                    //ellipse.a = lbpRadius;
                    //ellipse.b = lbpRadius+0.3;
                    //                          continue;
                    //                    }
                    //   ellipse.phi = 0;
                    // ellipse.phi = localOrientationEstimation.getDominantOrientation();
                    //  if(ellipse.phi < 0) {
                    //      int blub = 0;
                    //    localOrientationEstimation.estimateRotation();
                    //  }
                    //    if(train) {
                    //        ellipse.phi = Math.abs(Math.random() % 0.3);
                    //    }else {
                    //        ellipse.phi  = 0;
                    //   }
                    //   ellipse.a = lbpRadius; //*0.5;
                    //    ellipse.b = lbpRadius*1.2; //* 2;
                    // ellipse.phi = 0d;
                    ellipse.a = lbpRadius;
                    ellipse.b = lbpRadius;
                    ellipse.phi = localOrientationEstimation.getDominantOrientation();
                    try {
                          //  ArrayList <Double> orientations = localOrientationEstimation.getOrientations(lbpRadius);
                        //double alpha = Math.atan(
                        //         LBPScaleFilteredDataSupplier.computeSampleRadiusForLBPRadius(lbpRadius) /
                        //        lbpRadius);

                        //   for(int i = 0; i < orientations.size(); i++) 
                        //   {
                        // compute features
                        this.ellipseLookupTable.lookup(points, x, y, ellipse.a, ellipse.b, ellipse.phi);

                        //      EllipticPoint [] pointsRot = this.ellipseLookupTable.lookupOld(64, 32, 15, 3, 1d);
                        //      EllipticPoint [] pointsNoRot = this.ellipseLookupTable.lookupOld(64, 32, 15, 3, 0);
                        //      EllipticPoint [] manualRot = EllipticPoint.rotatePoints(pointsNoRot, 64, 32, 1d);
                        int[][] ltpPatterns = this.extractLTP(data, x, y, stdev, points);

                        // weight ehe patterns such that each interestpoint adds the same amount to the histogram 
                        // no matter of the the size of the sigma of the interestpoint
                        histLTPLower[ltpPatterns[0][0]] = histLTPLower[ltpPatterns[0][0]] + confidence;//localScaleEstimation.getConfidenceForScale(scaleLevel);
                        histLTPUpper[ltpPatterns[0][1]] = histLTPUpper[ltpPatterns[0][1]] + confidence;//localScaleEstimation.getConfidenceForScale(scaleLevel);

                        if(ltpPatterns[0][0] != ltpPatterns[1][0] && ltpPatterns[0][1] != ltpPatterns[1][1]) {
                            histLTPLower[ltpPatterns[1][0]] = histLTPLower[ltpPatterns[1][0]] + confidence;//localScaleEstimation.getConfidenceForScale(scaleLevel);
                            histLTPUpper[ltpPatterns[1][1]] = histLTPUpper[ltpPatterns[1][1]] + confidence;//localScaleEstimation.getConfidenceForScale(scaleLevel);
                        }

                        //   }
                    } catch(JCeliacGenericException e) {
                    }

                }
            }
        }
        histLTPLower[0] = 0;
        histLTPUpper[0] = 0;
        histogram.setDataLower(histLTPLower);
        histogram.setDataUpper(histLTPUpper);

        return histogram;
    }

    protected int[][] extractLTP(double[][] filteredData, int x, int y, double stdev, EllipticPoint[] points)
            throws JCeliacGenericException
    {
        int shiftMax = points.length - 1;
        int patternUpper = 0;
        int patternLower = 0;

        // find max distance
        double maxDistance = 0;
        for(EllipticPoint point : points) {
            if(point.distanceToCenter > maxDistance) {
                maxDistance = point.distanceToCenter;
            }
        }
        double threshold = Math.sqrt(stdev);
        double center = filteredData[x][y];
        for(int p = 0; p < points.length; p++) {
            double neighbor = 0;
            double adaptedThreshold = threshold * (points[p].distanceToCenter / maxDistance);

            if(points[p].x < filteredData.length - 1 && points[p].y < filteredData[0].length - 1
               && points[p].x >= 0 && points[p].y >= 0) {
                neighbor = this.getInterpolatedPixelValueFast(filteredData, points[p].x, points[p].y);
            } else {
                throw new JCeliacGenericException();
            }
            if(neighbor >= (center + adaptedThreshold)) {
                // this is in reverse order from the matlab thingy to speed up things
                patternUpper |= (1 << (shiftMax - p));
            } else if(neighbor <= (center - adaptedThreshold)) {
                // this is in reverse order from the matlab thingy to speed up things
                patternLower |= (1 << (shiftMax - p));
            } else {
                // zero
            }
        }
        int alternatePatternLower = (patternLower & 0xF0) >> 4 | (patternLower & 0xF) << 4;
        int alternatePatternUpper = (patternUpper & 0xF0) >> 4 | (patternUpper & 0xF) << 4;
        int[][] ret = {{patternLower, patternUpper}, {alternatePatternLower, alternatePatternUpper}};
        return ret;
    }

    private LBPHistogram performFeatureExtractionFuzzyLBP(LBPScaleFilteredDataSupplier filteredDataSupplier,
                                                          LBPScaleBias scaleBias,
                                                          LBPShapeBias shapeBias,
                                                          ScaleEstimation localScaleEstimation,
                                                          ScalespaceStructureTensor structureTensor,
                                                          ScalespaceStructureTensor testTensor)
            throws Exception
    {
        LBPHistogram histogram = new LBPHistogram();
        double[] histFuzzy = new double[getNumberOfBins(8)];

        // create elliptic pionts once and recycle to speedup 
        EllipticPoint[] points = new EllipticPoint[8];
        for(int i = 0; i < points.length; i++) {
            points[i] = new EllipticPoint();
        }

        for(int scaleLevel = 0;
            scaleLevel < this.scalespaceParameters.getNumberOfScales();
            scaleLevel++) {

            double confidence = localScaleEstimation.getConfidenceForScale(scaleLevel);
            if(confidence < 0.1) {
                continue;
            }
            double lbpRadius = scaleBias.getLBPRadiusForScaleLevel(scaleLevel);
            // this will deliver the correctly filtered data for the lbp radius
            double[][] data = filteredDataSupplier.retrieveLBPScaleFilteredDataForLBPRadius(lbpRadius);
            double stdev = filteredDataSupplier.getStdevForFilteredDataForLBPRadius(lbpRadius);

            for(int x = 2; x < this.imageWidth - 2; x++) {
                for(int y = 2; y < this.imageHeight - 2; y++) {

                    SecondMomentMatrix smm = structureTensor.getStructureTensor(x, y, scaleLevel);
                    Ellipse ellipse = new Ellipse(smm, lbpRadius, Ellipse.CIRCUMFERENCE_NORMALIZATION_MODE);
                    shapeBias.modifyShape(ellipse, lbpRadius);

                    ellipse.phi = 0;
                    if(ellipse.getIsotropyMeasure() > 0.9) {
                        continue;
                    }
                    try {
                        // compute features
                        this.ellipseLookupTable.lookup(points, x, y, ellipse.a, ellipse.b, ellipse.phi);
                        this.extractFuzzyLBP(histFuzzy, data, x, y, stdev, points, confidence);
                    } catch(Exception e) {
                        int blub = 0;
                    }

                }
            }
        }
        histogram.setData(histFuzzy);
        return histogram;
    }

    protected void extractFuzzyLBP(double[] hist, double[][] filteredData,
                                   int x, int y, double stdev,
                                   EllipticPoint[] points, double confidence)
            throws Exception
    {
        double[] contributions = new double[256];
        double[] neighborsArray = new double[8];

        // find max distance
        double maxDistance = 0;
        for(EllipticPoint point : points) {
            if(point.distanceToCenter > maxDistance) {
                maxDistance = point.distanceToCenter;
            }
        }
        double threshold = Math.sqrt(stdev);
        double[] adaptedThresholds = new double[8];
        for(int p = 0; p < points.length; p++) {
            double neighbor = this.getInterpolatedPixelValueFast(filteredData, points[p].x, points[p].y);
            neighborsArray[p] = neighbor;
            adaptedThresholds[p] = threshold * (points[p].distanceToCenter / maxDistance);
        }

        double center = filteredData[x][y];
        for(int lbpCode = 0; lbpCode < contributions.length; lbpCode++) {
            contributions[lbpCode] = calculateContributionOfPattern(lbpCode, neighborsArray, center, adaptedThresholds);
            hist[lbpCode] += contributions[lbpCode];
        }
    }

    protected double getMaxContribution(double[] contributions)
    {
        double max = 0;
        for(int index = 0; index < contributions.length; index++) {
            if(contributions[index] > max) {
                max = contributions[index];
            }
        }
        return max;
    }
    private final double THRESHOLD = 10.0d;

    protected double calculateContributionOfPattern(int lbp, double[] neighbors,
                                                    double center, double[] thresholds)
    {
        double contribution = 1.0d;
        for(int p = 0; p < neighbors.length; p++) {
            double m;
            if((lbp & (1 << p)) > 0) {
                m = calculateOneMembership(center, neighbors[p], thresholds[p]);
            } else {
                m = calculateZeroMembership(center, neighbors[p], thresholds[p]);
            }
            if(m == 0) {
                return 0;
            }
            contribution *= m;
        }
        return contribution;
    }

    protected double calculateZeroMembership(double center, double neighbor, double threshold)
    {
        if(neighbor >= (center + threshold)) // neighbor >= center -> pattern 1 -> membership is 0
        {
            return 0.0d;
        }
        if(neighbor <= (center - threshold)) // neighber <= center -> pattern 0 -> membership is 1
        {
            return 1.0d;
        }

        return linear((threshold - neighbor) + center, threshold);
    }

    protected double calculateOneMembership(double center, double neighbor, double threshold)
    {
        return (1.0d - calculateZeroMembership(center, neighbor, threshold));
    }

    protected double linear(double x, double threshold)
    {
        return x / (2 * threshold);
    }

    @Override
    public void initialize()
    {

    }

    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass)
            throws JCeliacGenericException
    {

        // estimate scales for training textures
        ScalespaceScaleAndShapeEstimator scaleAndShapeEstimator = new ScalespaceScaleAndShapeEstimator(this.scalespaceParameters);
        scaleAndShapeEstimator.estimateScalesByClass(readersByClass);
       // this.scaleEstimationPerClass = scaleAndShapeEstimator.reduceScaleEstimations(false);
        this.prepareScaleBiases();

    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {
        // nothing to do
    }

}
