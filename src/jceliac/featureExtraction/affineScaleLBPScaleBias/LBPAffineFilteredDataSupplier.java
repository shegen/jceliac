/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jceliac.featureExtraction.affineScaleLBPScaleBias;

import jceliac.featureExtraction.SALBP.LBPScaleFilteredDataSupplier;
import jceliac.featureExtraction.affineScaleLBP.Ellipse;
import jceliac.featureExtraction.affineScaleLBP.EllipticPoint;
import jceliac.featureExtraction.affineScaleLBP.EllipticPointLookup;
import jceliac.tools.filter.LowpassFilter;
import jceliac.tools.filter.LowpassFilter.EFilterType;
import jceliac.tools.filter.NonUniformGaussianFilter;
import jceliac.tools.filter.scalespace.SecondMomentMatrix;

/**
 *
 * @author shegen
 */
public class LBPAffineFilteredDataSupplier {

    protected double[] sinLookup;
    protected double[] cosLookup;

    protected double[] sin05Lookup;
    protected double[] cos05Lookup;

    private double[][] data;
    private LBPScaleFilteredDataSupplier dataSupplier;
    private EllipticPointLookup lookup;

    public LBPAffineFilteredDataSupplier(double[][] data, LBPScaleFilteredDataSupplier dataSupplier,
            EllipticPointLookup lookup) {
        this.sinLookup = new double[8];
        this.cosLookup = new double[8];

        this.sin05Lookup = new double[8];
        this.cos05Lookup = new double[8];

        for (int neighborNum = 0; neighborNum < 8; neighborNum++) {
            this.sinLookup[neighborNum] = Math.sin((2.0d * Math.PI * (double) (neighborNum + 1))
                    / (double) 8.0d);

            this.sin05Lookup[neighborNum] = Math.sin((Math.PI * 2 - 0.5) + ((2.0d * Math.PI * (double) (neighborNum + 1))
                    / (double) 8.0d));

            this.cosLookup[neighborNum] = Math.cos((2.0d * Math.PI * (double) (neighborNum + 1))
                    / (double) 8.0d);

            this.cos05Lookup[neighborNum] = Math.cos((Math.PI * 2 - 0.5) + ((2.0d * Math.PI * (double) (neighborNum + 1))
                    / (double) 8.0d));

        }
        this.dataSupplier = dataSupplier;
        this.data = data;
        this.lookup = lookup;
    }

    public boolean retrieveUniformFilteredNeighborsForLBPRadius(int x, int y,
            EllipticPoint[] points,
            double lbpRadius,
            AffineFilteredNeighbor[] neighbors,
            AffineFilteredNeighbor center)
            throws Exception {
        double sigma = LBPScaleFilteredDataSupplier.computeSigmaForLBPRadius(lbpRadius);

        int filterSize = LowpassFilter.computeOptimalFilterSize(127, sigma);
        LowpassFilter filter = new LowpassFilter(EFilterType.FILTER_GAUSSIAN, filterSize, sigma);

        double centerValue = filter.filterContinuousDataPosition(x, y, this.data);
        center.position = 0;
        center.value = centerValue;
        center.distanceToCenter = 0;

        for (int p = 0; p < 8; p++) {
            double coordX, coordY;
            coordX = points[p].x + x;
            coordY = points[p].y + y;
            double value = filter.filterContinuousDataPosition(coordX, coordY, this.data);
            neighbors[p].value = value;
            neighbors[p].position = p;
            neighbors[p].distanceToCenter = Math.sqrt((coordX - x) * (coordX - x)) + ((coordY - y) * (coordY - y));
        }
        return true;
    }

    // this expects an unnormalized smm
    public boolean retrieveAffineFilteredNeighborsForLBPRadius(int x, int y,
            Ellipse ellipse,
            SecondMomentMatrix smm,
            double lbpRadius,
            AffineFilteredNeighbor[] neighbors,
            AffineFilteredNeighbor center)
            throws Exception {
        double radius = LBPScaleFilteredDataSupplier.computeSampleRadiusForLBPRadius(lbpRadius);
        double sigma = LBPScaleFilteredDataSupplier.computeSigmaForLBPRadius(lbpRadius);
        ellipse.normalize(lbpRadius, Ellipse.CIRCUMFERENCE_NORMALIZATION_MODE);

        double[][] m = smm.getAsNormalizedArray(sigma);
        int filterSize = LowpassFilter.computeOptimalFilterSize(127, sigma);
        //   LowpassFilter filter = new LowpassFilter(EFilterType.FILTER_GAUSSIAN, filterSize, sigma);
        NonUniformGaussianFilter filter = new NonUniformGaussianFilter(filterSize, sigma, m);

        EllipticPoint[] points;
        points = this.lookup.lookupOld(x, y, ellipse.a, ellipse.b, ellipse.phi);

        double centerValue = filter.filterContinuousDataPosition(x, y, this.data);
        center.position = 0;
        center.value = centerValue;
        center.distanceToCenter = 0;

        for (int p = 0; p < 8; p++) {
            double coordX, coordY;
            coordX = points[p].x;
            coordY = points[p].y;
            double value = filter.filterContinuousDataPosition(coordX, coordY, this.data);
            neighbors[p].value = value;
            neighbors[p].position = p;
            neighbors[p].distanceToCenter = Math.sqrt((coordX - x) * (coordX - x)) + ((coordY - y) * (coordY - y));
        }
        return true;
    }

    // this expects an unnormalized smm
    public boolean retrieveAffineFilteredNeighborsForLBPRadius(int x, int y,
            SecondMomentMatrix smm,
            double lbpRadius,
            AffineFilteredNeighbor[] neighbors,
            AffineFilteredNeighbor center,
            boolean isTraining)
            throws Exception {
        double radius = LBPScaleFilteredDataSupplier.computeSampleRadiusForLBPRadius(lbpRadius);
        double sigma = LBPScaleFilteredDataSupplier.computeSigmaForLBPRadius(lbpRadius);

        double[][] m = smm.getAsNormalizedArray(sigma);
        Ellipse ellipse = new Ellipse(smm, lbpRadius, Ellipse.AXIS_SUM_NORMALIZATION_MODE);
        ellipse.a = 1.3;
        ellipse.b = 1;
        ellipse.increaseIsotropy(radius);

        int filterSize = NonUniformGaussianFilter.getBestNonuniformFilterDimension(127, m);
        LowpassFilter filter = new LowpassFilter(EFilterType.FILTER_GAUSSIAN, filterSize, sigma);
        //NonUniformGaussianFilter filter = new NonUniformGaussianFilter(filterSize, sigma, m);

        EllipticPoint[] points;

        if (isTraining) {
            // lookup should return values corresponding to this!!!
            //    EllipticPoint [] p1 = this.lookup.distributePointsOnEllipse(0, 0, 1, 1.3, 0);
            //   EllipticPoint [] p2 = this.lookup.distributePointsOnEllipse(0, 0, 1, 1.3, (Math.PI*2)-0.5);

            //points = this.lookup.lookupOld(x,y, 1.3, 1,  0);
            // EllipticLookupTableEntry entry = this.lookup.computeLookupTableEntry(1.0d / 1.3d);
            points = this.lookup.lookupOld(x, y, ellipse.a, ellipse.b, 0);
            int blub = 0;
            //   points = this.lookup.distributePointsOnEllipse(x,y, 1, 1.3, 0);
        } else {
            //    points = this.lookup.distributePointsOnEllipse(x,y, 1, 1.3, (Math.PI*2)-0.5);
            //points = this.lookup.lookupOld(x,y, 1.3, 1,  (Math.PI*2)-0.5);
            points = this.lookup.lookupOld(x, y, ellipse.a, ellipse.b, (Math.PI * 2) - 0.5);
        }

        // points = this.lookup.lookupOld(x, y, 1.3*lbpRadius, 1*lbpRadius, 0);
        double centerValue = filter.filterContinuousDataPosition(x, y, this.data);
        //   double centerValue = this.getInterpolatedPixelValueFast(data, x,y); //filter.filterContinuousDataPosition(x, y, this.data);
        center.position = 0;
        center.value = centerValue;
        center.distanceToCenter = 0;

        double minDistance = 0;
        for (int p = 0; p < 8; p++) {
            double coordX, coordY;
            /*if(isTraining) {
             coordX = x + lbpRadius * this.cosLookup[p];
             coordY = y + lbpRadius * this.sinLookup[p];
             }else {
             coordX = x + lbpRadius * this.cos05Lookup[p];
             coordY = y + lbpRadius * this.sin05Lookup[p];
             }*/
            coordX = points[p].x;
            coordY = points[p].y;
            double value = filter.filterContinuousDataPosition(coordX, coordY, this.data); //this.getInterpolatedPixelValueFast(data, coordX, coordY); //filter.filterContinuousDataPosition(coordX, coordY, this.data);
            neighbors[p].value = value;
            neighbors[p].position = p;
            neighbors[p].distanceToCenter = Math.sqrt((coordX - x) * (coordX - x)) + ((coordY - y) * (coordY - y));
        }
        return true;

    }

    protected double getInterpolatedPixelValueFast(final double[][] data, double x, double y)
            throws Exception {

        int fx = (int) x;
        int fy = (int) y;
        int cx = (int) Math.ceil(x);
        int cy = (int) Math.ceil(y);

        double ty = y - fy;
        double tx = x - fx;

        double invtx = 1 - tx;
        double invty = 1 - ty;

        double w1 = invtx * invty;
        double w2 = tx * invty;
        double w3 = invtx * ty;
        double w4 = tx * ty;

        // use this only in extreme debugging cases, this method is called every time
        // Experiment.log(Logger.LOG_HHDEBUG, "getInterpoaltedPixelValue: %f * %f", w1, data[fx][fy]);
        // Experiment.log(Logger.LOG_HHDEBUG, "getInterpoaltedPixelValue: %f * %f", w2, data[cx][fy]);
        // Experiment.log(Logger.LOG_HHDEBUG, "getInterpoaltedPixelValue: %f * %f", w3, data[fx][cy]);
        //  Experiment.log(Logger.LOG_HHDEBUG, "getInterpoaltedPixelValue: %f * %f", w4, data[cx][cy]);
        double ret = w1 * data[fx][fy] + w2 * data[cx][fy] + w3 * data[fx][cy] + w4 * data[cx][cy];
        return ret;
    }

}
