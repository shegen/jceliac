/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBPScaleBias;

import jceliac.featureExtraction.LTP.FeatureExtractionParametersLTP;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersScaleLBP extends FeatureExtractionParametersLTP {

    protected boolean useDoG = false;
    protected boolean useMultipleScalesPerPixel = true;
    protected boolean buildScaleMask = false;
    protected boolean useMinimas = false;
    protected boolean useMaximas = true;
    protected boolean useLocalMaximaPerPixelNeighborhood = true;

    public boolean isUseMultipleScalesPerPixel() {
        return useMultipleScalesPerPixel;
    }

    public void setUseMultipleScalesPerPixel(boolean useMultipleScalesPerPixel) {
        this.useMultipleScalesPerPixel = useMultipleScalesPerPixel;
    }

    public boolean isUseMinimas() {
        return useMinimas;
    }

    public void setUseMinimas(boolean useMinimas) {
        this.useMinimas = useMinimas;
    }

    public boolean isUseMaximas() {
        return useMaximas;
    }

    public void setUseMaximas(boolean useMaximas) {
        this.useMaximas = useMaximas;
    }

    public boolean isUseDoG() {
        return useDoG;
    }

    public void setUseDoG(boolean useDoG) {
        this.useDoG = useDoG;
    }

    public boolean isUseLocalMaximaPerPixelNeighborhood() {
        return useLocalMaximaPerPixelNeighborhood;
    }

    public void setUseLocalMaximaPerPixelNeighborhood(boolean useLocalMaximaPerPixelNeighborhood) {
        this.useLocalMaximaPerPixelNeighborhood = useLocalMaximaPerPixelNeighborhood;
    }

    public boolean isBuildScaleMask() {
        return buildScaleMask;
    }

    public void setBuildScaleMask(boolean buildScaleMask) {
        this.buildScaleMask = buildScaleMask;
    }

    @Override
    public String getMethodName() {
        return "ScaleLBP";
    }

    @Override
    public void report()
    {
        super.report();
    }
    
    

}
