/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBPScaleBias;

import jceliac.featureExtraction.affineScaleLBP.Ellipse;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ShapeEstimation;
import jceliac.tools.math.MathTools;

/**
 * This represents the shape bias, used for affine adaptive LBP and
 * experimental.
 * <p>
 * @author shegen
 */
public class LBPShapeBias
        implements Comparable<LBPShapeBias>
{

    // axis lengths have been compute for ellipses fitting a circle with radius 1
    private double majorAxisLen;
    private double minorAxisLen;
    private final int id;

    public LBPShapeBias(ShapeEstimation estimation, int id)
    {
        this.majorAxisLen = 0; //estimation.getAxisMaxSum();
        this.minorAxisLen = 0; //estimation.getAxisMinSum();
        this.id = id;
    }

    public LBPShapeBias(int id)
    {
        this.id = id;
    }

    public LBPShapeBias(int id, double major, double minor)
    {
        this.id = id;
        this.majorAxisLen = major;
        this.minorAxisLen = minor;
    }

    public double getDistance(LBPShapeBias o)
    {
        return Math.sqrt((this.majorAxisLen - o.majorAxisLen) * (this.majorAxisLen - o.majorAxisLen)
                         + (this.minorAxisLen - o.minorAxisLen) * (this.minorAxisLen - o.minorAxisLen));
    }

    public boolean isSimilar(LBPShapeBias o)
    {
        if(Math.abs(this.majorAxisLen - o.majorAxisLen) < 0.1
           && Math.abs(this.minorAxisLen - o.minorAxisLen) < 0.1) {
            return true;
        }
        return false;
    }

    /**
     * It is not clear how we do this yet, experimental.
     * <p>
     * @param ell    Ellipse to modify.
     * @param radius Reference radius.
     */
    public void modifyShape(Ellipse ell, double radius)
    {
        double origAxisSum = ell.a + ell.b;
        double circlec; // = 2.0d * radius * Math.PI;
        double a, b;

        if(true) {
            return;
        }

        double oldA = ell.a;
        double oldB = ell.b;

        double origCircumference = MathTools.ellipseCircumference(ell.a, ell.b);

        //double newAxissum = ell.a+ell.b;
        if(ell.a > ell.b) {
            ell.a = ell.a * (1 / majorAxisLen);
            ell.b = ell.b * (1 / minorAxisLen);
        } else {
            ell.b = ell.b * (1 / majorAxisLen);
            ell.a = ell.a * (1 / minorAxisLen);
        }
        a = ell.a;
        b = ell.b;
        double commonTerm = (3 * (a + b) * radius + Math.sqrt((3 * a + b) * (a + 3 * b) * (radius * radius)));
        double c1 = (2 * radius * radius) / commonTerm;
        double c2 = commonTerm / (3 * (a * a) + 4 * a * b + 3 * (b * b));

        double scaleConstant;
        double ellc1 = MathTools.ellipseCircumference(a * c1, b * c1);
        if(Math.abs(circlec - ellc1) < 0.0001) {
            scaleConstant = c1;
        } else {
            scaleConstant = c2;
        }

        ell.a *= scaleConstant;
        ell.b *= scaleConstant;
        double newCircumference = MathTools.ellipseCircumference(ell.a, ell.b);

    }

    @Override
    public int compareTo(LBPShapeBias o)
    {
        if(this.id == o.id) {
            return 0;
        } else {
            return 1;
        }
    }

}
