/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation;

import org.apache.commons.math3.exception.TooManyEvaluationsException;
import jceliac.tools.filter.scalespace.ScalespaceStructureTensor;
import org.apache.commons.math3.exception.ConvergenceException;
import jceliac.tools.filter.scalespace.ScalespaceParameters;
import jceliac.tools.filter.scalespace.SecondMomentMatrix;
import jceliac.featureExtraction.affineScaleLBP.Ellipse;
import jceliac.JCeliacGenericException;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.math.GaussianFit;
import java.util.ArrayList;
import jceliac.tools.filter.scalespace.ScalespaceRepresentation;
import jceliac.tools.math.MathTools;


/**
 * Performs the orientation estimation used in SOALBP type methods.
 * <p>
 * @author shegen
 */
public class OrientationEstimation
{

    private double[] preparedAngularData;

    public static final int NUM_BINS = 90;
    // take offsets such that angles with 15 degrees left and right are used,
    // this is the average error of the estimation
    public static final int ZERO_OFFSET = (int)Math.round(15.0d/(180.0d/NUM_BINS));

    private double dominantOrientation;
    private double orientationSigma;
    private double mu;
    private double sigma;
    private double angularOffset;

    private byte[][] orientationsPerPixel;
    public double [] confidenceOfOrientationDelta;
    
    
    /**
     * Computes scale confidences based on the standard deviation and mean of
     * the fitted gaussian function such that the dominant scale has a
     * confidence of 1.
     * <p>
     * @param mu    Mean value of fitted Gaussian.
     * @param sigma Standard deviation of fitted Gaussian.
     * @param mid   Estimated center index of Gaussian fit.
     */
    private void computeConfidencesForDeltas(double sigma)
    {
        this.confidenceOfOrientationDelta = new double[180];
        // scale the sigma, the fitted gaussian is in unit of histogram "bins"
        // we therefore have to rescale it to fit the angles
        double sigmaScaled = sigma * (180.0d / NUM_BINS);
        
        for(int index = 0; index < this.confidenceOfOrientationDelta.length; index++) {
            double confidence = Math.exp((-(index) * (index)) / ((2 * sigmaScaled) * (2 * sigmaScaled)));
            this.confidenceOfOrientationDelta[index] = confidence;
        }
    }
    

    // returns dominant orientation in "radiant"
    public double getDominantOrientation()
    {
        return dominantOrientation % (Math.PI * 2);
    }

    public void setDominantOrientation(double orientation)
    {
        this.dominantOrientation = orientation;
    }

    public double getOrientationSigma()
    {
        return orientationSigma;
    }

    public void overwriteDominantOrientation(double dominantOrientation)
    {
        this.dominantOrientation = dominantOrientation;
    }
    

    /**
     * Estimates the global orientation of a texture.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public void estimateGlobalOrientation(ScalespaceRepresentation scaleRep,
                                          int estimatedScale)
            throws JCeliacGenericException 
    {
        ScalespaceStructureTensor structureTensors = new ScalespaceStructureTensor(scaleRep);
        ArrayList<Double> angles = new ArrayList<>();

      //  int baseScale = Math.round(this.estimatedScale / 2);
        int baseScale = Math.round(estimatedScale);
        structureTensors.computeStructureTensorsAtSpecificScales(new int[]{baseScale - 1, baseScale, baseScale + 1});

        for (int i = 0; i < structureTensors.getWidth(); i++) {
            for (int j = 0; j < structureTensors.getHeight(); j++) {

                for (int scaleLevel = baseScale - 1; scaleLevel < baseScale + 1 && scaleLevel < scaleRep.getScalespaceParameters().getNumberOfScales();
                        scaleLevel++) {
                    SecondMomentMatrix smm = structureTensors.getStructureTensor(i, j, scaleLevel);
                    try {
                        if (smm != null) {
                            Ellipse ellipse1 = new Ellipse(smm, 1, Ellipse.NO_NORMALIZATION);
                            angles.add(ellipse1.phi % Math.PI);
                        }
                    } catch (JCeliacGenericException e) { // ignore if single structure tensors are bad

                    }
                }

            }
        }
        double[] anglesHistogram = new double[NUM_BINS];
        for (double angle : angles) {
            int angleIndex = (int) (((angle / Math.PI) * NUM_BINS)+0.5d);
            angleIndex = angleIndex % NUM_BINS;
            anglesHistogram[angleIndex]++;
        }
        this.preparedAngularData = new double[anglesHistogram.length];
        this.angularOffset = this.prepareAngularData(anglesHistogram, this.preparedAngularData);
        int maxIndex = ArrayTools.maxIndex(this.preparedAngularData);
        // zero out points at the borders
        for (int off = ZERO_OFFSET; maxIndex + off < this.preparedAngularData.length; off++) {
            int index = maxIndex + off;
            this.preparedAngularData[index] = 0;
        }
        for (int off = ZERO_OFFSET; maxIndex - off >= 0; off++) {
            int index = maxIndex - off;
            this.preparedAngularData[index] = 0;
        }
        // fit gaussian
        GaussianFit fitter = new GaussianFit(this.preparedAngularData);
        double _sigma, _mu;
        try {
            double[] gaussianParameters = fitter.fitBounded(10000);
            _sigma = gaussianParameters[2];
            _mu = gaussianParameters[1];
        } catch (ConvergenceException | TooManyEvaluationsException e) {
            _sigma = 0;
            _mu = maxIndex;
        }
        this.sigma = _sigma;
        this.mu = _mu;

        this.orientationSigma = _sigma;
        this.dominantOrientation = (((_mu + this.angularOffset) * (180 / NUM_BINS)) / 180) * Math.PI;
        if (this.dominantOrientation < 0) {
            this.dominantOrientation = Math.PI * 2 - this.dominantOrientation;
        }
    }

    /**
     * Estimates the local orientations of a texture on a pixel basis.
     * @throws jceliac.JCeliacGenericException
     */
    public void estimateLocalOrientation(ScalespaceRepresentation scaleRep,
                                          int estimatedScale)
            throws JCeliacGenericException
            
    {
        ScalespaceStructureTensor structureTensors = new ScalespaceStructureTensor(scaleRep);
        int baseScale = Math.round(estimatedScale);
        structureTensors.computeStructureTensorsAtSpecificScales(new int[] {baseScale-1, baseScale, baseScale+1} );
        
        // orientations in degrees between 0 and 180
        this.orientationsPerPixel = new byte[structureTensors.getWidth()][structureTensors.getHeight()];

        for(int i = 0; i < structureTensors.getWidth(); i++) {
            for(int j = 0; j < structureTensors.getHeight(); j++) {

                SecondMomentMatrix smm = structureTensors.getStructureTensor(i, j, baseScale);
                try {
                    //XXX: could optimize here, no need to normalize the ellipse only need angle between eigenvectors!
                    if(smm != null) {
                        Ellipse ellipse1 = new Ellipse(smm, 1, Ellipse.CIRCUMFERENCE_NORMALIZATION_MODE);
                        double angleRad = ellipse1.phi % Math.PI;
                        this.orientationsPerPixel[i][j] = (byte) Math.round((angleRad / Math.PI) * 180);
                    }
                } catch(JCeliacGenericException e) { // ignore if single structure tensors are bad

                }
            }
        }
    }

    public byte[][] getOrientationsPerPixel()
    {
        return this.orientationsPerPixel;

    }

    public double getOrientationForOffset(double sigmaOffset)
    {
        return Math.PI * 2 + ((((this.mu + this.angularOffset) * (180 / NUM_BINS)) / 180) * Math.PI);
    }

    /**
     * Prepares the angular data (which is modulus 2*pi) such that the maximum
     * of the histogram is centered in the new histogram for fitting the
     * Gaussian function. We then set certain data points far away on the x-axis
     * from the center to 0 to improve the Gaussian fit returns the offset of
     * the angular data.
     * <p>
     * @param anglesHistogram Histogram of angles.
     * <p>
     * @return Offset index into the new histogram.
     */
    private int prepareAngularData(double[] anglesHistogram, double [] _preparedAngularData)
    {
        // find max index
        int maxIndex = ArrayTools.maxIndex(anglesHistogram);
        int midIndex = Math.round(anglesHistogram.length / 2);

        for(int i = 0; i < anglesHistogram.length; i++) {
            _preparedAngularData[(midIndex + i) % anglesHistogram.length]
            = anglesHistogram[(maxIndex + i) % anglesHistogram.length];
        }
        return (maxIndex - midIndex);
    }
    
    /**
     * Compute the offset that the dominant orientation is within a 95% confidence intervall. 
     * 
     * @return 
     */
    public double getOrientationDelta()
    {
        // compute angle that is needed such that 95% off the gaussian mass is within
        // -indexOffset and indexOffset, given the sigma of the orientation
        int indexOffset = (int)Math.round(this.orientationSigma * Math.sqrt(2.0d) * MathTools.erfinv(0.95));
        // compute the angle based on number of bins in histogram
        double deltaDeg = (indexOffset / (double)NUM_BINS) * 180.0d;
        if(deltaDeg >= 90) {
            this.dominantOrientation = 0;
            return Math.PI/2;
        }
        // convert to rad
        return (deltaDeg / 180.0d) * Math.PI;
    }
    
    public double getConfidenceForOrientation(double orientationDelta)
    {
        double delta = Math.abs(orientationDelta);
        int deltaDeg = (int)Math.round((delta / Math.PI) * 180.0d);
        
        int index = deltaDeg % 180;
        return this.confidenceOfOrientationDelta[index];
    }

}
