/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Used to store multiple scale and shape estimations by class.
 * <p>
 * @author shegen
 */
public class EstimationByClass<T>
{

    protected HashMap<Integer, ArrayList<T>> estimationStorage;

    public EstimationByClass()
    {
        this.estimationStorage = new HashMap<>();
    }

    public void addEstimations(int classNumber, ArrayList<T> estimation)
    {
        ArrayList<T> list;

        if(this.estimationStorage.containsKey(classNumber)) {
            list = this.estimationStorage.get(classNumber);
        } else {
            list = new ArrayList<>();
        }
        list.addAll(estimation);
        this.estimationStorage.put(classNumber, list);
    }

    public Set<Integer> getKeys()
    {
        return this.estimationStorage.keySet();
    }
}
