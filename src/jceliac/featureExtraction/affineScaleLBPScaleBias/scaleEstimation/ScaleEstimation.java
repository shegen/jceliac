/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation;

import org.apache.commons.math3.exception.TooManyEvaluationsException;
import jceliac.tools.filter.scalespace.ScaleLevelIndexedDataSupplier;
import org.apache.commons.math3.exception.ConvergenceException;
import jceliac.tools.filter.scalespace.ScalespaceParameters;
import jceliac.tools.arrays.ArrayTools;
import jceliac.JCeliacGenericException;
import jceliac.tools.math.GaussianFit;
import jceliac.experiment.Experiment;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.filter.LowpassFilter;
import jceliac.tools.math.MathTools;

/**
 * This class represents the scale estimation used in scale adaptive LBP.
 * <p>
 * @author shegen
 */
public class ScaleEstimation
{

    private final static double CONFIDENCE_LOWER_THRESHOLD = 0.4082; // 20 / 49 for normalized sigma
    private final ScalespaceParameters scalespaceParameters;
    private ScaleLevelIndexedDataSupplier<double[][]> normalizedLaplacians;
    private double[] scaleMeasures;
    private double[] scaleMeasuresMinima;
    
    private double [][][] scaleMeasuresLocal;
    private double estimatedLocalScale [][];
    
    private double scaleDeviation;
    private int estimatedScaleLevel = -1;
    private double estimationStandardDeviation;
    private double gaussFittedMu = Double.NaN;
    private double gaussFittedSigma = Double.NaN;
    private double[] confidences;
    private int LOCAL_TILE_SIZE = 7;
    private boolean isLocalScaleEstimation = false;
    public double scaleBiasConfidence = 0;

    public ScaleEstimation(ScalespaceParameters scalespaceParameters,
                           ScaleLevelIndexedDataSupplier<double[][]> normalizedLaplacians,
                           int imageWidth, int imageHeight)
    {
        this.scalespaceParameters = scalespaceParameters;
        this.normalizedLaplacians = normalizedLaplacians;
    }

    public ScaleEstimation(ScalespaceParameters scalespaceParameters,
                           int dominantScale)
    {
        this.scalespaceParameters = scalespaceParameters;
        this.estimatedScaleLevel = dominantScale;
        this.confidences = new double[this.scalespaceParameters.getNumberOfScales()];
        this.confidences[dominantScale] = 1.0d;

    }

    /**
     * Computes the sums of maxima and minima per pixel measures of the
     * Laplacian response in scale-space.
     * <p>
     * @throws JCeliacGenericException
     */
    private void computeSumOfMinMaxScaleMeasure()
            throws JCeliacGenericException
    {
        ScalespaceParameters params = this.scalespaceParameters;
        this.scaleMeasures = new double[params.getNumberOfScales()];
        this.scaleMeasuresMinima = new double[params.getNumberOfScales()];

        for(int scaleLevel = 0; scaleLevel < params.getNumberOfScales(); scaleLevel++) {
            double[][] normalizedLaplacianAtScale = this.normalizedLaplacians.getData(scaleLevel);
            double sumMax = this.computeSumOfMaxima(normalizedLaplacianAtScale);
            double sumMin = this.computeSumOfMinima(normalizedLaplacianAtScale);

            this.scaleMeasures[scaleLevel] = sumMax;
            this.scaleMeasuresMinima[scaleLevel] = sumMin;
        }
        this.normalizedLaplacians = null;
    }
    
    public void estimateLocalScale() 
            throws JCeliacGenericException 
    {
        this.computeSumOfMinMaxScaleMeasureLocal();
        this.estimateLocalScaleLevelByGaussianFit();
        this.isLocalScaleEstimation = true;
    }
    
    private void computeSumOfMinMaxScaleMeasureLocal()
            throws JCeliacGenericException
    {
        ScalespaceParameters params = this.scalespaceParameters;
        double [][][] scaleMeasuresPerPixel = new double[params.getNumberOfScales()][][];
        
        // use average filtering to compute the mean values for each pixel
        LowpassFilter averagingFilter = new LowpassFilter(LowpassFilter.EFilterType.FILTER_AVERAGE, LOCAL_TILE_SIZE);
        for(int scaleLevel = 0; scaleLevel < params.getNumberOfScales(); scaleLevel++) {
            double[][] normalizedLaplacianAtScale = this.normalizedLaplacians.getData(scaleLevel);
            scaleMeasuresPerPixel[scaleLevel] = averagingFilter.filterData(normalizedLaplacianAtScale);
        }
        this.scaleMeasuresLocal = scaleMeasuresPerPixel;
    }
    
    private void estimateLocalScaleLevelByMaximum()
    {
        this.estimatedLocalScale = new double[this.scaleMeasuresLocal[0].length][this.scaleMeasuresLocal[0][0].length];
        for(int i = 0; i < this.scaleMeasuresLocal[0].length; i++) {
            for(int j = 0; j < this.scaleMeasuresLocal[0][0].length; j++) {
                double [] localMeasures = new double[this.scaleMeasuresLocal.length];
                for(int scaleLevel = 0; scaleLevel < localMeasures.length; scaleLevel++) {
                    localMeasures[scaleLevel] = this.scaleMeasuresLocal[scaleLevel][i][j];
                }
                int maxIndex = ArrayTools.maxIndex(localMeasures);
                if(maxIndex < 0) {
                    maxIndex = ArrayTools.maxIndex(localMeasures);
                }
                this.estimatedLocalScale[i][j] = maxIndex;
            }
        }
    }
    
    private void estimateLocalScaleLevelByGaussianFit()
            throws JCeliacGenericException
    {
        this.estimatedLocalScale = new double[this.scaleMeasuresLocal[0].length][this.scaleMeasuresLocal[0][0].length];
        for(int i = 0; i < this.scaleMeasuresLocal[0].length; i++) {
            for(int j = 0; j < this.scaleMeasuresLocal[0][0].length; j++) {
                double [] localMeasures = new double[this.scaleMeasuresLocal.length];
                for(int scaleLevel = 0; scaleLevel < localMeasures.length; scaleLevel++) {
                    localMeasures[scaleLevel] = this.scaleMeasuresLocal[scaleLevel][i][j];
                }
                int scaleLevel = this.fitGaussian(localMeasures);
                if(scaleLevel <= 0) {
                    int blub = 0;
                }
                this.estimatedLocalScale[i][j] = scaleLevel;
            }
        }
        LowpassFilter averagingFilter = new LowpassFilter(LowpassFilter.EFilterType.FILTER_AVERAGE, LOCAL_TILE_SIZE);
        averagingFilter.filterData(this.estimatedLocalScale);
    }
    
    private int fitGaussian(double [] data) 
    {
         // compute a hint for the threshold
        double[] dxScaleMeasuresMax = MathTools.derivativeOrder1(data);

        // find zeros
        int zeroIndexMax = -1;
        for(int i = 0; i < dxScaleMeasuresMax.length - 1; i++) {
            if(dxScaleMeasuresMax[i] >= 0 && dxScaleMeasuresMax[i + 1] <= 0) {
                zeroIndexMax = i;
                break;
            }
        }
        if(true) {
            int mid = Math.round(data.length / 2);
            if(zeroIndexMax < 0 || zeroIndexMax >= data.length) {
                return mid;
            }
            return zeroIndexMax;
        }
        
        int mid = Math.round(data.length / 2);
        int scaleLevel;
        try {
            // prepare data, pick 5 poitns on the left and right from the dominant scale to fit
            double[] fitData = new double[data.length];
            for(int off = -5;
                off <= 5 && zeroIndexMax + off < fitData.length && zeroIndexMax + off >= 0;
                off++) {
                fitData[zeroIndexMax + off] = data[zeroIndexMax + off];
            }
            GaussianFit gaussFitter = new GaussianFit(fitData);
            // params are [0] = A , [1] = mu, [2] = sigma
            double[] parameters = gaussFitter.fitBounded(10000);
            // sigma is the confidence of the estimation, the larger the sigma the bigger
            // the uncertainty of the dominant scale
            double sigma = parameters[2];
            // mu is the actual dominant scale
            double mu = parameters[1];

            // A is a constant scaling factor we currently do not use
            scaleLevel = (int) Math.round(mu);
            
            if(scaleLevel < 1
               || scaleLevel >= this.scalespaceParameters.getNumberOfScales()) {

                // this means we could not estimate the scale
                scaleLevel = mid; 
            }
        } catch(ConvergenceException | TooManyEvaluationsException e) {
            scaleLevel = mid;
        }
        return scaleLevel;
    }

    public boolean isValid()
    {
        if(this.estimatedScaleLevel < 1
           || this.estimatedScaleLevel >= this.scalespaceParameters.getNumberOfScales()
           || this.gaussFittedSigma < CONFIDENCE_LOWER_THRESHOLD)
        {
            return true;
        }else {
            return false;
        }
    }

    
    
    /**
     * Computes the sum of maxima Laplacian responses in scale-space.
     * <p>
     * @param data Normalized laplacian responses in scale-space at a certain
     *             scale-level.
     * <p>
     * @return Sum of maxima at that scale-level.
     */
    private double computeSumOfMaxima(double[][] data)
    {
        double sum = 0;
        for(int i = 0; i < data.length; i++) {
            for(int j = 0; j < data[0].length; j++) {
                if(data[i][j] > 0) {
                    sum += data[i][j];
                }
            }
        }
        return sum;
    }

    /**
     * Computes the sum of minima Laplacian responses in scale-space.
     * <p>
     * @param data Normalized laplacian responses in scale-space at a certain
     *             scale-level.
     * <p>
     * @return Sum of minima at that scale-level.
     */
    private double computeSumOfMinima(double[][] data)
    {
        double sum = 0;
        for(double[] tmpData : data) {
            for(int j = 0; j < data[0].length; j++) {
                if(tmpData[j] < 0) {
                    sum += Math.abs(tmpData[j]);
                }
            }
        }
        return sum;
    }

    
    private double computeSumOfMaximaLocal(double[][] data, int posX, int posY)
    {
        double sum = 0;
        for(int offX = 0; offX < LOCAL_TILE_SIZE; offX++) {
            for(int offY = 0; offY < LOCAL_TILE_SIZE; offY++) {
                if(data[posX + offX][posY + offY] > 0) {
                    sum += data[posX + offX][posY + offY];
                }
            }
        }
        return sum;
    }

    public int getNumberOfScales()
    {
        return this.scaleMeasures.length;
    }

    public double getConfidenceForScale(int scaleLevel)
    {
        return this.confidences[scaleLevel];
    }

    public void estimateGlobalScale() 
            throws JCeliacGenericException
    {
        this.computeSumOfMinMaxScaleMeasure();
        this.estimateScaleLevelByGaussianFit();
    }
    
    /**
     * Estimates the dominant scale of the computed scale-response measure
     * stored in scaleMeasures by fitting a gaussian function to the prepared
     * scaleMeasures.
     */
    public void estimateScaleLevelByGaussianFit()
    {

        // compute a hint for the threshold
        double[] dxScaleMeasuresMax = MathTools.derivativeOrder1(this.scaleMeasures);

        // find zeros
        int zeroIndexMax = -1;
        for(int i = 0; i < dxScaleMeasuresMax.length - 1; i++) {
            if(dxScaleMeasuresMax[i] >= 0 && dxScaleMeasuresMax[i + 1] <= 0) {
                zeroIndexMax = i;
                break;
            }
        }
        int mid = Math.round(this.scaleMeasures.length / 2);
        try {
            // prepare data, pick 5 points on the left and right from the dominant scale to fit
            double[] fitData = new double[this.scaleMeasures.length];
            // pick 10% left and right of the estimation for fitting the gaussian
            int offDelta = (int)Math.round(this.scaleMeasures.length / 10.0d);
            for(int off = -offDelta; off <= offDelta; off++) {
                if(zeroIndexMax+off > 0 && zeroIndexMax + off < this.scaleMeasures.length) {
                    fitData[zeroIndexMax + off] = this.scaleMeasures[zeroIndexMax + off];
                }
            }
            GaussianFit gaussFitter = new GaussianFit(fitData);
            // params are [0] = A , [1] = mu, [2] = sigma
            double[] parameters = gaussFitter.fitBounded(10000);
            // sigma is the confidence of the estimation, the larger the sigma the bigger
            // the uncertainty of the dominant scale
            double sigma = parameters[2];
            // mu is the actual dominant scale
            double mu = parameters[1];
            
            double normalizedSigma = sigma / this.scaleMeasures.length;
            
            this.gaussFittedSigma = normalizedSigma;
            this.gaussFittedMu = mu;

            // A is a constant scaling factor we currently do not use
            this.estimationStandardDeviation = sigma;
            this.estimatedScaleLevel = (int) Math.round(mu);
            if(this.estimatedScaleLevel < 1
               || this.estimatedScaleLevel >= this.scalespaceParameters.getNumberOfScales()
               || this.gaussFittedSigma > CONFIDENCE_LOWER_THRESHOLD) {

                // this means we could not estimate the scale
                this.estimatedScaleLevel = mid;
                this.estimationStandardDeviation = Double.POSITIVE_INFINITY;

                this.gaussFittedMu = mid;
                this.gaussFittedSigma = Double.POSITIVE_INFINITY;
            }
            this.computeConfidences(mu, sigma, mid);
        } catch(ConvergenceException | TooManyEvaluationsException e) {
            this.estimatedScaleLevel = mid;
            this.estimationStandardDeviation = Double.POSITIVE_INFINITY;

            this.gaussFittedMu = mid;
            this.gaussFittedSigma = Double.POSITIVE_INFINITY;

            this.confidences = new double[this.scalespaceParameters.getNumberOfScales()];
            this.confidences[this.estimatedScaleLevel] = 1.0d;
        }
    }

    /**
     * Computes scale confidences based on the standard deviation and mean of
     * the fitted gaussian function such that the dominant scale has a
     * confidence of 1.
     * <p>
     * @param mu    Mean value of fitted Gaussian.
     * @param sigma Standard deviation of fitted Gaussian.
     * @param mid   Estimated center index of Gaussian fit.
     */
    private void computeConfidences(double mu, double sigma, int mid)
    {
        this.confidences = new double[this.scalespaceParameters.getNumberOfScales()];
        for(int index = 0; index < this.confidences.length; index++) {
            double scaleLevel = index + 1; // scales start with level 1
            double confidence = Math.exp((-(scaleLevel - mu) * (scaleLevel - mu)) / ((2 * sigma) * (2 * sigma)));
            this.confidences[index] = confidence;
        }
        if(this.estimatedScaleLevel >= this.confidences.length) {
            Experiment.log(JCeliacLogger.LOG_WARN, "WARNING estimated scale > than expected maximum scale, setting to default value");
            this.confidences[mid] = 1.0d;
        } else {
            this.confidences[this.estimatedScaleLevel] = 1.0d;
        }
    }

    public double getEstimationStandardDeviation()
    {
        return estimationStandardDeviation;
    }

    public double getScaleDeviation()
    {
        return scaleDeviation;
    }

    public void setScaleDeviation(double scaleDeviation)
    {
        this.scaleDeviation = scaleDeviation;
    }

    public void overrideEstimatedScaleLevel(int estimatedScaleLevel)
    {
        this.estimatedScaleLevel = estimatedScaleLevel;
        this.confidences = new double[this.scalespaceParameters.getNumberOfScales()];
        this.confidences[estimatedScaleLevel] = 1.0d;
    }

    public ScalespaceParameters getScalespaceParameters()
    {
        return scalespaceParameters;
    }

    public int getEstimatedScaleLevel()
    {
        return estimatedScaleLevel;
    }
    
    public double [][] getEstimatedLocalScaleLevel()
    {
        return this.estimatedLocalScale;
    }

    /**
     * Returns scale measures normalized by the maximum value.
     * <p>
     * @return Normalized scale measures.
     */
    public double[] getNormalizedScaleMeasures()
    {
        double max = ArrayTools.max(this.scaleMeasures);
        double[] normalizedMeasures = new double[this.scaleMeasures.length];
        for(int i = 0; i < normalizedMeasures.length; i++) {
            normalizedMeasures[i] = this.scaleMeasures[i] / max;
        }
        return normalizedMeasures;
    }

    public double getGaussFittedMu()
    {
        return gaussFittedMu;
    }

    public double getGaussFittedSigma()
    {
        return gaussFittedSigma;
    }

    public boolean isIsLocalScaleEstimation()
    {
        return isLocalScaleEstimation;
    }
    
}
