/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation;

import jceliac.JCeliacGenericException;
import jceliac.tools.filter.scalespace.ScalespaceStructureTensor;
import jceliac.tools.filter.scalespace.SecondMomentMatrix;
import jceliac.featureExtraction.affineScaleLBP.Ellipse;

/**
 * Performs the shape estimation this is still very experimental.
 * <p>
 * @author shegen
 */
public class ShapeEstimation
{

    private int scale;
    private ScalespaceStructureTensor structureTensor;
    private double[][] ellipseAxes;

    private ShapeEstimation()
    {
    }

    public ShapeEstimation(int scale, ScalespaceStructureTensor structureTensor)
    {
        this.scale = scale;
        this.structureTensor = structureTensor;
    }

    public void estimate()
    {
        this.ellipseAxes = new double[this.structureTensor.getWidth() * this.structureTensor.getHeight()][2];

        int index = 0;
        // collect Ellipses at scale
        for(int i = 0; i < this.structureTensor.getWidth(); i++) {
            for(int j = 0; j < this.structureTensor.getHeight(); j++) {
                try {
                    SecondMomentMatrix smm = this.structureTensor.getStructureTensor(i, j, this.scale);
                    Ellipse ellipse = new Ellipse(smm, 10.0d, Ellipse.CIRCUMFERENCE_NORMALIZATION_MODE);
                    this.ellipseAxes[index][0] = Math.max(ellipse.a, ellipse.b);
                    this.ellipseAxes[index][1] = Math.min(ellipse.a, ellipse.b);
                    index++;
                } catch(JCeliacGenericException e) {
                    // ignore bad ellipses
                }
            }
        }
    }

    public double[][] getEllipseAxes()
    {
        return ellipseAxes;
    }
}
