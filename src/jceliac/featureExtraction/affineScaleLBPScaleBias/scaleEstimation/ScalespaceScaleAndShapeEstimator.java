/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation;

import jceliac.tools.filter.scalespace.LaplacianScalespaceRepresentation;
import jceliac.tools.filter.scalespace.ScaleLevelIndexedDataSupplier;
import jceliac.tools.filter.scalespace.ScalespaceStructureTensor;
import jceliac.tools.filter.scalespace.ScalespaceRepresentation;
import jceliac.tools.filter.scalespace.ScalespaceParameters;
import jceliac.tools.filter.scalespace.ScalespacePoint;
import jceliac.tools.cluster.KMeansClustering;
import jceliac.tools.cluster.KMeansDataPoint;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.CountDownLatch;
import jceliac.tools.data.ContentStream;
import jceliac.JCeliacGenericException;
import jceliac.tools.arrays.ArrayTools;
import java.util.concurrent.Executors;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.DataSource;
import jceliac.experiment.Experiment;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.data.Frame;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;


/**
 * This estimator is used to estimate a scale distribution of a signal. It is
 * based on spatial maxima of scale-normalized Laplacian maxima in a scalespace.
 * The reason for using spatial maxima is based on the fact that 3d-maxima will
 * be sparse in certain textures and scale estimation will suffer. For example
 * non-regular textures will have bad scale estimation, also stochastic textures
 * will have bad scale estimation.
 * <p>
 * The idea of the method is based on following observations: - average
 * responses of spatial maxima will be highest at a "good scale" - at the best
 * scale for a texture the standard deviation among responses will be low - at a
 * good scale, a high percentage of the area of the image will be covered by the
 * spatial maxima
 * <p>
 * Combining these three observations we compute a scale confidence as follows:
 * - compute 1D scale normalized laplacian scalespace maxima - remove maxima
 * that describe edges (an isotropy measure bigger than 0.9) - compute the scale
 * measure as: (meanResponseAtScale-std(responsesAtScale)); - finally normalize
 * the computed distribution such that the maximum is 1.0d
 * <p>
 * The reference implementation can be found in plotResponsesByScale.m
 * <p>
 * @author shegen
 */
public class ScalespaceScaleAndShapeEstimator
{
    
    private EstimationByClass<ScaleEstimation> scaleEstimationByClass;
    private EstimationByClass<ShapeEstimation> shapeEstimationByClass;
    private ScaleEstimationByImage scaleEstimationByImage;
    private ScalespaceParameters scalespaceParameters;
    private CountDownLatch countDownLatch;

    // ToDo: Currently estimates only the scale, add shape estimation if needed. 
    public ScalespaceScaleAndShapeEstimator(ScalespaceParameters scalespaceParameters)
    {
        this.scalespaceParameters = scalespaceParameters;
        this.scaleEstimationByClass = new EstimationByClass<>();
        this.shapeEstimationByClass = new EstimationByClass<>();
        this.scaleEstimationByImage = new ScaleEstimationByImage();
    }

    /*
     * Estimates scale for the data single threaded.
     */
    public ScaleEstimation estimateScale(double[][] data)
    {
        ScalespaceScaleEstimatorThread worker
                                       = new ScalespaceScaleEstimatorThread(data, this.scalespaceParameters);
        ScaleEstimation estimation = worker.runEstimation();
        return estimation;
    }
    /*
     * Compute scale over all data provided by the DataReader. This will use
     * multithreading but will use only the grayscale data.
     */

    public EstimationByClass estimateScalesByClass(ArrayList<DataReader> readers)
            throws JCeliacGenericException
    {
        int numThreads = Math.min(readers.size(), Runtime.getRuntime().availableProcessors());
        ExecutorService threadPool = Executors.newScheduledThreadPool(numThreads);
        int numClasses = readers.size();
        this.countDownLatch = new CountDownLatch(numClasses);
        this.scaleEstimationByClass = new EstimationByClass<>();

        for(DataReader currentReader : readers) {
            ScalespaceScaleEstimatorThread worker
                                           = new ScalespaceScaleEstimatorThread(currentReader, this.scalespaceParameters, true);

            threadPool.submit(worker);
        }
        try {
            this.countDownLatch.await();
            threadPool.shutdownNow();
        } catch(InterruptedException e) {
            Experiment.log(JCeliacLogger.LOG_WARN, "Scale estimator thread didn't finish correctly: " + e.getMessage());
            // ignore 
        }
        // reset all readers
        for(DataReader currentReader : readers) {
            currentReader.reset();
        }
        return this.scaleEstimationByClass;
    }

    public EstimationByClass<ShapeEstimation> getShapeEstimationByClass()
    {
        return shapeEstimationByClass;
    }

    public void setShapeEstimationByClass(EstimationByClass<ShapeEstimation> shapeEstimationByClass)
    {
        this.shapeEstimationByClass = shapeEstimationByClass;
    }

    public HashMap<Integer, ShapeEstimation> reduceShapeEstimations()
            throws JCeliacGenericException
    {
        HashMap<Integer, ShapeEstimation> reducedShapeEstimationsByClass = new HashMap<>();
        Set<Integer> classKeys = this.shapeEstimationByClass.getKeys();

        for(Integer classKey : classKeys) {
            ArrayList<ShapeEstimation> estimationsForClass
                                       = this.shapeEstimationByClass.estimationStorage.get(classKey);

            ShapeEstimation reducedShapeEstimation = this.reduceToSingleShapeEstimation(estimationsForClass);
            reducedShapeEstimationsByClass.put(classKey, reducedShapeEstimation);
        }
        return reducedShapeEstimationsByClass;
    }

    public HashMap<Integer, ArrayList <ScaleEstimation>> reduceScaleEstimations(boolean allowMultipleScalesPerClass)
            throws JCeliacGenericException
    {
        HashMap<Integer, ArrayList <ScaleEstimation>> dominantScalesByClass = new HashMap<>();
        Set<Integer> classKeys = this.scaleEstimationByClass.getKeys();

        for(Integer classKey : classKeys) {
            ArrayList <ScaleEstimation> dominantScalesOfClass = new ArrayList <>();
            ArrayList<ScaleEstimation> estimationsForScale
                                       = this.scaleEstimationByClass.estimationStorage.get(classKey);

            if(allowMultipleScalesPerClass == false) // default behaviour
            {
                ScaleEstimation dominantScale = this.reduceToSingleEstimation(estimationsForScale);
                dominantScalesOfClass.add(dominantScale);
                dominantScalesByClass.put(classKey, dominantScalesOfClass);
            } else {
                dominantScalesOfClass = this.reduceToMultipleScaleEstimations(estimationsForScale, 0.75d);
                dominantScalesByClass.put(classKey, dominantScalesOfClass);
            }
        }
        return dominantScalesByClass;

    }

    private ScaleEstimation reduceToSingleEstimation(ArrayList<ScaleEstimation> estimationsForClass)
    {
        ArrayList<Integer> dominantScales = new ArrayList<>();
        ArrayList<Double> sigmas = new ArrayList<>();

        for(int index = 0; index < estimationsForClass.size(); index++) {
            ScaleEstimation currentEstimation = estimationsForClass.get(index);
        
            int dominantScale = currentEstimation.getEstimatedScaleLevel();
            if(currentEstimation.isValid()) {
                sigmas.add(currentEstimation.getEstimationStandardDeviation());
                dominantScales.add(dominantScale);
            }
        }
        int dominantScale;
        if(dominantScales.isEmpty()) {
            dominantScale = this.scalespaceParameters.getNumberOfScales()/2;
        } else {
            dominantScale = (int) ArrayTools.median(dominantScales);
            if(dominantScale >= this.scalespaceParameters.getNumberOfScales()) {
                dominantScale = this.scalespaceParameters.getNumberOfScales()/2;
            }
        }
        double avgSigma = ArrayTools.mean(sigmas);
        ScaleEstimation scaleEst = new ScaleEstimation(this.scalespaceParameters, dominantScale);
        scaleEst.setScaleDeviation(avgSigma);
        return scaleEst;

    }

    private class Pair {
            public int index;
            public double value;
    };
     
    /**
     * Reduces a list of scale estimations such that the top N percent of estimations 
     * are kept.
     * 
     * @param estimationsForClass
     * @param maxEstimationsPerClass
     * @return
     * @throws JCeliacGenericException 
     */
    
    private ArrayList<ScaleEstimation> 
        reduceToMultipleScaleEstimations(ArrayList<ScaleEstimation> estimationsForClass,
                                         double T)
            throws JCeliacGenericException
    {
        int sumInvalid = 0;
        ArrayList<Integer> estimatedScales = new ArrayList<>();
        for(int index = 0; index < estimationsForClass.size(); index++) {
            ScaleEstimation currentEstimation = estimationsForClass.get(index);
            //currentEstimation.estimateScaleLevelByGaussianFit();

            int dominantScale = currentEstimation.getEstimatedScaleLevel();
            if(currentEstimation.isValid()) {
                estimatedScales.add(dominantScale);
                sumInvalid++;       
            }
        }
        // compute histogram
        int [] hist = new int[this.scalespaceParameters.getNumberOfScales()];
        for(Integer est : estimatedScales) {
            hist[est]++;
        }
        double sum = ArrayTools.sum(hist);
        
        // find top maxEstimationsPerClass in hist
        TreeSet<Pair> sortedList
                      = new TreeSet<>((a,b) -> a.value > b.value ?  -1 : 1);
        for(int i = 0; i < hist.length; i++) {
            Pair p = new Pair();
            p.index = i;
            p.value = ((double)hist[i] / sum);
            sortedList.add(p);
        }
        double classScalesConfidence = (double)sumInvalid / (double)estimationsForClass.size();
        double cumSum = 0;
        ArrayList <ScaleEstimation> scaleEstimations = new ArrayList <>();
        for(Pair p : sortedList) {
            ScaleEstimation scaleEst = new ScaleEstimation(this.scalespaceParameters, p.index);
            scaleEst.scaleBiasConfidence = classScalesConfidence;
            scaleEstimations.add(scaleEst);
            cumSum += p.value;
            if(cumSum >= T) {
                break;
            }
        }
        return scaleEstimations;
    }
    
    private ShapeEstimation reduceToSingleShapeEstimation(ArrayList<ShapeEstimation> estimationsForClass)
            throws JCeliacGenericException
    {
        double[][] ellipsesAxesByClass = null;

        int j = 0;
        for(int index = 0; index < estimationsForClass.size(); index++) {
            ShapeEstimation currentEstimation = estimationsForClass.get(index);
            double[][] tmpData = currentEstimation.getEllipseAxes();
            if(ellipsesAxesByClass == null) {
                ellipsesAxesByClass = new double[tmpData.length * estimationsForClass.size()][2];
            }
            for(double[] tmp : tmpData) {
                ellipsesAxesByClass[j] = tmp;
                j++;
            }
        }
        KMeansClustering kmc = new KMeansClustering(ellipsesAxesByClass, 5, 100);
        kmc.cluster();
        KMeansDataPoint[] centers = kmc.getClusterCenters();

        return null;
    }

    /**
     * Compute scale over all data provided by the DataReader. This will use
     * multithreading but will use only the grayscale data.
     * 
     * @param readers DataReaders for all classes
     * @return Scales estimation per image. 
     */
    public ScaleEstimationByImage estimateScalesByImage(ArrayList<DataReader> readers)
    {
        int numThreads = Runtime.getRuntime().availableProcessors();
        ExecutorService threadPool = Executors.newScheduledThreadPool(numThreads);
        int numClasses = readers.size();
        this.countDownLatch = new CountDownLatch(numClasses);
        this.scaleEstimationByImage = new ScaleEstimationByImage();

        for(DataReader currentReader : readers) {
            ScalespaceScaleEstimatorThread worker
                                           = new ScalespaceScaleEstimatorThread(currentReader, this.scalespaceParameters, false);

            threadPool.submit(worker);
        }
        try {
            this.countDownLatch.await();
            threadPool.shutdownNow();
        } catch(InterruptedException e) {
            // ignore error
            Experiment.log(JCeliacLogger.LOG_WARN, "Scale estimator thread didn't finish correctly: " + e.getMessage());
        }
        // reset all readers
        for(DataReader currentReader : readers) {
            currentReader.reset();
        }
        return this.scaleEstimationByImage;
    }

    public synchronized void notifyFinished(Integer classNumber, ArrayList<ScaleEstimation> scaleEstimations)
    {
        this.scaleEstimationByClass.addEstimations(classNumber, scaleEstimations);
        if(this.countDownLatch != null) {
            this.countDownLatch.countDown();
        }
    }

    public synchronized void notifyFinished(Integer classNumber, ArrayList<ScaleEstimation> scaleEstimations,
                                            ArrayList<ShapeEstimation> shapeEstimations)
    {
        this.scaleEstimationByClass.addEstimations(classNumber, scaleEstimations);
        this.shapeEstimationByClass.addEstimations(classNumber, shapeEstimations);
        if(this.countDownLatch != null) {
            this.countDownLatch.countDown();
        }
    }

    public synchronized void notifyFinishedImage(String identifier, ScaleEstimation scaleEstimation)
    {
        this.scaleEstimationByImage.addEstimation(identifier, scaleEstimation);
    }

    public synchronized void notifyFinished()
    {
        if(this.countDownLatch != null) {
            this.countDownLatch.countDown();
        }
    }

    /**
     * This is used to actually perform the work. We might want to run this
     * multithreaded in the parameterEstimation() of the feature extraction
     * method.
     * <p>
     */
    public class ScalespaceScaleEstimatorThread
            implements Runnable
    {

        private double[][] data;
        private final ScalespaceParameters scalespaceParameters;
        private DataReader reader;
        private boolean isByClass = false;

        public ScalespaceScaleEstimatorThread(DataReader reader,
                                              ScalespaceParameters scalespaceParameters,
                                              boolean estimateByClass)
        {
            this.scalespaceParameters = scalespaceParameters;
            this.reader = reader;
            this.isByClass = estimateByClass;

        }

        public ScalespaceScaleEstimatorThread(double[][] data, ScalespaceParameters scalespaceParameters)
        {
            this.data = data;
            this.scalespaceParameters = scalespaceParameters;
        }

        // XXX: currently estimated only scale, not shape!
        @Override
        public void run()
        {
            try {
            ArrayList<ScaleEstimation> estimates = new ArrayList<>();

            while(this.reader.hasNext()) {
                DataSource source = (DataSource) this.reader.next();
                ContentStream streamFromSource = source.createContentStream();
                while(streamFromSource.hasNext()) {
                    Frame frame = streamFromSource.next();
                    ScaleEstimation scaleEstimate;
                    ShapeEstimation shapeEstimate = null;
                    try {
                        ScalespaceRepresentation scaleRep
                                                 = new LaplacianScalespaceRepresentation(frame.getGrayData(), this.scalespaceParameters);
                        scaleRep.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_ABSOLUTE_SUM);

                        scaleEstimate = new ScaleEstimation(this.scalespaceParameters,
                                                            scaleRep.getNormalizedLaplacianDataSupplier(),
                                                            frame.getWidth(), frame.getHeight());
                        scaleEstimate.estimateGlobalScale();

                        if(this.isByClass == false) {
                            ScalespaceScaleAndShapeEstimator.this.notifyFinishedImage(frame.getSignalIdentifier(), scaleEstimate);
                        } else {
                            estimates.add(scaleEstimate);
                        }
                    } catch(JCeliacGenericException e) {
                        Experiment.log(JCeliacLogger.LOG_WARN, "Scale estimator thread didn't finish correctly: " + e.getMessage());
                        // XXX: We should change this to use the ExecutorCompletionService mechanism at some time.  
                    }
                }
            }
            if(this.isByClass == false) {
                ScalespaceScaleAndShapeEstimator.this.notifyFinished();
            } else {
                ScalespaceScaleAndShapeEstimator.this.notifyFinished(this.reader.getClassNumber(), estimates);
            }
            }catch(JCeliacGenericException e) {
                  Experiment.log(JCeliacLogger.LOG_WARN, "Scale estimations failed: " + e.getMessage());
            }
        }

        public ScaleEstimation runEstimation()
        {
            ScaleEstimation scaleEstimate = null;
            try {
                ScalespaceRepresentation scaleRep
                                         = new LaplacianScalespaceRepresentation(this.data, this.scalespaceParameters);
                scaleRep.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_ABSOLUTE_SUM);
            } catch(JCeliacGenericException e) {
                Experiment.log(JCeliacLogger.LOG_WARN, "Scale Estimation failed: " + e.getMessage());
                // XXX: We should change this to use the ExecutorCompletionService mechanism at some time.  
            }
            return scaleEstimate;
        }

    }

}
