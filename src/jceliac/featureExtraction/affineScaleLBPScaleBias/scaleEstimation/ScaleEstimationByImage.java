/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation;

import java.util.HashMap;
import java.util.Set;

/**
 * Represents the scale estimated by image.
 * <p>
 * @author shegen
 */
public class ScaleEstimationByImage
{

    protected HashMap<String, ScaleEstimation> scaleEstimationStorage;

    public ScaleEstimationByImage()
    {
        this.scaleEstimationStorage = new HashMap<>();
    }

    public void addEstimation(String identifier, ScaleEstimation estimation)
    {
        this.scaleEstimationStorage.put(identifier, estimation);
    }

    public ScaleEstimation getEstimationForImage(String identifier)
    {
        return this.scaleEstimationStorage.get(identifier);
    }

    public Set<String> getKeys()
    {
        return this.scaleEstimationStorage.keySet();
    }
}
