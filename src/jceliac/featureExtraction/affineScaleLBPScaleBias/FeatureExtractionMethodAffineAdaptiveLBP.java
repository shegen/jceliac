/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBPScaleBias;

import jceliac.featureExtraction.RotationAdaptiveLBPFeatures;
import jceliac.featureExtraction.SALBP.LBPScaleFilteredDataSupplier;
import jceliac.featureExtraction.SALBP.LBPScaleBias;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.OrientationEstimation;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ScaleEstimation;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.tools.filter.scalespace.LaplacianScalespaceRepresentation;
import jceliac.featureExtraction.affineScaleLBP.EllipticPointLookup;
import jceliac.tools.filter.scalespace.ScalespaceStructureTensor;
import jceliac.featureExtraction.LBP.FeatureExtractionMethodLBP;
import jceliac.tools.filter.scalespace.ScalespaceRepresentation;
import jceliac.featureExtraction.affineScaleLBP.EllipticPoint;
import jceliac.tools.filter.scalespace.ScalespaceParameters;
import jceliac.experiment.FeatureExtractionMethodFactory;
import jceliac.featureExtraction.affineScaleLBP.Ellipse;
import jceliac.tools.interpolator.BilinearInterpolator;
import jceliac.featureExtraction.LBP.LBPHistogram;
import jceliac.featureExtraction.ExperimentalCode;
import jceliac.featureExtraction.LBP.LBPFeatures;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.JCeliacGenericException;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.Frame;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The most recent affine adaptive lbp implementation, some issues with the
 * ellipse normalization and orientation are still open, this code should not be
 * used in it's current state.
 * <p>
 * @author shegen
 */
@ExperimentalCode(
        comment = "Some issues with the ellipse normalization and orientation are still open, do not use!",
        date = "20.01.2014",
        lastModified = "20.01.2014")
public class FeatureExtractionMethodAffineAdaptiveLBP
        extends FeatureExtractionMethodLBP
{

    private final FeatureExtractionParametersAffineAdaptiveLBP myParameters;
    private final HashMap<Integer, LBPScaleBias> scaleBiasPerClass;
    private final ScalespaceParameters scalespaceParameters;
    private final EllipticPointLookup lookup;

    public FeatureExtractionMethodAffineAdaptiveLBP(FeatureExtractionParameters param)
            throws JCeliacGenericException
    {
        super(param);

        /* The scalespace is constructed such that the middle scale
         * sqrt(2)^0*c*sqrt(2) is 3 pixels wide. We define the scale bias to be
         * 3 pixels wide.
         */
        this.scalespaceParameters = new ScalespaceParameters(Math.sqrt(2.0d),
                                                             -4,
                                                             8,
                                                             0.25,
                                                             2.1214);

        // compute cos and sin lookup tables for scalespace paramters
        this.myParameters = (FeatureExtractionParametersAffineAdaptiveLBP) param;
        this.scaleBiasPerClass = new HashMap<>();
        this.lookup = EllipticPointLookup.create();
    }

    private DiscriminativeFeatures doExtractFeatures(Frame content)
            throws JCeliacGenericException
    {
        
        LBPScaleFilteredDataSupplier dataSupplier;
        FeatureExtractionParameters p = this.myParameters.createParametersForSubmethod();
        IAffineAdaptiveFeatureExtractionMethod subMethod = (IAffineAdaptiveFeatureExtractionMethod) FeatureExtractionMethodFactory.createFeatureExtractionMethod(this.myParameters.getExtractionMethod(), p);
        dataSupplier = new LBPScaleFilteredDataSupplier(content.getGrayData());

        // now estimate the scales of the input data
        ScalespaceRepresentation scaleRep = new LaplacianScalespaceRepresentation(content.getGrayData(), this.scalespaceParameters);
        scaleRep.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_SUM);
        scaleRep.computeMaxima();
        ScalespaceStructureTensor structureTensors = new ScalespaceStructureTensor(scaleRep);

        ScaleEstimation localScaleEstimate = new ScaleEstimation(this.scalespaceParameters,
                                                                 scaleRep.getNormalizedLaplacianDataSupplier(),
                                                                 content.getWidth(), content.getHeight());
        localScaleEstimate.estimateGlobalScale();
        OrientationEstimation localOrientationEstimation = new OrientationEstimation();
        localOrientationEstimation.estimateGlobalOrientation(scaleRep, localScaleEstimate.getEstimatedScaleLevel());

        RotationAdaptiveLBPFeatures rotFeatures = new RotationAdaptiveLBPFeatures(this.myParameters);

        double rotationSigma = localOrientationEstimation.getOrientationSigma();

        if(rotationSigma > 20) {
            rotationSigma = 0;
        }
        double off = rotationSigma / 20;
        for(double angleOff = -off; angleOff <= off; angleOff += 0.0873) {
            LBPHistogram histogram1 = this.performAffineAdaptiveFeatureExtraction(subMethod, dataSupplier,
                                                                                  structureTensors, localOrientationEstimation,
                                                                                  1.5d, angleOff);

            LBPHistogram histogram2 = this.performAffineAdaptiveFeatureExtraction(subMethod, dataSupplier,
                                                                                  structureTensors, localOrientationEstimation,
                                                                                  3d, angleOff);

            LBPHistogram histogram3 = this.performAffineAdaptiveFeatureExtraction(subMethod, dataSupplier,
                                                                                  structureTensors, localOrientationEstimation,
                                                                                  4.5d, angleOff);

            LBPFeatures featureForAngleOffset = new LBPFeatures(this.myParameters);
            featureForAngleOffset.addAbstractFeature(histogram1);
            featureForAngleOffset.addAbstractFeature(histogram2);
            featureForAngleOffset.addAbstractFeature(histogram3);
            
            rotFeatures.addRotatedFeature(featureForAngleOffset);

        }
        rotFeatures.setFeatureExtractionParameters(this.myParameters);
        rotFeatures.setSignalIdentifier(content.getSignalIdentifier());
        rotFeatures.setFrameNumber(content.getFrameIdentifier());

        return rotFeatures;
    }

    private LBPHistogram performAffineAdaptiveFeatureExtraction(IAffineAdaptiveFeatureExtractionMethod subMethod,
                                                                LBPScaleFilteredDataSupplier dataSupplier,
                                                                ScalespaceStructureTensor structureTensors,
                                                                OrientationEstimation orientationEstimation,
                                                                double lbpRadius,
                                                                double angleOffset)
            throws JCeliacGenericException
    {

        AffineFilteredNeighbor center = new AffineFilteredNeighbor();
        AffineFilteredNeighbor[] neighbors = new AffineFilteredNeighbor[8];
        for(int i = 0; i < neighbors.length; i++) {
            neighbors[i] = new AffineFilteredNeighbor();
        }

        Ellipse ell = new Ellipse();

        double c = (lbpRadius / 0.94) - lbpRadius;
        ell.a = lbpRadius;
        ell.b = lbpRadius + c;
        ell.phi = Math.PI * 2 + orientationEstimation.getDominantOrientation() + angleOffset;
        EllipticPoint[] neighborPoints = this.lookup.lookupOld(0, 0, ell.a, ell.b, ell.phi);

        LBPHistogram histogram = subMethod.initializeHistogram();
        double[][] data = dataSupplier.retrieveLBPScaleFilteredDataForLBPRadius(lbpRadius);
        for(int x = 2; x < structureTensors.getWidth() - 2; x++) {
            for(int y = 2; y < structureTensors.getHeight() - 2; y++) {
                try {
                    this.collectNeighbors(data, x, y, neighborPoints, center, neighbors);
                    //dataSupplier.retrieveUniformFilteredNeighborsForLBPRadius(x, y, neighborPoints,
                    //       lbpRadius, neighbors, center);

                    subMethod.computeAffineAdaptivePattern(histogram, center, neighbors, 25, 1.0d);

                } catch(Exception e) {
                }
            }
        }
        subMethod.finalizeHistogram(histogram);
        return histogram;
    }

    private void collectNeighbors(double[][] data, int x, int y, EllipticPoint[] points,
                                  AffineFilteredNeighbor center, AffineFilteredNeighbor[] neighbors)
    {
        BilinearInterpolator interp = new BilinearInterpolator();
        center.value = interp.interpolate(data, x, y);
        center.distanceToCenter = 0;

        for(int p = 0; p < 8; p++) {
            double coordX, coordY;
            coordX = points[p].x + x;
            coordY = points[p].y + y;
            double value = interp.interpolate(data, coordX, coordY);
            neighbors[p].value = value;
            neighbors[p].position = p;
            neighbors[p].distanceToCenter = Math.sqrt((coordX - x) * (coordX - x)) + ((coordY - y) * (coordY - y));
        }
    }

    @Override
    @SuppressWarnings("UnusedAssignment")
    public DiscriminativeFeatures extractTrainingFeatures(Frame content, int classIndex)
            throws JCeliacGenericException
    {
        return this.doExtractFeatures(content);
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {
        return this.doExtractFeatures(content);
    }

    protected double getNeighbor(double[][] data, int x, int y, int p, double radius)
    {
        double nx, ny;

        ny = (double) y - radius * Math.sin((2.0d * Math.PI * (double) (p)) / (double) 8.0d);
        nx = (double) x + radius * Math.cos((2.0d * Math.PI * (double) (p)) / (double) 8.0d);
        try {
            return getInterpolatedPixelValueFast(data, nx, ny);
        } catch(Exception e) {
            return -1;

        }
    }

    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass)
            throws JCeliacGenericException
    {
    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {
        // nothing to do
    }
}
