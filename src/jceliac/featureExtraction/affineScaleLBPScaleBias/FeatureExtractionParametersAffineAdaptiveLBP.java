/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.affineScaleLBPScaleBias;

import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.featureExtraction.FLBP.FeatureExtractionParametersFLBP;
import jceliac.featureExtraction.LBP.FeatureExtractionParametersLBP;
import jceliac.featureExtraction.LTP.FeatureExtractionParametersLTP;

public class FeatureExtractionParametersAffineAdaptiveLBP
        extends FeatureExtractionParametersLBP
{

    protected String extractionMethod = "LTP";

    @Override
    public String getMethodName()
    {
        return "AALBP (" + this.extractionMethod + ")";
    }

    public FeatureExtractionMethod.EFeatureExtractionMethod getExtractionMethod()
            throws JCeliacGenericException
    {
        return FeatureExtractionMethod.EFeatureExtractionMethod.valueOfIgnoreCase(this.extractionMethod);
    }

    public void setExtractionMethod(String extractionMethod)
    {
        this.extractionMethod = extractionMethod;
    }

    /**
     * This is used by the scale and orientation adaptive framework to create
     * standard parameters for the used sub-methods.
     * <p>
     * @return Default parameters for a sub-method.
     * <p>
     * @throws JCeliacGenericException
     */
    public FeatureExtractionParametersLBP createParametersForSubmethod()
            throws JCeliacGenericException
    {
        FeatureExtractionParametersLBP params;
        switch(this.getExtractionMethod()) {
            case LTP:
                params = new FeatureExtractionParametersLTP();
                break;
            case LBP:
                params = new FeatureExtractionParametersLBP();
                break;
            case FLBP:
                params = new FeatureExtractionParametersFLBP();
                break;
            default:
                throw new JCeliacGenericException("Submethod not support yet: " + this.getExtractionMethod().getStringValue());
        }
        params.setMaxScale(this.maxScale);
        params.setMinScale(this.minScale);
        params.setNumberOfNeighbors(this.numberOfNeighbors);
        params.setUseUniformPatterns(this.useUniformPatterns);
        params.setUseRotationalInvariance(this.useRotationalInvariance);
        return params;
    }

    @Override
    public void report()
    {
        super.report(); 
    }
    
    
}
