/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction;

import java.util.*;
import java.io.*;
import jceliac.JCeliacGenericException;
import jceliac.classification.classificationParameters.ClassificationParametersKNN.EDistanceType;
import jceliac.experiment.Experiment;
import jceliac.logging.*;

/**
 * Provides method to export features for matlab code.
 * <p>
 * @author shegen
 */
public class FeatureExport
{

    private final ArrayList<DiscriminativeFeatures> exportedFeatures;

    public FeatureExport()
    {
        this.exportedFeatures = new ArrayList<>();
    }

    public void addFeatures(ArrayList<DiscriminativeFeatures> features)
    {
        this.exportedFeatures.addAll(features);
    }

    /**
     * Exports the code usind the format {x1, x2, x3; x4, x5, x6;}.
     * <p>
     * @param exportFile Filename to write features to.
     * <p>
     * @throws JCeliacGenericException
     */
    public void exportFeatures(String exportFile) throws JCeliacGenericException
    {
        double[][] distanceMatrix = computeDistanceMatrix();
        StringBuilder export = new StringBuilder();
        export.append("\n");
        for(double[] tmpMatrix : distanceMatrix) {
            for(int j = 0; j < distanceMatrix[0].length; j++) {
                export.append(tmpMatrix[j]);
                export.append(",");
            }
            export.append(";\n");
        }
        export.append("\n");
        try(BufferedWriter outWriter = new BufferedWriter(new FileWriter(exportFile))) {
            outWriter.write(export.toString());
            outWriter.close();
        } catch(IOException e) {
            throw new JCeliacGenericException(e);
        }

    }

    /**
     * Computes a euclidean distance between all pairs of features provided.
     * <p>
     * @return A square matrix of pairwise feature distances.
     * <p>
     * @throws JCeliacGenericException
     */
    private double[][] computeDistanceMatrix() throws JCeliacGenericException
    {
        double[][] distanceMatrix = new double[this.exportedFeatures.size()][this.exportedFeatures.size()];

        // turn of warning for computing the distances, as the code might interpret this as overfitting
        // because distances between the same samples are calculated
        this.disableLoggerWarnings();

        for(int i = 0; i < distanceMatrix.length; i++) {
            for(int j = 0; j < distanceMatrix[0].length; j++) {
                DiscriminativeFeatures tmpi = this.exportedFeatures.get(i);
                DiscriminativeFeatures tmpj = this.exportedFeatures.get(j);
                distanceMatrix[i][j] = tmpi.distanceTo(tmpj);
            }
        }
        // reactivate the logging
        this.enableLoggerWarnings();
        return distanceMatrix;
    }

    /**
     * Disable logger to avoid warnings.
     */
    private void disableLoggerWarnings()
    {
        JCeliacLogger[] loggers = Experiment.getLogger();
        for(JCeliacLogger logger : loggers) {
            logger.removeLogLevel(JCeliacLogger.LOG_WARN);
        }
    }

    /**
     * Enable logger.
     */
    private void enableLoggerWarnings()
    {
        JCeliacLogger[] loggers = Experiment.getLogger();
        for(JCeliacLogger logger : loggers) {
            logger.addLogLevel(JCeliacLogger.LOG_WARN);
        }
    }

}
