/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction;

import jceliac.JCeliacGenericException;
import jceliac.featureOptimization.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Low level feature vector representation, holding the raw data of possibly multiple
 * sub-features.
 * <p>
 * @author shegen
 */
public class DynamicFeatureVector 
 
        
{

    private final ArrayList<double[]> featureVector;
    private final ConcurrentHashMap<Integer, double[]> subsetCache;
    private final int MAX_CACHE_SIZE = 1;
    private int iteratorIndex = 0;

    public DynamicFeatureVector()
    {
        this.featureVector = new ArrayList<>();
        this.subsetCache = new ConcurrentHashMap<>();
    }

    /**
     * Returns the dimensionality, this can either be the number of histograms
     * or the dimension of a single feature vector and depends on the feature
     * extraction method.
     * <p>
     * @return Dimension of feature vector
     */
 
    public int getFeatureVectorDimensionality()
    {
        return this.featureVector.size();
    }

    

    
    /**
     * Computes the raw features as a double array from a possibly set of
     * feature vectors based on the requested feature subset.
     * <p>
     * @param subset Requested feature subset, if null the entire feature vector
     *               is returned.
     * <p>
     * @return A double array containing the raw feature vector
     * <p>
     * @throws JCeliacGenericException
     */
 
    public synchronized double[] getFeatureVectorSubsetData(FeatureVectorSubsetEncoding subset)
            throws JCeliacGenericException
    {
        FeatureVectorSubsetEncoding localSubset = subset;
        if(localSubset == null) { // return the entire feature vector
            localSubset = new FeatureVectorSubsetEncoding(this.getFeatureVectorDimensionality());
            localSubset.setAll();
        }
        if(this.subsetCache.containsKey(localSubset.hashCode())) {
            double[] cachedHist = this.subsetCache.get(localSubset.hashCode());
            return cachedHist;
        }
        // build feature Vector
        int len = 0; // it's possible that single features have different dimensionality
        for(int index = 0; index < this.featureVector.size(); index++) {
            if(localSubset.isSet(index)) {
                len += this.featureVector.get(index).length;
            }
        }
        // bad feature subset requested
        if(len == 0) { 
             throw new JCeliacGenericException("Bad Feature Subset Requested!");
        }
        double[] localArray = new double[len];
        int pos = 0;
        for(int index = 0; index < this.featureVector.size(); index++) {
            if(localSubset.isSet(index)) {
                double[] feature = this.featureVector.get(index);
                System.arraycopy(feature, 0, localArray, pos, feature.length);
                pos += feature.length;
            }
        }
        // if the cache has size put it in the cache, else clear the cache
        if(this.subsetCache.size() > MAX_CACHE_SIZE) {
            this.subsetCache.clear();
        }
        double[] clonedArray = localArray.clone();
        this.subsetCache.put(localSubset.hashCode(), clonedArray);
        return localArray;
    }

   /**
     * Computes the raw features as a double array from all featuer data. 
     * <p>
     * @return A double array containing the raw feature vector
     * <p>
     * @throws JCeliacGenericException
     */

    public synchronized double[] getAllFeatureData()
            throws JCeliacGenericException
    {
        return this.getFeatureVectorSubsetData(null);
    }
    
    /**
     * This adds a single feature vector to the collection of feature vectors.
     * <p>
     * @param feature
     */

    public synchronized void addSingleFeature(final double[] feature)
    {
        this.subsetCache.clear();
        this.featureVector.add(feature);
    }
    
 
    public synchronized void setFeature(final double [] feature) {
        this.subsetCache.clear();
        this.featureVector.clear();
        this.featureVector.add(feature);
    }

    public boolean hasNext()
    {
        return (this.iteratorIndex < this.featureVector.size());
    }


    public double[] next()
    {
        double[] fVec = this.featureVector.get(this.iteratorIndex);
        this.iteratorIndex++;
        return fVec;
    }


    public void remove()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

   

   /* @Override
    protected Object clone() throws CloneNotSupportedException
    {
        super.clone();
        DynamicFeatureVector cloned = new DynamicFeatureVector();

        // arraylist performs a shallow copy, hence clone all feature vectors
        for(double[] vec : featureVector) {
            cloned.addSingleFeature(vec.clone());
        }
        return cloned;
    }*/
    

}
