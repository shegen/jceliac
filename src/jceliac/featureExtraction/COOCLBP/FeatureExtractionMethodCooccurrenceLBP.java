/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.COOCLBP;

import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.*;
import jceliac.tools.data.Frame;
import jceliac.tools.filter.*;
import jceliac.experiment.*;
import jceliac.featureExtraction.LBP.FeatureExtractionMethodLBP;
import jceliac.featureExtraction.LBP.LBPFeatures;
import jceliac.featureExtraction.LBP.LBPHistogram;
import jceliac.featureExtraction.LBP.LBPMultiscale;
import jceliac.tools.data.*;
import jceliac.logging.*;

/**
 * Coocurrence Local Binary Patterns.
 * <p>
 * @author shegen
 */
@ExperimentalCode(comment = "Code is executable but has not been thorougly tested.", 
                  date = "16.01.2014", 
                  lastModified = "16.01.2014")
public class FeatureExtractionMethodCooccurrenceLBP
        extends FeatureExtractionMethodLBP
{

    private final FeatureExtractionParametersCOOCLBP myParameters;

    public FeatureExtractionMethodCooccurrenceLBP(FeatureExtractionParameters param)
    {
        super(param);

        // this is just for convenience
        this.myParameters = (FeatureExtractionParametersCOOCLBP) super.parameters;
    }

    @Override
    public void initialize()
            throws JCeliacGenericException
    {
        super.initialize();
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {

        Experiment.log(JCeliacLogger.LOG_HINFO, "Extracting COOCLBP-Features from SignalID: %s, FrameID: %d.",
                       content.getSignalIdentifier(), content.getFrameIdentifier());

        // create DiscriminativeFeatures structure once for each input sample
        LBPFeatures features = new LBPFeatures(this.myParameters);
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());

        for(int currentScale = myParameters.getMinScale(); currentScale <= myParameters.getMaxScale();
            currentScale++) {

            if(myParameters.isUseColor()) {
                // Prepare content for scale.

                LBPHistogram histRedGreen = computeDistribution(content, DataSource.CHANNEL_RED, DataSource.CHANNEL_GREEN, currentScale);
                LBPHistogram histRedBlue = computeDistribution(content, DataSource.CHANNEL_RED, DataSource.CHANNEL_BLUE, currentScale);
                LBPHistogram histGreenBlue = computeDistribution(content, DataSource.CHANNEL_GREEN, DataSource.CHANNEL_BLUE, currentScale);

                features.addAbstractFeature(histRedGreen);
                features.addAbstractFeature(histRedBlue);
                features.addAbstractFeature(histGreenBlue);

            } else {
                throw new JCeliacGenericException("COOCLBP cannot used in grayscale mode!");
            }
        }
        return features;
    }

    protected LBPHistogram computeDistribution(Frame content, int colorChannel1, int colorChannel2, int scale)
            throws JCeliacGenericException
    {
        LBPHistogram lbpHistogram;

        double[][] data1;
        double[][] data2;
        double radius = LBPMultiscale.getRadiusForScale(scale);
        int neighbors = LBPMultiscale.getNeighborsForScale(scale);
        int margin = (int) Math.ceil(radius);

        DataFilter2D filter = LBPMultiscale.getFilterForScale(scale);
        switch(colorChannel1) {
            case DataSource.CHANNEL_GREEN:
                data1 = filter.filterData(content.getGreenData());
                break;

            case DataSource.CHANNEL_RED:
                data1 = filter.filterData(content.getRedData());
                break;

            case DataSource.CHANNEL_BLUE:
                data1 = filter.filterData(content.getBlueData());
                break;

            case DataSource.CHANNEL_GRAY:
                data1 = filter.filterData(content.getGrayData());
                break;

            default:
                Experiment.log(JCeliacLogger.LOG_ERROR, "Unknown Color Channel encountered: %d!", colorChannel1);
                throw new JCeliacGenericException("Unknown Color Channel encountered");

        }
        switch(colorChannel2) {
            case DataSource.CHANNEL_GREEN:
                data2 = filter.filterData(content.getGreenData());
                break;

            case DataSource.CHANNEL_RED:
                data2 = filter.filterData(content.getRedData());
                break;

            case DataSource.CHANNEL_BLUE:
                data2 = filter.filterData(content.getBlueData());
                break;

            case DataSource.CHANNEL_GRAY:
                data2 = filter.filterData(content.getGrayData());
                break;

            default:
                Experiment.log(JCeliacLogger.LOG_ERROR, "Unknown Color Channel encountered: %d!", colorChannel2);
                throw new JCeliacGenericException("Unknown Color Channel encountered");

        }
        lbpHistogram = computeHistogram(data1, data2, neighbors, radius, margin, scale);
        return lbpHistogram;
    }

    protected LBPHistogram computeHistogram(double[][] data1, double[][] data2, int neighbors,
                                            double radius, int margin, int scale)
    {
        double[][] coocMatrix = new double[256][256];

        int shiftMax = neighbors - 1;
        for(int x = margin; x < data1.length - margin; x++) {
            for(int y = margin; y < data1[0].length - margin; y++) {

                double center1 = (double) data1[x][y];
                double center2 = (double) data2[x][y];
                int pattern1 = 0;
                int pattern2 = 0;

                for(int p = 0; p < neighbors; p++) {
                    double neighbor1 = getNeighbor(data1, x, y, p, scale);
                    double neighbor2 = getNeighbor(data2, x, y, p, scale);

                    if(neighbor1 >= center1) {
                        // this is in reverse order from the matlab thingy to speed up things
                        pattern1 |= (1 << (shiftMax - p));
                    }
                    if(neighbor2 >= center2) {
                        // this is in reverse order from the matlab thingy to speed up things
                        pattern2 |= (1 << (shiftMax - p));
                    }
                }
                if(pattern1 == pattern2) {
                    coocMatrix[pattern1][pattern2]++;
                }
            }
        }
        double[] matrix1D = new double[coocMatrix.length * coocMatrix[0].length];
        int destPos = 0;
        for(double[] tmpCoocMatrix : coocMatrix) {
            System.arraycopy(tmpCoocMatrix, 0, matrix1D, destPos, tmpCoocMatrix.length);
            destPos += tmpCoocMatrix.length;
        }
        LBPHistogram histogram = new LBPHistogram();
        histogram.setData(matrix1D);
        return histogram;
    }
}
