/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.COOCLBP;

import jceliac.featureExtraction.LBP.FeatureExtractionParametersLBP;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersCOOCLBP
        extends FeatureExtractionParametersLBP
{

    @Override
    public void report()
    {
        super.report();

    }
    
    @Override
    public String getMethodName()
    {
        return "COOCLBP";
    }

}
