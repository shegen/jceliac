/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.COOCLBP;

import jceliac.experiment.*;
import jceliac.featureExtraction.FLBP.FeatureExtractionParametersFLBP;
import jceliac.logging.*;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersCOOCFLBP
        extends FeatureExtractionParametersFLBP
{

    public static enum COOCModes
    {

        /**
         * Color mode computes co-occurrence between color channels.
         */
        COLOR_MODE
    }

    /**
     * The mode of the COOCFLBP. We support: - COLOR: Color mode computes
     * co-occurrence between color channels
     */
    protected COOCModes mode = COOCModes.COLOR_MODE;

    public COOCModes getMode()
    {
        return mode;
    }

    public void setMode(String mode)
    {
        if(mode.equalsIgnoreCase("COLOR")) {
            this.mode = COOCModes.COLOR_MODE;
        }
    }

    @Override
    public void report()
    {
        super.report();
        switch(mode) {
            case COLOR_MODE:
                Experiment.log(JCeliacLogger.LOG_INFO, "COOC-Mode: %s", "COLOR_MODE");
                break;
        }

    }

    @Override
    public String getMethodName()
    {
        return "COOCFLBP";
    }

}
