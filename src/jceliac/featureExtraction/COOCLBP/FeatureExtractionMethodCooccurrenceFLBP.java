/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.COOCLBP;

import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.LBP.*;
import jceliac.tools.filter.*;
import jceliac.tools.data.*;
import jceliac.experiment.*;
import jceliac.featureExtraction.ExperimentalCode;
import jceliac.featureExtraction.FLBP.FeatureExtractionMethodFLBP;
import jceliac.logging.*;

/**
 * Feature Extraction Method Coocurrence FLBP.
 * <p>
 * @author shegen
 */
@ExperimentalCode(
        comment = "Code is executable but has not been thorougly tested.",
        date = "16.01.2014",
        lastModified = "16.01.2014")
public class FeatureExtractionMethodCooccurrenceFLBP
        extends FeatureExtractionMethodFLBP
{

    private final FeatureExtractionParametersCOOCFLBP myParameters;
    private final boolean[] uniformPatternsLUT;

    public FeatureExtractionMethodCooccurrenceFLBP(FeatureExtractionParameters param)
    {
        super(param);
        myParameters = (FeatureExtractionParametersCOOCFLBP) param;
        uniformPatternsLUT = LBPHistogram.getUniformLUT((int) Math.pow(2, myParameters.getNumberOfNeighbors()));
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {

        Experiment.log(JCeliacLogger.LOG_HINFO, "Extracting COOCFLBP-Features from SignalID: %s, FrameID: %d.",
                       content.getSignalIdentifier(), content.getFrameIdentifier());

        // create DiscriminativeFeatures structure once for each input sample
        LBPFeatures features = new LBPFeatures(this.myParameters);
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());

        for(int currentScale = myParameters.getMinScale(); currentScale <= myParameters.getMaxScale();
            currentScale++) {
            if(this.myParameters.isUseColor()) {
                
                // Prepare content for scale.
                LBPHistogram histRedGreen = computeDistributionColorMode(content, DataSource.CHANNEL_RED, DataSource.CHANNEL_GREEN, currentScale);
                LBPHistogram histRedBlue = computeDistributionColorMode(content, DataSource.CHANNEL_RED, DataSource.CHANNEL_BLUE, currentScale);
                LBPHistogram histGreenBlue = computeDistributionColorMode(content, DataSource.CHANNEL_GREEN, DataSource.CHANNEL_BLUE, currentScale);

                features.addAbstractFeature(histRedGreen);
                features.addAbstractFeature(histRedBlue);
                features.addAbstractFeature(histGreenBlue);
                break;

            } else {
                throw new JCeliacGenericException("COOCLBP cannot used in grayscale mode!");
            }
        }
        return features;
    }

    protected LBPHistogram computeDistributionColorMode(Frame content, int colorChannel1,
                                                        int colorChannel2, int scale)
            throws JCeliacGenericException
    {
        LBPHistogram lbpHistogram;

        double[][] data1;
        double[][] data2;
        double radius = LBPMultiscale.getRadiusForScale(scale);
        int neighbors = LBPMultiscale.getNeighborsForScale(scale);
        int margin = (int) Math.ceil(radius);

        DataFilter2D filter = LBPMultiscale.getFilterForScale(scale);
        switch(colorChannel1) {
            case DataSource.CHANNEL_GREEN:
                data1 = filter.filterData(content.getGreenData());
                break;

            case DataSource.CHANNEL_RED:
                data1 = filter.filterData(content.getRedData());
                break;

            case DataSource.CHANNEL_BLUE:
                data1 = filter.filterData(content.getBlueData());
                break;

            case DataSource.CHANNEL_GRAY:
                data1 = filter.filterData(content.getGrayData());
                break;

            default:
                Experiment.log(JCeliacLogger.LOG_ERROR, "Unknown Color Channel encountered: %d!", colorChannel1);
                throw new JCeliacGenericException("Unknown Color Channel encountered");

        }
        switch(colorChannel2) {
            case DataSource.CHANNEL_GREEN:
                data2 = filter.filterData(content.getGreenData());
                break;

            case DataSource.CHANNEL_RED:
                data2 = filter.filterData(content.getRedData());
                break;

            case DataSource.CHANNEL_BLUE:
                data2 = filter.filterData(content.getBlueData());
                break;

            case DataSource.CHANNEL_GRAY:
                data2 = filter.filterData(content.getGrayData());
                break;

            default:
                Experiment.log(JCeliacLogger.LOG_ERROR, "Unknown Color Channel encountered: %d!", colorChannel2);
                throw new JCeliacGenericException("Unknown Color Channel encountered");

        }
        lbpHistogram = computeHistogramColorMode(data1, data2, neighbors, radius, margin, scale);
        return lbpHistogram;
    }

    protected LBPHistogram computeHistogramColorMode(double[][] data1, double[][] data2, int neighbors, double radius, int margin, int scale)
    {
        double[] hist = new double[getNumberOfBins(neighbors)];

        for(int x = margin; x < data1.length - margin; x++) {
            for(int y = margin; y < data1[0].length - margin; y++) {

                double center1 = (double) data1[x][y];
                double center2 = (double) data2[x][y];

                // collect neighbors
                double[] neighborsArray1 = new double[neighbors];
                double[] neighborsArray2 = new double[neighbors];

                for(int p = 0; p < neighbors; p++) {
                    double neighbor1 = getNeighbor(data1, x, y, p, scale);
                    double neighbor2 = getNeighbor(data2, x, y, p, scale);
                    neighborsArray1[p] = neighbor1;
                    neighborsArray2[p] = neighbor2;
                }

                double[] contributions1 = new double[getNumberOfBins(neighbors)];
                double[] contributions2 = new double[getNumberOfBins(neighbors)];

                for(int lbpCode = 0; lbpCode < contributions1.length; lbpCode++) {
                    if(uniformPatternsLUT[lbpCode]) {
                        contributions1[lbpCode] = calculateContributionOfPattern(lbpCode, neighborsArray1, center1, THRESHOLD);
                        contributions2[lbpCode] = calculateContributionOfPattern(lbpCode, neighborsArray2, center2, THRESHOLD);

                        // if there is no noise, the contributions should be almost equal;
                        // the blue channel usually has the most noise (bayer pattern)
                        hist[lbpCode] += Math.min(contributions1[lbpCode], contributions2[lbpCode]);

                        //contributions1[lbpCode] + contributions2[lbpCode]) / 2;
                    }
                }
            }
        }
        LBPHistogram lbpHistogram = new LBPHistogram();
        lbpHistogram.setData(hist);
        return lbpHistogram;
    }
}
