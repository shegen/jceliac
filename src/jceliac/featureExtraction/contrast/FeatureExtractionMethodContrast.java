/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.contrast;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.AbstractFeatureVector;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FixedFeatureVector;
import jceliac.featureExtraction.StandardEuclideanFeatures;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.Frame;

/**
 *
 * @author shegen
 */
public class FeatureExtractionMethodContrast
        extends FeatureExtractionMethod
{

    protected FeatureExtractionParametersContrast myParameters;

    public FeatureExtractionMethodContrast(FeatureExtractionParameters param)
    {
        super(param);
        this.myParameters = (FeatureExtractionParametersContrast) param;
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {
        StandardEuclideanFeatures features = new StandardEuclideanFeatures();
        if(this.myParameters.isUseColor()) {
            for(Integer colorChannel : this.myParameters.getColorChannels())
            {
                AbstractFeatureVector vec = this.computeFeatures(content.getColorChannel(colorChannel));
                features.addAbstractFeature(vec);
            }
        }else {
            AbstractFeatureVector vecGray = this.computeFeatures(content.getGrayData());
            features.addAbstractFeature(vecGray);
        }
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());
        return features;
    }

    private FixedFeatureVector computeFeatures(double[][] data)
            throws JCeliacGenericException
    {
        int offsetX, offsetY;
        ArrayList<Double> featureData = new ArrayList<>();
        
        for(int sz = this.myParameters.getOffsetStartSize(); sz <= this.myParameters.getOffsetEndSize(); sz++) {
            for(int angle = 1; angle <= this.myParameters.getNumberOfAngles(); angle++) {
                switch(angle) {
                    case 1:
                        offsetX = 0;
                        offsetY = sz;
                        break;
                    case 2:
                        offsetX = sz;
                        offsetY = 0;
                        break;
                    case 3:
                        offsetX = sz;
                        offsetY = sz;
                        break;
                    case 4:
                        offsetX = -sz;
                        offsetY = sz;
                        break;
                    default:
                        throw new JCeliacGenericException("Invalid number of angles!");
                }
                double[][] glcms = this.computeGraylevelCoocurrenceMatrix(data, offsetX, offsetY);
                this.normalizeGrayscaleCoocurrenceMatrix(glcms);
                double contrast = this.computeContrast(glcms);
                featureData.add(contrast);
            }
        }
        double [] rawFeatureData = new double[featureData.size()];
        for(int i = 0; i < featureData.size(); i++) {
            rawFeatureData[i] = featureData.get(i);
        }
        FixedFeatureVector vec = new FixedFeatureVector();
        vec.setData(rawFeatureData);
        return vec;
    }

    private double[][] computeGraylevelCoocurrenceMatrix(double[][] data, int deltaX, int deltaY)
    {
        double[][] coocMatrix = new double[256][256];

        for(int p = 0; p < data.length; p++) {
            for(int q = 0; q < data[0].length; q++) {
                // GRAYCOMATRIX ignores border pixels, if the corresponding neighbors
                // defined by 'Offset' fall outside the image boundaries.
                if(p + deltaX >= data.length || p + deltaX < 0
                   || q + deltaY >= data[0].length || q + deltaY < 0) {
                    continue;
                }
                // symmetric behaviour of GRAYCOMATRIX
                int value1 = (int) data[p][q];
                int value2 = (int) data[p + deltaX][q + deltaY];
                coocMatrix[value1][value2]++;
                coocMatrix[value2][value1]++;
            }
        }
        return coocMatrix;
    }

    private void normalizeGrayscaleCoocurrenceMatrix(double[][] glcM)
    {
        int sum = 0;
        for(int i = 0; i < glcM.length; i++) {
            for(int j = 0; j < glcM[0].length; j++) {
                sum += glcM[i][j];
            }
        }
        for(int i = 0; i < glcM.length; i++) {
            for(int j = 0; j < glcM[0].length; j++) {
                glcM[i][j] = glcM[i][j] / sum;
            }
        }
    }

    private double computeContrast(double[][] glcM)
    {
        double contrast = 0;
        for(int i = 0; i < glcM.length; i++) {
            for(int j = 0; j < glcM[0].length; j++) {
                contrast += (i - j) * (i - j) * glcM[i][j];
            }
        }
        return contrast;
    }

    @Override
    public void initialize()
            throws JCeliacGenericException
    {
    }

    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass)
            throws JCeliacGenericException
    {
    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {
    }

}
