/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.featureExtraction.contrast;

import jceliac.experiment.Experiment;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersContrast 
    extends FeatureExtractionParameters
{

    protected int offsetStartSize = 4;
    protected int offsetEndSize = 4;
    protected int numberOfAngles = 4;
    
    @Override
    public void report()
    {
        super.report();
        Experiment.log(JCeliacLogger.LOG_INFO, "Offset start size: "+this.offsetStartSize);
        Experiment.log(JCeliacLogger.LOG_INFO, "Offset end size: "+this.offsetEndSize);
        Experiment.log(JCeliacLogger.LOG_INFO, "Number of angles: "+this.numberOfAngles);
    }

    
    @Override
    public String getMethodName()
    {
        return "Contrast";
    }

    public int getOffsetStartSize()
    {
        return offsetStartSize;
    }

    public int getOffsetEndSize()
    {
        return offsetEndSize;
    }

    public int getNumberOfAngles()
    {
        return numberOfAngles;
    }
    
    

}
