/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.HOG;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.AbstractFeatureVector;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FixedFeatureVector;
import jceliac.featureExtraction.StandardEuclideanFeatures;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.Frame;

/**
 * Port of the code of: 'Trainable Classifier-Fusion Schemes: An Application To Pedestrian Detection'.
 * 
 * @author shegen
 */
public class FeatureExtractionMethodHOG
    extends FeatureExtractionMethod
{

    protected FeatureExtractionParametersHOG myParameters;
    
    public FeatureExtractionMethodHOG(FeatureExtractionParameters param)
    {
        super(param);
        this.myParameters = (FeatureExtractionParametersHOG) param;
    }

    
    @Override
    public DiscriminativeFeatures extractFeatures(Frame content) throws
                                                          JCeliacGenericException
    {
        StandardEuclideanFeatures features = new StandardEuclideanFeatures();
        if(this.myParameters.isUseColor()) 
        {
            for(Integer colorChannel : this.myParameters.getColorChannels()) {
                AbstractFeatureVector vec = this.computeFeatures(content.getColorChannel(colorChannel));
                features.addAbstractFeature(vec);
            }
        }else {
            AbstractFeatureVector vecGray = this.computeFeatures(content.getGrayData());
            features.addAbstractFeature(vecGray);
        }
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());
        return features;
    }

    
    protected FixedFeatureVector computeFeatures(double [][] data)
    {
        ArrayList <double []> histData = new ArrayList <>();
        int stepX = (int)Math.floor(data.length / (this.myParameters.numWindowsX+1));
        int stepY = (int)Math.floor(data[0].length / (this.myParameters.numWindowsY+1));
        
        double [] filterX = new double[] {-1, 0, 1};
        double [] filterY = new double[] {1, 0, -1};
        double [][] gradX = this.correlate(data, filterX, 0);
        double [][] gradY = this.correlate(data, filterY, 1);
        
        double [][] angles = new double[gradX.length][gradX[0].length];
        double [][] magnitude = new double[gradX.length][gradX[0].length];
        
        for(int i = 0; i < angles.length; i++) {
            for(int j = 0; j < angles[0].length; j++) {
                angles[i][j] = Math.atan2(gradY[i][j], gradX[i][j]);
                magnitude[i][j] = Math.sqrt((gradY[i][j] * gradY[i][j]) + (gradX[i][j] * gradX[i][j]));
            }
        }
        for(int n = 0; n < this.myParameters.numWindowsY; n++) {
            for(int m = 0; m < this.myParameters.numWindowsX; m++) {
                double [] collectedAngles = this.collectData(angles, m * stepX, (m+2) * stepX, n*stepY, (n+2) * stepY);
                double [] collectedMagnitude = this.collectData(magnitude, m * stepX, (m+2) * stepX, n*stepY, (n+2) * stepY);
            
                double [] histogram = this.computeHistogram(collectedAngles, collectedMagnitude, this.myParameters.numHistogramBins);
                this.normalize(histogram);
                histData.add(histogram);
            }
        }
        double [] combinedHistData = new double[this.myParameters.numHistogramBins * histData.size()];
        for(int i = 0 ; i < histData.size(); i++) {
            double [] hist = histData.get(i);
            System.arraycopy(hist, 0, combinedHistData, i*this.myParameters.numHistogramBins, this.myParameters.numHistogramBins);
        }
        FixedFeatureVector vec = new FixedFeatureVector();
        vec.setData(combinedHistData);
        return vec;
    }
    
    private void normalize(double [] hist) 
    {
        double sum = 0;
        for(int i = 0; i < hist.length; i++) {
            sum += hist[i]*hist[i];
        }
        // the original implementation adds 0.01 here to avoid division by 0 ...
        // we keep this to be consistent
        sum = Math.sqrt(sum) + 0.01; 
        for(int i = 0; i < hist.length; i++) {
            hist[i] = hist[i] / sum;
        }
    }
    
    private double [] computeHistogram(double [] angles, double [] magnitude, int numBins)
    {
        double [] histogram = new double[numBins];
        for(int i = 0; i < angles.length; i++) {
            double angle = angles[i] + Math.PI;
              
            int histIndex = (int)Math.floor((this.myParameters.numHistogramBins / (2*Math.PI)) * angle);
            // the original implementation does assign -pi to bin 0 but pi is discarded, 
            // it is probably a bug but I keep it to be consistent
            if(histIndex == this.myParameters.numHistogramBins) { 
                continue;
            }
            histogram[histIndex] += magnitude[i];
        }
        return histogram;
    }
    
    private double [] collectData(double [][] data, int xStart, int xEnd, int yStart, int yEnd)
    {
        double [] collectedData = new double[(xEnd-xStart) * (yEnd-yStart)];
        int pos = 0;
        
        for(int i = xStart; i < xEnd; i++) {
            for(int j = yStart; j < yEnd; j++) {
               collectedData[pos] = data[i][j];
               pos++;
            }
        }
        return collectedData;
    }
    
    /**
     * Computes correlation between data and filter at specified dimension. 
     * Border handling is done such that indices falling outside the data are 
     * assumed to be 0, this is equal to matlabs: imfilter(data, filter);
     * @param signal
     * @param filter
     * @param dimension 0 = first dimension (rows) 1 = second dimension (columns)
     * @return 
     */
    protected double [][] correlate(double [][] signal, double [] filter, int dimension) 
    {
        int xIndex, yIndex;
        double[][] outArray = new double[signal.length][signal[0].length];

        int deltaX = filter.length / 2;
        int deltaY = filter.length / 2;

        // rows
        if(dimension == 0) {
            int xMax = signal.length - 1;

            for(int x = 0; x < signal.length; x++) {
                for(int y = 0; y < signal[0].length; y++) {
                    // convolve
                    double convSum = 0;
                    for(int i = 0; i < filter.length; i++) {
                        xIndex = x - deltaX + i;
                        yIndex = y;

                        if(xIndex < 0 || xIndex > xMax) // assume 0
                        {
                           continue;
                        }
                        convSum += filter[i] * signal[xIndex][yIndex];
                    }
                    outArray[x][y] = convSum;
                }
            }
        }
        // columns
        if(dimension == 1) {
            int yMax = signal[0].length - 1;

            for(int x = 0; x < signal.length; x++) {
                for(int y = 0; y < signal[0].length; y++) {
                    // convolve
                    double convSum = 0;
                    for(int i = 0; i < filter.length; i++) {
                        xIndex = x;
                        yIndex = y - deltaY + i;

                        if(yIndex < 0 || yIndex > yMax) // assume 0
                        {
                            continue;
                        }
                        convSum += filter[i] * signal[xIndex][yIndex];
                    }
                    outArray[x][y] = convSum;
                }
            }
        }
        return outArray;
    }
    
    
    @Override
    public void initialize() throws JCeliacGenericException
    {
    }

    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass) throws
                                                                                JCeliacGenericException
    {
    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {
    }
    
}
