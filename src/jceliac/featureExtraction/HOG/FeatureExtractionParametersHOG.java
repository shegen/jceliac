/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.HOG;

import jceliac.experiment.Experiment;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersHOG
        extends FeatureExtractionParameters
{

    protected int numWindowsX = 1;
    protected int numWindowsY = 1;
    protected int numHistogramBins = 9;

    @Override
    public String getMethodName()
    {
        return "HOG";
    }

    @Override
    public void report()
    {
        super.report();
        Experiment.log(JCeliacLogger.LOG_INFO, "Number of Windows (X): " + this.numWindowsX);
        Experiment.log(JCeliacLogger.LOG_INFO, "Number of Windows (Y): " + this.numWindowsY);
        Experiment.log(JCeliacLogger.LOG_INFO, "Number of Histogram bins: " + this.numHistogramBins);
    }

}
