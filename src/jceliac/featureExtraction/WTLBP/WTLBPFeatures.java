/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.WTLBP;

import jceliac.featureExtraction.LBP.*;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureOptimization.*;
import java.util.*;

/**
 *
 * @author shegen
 */
public class WTLBPFeatures 
        extends LBPFeatures
{

    private final ArrayList<String> waveletScales;
    private final ArrayList<String> waveletSubbands;
    private final ArrayList<String> waveletColors;

    public WTLBPFeatures(FeatureExtractionParameters parameters)
    {
        super(parameters);
        this.waveletScales = new ArrayList<>();
        this.waveletSubbands = new ArrayList<>();
        this.waveletColors = new ArrayList<>();
    }

    public WTLBPFeatures()
    {
        super();
        this.waveletScales = new ArrayList<>();
        this.waveletSubbands = new ArrayList<>();
        this.waveletColors = new ArrayList<>();
    }

    @Override
    public String getScaleString(FeatureVectorSubsetEncoding subset)
    {
        return "-";
    }

    @Override
    public String getColorString(FeatureVectorSubsetEncoding subset)
    {
        return "unsupported";
    }

    @Override
    public String getSubbandString(FeatureVectorSubsetEncoding subset)
    {
        return "-";
    }

    public void addWaveletScale(String scale)
    {
        waveletScales.add(scale);
    }

    public void addWaveletSubband(String subband)
    {
        waveletSubbands.add(subband);
    }

    public void addColor(String color)
    {
        waveletColors.add(color);
    }

    @Override
    public String getWaveletSubbandString(FeatureVectorSubsetEncoding subset)
    {
        String subbandString = "";

        for(String s : this.waveletSubbands) {
            subbandString += s + ";";
        }
        return subbandString;
    }

    @Override
    public String getWaveletScaleString(FeatureVectorSubsetEncoding subset)
    {
        String scaleString = "";

        for(String s : this.waveletScales) {
            scaleString += s + ";";
        }
        return scaleString;
    }

}
