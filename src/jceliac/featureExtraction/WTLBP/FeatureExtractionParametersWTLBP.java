/*
 *  JCeliac Copyright (C) 2010-2011 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.WTLBP;

import jceliac.featureExtraction.LBP.FeatureExtractionParametersLBP;
import jceliac.experiment.*;
import jceliac.logging.*;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersWTLBP extends FeatureExtractionParametersLBP {

    /**
     * The number of contrast bins influences the binning of the 2-dimensional
     * histogram. Default is 6.
     */
    protected int contrastBins = 6; // lbpc

    /**
     * Vertical LTP threshold base, do not use.
     */
    protected double verticalThresholdBeta;  // ltp

    /**
     * Alpha is used as a weight to define how much impact the standard
     * deviation of the current signal sample has. Vertical sub-band.
     */
    protected double verticalThresholdAlpha = 0.1; // ltp

    /**
     * Horizontal LTP threshold base, do not use.
     */
    protected double horizontallThresholdBeta;  // ltp

    /**
     * Alpha is used as a weight to define how much impact the standard
     * deviation of the current signal sample has. Horizontal sub-band.
     */
    protected double horizontalThresholdAlpha = 0.1; // ltp

    /**
     * Diagonal LTP threshold base, do not use.
     */
    protected double diagonalThresholdBeta;  // ltp

    /**
     * Alpha is used as a weight to define how much impact the standard
     * deviation of the current signal sample has. Diagonal sub-band.
     */
    protected double diagonalThresholdAlpha = 0.1; // ltp

    protected int waveletScales = 3;
    protected boolean resizeImage = true;

    @Override
    public String getMethodName() {
        return "WTLBP";
    }

    @Override
    public void report() {
        super.report();
        Experiment.log(JCeliacLogger.LOG_INFO, "Contrast-Bins: %d", this.contrastBins);
        
        Experiment.log(JCeliacLogger.LOG_INFO, "Threshold alpha (vertical): "+this.verticalThresholdAlpha);
        Experiment.log(JCeliacLogger.LOG_INFO, "Threshold alpha (horizontal): "+this.horizontalThresholdAlpha);
        Experiment.log(JCeliacLogger.LOG_INFO, "Threshold alpha (diagonal): "+this.diagonalThresholdAlpha);
        
        Experiment.log(JCeliacLogger.LOG_INFO, "Threshold beta (vertical): "+this.verticalThresholdBeta);
        Experiment.log(JCeliacLogger.LOG_INFO, "Threshold beta (horizontal): "+this.horizontallThresholdBeta);
        Experiment.log(JCeliacLogger.LOG_INFO, "Threshold beta (diagonal): "+this.diagonalThresholdBeta);  
    }

    public int getContrastBins() {
        return contrastBins;
    }

    public void setContrastBins(int contrastBins) {
        this.contrastBins = contrastBins;
    }

    public double getDiagonalThresholdAlpha() {
        return diagonalThresholdAlpha;
    }

    public void setDiagonalThresholdAlpha(double diagonalThresholdAlpha) {
        this.diagonalThresholdAlpha = diagonalThresholdAlpha;
    }

    public double getDiagonalThresholdBeta() {
        return diagonalThresholdBeta;
    }

    public void setDiagonalThresholdBeta(double diagonalThresholdBeta) {
        this.diagonalThresholdBeta = diagonalThresholdBeta;
    }

    public double getHorizontalThresholdAlpha() {
        return horizontalThresholdAlpha;
    }

    public void setHorizontalThresholdAlpha(double horizontalThresholdAlpha) {
        this.horizontalThresholdAlpha = horizontalThresholdAlpha;
    }

    public double getHorizontallThresholdBeta() {
        return horizontallThresholdBeta;
    }

    public void setHorizontallThresholdBeta(double horizontallThresholdBeta) {
        this.horizontallThresholdBeta = horizontallThresholdBeta;
    }

    public double getVerticalThresholdAlpha() {
        return verticalThresholdAlpha;
    }

    public void setVerticalThresholdAlpha(double verticalThresholdAlpha) {
        this.verticalThresholdAlpha = verticalThresholdAlpha;
    }

    public double getVerticalThresholdBeta() {
        return verticalThresholdBeta;
    }

    public void setVerticalThresholdBeta(double verticalThresholdBeta) {
        this.verticalThresholdBeta = verticalThresholdBeta;
    }

    public int getWaveletScales() {
        return waveletScales;
    }

    public boolean getResizeImage() {
        return resizeImage;
    }

    public void setResizeImage(boolean resizeImage) {
        this.resizeImage = resizeImage;
    }
}
