/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.WTLBP;

import jceliac.tools.transforms.wavelet.WaveletTransform;
import jceliac.tools.transforms.wavelet.WaveletDecomposition;
import jceliac.featureExtraction.LTP.FeatureExtractionParametersLTP;
import jceliac.featureExtraction.LBPC.FeatureExtractionParametersLBPC;
import jceliac.featureExtraction.LBP.FeatureExtractionParametersLBP;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.tools.transforms.wavelet.WaveletTransform.EFilterBankType;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.LBPC.*;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.LTP.*;
import jceliac.featureExtraction.LBP.*;
import jceliac.tools.filter.*;
import jceliac.tools.math.*;
import jceliac.experiment.*;
import jceliac.tools.data.*;
import java.util.ArrayList;
import jceliac.featureExtraction.AbstractFeatureVector;
import jceliac.featureExtraction.StableCode;
import jceliac.logging.*;

/**
 * Wavelet Transform bassed Local Binary Patterns.
 * <p>
 * @author shegen
 */
@StableCode(comment = "This code is trusted.",
            date = "16.01.2014",
            lastModified = "16.01.2014")
public class FeatureExtractionMethodWTLBP
        extends FeatureExtractionMethodLBP
{

    private final FeatureExtractionParametersWTLBP myParameters;

    private final FeatureExtractionMethodLBPC methodLBPC;
    private final FeatureExtractionMethodLTP verticalMethodLTP;
    private final FeatureExtractionMethodLTP diagonalMethodLTP;
    private final FeatureExtractionMethodLTP horizontalMethodLTP;

    private final FeatureExtractionParametersLBPC paramsLBPC;
    private final FeatureExtractionParametersLTP verticalParamsLTP;
    private final FeatureExtractionParametersLTP horizontalParamsLTP;
    private final FeatureExtractionParametersLTP diagonalParamsLTP;

    public FeatureExtractionMethodWTLBP(FeatureExtractionParameters param)
    {
        super(param);
        this.myParameters = (FeatureExtractionParametersWTLBP) param;

        this.paramsLBPC = new FeatureExtractionParametersLBPC();
        this.paramsLBPC.setContrastBins(myParameters.getContrastBins());
        this.paramsLBPC.setMaxScale(3);
        this.setCommonParameters(this.paramsLBPC);
        this.methodLBPC = new FeatureExtractionMethodLBPC(this.paramsLBPC);

        this.verticalParamsLTP = new FeatureExtractionParametersLTP();
        this.horizontalParamsLTP = new FeatureExtractionParametersLTP();
        this.diagonalParamsLTP = new FeatureExtractionParametersLTP();

        // setup parameters
        this.verticalParamsLTP.setThresholdAlpha(this.myParameters.getVerticalThresholdAlpha());
        this.horizontalParamsLTP.setThresholdAlpha(this.myParameters.getHorizontalThresholdAlpha());
        this.diagonalParamsLTP.setThresholdAlpha(this.myParameters.getDiagonalThresholdAlpha());

        this.verticalParamsLTP.setMaxScale(1);
        this.horizontalParamsLTP.setMaxScale(1);
        this.diagonalParamsLTP.setMaxScale(1);

        this.setCommonParameters(this.verticalParamsLTP);
        this.setCommonParameters(this.horizontalParamsLTP);
        this.setCommonParameters(this.diagonalParamsLTP);

        this.verticalMethodLTP = new FeatureExtractionMethodLTP(this.verticalParamsLTP);
        this.horizontalMethodLTP = new FeatureExtractionMethodLTP(this.horizontalParamsLTP);
        this.diagonalMethodLTP = new FeatureExtractionMethodLTP(this.diagonalParamsLTP);
    }

    public final void setCommonParameters(FeatureExtractionParametersLBP params)
    {
        params.setUseUniformPatterns(this.myParameters.isUseUniformPatterns());
        params.setUseColor(this.myParameters.isUseColor());
    }

    private void prepareCoefficients(double[][] coeffs)
    {
        MathTools.abs(coeffs);
        DataFilter2D.scaleData(coeffs);
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content) throws
            JCeliacGenericException
    {
        WaveletTransform wt = new WaveletTransform(EFilterBankType.CDF97);
        ArrayList<DiscriminativeFeatures> approxFeatures = new ArrayList<>();
        ArrayList<DiscriminativeFeatures> vertFeatures = new ArrayList<>();
        ArrayList<DiscriminativeFeatures> horizFeatures = new ArrayList<>();
        ArrayList<DiscriminativeFeatures> diagFeatures = new ArrayList<>();

        if(this.myParameters.getResizeImage()) {
            // sizeup frame so the first approximation has the same dimension as the natural image
            ImageResize resizer = new ImageResize(ImageResize.EInterpolationType.BILINEAR);
            resizer.resizeFrame(content, content.getWidth() * 2, content.getHeight() * 2);
        }

        if(this.myParameters.isUseColor()) 
        {
            int colorChannelFlag = 0;
            for(Integer colorChannel : this.myParameters.getColorChannels()) {
                colorChannelFlag |= colorChannel;
            }
            wt.transformForwardMultiChannel(content, this.myParameters.getWaveletScales(), colorChannelFlag);
            for(Integer colorChannel : this.myParameters.getColorChannels()) {
                WaveletDecomposition decomp = wt.getWaveletDecompositionTree(colorChannel);
                
                for(int curScale = 1; curScale <= this.myParameters.getWaveletScales(); curScale++) {
                    Frame transformedApproxFrame = new Frame(content.getParentFile(), content.getFrameIdentifier());
                    Frame transformedVerticalFrame = new Frame(content.getParentFile(), content.getFrameIdentifier());
                    Frame transformedHorizontalFrame = new Frame(content.getParentFile(), content.getFrameIdentifier());
                    Frame transformedDiagonalFrame = new Frame(content.getParentFile(), content.getFrameIdentifier());

                    // Approximation Subband
                    prepareCoefficients(decomp.getApproximationSubband(curScale));
                    transformedApproxFrame.setColorChannel(colorChannel, decomp.getApproximationSubband(curScale));
                   
                    // Vertical Detail Subband
                    prepareCoefficients(decomp.getVerticalDetailSubband(curScale));
                    transformedVerticalFrame.setColorChannel(colorChannel, decomp.getVerticalDetailSubband(curScale));
                    
                    // Horizontal
                    prepareCoefficients(decomp.getHorizontalDetailSubband(curScale));
                    transformedHorizontalFrame.setColorChannel(colorChannel, decomp.getHorizontalDetailSubband(curScale));
     
                    // Diagonal
                    prepareCoefficients(decomp.getDiagonalDetailSubband(curScale));
                    transformedDiagonalFrame.setColorChannel(colorChannel, decomp.getDiagonalDetailSubband(curScale));

                    DiscriminativeFeatures lbpcFeatures = this.methodLBPC.extractFeatures(transformedApproxFrame);
                    DiscriminativeFeatures ltpVerticalFeatures = this.verticalMethodLTP.extractFeatures(transformedVerticalFrame);
                    DiscriminativeFeatures ltpHorizontalFeatures = this.horizontalMethodLTP.extractFeatures(transformedHorizontalFrame);
                    DiscriminativeFeatures ltpDiagonalFeatures = this.diagonalMethodLTP.extractFeatures(transformedDiagonalFrame);

                    approxFeatures.add(lbpcFeatures);
                    vertFeatures.add(ltpVerticalFeatures);
                    horizFeatures.add(ltpHorizontalFeatures);
                    diagFeatures.add(ltpDiagonalFeatures);
                }
                
            }
        } else // grayscale
        {
            wt.transformForwardMultiChannel(content, this.myParameters.getWaveletScales(), DataSource.CHANNEL_GRAY);
            WaveletDecomposition grayWd = wt.getWaveletDecompositionTree(DataSource.CHANNEL_GRAY);
            for(int curScale = 1; curScale <= this.myParameters.getWaveletScales(); curScale++) {
                Frame transformedApproxFrame = new Frame(content.getParentFile(), content.getFrameIdentifier());
                Frame transformedVerticalFrame = new Frame(content.getParentFile(), content.getFrameIdentifier());
                Frame transformedHorizontalFrame = new Frame(content.getParentFile(), content.getFrameIdentifier());
                Frame transformedDiagonalFrame = new Frame(content.getParentFile(), content.getFrameIdentifier());

                prepareCoefficients(grayWd.getApproximationSubband(curScale));
                transformedApproxFrame.setGrayData(grayWd.getApproximationSubband(curScale));

                prepareCoefficients(grayWd.getVerticalDetailSubband(curScale));
                transformedVerticalFrame.setGrayData(grayWd.getVerticalDetailSubband(curScale));

                prepareCoefficients(grayWd.getHorizontalDetailSubband(curScale));
                transformedHorizontalFrame.setGrayData(grayWd.getHorizontalDetailSubband(curScale));

                prepareCoefficients(grayWd.getDiagonalDetailSubband(curScale));
                transformedDiagonalFrame.setGrayData(grayWd.getDiagonalDetailSubband(curScale));

                DiscriminativeFeatures lbpcFeatures = this.methodLBPC.extractFeatures(transformedApproxFrame);
                DiscriminativeFeatures ltpVerticalFeatures = this.verticalMethodLTP.extractFeatures(transformedVerticalFrame);
                DiscriminativeFeatures ltpHorizontalFeatures = this.horizontalMethodLTP.extractFeatures(transformedHorizontalFrame);
                DiscriminativeFeatures ltpDiagonalFeatures = this.diagonalMethodLTP.extractFeatures(transformedDiagonalFrame);

                approxFeatures.add(lbpcFeatures);
                vertFeatures.add(ltpVerticalFeatures);
                horizFeatures.add(ltpHorizontalFeatures);
                diagFeatures.add(ltpDiagonalFeatures);
            }
        }
        return combineFeatures(approxFeatures, vertFeatures, horizFeatures,
                               diagFeatures, content);
    }

    private DiscriminativeFeatures combineFeatures(ArrayList<DiscriminativeFeatures> approxFeatures,
                                     ArrayList<DiscriminativeFeatures> vertFeatures,
                                     ArrayList<DiscriminativeFeatures> horizFeatures,
                                     ArrayList<DiscriminativeFeatures> diagFeatures,
                                     Frame content)
            throws JCeliacGenericException
    {
        String colors[] = {"R", "G", "B"};

        WTLBPFeatures combinedFeatures = new WTLBPFeatures(this.myParameters);
        combinedFeatures.setSignalIdentifier(content.getSignalIdentifier());
        combinedFeatures.setFrameNumber(content.getFrameIdentifier());

        int scale = 0;
        for(DiscriminativeFeatures tmp : approxFeatures) {
            for(AbstractFeatureVector vec : tmp.getFeatureVectors()) {
                combinedFeatures.addAbstractFeature(vec);
                combinedFeatures.addWaveletSubband("a");
                combinedFeatures.addWaveletScale(String.valueOf(scale));
                scale++;
            }

        }
        scale = 0;
        for(DiscriminativeFeatures tmp : vertFeatures) {
              for(AbstractFeatureVector vec : tmp.getFeatureVectors()) {
                combinedFeatures.addAbstractFeature(vec);
                combinedFeatures.addWaveletSubband("v");
                combinedFeatures.addWaveletScale(String.valueOf(scale));
                scale++;
              }

        }
        scale = 0;
        for(DiscriminativeFeatures tmp : horizFeatures) {
            for(AbstractFeatureVector vec : tmp.getFeatureVectors()) {
                combinedFeatures.addAbstractFeature(vec);
                combinedFeatures.addWaveletSubband("h");
                combinedFeatures.addWaveletScale(String.valueOf(scale));
                scale++;
            }

        }
        scale = 0;
        for(DiscriminativeFeatures tmp : diagFeatures) {
            for(AbstractFeatureVector vec : tmp.getFeatureVectors()) {
                combinedFeatures.addAbstractFeature(vec);
                combinedFeatures.addWaveletSubband("d");
                combinedFeatures.addWaveletScale(String.valueOf(scale));
                scale++;
            }

        }
        return combinedFeatures;
    }

    @Override
    public void initialize()
            throws JCeliacGenericException
    {
        super.initialize();
        this.methodLBPC.initialize();
        this.verticalMethodLTP.initialize();
        this.horizontalMethodLTP.initialize();
        this.diagonalMethodLTP.initialize();
    }

    @Override
    public void estimateParameters(ArrayList<DataReader> dataReaders)
            throws JCeliacGenericException
    {
        // perform wavelet decompositions
        Experiment.log(JCeliacLogger.LOG_INFO, "Estimating WTLBP Parameters (empirical contrast distribution function + thresholds).");

        for(int classNum = 0; classNum < dataReaders.size(); classNum++) {
            DataReader current = dataReaders.get(classNum);
            while(current.hasNext()) {
                DataSource currentFile = (DataSource) current.next();
                ContentStream cstream = currentFile.createContentStream();

                while(cstream.hasNext()) {
                    Frame content = (Frame) cstream.next();

                    ImageResize resizer = new ImageResize(ImageResize.EInterpolationType.BILINEAR);
                    resizer.resizeFrame(content, content.getWidth() * 2, content.getHeight() * 2);
                    WaveletTransform wt = new WaveletTransform(EFilterBankType.CDF97);

                    // the parameters are computed using scale 1, grayscale
                    wt.transformForwardMultiChannel(content, 1, DataSource.CHANNEL_GRAY);
                    WaveletDecomposition grayWd = wt.getWaveletDecompositionTree(DataSource.CHANNEL_GRAY);

                    Frame transformedApproxFrame = new Frame(content.getParentFile(), content.getFrameIdentifier());
                    Frame transformedVerticalFrame = new Frame(content.getParentFile(), content.getFrameIdentifier());
                    Frame transformedHorizontalFrame = new Frame(content.getParentFile(), content.getFrameIdentifier());
                    Frame transformedDiagonalFrame = new Frame(content.getParentFile(), content.getFrameIdentifier());

                    prepareCoefficients(grayWd.getApproximationSubband(1));
                    transformedApproxFrame.setGrayData(grayWd.getApproximationSubband(1));

                    prepareCoefficients(grayWd.getVerticalDetailSubband(1));
                    transformedVerticalFrame.setGrayData(grayWd.getVerticalDetailSubband(1));

                    prepareCoefficients(grayWd.getHorizontalDetailSubband(1));
                    transformedHorizontalFrame.setGrayData(grayWd.getHorizontalDetailSubband(1));

                    prepareCoefficients(grayWd.getDiagonalDetailSubband(1));
                    transformedDiagonalFrame.setGrayData(grayWd.getDiagonalDetailSubband(1));

                    this.methodLBPC.estimateParametersIterative(transformedApproxFrame);
                    this.verticalMethodLTP.estimateParametersIterative(transformedVerticalFrame);
                    this.horizontalMethodLTP.estimateParametersIterative(transformedHorizontalFrame);
                    this.diagonalMethodLTP.estimateParametersIterative(transformedDiagonalFrame);

                    FeatureExtractionParametersLTP diagParams
                                                   = (FeatureExtractionParametersLTP) this.diagonalMethodLTP.getMethodParameters();
                    FeatureExtractionParametersLTP vertParams
                                                   = (FeatureExtractionParametersLTP) this.verticalMethodLTP.getMethodParameters();
                    FeatureExtractionParametersLTP horizParams
                                                   = (FeatureExtractionParametersLTP) this.horizontalMethodLTP.getMethodParameters();

                    this.myParameters.setDiagonalThresholdBeta(diagParams.getThresholdBeta());
                    this.myParameters.setHorizontallThresholdBeta(horizParams.getThresholdBeta());
                    this.myParameters.setVerticalThresholdBeta(vertParams.getThresholdBeta());
                }
            }
            current.reset();

        }

    }

}
