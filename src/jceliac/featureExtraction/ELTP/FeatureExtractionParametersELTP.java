/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.ELTP;

import jceliac.experiment.Experiment;
import jceliac.featureExtraction.ELBP.FeatureExtractionParametersELBP;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersELTP
        extends FeatureExtractionParametersELBP
{

    public FeatureExtractionParametersELTP()
    {
    }

    /**
     * The beta values are found using statistics on the training data, do not
     * set them manually!
     */
    protected double betaValueHoriz;

    /**
     * Alpha is used as a weight to define how much impact the standard
     * deviation of the current signal sample has. Horizontal sub-band.
     */
    protected double alphaValueHoriz = 0.05;

    /**
     * The beta values are found using statistics on the training data, do not
     * set them manually!
     */
    protected double betaValueVert;

    /**
     * Alpha is used as a weight to define how much impact the standard
     * deviation of the current signal sample has. Vertical sub-band.
     */
    protected double alphaValueVert = 0.05;

    /**
     * The beta values are found using statistics on the training data, do not
     * set them manually!
     */
    protected double betaValueDiag;

    /**
     * Alpha is used as a weight to define how much impact the standard
     * deviation of the current signal sample has. Diagonal sub-band.
     */
    protected double alphaValueDiag = 0.05;

    public double getThresholdAlphaVert()
    {
        return alphaValueVert;
    }

    public double getThresholdBetaVert()
    {
        return betaValueVert;
    }

    public void setThresholdAlphaVert(double a)
    {
        this.alphaValueVert = a;
    }

    public void setThresholdBetaVert(double b)
    {
        this.betaValueVert = b;
    }

    public double getThresholdAlphaHoriz()
    {
        return alphaValueHoriz;
    }

    public double getThresholdBetaHoriz()
    {
        return betaValueHoriz;
    }

    public void setThresholdAlphaHoriz(double a)
    {
        this.alphaValueHoriz = a;
    }

    public void setThresholdBetaHoriz(double b)
    {
        this.betaValueHoriz = b;
    }

    public double getThresholdAlphaDiag()
    {
        return alphaValueDiag;
    }

    public void setThresholdAlphaDiag(double alphaValueDiag)
    {
        this.alphaValueDiag = alphaValueDiag;
    }

    public double getThresholdBetaDiag()
    {
        return betaValueDiag;
    }

    public void setThresholdBetaDiag(double betaValueDiag)
    {
        this.betaValueDiag = betaValueDiag;
    }

    @Override
    public void report()
    {
        super.report();
        Experiment.log(JCeliacLogger.LOG_INFO, "Threshold alpha (vertical): "+this.alphaValueVert);
        Experiment.log(JCeliacLogger.LOG_INFO, "Threshold alpha (horizontal): "+this.alphaValueHoriz);
        Experiment.log(JCeliacLogger.LOG_INFO, "Threshold alpha (diagonal): "+this.alphaValueDiag);
        
        Experiment.log(JCeliacLogger.LOG_INFO, "Threshold beta (vertical): "+this.betaValueVert);
        Experiment.log(JCeliacLogger.LOG_INFO, "Threshold beta (horizontal): "+this.betaValueDiag);
        Experiment.log(JCeliacLogger.LOG_INFO, "Threshold beta (diagonal): "+this.betaValueDiag);  
    }

    @Override
    public String getMethodName()
    {
        return "ELTP";
    }
}
