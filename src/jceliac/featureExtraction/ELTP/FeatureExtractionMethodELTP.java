/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.ELTP;

import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.StableCode;
import jceliac.featureExtraction.ELBP.*;
import jceliac.featureExtraction.LTP.*;
import jceliac.featureExtraction.LBP.*;
import jceliac.JCeliacGenericException;
import jceliac.tools.math.MathTools;
import jceliac.tools.filter.*;
import jceliac.experiment.*;
import jceliac.tools.data.*;
import jceliac.logging.*;
import java.util.*;

/**
 * Extended Local Ternary Patterns.
 * <p>
 * @author shegen
 */
@StableCode(date = "16.01.2014", lastModified = "16.01.2014")
public class FeatureExtractionMethodELTP
        extends FeatureExtractionMethodELBP
{

    private final FeatureExtractionParametersELTP myParameters;

    public FeatureExtractionMethodELTP(FeatureExtractionParameters param)
    {
        super(param);

        // this is just for convenience
        this.myParameters = (FeatureExtractionParametersELTP) super.parameters;

        // setup my data structs
    }

    @Override
    protected LBPHistogram computeDistribution(double [][] dataChannel, int scale, int filterOrientation)
            throws JCeliacGenericException
    {
        LBPHistogram elbpHistogram;

        int neighbors = LBPMultiscale.getNeighborsForScale(scale);
        double radius = LBPMultiscale.getRadiusForScale(scale);
        int margin = (int) Math.ceil(radius);

        DataFilter2D sobelFilter = new SobelFilter(filterOrientation);
        DataFilter2D multiScaleFilter = LBPMultiscale.getFilterForScale(scale);

        double[][] dataSobeled;
        dataSobeled = sobelFilter.filterData(dataChannel);
        dataSobeled = multiScaleFilter.filterData(dataSobeled);

        elbpHistogram = computeHistogram(dataSobeled, neighbors, radius, margin, scale, filterOrientation);
        elbpHistogram.setSubband(filterOrientation);

        return elbpHistogram;
    }

    protected LBPHistogram computeHistogram(double[][] data, int neighbors, double radius, int margin, int scale, int filterOrientation)
            throws JCeliacGenericException
    {
        double[] histLower = new double[getNumberOfBins(neighbors)];
        double[] histUpper = new double[getNumberOfBins(neighbors)];
        double threshold;
        double beta;
        double alpha;
        double stdev;

        stdev = MathTools.stdev(data);
        switch(filterOrientation) {
            case SobelFilter.ORIENTATION_HORIZONTAL:
                beta = this.myParameters.getThresholdBetaHoriz();
                alpha = this.myParameters.getThresholdAlphaHoriz();
                break;

            case SobelFilter.ORIENTATION_VERTICAL:
                beta = this.myParameters.getThresholdBetaVert();
                alpha = this.myParameters.getThresholdAlphaVert();
                break;

            case SobelFilter.ORIENTATION_DIAGONAL:
                beta = this.myParameters.getThresholdBetaDiag();
                alpha = this.myParameters.getThresholdAlphaDiag();
                break;

            case SobelFilter.ORIENTATION_MAGNITUDE:
                beta = 0.5d * (this.myParameters.getThresholdBetaHoriz() + this.myParameters.getThresholdBetaVert());
                alpha = 0.5d * (this.myParameters.getThresholdAlphaHoriz() + this.myParameters.getThresholdAlphaVert());
                break;
            default:
                throw new JCeliacGenericException("Unknown Filter Direction, using 0 Threshold");
        }

        if(stdev >= beta) {
            threshold = beta + alpha * stdev;
        } else {
            threshold = beta - alpha * stdev;
        }

        int shiftMax = neighbors - 1;
        for(int x = margin; x < data.length - margin; x++) {
            for(int y = margin; y < data[0].length - margin; y++) {

                // THE MATLAB CODE WAS BUGGED, this is correct!
                double center = (double) data[x][y];
                int patternUpper = 0;
                int patternLower = 0;

                for(int p = 0; p < neighbors; p++) {
                    double neighbor = getNeighbor(data, x, y, p, scale);

                    if(neighbor >= (center + threshold)) {
                        // this is in reverse order from the matlab thingy to speed up things
                        patternUpper |= (1 << (shiftMax - p));
                    } else if(neighbor <= (center - threshold)) {
                        // this is in reverse order from the matlab thingy to speed up things
                        patternLower |= (1 << (shiftMax - p));
                    } else {
                        // zero
                    }
                }
                /* reorder pattern to comply with the other implementations,
                 * freaking hokus pokus!! pick top 3 bit bits from msb
                 */
                int tmphUpper = patternUpper & 0xE0; // 224
                // pick bottom 5 bits from lsb
                int tmplUpper = patternUpper & 0x1F; // 31;
                int patternUpperFixed = (tmphUpper >> 5) | (tmplUpper << 3);

                /* reorder pattern to comply with the other implementations,
                 * freaking hokus pokus!! pick top 3 bit bits from msb
                 */
                int tmphLower = patternLower & 0xE0; // 224
                // pick bottom 5 bits from lsb
                int tmplLower = patternLower & 0x1F; // 31;
                int patternLowerFixed = (tmphLower >> 5) | (tmplLower << 3);

                histUpper[patternUpperFixed] = histUpper[patternUpperFixed] + 1.0d;
                histLower[patternLowerFixed] = histLower[patternLowerFixed] + 1.0d;
            }
        }

        // many patterns are all zeros within this operator, avoid them
        histLower[0] = 0;
        histUpper[0] = 0;

        LTPHistogram eltpHist = new LTPHistogram();
        eltpHist.setDataLower(histLower);
        eltpHist.setDataUpper(histUpper);
        return eltpHist;
    }

    /**
     * This estimates the thresholds used within the LTP operator. The
     * thresholds are calculated as: stdev(content) > beta => beta + alpha *
     * stdev(content); stdev(content) < beta => beta + alpha * stdev(content);
     * The value alpha is a weighting factor and is parametrizeable. The value
     * beta is estimated by this method as the standard deviation of all images
     * (pixel intensity deviation).
     * <p>
     * <p>
     * @param dataReaders Data readers for all classes.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    @Override
    public void estimateParameters(ArrayList<DataReader> dataReaders)
            throws JCeliacGenericException
    {
        double meanHoriz = 0;
        double meanVert = 0;
        double meanDiag = 0;
        double sumHoriz = 0;
        double sumVert = 0;
        double sumDiag = 0;
        double count = 0;

        Experiment.log(JCeliacLogger.LOG_INFO, "Estimating ELTP Parameters (Treshold Beta).");
        DataFilter2D sobelHoriz = new SobelFilter(SobelFilter.ORIENTATION_HORIZONTAL);
        DataFilter2D sobelVert = new SobelFilter(SobelFilter.ORIENTATION_VERTICAL);
        DataFilter2D sobelDiag = new SobelFilter(SobelFilter.ORIENTATION_DIAGONAL);

        for(int classNum = 0; classNum < dataReaders.size(); classNum++) {
            DataReader current = dataReaders.get(classNum);
            while(current.hasNext()) {
                DataSource currentFile = (DataSource) current.next();
                ContentStream cstream = currentFile.createContentStream();

                while(cstream.hasNext()) {
                    Frame content = (Frame) cstream.next();
                    double[][] data = content.getGrayData();
                    try {
                        double[][] dataVert = sobelVert.filterData(data);
                        double[][] dataHoriz = sobelHoriz.filterData(data);
                        double[][] dataDiag = sobelDiag.filterData(data);

                        for(int x = 0; x < data.length; x++) {
                            for(int y = 0; y < data[0].length; y++) {
                                meanVert += dataVert[x][y];
                                meanHoriz += dataHoriz[x][y];
                                meanDiag += dataDiag[x][y];
                                count++;
                            }
                        }
                    } catch(JCeliacGenericException e)// this is bad, can only happen if a wrong filter is used
                    {
                        Experiment.log(JCeliacLogger.LOG_ERROR, "Wrong filter used in ELTP parameters estimation: %s", e.getLocalizedMessage());
                        throw e;
                    }
                }
            }
            current.reset();
        }
        meanVert = (meanVert / count);
        meanHoriz = (meanHoriz / count);
        meanDiag = (meanDiag / count);

        for(int classNum = 0; classNum < dataReaders.size(); classNum++) {
            DataReader current = dataReaders.get(classNum);
            while(current.hasNext()) {
                DataSource currentFile = (DataSource) current.next();
                ContentStream cstream = currentFile.createContentStream();

                while(cstream.hasNext()) {
                    Frame content = (Frame) cstream.next();
                    double[][] data = content.getGrayData();

                    try {
                        double[][] dataVert = sobelVert.filterData(data);
                        double[][] dataHoriz = sobelHoriz.filterData(data);
                        double[][] dataDiag = sobelDiag.filterData(data);

                        for(int x = 0; x < data.length; x++) {
                            for(int y = 0; y < data[0].length; y++) {
                                double tmpVert = dataVert[x][y];
                                double tmpHoriz = dataHoriz[x][y];
                                double tmpDiag = dataDiag[x][y];

                                sumVert += (tmpVert - meanVert) * (tmpVert - meanVert);
                                sumHoriz += (tmpHoriz - meanHoriz) * (tmpHoriz - meanHoriz);
                                sumDiag += (tmpDiag - meanDiag) * (tmpDiag - meanDiag);
                            }
                        }
                    } catch(JCeliacGenericException e)// XXX this is bad, can only happen if a wrong filter is used
                    {
                        Experiment.log(JCeliacLogger.LOG_ERROR, "Wrong filter used in ELTP parameters estimation: %s", e.getLocalizedMessage());
                        throw e;
                    }

                }
            }
            current.reset();
        }

        double stdevVert = Math.sqrt(sumVert / (count - 1)); // compliant to matlab std(X)
        double stdevHoriz = Math.sqrt(sumHoriz / (count - 1)); // compliant to matlab std(X)
        double stdevDiag = Math.sqrt(sumDiag / (count - 1)); // compliant to matlab std(X)

        Experiment.log(JCeliacLogger.LOG_INFO, "Horizontal Grayscale Pixel Standard Deviation: %f.", stdevHoriz);
        Experiment.log(JCeliacLogger.LOG_INFO, "Vertical Grayscale Pixel Standard Deviation: %f.", stdevVert);

        // The threshold is SET to sqrt of the std!! this is correct!!
        this.myParameters.setThresholdBetaHoriz(Math.sqrt(stdevHoriz) / 3);
        this.myParameters.setThresholdBetaVert(Math.sqrt(stdevVert) / 3);
        this.myParameters.setThresholdBetaDiag(Math.sqrt(stdevDiag) / 3);
    }
}
