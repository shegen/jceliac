/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.LTP;

import static jceliac.featureExtraction.LBP.LBPHistogram.getNumberOfNeighbors;
import jceliac.featureExtraction.LBP.*;
import jceliac.experiment.Experiment;
import jceliac.logging.*;
import jceliac.*;

/**
 * LTP Histograms use an upper and lower histogram.
 * <p>
 * @author shegen
 */
public class LTPHistogram
        extends LBPHistogram
{

    private double[] dataLower = null; // patterns with -1
    private double[] dataUpper = null; // patterns with 1

    public void setDataLower(double[] data)
    {
        this.dataLower = data;
        this.histogramDimension = data.length * 2;
    }

    public void setDataUpper(double[] data)
    {
        this.dataUpper = data;
        this.histogramDimension = data.length * 2;
    }

    @Override
    public synchronized void prepare()
    {
        if(this.dataUpper != null && this.dataLower != null) {
            if(this.dataLower.length != this.dataUpper.length) {
                Experiment.log(JCeliacLogger.LOG_ERROR, "Histograms have different dimensions!");
            }
            this.data = new double[this.dataLower.length + this.dataUpper.length];
            System.arraycopy(this.dataUpper, 0, this.data, 0, this.dataUpper.length);
            System.arraycopy(this.dataLower, 0, this.data, this.dataUpper.length, this.dataLower.length);
            this.data = super.normalize(this.data);
        }
    }

    /**
     * Data has to be extracted from the lower and upper part separately.
     * <p>
     * @return Concatenated upper and lower uniform histogram.
     * <p>
     * @throws jceliac.JCeliacLogableException
     */
    @Override
    public double[] getUniformData()
            throws JCeliacLogableException
    {
        if(this.uniformData != null) {
            return this.uniformData;
        }

        this.prepare();
        // access to data is save from here
        boolean[] LUT = LBPHistogram.getUniformLUT(this.dataUpper.length);

        double[] uniformLower = new double[this.dataLower.length];
        double[] uniformUpper = new double[this.dataUpper.length];

        this.uniformData = new double[uniformLower.length + uniformUpper.length];
        double uniformLowerSum = 0;
        double uniformUpperSum = 0;
        for(int index = 0; index < this.dataLower.length; index++) {
            if(LUT[index] == true) {
                // access to data is save after prepare()
                uniformLower[index] = this.dataLower[index];
                uniformUpper[index] = this.dataUpper[index];
            } else {
                uniformLowerSum += this.dataLower[index];
                uniformUpperSum += this.dataUpper[index];
            }
        }
        uniformLower[9] = uniformLowerSum;
        uniformUpper[9] = uniformUpperSum;

        System.arraycopy(uniformUpper, 0, this.uniformData, 0, uniformUpper.length);
        System.arraycopy(uniformLower, 0, this.uniformData, uniformUpper.length, uniformLower.length);
        this.uniformData = normalize(this.uniformData);
        return this.uniformData;
    }

    /**
     * Data has to be extracted from the lower and upper part separately.
     * <p>
     * @return Concatenated upper and lower rotation invariant histogram.
     * <p>
     * @throws jceliac.JCeliacLogableException
     */
    @Override
    public double[] getRotatedData()
            throws JCeliacLogableException
    {
        if(this.rotatedData != null) {
            return this.rotatedData;
        }

        this.prepare();
        // access to data is save from here
        double[] rotatedLower = new double[this.dataLower.length];
        double[] rotatedUpper = new double[this.dataUpper.length];

        int neighbors = getNumberOfNeighbors(this.data.length);
        if(neighbors == 8) {
            this.rotinvLUT = LBPHistogram.rotationLUT8;
        } else {
            this.rotinvLUT = LBPHistogram.computeRotationLUT(neighbors);
        }
        for(int index = 0; index < this.dataLower.length; index++) {
            int rotatedCode = this.rotinvLUT[index];
            rotatedLower[rotatedCode] += this.dataLower[index];
            rotatedUpper[rotatedCode] += this.dataUpper[index];
        }
        this.rotatedData = new double[rotatedLower.length + rotatedUpper.length];
        System.arraycopy(rotatedUpper, 0, this.rotatedData, 0, rotatedUpper.length);
        System.arraycopy(rotatedLower, 0, this.rotatedData, rotatedUpper.length, rotatedLower.length);
        this.rotatedData = normalize(this.rotatedData);
        return this.rotatedData;
    }

    /**
     * Data has to be extracted from the lower and upper part separately.
     * <p>
     * @return Concatenated upper and lower rotation invariant uniform
     *         histogram.
     * <p>
     * @throws jceliac.JCeliacLogableException
     */
    @Override
    public double[] getRotatedUniformData()
            throws JCeliacLogableException
    {
        if(this.rotatedUniformData != null) {
            return this.rotatedUniformData;
        }

        this.prepare();
        // access to data is save from here
        boolean[] LUT = LBPHistogram.getUniformLUT(this.dataUpper.length);

        double[] uniformLower = new double[this.dataLower.length];
        double[] uniformUpper = new double[this.dataUpper.length];

        for(int index = 0; index < this.dataLower.length; index++) {
            if(LUT[index] == true) {
                // access to data is save after prepare()
                uniformLower[index] = this.dataLower[index];
                uniformUpper[index] = this.dataUpper[index];
            }
        }
        // access to data is save from here
        double[] rotatedLower = new double[this.dataLower.length];
        double[] rotatedUpper = new double[this.dataUpper.length];

        int neighbors = getNumberOfNeighbors(this.data.length);
        if(neighbors == 8) {
            this.rotinvLUT = LBPHistogram.rotationLUT8;
        } else {
            this.rotinvLUT = LBPHistogram.computeRotationLUT(neighbors);
        }
        for(int index = 0; index < this.dataLower.length; index++) {
            int rotatedCode = this.rotinvLUT[index];
            rotatedLower[rotatedCode] += uniformLower[index];
            rotatedUpper[rotatedCode] += uniformUpper[index];
        }
        this.rotatedUniformData = new double[rotatedLower.length + rotatedUpper.length];
        System.arraycopy(rotatedUpper, 0, this.rotatedUniformData, 0, rotatedUpper.length);
        System.arraycopy(rotatedLower, 0, this.rotatedUniformData, rotatedUpper.length, rotatedLower.length);
        this.rotatedUniformData = normalize(this.rotatedUniformData);
        return this.rotatedUniformData;
    }

    @Override
    public void disposeData()
    {
        this.data = null;
        this.uniformData = null;
        this.dataLower = null;
        this.dataUpper = null;
    }

    public static double[] normalizeUpper(double[] hist)
    {
        double sum = 0;
        double[] nhist = new double[hist.length];
        for(int index = 0; index < hist.length; index++) {
            sum += hist[index];
        }
        if(sum == 0) // nasty bug, forgot this took me 2 days to find :/
        {
            return hist;
        }
        for(int index = 0; index < hist.length; index++) {
            nhist[index] = (hist[index] / sum);
        }
        return nhist;
    }

    public static double[] normalizeLower(double[] hist)
    {
        double sum = 0;
        double[] nhist = new double[hist.length];
        for(int index = 0; index < hist.length; index++) {
            sum += hist[index];
        }
        if(sum == 0) // nasty bug, forgot this took me 2 days to find :/
        {
            return hist;
        }
        for(int index = 0; index < hist.length; index++) {
            nhist[index] = (hist[index] / sum);
        }
        return nhist;
    }

    public double[] getRawDataLower()
    {
        return dataLower;
    }

    public double[] getRawDataUpper()
    {
        return dataUpper;
    }

}
