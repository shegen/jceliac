/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.LTP;

import jceliac.featureExtraction.LBP.FeatureExtractionParametersLBP;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersLTP
        extends FeatureExtractionParametersLBP
        implements ILBPThresholds
{

    public FeatureExtractionParametersLTP()
    {

    }

    /**
     * The beta values are found using statistics on the training data, do not
     * set them manually!
     */
    private double thresholdBeta;

    /**
     * Alpha is used as a weight to define how much impact the standard
     * deviation of the current signal sample has. Diagonal sub-band.
     */
    private double thresholdAlpha = 0.1;

    @Override
    public double getThresholdAlpha()
    {
        return this.thresholdAlpha;
    }

    @Override
    public double getThresholdBeta()
    {
        return this.thresholdBeta;
    }

    @Override

    public void setThresholdAlpha(double thresholdAlpha)
    {
        this.thresholdAlpha = thresholdAlpha;
    }

    @Override
    public void setThresholdBeta(double thresholdBeta)
    {
        this.thresholdBeta = thresholdBeta;
    }

    @Override
    public void report()
    {
        super.report(); 
    }

    @Override
    public String getMethodName()
    {
        return "LTP";
    }

}
