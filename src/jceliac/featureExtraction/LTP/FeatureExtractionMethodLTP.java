/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.LTP;

import jceliac.featureExtraction.affineScaleLBPScaleBias.IAffineAdaptiveFeatureExtractionMethod;
import jceliac.featureExtraction.SALBP.IScaleAdaptiveFeatureExtractionMethod;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ScaleEstimation;
import jceliac.featureExtraction.SALBP.LBPScaleFilteredDataSupplier;
import jceliac.featureExtraction.FeatureExtractionMethod.EFeatureExtractionMethod;
import jceliac.featureExtraction.affineScaleLBPScaleBias.AffineFilteredNeighbor;
import jceliac.featureExtraction.SALBP.LBPScaleBias;
import jceliac.featureExtraction.affineScaleLBP.EllipticPointLookup;
import jceliac.featureExtraction.affineScaleLBP.EllipticPoint;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.LBP.*;
import jceliac.featureExtraction.*;
import jceliac.tools.filter.*;
import jceliac.experiment.*;
import jceliac.tools.data.*;
import jceliac.logging.*;
import java.util.*;
import jceliac.featureExtraction.SALBP.FeatureExtractionParametersScaleAdaptiveLBP;
import jceliac.featureExtraction.SALBP.LBPNeighborLookup;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.OrientationEstimation;

/**
 * Local Ternary Patterns
 * <p>
 * @author shegen
 */
@StableCode(comment = "This code is trusted.",
            date = "16.01.2014",
            lastModified = "16.01.2014")
public class FeatureExtractionMethodLTP
        extends FeatureExtractionMethodLBP
        implements IScaleAdaptiveFeatureExtractionMethod,
                   IAffineAdaptiveFeatureExtractionMethod
{

    private final FeatureExtractionParametersLTP myParameters;

    private class IterativeVariables
    {

        public int[] pixelDistribution;

        public IterativeVariables()
        {
            this.pixelDistribution = new int[256];
        }
    }
    private final IterativeVariables iterativeVariables;

    public FeatureExtractionMethodLTP(FeatureExtractionParameters param)
    {
        super(param);

        // this is just for convenience
        this.myParameters = (FeatureExtractionParametersLTP) super.parameters;

        // setup my data structs
        this.iterativeVariables = new IterativeVariables();

    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content)
            throws JCeliacGenericException
    {

        Experiment.log(JCeliacLogger.LOG_HINFO, "Extracting LBP-Features from SignalID: %s, FrameID: %d.",
                       content.getSignalIdentifier(), content.getFrameIdentifier());

        // create DiscriminativeFeatures structure once for each input sample
        LBPFeatures features = new LBPFeatures(this.myParameters);
        features.setSignalIdentifier(content.getSignalIdentifier());
        features.setFrameNumber(content.getFrameIdentifier());

        for(int currentScale = myParameters.getMinScale();
            currentScale <= myParameters.getMaxScale(); currentScale++) {
            if(myParameters.isUseColor()) {
                // Prepare content for scale.

                for(Integer colorChannel : this.myParameters.getColorChannels()) {
                     LTPHistogram hist = this.computeDistribution(content.getColorChannel(colorChannel), 
                                                                  currentScale);
                     hist.setColorChannel(colorChannel);
                     hist.setScale(currentScale);
                     features.addAbstractFeature(hist);
                }
            } else {
                LTPHistogram histGray = computeDistribution(content.getGrayData(), currentScale);
                histGray.setColorChannel(DataSource.CHANNEL_GRAY);
                histGray.setScale(currentScale);
                features.addAbstractFeature(histGray);
            }
        }
        return features;
    }

    /* Results validated on 01.10.2010 compliant to the matlab and mex
     * implementation (exception rounding errors).
     */
    @Override
    protected LTPHistogram computeDistribution(double [][] dataChannel, int scale)
            throws JCeliacGenericException
    {
        LTPHistogram ltpHistogram = new LTPHistogram();

        double[][] data;
        double radius = LBPMultiscale.getRadiusForScale(scale);
        int neighbors = LBPMultiscale.getNeighborsForScale(scale);
        int margin = (int) Math.ceil(radius);
        double[] histUpper = new double[getNumberOfBins(neighbors)];
        double[] histLower = new double[getNumberOfBins(neighbors)];

        DataFilter2D filter = LBPMultiscale.getFilterForScale(scale);
        data = filter.filterData(dataChannel);
        
        double threshold;
        /* double stdev = std(data);
         *
         * if(stdev >= myParameters.getThresholdBeta()) { threshold =
         * myParameters.getThresholdBeta() + myParameters.getThresholdAlpha() *
         * stdev; }else { threshold = myParameters.getThresholdBeta() -
         * myParameters.getThresholdAlpha() * stdev; } */

        // changed for the affine scale lbp experiments to reflect the description in the original paper
        threshold = 5.0d; // the only mentioned value in the paper
        int shiftMax = neighbors - 1;
        for(int x = margin; x < data.length - margin; x++) {
            for(int y = margin; y < data[0].length - margin; y++) {

                // THE MATLAB CODE WAS BUGGED, this is correct!
                double center = (double) data[x][y];
                int patternUpper = 0;
                int patternLower = 0;

                for(int p = 0; p < neighbors; p++) {
                    double neighbor = getNeighbor(data, x, y, p, scale);

                    if(neighbor >= (center + threshold)) {
                        // this is in reverse order from the matlab thingy to speed up things
                        patternUpper |= (1 << (shiftMax - p));
                    } else if(neighbor <= (center - threshold)) {
                        // this is in reverse order from the matlab thingy to speed up things
                        patternLower |= (1 << (shiftMax - p));
                    } else {
                        // zero
                    }
                }
                /* reorder pattern to comply with the other implementations,
                 * freaking hokus pokus!! pick top 3 bit bits from msb
                 */
                int tmphUpper = patternUpper & 0xE0; // 224
                // pick bottom 5 bits from lsb
                int tmplUpper = patternUpper & 0x1F; // 31;
                int patternUpperFixed = (tmphUpper >> 5) | (tmplUpper << 3);

                /* reorder pattern to comply with the other implementations,
                 * freaking hokus pokus!! pick top 3 bit bits from msb
                 */
                int tmphLower = patternLower & 0xE0; // 224
                // pick bottom 5 bits from lsb
                int tmplLower = patternLower & 0x1F; // 31;
                int patternLowerFixed = (tmphLower >> 5) | (tmplLower << 3);

                histUpper[patternUpperFixed] = histUpper[patternUpperFixed] + 1.0d;
                histLower[patternLowerFixed] = histLower[patternLowerFixed] + 1.0d;
            }
        }
        ltpHistogram.setDataLower(histLower);
        ltpHistogram.setDataUpper(histUpper);
        return ltpHistogram;
    }

    /**
     * This estimates the thresholds used within the LTP operator. The
     * thresholds are calculated as: stdev(content) > beta => beta + alpha *
     * stdev(content); stdev(content) < beta => beta + alpha * stdev(content);
     * The value alpha is a weighting factor and is parametrizeable. The value
     * beta is estimated by this method as the standard deviation of all images
     * (pixel intensity deviation).
     * <p>
     * @param dataReaders Data readers for training images.
     * @throws jceliac.JCeliacGenericException
     */
    @Override
    public void estimateParameters(ArrayList<DataReader> dataReaders)
            throws JCeliacGenericException
    {
        double meanSum = 0;
        double mean;
        double count = 0;
        double sum = 0;

        Experiment.log(JCeliacLogger.LOG_INFO, "Estimating LTP Parameters (Treshold Beta).");

        for(int classNum = 0; classNum < dataReaders.size(); classNum++) {
            DataReader current = dataReaders.get(classNum);
            while(current.hasNext()) {
                DataSource currentFile = (DataSource) current.next();
                ContentStream cstream = currentFile.createContentStream();

                while(cstream.hasNext()) {
                    Frame content = (Frame) cstream.next();
                    double[][] data = content.getGrayData();
                    for(double[] tmpData : data) {
                        for(int y = 0; y < data[0].length; y++) {
                            meanSum += tmpData[y];
                            count++;
                        }
                    }
                }
            }
            current.reset();
        }
        mean = (meanSum / count);

        for(int classNum = 0; classNum < dataReaders.size(); classNum++) {
            DataReader current = dataReaders.get(classNum);
            while(current.hasNext()) {
                DataSource currentFile = (DataSource) current.next();
                ContentStream cstream = currentFile.createContentStream();

                while(cstream.hasNext()) {
                    Frame content = (Frame) cstream.next();
                    double[][] data = content.getGrayData();
                    for(double[] tmpData : data) {
                        for(int y = 0; y < data[0].length; y++) {
                            double tmp = tmpData[y];
                            sum += (tmp - mean) * (tmp - mean);
                        }
                    }

                }
            }
            current.reset();
        }
        double stdev = Math.sqrt(sum / (count - 1)); // compliant to matlab std(X)
        Experiment.log(JCeliacLogger.LOG_INFO, "Grayscale Pixel Standard Deviation: %f.", stdev);
        this.myParameters.setThresholdBeta(Math.sqrt(stdev));
    }

    /**
     * Same as estimate parameters but iterative version. This is used by an
     * operator that used sub-operators to save memory consumption.
     * OutOfMemoryErrors were frequent.
     * <p>
     * @param content Frame content of a single frame.
     */
    @Override
    public void estimateParametersIterative(Frame content)
    {
        double[][] data = content.getGrayData();
        double meanSum = 0;
        int count = 0;
        for(double[] tmpData : data) {
            for(int y = 0; y < data[0].length; y++) {
                double pixel = tmpData[y];
                this.iterativeVariables.pixelDistribution[(int) Math.round(pixel)]++;
            }
        }
        for(int i = 0; i < this.iterativeVariables.pixelDistribution.length; i++) {
            meanSum += i * this.iterativeVariables.pixelDistribution[i];
            count += this.iterativeVariables.pixelDistribution[i];
        }
        double mean = meanSum / (double) count;
        double stdSum = 0;
        for(int i = 0; i < this.iterativeVariables.pixelDistribution.length; i++) {
            int tmp = this.iterativeVariables.pixelDistribution[i];
            stdSum += (((i - mean) * (i - mean)) * tmp);
        }
        double stdev = Math.sqrt(stdSum / (count - 1)); // compliant to matlab std(X)
        this.myParameters.setThresholdBeta(Math.sqrt(stdev));
    }

    /**
     * Part of the scale-adaptive implementations of
     * IScaleAdaptiveFeatureExtractionMethod. Computes the LBP patterns in s
     * scale-adaptive fashion, called by the specific SOALBP and SALBP methods.
     * <p>
     * @param filteredDataSupplier Data supplier of correctly scale-adaptively
     *                             filtered image data.
     * @param scaleBias            The used scale bias for computation (this is
     *                             called trained base scale in the
     *                             publication).
     * @param localScaleEstimation The estimation of scale of the current image.
     * <p>
     * @param neighborLookup Lookup table for LBP neighbors. 
     * @return Scale-Adaptively computed LBP-histogram.
     * <p>
     * @throws JCeliacGenericException
     */
    @Override
    public LBPHistogram performScaleAdaptiveFeatureExtraction(LBPScaleFilteredDataSupplier filteredDataSupplier,
                                                              LBPScaleBias scaleBias,
                                                              ScaleEstimation localScaleEstimation,
                                                              LBPNeighborLookup neighborLookup)
            throws JCeliacGenericException
    {

        LTPHistogram histogram = new LTPHistogram();
        double[] histLTPUpper = new double[getNumberOfBins(8)];
        double[] histLTPLower = new double[getNumberOfBins(8)];
        int computedPatterns = 0;
        int possiblyComputedPatterns = 0;

        // create elliptic pionts once and recycle to speedup 
        double [][] points = new double[8][2];

        for(int scaleLevel = 0;
            scaleLevel < scaleBias.getScalespaceParameters().getNumberOfScales();
            scaleLevel++) {

            double confidence = localScaleEstimation.getConfidenceForScale(scaleLevel);
            if(confidence < this.myParametersSA.getMinConfidenceLevel()) {
                continue;
            }
            double lbpRadius = scaleBias.getLBPRadiusForScaleLevel(scaleLevel);

            // this will deliver the correctly filtered data for the lbp radius
            double[][] data = filteredDataSupplier.retrieveLBPScaleFilteredDataForLBPRadius(lbpRadius);
            double stdev = filteredDataSupplier.getStdevForFilteredDataForLBPRadius(lbpRadius);
            for(int x = 2; x < data.length - 2; x++) {
                for(int y = 2; y < data[0].length - 2; y++) {
                    possiblyComputedPatterns++;
                    try {
                        neighborLookup.lookup(lbpRadius, points);
                        int[][] ltpPatterns = this.extractScaleAdaptiveLTP(data, x, y, stdev, points);

                        // weight ehe patterns such that each interestpoint adds the same amount to the histogram 
                        // no matter of the the size of the sigma of the interestpoint
                        histLTPLower[ltpPatterns[0][0]] = histLTPLower[ltpPatterns[0][0]] + confidence;
                        histLTPUpper[ltpPatterns[0][1]] = histLTPUpper[ltpPatterns[0][1]] + confidence;
                        computedPatterns++;
                    } catch(ArrayIndexOutOfBoundsException e) { // ignored, do not border handling
                    }
                }
            }
        }
        histLTPLower[0] = 0;
        histLTPUpper[0] = 0;
        histogram.setDataLower(histLTPLower);
        histogram.setDataUpper(histLTPUpper);
        histogram.setComputedPatternRatio((double) computedPatterns / (double) possiblyComputedPatterns);
        return histogram;
    }

    

    
    /**
     * Performs the actual computation of scale-adaptive patterns.
     * <p>
     * @param filteredData Scale-adaptively filtered data.
     * @param x            Coordinate x.
     * @param y            Coordinate y.
     * @param stdev        Standard deviation of filtered data.
     * @param points       Neighbor positions.
     * <p>
     * @return LBP pattern at x,y.
     * <p>
     */
    protected int[][] extractScaleAdaptiveLTP(double[][] filteredData, int x, int y, 
                                              double stdev, double [][] points)
    {
        int shiftMax = points.length - 1;
        int patternUpper = 0;
        int patternLower = 0;

        double threshold = Math.sqrt(stdev);
        double center = filteredData[x][y];
        for(int p = 0; p < points.length; p++) {
            double neighbor = 0;

            if(x+points[p][0] < filteredData.length - 1 && y-points[p][1] < filteredData[0].length - 1
               && x+points[p][0] >= 0 && y-points[p][1] >= 0) {
                neighbor = this.getInterpolatedPixelValueFast(filteredData, x+points[p][0], y-points[p][1]);
            } else {
                throw new ArrayIndexOutOfBoundsException();
            }
            if(neighbor >= (center + threshold)) {
                // this is in reverse order from the matlab thingy to speed up things
                patternUpper |= (1 << (shiftMax - p));
            } else if(neighbor <= (center - threshold)) {
                // this is in reverse order from the matlab thingy to speed up things
                patternLower |= (1 << (shiftMax - p));
            } else {
                // zero
            }
        }
        int alternatePatternLower = (patternLower & 0xF0) >> 4 | (patternLower & 0xF) << 4;
        int alternatePatternUpper = (patternUpper & 0xF0) >> 4 | (patternUpper & 0xF) << 4;
        int[][] ret = {{patternLower, patternUpper}, {alternatePatternLower, alternatePatternUpper}};
        return ret;
    }

    @Override
    public void finalizeHistogram(LBPHistogram histogram)
    {
        LTPHistogram ltpHistogram = (LTPHistogram) histogram;
        double[] histDataLower = ltpHistogram.getRawDataLower();
        double[] histDataUpper = ltpHistogram.getRawDataUpper();
        histDataLower[0] = 0;
        histDataUpper[0] = 0;
    }

    @Override
    public LBPHistogram initializeHistogram()
    {
        LTPHistogram ltpHistogram = new LTPHistogram();
        ltpHistogram.setDataLower(new double[256]);
        ltpHistogram.setDataUpper(new double[256]);
        return ltpHistogram;
    }

    /**
     * Computes affine adaptive patterns, called by external methods such as
     * SOALBP.
     * <p>
     * @param hist         Externally initialized LBP-Histogram, will be updated
     *                     by this method.
     * @param center       Object describing the center pixel.
     * @param neighbors    Array of Objects describing the neighbor pixels.
     * @param stdev        Standard deviation of corresponding scale-filtered
     *                     image data.
     * @param contribution Contribution of pattern based no scale-estimation
     *                     reliability.
     */
    @Override
    public void computeAffineAdaptivePattern(LBPHistogram hist, AffineFilteredNeighbor center,
                                             AffineFilteredNeighbor[] neighbors, double stdev, double contribution)
    {
        int shiftMax = 7;
        int patternLower = 0;
        int patternUpper = 0;

        double[] histDataLower;
        double[] histDataUpper;

        histDataLower = ((LTPHistogram) hist).getRawDataLower();
        histDataUpper = ((LTPHistogram) hist).getRawDataUpper();

        double centerValue = center.value;
        double threshold = Math.sqrt(stdev);
        for(int p = 0; p < neighbors.length; p++) {
            double neighborValue = neighbors[p].value;
            if(neighborValue >= (centerValue + threshold)) {
                // this is in reverse order from the matlab thingy to speed up things
                patternUpper |= (1 << (shiftMax - p));
            } else if(neighborValue <= (centerValue - threshold)) {
                // this is in reverse order from the matlab thingy to speed up things
                patternLower |= (1 << (shiftMax - p));
            } else {
                // zero
            }
        }
        int alternatePatternLower = (patternLower & 0xF0) >> 4 | (patternLower & 0xF) << 4;
        int alternatePatternUpper = (patternUpper & 0xF0) >> 4 | (patternUpper & 0xF) << 4;

        histDataLower[patternLower] += contribution;
        histDataUpper[patternUpper] += contribution;
        if(patternUpper != alternatePatternUpper) {
            histDataUpper[alternatePatternUpper] += contribution;
        }
        if(patternLower != alternatePatternLower) {
            histDataLower[alternatePatternLower] += contribution;
        }
    }
}
