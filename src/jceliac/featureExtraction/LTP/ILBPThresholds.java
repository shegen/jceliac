/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.LTP;

/**
 * We need this for ELTP, ELTP inherits from ELBP, but uses thresholding. This
 * simulates multiple inheritance.
 *
 * @author shegen
 */
public interface ILBPThresholds {

    public double getThresholdAlpha();

    public double getThresholdBeta();

    public void setThresholdAlpha(double a);

    public void setThresholdBeta(double b);

}
