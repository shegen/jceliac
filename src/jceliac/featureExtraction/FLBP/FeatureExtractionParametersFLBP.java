/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.FLBP;

import jceliac.experiment.*;
import jceliac.featureExtraction.LBP.FeatureExtractionParametersLBP;
import jceliac.logging.*;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersFLBP
        extends FeatureExtractionParametersLBP
{

    public static enum EMembershipFunction
    {

        LINEAR,
        LOGISTIC
    }

    /**
     * Type of membership function to use. Linear ("linear"): a linear type
     * function is used (default) Logistic ("logistic"): a logistic type
     * function is used
     */
    protected String membershipFunction = "linear";

    public FeatureExtractionParametersFLBP()
    {
    }

    @Override
    public void report()
    {
        super.report();
        Experiment.log(JCeliacLogger.LOG_INFO, "Membership Function: %s", this.membershipFunction);
    }

    @Override
    public String getMethodName()
    {
        return "FLBP";
    }

    public EMembershipFunction getMembershipFunction()
    {
        if(this.membershipFunction.compareToIgnoreCase("linear") == 0) {
            return EMembershipFunction.LINEAR;
        }

        if(this.membershipFunction.compareToIgnoreCase("logistic") == 0) {
            return EMembershipFunction.LOGISTIC;
        }

        Experiment.log(JCeliacLogger.LOG_INFO, "Unknown Membership Function Type using Default!");
        return EMembershipFunction.LINEAR; // default
    }

    public void setMembershipFunction(String membershipFunction)
    {
        this.membershipFunction = membershipFunction;
    }

}
