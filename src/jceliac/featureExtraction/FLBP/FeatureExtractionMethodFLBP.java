/*
 *  JCeliac Copyright (C) 2010-2011 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction.FLBP;

import jceliac.featureExtraction.affineScaleLBPScaleBias.IAffineAdaptiveFeatureExtractionMethod;
import jceliac.featureExtraction.SALBP.IScaleAdaptiveFeatureExtractionMethod;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ScaleEstimation;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.SALBP.LBPScaleFilteredDataSupplier;
import jceliac.featureExtraction.affineScaleLBPScaleBias.AffineFilteredNeighbor;
import jceliac.featureExtraction.SALBP.LBPScaleBias;
import jceliac.featureExtraction.affineScaleLBP.EllipticPointLookup;
import jceliac.featureExtraction.affineScaleLBP.EllipticPoint;
import jceliac.featureExtraction.affineScaleLBP.Ellipse;
import jceliac.featureExtraction.StableCode;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.LBP.*;
import java.util.*;
import jceliac.featureExtraction.SALBP.LBPNeighborLookup;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.OrientationEstimation;

/**
 * Fuzzy Local Binary Patterns.
 * <p>
 * @author shegen
 */
@StableCode(date = "16.01.2014", lastModified = "16.01.2014")
public class FeatureExtractionMethodFLBP
        extends FeatureExtractionMethodLBP
        implements IScaleAdaptiveFeatureExtractionMethod,
                   IAffineAdaptiveFeatureExtractionMethod
{

    protected double THRESHOLD = 4.5d;
    private final FeatureExtractionParametersFLBP myParameters;
    private double logisticBeta;
    private HashMap<Integer, Double> LUT;
    private FeatureExtractionParametersFLBP.EMembershipFunction memFunc;

    public FeatureExtractionMethodFLBP(FeatureExtractionParameters param)
    {
        super(param);

        // this is just for convenience
        this.myParameters = (FeatureExtractionParametersFLBP) param;
        this.memFunc = this.myParameters.getMembershipFunction();
        this.memFunc = FeatureExtractionParametersFLBP.EMembershipFunction.LINEAR;
    }

    @Override
    public void initialize()
            throws JCeliacGenericException
    {
        super.initialize();
        this.logisticBeta = this.calculateLogisticParameterForThreshold(this.THRESHOLD);
        this.LUT = this.prepareLookupTable(this.THRESHOLD);
    }

    @Override
    public LBPHistogram computeHistogram(double[][] data, int neighbors, double radius, int margin, int scale)
    {

        double[] hist = new double[getNumberOfBins(neighbors)];

        for(int x = margin; x < data.length - margin; x++) {
            for(int y = margin; y < data[0].length - margin; y++) {
                double center = (double) data[x][y];

                // collect neighbors
                double[] neighborsArray = new double[neighbors];
                for(int p = 0; p < neighbors; p++) {
                    double neighbor = getNeighbor(data, x, y, p, scale);
                    neighborsArray[p] = neighbor;
                }
                double[] contributions = new double[getNumberOfBins(neighbors)];
                for(int lbpCode = 0; lbpCode < contributions.length; lbpCode++) {
                    contributions[lbpCode] = this.calculateContributionOfPattern(lbpCode, neighborsArray, center, THRESHOLD);
                    hist[lbpCode] += contributions[lbpCode];

                }
            }
        }
        LBPHistogram lbpHistogram = new LBPHistogram();
        lbpHistogram.setData(hist);
        return lbpHistogram;
    }

    /**
     * Get Maximum contributions.
     * <p>
     * @param contributions All Contributions
     * <p>
     * @return
     */
    protected double getMaxContribution(double[] contributions)
    {
        double max = 0;
        for(int index = 0; index < contributions.length; index++) {
            if(contributions[index] > max) {
                max = contributions[index];
            }
        }
        return max;
    }

    /**
     * Compute contribution of a single LBP pattern.
     * <p>
     * @param lbp       LBP pattern.
     * @param neighbors LBP neighbors.
     * @param center    LBP center.
     * @param threshold Threshold
     * <p>
     * @return
     */
    protected double calculateContributionOfPattern(int lbp, double[] neighbors,
                                                    double center, double threshold)
    {
        double contribution = 1.0d;
        for(int p = 0; p < neighbors.length; p++) {
            double m;
            if((lbp & (1 << p)) > 0) {
                m = calculateOneMembership(center, neighbors[p], threshold);
            } else {
                m = calculateZeroMembership(center, neighbors[p], threshold);
            }
            if(m == 0) {
                return 0;
            }
            contribution *= m;
        }
        return contribution;
    }

    /**
     * Compute contribution of a single LBP pattern faster implementation.
     * <p>
     * @param lbp       LBP pattern.
     * @param neighbors LBP neighbors.
     * @param center    LBP center.
     * @param threshold Threshold
     * <p>
     * @return
     */
    private double calculateContributionOfPatternFast(int lbp, AffineFilteredNeighbor[] neighbors,
                                                      double center, double threshold)
    {
        double TLow = center - threshold;
        double THigh = center + threshold;
        double contribution = 1.0d;

        for(int p = 0; p < neighbors.length; p++) {
            double m;

            if(neighbors[p].value <= TLow) {
                m = 1.0d;
            } else if(neighbors[p].value >= THigh) {
                m = 0;
            } else {
                m = (((threshold - neighbors[p].value) + center) / (2.0d * threshold));
            }

            if((lbp & (1 << p)) > 0) {
                m = 1.0d - m;
            }
            if(m == 0) {
                return 0;
            }
            contribution *= m;
        }
        return contribution;
    }

    /**
     * Compute contribution of a single LBP pattern faster implementation.
     * <p>
     * @param lbp       LBP pattern.
     * @param neighbors LBP neighbors.
     * @param center    LBP center.
     * @param threshold Threshold
     * <p>
     * @return
     */
    private double calculateContributionOfPatternFast(int lbp, double[] neighbors,
                                                      double center, double threshold)
    {
        double TLow = center - threshold;
        double THigh = center + threshold;
        double contribution = 1.0d;

        for(int p = 0; p < neighbors.length; p++) {
            double m;

            if(neighbors[p] <= TLow) {
                m = 1.0d;
            } else if(neighbors[p] >= THigh) {
                m = 0;
            } else {
                m = (((threshold - neighbors[p]) + center) / (2.0d * threshold));
            }

            if((lbp & (1 << p)) > 0) {
                m = 1.0d - m;
            }
            if(m == 0) {
                return 0;
            }
            contribution *= m;
        }
        return contribution;
    }

    /**
     * Calculates the zero membership of a single bit in the pattern.
     * <p>
     * @param center    LBP center value.
     * @param neighbor  LBP neighbor value.
     * @param threshold Threshold
     * <p>
     * @return The zero membership of the bit representing the neighbor.
     */
    protected double calculateZeroMembership(double center, double neighbor, double threshold)
    {
        if(neighbor >= (center + threshold)) // neighbor >= center -> pattern 1 -> membership is 0
        {
            return 0.0d;
        }
        if(neighbor <= (center - threshold)) // neighber <= center -> pattern 0 -> membership is 1
        {
            return 1.0d;
        }

        switch(this.memFunc) {
            case LINEAR:
                return linear((threshold - neighbor) + center, threshold);
            case LOGISTIC:
                return logisticSigmoid(center - neighbor, logisticBeta); // treshold is implicit within the beta

        }
        // default is linear
        return (((threshold - neighbor) + center) / (2.0d * threshold));
    }

    /**
     * Calculates the one membership of a single bit in the pattern.
     * <p>
     * @param center    LBP center value.
     * @param neighbor  LBP neighbor value.
     * @param threshold Threshold
     * <p>
     * @return The one membership of the bit representing the neighbor.
     */
    protected double calculateOneMembership(double center, double neighbor, double threshold)
    {
        return (1.0d - calculateZeroMembership(center, neighbor, threshold));
    }

    /**
     * Linear membership function.
     * <p>
     * @param x         Difference of center and neighbor.
     * @param threshold Threshold
     * <p>
     * @return Membership of neighbor.
     */
    protected double linear(double x, double threshold)
    {
        return x / (2 * threshold);
    }

    /**
     * Dichtefunktion der Logistischen Verteilung, alpha = 0 -> symmetrisch
     * private double logisticSigmoid(double x, double alpha, double beta) {
     * double e1 = Math.exp(-alpha/beta); double e2 = Math.exp(-(1/beta)*x);
     * return 1 / (1+e1*e2); } optimized for the symmetric case (alpha = 0)
     * <p>
     * @param x    Difference of center and neighbor.
     * @param beta Parameter of the sigmoid function.
     * <p>
     * @return Membership of neighbor.
     */
    protected double logisticSigmoid(double x, double beta)
    {
        double e2 = Math.exp(-(x / beta));
        return 1 / (1 + e2);
    }

    /** If the value of the differences between neighbor and center is on the
     * "left" of the threshold or the "right" of the threshold the membership is
     * 1 or 0, we calculate the beta value for the logistic function such that
     * the value of the function (which is asymptotic) to be below 1/100 and
     * then "cut off" the rest.
     * <p>
     * @param threshold Threshold
     * <p>
     * @return Logistic threshold parameter.
     */
    protected double calculateLogisticParameterForThreshold(double threshold)
    {
        // some algebra gives this assuming alpha is 0, this means at the point
        // 0-threshold is <= 0.001 and 0+threshold is >= 0.999
        return (threshold / Math.log(100));
    }

    /**
     * Computes lookup table for logisticSigmoid function.
     * <p>
     * @param threshold Threshold to use for computation.
     * <p>
     * @return Lookup table for logisticSigmoid function
     */
    protected HashMap<Integer, Double> prepareLookupTable(double threshold)
    {
        HashMap<Integer, Double> lookup = new HashMap<>();
        for(int index = (int) -Math.ceil(threshold); index <= (int) Math.ceil(threshold); index++) {
            double val = logisticSigmoid(index, this.logisticBeta);
            lookup.put(index, val);
        }
        return lookup;
    }

    /**
     * Perofrms the scale adaptive feature extraction, called by SAFLBP.
     * <p>
     * @param filteredDataSupplier Standard filtered data supplier.
     * @param scaleBias            Scale bias to use for computation.
     * @param localScaleEstimation Local scale estimation of the image.
     * @param neighborLookup Lookup table for LBP neighbors. 
     * @return Scale adapted FLBP features.
     * <p>
     * @throws JCeliacGenericException
     */
    @Override
    public LBPHistogram performScaleAdaptiveFeatureExtraction(LBPScaleFilteredDataSupplier filteredDataSupplier,
                                                              LBPScaleBias scaleBias,
                                                              ScaleEstimation localScaleEstimation,
                                                              LBPNeighborLookup neighborLookup)
            throws JCeliacGenericException
    {

        LBPHistogram histogram = new LBPHistogram();
        double[] histFuzzy = new double[getNumberOfBins(8)];
        double [][] points = new double[8][2];

        for(int scaleLevel = 0;
            scaleLevel < scaleBias.getScalespaceParameters().getNumberOfScales();
            scaleLevel++) {

            double confidence = localScaleEstimation.getConfidenceForScale(scaleLevel);
            if(confidence < this.myParametersSA.getMinConfidenceLevel()) {
                continue;
            }
            double lbpRadius = scaleBias.getLBPRadiusForScaleLevel(scaleLevel);
            // this will deliver the correctly filtered data for the lbp radius
            double[][] data = filteredDataSupplier.retrieveLBPScaleFilteredDataForLBPRadius(lbpRadius);
            double stdev = filteredDataSupplier.getStdevForFilteredDataForLBPRadius(lbpRadius);

            for(int x = 2; x < data.length - 2; x++) {
                for(int y = 2; y < data[0].length - 2; y++) {

                    try {
                        // compute features
                        neighborLookup.lookup(lbpRadius, points);
                        this.extractScaleAdaptiveFuzzyLBP(histFuzzy, data, x, y, stdev, points, confidence);
                    } catch(ArrayIndexOutOfBoundsException e) { // ignored
                    }

                }
            }
        }
        histogram.setData(histFuzzy);
        return histogram;
    }

     
    

    /**
     * Performs the actual computation of scale adaptive FLBP.
     * <p>
     * @param hist         Histogram of FLBP, will be updated by the method.
     * @param filteredData Scale adapted data.
     * @param x            X coordinate in image.
     * @param y            Y coordinate in image.
     * @param stdev        Standard deviation of image at adapted scale.
     * @param points       Point positions of neighbors.
     * @param confidence   Scale estimation reliability.
     * <p>
     */
    protected void extractScaleAdaptiveFuzzyLBP(double[] hist, double[][] filteredData,
                                                int x, int y, double stdev,
                                                double [][] points, double confidence)
    {
        double[] neighborsArray = new double[8];
        for(int p = 0; p < neighborsArray.length; p++) {
            neighborsArray[p] = super.getInterpolatedPixelValueFast(filteredData, x+points[p][0], y-points[p][1]);
        }

        double threshold = Math.sqrt(stdev);
        double center = filteredData[x][y];
        for(int lbpCode = 0; lbpCode < 256; lbpCode++) {
            double fuzzyPatternContribution = this.calculateContributionOfPatternFast(lbpCode, neighborsArray, center, threshold);
            hist[lbpCode] += fuzzyPatternContribution * confidence;
        }
    }

    /**
     * Performs the actual computation of scale adaptive FLBP.
     * <p>
     * @param hist        Histogram of FLBP, will be updated by the method.
     * @param centerPoint LBP center point.
     * @param points      LBP neighbors.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    protected void extractAffineAdaptiveFuzzyLBP(double[] hist, AffineFilteredNeighbor centerPoint,
                                                 AffineFilteredNeighbor[] points)
            throws JCeliacGenericException
    {
        double[] contributions = new double[256];
        double[] neighborsArray = new double[8];

        for(int p = 0; p < neighborsArray.length; p++) {
            neighborsArray[p] = points[p].value;
        }
        double threshold = 5.0d;
        double center = centerPoint.value;
        for(int lbpCode = 0; lbpCode < 256; lbpCode++) {
            contributions[lbpCode] = calculateContributionOfPattern(lbpCode, neighborsArray, center, threshold);
            hist[lbpCode] += contributions[lbpCode];
        }
    }

    @Override
    public void computeAffineAdaptivePattern(LBPHistogram hist, AffineFilteredNeighbor center,
                               AffineFilteredNeighbor[] neighbors, double stdev,
                               double contribution)
    {
        double[] histData = hist.getRawData();

        double threshold = Math.sqrt(stdev);
        double centerValue = center.value;
        for(int lbpCode = 0; lbpCode < histData.length; lbpCode++) {
            int alternatePattern = (lbpCode & 0xF0) >> 4 | (lbpCode & 0xF) << 4;
            double fuzzyPatternContribution = this.calculateContributionOfPatternFast(lbpCode, neighbors, centerValue, threshold);

            histData[lbpCode] += (fuzzyPatternContribution * contribution);
            if(alternatePattern != lbpCode) {
                histData[alternatePattern] += (fuzzyPatternContribution * contribution);
            }
        }
    }

}
