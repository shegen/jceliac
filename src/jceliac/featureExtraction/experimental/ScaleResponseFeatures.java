/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.featureExtraction.experimental;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.classification.kernelSVM.SVMFeatureVector;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;

/**
 *
 * @author shegen
 */
public class ScaleResponseFeatures 
    extends DiscriminativeFeatures
{

    protected ArrayList <double []> localFeatureData;
    protected int scaleIdentifier;
    
    public ScaleResponseFeatures(FeatureExtractionParameters parameters)
    {
        super(parameters);
    }

    public ScaleResponseFeatures()
    {
    }

    
    @Override
    public DiscriminativeFeatures cloneEmptyFeature()
    {
        ScaleResponseFeatures featureSubset = new ScaleResponseFeatures(super.featureExtractionParameters);
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
        featureSubset.localFeatureData = this.localFeatureData;
        
        return featureSubset;
    }

    @Override
    protected DiscriminativeFeatures cloneFeatureSubset(FeatureVectorSubsetEncoding subset)
            throws JCeliacGenericException
    {
        ScaleResponseFeatures featureSubset = new ScaleResponseFeatures(super.featureExtractionParameters);
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
        featureSubset.localFeatureData = this.localFeatureData;
        
        return featureSubset;
    }

    @Override
    public double doDistanceTo(DiscriminativeFeatures them) 
            throws JCeliacGenericException
    {
        throw new JCeliacGenericException("Feature is not supposed to be used before generative model mapping!");
    }

    @Override
    public ArrayList<double[]> getLocalFeatureVectorData()
    {
        return this.localFeatureData;
    }

    public void setLocalFeatureData(ArrayList<double[]> localFeatureData)
    {
        this.localFeatureData = localFeatureData;
    }

    public int getScaleIdentifier()
    {
        return scaleIdentifier;
    }

    public void setScaleIdentifier(int scaleIdentifier)
    {
        this.scaleIdentifier = scaleIdentifier;
    }
    
}
