/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.featureExtraction.experimental;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.ExperimentalCode;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.affineScaleLBPScaleBias.scaleEstimation.ScaleEstimation;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.Frame;
import jceliac.tools.filter.scalespace.LaplacianScalespaceRepresentation;
import jceliac.tools.filter.scalespace.ScaleLevelIndexedDataSupplier;
import jceliac.tools.filter.scalespace.ScalespaceParameters;
import jceliac.tools.filter.scalespace.ScalespaceRepresentation;


/**
 *
 * @author shegen
 */
@ExperimentalCode(
        comment = "Experimental",
        date = "27.03.2014",
        lastModified = "27.03.2014")
public class FeatureExtractionMethodScaleResponse 
    extends FeatureExtractionMethod
{

    protected ScalespaceParameters scalespaceParameters;
    
    public FeatureExtractionMethodScaleResponse(FeatureExtractionParameters param)
    {
        super(param);
        /* The scalespace is constructed such that the middle scale
         * sqrt(2)^0*c*sqrt(2) is 3 pixels wide. We define the scale bias to be
         * 3 pixels wide.
         */
        this.scalespaceParameters = new ScalespaceParameters(Math.sqrt(2.0d),
                                                             -4,
                                                             8,
                                                             0.25,
                                                             2.1214);
    }

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content) 
            throws JCeliacGenericException
    {
        ScaleResponseFeatures features = this.computeFeatures(content.getGrayData());
        features.setSignalIdentifier(content.getSignalIdentifier());
        return features;
    }
    
    public ScaleResponseFeatures computeFeatures(double [][] data)
            throws JCeliacGenericException
    {
        ScalespaceRepresentation scaleRep = new LaplacianScalespaceRepresentation(data, this.scalespaceParameters);
        scaleRep.computeScalespaceRepresentation(LaplacianScalespaceRepresentation.DETECTOR_TYPE_ABSOLUTE_SUM);
        ScaleLevelIndexedDataSupplier <double [][]> normalizedLaplacians = scaleRep.getNormalizedLaplacianDataSupplier();
        
        ScaleEstimation scaleEstimation = new ScaleEstimation(this.scalespaceParameters, normalizedLaplacians, 
                                                              data.length, data[0].length);
        scaleEstimation.estimateGlobalScale();
        int dominantScale = scaleEstimation.getEstimatedScaleLevel();
        
        ArrayList <double[]> localFeatureVectors = new ArrayList <>();
        for(int i = 0; i < data.length; i++) {
            for(int j = 0; j < data[0].length; j++) {
                double [] scaleResponsesAtPixel = new double[normalizedLaplacians.getScaleCount()];
                for(int scale = 0; scale < normalizedLaplacians.getScaleCount(); scale++) {
                    scaleResponsesAtPixel[scale] = normalizedLaplacians.getData(scale)[i][j];
                }
          //      if(this.isValidFeatureVector(scaleResponsesAtPixel, dominantScale)) {
                //    double [] featureVector = this.alignScaleResponses(scaleResponsesAtPixel, dominantScale);
          //          localFeatureVectors.add(featureVector);
          //      }
                    this.normalizeLinear(scaleResponsesAtPixel);
                localFeatureVectors.add(scaleResponsesAtPixel);
            }
        }
        ScaleResponseFeatures features = new ScaleResponseFeatures();
        features.setScaleIdentifier(dominantScale);
        features.setLocalFeatureData(localFeatureVectors);
        return features;
    }
    
    // normalize features linearly between 0 and 1
    protected void normalizeLinear(double [] data)
    {
        double min, max;

        max = ArrayTools.max(data);
        min = ArrayTools.min(data);
        double width = (max - min);
        double scaleFactor = (1.0d / width); 

        for(int y = 0; y < data.length; y++) {
            double k = data[y];
            double normalized = (k - min) * scaleFactor;
            data[y] = normalized;
        }
    }
     
    private boolean isValidFeatureVector(double [] scaleResponses, int dominantScale)
    {
      /*  int dominantLocalScale = ArrayTools.maxIndex(scaleResponses);
        if(Math.abs(dominantLocalScale - dominantScale) < 3) {
            return true;
        }
        return false;*/
        return true;
    }
    
    private double [] alignScaleResponses(double [] scaleResponses, int dominantScale)
    {
        double [] alignedFeatureVector = new double[scaleResponses.length];
        int index = dominantScale;
        int alignedIndex = 0;
        do 
        {
            alignedFeatureVector[alignedIndex] = scaleResponses[index];
            index = (index+1) % scaleResponses.length;
            alignedIndex++;
        }while(index != dominantScale);
        return alignedFeatureVector;
    }
            
    
    // <editor-fold desc="Initializers and parameter estimators, not used.">
    @Override
    public void initialize() throws JCeliacGenericException
    {
    }

    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass) throws
                                                                                JCeliacGenericException
    {
    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {
    }
    // </editor-fold>
    
}
