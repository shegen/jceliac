/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.classification.kernelSVM.SVMFeatureVector;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;

/**
 * Standard feature vector type, of features using euclidean distance to compute
 * between each other.
 * <p>
 * @author shegen
 */
public class StandardEuclideanFeatures
        extends DiscriminativeFeatures 
{

    public StandardEuclideanFeatures(FeatureExtractionParameters parameters)
    {
        super(parameters);
    }

    public StandardEuclideanFeatures()
    {
        super();
    }

    @Override
    public DiscriminativeFeatures cloneFeatureSubset(FeatureVectorSubsetEncoding subset)
            throws JCeliacGenericException
    {
        StandardEuclideanFeatures featureSubset = new StandardEuclideanFeatures(super.featureExtractionParameters);
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
        
        double [] rawFeatureCopy = this.getFeatureVectorSubsetData(subset);
        FixedFeatureVector vector = new FixedFeatureVector();
        vector.setData(rawFeatureCopy);
        featureSubset.addAbstractFeature(vector);
        return featureSubset;
    }

    @Override
    public DiscriminativeFeatures cloneEmptyFeature()
    {
        StandardEuclideanFeatures featureSubset = new StandardEuclideanFeatures(super.featureExtractionParameters);
        featureSubset.classIdentifier = this.classIdentifier;
        featureSubset.classNumber = this.classNumber;
        featureSubset.featureExtractionParameters = this.featureExtractionParameters;
        featureSubset.frameNumber = this.frameNumber;
        featureSubset.signalIdentifier = this.signalIdentifier;
      
        return featureSubset;
    }

    @Override
    public double doDistanceTo(DiscriminativeFeatures them)
            throws JCeliacGenericException
    {
        return Distances.euclideanDistance(them.getAllFeatureVectorData(), 
                                           this.getAllFeatureVectorData());
    }
    
    @Override
    public ArrayList<double[]> getLocalFeatureVectorData()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
