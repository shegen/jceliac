/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureExtraction;

import jceliac.JCeliacGenericException;
import jceliac.experiment.*;
import jceliac.logging.*;

/**
 * Implements methods used to compute feature distances.
 * <p>
 * @author shegen
 */
public class Distances
{

    /**
     * Standard histogram instersection, expects normalized histograms of equal
     * size.
     * <p>
     * @param h1 One dimensional histogram.
     * @param h2 One dimensional histogram.
     * <p>
     * @return
     * <p>
     * @throws JCeliacGenericException
     */
    public static double histogramIntersectAsDistance(double[] h1, double[] h2)
            throws JCeliacGenericException
    {
        if(h1 == null || h2 == null) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Histogram is null!");
            throw new JCeliacGenericException("Histogram is null");
        }
        if(h1.length != h2.length) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Histogram Dimensions mismatch!");
            throw new JCeliacGenericException("Histogram Dimensions mismatch");
        }

        // calculate the sum of bins for each histogram
        double h1Sum = 0;
        double h2Sum = 0;

        for(int index = 0; index < h1.length; index++) {
            h1Sum += h1[index];
            h2Sum += h2[index];
        }
        if(h1Sum == 0 || h2Sum == 0) {
            Experiment.log(JCeliacLogger.LOG_WARN, "Empty Histogram encountered, treating as Double.MAX_VALUE distance!");
            return Double.POSITIVE_INFINITY;
        }

        double histIntersectSum = 0;

        if(h1Sum == h2Sum) // the fast version
        {

            // calculate hist intersection
            double histIntersectSumTmpH1 = 0;
            double histIntersectSumTmpH2 = 0;
            for(int index = 0; index < h1.length; index++) {
                double tmpH1 = h1[index];
                double tmpH2 = h2[index];

                if(tmpH1 < tmpH2) {
                    histIntersectSumTmpH1 += (tmpH1);
                } else {
                    histIntersectSumTmpH2 += (tmpH2);
                }

            }
            histIntersectSum = (histIntersectSumTmpH1 / h1Sum) + (histIntersectSumTmpH2 / h2Sum);
        } else { // slower version, not equal number of patterns

            // calculate hist intersection
            for(int index = 0; index < h1.length; index++) {
                double tmpH1 = h1[index] / h1Sum;
                double tmpH2 = h2[index] / h2Sum;

                if(tmpH1 < tmpH2) {
                    histIntersectSum += tmpH1;
                } else {
                    histIntersectSum += tmpH2;
                }

            }
        }
        // treat it as distance, not as similarity; 0 is very close :)
        double distance = (1.0d - histIntersectSum);
        return distance;
    }
    
    /**
     * In this variation, the feature vectors are set up such that training faetures
     * contain sane values only at the corresponding scale bias of the class while
     * evaluation features contain sane values at all positions, hy using histogram
     * intersection the correct histogram is automatically picked, but the normalization has 
     * to be slightly changed for it to work. 
     * 
     * @param h1
     * @param h2
     * @return
     * @throws JCeliacGenericException 
     */
    public static double histogramIntersectSALBP(double[] h1, double[] h2)
            throws JCeliacGenericException
    {
        if(h1 == null || h2 == null) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Histogram is null!");
            throw new JCeliacGenericException("Histogram is null");
        }
        if(h1.length != h2.length) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Histogram Dimensions mismatch!");
            throw new JCeliacGenericException("Histogram Dimensions mismatch");
        }

        // calculate the sum of bins for each histogram
        double h1Sum = 0;
        double h2Sum = 0;

        for(int index = 0; index < h1.length; index++) {
            h1Sum += h1[index];
            h2Sum += h2[index];
        }
        if(h1Sum == 0 || h2Sum == 0) {
            Experiment.log(JCeliacLogger.LOG_WARN, "Empty Histogram encountered, treating as Double.MAX_VALUE distance!");
            return Double.POSITIVE_INFINITY;
        }

        double histIntersectSum = 0;
        // have to normalize by the smaller, because we are actually only comparing 2 histograms
        double normsum = Math.min(h1Sum, h2Sum);

        // calculate hist intersection
        for(int index = 0; index < h1.length; index++) {
            double tmpH1 = h1[index] / normsum;
            double tmpH2 = h2[index] / normsum;

            if(tmpH1 < tmpH2) {
                histIntersectSum += tmpH1;
            } else {
                histIntersectSum += tmpH2;
            }

        }

        // treat it as distance, not as similarity; 0 is very close :)
        return (1.0d - histIntersectSum);
    }


    /**
     * Standard histogram instersection, expects normalized histograms of equal
     * size.
     * <p>
     * @param h1 Two dimensional histogram.
     * @param h2 Two dimensional histogram.
     * <p>
     * @return
     * <p>
     * @throws JCeliacGenericException
     */
    public static double histogramIntersect(double[][] h1, double[][] h2)
            throws Exception
    {

        if(h1 == null || h2 == null) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Histogram is null!");
            throw new JCeliacGenericException("Histogram is null");
        }
        if(h1.length != h2.length || h1[0].length != h2[0].length) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Histogram Dimensions mismatch!");
            throw new JCeliacGenericException("Histogram Dimensions mismatch");
        }

        // calculate the sum of bins for each histogram
        double h1Sum = 0;
        double h2Sum = 0;

        for(int index = 0; index < h1.length; index++) {
            for(int index2 = 0; index2 < h2.length; index2++) {
                h1Sum += h1[index][index2];
                h2Sum += h2[index][index2];
            }
        }

        if(h1Sum == 0 || h2Sum == 0) {
            Experiment.log(JCeliacLogger.LOG_WARN, "Empty Histogram encountered, treating as Double.MAX_VALUE distance!");
            return Double.POSITIVE_INFINITY;
        }

        double histIntersectSum = 0;

        if(h1Sum == h2Sum) // the fast version
        {

            // calculate hist intersection
            double histIntersectSumTmpH1 = 0;
            double histIntersectSumTmpH2 = 0;
            for(int index = 0; index < h1.length; index++) {
                for(int index2 = 0; index2 < h1[0].length; index2++) {
                    double tmpH1 = h1[index][index2];
                    double tmpH2 = h2[index][index2];

                    if(tmpH1 < tmpH2) {
                        histIntersectSumTmpH1 += (tmpH1);
                    } else {
                        histIntersectSumTmpH2 += (tmpH2);
                    }
                }

            }
            histIntersectSum = (histIntersectSumTmpH1 / h1Sum) + (histIntersectSumTmpH2 / h2Sum);
        } else { // slower version, not equal number of patterns

            // calculate hist intersection
            for(int index = 0; index < h1.length; index++) {
                for(int index2 = 0; index2 < h1[0].length; index2++) {
                    double tmpH1 = h1[index][index2] / h1Sum;
                    double tmpH2 = h2[index][index2] / h2Sum;

                    if(tmpH1 < tmpH2) {
                        histIntersectSum += tmpH1;
                    } else {
                        histIntersectSum += tmpH2;
                    }
                }
            }
        }
        // treat it as distance, not as similarity; 0 is very close :)
        return (1.0d - histIntersectSum);
    }

    /**
     * Euclidean distance, expects two vectors.
     * <p>
     * @param h1 Vector
     * @param h2 Vector
     * <p>
     * @return
     * <p>
     * @throws JCeliacGenericException
     */
    public static double euclideanDistance(double[] h1, double[] h2)
            throws JCeliacGenericException
    {
        if(h1.length != h2.length) {
            throw new JCeliacGenericException();
        }
        double sum = 0;
        for(int i = 0; i < h1.length; i++) {
            sum += (h1[i] - h2[i]) * (h1[i] - h2[i]);
        }
        double ret = Math.sqrt(sum);
        return ret;
    }
    
    /**
     * Euclidean distance, expects two vectors.
     * <p>
     * @param h1 Vector
     * @param h2 Vector
     * <p>
     * @return
     * <p>
     * @throws JCeliacGenericException
     */
    public static double squaredEuclideanDistance(double[] h1, double[] h2)
            throws JCeliacGenericException
    {
        if(h1.length != h2.length) {
            throw new JCeliacGenericException();
        }
        double sum = 0;
        for(int i = 0; i < h1.length; i++) {
            sum += (h1[i] - h2[i]) * (h1[i] - h2[i]);
        }
        return sum;
    }
    
    /**
     * Manhattan distance or L1 norm, expects two vectors.
     * <p>
     * @param h1 Vector
     * @param h2 Vector
     * <p>
     * @return
     * <p>
     * @throws JCeliacGenericException
     */
    public static double manhattanDistance(double[] h1, double[] h2)
            throws JCeliacGenericException
    {
        double sum = 0;
        for(int i = 0; i < h1.length; i++) {
            sum += Math.abs(h1[i] - h2[i]);
        }
        return sum;
    }
    
    
    public static double chiSquaredDistance(double [] h1, double [] h2)
    {
        double distance = 0;
        for(int i = 0; i < h1.length; i++) {
            double denom = (h1[i] + h2[i]);
            if(denom == 0) { // what's the best strategy here, using eps does not seem right to me!
                continue;
            }
            distance += ((h1[i] - h2[i]) * (h1[i] - h2[i])) / denom;
        }
        return 0.5*distance;
    }

}
