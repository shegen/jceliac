/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.featureExtraction.SIFT;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.StandardEuclideanFeatures;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.Frame;
import jceliac.tools.filter.DataFilter2D;

/**
 * Partial port of vl_dsift, not useable yet! The correctnes of the descriptor representation is not
 * validated, but the computational complexity should be correct. 
 * If used later check the results and also check the normalization of the 
 * descriptors. 
 * 
 * @author shegen
 */
public class FeatureExtractionMethodDenseSIFT
    extends FeatureExtractionMethod
{

    private FeatureExtractionParametersDenseSIFT myParameters;
    
    public FeatureExtractionMethodDenseSIFT(FeatureExtractionParameters param)
    {
        super(param);
        this.myParameters = (FeatureExtractionParametersDenseSIFT) param;
    }
    
    

    @Override
    public DiscriminativeFeatures extractFeatures(Frame content) 
            throws JCeliacGenericException
    {
        this.processDSIFT(content.getGrayData());
        return new StandardEuclideanFeatures();
    }
    

    private double [] computeDSIFTKernel(int binSize, int numBins, int binIndex, double windowSize)
    {
        int filtLen = 2 * binSize - 1 ;
        double [] ker = new double[filtLen];
        double delta = binSize * (binIndex - 0.5F * (numBins - 1)) ;
        double sigma = (double) binSize * (double) windowSize ;

        int kerIter = 0;
        for (int x = - binSize + 1 ; x <= + binSize - 1 ; ++ x) {
          double z = (x - delta) / sigma ;
          ker[kerIter++] = (1.0F - Math.abs(x) / binSize) *
                           ((binIndex >= 0) ? Math.exp(- 0.5F * z*z) : 1.0F) ;
        }
        return ker ;
    }
    
    
    private void processDSIFT(double[][] data)
    {
        double [][][] grads = new double[this.myParameters.numBinT][data.length][data[0].length];
        
        for(int y = 0; y < data[0].length-1; y++) {
            for(int x = 0; x < data.length-1; x++) {
                double gx, gy;

                // y derivative
                if(y == 0) {
                    gy = data[x][y + 1] - data[x][y];
                } else if(y == data[0].length-1) {
                    gy = data[x][y] - data[x][y - 1];
                } else {
                    gy = 0.5d * (data[x][y + 1] - data[x][y - 1]);
                }
                // x derivative
                if(x == 0) {
                    gx = data[x + 1][y] - data[x][y];
                } else if(x == data.length - 1) {
                    gx = data[x][y] - data[x - 1][y];
                } else {
                    gx = 0.5F * (data[x + 1][y] - data[x - 1][y]);
                }
                
                /* angle and modulus */
                double angle = Math.atan2(gy,gx) + 2*Math.PI;
                double mod = Math.sqrt(gx*gx + gy*gy) ;

                /* quantize angle */
                double nt = (angle % (2*Math.PI)) * (this.myParameters.numBinT / (2*Math.PI)) ;
                int bint = (int) Math.floor (nt) ;
                double rbint = nt - bint;
      
                grads[bint % this.myParameters.numBinT][x][y] = (1 - rbint) * mod;
                grads[(bint +1) % this.myParameters.numBinT][x][y] = (rbint) * mod;
            }
        }
        double [][] dsiftDescriptors = this.computeDSIFTWithGaussianWindow(grads);
        
        int frameSizeX = this.myParameters.binSizeX * (this.myParameters.numBinX - 1) + 1;
        int frameSizeY = this.myParameters.binSizeY * (this.myParameters.numBinY - 1) + 1;
        double normConstant = frameSizeX*frameSizeY;
        
        for(int descIndex = 0; descIndex < dsiftDescriptors.length; descIndex++) 
        {
            // compute mass
            double mass = 0;
            for(int bint = 0; bint < dsiftDescriptors[0].length; bint++) {
                mass += dsiftDescriptors[descIndex][bint];
            }
            mass /= normConstant;
            // vlfeat now does, we do not have a working representation but this 
            // should be done in a working version
            // frameIter->norm = mass ;
            
            // L2 normalize
            double norm = 0;
            for(int bint = 0; bint < dsiftDescriptors[0].length; bint++) {
                norm += (dsiftDescriptors[descIndex][bint] * dsiftDescriptors[descIndex][bint]);
            }
            norm = Math.sqrt(norm);
            for(int bint = 0; bint < dsiftDescriptors[0].length; bint++) {
                dsiftDescriptors[descIndex][bint] = dsiftDescriptors[descIndex][bint] / norm;
            }
            
            // clamp
            for(int i = 0; i < dsiftDescriptors[0].length; i++) {
                if(dsiftDescriptors[descIndex][i] > 0.2d) { 
                    dsiftDescriptors[descIndex][i] = 0.2d;
                }
            }
            // L2 normalize
            norm = 0;
            for(int bint = 0; bint < dsiftDescriptors[0].length; bint++) {
                norm += (dsiftDescriptors[descIndex][bint] * dsiftDescriptors[descIndex][bint]);
            }
            norm = Math.sqrt(norm);
            for(int bint = 0; bint < dsiftDescriptors[0].length; bint++) {
                dsiftDescriptors[descIndex][bint] = dsiftDescriptors[descIndex][bint] / norm;
            } 
        }
    }   

    
    
    private double[][] computeDSIFTWithGaussianWindow(double[][][] grads)
    {

        int Wx = this.myParameters.binSizeX - 1;
        int Wy = this.myParameters.binSizeY - 1;

        int descSize = this.myParameters.numBinX
                       * this.myParameters.numBinY
                       * this.myParameters.numBinT;

        int rangeX = grads[0].length - (this.myParameters.numBinX - 1) * this.myParameters.binSizeX;
        int rangeY = grads[0][0].length - (this.myParameters.numBinY - 1) * this.myParameters.binSizeY;
        int numFramesX = (rangeX >= 0) ? rangeX / this.myParameters.stepX + 1 : 0;
        int numFramesY = (rangeY >= 0) ? rangeY / this.myParameters.stepY + 1 : 0;
        int numFrames = numFramesX * numFramesY;

        double[][] dsiftDescriptors = new double[numFrames][descSize];

        for(int biny = 0; biny < this.myParameters.numBinY; ++biny) {
            double[] yker = this.computeDSIFTKernel(this.myParameters.binSizeY,
                                                    this.myParameters.numBinY,
                                                    biny,
                                                    this.myParameters.windowSize);

            for(int binx = 0; binx < this.myParameters.numBinX; ++binx) {
                double[] xker = this.computeDSIFTKernel(this.myParameters.binSizeX,
                                                        this.myParameters.numBinX,
                                                        binx,
                                                        this.myParameters.windowSize);

                for(int bint = 0; bint < this.myParameters.numBinT; ++bint) {
                    double[][] convTmp1 = DataFilter2D.convolveMirrorSeparable(grads[bint], yker, 0);
                    double[][] convTmp2 = DataFilter2D.convolveMirrorSeparable(convTmp1, xker, 1);

                    int frameSizeX = this.myParameters.binSizeX * (this.myParameters.numBinX - 1) + 1;
                    int frameSizeY = this.myParameters.binSizeY * (this.myParameters.numBinY - 1) + 1;

                    int descIndex = 0;
                    for(int framey = 0;
                        framey <= grads[0][0].length - frameSizeY + 1;
                        framey += this.myParameters.stepY) {

                        for(int framex = 0;
                            framex <= grads[0].length - frameSizeX + 1;
                            framex += this.myParameters.stepX) {
                            int descPos = bint
                                          + binx * this.myParameters.numBinT
                                          + biny * (this.myParameters.numBinX * this.myParameters.numBinT);

                            dsiftDescriptors[descIndex][descPos]
                            = convTmp2[framex + binx * this.myParameters.binSizeX][framey + biny * this.myParameters.binSizeY];
                            descIndex++;
                        }
                    }
                }

            }
        }
        return dsiftDescriptors;
    }

    //<editor-fold desc="Unused initialization and parameter estimation methods.">
    @Override
    public void initialize() throws JCeliacGenericException
    {
    }

    @Override
    public void estimateParameters(ArrayList<DataReader> readersByClass) throws
                                                                                JCeliacGenericException
    {
    }

    @Override
    public void estimateParametersIterative(Frame nextFrame)
    {
    }
    //</editor-fold>
    
}
