/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.featureExtraction.SIFT;

import jceliac.featureExtraction.FeatureExtractionParameters;

/**
 *
 * @author shegen
 */
public class FeatureExtractionParametersDenseSIFT
    extends FeatureExtractionParameters
{

    public FeatureExtractionParametersDenseSIFT()
    {
    }

    // parameters taken from dsift.c vl_dsift_new()
    protected double windowSize = 2.0d; 
    protected int numFrames = 0;
    protected int descrSize = this.numBinT * this.numBinX * this.numBinY;
    
    protected int stepX = 6; // 6 was used in our experiments, default in vl_dsift is 5
    protected int stepY = 6;
    
    // VlDsiftDescriptorGeometry
    protected int numBinT = 8;
    protected int numBinX = 4;
    protected int numBinY = 4;
    protected int binSizeX = 5;
    protected int binSizeY = 5;
   
    
    @Override
    public String getMethodName()
    {
        return "Dense SIFT";
    }
    
    
}
