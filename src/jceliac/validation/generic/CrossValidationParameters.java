/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.validation.generic;

import jceliac.validation.generic.ValidationMethodParameters.ECrossValidationType;
import jceliac.validation.generic.ValidationMethodParameters.ESplitStrategy;



/**
 *
 * @author shegen
 */
public class CrossValidationParameters
{
    private final int numberOfFolds;
    private final int numIterations;
    private final ECrossValidationType crossValidationType;
    private final ESplitStrategy splitStrategy;

    public CrossValidationParameters(int numberOfFolds, int numIterations, ECrossValidationType crossValidationType, ESplitStrategy splitStrategy)
    {
        this.numberOfFolds = numberOfFolds;
        this.numIterations = numIterations;
        this.crossValidationType = crossValidationType;
        this.splitStrategy = splitStrategy;
    }
    
    public int getNumberOfFolds()
    {
        return numberOfFolds;
    } 

    public int getNumIterations()
    {
        return numIterations;
    } 

    public ECrossValidationType getCrossValidationType()
    {
        return crossValidationType;
    }

    public ESplitStrategy getSplitStrategy()
    {
        return splitStrategy;
    }  
}
