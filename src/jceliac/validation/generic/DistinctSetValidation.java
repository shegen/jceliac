/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.validation.generic;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.ClassificationOutcome;
import jceliac.classification.ClassificationResult;
import jceliac.classification.model.GenerativeModel;
import jceliac.classification.model.GenerativeModelFeatureMapping;
import jceliac.classification.ClassificationMapping;
import jceliac.classification.ClassificationMethod;
import jceliac.classification.IterableClassificationParameterOptimizer;
import jceliac.classification.IteratedClassificationResult;
import jceliac.featureExtraction.FeatureCollection;
import jceliac.featureOptimization.FeatureOptimizationMethod;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;
import jceliac.featureOptimization.IterableFeatureOptimizer;

/**
 *
 * @author shegen
 */
public class DistinctSetValidation
        extends ValidationMethod
{

    public DistinctSetValidation(ValidationMethodParameters parameters,
                                 ClassificationMethod classificationMethod,
                                 GenerativeModel model,
                                 FeatureOptimizationMethod featureOptimizationMethod)
    {
        super(parameters, classificationMethod, model, featureOptimizationMethod);
    }

    /**
     * Validates by optimizing features and models over the entire validation,
     * this is prone to bias the result and should not be used.
     * <p>
     * @param trainingFeatures
     * @param evaluationFeatures
     * <p>
     * @return
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    @Override
    public IteratedClassificationResult validateOuter(FeatureCollection trainingFeatures,
                                              FeatureCollection evaluationFeatures)
            throws JCeliacGenericException
    {
        ClassificationResult result = null;
        ClassificationConfiguration classificationConfiguration;
        MultiVariableOptimizer localMultiOptimizer = new MultiVariableOptimizer();
        MultiVariableOptimizer globalMultiOptimizer = new MultiVariableOptimizer();

        // optimize features based on the distinct set validation result, this overfits the parameters!
        // initialize feature vector subset optimizer and start optimization
        int featureDim = trainingFeatures.get(0).getFeatureVectorDimensionality();
        IterableFeatureOptimizer featureOptimizer = this.featureOptimizationMethod.initializeOptimization(featureDim);
        FeatureVectorSubsetEncoding featureSubsetEncoding = featureOptimizer.startOptimization();

        // start feature optimization
        do {
            FeatureCollection trainingSubsetFeatures = FeatureCollection.createClonedFeatureCollection(trainingFeatures, featureSubsetEncoding);
            FeatureCollection evaluationSubsetFeatures = FeatureCollection.createClonedFeatureCollection(evaluationFeatures, featureSubsetEncoding);

            // train the model using the feature subset, the model is not dependent on the classification
            // configuration
            GenerativeModelFeatureMapping modelMapping
                                          = this.model.trainModel(trainingSubsetFeatures);

            // map the training data using the model
            FeatureCollection mappedTrainingData = modelMapping.mapFeatures(trainingSubsetFeatures);

            // optimize classification paramters based on the distinct set result,
            // this overfits the parameters
            IterableClassificationParameterOptimizer classificationOptimizer = this.classificationMethod.initializeOptimization();
            classificationConfiguration = classificationOptimizer.startOptimization();

            this.classificationMethod.hint(mappedTrainingData, modelMapping.mapFeatures(evaluationSubsetFeatures));
            
            // train a classifier using the model mapped data of the current feature vector subset
            ClassificationMapping trainedMethod
                                              = this.classificationMethod.train(mappedTrainingData,
                                                                                classificationConfiguration);

            do {
                result = new ClassificationResult();
                ArrayList<ClassificationOutcome> outcomeList = this.runValidation(trainedMethod, modelMapping, evaluationSubsetFeatures);
                result.addOutcomeList(outcomeList);
                result.setConfiguration(classificationConfiguration);
                result.setFeatureSubset(featureSubsetEncoding);

                localMultiOptimizer.addResultConfiguration(result, featureSubsetEncoding, classificationConfiguration);
                globalMultiOptimizer.addResultConfiguration(result, featureSubsetEncoding, classificationConfiguration);
                classificationConfiguration = classificationOptimizer.iterateOptimization(result, classificationConfiguration);

                trainedMethod = this.classificationMethod.train(mappedTrainingData,
                                                                classificationConfiguration);
                  
            } while(false == classificationOptimizer.iterationConverged());

            // notify the optimizer and retrieve the currently best feature vector subset
            featureSubsetEncoding = featureOptimizer.iterateOptimization(localMultiOptimizer.getBestResult(), featureSubsetEncoding);
        } while(false == featureOptimizer.iterationConverged());
        
        IteratedClassificationResult iteratedResult = new IteratedClassificationResult();
        iteratedResult.addSingleResult(globalMultiOptimizer.getBestResult());
        return iteratedResult;
    }

    /**
     * Validates using cross validation on the training set to optimize
     * parameters.
     * <p>
     * @param trainingFeatures
     * @param evaluationFeatures
     * <p>
     * @return
     * <p>
     * @throws JCeliacGenericException
     */
    @Override
    public IteratedClassificationResult validateInner(FeatureCollection trainingFeatures,
                                                      FeatureCollection evaluationFeatures)
            throws JCeliacGenericException
    {
        // 5 fold cross validation, 1 iteration for optimizing parameters
        CrossValidationParameters cvParams = new CrossValidationParameters(5, 1, 
                                                                           ValidationMethodParameters.ECrossValidationType.KFOLD,
                                                                           this.parameters.getSplitStrategy());

        GenericCrossValidation crossValidation = new GenericCrossValidation(this.parameters,
                                                                            this.classificationMethod,
                                                                            this.model,
                                                                            this.featureOptimizationMethod);

        
        IteratedClassificationResult cvResult = crossValidation.validateOuter(trainingFeatures, cvParams);
        FeatureCollection trainingFeatureSubset = FeatureCollection.createClonedFeatureCollection(trainingFeatures,
                                                                                                         cvResult.getFeatureSubset());
        FeatureCollection evaluationFeatureSubset = FeatureCollection.createClonedFeatureCollection(evaluationFeatures,
                                                                                                           cvResult.getFeatureSubset());
        trainingFeatureSubset.flagAsTrainingFeatures();
        evaluationFeatureSubset.flagAsEvaluationFeatures();
        
        ClassificationResult result = new ClassificationResult();
        // classify based on validation on the training data
        GenerativeModelFeatureMapping modelMapping = this.model.trainModel(trainingFeatureSubset);
        FeatureCollection mappedTrainingDAta = modelMapping.mapFeatures(trainingFeatureSubset);
        ClassificationMapping trainedClassifier = this.classificationMethod.train(mappedTrainingDAta, cvResult.getConfiguration());
        ArrayList<ClassificationOutcome> outcomes = this.runValidation(trainedClassifier, modelMapping, evaluationFeatureSubset);
        result.addOutcomeList(outcomes);
        result.setConfiguration(cvResult.getConfiguration());
        
        IteratedClassificationResult iteratedResult = new IteratedClassificationResult();
        iteratedResult.addSingleResult(result);
        return iteratedResult;
    }
}
