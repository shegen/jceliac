/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.validation.generic;

import jceliac.JCeliacGenericException;
import jceliac.classification.model.GenerativeModel;
import jceliac.experiment.Experiment;
import jceliac.experiment.InitializeFailedException;
import jceliac.classification.ClassificationMethod;
import jceliac.featureOptimization.FeatureOptimizationMethod;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.parameter.xml.XMLParseException;
import jceliac.validation.custom.NBIProjectValidationMethod;
import jceliac.validation.custom.NBIProjectValidationParameters;
import jceliac.validation.custom.SampledDistinctValidation.SampledDistinctValidation;
import jceliac.validation.custom.SampledDistinctValidation.SampledDistinctValidationParameters;


/**
 * The factory for creating objects of type ValidationMethod.
 * <p>
 * @author shegen
 */
public class ValidationMethodFactory
{

    public static ValidationMethod createValidationMethod(String methodString,
                                                         ValidationMethodParameters parameters,
                                                         ClassificationMethod classificationMethod, 
                                                         GenerativeModel model, 
                                                         FeatureOptimizationMethod featureOptimizationMethod)
            throws JCeliacGenericException
    {
        ValidationMethod.EValidationTypes method = ValidationMethod.EValidationTypes.valueOfIgnoreCase(methodString);
        return ValidationMethodFactory.createValidationMethod(method, parameters, classificationMethod, 
                                                              model, featureOptimizationMethod);
                
    }
    
    public static ValidationMethod
            createValidationMethod(ValidationMethod.EValidationTypes type,
                                           ValidationMethodParameters parameters,
                                           ClassificationMethod classificationMethod, 
                                           GenerativeModel model, 
                                           FeatureOptimizationMethod featureOptimizationMethod)
            throws InitializeFailedException, 
                   JCeliacGenericException
    {
        switch(type) {
           
            case CrossValidation:
                return new DefaultCrossValidation(parameters, classificationMethod, 
                                             model, featureOptimizationMethod);
            case DistinctValidation:
                return new DistinctSetValidation(parameters, classificationMethod, 
                                                 model, featureOptimizationMethod);
            case SampledTwoFoldValidation:
                return new SampledTwoFoldValidation(parameters, classificationMethod, 
                                                 model, featureOptimizationMethod);
            case NBIProjectValidation:
                return new NBIProjectValidationMethod(parameters, classificationMethod, 
                                                 model, featureOptimizationMethod);
            case ScaleConstrainedValidation: // backwards compatibility for Pattern Recognition experiments
            case SampledDistinctValidation:
                return new SampledDistinctValidation(parameters, classificationMethod, 
                                                            model, featureOptimizationMethod);
            
            default:
                Experiment.log(JCeliacLogger.LOG_ERROR, "Unknown Validation Method!");
                throw new InitializeFailedException("Unknown Validation Method");
        }
    }

    public static ValidationMethodParameters createParametersByMethod(String method)
            throws XMLParseException, JCeliacGenericException
    {
        ValidationMethod.EValidationTypes m = ValidationMethod.EValidationTypes.valueOfIgnoreCase(method);

        switch(m) {
            case SampledTwoFoldValidation:
                return new SampledTwoFoldValidationMethodParameters();
            case NBIProjectValidation:
                return new NBIProjectValidationParameters();
            case ScaleConstrainedValidation: // backwards compatibility for Pattern Recognition experiments
            case SampledDistinctValidation:
                return new SampledDistinctValidationParameters();
            default:
                return new ValidationMethodParameters();
        }
    }
}
