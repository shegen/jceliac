/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.validation.generic;

import java.io.Serializable;
import jceliac.JCeliacGenericException;
import jceliac.experiment.AbstractParameters;
import jceliac.experiment.Experiment;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
public class ValidationMethodParameters extends AbstractParameters
                      implements Cloneable, Serializable
{
    protected String optimizationType = "OUTER";
    protected String crossValidationType = "KFOLD";
    protected String splitStrategy = "SAMPLE";
    
    protected int numberOfFolds;
    protected int numberOfIterations;
    
    public enum EOptimizationType {
        INNER,
        OUTER;
        
        public static ValidationMethodParameters.EOptimizationType valueOfIgnoreCase(String s)
                throws JCeliacGenericException
        {
            for(ValidationMethodParameters.EOptimizationType type : ValidationMethodParameters.EOptimizationType.values()) {
               if(type.name().equalsIgnoreCase(s)) {
                   return type;
               }
            }
            throw new JCeliacGenericException("Unknown EOptimizationType: "+s);
        }
    }
    
    public enum ECrossValidationType {
        LOO,
        LOPO,
        KFOLD,
        DISTINCT;
        
        public static ValidationMethodParameters.ECrossValidationType valueOfIgnoreCase(String s)
                 throws JCeliacGenericException
        {
            for(ValidationMethodParameters.ECrossValidationType type : ValidationMethodParameters.ECrossValidationType.values()) {
               if(type.name().equalsIgnoreCase(s)) {
                   return type;
               }
            }
            throw new JCeliacGenericException("Unknown ECrossValidationType: "+s);
        }
    }
    
    public enum ESplitStrategy {
        SAMPLE,
        PATIENT;
        
        public static ValidationMethodParameters.ESplitStrategy valueOfIgnoreCase(String s)
                throws JCeliacGenericException
        {
            for(ValidationMethodParameters.ESplitStrategy type : ValidationMethodParameters.ESplitStrategy.values()) {
               if(type.name().equalsIgnoreCase(s)) {
                   return type;
               }
            }
            throw new JCeliacGenericException("Unknown ESplitStrategy: "+s);
        }
    }
    
    public EOptimizationType getOptimizationType()
            throws JCeliacGenericException
    {
        return ValidationMethodParameters.EOptimizationType.valueOfIgnoreCase(this.optimizationType);
    }

    public ECrossValidationType getCrossValidationType()
            throws JCeliacGenericException
    {
        return ValidationMethodParameters.ECrossValidationType.valueOfIgnoreCase(this.crossValidationType);
    }

    public ESplitStrategy getSplitStrategy()
            throws JCeliacGenericException
    {
        return ValidationMethodParameters.ESplitStrategy.valueOfIgnoreCase(this.splitStrategy);
    }
    
    public int getNumberOfFolds()
    {
        return numberOfFolds;
    }

    public int getNumberOfIterations()
    {
        return numberOfIterations;
    }

    public void setOptimizationType(String optimizationType)
    {
        this.optimizationType = optimizationType;
    }

    public void setCrossValidationType(String crossValidationType)
    {
        this.crossValidationType = crossValidationType;
    }

    public void setSplitStrategy(String splitStrategy)
    {
        this.splitStrategy = splitStrategy;
    }

    public void setNumberOfFolds(int numberOfFolds)
    {
        this.numberOfFolds = numberOfFolds;
    }

    public void setNumberOfIterations(int numberOfIterations)
    {
        this.numberOfIterations = numberOfIterations;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException
    {
        return super.clone(); 
    }  
}
