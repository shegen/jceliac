/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.validation.generic;

import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationResult;
import jceliac.classification.model.GenerativeModel;
import jceliac.classification.ClassificationMethod;
import jceliac.classification.IteratedClassificationResult;
import jceliac.featureExtraction.FeatureCollection;
import jceliac.featureOptimization.FeatureOptimizationMethod;

/**
 *
 * @author shegen
 */
public class DefaultCrossValidation extends ValidationMethod
{
    // inner CV uses an extra validation hence a high number of iterations could take a long time
    public static final int KFOLD_INNER_ITERATIONS = 5; 
    public static final int KFOLD_OUTER_ITERATIONS = 5;
    public static final int KFOLD_FOLDS = 10;
    
    public static final double TWOFOLD_TRAINSPLIT_FRACTION = 0.9;
    public static final double TWOFOLD_EVALSPLIT_FRACTION = 0.1;
    public static final int TWOFOLD_ITERATIONS = 50;
    
    public DefaultCrossValidation(ValidationMethodParameters parameters, 
                             ClassificationMethod classificationMethod, 
                             GenerativeModel model, 
                             FeatureOptimizationMethod featureOptimizationMethod)
    {
        super(parameters, classificationMethod, model, featureOptimizationMethod);
    }

    
    @Override
    protected IteratedClassificationResult validateInner(FeatureCollection trainingFeatures, 
                                                 FeatureCollection unused)
            throws JCeliacGenericException
    {
               GenericCrossValidation crossValidation = new GenericCrossValidation(this.parameters, 
                                                                            this.classificationMethod, 
                                                                            this.model, 
                                                                            this.featureOptimizationMethod);
        // validate method parameters
        switch(this.parameters.getCrossValidationType()) 
        {
            case LOO:
            case KFOLD:
            case LOPO:
                if(unused != null && unused.isEmpty() == false) {
                    throw new JCeliacGenericException("Cross validation expects evaluation features to be null or empty, this could be a mistake!");
                }
            break;
        }
     
        CrossValidationParameters cvParameters;
        
        // construct parameters for generic cross validation
        switch(this.parameters.getCrossValidationType()) 
        {
            case LOO:
                cvParameters = new CrossValidationParameters(trainingFeatures.size(), 1, 
                                                             ValidationMethodParameters.ECrossValidationType.LOO,
                                                             this.parameters.getSplitStrategy());
                break;
            case KFOLD:
                cvParameters = new CrossValidationParameters(KFOLD_FOLDS, KFOLD_INNER_ITERATIONS,
                                                             ValidationMethodParameters.ECrossValidationType.KFOLD,
                                                             this.parameters.getSplitStrategy());
                break;
            default:
                throw new  JCeliacGenericException("Unsupported cross validation type requested!");
        }
        FeatureCollection trainingFeatureCollection = FeatureCollection.createClonedFeatureCollection(trainingFeatures);
        return crossValidation.validateInner(trainingFeatureCollection, cvParameters);
    }

    @Override
    protected IteratedClassificationResult validateOuter(FeatureCollection trainingFeatures, 
                                                 FeatureCollection unused)
            throws JCeliacGenericException
    {
     
        GenericCrossValidation crossValidation = new GenericCrossValidation(this.parameters, 
                                                                            this.classificationMethod, 
                                                                            this.model, 
                                                                            this.featureOptimizationMethod);
        // validate method parameters
        switch(this.parameters.getCrossValidationType()) 
        {
            case LOO:
            case KFOLD:
            case LOPO:
                if(unused != null && unused.isEmpty() == false) {
                    throw new JCeliacGenericException("Cross validation expects evaluation features to be null or empty, this could be a mistake!");
                }
            break;
        }
        CrossValidationParameters cvParameters;
        
        // construct parameters for generic cross validation
        switch(this.parameters.getCrossValidationType()) 
        {
            case LOO:
                cvParameters = new CrossValidationParameters(trainingFeatures.size(), 1, 
                                                             ValidationMethodParameters.ECrossValidationType.LOO,
                                                             this.parameters.getSplitStrategy());
                break;
            case KFOLD:
                cvParameters = new CrossValidationParameters(KFOLD_FOLDS, KFOLD_OUTER_ITERATIONS, 
                                                             ValidationMethodParameters.ECrossValidationType.KFOLD,
                                                             this.parameters.getSplitStrategy());
                break;
            default:
                throw new  JCeliacGenericException("Unsupported cross validation type requested!");
        }
        FeatureCollection trainingFeatureCollection = FeatureCollection.createClonedFeatureCollection(trainingFeatures);
        return crossValidation.validateOuter(trainingFeatureCollection, cvParameters);
    }
}
