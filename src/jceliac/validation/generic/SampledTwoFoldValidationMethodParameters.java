/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.validation.generic;

/**
 *
 * @author shegen
 */
public class SampledTwoFoldValidationMethodParameters 
    extends ValidationMethodParameters
{
    // if useing two fold split type this are the default fractions used 
    private final double trainingSplitFraction = 0.8d;
    private final double evaluationSplitFraction = 0.2d;

    public SampledTwoFoldValidationMethodParameters()
    {
        this.numberOfFolds = 2;
        this.numberOfIterations = 50;
    }

    
    public double getTrainingSplitFraction()
    {
        return trainingSplitFraction;
    }

    public double getEvaluationSplitFraction()
    {
        return evaluationSplitFraction;
    }
}
