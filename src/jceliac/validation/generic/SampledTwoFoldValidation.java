/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.validation.generic;

import java.util.ArrayList;
import java.util.Random;
import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.ClassificationOutcome;
import jceliac.classification.model.GenerativeModel;
import jceliac.classification.model.GenerativeModelFeatureMapping;
import jceliac.classification.ClassificationMapping;
import jceliac.classification.ClassificationMethod;
import jceliac.classification.ClassificationResult;
import jceliac.classification.IterableClassificationParameterOptimizer;
import jceliac.classification.IteratedClassificationResult;
import jceliac.experiment.Experiment;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;
import jceliac.featureOptimization.FeatureOptimizationMethod;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;
import jceliac.featureOptimization.IterableFeatureOptimizer;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
public class SampledTwoFoldValidation extends ValidationMethod
{

    protected SampledTwoFoldValidationMethodParameters myParameters;
    public SampledTwoFoldValidation(ValidationMethodParameters parameters, 
                                   ClassificationMethod classificationMethod, 
                                   GenerativeModel model, 
                                   FeatureOptimizationMethod featureOptimizationMethod)
    {
        super(parameters, classificationMethod, model, featureOptimizationMethod);
        this.myParameters = (SampledTwoFoldValidationMethodParameters) parameters;
    }

    
    /* We do not support feature subset selection anymore!
     */
    @Override
    protected IteratedClassificationResult validateInner(FeatureCollection trainingFeatures, 
                                                         FeatureCollection unused)
            throws JCeliacGenericException
    {
        IteratedClassificationResult allResults = new IteratedClassificationResult();
        
        Random rand = new Random(this.seed);
        // create splits, always based on the same random seed for each subset to ensure the same splits for each subset
        ArrayList<FeatureCollection[]> cvSplitData = this.createDataSplit(trainingFeatures, rand);
        
        int i = 0;
        for(FeatureCollection[] currentSplit : cvSplitData) 
        {
            FeatureCollection currentTrainingFold = currentSplit[0];
            FeatureCollection currentEvaluationFold = currentSplit[1];
                        
            // 5 fold cross validation, 1 iteration for optimizing parameters
            CrossValidationParameters cvParams = new CrossValidationParameters(5, 1, 
                                                                               ValidationMethodParameters.ECrossValidationType.KFOLD,
                                                                               this.parameters.getSplitStrategy());

            GenericCrossValidation crossValidation = new GenericCrossValidation(this.parameters,
                                                                                this.classificationMethod,
                                                                                this.model,
                                                                                this.featureOptimizationMethod);


            IteratedClassificationResult cvResult = crossValidation.validateOuter(currentTrainingFold, cvParams);
            currentTrainingFold.flagAsTrainingFeatures();
            currentEvaluationFold.flagAsEvaluationFeatures();
            
            // train model based on entire split data (outer CV)
            GenerativeModelFeatureMapping modelMapping = this.model.trainModel(currentTrainingFold);
            FeatureCollection currentTrainingFoldMapped = modelMapping.mapFeatures(currentTrainingFold);
            
            // train a classifier using the model mapped data of the current feature vector subset
            ClassificationMapping trainedMethod = this.classificationMethod.train(currentTrainingFoldMapped, cvResult.getConfiguration());
            ArrayList<ClassificationOutcome> outcomeList = this.runValidation(trainedMethod, modelMapping, currentEvaluationFold);
            ClassificationResult overallResult = new ClassificationResult();
            overallResult.addOutcomeList(outcomeList);
            allResults.addSingleResult(overallResult);
            
            Experiment.log(JCeliacLogger.LOG_INFO, "Iteration %d: OCR = %.2f", i+1, overallResult.getOverallClassificationRate()*100.0f);
            i++; 
        }
        return allResults;
    }

    
    
    
    @Override
    protected IteratedClassificationResult validateOuter(FeatureCollection trainingFeatures,
                                                         FeatureCollection unused)
            throws JCeliacGenericException
    {

        IteratedClassificationResult allResults = new IteratedClassificationResult();
        Random rand = new Random(this.seed);
        
        for(int i = 0; i < this.myParameters.getNumberOfIterations(); i++) 
        {
            MultiVariableOptimizer globalMultiOptimizer = new MultiVariableOptimizer();
            MultiVariableOptimizer localMultiOptimizer = new MultiVariableOptimizer();

            // optimize features based on the overall classification rate of the cross validation
            int featureDim = trainingFeatures.get(0).getFeatureVectorDimensionality();
            IterableFeatureOptimizer featureOptimizer = this.featureOptimizationMethod.initializeOptimization(featureDim);
            FeatureVectorSubsetEncoding featureSubsetEncoding = featureOptimizer.startOptimization();

            do {
                FeatureCollection trainingFeatureCollection = FeatureCollection.
                        createClonedFeatureCollection(trainingFeatures,
                                                      featureSubsetEncoding);

                this.classificationMethod.hint(trainingFeatureCollection, null);

                // create splits, always based on the same random seed for each subset to ensure the same splits for each subset
                ArrayList<FeatureCollection[]> cvSplitData
                                               = this.createDataSplit(trainingFeatureCollection, rand);

                // optimize classifier over the overall result of the CVs (outer CV)
                IterableClassificationParameterOptimizer classificationOptimizer
                                                         = this.classificationMethod.initializeOptimization();
                ClassificationConfiguration classificationConfiguration = classificationOptimizer.startOptimization();
                ClassificationResult overallResult;
                do {
                    overallResult = new ClassificationResult();
                    localMultiOptimizer = new MultiVariableOptimizer();
                    // iterate over all supplied splits (iterations of CVs)
                    for(FeatureCollection[] currentSplit : cvSplitData) {

                        FeatureCollection allCurrentData = FeatureCollection.combine(currentSplit);

                        // train model based on entire split data (outer CV)
                        GenerativeModelFeatureMapping modelMapping = this.model.trainModel(allCurrentData);

                        // evaluate current split
                        FeatureCollection currentTrainingFold = currentSplit[0];
                        FeatureCollection currentEvaluationFold = currentSplit[1];

                        FeatureCollection currentTrainingFoldMapped = modelMapping.mapFeatures(currentTrainingFold);

                        // train a classifier using the model mapped data of the current feature vector subset
                        ClassificationMapping trainedMethod = this.classificationMethod.train(currentTrainingFoldMapped,
                                                                                              classificationConfiguration);
                        ArrayList<ClassificationOutcome> outcomeList = this.runValidation(trainedMethod, modelMapping, currentEvaluationFold);
                        overallResult.addOutcomeList(outcomeList);

                    }
                    localMultiOptimizer.addResultConfiguration(overallResult, featureSubsetEncoding, classificationConfiguration);
                    globalMultiOptimizer.addResultConfiguration(overallResult, featureSubsetEncoding, classificationConfiguration);
                    classificationConfiguration = classificationOptimizer.iterateOptimization(overallResult, classificationConfiguration);
                } while(false == classificationOptimizer.iterationConverged());
                // notify the optimizer and retrieve the currently best feature vector subset
                featureSubsetEncoding = featureOptimizer.iterateOptimization(localMultiOptimizer.getBestResult(), featureSubsetEncoding);
            } while(false == featureOptimizer.iterationConverged());

            Experiment.log(JCeliacLogger.LOG_INFO, "Iteration %d: OCR = %.2f", i+1, globalMultiOptimizer.getBestResult().getOverallClassificationRate()*100.0f);
            allResults.addSingleResult(globalMultiOptimizer.getBestResult());
        }
        return allResults;
    }

    /**
     * Generate a random split of the supplied data into fracTrain * features.size()
     * training data and fracEval * features.size() evaluation data. 
     * 
     * @param features
     * @param fracTrain fraction of training data
     * @param fracEval fraction of evaluation data (fracTrain+fracEval must be 1)
     * @param rand
     * @throws jceliac.JCeliacGenericException 
     * @return 
     */
    @SuppressWarnings("unchecked")
    protected FeatureCollection [] generateRandomDataSplit(FeatureCollection features,
                                                           double fracTrain, 
                                                           double fracEval,
                                                           Random rand)
            throws JCeliacGenericException
    {
        if((fracTrain + fracEval) != 1.0d) {
            throw new JCeliacGenericException("Not all data in split!");
        }
        PatientMapping patientMapping = null;
        int numIndices;
        if(this.parameters.getSplitStrategy() == ValidationMethodParameters.ESplitStrategy.PATIENT) {
            patientMapping = new PatientMapping(features);
            numIndices = patientMapping.getNumberOfPatients();
        }else {
            numIndices = features.size();
        }
        
        int numTrainingFeatures = (int)Math.round(numIndices * fracTrain);
        int numEvalFeatures = (int)Math.round(numIndices * fracEval);
        
        // sanity check
        if((numTrainingFeatures + numEvalFeatures) != numIndices) {
            throw new JCeliacGenericException("Split misses some data!");
        }
        
        ArrayList <DiscriminativeFeatures> trainingFeatures; 
        ArrayList <DiscriminativeFeatures> evaluationFeatures; 
        do {
            boolean [] labels = this.generateRandomSplitLabels(numTrainingFeatures, numEvalFeatures, rand);

            trainingFeatures = new ArrayList<>();
            evaluationFeatures = new ArrayList<>();
            for(int i = 0; i < labels.length; i++) 
            {
                if(patientMapping != null) {
                    ArrayList <DiscriminativeFeatures> featuresForPatient = patientMapping.getFeaturesForPatient(i);
                    if(false == labels[i]) {
                        trainingFeatures.addAll(featuresForPatient);
                    }else {
                        evaluationFeatures.addAll(featuresForPatient);
                    }
                }else {
                    if(false == labels[i]) {
                        trainingFeatures.add(features.get(i));
                    }else {
                        evaluationFeatures.add(features.get(i));
                    }
                }
            }
        }while(false == GenericCrossValidation.validateSplitData(new ArrayList [] {trainingFeatures, evaluationFeatures}));
        FeatureCollection trainingFeatureCollection = FeatureCollection.createClonedFeatureCollection(trainingFeatures);
        FeatureCollection evaluationFeatureCollection = FeatureCollection.createClonedFeatureCollection(evaluationFeatures);
        return new FeatureCollection [] {trainingFeatureCollection, evaluationFeatureCollection};
    }
    
    
    /**
     * Generates a randomized array such that numTrainFeatures entries are false
     * and numEvalFeatures entries are true, hence false indicates a training 
     * features, and true indicates an eval feature in the returned array.
     * @param numTrainFeatures
     * @param numEvalFeatures
     * @param rand
     * @return 
     */
    protected boolean [] generateRandomSplitLabels(int numTrainFeatures, int numEvalFeatures, Random rand) 
    {
        int numAllFeatures = numTrainFeatures+numEvalFeatures;
       
        boolean [] ret = new boolean[numAllFeatures];
        for(int i = 0; i < numAllFeatures; i++) {
            boolean isEval = (i >= numTrainFeatures);
            ret[i] = isEval;
        }
        // shuffle array using fisher-yates algorithm
        for(int i = 0; i < numAllFeatures; i++) {
            int j = Math.abs(rand.nextInt()) % (numAllFeatures);
            boolean tmp = ret[j];
            ret[j] = ret[i];
            ret[i] = tmp;
        }
        return ret;   
    } 
    


    protected ArrayList<FeatureCollection[]>
            createDataSplit(FeatureCollection features,
                    Random rand)
            throws JCeliacGenericException {
        ArrayList<FeatureCollection[]> dataSplitsPerIteration = new ArrayList<>();
        int iteration = 0;
        while (iteration < this.myParameters.getNumberOfIterations()) {

            FeatureCollection[] folds = this.generateRandomDataSplit(features,
                    this.myParameters.getTrainingSplitFraction(),
                    this.myParameters.getEvaluationSplitFraction(),
                    rand);
            dataSplitsPerIteration.add(folds);
            iteration++;
        }
        return dataSplitsPerIteration;
    }
         
            
}