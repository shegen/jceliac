/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.validation.generic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.Callable;
import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationOutcome;
import jceliac.classification.model.GenerativeModel;
import jceliac.classification.model.GenerativeModelFeatureMapping;
import jceliac.classification.ClassificationMapping;
import jceliac.classification.ClassificationMethod;
import jceliac.classification.ClassificationMethod.EClassificationMethodType;
import jceliac.classification.IteratedClassificationResult;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;
import jceliac.featureOptimization.FeatureOptimizationMethod;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;

/**
 *
 * @author shegen
 */
public abstract class ValidationMethod
{
    protected FeatureOptimizationMethod featureOptimizationMethod;
    protected ClassificationMethod classificationMethod;
    protected ValidationMethodParameters parameters;
    protected GenerativeModel model;
    protected int numberOfTrainingClasses;
    protected int numberOfEvaluationClasses;
    protected final long seed = 61254917;
    
    // this can hold user supplied validation types such as used for the miccai14 paper for example
    public static enum EValidationTypes
    {
        // generic methods
       CrossValidation,
       DistinctValidation,
       SampledTwoFoldValidation, // create two random partitions from a single set of data and classify distinct
       // custom methods
       NBIProjectValidation,
       ScaleConstrainedValidation,
       SampledDistinctValidation; // this is basically the same as ScaleConstrainedValidation but more meaningful and does not break old experiments

        public static ValidationMethod.EValidationTypes valueOfIgnoreCase(String stringValue)
                throws JCeliacGenericException
        {
            for(ValidationMethod.EValidationTypes value : ValidationMethod.EValidationTypes.values()) {
                if(value.name().equalsIgnoreCase(stringValue)) {
                    return value;
                }
            }
            throw new JCeliacGenericException("Unknown Validation Type: " + stringValue);
        }
    }
    
    public ValidationMethod(ValidationMethodParameters parameters,
                              ClassificationMethod classificationMethod, 
                              GenerativeModel model, 
                              FeatureOptimizationMethod featureOptimizationMethod)
    {
        this.parameters = parameters;
        this.classificationMethod = classificationMethod;
        this.model = model;
        this.featureOptimizationMethod = featureOptimizationMethod;
    }
    
    public IteratedClassificationResult validate(ArrayList <DiscriminativeFeatures> trainingFeatures,
                                         ArrayList <DiscriminativeFeatures> evaluationFeatures)
            throws JCeliacGenericException
    {
         // sort the features to always guarantee the same results due to CV splitting
         // this is later randomized by the Feature Collection again, but we need a common
         // basis for this shuffle
         Collections.sort(trainingFeatures);
         Collections.sort(evaluationFeatures);
         
         this.numberOfTrainingClasses = this.countNumberOfClasses(trainingFeatures);
         if(evaluationFeatures == null) {
             this.numberOfEvaluationClasses = this.numberOfTrainingClasses;
         }else {
            this.numberOfEvaluationClasses = this.countNumberOfClasses(evaluationFeatures);
         }
         
         if(this.numberOfEvaluationClasses > this.numberOfTrainingClasses &&
            this.classificationMethod.getClassificationMethodType() != 
             EClassificationMethodType.KERNEL_ONE_CLASS_SVM) 
         { 
            throw new JCeliacGenericException("More evaluation than training classes!");
         }
         
         FeatureCollection trainingFeatureCollection = FeatureCollection.createClonedFeatureCollection(trainingFeatures);
         FeatureCollection evaluationFeaturesCollection = FeatureCollection.createClonedFeatureCollection(evaluationFeatures);
         switch(this.parameters.getOptimizationType()) {
             case INNER:
                 return this.validateInner(trainingFeatureCollection, evaluationFeaturesCollection);
             case OUTER:
                 return this.validateOuter(trainingFeatureCollection, evaluationFeaturesCollection);
         }
         throw new JCeliacGenericException("This should not be reachable!");    
    }
    
    protected abstract IteratedClassificationResult validateInner(FeatureCollection trainingFeatures,
                                                          FeatureCollection evaluationFeatures)
            throws JCeliacGenericException;
    
    protected abstract IteratedClassificationResult validateOuter(FeatureCollection trainingFeatures,
                                                          FeatureCollection evaluationFeatures)
            throws JCeliacGenericException;
 
    protected Callable<ArrayList<ClassificationOutcome>> createParallelValidation(
            ClassificationMapping trainedClassifier,
            GenerativeModelFeatureMapping trainedModel,
            FeatureCollection evaluationFeatures)
            throws JCeliacGenericException
    {
        return () -> {
            ArrayList<ClassificationOutcome> outcomeList = new ArrayList<>();

            // evaluate the test data
            for(DiscriminativeFeatures tmpEval : evaluationFeatures) {
                DiscriminativeFeatures modelMappedFeatures = trainedModel.mapFeatures(tmpEval);
                ArrayList<ClassificationOutcome> outcomes = trainedClassifier.map(modelMappedFeatures);
                for(ClassificationOutcome tmpOutcome : outcomes) {
                    tmpOutcome.setFeatureVector(tmpEval);
                    outcomeList.add(tmpOutcome);
                }
            }
            return outcomeList;
        };
    }
    
    protected ArrayList <ClassificationOutcome> runValidation(ClassificationMapping trainedClassifier,
                                                 GenerativeModelFeatureMapping trainedModel,
                                                 FeatureCollection evaluationFeatures)
            throws JCeliacGenericException
    {
        ArrayList <ClassificationOutcome> outcomeList = new ArrayList <>();

        int index = 0;
        // evaluate the test data
        for(DiscriminativeFeatures tmpEval : evaluationFeatures) {
            DiscriminativeFeatures modelMappedFeatures = trainedModel.mapFeatures(tmpEval);
            ArrayList<ClassificationOutcome> outcomes = trainedClassifier.map(modelMappedFeatures);
            for(ClassificationOutcome tmpOutcome : outcomes) {
                tmpOutcome.setFeatureVector(tmpEval);
                outcomeList.add(tmpOutcome);
            }
            index++;
        }
        return outcomeList;
    }
    
    protected int countNumberOfClasses(ArrayList <DiscriminativeFeatures> features) 
    {
        // figure out number of classes
        HashMap<Integer, Integer> classLabelCount = new HashMap<>();
        for(DiscriminativeFeatures f : features) {
            classLabelCount.put(f.getClassNumber(), 1);
        }
        return classLabelCount.size();
    }
    
    /**
     * Creates a new list of feature subsets (a copy of features) to have thread
     * safe access to the features. 
     * 
     * @param features
     * @param encoding
     * @return
     * @throws JCeliacGenericException 
     */
    protected static ArrayList <DiscriminativeFeatures> cloneSubsetFeatures(ArrayList <DiscriminativeFeatures> features,
                                                                       FeatureVectorSubsetEncoding encoding)
            throws JCeliacGenericException
    {
        ArrayList <DiscriminativeFeatures> preparedSubsetFeatures = new ArrayList <>();
        for(DiscriminativeFeatures tmp : features) {
            // fix the data by copying the features, this makes it thread safe
            DiscriminativeFeatures tmpFeatureSubset = tmp.getClonedFeatureSubset(encoding);
            preparedSubsetFeatures.add(tmpFeatureSubset);
        }
        return preparedSubsetFeatures;
    }
    
   /**
     * @param numFeatures
     * @param rand
     * @param numFolds
     * @return 
     */
    protected int [] generateRandomFoldLabels(int numFeatures, int numFolds, Random rand) 
    {
        
        ArrayList <Integer> randomIndices = new ArrayList <>();
        int featuresPerFold = (int)(numFeatures / (double)numFolds);
        
        // generate randomized randomIndices
        while(randomIndices.size() < numFeatures)
        {
            int nextRandomIndex = Math.abs(rand.nextInt()) % (numFeatures);
            if(false == randomIndices.contains(nextRandomIndex)) {
                randomIndices.add(nextRandomIndex);
            }
        }
        int [] ret = new int[numFeatures];
        for(int i = 0; i < numFeatures; i++) {
            int fold = (int)(randomIndices.get(i) / featuresPerFold);
            // if numFeatures is not divisible by numFolds the last fold will have some more features
            fold = Math.min(numFolds-1, fold);
            ret[i] = fold;
        }
        return ret;   
    } 
       
}
