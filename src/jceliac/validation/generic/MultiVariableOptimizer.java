/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.validation.generic;

import java.util.PriorityQueue;
import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.ClassificationResult;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;

/**
 * This is used to find a globally optimal result optimized locally by several 
 * specific optimizers. 
 * 
 * 
 * @author shegen
 */
public class MultiVariableOptimizer
{
    private ClassificationResult bestGlobalResult = null;
    private FeatureVectorSubsetEncoding bestSubsetEncoding = null;
    private ClassificationConfiguration bestClassificationConfiguration = null;
    
    public void addResultConfiguration(ClassificationResult result, 
                                       FeatureVectorSubsetEncoding subsetEncoding,
                                       ClassificationConfiguration classificationConfiguration)
    {
        
        if(this.bestGlobalResult == null || 
           result.getOverallClassificationRate() > this.bestGlobalResult.getOverallClassificationRate()) 
        {
            this.bestGlobalResult = result;
            this.bestSubsetEncoding = subsetEncoding;
            this.bestClassificationConfiguration = classificationConfiguration;
        }
    }
    
    public ClassificationResult getBestResult()
    {
        this.bestGlobalResult.setConfiguration(this.bestClassificationConfiguration);
        this.bestGlobalResult.setFeatureSubset(this.bestSubsetEncoding);
        return this.bestGlobalResult;
    }
}


