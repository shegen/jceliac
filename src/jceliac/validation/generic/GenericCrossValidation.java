/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.validation.generic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.Callable;
import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.ClassificationOutcome;
import jceliac.classification.ClassificationResult;
import jceliac.classification.model.GenerativeModel;
import jceliac.classification.model.GenerativeModelFeatureMapping;
import jceliac.experiment.Experiment;
import jceliac.classification.ClassificationMapping;
import jceliac.classification.ClassificationMethod;
import jceliac.classification.IterableClassificationParameterOptimizer;
import jceliac.classification.IteratedClassificationResult;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;
import jceliac.featureOptimization.FeatureOptimizationMethod;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;
import jceliac.featureOptimization.IterableFeatureOptimizer;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.data.AttributeCounter;
import jceliac.tools.parallel.ParallelTaskExecutor;
import jceliac.validation.generic.ValidationMethodParameters.ESplitStrategy;

/**
 *
 * @author shegen
 */
public class GenericCrossValidation
{

    protected ValidationMethodParameters parameters;
    protected ClassificationMethod classificationMethod;
    protected GenerativeModel model;
    protected FeatureOptimizationMethod featureOptimizationMethod;
    protected int numberOfTrainingClasses;
    protected int numberOfEvaluationClasses;
    protected final long outerSeed = 61254917;
    protected final long innerSeed = 415341534;

    public GenericCrossValidation(ValidationMethodParameters parameters,
                                  ClassificationMethod classificationMethod,
                                  GenerativeModel model,
                                  FeatureOptimizationMethod featureOptimizationMethod)
    {
        this.parameters = parameters;
        this.classificationMethod = classificationMethod;
        this.model = model;
        this.featureOptimizationMethod = featureOptimizationMethod;
    }

    protected IteratedClassificationResult validateInner(FeatureCollection features,
                                                 CrossValidationParameters cvParams)

            throws JCeliacGenericException
    {
        //return this.validateInnerSingleThreaded(features, cvParams);
        return this.validateInnerMultiThreaded(features, cvParams);
    }

    protected IteratedClassificationResult validateOuter(FeatureCollection features,
                                                 CrossValidationParameters cvParams)

            throws JCeliacGenericException
    {
        //return this.validateOuterSingleThreaded(features, cvParams);
        return this.validateOuterMultiThreaded(features, cvParams);
    }

    /**
     * INNER cross validation runs an extra cross validation on the specific
     * portion of training data to optimize results. This can be slow, we
     * optimize speed by running multiple extra CVs in parallel.
     * <p>
     * @param features      Features to cross validate.
     * @param outerCVParams Parameters for the cross validation.
     * <p>
     * @return Result of cross validation based on parameter optimized on an
     *         inner cross validation run.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    protected IteratedClassificationResult validateInnerMultiThreaded(FeatureCollection features,
                                                              CrossValidationParameters outerCVParams)
            throws JCeliacGenericException
    {

        Random rand = new Random(this.innerSeed);
        ClassificationResult overallResult = new ClassificationResult();
        IteratedClassificationResult iteratedResult = new IteratedClassificationResult();
        
        for(int iteration = 0; iteration < outerCVParams.getNumIterations(); iteration++) 
        {
            ArrayList<Callable<ParallelValidationState>> parallelCVTasks = new ArrayList<>();

            // create splits, always based on the same random seed for each subset to ensure the same splits for each subset
            ArrayList<FeatureCollection[]> cvSplitData
                                           = this.createDataSplitForCVType(features,
                                                                           outerCVParams,
                                                                           rand);

            // parameters of the extra (inner) cross validation used to optimize parameters
            // currently 5-fold cross validation on the training set is performed to optimizes parameters
            CrossValidationParameters innerCVParams = new CrossValidationParameters(5, 1, 
                                                                                    ValidationMethodParameters.ECrossValidationType.KFOLD,
                                                                                    this.parameters.getSplitStrategy());


            // iterate over all supplied splits (iterations of CVs)
            for(FeatureCollection[] currentSplit : cvSplitData) {
                for(int evaluationFoldIndex = 0; evaluationFoldIndex < currentSplit.length;
                    evaluationFoldIndex++) {
                    FeatureCollection currentTrainingFold = FeatureCollection.combineExcept(currentSplit, evaluationFoldIndex);
                    FeatureCollection currentEvaluationFold = currentSplit[evaluationFoldIndex];

                    parallelCVTasks.add(this.creatParallelOuterValidation(currentTrainingFold,
                                                                          currentEvaluationFold,
                                                                          innerCVParams));
                }
            }
            ParallelTaskExecutor<ParallelValidationState> taskExecutor = new ParallelTaskExecutor<>(parallelCVTasks, true);
            for(ParallelValidationState tmpState : taskExecutor.executeTasks()) {
                FeatureCollection cvTrainingFeaturesSubset
                                  = FeatureCollection.createClonedFeatureCollection(tmpState.getTrainingFeatures(), tmpState.getValidationResult().getFeatureSubset());

                FeatureCollection cvEvaluationFeaturesSubset
                                  = FeatureCollection.createClonedFeatureCollection(tmpState.getEvaluationFeatures(), tmpState.getValidationResult().getFeatureSubset());

                // classify based on validation on the training data
                GenerativeModelFeatureMapping modelMapping = this.model.trainModel(cvTrainingFeaturesSubset);
                FeatureCollection mappedTrainingDAta = modelMapping.mapFeatures(cvTrainingFeaturesSubset);
                ClassificationMapping trainedClassifier = this.classificationMethod.train(mappedTrainingDAta, tmpState.getValidationResult().getConfiguration());
                ArrayList<ClassificationOutcome> outcomes = this.runValidation(trainedClassifier, modelMapping, cvEvaluationFeaturesSubset);
                overallResult.addOutcomeList(outcomes);
                ClassificationResult result = new ClassificationResult();
                result.addOutcomeList(outcomes);
                result.setConfiguration(tmpState.getValidationResult().getConfiguration());
                result.setFeatureSubset(tmpState.getValidationResult().getFeatureSubset());
                iteratedResult.addSingleResult(result);
            }
        }
        return iteratedResult;
    }

    /**
     * This method performs classification based on cross-validation with
     * outer-validation see the IPMI'11 Hegenbart11c paper on outer-validation.
     * This method is prone to over-fitting and should be avoided if
     * inner-validation is computationally feasible or to optimize features on a
     * training set and then use them on a distinct set. Inner validation
     * performs the optimization of parameter on calling this method on a subset
     * of the training data.
     * <p>
     * This method uses multi threading based on the evaluation of entries.
     * <p>
     * @param features Data to perform cross validation on.
     * @param cvParams Parameters of the cross validation.
     * <p>
     * @return Optimized classification result containing the best classifier
     *         configuration and feature subset.
     * <p>
     * @throws JCeliacGenericException
     */
    protected IteratedClassificationResult validateOuterMultiThreaded(FeatureCollection features,
                                                              CrossValidationParameters cvParams)
            throws JCeliacGenericException
    {
        IteratedClassificationResult iteratedResult = new IteratedClassificationResult();
        ArrayList <ClassificationOutcome> allOutcomes = new ArrayList<>();
        for(int iteration = 0; iteration < cvParams.getNumIterations(); iteration++) 
        {
            MultiVariableOptimizer globalMultiOptimizer = new MultiVariableOptimizer();
            MultiVariableOptimizer localMultiOptimizer = new MultiVariableOptimizer();

            // optimize features based on the overall classification rate of the cross validation
            int featureDim = features.get(0).getFeatureVectorDimensionality();
            IterableFeatureOptimizer featureOptimizer = this.featureOptimizationMethod.initializeOptimization(featureDim);
            FeatureVectorSubsetEncoding featureSubsetEncoding = featureOptimizer.startOptimization();

            do {
                FeatureCollection featuresSubset = FeatureCollection.createClonedFeatureCollection(features, featureSubsetEncoding);
                // create splits, always based on the same random seed for each subset to ensure the same splits for each subset
                ArrayList<FeatureCollection[]> cvSplitData = GenericCrossValidation.createDataSplitForCVType(featuresSubset,
                                                                                                             cvParams,
                                                                                                             new Random(this.outerSeed));

                // optimize classifier over the overall result of the CVs (outer CV)
                IterableClassificationParameterOptimizer classificationOptimizer
                                                         = this.classificationMethod.initializeOptimization();
                ClassificationConfiguration classificationConfiguration = classificationOptimizer.startOptimization();
                ClassificationResult overallResult;
                do {
                    overallResult = new ClassificationResult();
                    localMultiOptimizer = new MultiVariableOptimizer();
                    
                    // iterate over all supplied splits (iterations of CVs)
                    for(FeatureCollection[] currentSplit : cvSplitData) {
                        FeatureCollection allCurrentData = FeatureCollection.combine(currentSplit);

                        // train model based on entire split data (outer CV)
                        GenerativeModelFeatureMapping modelMapping = this.model.trainModel(allCurrentData);

                        ArrayList<Callable<ArrayList<ClassificationOutcome>>> parallelCVTasks = new ArrayList<>();

                        // evaluate current split
                        for(int evaluationFoldIndex = 0; evaluationFoldIndex < currentSplit.length;
                            evaluationFoldIndex++) {
                            FeatureCollection currentTrainingFold = FeatureCollection.combineExcept(currentSplit, evaluationFoldIndex);
                            currentTrainingFold.flagAsTrainingFeatures();
                            
                            FeatureCollection currentEvaluationFold = currentSplit[evaluationFoldIndex];
                            currentEvaluationFold.flagAsEvaluationFeatures();

                            FeatureCollection currentTrainingFoldMM = modelMapping.mapFeatures(currentTrainingFold);
                            // train a classifier using the model mapped data of the current feature vector subset

                            parallelCVTasks.add(
                                    this.createParallelTrainingAndValidation(currentTrainingFoldMM, classificationConfiguration, modelMapping, currentEvaluationFold));
                        }
                        ParallelTaskExecutor<ArrayList<ClassificationOutcome>> taskExecutor = new ParallelTaskExecutor<>(parallelCVTasks, true);
                        for(ArrayList<ClassificationOutcome> outcomes : taskExecutor.executeTasks()) {
                            overallResult.addOutcomeList(outcomes);
                        }
                    }
                    localMultiOptimizer.addResultConfiguration(overallResult, featureSubsetEncoding, classificationConfiguration);
                    globalMultiOptimizer.addResultConfiguration(overallResult, featureSubsetEncoding, classificationConfiguration);
                    classificationConfiguration = classificationOptimizer.iterateOptimization(overallResult, classificationConfiguration);
                    
                } while(false == classificationOptimizer.iterationConverged());
                // notify the optimizer and retrieve the currently best feature vector subset
                featureSubsetEncoding = featureOptimizer.iterateOptimization(localMultiOptimizer.getBestResult(), featureSubsetEncoding);
            } while(false == featureOptimizer.iterationConverged());
            allOutcomes.addAll(globalMultiOptimizer.getBestResult().getClassificationOutcomes());
            iteratedResult.addSingleResult(globalMultiOptimizer.getBestResult());
        }
        return iteratedResult;
       
    }

    /**
     * Same as the multithreaded version, kept to double check code.
     * <p>
     * @param features
     * @param outerCVParams <p>
     * @return <p>
     * @throws JCeliacGenericException
     */
    @Deprecated
    protected ClassificationResult validateInnerSingleThreaded(FeatureCollection features,
                                                               CrossValidationParameters outerCVParams)
            throws JCeliacGenericException
    {
        // create splits, always based on the same random seed for each subset to ensure the same splits for each subset
        ArrayList<FeatureCollection[]> cvSplitData
                                       = this.createDataSplitForCVType(features,
                                                                       outerCVParams,
                                                                       new Random(this.innerSeed));

        // parameters of the extra (inner) cross validation used to optimize parameters
        // currently 5-fold cross validation on the training set is performed to optimizes parameters
        CrossValidationParameters innerCVParams = new CrossValidationParameters(5, 1, 
                                                                                ValidationMethodParameters.ECrossValidationType.KFOLD,
                                                                                this.parameters.getSplitStrategy());

        ClassificationResult overallResult = new ClassificationResult();
        // iterate over all supplied splits (iterations of CVs)
        for(FeatureCollection[] currentSplit : cvSplitData) {
            for(int evaluationFoldIndex = 0; evaluationFoldIndex < currentSplit.length;
                evaluationFoldIndex++) {
                FeatureCollection currentTrainingFold = FeatureCollection.combineExcept(currentSplit, evaluationFoldIndex);
                FeatureCollection currentEvaluationFold = currentSplit[evaluationFoldIndex];

                // find parameters using an extra validation , this is in fact the inner validation here
                IteratedClassificationResult optimizedResult = this.validateOuter(currentTrainingFold, innerCVParams);

                // classify based on validation on the training data
                GenerativeModelFeatureMapping modelMapping = this.model.trainModel(currentTrainingFold);
                FeatureCollection mappedTrainingDAta = modelMapping.mapFeatures(currentTrainingFold);
                ClassificationMapping trainedClassifier = this.classificationMethod.train(mappedTrainingDAta, optimizedResult.getConfiguration());
                ArrayList<ClassificationOutcome> outcomes = this.runValidation(trainedClassifier, modelMapping, currentEvaluationFold);
                overallResult.addOutcomeList(outcomes);
            }
        }
        return overallResult;
    }

    /**
     * Same as the multi threaded version, kept to double check code.
     * <p>
     * @param features
     * @param cvParams <p>
     * @return <p>
     * @throws JCeliacGenericException
     */
    @Deprecated
    protected ClassificationResult validateOuterSingleThreaded(FeatureCollection features,
                                                               CrossValidationParameters cvParams)

            throws JCeliacGenericException
    {
        MultiVariableOptimizer globalMultiOptimizer = new MultiVariableOptimizer();
        MultiVariableOptimizer localMultiOptimizer = new MultiVariableOptimizer();

        // optimize features based on the overall classification rate of the cross validation
        int featureDim = features.get(0).getFeatureVectorDimensionality();
        IterableFeatureOptimizer featureOptimizer = this.featureOptimizationMethod.initializeOptimization(featureDim);
        FeatureVectorSubsetEncoding featureSubsetEncoding = featureOptimizer.startOptimization();

        do {
            FeatureCollection featureSubset = FeatureCollection.createClonedFeatureCollection(features, 
                                                                                              featureSubsetEncoding);
            // create splits, always based on the same random seed for each subset to ensure the same splits for each subset
            ArrayList<FeatureCollection[]> cvSplitData = this.createDataSplitForCVType(featureSubset,
                                                                                       cvParams,
                                                                                       new Random(this.outerSeed));

            // optimize classifier over the overall result of the CVs (outer CV)
            IterableClassificationParameterOptimizer classificationOptimizer
                                                     = this.classificationMethod.initializeOptimization();
            ClassificationConfiguration classificationConfiguration = classificationOptimizer.startOptimization();
            ClassificationResult overallResult;
            do {
                overallResult = new ClassificationResult();
                localMultiOptimizer = new MultiVariableOptimizer();
                // iterate over all supplied splits (iterations of CVs)
                for(FeatureCollection[] currentSplit : cvSplitData) {
                    FeatureCollection allCurrentData = FeatureCollection.combine(currentSplit);

                    // train model based on entire split data (outer CV)
                    GenerativeModelFeatureMapping modelMapping = this.model.trainModel(allCurrentData);

                    // evaluate current split
                    for(int evaluationFoldIndex = 0; evaluationFoldIndex < currentSplit.length;
                        evaluationFoldIndex++) {
                        FeatureCollection currentTrainingFold = FeatureCollection.combineExcept(currentSplit, evaluationFoldIndex);
                        FeatureCollection currentEvaluationFold = currentSplit[evaluationFoldIndex];

                        FeatureCollection currentTrainingFoldMapped = modelMapping.mapFeatures(currentTrainingFold);

                        // train a classifier using the model mapped data of the current feature vector subset
                        ClassificationMapping trainedMethod = this.classificationMethod.train(currentTrainingFoldMapped,
                                                                                                          classificationConfiguration);
                        ArrayList<ClassificationOutcome> outcomeList = this.runValidation(trainedMethod, modelMapping, currentEvaluationFold);
                        overallResult.addOutcomeList(outcomeList);
                    }
                }
                localMultiOptimizer.addResultConfiguration(overallResult, featureSubsetEncoding, classificationConfiguration);
                globalMultiOptimizer.addResultConfiguration(overallResult, featureSubsetEncoding, classificationConfiguration);
                classificationConfiguration = classificationOptimizer.iterateOptimization(overallResult, classificationConfiguration);
            } while(false == classificationOptimizer.iterationConverged());
            // notify the optimizer and retrieve the currently best feature vector subset
            featureSubsetEncoding = featureOptimizer.iterateOptimization(localMultiOptimizer.getBestResult(), featureSubsetEncoding);
        } while(false == featureOptimizer.iterationConverged());
        return globalMultiOptimizer.getBestResult();
    }

    /**
     * Creates a separate validation. This is done in inner validation mode for
     * each split that is evaluated.
     * <p>
     * @param trainingData
     * @param evaluationData Not used in here, but used to run the
     *                       classification on the optimized results later.
     * @param cvParams       <p>
     * @return <p>
     * @throws JCeliacGenericException
     */
    protected Callable<ParallelValidationState>
            creatParallelOuterValidation(FeatureCollection trainingData,
                                         FeatureCollection evaluationData,
                                         CrossValidationParameters cvParams)
            throws JCeliacGenericException
    {
        return () -> {
            MultiVariableOptimizer globalMultiOptimizer = new MultiVariableOptimizer();
            MultiVariableOptimizer localMultiOptimizer = new MultiVariableOptimizer();

            // optimize features based on the overall classification rate of the cross validation
            int featureDim = trainingData.get(0).getFeatureVectorDimensionality();
            IterableFeatureOptimizer featureOptimizer = this.featureOptimizationMethod.initializeOptimization(featureDim);
            FeatureVectorSubsetEncoding featureSubsetEncoding = featureOptimizer.startOptimization();

            do {
                FeatureCollection featureSubset = FeatureCollection.createClonedFeatureCollection(trainingData, 
                                                                                              featureSubsetEncoding);
                // create splits, always based on the same random seed for each subset to ensure the same splits for each subset
                ArrayList<FeatureCollection[]> cvSplitData = GenericCrossValidation.createDataSplitForCVType(featureSubset,
                                                                                                             cvParams,
                                                                                                             new Random(this.outerSeed));

                this.classificationMethod.hint(featureSubset, null);
                // optimize classifier over the overall result of the CVs (outer CV)
                IterableClassificationParameterOptimizer classificationOptimizer
                                                         = this.classificationMethod.initializeOptimization();
                ClassificationConfiguration classificationConfiguration = classificationOptimizer.startOptimization();
                ClassificationResult overallResult;
                do {
                    overallResult = new ClassificationResult();
                    localMultiOptimizer = new MultiVariableOptimizer();
                    // iterate over all supplied splits (iterations of CVs)
                    for(FeatureCollection[] currentSplit : cvSplitData) {
                        FeatureCollection allCurrentData = FeatureCollection.combine(currentSplit);

                        // train model based on entire split data (outer CV)
                        GenerativeModelFeatureMapping modelMapping = this.model.trainModel(allCurrentData);

                        // evaluate current split
                        for(int evaluationFoldIndex = 0; evaluationFoldIndex < currentSplit.length;
                            evaluationFoldIndex++) {
                            FeatureCollection currentTrainingFold = FeatureCollection.combineExcept(currentSplit, evaluationFoldIndex);
                            FeatureCollection currentEvaluationFold = currentSplit[evaluationFoldIndex];

                            FeatureCollection currentTrainingFoldMapped = modelMapping.mapFeatures(currentTrainingFold);

                            // train a classifier using the model mapped data of the current feature vector subset
                            ClassificationMapping trainedMethod = this.classificationMethod.train(currentTrainingFoldMapped,
                                                                                                              classificationConfiguration);
                            ArrayList<ClassificationOutcome> outcomeList = this.runValidation(trainedMethod, modelMapping, currentEvaluationFold);
                            overallResult.addOutcomeList(outcomeList);
                        }
                    }
                    localMultiOptimizer.addResultConfiguration(overallResult, featureSubsetEncoding, classificationConfiguration);
                    globalMultiOptimizer.addResultConfiguration(overallResult, featureSubsetEncoding, classificationConfiguration);
                    classificationConfiguration = classificationOptimizer.iterateOptimization(overallResult, classificationConfiguration);
                } while(false == classificationOptimizer.iterationConverged());
                // notify the optimizer and retrieve the currently best feature vector subset
                featureSubsetEncoding = featureOptimizer.iterateOptimization(localMultiOptimizer.getBestResult(), featureSubsetEncoding);
            } while(false == featureOptimizer.iterationConverged());
            ClassificationResult bestResult = globalMultiOptimizer.getBestResult();
            ParallelValidationState state = new ParallelValidationState(trainingData,
                                                                        evaluationData,
                                                                        bestResult);
            return state;
        };
    }
            
    /**
     * Generates data splits based on ArrayLists to conduct the cross
     * validations. The ArrayLists are shallow copies of the original data.
     * <p>
     * @param features
     * @param cvParams
     * @param rand          <p>
     * @return <p>
     * @throws JCeliacGenericException
     */
    public static ArrayList<FeatureCollection[]>
            createDataSplitForCVType(FeatureCollection features,
                                     CrossValidationParameters cvParams,
                                     Random rand)
            throws JCeliacGenericException
    {
        ArrayList<FeatureCollection[]> dataSplitsPerIteration = new ArrayList<>();
        int iteration = 0;
        while(iteration < cvParams.getNumIterations()) {

            FeatureCollection[] folds = GenericCrossValidation.generateKFoldDataSplit(features,
                                                                    cvParams.getNumberOfFolds(),
                                                                    cvParams.getSplitStrategy(),
                                                                    rand);
            dataSplitsPerIteration.add(folds);
            iteration++;
        }
        return dataSplitsPerIteration;
    }

    /**
     * In some care cases it could happen that a split contains all features of
     * a class it could therfore happen that a split is evaluated without
     * samples of that class in the training data, we therefore ensure that this
     * rare case does not happen.
     * <p>
     * @param splitData <p>
     * @return
     */
    public static boolean validateSplitData(ArrayList<DiscriminativeFeatures>[] splitData)
    {
        AttributeCounter<Integer> globalImageCountPerClass = new AttributeCounter<>();
        @SuppressWarnings("unchecked")
        AttributeCounter<Integer> foldImageCountPerClass[] = new AttributeCounter[splitData.length];

        for(int splitIndex = 0; splitIndex < splitData.length; splitIndex++) {
            foldImageCountPerClass[splitIndex] = new AttributeCounter<>();
            for(DiscriminativeFeatures tmpFeatures : splitData[splitIndex]) {
                foldImageCountPerClass[splitIndex].count(tmpFeatures.getClassNumber());
                globalImageCountPerClass.count(tmpFeatures.getClassNumber());
            }
        }
        // check if any of the foldImageCountPerClass is equal to the globalImageCountPerClass
        for(int splitIndex = 0; splitIndex < splitData.length; splitIndex++) {
            for(int classLabel : foldImageCountPerClass[splitIndex].getDistinctAttributes()) {
                if(foldImageCountPerClass[splitIndex].getCountForAttribute(classLabel)
                   == globalImageCountPerClass.getCountForAttribute(classLabel)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Creates a split of data with numFold folds, ensures that the splits are
     * sane.
     * <p>
     * @param features
     * @param foldCount
     * @param numFolds
     * @param splitStrategy
     * @param featureSubset
     * @param rand          <p>
     * @return <p>
     * @throws JCeliacGenericException
     */
    @SuppressWarnings("unchecked")
    protected static FeatureCollection[]
            generateKFoldDataSplit(FeatureCollection features,
                                   int foldCount,
                                   ESplitStrategy splitStrategy,
                                   Random rand)
            throws JCeliacGenericException
    {
        ArrayList<DiscriminativeFeatures>[] foldData = null;
        PatientMapping patientMapping = null;
        int numIndices, numFolds;
        
        if(splitStrategy == ESplitStrategy.PATIENT) {
            patientMapping = new PatientMapping(features);
            numIndices = patientMapping.getNumberOfPatients();
        }else {
            numIndices = features.size();
        }
        // for loocv
        if(foldCount > numIndices) {
            numFolds = numIndices;
        }else {
            numFolds = foldCount;
        }
        
        while(true) 
        {
            int[] foldLabels = GenericCrossValidation.generateRandomFoldLabels(numIndices, numFolds, rand);
            foldData = new ArrayList[numFolds];
            for(int i = 0; i < numFolds; i++) {
                foldData[i] = new ArrayList<>();
            }

            for(int i = 0; i < foldLabels.length; i++) {
                if(patientMapping != null) {
                     ArrayList <DiscriminativeFeatures> featuresForPatient = 
                                                patientMapping.getFeaturesForPatient(i);
                     
                     foldData[foldLabels[i]].addAll(featuresForPatient);
                }else {
                    foldData[foldLabels[i]].add(features.get(i));
                }
            }
            // validate split data, such that no split contains all data of a certain class
            if(true == GenericCrossValidation.validateSplitData(foldData)) {
                break;
            }
            Experiment.log(JCeliacLogger.LOG_INFO, "All features of a class in single fold, re-computing split.");
        }
        FeatureCollection[] folds = new FeatureCollection[numFolds];
        for(int i = 0; i < numFolds; i++) {
            folds[i] = FeatureCollection.createClonedFeatureCollection(foldData[i]);
        }
        return folds;
    }

    protected static int[] generateRandomFoldLabels(int numFeatures, int numFolds, Random rand)
    {

        ArrayList<Integer> randomIndices = new ArrayList<>();
        int featuresPerFold = (int) (numFeatures / (double) numFolds);

        // generate randomized randomIndices
        while(randomIndices.size() < numFeatures) {
            int nextRandomIndex = Math.abs(rand.nextInt()) % (numFeatures);
            if(false == randomIndices.contains(nextRandomIndex)) {
                randomIndices.add(nextRandomIndex);
            }
        }
        int[] ret = new int[numFeatures];
        for(int i = 0; i < numFeatures; i++) {
            int fold = (int) (randomIndices.get(i) / featuresPerFold);
            // if numFeatures is not divisible by numFolds the last fold will have some more features
            fold = Math.min(numFolds - 1, fold);
            ret[i] = fold;
        }
        return ret;
    }

    /**
     * In some care cases it could happen that a split contains only features
     * from a single class, it could therfore happen that a split is evaluated
     * without samples of that class in the training data, we therefore ensure
     * that features from all classes are in a split.
     * <p>
     * @param splitData
     * <p>
     * @return
     */
    protected boolean validateSplit(FeatureCollection splitData)
    {
        HashMap<Integer, Integer> classCount = new HashMap<>();
        for(DiscriminativeFeatures tmpFeatures : splitData) {
            classCount.put(tmpFeatures.getClassNumber(), 0);
        }
        if(this.numberOfTrainingClasses != classCount.size()) {
            Experiment.log(JCeliacLogger.LOG_WARN, "Data split does not contain all classes, re-generating!");
            return false;
        }
        return true;
    }

    /**
     * Run the validation for a single split.
     * <p>
     * @param trainedClassifier
     * @param trainedModel
     * @param evaluationFeatures <p>
     * @return <p>
     * @throws JCeliacGenericException
     */
    protected ArrayList<ClassificationOutcome> runValidation(ClassificationMapping trainedClassifier,
                                                             GenerativeModelFeatureMapping trainedModel,
                                                             FeatureCollection evaluationFeatures)
            throws JCeliacGenericException
    {
        ArrayList<ClassificationOutcome> outcomeList = new ArrayList<>();

        // evaluate the test data
        for(DiscriminativeFeatures tmpEval : evaluationFeatures) {
            DiscriminativeFeatures modelMappedFeatures = trainedModel.mapFeatures(tmpEval);
            ArrayList<ClassificationOutcome> outcomes = trainedClassifier.map(modelMappedFeatures);
            for(ClassificationOutcome tmpOutcome : outcomes) {
                tmpOutcome.setFeatureVector(tmpEval);
                outcomeList.add(tmpOutcome);
            }
        }
        return outcomeList;
    }

    /**
     * Creates a parallel evaluation instance for a single split, also trains
     * the classifier.
     * <p>
     * @param currentTrainingFold
     * @param classificationConfiguration
     * @param trainedModel
     * @param evaluationFeatures          <p>
     * @return <p>
     * @throws JCeliacGenericException
     */
    protected Callable<ArrayList<ClassificationOutcome>> createParallelTrainingAndValidation(
            FeatureCollection currentTrainingFold,
            ClassificationConfiguration classificationConfiguration,
            GenerativeModelFeatureMapping trainedModel,
            FeatureCollection evaluationFeatures)
            throws JCeliacGenericException
    {
        return () -> {
            ArrayList<ClassificationOutcome> outcomeList = new ArrayList<>();

            ClassificationMapping trainedMethod
                                              = this.classificationMethod.train(currentTrainingFold,
                                                                                classificationConfiguration);

            // evaluate the test data
            for(DiscriminativeFeatures tmpEval : evaluationFeatures) {
                DiscriminativeFeatures modelMappedFeatures = trainedModel.mapFeatures(tmpEval);
                ArrayList<ClassificationOutcome> outcomes = trainedMethod.map(modelMappedFeatures);
                for(ClassificationOutcome tmpOutcome : outcomes) {
                    tmpOutcome.setFeatureVector(tmpEval);
                    outcomeList.add(tmpOutcome);
                }
            }
            return outcomeList;
        };
    }

}
