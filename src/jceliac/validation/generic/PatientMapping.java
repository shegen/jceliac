/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.validation.generic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import jceliac.JCeliacGenericException;
import jceliac.experiment.Experiment;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.parser.LOPOIdentifierParser;

/**
 *
 * @author shegen
 */
public class PatientMapping
{
    private HashMap<String, ArrayList<DiscriminativeFeatures>> patientMap;
    private HashMap<Integer, String> patientNameToIndexMap;
    
    public PatientMapping(FeatureCollection features)
            throws JCeliacGenericException
    {
        this.createMapping(features);
        // create a patient-Name to index mapping
        int index = 0;
        this.patientNameToIndexMap = new HashMap <>();
        for(String key : this.patientMap.keySet()) {
            this.patientNameToIndexMap.put(index, key);
            index++;
        }
    }
    
    public ArrayList<DiscriminativeFeatures> getFeaturesForPatient(int patientIndex)
    {
        return this.patientMap.get(this.patientNameToIndexMap.get(patientIndex));
    }
    
    public int getNumberOfPatients()
    {
        return this.patientMap.size();
    }
    
    /**
     * Figure out the patients from filenames. This is based on the image names,
     * and might not be applicable for all image sets. This is possible for the
     * celiac sets.
     * <p>
     *
     */
    private void createMapping(FeatureCollection features)
            throws JCeliacGenericException
    {

        this.patientMap = new HashMap<>();
        for(DiscriminativeFeatures tmp : features) 
        {
            LOPOIdentifierParser parser = LOPOIdentifierParser.createParser(tmp.getSignalIdentifier());

            if(parser == null) {
                throw new JCeliacGenericException("Could not create parser");
            }
            String uniqueIdentifier = parser.generateUniqueIdentifier(tmp.getSignalIdentifier());
            ArrayList<DiscriminativeFeatures> l = this.patientMap.get(uniqueIdentifier);
            if(l == null) {
                l = new ArrayList<>();
                l.add(tmp);
                this.patientMap.put(uniqueIdentifier, l);
            } else {
                l.add(tmp);
                this.patientMap.put(uniqueIdentifier, l);
            }
        }
    }

    /**
     * Logs Patient Information.
     */
    public void printLOPOPatientInfo()
    {
        // print patient info
        Experiment.log(JCeliacLogger.LOG_INFO, "LOPO Patient Information:");
        Set<String> keySet = this.patientMap.keySet();
        int count = 1;
        for(String tmp : keySet) {
            Experiment.log(JCeliacLogger.LOG_INFO, "Patient (%d): %s ", count, tmp);
            count++;
            ArrayList<DiscriminativeFeatures> tmpFlist = patientMap.get(tmp);
            for(DiscriminativeFeatures f : tmpFlist) {
                Experiment.log(JCeliacLogger.LOG_INFO, "\t\t-> %s", f.getSignalIdentifier());
            }
        }
        int patientCount;
        String classIdentifier;
        for(int i = 0; i < 6; i++) {
            patientCount = this.getPatientCountForClass(i);
            classIdentifier = this.getClassIdentifier(i);
            if(patientCount > 0) {
                Experiment.log(JCeliacLogger.LOG_INFO, "Class %s, Patient Count: %d", classIdentifier, patientCount);
            }
        }
    }

    /**
     *
     * @param classNum Class number in question.
     * <p>
     * @return The number of Patient for the class with class number classNum.
     */
    private int getPatientCountForClass(int classNum)
    {
        int count = 0;

        Set<String> keySet = patientMap.keySet();
        for(String tmp : keySet) {
            ArrayList<DiscriminativeFeatures> tmpFlist = patientMap.get(tmp);
            if(tmpFlist.get(0).getClassNumber() == classNum) {
                count++;
            }
        }
        return count;
    }
    
   /**
     *
     * @param classNum Class number in question.
     * <p>
     * @return The class identifier for class with class number classNum.
     */
    private String getClassIdentifier(int classNum)
    {
        for(ArrayList <DiscriminativeFeatures> tmp : this.patientMap.values()) {
            if(tmp.get(0).getClassNumber() == classNum) {
                return tmp.get(0).getClassIdentifier();
            }
        }
        return "UNKNOWN";
    }
    
}
