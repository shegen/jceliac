/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.validation.generic;

import java.util.ArrayList;
import jceliac.classification.ClassificationResult;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;

/**
 *
 * @author shegen
 */
public class ParallelValidationState
{

    protected FeatureCollection trainingFeatures;
    protected FeatureCollection evaluationFeatures;
    protected ClassificationResult validationResult;

    public ParallelValidationState(FeatureCollection trainingFeatures,
                                   FeatureCollection evaluationFeatures,
                                   ClassificationResult validationResult)
    {
        this.trainingFeatures = trainingFeatures;
        this.evaluationFeatures = evaluationFeatures;
        this.validationResult = validationResult;
    }

    public FeatureCollection getTrainingFeatures()
    {
        return trainingFeatures;
    }

    public FeatureCollection getEvaluationFeatures()
    {
        return evaluationFeatures;
    }

    public ClassificationResult getValidationResult()
    {
        return validationResult;
    }
}
