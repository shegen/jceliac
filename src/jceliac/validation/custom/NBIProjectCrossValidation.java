/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */
package jceliac.validation.custom;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Callable;
import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.ClassificationMapping;
import jceliac.classification.ClassificationMethod;
import jceliac.classification.ClassificationOutcome;
import jceliac.classification.ClassificationResult;
import jceliac.classification.IterableClassificationParameterOptimizer;
import jceliac.classification.IteratedClassificationResult;
import jceliac.classification.model.GenerativeModel;
import jceliac.classification.model.GenerativeModelFeatureMapping;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;
import jceliac.featureOptimization.FeatureOptimizationMethod;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;
import jceliac.featureOptimization.IterableFeatureOptimizer;
import jceliac.tools.parallel.ParallelTaskExecutor;
import jceliac.validation.generic.CrossValidationParameters;
import jceliac.validation.generic.GenericCrossValidation;
import jceliac.validation.generic.MultiVariableOptimizer;
import jceliac.validation.generic.ParallelValidationState;
import jceliac.validation.generic.ValidationMethodParameters;

/**
 *
 * @author shegen
 */
public class NBIProjectCrossValidation
        extends GenericCrossValidation
{

    private final ArrayList<Long> nbiFeatureIDs;
    private final ArrayList<Long> tradFeatureIDs;

    protected NBIProjectValidationParameters myParameters;

    public NBIProjectCrossValidation(ValidationMethodParameters parameters,
                                     ClassificationMethod classificationMethod,
                                     GenerativeModel model,
                                     FeatureOptimizationMethod featureOptimizationMethod,
                                     ArrayList<Long> nbiFeatureIDs,
                                     ArrayList<Long> tradFeatureIDs)
    {
        super(parameters, classificationMethod, model, featureOptimizationMethod);
        this.myParameters = (NBIProjectValidationParameters) parameters;
        this.nbiFeatureIDs = nbiFeatureIDs;
        this.tradFeatureIDs = tradFeatureIDs;
    }

    @Override
    protected IteratedClassificationResult validateOuter(FeatureCollection features, 
                                                 CrossValidationParameters cvParams)
            throws JCeliacGenericException
    {
        return this.validateOuterMultiThreaded(features, cvParams);
    }

    @Override
    protected IteratedClassificationResult validateInner(FeatureCollection features, 
                                                 CrossValidationParameters cvParams)
            throws JCeliacGenericException
    {
        return this.validateInnerMultiThreaded(features, cvParams);
    }

    private boolean isNBI(DiscriminativeFeatures features)
            throws JCeliacGenericException
    {
        boolean isNBI = this.nbiFeatureIDs.contains(features.getFeatureID());
        boolean isTrad = this.tradFeatureIDs.contains(features.getFeatureID());

        // sanity check
        if(isNBI == isTrad) {
            throw new JCeliacGenericException("Feature can not be traditional and NBI at the same time!");
        }
        return isNBI;
    }

    private FeatureCollection removeNBIFeatures(FeatureCollection collection)
            throws JCeliacGenericException
    {
        ArrayList<DiscriminativeFeatures> features = collection.getFeatures();
        ArrayList<DiscriminativeFeatures> tradFeatures = new ArrayList<>();

        for(DiscriminativeFeatures tmpFeatures : features) {
            if(false == this.isNBI(tmpFeatures)) {
                tradFeatures.add(tmpFeatures);
            }
        }
        return FeatureCollection.createClonedFeatureCollection(tradFeatures);
    }

    private FeatureCollection removeTraditionalFeatures(FeatureCollection collection)
            throws JCeliacGenericException
    {
        ArrayList<DiscriminativeFeatures> features = collection.getFeatures();
        ArrayList<DiscriminativeFeatures> nbiFeatures = new ArrayList<>();

        for(DiscriminativeFeatures tmpFeatures : features) {
            if(true == this.isNBI(tmpFeatures)) {
                nbiFeatures.add(tmpFeatures);
            }
        }
        return FeatureCollection.createClonedFeatureCollection(nbiFeatures);
    }

    /**
     * INNER cross validation runs an extra cross validation on the specific
     * portion of training data to optimize results. This can be slow, we
     * optimize speed by running multiple extra CVs in parallel.
     * <p>
     * @param features      Features to cross validate.
     * @param outerCVParams Parameters for the cross validation.
     * <p>
     * @return Result of cross validation based on parameter optimized on an
     *         inner cross validation run.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    @Override
    protected IteratedClassificationResult validateInnerMultiThreaded(FeatureCollection features,
                                                              CrossValidationParameters outerCVParams)
            throws JCeliacGenericException
    {

        Random rand = new Random(this.innerSeed);
        ClassificationResult overallResult = new ClassificationResult();
        IteratedClassificationResult iteratedResult = new IteratedClassificationResult();
        
        for(int iteration = 0; iteration < outerCVParams.getNumIterations(); iteration++) {
            ArrayList<Callable<ParallelValidationState>> parallelCVTasks = new ArrayList<>();

            // create splits, always based on the same random seed for each subset to ensure the same splits for each subset
            ArrayList<FeatureCollection[]> cvSplitData
                                           = this.createDataSplitForCVType(features,
                                                                           outerCVParams,
                                                                           rand);

            // parameters of the extra (inner) cross validation used to optimize parameters
            // currently 5-fold cross validation on the training set is performed to optimizes parameters
            CrossValidationParameters innerCVParams = new CrossValidationParameters(5, 1,
                                                                                    ValidationMethodParameters.ECrossValidationType.KFOLD,
                                                                                    this.parameters.getSplitStrategy());

            // iterate over all supplied splits (iterations of CVs)
            for(FeatureCollection[] currentSplit : cvSplitData) {
                for(int evaluationFoldIndex = 0; evaluationFoldIndex < currentSplit.length;
                    evaluationFoldIndex++) {
                    FeatureCollection currentTrainingFold = FeatureCollection.combineExcept(currentSplit, evaluationFoldIndex);
                    FeatureCollection currentEvaluationFold = currentSplit[evaluationFoldIndex];

                    FeatureCollection unimodalTrainingFold, unimodalEvaluationFold;
                    if(this.myParameters.trainNBI) {
                        unimodalTrainingFold = this.removeTraditionalFeatures(currentTrainingFold);
                        unimodalEvaluationFold = this.removeNBIFeatures(currentEvaluationFold);
                    } else {
                        unimodalTrainingFold = this.removeNBIFeatures(currentTrainingFold);
                        unimodalEvaluationFold = this.removeTraditionalFeatures(currentEvaluationFold);
                    }

                    parallelCVTasks.add(this.creatParallelOuterValidation(unimodalTrainingFold,
                                                                          unimodalEvaluationFold,
                                                                          innerCVParams));
                }
            }
            ParallelTaskExecutor<ParallelValidationState> taskExecutor = new ParallelTaskExecutor<>(parallelCVTasks, true);
            for(ParallelValidationState tmpState : taskExecutor.executeTasks()) {
                FeatureCollection cvTrainingFeaturesSubset
                                  = FeatureCollection.createClonedFeatureCollection(tmpState.getTrainingFeatures(), tmpState.getValidationResult().getFeatureSubset());

                FeatureCollection cvEvaluationFeaturesSubset
                                  = FeatureCollection.createClonedFeatureCollection(tmpState.getEvaluationFeatures(), tmpState.getValidationResult().getFeatureSubset());

                // classify based on validation on the training data
                GenerativeModelFeatureMapping modelMapping = this.model.trainModel(cvTrainingFeaturesSubset);
                FeatureCollection mappedTrainingDAta = modelMapping.mapFeatures(cvTrainingFeaturesSubset);
                ClassificationMapping trainedClassifier = this.classificationMethod.train(mappedTrainingDAta, tmpState.getValidationResult().getConfiguration());
                ArrayList<ClassificationOutcome> outcomes = this.runValidation(trainedClassifier, modelMapping, cvEvaluationFeaturesSubset);
                overallResult.addOutcomeList(outcomes);
            }
            iteratedResult.addSingleResult(overallResult);
        }
        return iteratedResult;
    }

    /**
     * This method performs classification based on cross-validation with
     * outer-validation see the IPMI'11 Hegenbart11c paper on outer-validation.
     * This method is prone to over-fitting and should be avoided if
     * inner-validation is computationally feasible or to optimize features on a
     * training set and then use them on a distinct set. Inner validation
     * performs the optimization of parameter on calling this method on a subset
     * of the training data.
     * <p>
     * This method uses multi threading based on the evaluation of entries.
     * <p>
     * @param features Data to perform cross validation on.
     * @param cvParams Parameters of the cross validation.
     * <p>
     * @return Optimized classification result containing the best classifier
     *         configuration and feature subset.
     * <p>
     * @throws JCeliacGenericException
     */
    @Override
    protected IteratedClassificationResult validateOuterMultiThreaded(FeatureCollection features,
                                                              CrossValidationParameters cvParams)

            throws JCeliacGenericException
    {
        IteratedClassificationResult iteratedResult = new IteratedClassificationResult();
        
        for(int iteration = 0; iteration < cvParams.getNumIterations(); iteration++) {
            MultiVariableOptimizer globalMultiOptimizer = new MultiVariableOptimizer();
            MultiVariableOptimizer localMultiOptimizer = new MultiVariableOptimizer();

            // optimize features based on the overall classification rate of the cross validation
            int featureDim = features.get(0).getFeatureVectorDimensionality();
            IterableFeatureOptimizer featureOptimizer = this.featureOptimizationMethod.initializeOptimization(featureDim);
            FeatureVectorSubsetEncoding featureSubsetEncoding = featureOptimizer.startOptimization();

            do {
                FeatureCollection featuresSubset = FeatureCollection.createClonedFeatureCollection(features, featureSubsetEncoding);
                // create splits, always based on the same random seed for each subset to ensure the same splits for each subset
                ArrayList<FeatureCollection[]> cvSplitData = this.createDataSplitForCVType(featuresSubset,
                                                                                           cvParams,
                                                                                           new Random(this.outerSeed));

                // optimize classifier over the overall result of the CVs (outer CV)
                IterableClassificationParameterOptimizer classificationOptimizer
                                                         = this.classificationMethod.initializeOptimization();
                ClassificationConfiguration classificationConfiguration = classificationOptimizer.startOptimization();
                ClassificationResult overallResult;
                do {
                    overallResult = new ClassificationResult();
                    localMultiOptimizer = new MultiVariableOptimizer();
                    // iterate over all supplied splits (iterations of CVs)
                    for(FeatureCollection[] currentSplit : cvSplitData) {
                        FeatureCollection allCurrentData = FeatureCollection.combine(currentSplit);

                        // train model based on entire split data (outer CV)
                        GenerativeModelFeatureMapping modelMapping = this.model.trainModel(allCurrentData);

                        ArrayList<Callable<ArrayList<ClassificationOutcome>>> parallelCVTasks = new ArrayList<>();

                        // evaluate current split
                        for(int evaluationFoldIndex = 0; evaluationFoldIndex < currentSplit.length;
                            evaluationFoldIndex++) {
                            FeatureCollection currentTrainingFold = FeatureCollection.combineExcept(currentSplit, evaluationFoldIndex);
                            FeatureCollection currentEvaluationFold = currentSplit[evaluationFoldIndex];

                            FeatureCollection unimodalTrainingFold, unimodalEvaluationFold;
                            if(this.myParameters.trainNBI) {
                                unimodalTrainingFold = this.removeTraditionalFeatures(currentTrainingFold);
                                unimodalEvaluationFold = this.removeNBIFeatures(currentEvaluationFold);
                            } else {
                                unimodalTrainingFold = this.removeNBIFeatures(currentTrainingFold);
                                unimodalEvaluationFold = this.removeTraditionalFeatures(currentEvaluationFold);
                            }

                            FeatureCollection currentTrainingFoldMM = modelMapping.mapFeatures(unimodalTrainingFold);
                            // train a classifier using the model mapped data of the current feature vector subset
                            parallelCVTasks.add(
                                    this.createParallelTrainingAndValidation(currentTrainingFoldMM, classificationConfiguration, modelMapping, unimodalEvaluationFold));
                        }
                        ParallelTaskExecutor<ArrayList<ClassificationOutcome>> taskExecutor = new ParallelTaskExecutor<>(parallelCVTasks, true);
                        for(ArrayList<ClassificationOutcome> outcomes : taskExecutor.executeTasks()) {
                            overallResult.addOutcomeList(outcomes);
                        }
                    }
                    localMultiOptimizer.addResultConfiguration(overallResult, featureSubsetEncoding, classificationConfiguration);
                    globalMultiOptimizer.addResultConfiguration(overallResult, featureSubsetEncoding, classificationConfiguration);
                    classificationConfiguration = classificationOptimizer.iterateOptimization(overallResult, classificationConfiguration);
                } while(false == classificationOptimizer.iterationConverged());
                // notify the optimizer and retrieve the currently best feature vector subset
                featureSubsetEncoding = featureOptimizer.iterateOptimization(localMultiOptimizer.getBestResult(), featureSubsetEncoding);
            } while(false == featureOptimizer.iterationConverged());
            iteratedResult.addSingleResult(globalMultiOptimizer.getBestResult());
        }
        return iteratedResult;
    }

    /**
     * Creates a separate validation. This is done in innver validation mode for
     * each split that is evaluated.
     * <p>
     * @param trainingData
     * @param evaluationData Not used in here, but used to run the
     *                       classifiaction on the optimized results later.
     * @param cvParams       <p>
     * @return <p>
     * @throws JCeliacGenericException
     */
    @Override
    protected Callable<ParallelValidationState>
            creatParallelOuterValidation(FeatureCollection trainingData,
                                         FeatureCollection evaluationData,
                                         CrossValidationParameters cvParams)
            throws JCeliacGenericException
    {
        return () -> {
            MultiVariableOptimizer globalMultiOptimizer = new MultiVariableOptimizer();
            MultiVariableOptimizer localMultiOptimizer = new MultiVariableOptimizer();

            // optimize features based on the overall classification rate of the cross validation
            int featureDim = trainingData.get(0).getFeatureVectorDimensionality();
            IterableFeatureOptimizer featureOptimizer = this.featureOptimizationMethod.initializeOptimization(featureDim);
            FeatureVectorSubsetEncoding featureSubsetEncoding = featureOptimizer.startOptimization();

            do {
                FeatureCollection featureSubset = FeatureCollection.createClonedFeatureCollection(trainingData,
                                                                                                  featureSubsetEncoding);
                // create splits, always based on the same random seed for each subset to ensure the same splits for each subset
                ArrayList<FeatureCollection[]> cvSplitData = this.createDataSplitForCVType(featureSubset,
                                                                                           cvParams,
                                                                                           new Random(this.outerSeed));

                this.classificationMethod.hint(featureSubset, null);
                // optimize classifier over the overall result of the CVs (outer CV)
                IterableClassificationParameterOptimizer classificationOptimizer
                                                         = this.classificationMethod.initializeOptimization();
                ClassificationConfiguration classificationConfiguration = classificationOptimizer.startOptimization();
                ClassificationResult overallResult;
                do {
                    overallResult = new ClassificationResult();
                    localMultiOptimizer = new MultiVariableOptimizer();
                    // iterate over all supplied splits (iterations of CVs)
                    for(FeatureCollection[] currentSplit : cvSplitData) {
                        FeatureCollection allCurrentData = FeatureCollection.combine(currentSplit);

                        // train model based on entire split data (outer CV)
                        GenerativeModelFeatureMapping modelMapping = this.model.trainModel(allCurrentData);

                        // evaluate current split
                        for(int evaluationFoldIndex = 0; evaluationFoldIndex < currentSplit.length;
                            evaluationFoldIndex++) {
                            FeatureCollection currentTrainingFold = FeatureCollection.combineExcept(currentSplit, evaluationFoldIndex);
                            FeatureCollection currentEvaluationFold = currentSplit[evaluationFoldIndex];

                            FeatureCollection currentTrainingFoldMapped = modelMapping.mapFeatures(currentTrainingFold);

                            // train a classifier using the model mapped data of the current feature vector subset
                            ClassificationMapping trainedMethod = this.classificationMethod.train(currentTrainingFoldMapped,
                                                                                                  classificationConfiguration);
                            ArrayList<ClassificationOutcome> outcomeList = this.runValidation(trainedMethod, modelMapping, currentEvaluationFold);
                            overallResult.addOutcomeList(outcomeList);
                        }
                    }
                    localMultiOptimizer.addResultConfiguration(overallResult, featureSubsetEncoding, classificationConfiguration);
                    globalMultiOptimizer.addResultConfiguration(overallResult, featureSubsetEncoding, classificationConfiguration);
                    classificationConfiguration = classificationOptimizer.iterateOptimization(overallResult, classificationConfiguration);
                } while(false == classificationOptimizer.iterationConverged());
                // notify the optimizer and retrieve the currently best feature vector subset
                featureSubsetEncoding = featureOptimizer.iterateOptimization(localMultiOptimizer.getBestResult(), featureSubsetEncoding);
            } while(false == featureOptimizer.iterationConverged());
            ClassificationResult bestResult = globalMultiOptimizer.getBestResult();
            ParallelValidationState state = new ParallelValidationState(trainingData,
                                                                        evaluationData,
                                                                        bestResult);
            return state;
        };
    }

}
