/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.validation.custom.SampledDistinctValidation;

import java.util.ArrayList;
import java.util.HashMap;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;
import jceliac.tools.parser.LOPOIdentifierParser;

/**
 * Maps an image to an image id, this is used to identify the same images
 * at different scales (KTH-TIPS) database such that the same image is not
 * in the training and evaluation set. 
 * 
 * @author shegen
 */
public class ImageNameMapping
{
    // key = unique ID per parentImage, value = List of name of sub-images (or scaled-image)
    private final HashMap<String, ArrayList <String>> imageIDToNameMapping ;
    // key = name of sub-image (or scaled-image), value = unique ID per parentImage
    private final HashMap<String, String> imageNameToIDMapping;

    public ImageNameMapping(FeatureCollection trainingFeatures,
                        FeatureCollection evaluationFeatures)
            throws JCeliacGenericException
    {
        this.imageIDToNameMapping = new HashMap <>();
        this.imageNameToIDMapping = new HashMap <>();
        this.createMapping(trainingFeatures);
        this.createMapping(evaluationFeatures);
    }

    private void createMapping(FeatureCollection features)
            throws JCeliacGenericException
    {
        for(DiscriminativeFeatures tmp : features) 
        {
            LOPOIdentifierParser parser = LOPOIdentifierParser.createParser(tmp.getSignalIdentifier());

            if(parser == null) {
                throw new JCeliacGenericException("Could not create parser!");
            }
            String uniqueID = parser.generateUniqueIdentifier(tmp.getSignalIdentifier());
            ArrayList <String> namesPerID = new ArrayList <>();
            if(this.imageIDToNameMapping.containsKey(uniqueID) == false) {
                namesPerID = new ArrayList <>();
            }else {
                namesPerID = (ArrayList <String>)this.imageIDToNameMapping.get(uniqueID);
            }
            namesPerID.add(tmp.getSignalIdentifier());
            this.imageIDToNameMapping.put(uniqueID, namesPerID);
            this.imageNameToIDMapping.put(tmp.getSignalIdentifier(), uniqueID); 
        }
    }
    
    public String getIDForImage(String imageIdentifier)
    {
        return this.imageNameToIDMapping.get(imageIdentifier);
    }
    public ArrayList <String> getIdentifiersForID(String imageID) {
        return this.imageIDToNameMapping.get(imageID);
    }    
}
