/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.validation.custom.SampledDistinctValidation;

import jceliac.validation.generic.ValidationMethodParameters;

/**
 *
 * @author shegen
 */
public class SampledDistinctValidationParameters
    extends ValidationMethodParameters
{

    protected double evaluationSplitFraction = 0.25;
    protected double trainingSplitFraction = 0.75;
    
    public SampledDistinctValidationParameters()
    {
        this.numberOfIterations = 50; // default
    }

    public double getEvaluationSampleFraction()
    {
        return evaluationSplitFraction;
    }

    public double getTrainingSampleFraction()
    {
        return trainingSplitFraction;
    }
    
    
    
    
}
