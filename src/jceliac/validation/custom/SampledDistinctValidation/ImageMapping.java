/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.validation.custom.SampledDistinctValidation;

import java.util.HashMap;

/**
 * Maps an image to an image id, this is used to identify the same images
 * at different scales (KTH-TIPS) database such that the same image is not
 * in the training and evaluation set. 
 * 
 * @author shegen
 */
public class ImageMapping
{
    private HashMap <Long, String> imageNameToIDMapping;
    
    
}
