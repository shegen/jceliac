/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.validation.custom.SampledDistinctValidation;

import java.util.ArrayList;
import java.util.Random;
import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.ClassificationMapping;
import jceliac.classification.ClassificationMethod;
import jceliac.classification.ClassificationOutcome;
import jceliac.classification.ClassificationResult;
import jceliac.classification.IterableClassificationParameterOptimizer;
import jceliac.classification.IteratedClassificationResult;
import jceliac.classification.KNN.ClassificationConfigurationKNN;
import jceliac.classification.model.GenerativeModel;
import jceliac.classification.model.GenerativeModelFeatureMapping;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;
import jceliac.featureExtraction.affineScaleLBPScaleBias.DistanceStats;
import jceliac.featureOptimization.FeatureOptimizationMethod;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;
import jceliac.featureOptimization.IterableFeatureOptimizer;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.AttributeCounter;
import jceliac.validation.generic.ValidationMethod;
import jceliac.validation.generic.ValidationMethodParameters;

/**
 * This validation is used to perform some sort of cross validation that is
 * constrained by certain scales in training and evaluation sets. 
 * 
 * @author shegen
 */
public class SampledDistinctValidation
    extends ValidationMethod
{
    private final long RANDOM_SEED = 329423948L;
        
    protected SampledDistinctValidationParameters myParameters;
    public SampledDistinctValidation(ValidationMethodParameters parameters, 
                                      ClassificationMethod classificationMethod, 
                                      GenerativeModel model, 
                                      FeatureOptimizationMethod featureOptimizationMethod)
    {
        super(parameters, classificationMethod, model, featureOptimizationMethod);
        this.myParameters = (SampledDistinctValidationParameters) parameters;
    }
    
    @Override
    public IteratedClassificationResult validateInner(FeatureCollection trainingFeatures, 
                                                      FeatureCollection evaluationFeatures)
            throws JCeliacGenericException
    {
        if(evaluationFeatures.isEmpty()) {
            throw new JCeliacGenericException("SampledDistinctValidation requires two data sets.");
        }
        ArrayList <Double> ocrs = new ArrayList <>();
        
        // create subsets for the training and evaluation set (this means that we 
        // always train a scale and evaluate another scale) and validate it
        Random rand = new Random(RANDOM_SEED);
        IteratedClassificationResult iteratedResult = new IteratedClassificationResult();
        
        this.model.initializeModel();
        GenerativeModelFeatureMapping trainedModel = this.model.trainModel(trainingFeatures);
        
        for(int iteration = 0; iteration < this.myParameters.getNumberOfIterations(); iteration++)
        {
            FeatureCollection trainingSplit = 
                    this.createBalancedSubset(trainingFeatures, 
                                              this.myParameters.getTrainingSampleFraction(), 
                                              rand);
            FeatureCollection evaluationSplit = 
                    this.createBalancedSubset(evaluationFeatures, 
                                              this.myParameters.getEvaluationSampleFraction(), 
                                              rand);
            
            IteratedClassificationResult result = this.validateDistinct(trainingSplit, 
                                                                        evaluationSplit, 
                                                                        trainedModel);
   
            iteratedResult.addSingleResult(result.getSingleResult(0));
            if(result.getConfiguration() instanceof ClassificationConfigurationKNN)
            {
                ClassificationConfigurationKNN config = (ClassificationConfigurationKNN)result.getConfiguration();
                ocrs.add(result.getOverallClassificationRate());
                System.out.println(iteration + "/"+this.myParameters.getNumberOfIterations() +": "+result.getOverallClassificationRate()  + " ("+ArrayTools.mean(ocrs)+", k = "+config.getkValue()+")");
            }
        }
        return iteratedResult;  
    }
    
    

    public IteratedClassificationResult validateDistinct(FeatureCollection trainingFeatures,
                                                         FeatureCollection evaluationFeatures,
                                                         GenerativeModelFeatureMapping modelMapping)
            throws JCeliacGenericException
    {
        int featureDim = trainingFeatures.get(0).getFeatureVectorDimensionality();
        IterableFeatureOptimizer featureOptimizer = this.featureOptimizationMethod.initializeOptimization(featureDim);
        FeatureVectorSubsetEncoding featureSubsetEncoding = featureOptimizer.startOptimization();
        
        FeatureCollection trainingSubsetFeatures = FeatureCollection.createClonedFeatureCollection(trainingFeatures, featureSubsetEncoding);
        FeatureCollection evaluationSubsetFeatures = FeatureCollection.createClonedFeatureCollection(evaluationFeatures, featureSubsetEncoding);
        
        // map the training data using the model
        FeatureCollection mappedTrainingData = modelMapping.mapFeatures(trainingSubsetFeatures);
        
            // optimize classification paramters based on the distinct set result,
        // this overfits the parameters
        IterableClassificationParameterOptimizer classificationOptimizer = this.classificationMethod.initializeOptimization();
        ClassificationConfiguration classificationConfiguration
                                    = classificationOptimizer.startOptimization();

        this.classificationMethod.hint(mappedTrainingData, modelMapping.mapFeatures(evaluationSubsetFeatures));

        // train a classifier using the model mapped data of the current feature vector subset
        ClassificationMapping trainedMethod
                              = this.classificationMethod.train(mappedTrainingData,
                                                                classificationConfiguration);

        ClassificationResult result = new ClassificationResult();
        result.setConfiguration(classificationConfiguration);
        ArrayList<ClassificationOutcome> outcomeList = this.runValidation(trainedMethod, modelMapping, evaluationSubsetFeatures);
        result.addOutcomeList(outcomeList);
        

        IteratedClassificationResult iteratedResult = new IteratedClassificationResult();
        iteratedResult.addSingleResult(result);
        return iteratedResult;
    }

    
    private void evalDistanceStats(ArrayList <DistanceStats> distanceStats) 
    {
        double diffBelow = 0;
        double diffAbove = 0;
        double stdBelow = 0;
        double stdAbove = 0;
        double numBelow = 0;
        double numAbove = 0;
        
        for(DistanceStats stats : distanceStats) {
            if(stats.numNeighbors > 15) {
                diffAbove += stats.diffMaxMin;
                stdAbove += stats.std;
                numAbove++;
            }else {
                diffBelow += stats.diffMaxMin;
                stdBelow += stats.std;
                numBelow ++;
            } 
        }
        diffBelow /= numBelow;
        diffAbove /= numAbove;
        stdBelow /= numBelow;
        stdAbove /= numAbove;
        
        System.out.println("Diff below: "+diffBelow);
        System.out.println("Diff above: "+diffAbove);
        
        System.out.println("Std below: "+stdBelow);
        System.out.println("Std above: "+stdAbove);
        
    }
    
    
    @Override
    protected IteratedClassificationResult validateOuter(FeatureCollection trainingFeatures, 
                                                 FeatureCollection evaluationFeatures)
            throws JCeliacGenericException
    {
        throw new UnsupportedOperationException("Outer validation not supported!"); 
    }

    
    private synchronized FeatureCollection createBalancedSubset(FeatureCollection features, 
                                                                double fraction, Random rand) 
            throws JCeliacGenericException
    {
        // figure out number of classes
        AttributeCounter <Integer> counter = new AttributeCounter<>();
        for(DiscriminativeFeatures f : features) {
            counter.count(f.getClassNumber());
        }
        int numClasses = counter.getDistinctAttributeCount();
        int numRequestedFeatures = (int)Math.round(features.size() * fraction);
      
        // build list of features by class
        @SuppressWarnings("unchecked")
        ArrayList <DiscriminativeFeatures> [] featuresByClass = new ArrayList[numClasses];
        int its = 0;
        for(DiscriminativeFeatures f : features) {
            int classNum = f.getClassNumber();
            if(featuresByClass[classNum] == null) {
                featuresByClass[classNum] = new ArrayList <>();
            }
            featuresByClass[classNum].add(f);
            its++;
        }
        
        ArrayList <DiscriminativeFeatures> featureSubset = new ArrayList <>();
        // try to create random balanced set
        for(int n = 0; n < numRequestedFeatures; n++) {
            int curClass = n % numClasses;
            if(featuresByClass[curClass].isEmpty()) {
                // no more images from this class available ignore
            }else {
                int randomIndex = rand.nextInt(featuresByClass[curClass].size());
                DiscriminativeFeatures feature = featuresByClass[curClass].remove(randomIndex);
                featureSubset.add(feature);
            }
        }
        return FeatureCollection.createClonedFeatureCollection(featureSubset);
    }
    
    
}
