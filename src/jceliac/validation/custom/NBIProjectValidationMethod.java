/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.validation.custom;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationMethod;
import jceliac.classification.ClassificationResult;
import jceliac.classification.IteratedClassificationResult;
import jceliac.classification.model.GenerativeModel;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;
import jceliac.featureOptimization.FeatureOptimizationMethod;
import jceliac.validation.generic.CrossValidationParameters;
import jceliac.validation.generic.DistinctSetValidation;
import jceliac.validation.generic.ValidationMethodParameters;

/**
 * Validation for the NBI Project, this is used to achieve a LOPO cross validation
 * restricting the training set to either the NBI or the tradiational data only. 
 * 
 * 
 * @author shegen
 */
public class NBIProjectValidationMethod
    extends DistinctSetValidation
{

    
    protected NBIProjectValidationParameters myParameters;
    public NBIProjectValidationMethod(ValidationMethodParameters parameters, 
                                      ClassificationMethod classificationMethod, 
                                      GenerativeModel model, 
                                      FeatureOptimizationMethod featureOptimizationMethod)
    {
        super(parameters, classificationMethod, model, featureOptimizationMethod);
        this.myParameters = (NBIProjectValidationParameters) parameters;
    }
    
    @Override
    public IteratedClassificationResult validateInner(FeatureCollection trainingFeatures, 
                                              FeatureCollection evaluationFeatures)
            throws JCeliacGenericException
    {
         ArrayList <Long> nbiFeatureIDs = new ArrayList <>();
         ArrayList <Long> tradFeatureIDs = new ArrayList <>();
         
         if(this.myParameters.trainNBI == true) {
             for(DiscriminativeFeatures tmp : trainingFeatures) {
                 nbiFeatureIDs.add(tmp.getFeatureID());
             }
             for(DiscriminativeFeatures tmp : evaluationFeatures) {
                 tradFeatureIDs.add(tmp.getFeatureID());
             }
         }else {
             for(DiscriminativeFeatures tmp : trainingFeatures) {
                 tradFeatureIDs.add(tmp.getFeatureID());
             }
             for(DiscriminativeFeatures tmp : evaluationFeatures) {
                 nbiFeatureIDs.add(tmp.getFeatureID());
             }
         }
         FeatureCollection mergedFeatures = FeatureCollection.combine(new FeatureCollection[] {trainingFeatures, evaluationFeatures});
         NBIProjectCrossValidation cv = new NBIProjectCrossValidation(this.parameters, 
                                                                      this.classificationMethod, 
                                                                      this.model, 
                                                                      this.featureOptimizationMethod, 
                                                                      nbiFeatureIDs, tradFeatureIDs);
         CrossValidationParameters cvParams = new CrossValidationParameters(10, 5, ValidationMethodParameters.ECrossValidationType.KFOLD, ValidationMethodParameters.ESplitStrategy.PATIENT);
         return cv.validateInner(mergedFeatures, cvParams);
    }

    @Override
    public IteratedClassificationResult validateOuter(FeatureCollection trainingFeatures, 
                                              FeatureCollection evaluationFeatures)
            throws JCeliacGenericException
    {
        return super.validateOuter(trainingFeatures, evaluationFeatures); //To change body of generated methods, choose Tools | Templates.
    }
    
}
