/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment;

import jceliac.classification.classificationParameters.ClassificationParameters.*;
import jceliac.experiment.Experiment.ExperimentState;
import jceliac.tools.transforms.wavelet.WaveletTransform.*;
import jceliac.classification.*;
import java.io.Serializable;
import jceliac.logging.*;
import java.util.*;
import java.net.*;
import jceliac.JCeliacGenericException;

/**
 * MainExperiment is the abstraction of an Experiment that contains multiple
 * SubExperiments.
 * <p>
 * A MainExperiment is initialized using a set of parameter ranges defined by
 * the user. The code then creates a single object of subtype Experiment for
 * each combination of parameters to optimize. Each SubExperiment is then
 * assigned to a MainExperiment which is related to the ExprimentTable within
 * the database. A MainExperiment can be restarted and only SubExperiments which
 * have not finished will be restarted (if it's a OnlineExperiment).
 * <p>
 * @author shegen
 */
public class MainExperiment
        implements Serializable
{

    private long PK = -1;
    protected int index = 0;
    protected Thread shutdownHook;
    protected String experimentName;
    protected boolean clean = false;
    protected boolean online = false;
    protected ArrayList<Experiment> subExperiments;
    protected ArrayList<IteratedClassificationResult> classificationResults;

    public MainExperiment(String experimentName)
    {
        this.experimentName = experimentName;
        this.subExperiments = new ArrayList<>();
        this.classificationResults = new ArrayList<>();

        this.shutdownHook = new ExperimentCleanupThread(this);
        Runtime.getRuntime().addShutdownHook(shutdownHook);

    }

    public int getSubExperimentCount()
    {
        return this.subExperiments.size();
    }

    public Experiment getNextExperiment()
    {
        Experiment experiment = this.subExperiments.get(0);
        return experiment;
    }

    public boolean hasNext()
    {
        return this.subExperiments.size() > 0;
    }

    public void addSubExperiment(Experiment experiment)
    {
        this.subExperiments.add(experiment);
        experiment.setSubExperimentIdentifier(this.subExperiments.size());
    }



    /**
     * Initializes the main experiment.
     * <p>
     * @throws InitializeFailedException
     * @throws Exception
     */
    public void initializeMainExperiment()
            throws InitializeFailedException,
                   Exception
    {
        if(this.subExperiments.size() < 1) {
            throw new InitializeFailedException("No Sub-Experiments for Main-Experiment!");
        }
    }

    /**
     * Cleanup the main experiment.
     * <p>
     * @throws Exception
     */
    public synchronized void cleanupMainExperiment()
            throws Exception
    {
        removeShutdownHook();
        this.clean = true;
    }

    public boolean isClean()
    {
        return this.clean;
    }

    public void removeShutdownHook()
    {
        Runtime.getRuntime().removeShutdownHook(this.shutdownHook);
    }

    public String getExperimentName()
    {
        return this.experimentName;
    }

    /**
     * Runs the main experiment.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public void runMainExperiment()
            throws JCeliacGenericException
    {
        while(this.hasNext()) {
            Experiment exp = this.getNextExperiment();
            exp.initializeExperiment();
            Experiment.log(JCeliacLogger.LOG_INFO, "Running Experiment: %s.", exp.getExperimentName());
            IteratedClassificationResult result = exp.runExperimentThreaded();
            // remove some unused stuff to avoid a memory leak!
            result.clearData();
            this.classificationResults.add(result);
            exp.cleanupExperiment(ExperimentState.SUCCESS);

            // remove the reference to free up some memory we might have missed!
            // we do not use the experiment's object anymore!
            this.subExperiments.remove(exp);
        }
    }

     public String getHostname()
    {
        try {
            InetAddress addr = InetAddress.getLocalHost();
            String hostname = addr.getHostName();
            return hostname;
        } catch(UnknownHostException e) {
            return "UNKNOWN";
        }
    }

}
