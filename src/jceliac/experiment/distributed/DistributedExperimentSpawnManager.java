/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment.distributed;

import com.jcraft.jsch.JSchException;
import java.io.IOException;
import java.util.ArrayList;
import jceliac.JCeliacGenericException;

/**
 *
 * @author shegen
 */
public class DistributedExperimentSpawnManager
{

    private final ArrayList<RemoteExecutor> remoteExecutors;
    private final String serverHostname;
    private final String javaCmd = "java -Xms40000m -Xmx40000m -jar";

    public DistributedExperimentSpawnManager(ArrayList<String> clientHostnames,
                                             String serverHostname,
                                             String username, String password)
            throws JSchException, JCeliacGenericException
    {
        this.remoteExecutors = new ArrayList<>();
        this.serverHostname = serverHostname;
        for(String clientHost : clientHostnames) {
            RemoteExecutor e = new RemoteExecutor(clientHost, username, password);
            this.remoteExecutors.add(e);
        }
    }

    /**
     * Spawns client hosts using SSH (JSCH library).
     * <p>
     * @param jarfilePath The jar file to execute at the client.
     * <p>
     * @throws JCeliacGenericException
     */
    public void spawnClients(String jarfilePath)
            throws JCeliacGenericException
    {
        try {
            String cwd = System.getProperty("user.dir");
            
            String fullCmd = "cd " + cwd + ";" +  
                            this.javaCmd + " " + jarfilePath + " -rmi-client " + this.serverHostname + " &";
            for(RemoteExecutor e : this.remoteExecutors) {
                e.executeCommand(fullCmd);
            }
        } catch(JSchException | IOException e) {
            throw new JCeliacGenericException(e);
        } finally {
            for(RemoteExecutor e : this.remoteExecutors) {
                e.cleanup();
            }
        }

    }

}
