/*
 *  JCeliac Copyright (C) 2010-20114 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment.distributed;

import jceliac.classification.classificationParameters.ClassificationParameters.*;
import jceliac.experiment.Experiment.ExperimentState;
import jceliac.tools.transforms.wavelet.WaveletTransform.*;
import java.io.Serializable;
import jceliac.classification.*;
import jceliac.logging.*;
import java.util.*;
import java.net.*;
import java.rmi.RemoteException;
import jceliac.JCeliacGenericException;
import jceliac.experiment.Experiment;
import jceliac.experiment.ExperimentCleanupThread;
import jceliac.experiment.InitializeFailedException;
import jceliac.experiment.MainExperiment;

/**
 * DistributedMainExperiment is the abstraction of an Experiment that contains
 * multiple SubExperiments distributed on multiple client hosts.
 * <p>
 * A MainExperiment is initialized using a set of parameter ranges defined by
 * the user. The code then creates a single object of subtype Experiment for
 * each combination of parameters to optimize. Each SubExperiment is then
 * assigned to a MainExperiment which is related to the ExprimentTable within
 * the database. A MainExperiment can be restarted and only SubExperiments which
 * have not finished will be restarted (if it's a OnlineExperiment).
 * <p>
 * @author shegen
 */
public class DistributedMainExperiment
        extends MainExperiment
        implements Serializable
{

    protected int clientID;
    protected IDistributedExperimentConnectionHandler distributedConnectionHandler;
    protected String currentHostname = "";

    /**
     * Initializes the distributed main experiment.
     * <p>
     * @param experimentName Name of experiment.
     * <p>
     * @throws InitializeFailedException
     */
    public DistributedMainExperiment(String experimentName)
            throws InitializeFailedException
    {
        super(experimentName);
        this.experimentName = experimentName;
        this.subExperiments = new ArrayList<>();
        this.classificationResults = new ArrayList<>();

        this.shutdownHook = new ExperimentCleanupThread(this);
        Runtime.getRuntime().addShutdownHook(shutdownHook);
        try {
            this.currentHostname = InetAddress.getLocalHost().getHostName();
        } catch(UnknownHostException e) {
            throw new InitializeFailedException(e.getMessage());
        }

    }

    /**
     * Runs initialization prior to the experiment.
     * <p>
     * @throws InitializeFailedException
     */
    @Override
    public void initializeMainExperiment()
            throws InitializeFailedException
    {
        if(this.subExperiments.size() < 1) {
            throw new InitializeFailedException("No Sub-Experiments for Main-Experiment!");
        }
    }

    /**
     * Cleanup the experiment after finished.
     * <p>
     * @throws JCeliacGenericException
     */
    @Override
    public synchronized void cleanupMainExperiment()
            throws JCeliacGenericException
    {
        if(this.clean) {
            return;
        }
        this.removeShutdownHook();
        this.clean = true;
    }

    /**
     * Runs the main experiment and all corresponding sub-experiments. Notifies
     * the server after each experiment has finished.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    @Override
    public void runMainExperiment() throws JCeliacGenericException
    {
        String currentExperimentName = "";
        try {
         
            while(hasNext()) {
                DistributedOfflineExperiment exp = (DistributedOfflineExperiment) this.getNextExperiment();

                exp.setClientID(this.clientID);
                exp.setDistributedConnectionHandler(this.distributedConnectionHandler);
                exp.setCurrentHostname(this.currentHostname);

                exp.initializeExperiment();
                Experiment.log(JCeliacLogger.LOG_INFO, "Running Experiment: %s.", exp.getExperimentName());
                currentExperimentName = exp.getExperimentName();
                IteratedClassificationResult result = exp.runExperimentThreaded();
                // remove some unused stuff to avoid a memory leak!
                result.clearData();
                this.classificationResults.add(result);
                exp.cleanupExperiment(ExperimentState.SUCCESS);

                // remove the reference to free up some memory we might have missed!
                // we do not use the experiment's object anymore!
                this.subExperiments.remove(exp);

                DistributedClientStatus status = new DistributedClientStatus(DistributedClientStatus.EClientStatus.ExperimentFinished,
                                                                             exp.getExperimentName(),
                                                                             result);
                this.distributedConnectionHandler.notifyStatus(this.clientID, status);
            }
        } catch( JCeliacGenericException | RemoteException e) {
            try {
                Experiment.log(JCeliacLogger.LOG_ERROR, "Experiment failed: "+e.getMessage());
                Experiment.logException(JCeliacLogger.LOG_ERROR, e);
                DistributedClientStatus status = new DistributedClientStatus(DistributedClientStatus.EClientStatus.ExperimentFailed,
                                                                             currentExperimentName);
                status.setFailReason(e.getMessage());
                status.setStackTrace(e.getStackTrace());
                this.distributedConnectionHandler.notifyStatus(this.clientID, status);
            } catch(RemoteException ex) {
                Experiment.logException(JCeliacLogger.LOG_ERROR, ex);
            }
        }
    }

    public void setClientID(int clientID)
    {
        this.clientID = clientID;
    }

    public void setDistributedConnectionHandler(IDistributedExperimentConnectionHandler distributedConnectionHandler)
    {
        this.distributedConnectionHandler = distributedConnectionHandler;
    }

}
