/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment.distributed;

import jceliac.experiment.GlobalTestConfigurationParameters;
import java.rmi.server.UnicastRemoteObject;
import jceliac.experiment.MainExperiment;
import java.rmi.registry.LocateRegistry;
import java.net.MalformedURLException;
import com.jcraft.jsch.JSchException;
import jceliac.logging.JCeliacLogger;
import java.rmi.registry.Registry;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import jceliac.JCeliacGenericException;
import jceliac.Main;

/**
 * Implementation of the distributed experiment server.
 * <p>
 * @author shegen
 */
public class DistributedExperimentServer
        extends UnicastRemoteObject
        implements IDistributedExperimentConnectionHandler
{

    public final static String RMI_BIND_NAME = "//JCeliac";
    public static final int RMI_PORT = 45671;

    private final HashMap<Integer, Double> progessByClient = new HashMap<>();
    private int lastNumberOfLines = 0;
    private final HashMap<Integer, DistributedClientInfo> registeredClients = new HashMap<>();
    private final HashMap<Integer, MainExperiment> runningExperiments;
    private final GlobalTestConfigurationParameters globalParameters;
    private final HashMap<String, Integer> experimentFailCount;
    private final ArrayList<MainExperiment> queuedExperiments;
    private String bindingName;
    private Registry registry;
    private int nextID = 1;
    
    private final Object lock;

    public DistributedExperimentServer(GlobalTestConfigurationParameters globalParameters,
                                       ArrayList<MainExperiment> queuedExperiments)
            throws RemoteException
    {
        this.globalParameters = globalParameters;
        this.runningExperiments = new HashMap<>();
        this.experimentFailCount = new HashMap<>();
        this.queuedExperiments = queuedExperiments;
        this.lock = new Object();
    }

    /**
     * Registers a client at the server.
     * <p>
     * @param info The client info as supplied by the client.
     * <p>
     * @return The assigned client id is returned to the client.
     */
    @Override
    public int registerClient(DistributedClientInfo info)
    {
        synchronized(this.lock) {
            // generate a random client id
            int clientID = this.nextID;
            this.nextID++;
            this.registeredClients.put(clientID, info);
            Main.log(JCeliacLogger.LOG_INFO, "--------------------------");
            Main.log(JCeliacLogger.LOG_INFO, "New Distributed Client (ID = %d) registered:\n"
                                             + "Hostname: %s \n"
                                             + "CPULoad: %.4f \n"
                                             + "CPUs: %d", clientID, info.getHostname(), info.getAverageCPULoad(), info.getNumberOfCPUs());
            return clientID;
        }

    }

    /**
     * Handles the experiment requests by the clients.
     * <p>
     * @param clientID The requesting client's clientID.
 <p>
     * @return A runnable distributed main experiment if exists, or null if no
     *         experiments are available.
     */
    @Override
    public synchronized DistributedMainExperiment requestExperiment(int clientID)
    {
        synchronized(this.lock) 
        {
            Main.log(JCeliacLogger.LOG_INFO, "Client %s (ID = %d) requested experiment.", this.registeredClients.get(clientID).getHostname(), clientID);
            if(this.queuedExperiments.isEmpty()) {
                return null;
            }
            DistributedMainExperiment distributedExperiment = (DistributedMainExperiment) this.queuedExperiments.remove(0);
            this.runningExperiments.put(clientID, distributedExperiment);
            Main.log(JCeliacLogger.LOG_INFO, "Client %s (ID = %d) running experiment: %s.", this.registeredClients.get(clientID).getHostname(), clientID, distributedExperiment.getExperimentName());
            return distributedExperiment;
        }
    }

    /**
     * Handles the status notifications of the clients.
     * <p>
     * @param clientID ID of the client.
     * @param status   Status of the client.
     */
    @Override
    public void notifyStatus(int clientID, DistributedClientStatus status)
    {
        synchronized(this.lock) {
            switch(status.getStatus()) {
                case ExperimentFinished:
                    this.handleExperimentFinished(clientID, status);
                    break;
                case ExperimentFailed:
                    this.handleExperimentFailed(clientID, status);
                    break;

                case ExperimentRunning:
                    this.handleExperimentRunning(clientID, status);
                    break;
            }
        }
    }

    /**
     * Removes the client from the running experiments list and logs the
     * results.
     * <p>
     * @param clientID ID of client.
     * @param status   Status of client.
     */
    private void handleExperimentFinished(int clientID, DistributedClientStatus status)
    {
        synchronized(this.lock) {
            DistributedClientInfo info = this.registeredClients.get(clientID);
            Main.log(JCeliacLogger.LOG_RESULT, "%s finished on host %s.", status.getExperimentName(), info.getHostname());
            Main.log(JCeliacLogger.LOG_RESULT, "%s", status.report());

            // remove experiment from running list
            this.runningExperiments.remove(clientID);
            this.progessByClient.remove(clientID);
        }
    }

    /**
     * Handles failed experiments. We requeues failed experiments three times
     * until the experiment is marked as failed and removed from the queues.
     * <p>
     * @param clientID ID of the client handling the failed experiment.
     * @param status   Status of the client handling the failed experiment.
     */
    private void handleExperimentFailed(int clientID, DistributedClientStatus status)
    {
        synchronized(this.lock) {

            int failCount = 1;
            if(this.experimentFailCount.containsKey(status.getExperimentName()) == false) {
                this.experimentFailCount.put(status.getExperimentName(), 1);
            } else {
                failCount = this.experimentFailCount.get(status.getExperimentName());
                if(failCount > 3) {
                    Main.log(JCeliacLogger.LOG_ERROR, "Experiment (%s) failed %d times, giving up.", status.getExperimentName(), failCount);
                    this.runningExperiments.remove(clientID);
                    return;
                }
                failCount++;
                this.experimentFailCount.put(status.getExperimentName(), failCount);
            }
            Main.log(JCeliacLogger.LOG_ERROR, "Experiment (%s) failed %d times, requeuing.", status.getExperimentName(), failCount);
            Main.log(JCeliacLogger.LOG_ERROR, "Reason: %s.", status.getFailReason());
            Main.log(JCeliacLogger.LOG_ERROR, "Stack Trace: %s.", status.getStackTrace());

            // remove experiment from running list and requeue
            this.queuedExperiments.add(this.runningExperiments.get(clientID));
            this.progessByClient.remove(clientID);
        }
    }

    

    /**
     * Visualizes and logs the progress of running experiments.
     * <p>
     * @param clientID ID of client.
     * @param status   Status of client.
     */
    private void handleExperimentRunning(int clientID, DistributedClientStatus status)
    {
        synchronized(this.lock) {
            this.progessByClient.put(clientID, status.getCurrentProgress());
            // visualized the progress
            Set<Integer> keys = this.progessByClient.keySet();
            List<Integer> sortedKeys = new ArrayList<>(keys);
            java.util.Collections.sort(sortedKeys);

            for(int i = 0; i < this.lastNumberOfLines; i++) {
                Main.socketLogger.writeProgress("\33[1A\33[2K");
            }
            this.lastNumberOfLines = 0;

            for(Integer key : sortedKeys) {

                double progressPercent = this.progessByClient.get(key);
                String progressString = "";
                for(int i = 0; i < 10; i++) {
                    if(i >= progressPercent / 10.0d) {
                        progressString += " ";
                    } else {
                        progressString += "#";
                    }
                }
                DistributedClientInfo info = this.registeredClients.get(key);
                Main.socketLogger.writeProgress(info.getHostname() + "(ID = " + key + "): [" + progressString + "] " + String.format("%.2f.", progressPercent) + "\n");
                this.lastNumberOfLines++;
            }
        }

    }

    /**
     * Runs the supplied experiments by spawning the client hosts and
     * initializing the RMI service.
     * <p>
     * @throws JSchException
     * @throws RemoteException
     * @throws MalformedURLException
     * @throws jceliac.JCeliacGenericException
     * @throws java.lang.InterruptedException
     */
    public void runExperiments()
            throws JSchException, RemoteException, MalformedURLException,
                   JCeliacGenericException, InterruptedException
    {
        try {
            // server has to be started with -Djava.rmi.server=IP to ensure it binds to the correct 
            // network device!
            // start the rmiregistry
            this.registry = LocateRegistry.createRegistry(RMI_PORT);
            this.bindingName = "//JCeliac";
            // bind on that registry, do not use the Naming.rebind because it will mess up
            // the java.rmi.server.codebase, and I could not fix this!
            this.registry.rebind(this.bindingName, this);

        } catch(RemoteException e) {
            Main.log(JCeliacLogger.LOG_WARN, "Registry Already Exists!");
            throw new JCeliacGenericException(e);
        }
        // spawn client processes
        DistributedExperimentSpawnManager spawnManager
                                          = new DistributedExperimentSpawnManager(this.globalParameters.getClientHostnames(),
                                                                                  this.globalParameters.getServerHostname(),
                                                                                  this.globalParameters.getSSHUser(),
                                                                                  this.globalParameters.getSSHPassword());
        spawnManager.spawnClients(this.globalParameters.getJARFilename());
        // clients are supposed to be spawned now, as soon as they run they register at the server
        // from here on, all handling is done with a pull principle 
        while(true) {
            synchronized(this.queuedExperiments) {
                if(this.queuedExperiments.isEmpty() && this.runningExperiments.isEmpty()) {
                    break;
                }
                this.queuedExperiments.wait(1000);
            }
        }
    }

    public void cleanup()
    {
        try {
            if(this.registry != null) {
                this.registry.unbind("//JCeliac");
                UnicastRemoteObject.unexportObject(this, true);
            }
        } catch(NotBoundException | RemoteException e) {
            Main.log(JCeliacLogger.LOG_ERROR, "Could not cleanup DistributedExperimentServer: %s ", e.getMessage());
        }
    }

}
