/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment.distributed;

import java.io.Serializable;
import jceliac.classification.ClassificationResult;
import jceliac.classification.IteratedClassificationResult;

/**
 * Represents client status objects used to communicate between server and
 * client.
 * <p>
 * @author shegen
 */
public class DistributedClientStatus
        implements Serializable
{

    public enum EClientStatus
    {

        ExperimentRunning,
        WaitingForExperiment,
        ExperimentFinished,
        ClientFinished,
        ExperimentFailed

    };

    private final EClientStatus status;
    private double currentProgress;
    private String resultString;
    private String experimentName;
    private String failReason;
    private StackTraceElement [] stackTrace;
    

    public DistributedClientStatus(EClientStatus status, double currentProgress)
    {
        this.status = status;
        this.currentProgress = currentProgress;

    }

    public DistributedClientStatus(EClientStatus status, String experimentName,
                                   IteratedClassificationResult result)
    {
        this.status = status;
        this.resultString = this.createResultString(result);
        this.experimentName = experimentName;

    }

    public DistributedClientStatus(EClientStatus status, String experimentName)
    {
        this.status = status;
        this.experimentName = experimentName;
    }

    public DistributedClientStatus(EClientStatus status)
    {
        this.status = status;
    }

    public EClientStatus getStatus()
    {
        return status;
    }

    public double getCurrentProgress()
    {
        return currentProgress;
    }

    public String report()
    {
        String repString = "";
        repString += this.status + " ";

        switch(this.status) {
            case ExperimentRunning:
                repString += this.currentProgress;
                break;
            case ExperimentFinished:
                repString += this.resultString;
                break;
            case ClientFinished:
                repString += "";

        }
        return repString;
    }

    private String createResultString(IteratedClassificationResult result)
    {
        return "Overall Classification Accuracy: " + result.getOverallClassificationRate() * 100;
    }

    public String getResultString()
    {
        return resultString;
    }

    public void setResultString(String resultString)
    {
        this.resultString = resultString;
    }

    public String getExperimentName()
    {
        return experimentName;
    }

    public void setExperimentName(String experimentName)
    {
        this.experimentName = experimentName;
    }

    public String getFailReason()
    {
        return failReason;
    }

    public void setFailReason(String failReason)
    {
        this.failReason = failReason;
    }

    public String getStackTrace()
    {
        String ret = "";
        for(int i = 0; i < this.stackTrace.length; i++) {
            ret += this.stackTrace[i] + "\n";
        }
        return ret;
    }

    public void setStackTrace(StackTraceElement[] stackTrace)
    {
        this.stackTrace = stackTrace;
    }
    
    
    

}
