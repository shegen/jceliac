/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.experiment.distributed;

import java.io.Serializable;


/**
 * Represents information about registered clients. 
 * 
 * @author shegen
 */
public class DistributedClientInfo implements Serializable {

    private String hostname;
    private double averageCPULoad;
    private int numberOfCPUs;

    public DistributedClientInfo(String hostname, double averageCPULoad, int numberOfCPUs) {
        this.hostname = hostname;
        this.averageCPULoad = averageCPULoad;
        this.numberOfCPUs = numberOfCPUs;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public double getAverageCPULoad() {
        return averageCPULoad;
    }

    public void setAverageCPULoad(double averageCPULoad) {
        this.averageCPULoad = averageCPULoad;
    }

    public int getNumberOfCPUs() {
        return numberOfCPUs;
    }

    public void setNumberOfCPUs(int numberOfCPUs) {
        this.numberOfCPUs = numberOfCPUs;
    }

}
