/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment.distributed;

import jceliac.experiment.ExperimentParameters;
import jceliac.experiment.OfflineExperiment;
import jceliac.JCeliacGenericException;
import jceliac.experiment.Experiment;
import jceliac.logging.JCeliacLogger;
import java.rmi.RemoteException;

/**
 * The distributed offline experiment implementation.
 * <p>
 * @author shegen
 */
public class DistributedOfflineExperiment
        extends OfflineExperiment
{

    protected int clientID;
    protected IDistributedExperimentConnectionHandler distributedConnectionHandler;
    protected String currentHostname;

    public DistributedOfflineExperiment(ExperimentParameters param)
            throws JCeliacGenericException
    {
        super(param);
    }

    /**
     * Notifies the server of the client's progress and stores the computed
     * features at the main thread.
     * <p>
     * @param f Computed features. 
     * <p>
     * @throws JCeliacGenericException
     */
    @Override
    public synchronized void threadFinish(Object f)
            throws JCeliacGenericException
    {
        super.threadFinish(f);
        double progress = ((double) this.finishedImages * 100.0d) / (double) this.numberOfImages;
        DistributedClientStatus status = new DistributedClientStatus(DistributedClientStatus.EClientStatus.ExperimentRunning,
                                                                     progress);
        try {
            this.distributedConnectionHandler.notifyStatus(this.clientID, status);
        } catch(RemoteException e) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Remote Exception encountered: %s.", e.getMessage());
            Experiment.logException(JCeliacLogger.LOG_ERROR, e);
            throw new JCeliacGenericException(e);
        }
    }

    public void setClientID(int clientID)
    {
        this.clientID = clientID;
    }

    public void setDistributedConnectionHandler(IDistributedExperimentConnectionHandler distributedConnectionHandler)
    {
        this.distributedConnectionHandler = distributedConnectionHandler;
    }

    public String getCurrentHostname()
    {
        return currentHostname;
    }

    public void setCurrentHostname(String currentHostname)
    {
        this.currentHostname = currentHostname;
    }
  

}
