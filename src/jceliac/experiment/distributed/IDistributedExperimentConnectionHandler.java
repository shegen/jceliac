/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment.distributed;

import java.rmi.RemoteException;
import java.rmi.Remote;

/**
 * The interface of the remote object implemented by the distributed server.
 * <p>
 * @author shegen
 */
public interface IDistributedExperimentConnectionHandler
        extends Remote
{

    public int registerClient(DistributedClientInfo info) throws RemoteException;

    public DistributedMainExperiment requestExperiment(int id) throws
            RemoteException;

    public void notifyStatus(int clientID, DistributedClientStatus status)
            throws RemoteException;

}
