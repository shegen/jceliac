/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment.distributed;

import java.rmi.registry.LocateRegistry;
import jceliac.JCeliacGenericException;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.registry.Registry;
import jceliac.tools.cpu.CPUInfo;
import java.rmi.RemoteException;
import java.net.InetAddress;

/**
 * The client implementation for handling distributed experiments.
 * <p>
 * @author shegen
 */
public class DistributedExperimentClient
{

    private final String serverHostname;
    private IDistributedExperimentConnectionHandler handler;
    private int clientID;

    public DistributedExperimentClient(String serverHostname)
    {
        this.serverHostname = serverHostname;
    }

    /**
     * Connets to server and gets server interface handling remote rmi object.
     * <p>
     * @throws java.rmi.RemoteException
     * @throws java.rmi.NotBoundException
     * @throws jceliac.JCeliacGenericException
     */
    public void initialize()
            throws RemoteException, NotBoundException, JCeliacGenericException
    {
        String bindName = DistributedExperimentServer.RMI_BIND_NAME;

        // get registry, do not use the Naming.lookup due to problems with the java.rmi.server.codebase
        Registry registry = LocateRegistry.getRegistry(this.serverHostname, DistributedExperimentServer.RMI_PORT);
        this.handler = (IDistributedExperimentConnectionHandler) registry.lookup(bindName);
        this.clientID = this.handler.registerClient(this.createInformationForClient());
    }

    /**
     * Creates a DistributedClientInfo object with appropriate values.
     * <p>
     * @return
     * <p>
     * @throws JCeliacGenericException
     */
    private DistributedClientInfo createInformationForClient()
            throws JCeliacGenericException
    {
        try {
            String hostname = InetAddress.getLocalHost().getHostName();
            double cpuload = CPUInfo.getSystemCPULoad();
            int numCPUs = CPUInfo.getNumberOfCPUs();

            DistributedClientInfo info = new DistributedClientInfo(hostname, cpuload, numCPUs);
            return info;
        } catch(UnknownHostException e) {
            throw new JCeliacGenericException(e);
        }
    }

    /**
     * Requests an experiment to run from the server.
     * <p>
     * @return A DistributedMainExperiment as provided by the server.
     * <p>
     * @throws RemoteException
     */
    public DistributedMainExperiment requestExperiment()
            throws RemoteException
    {
        DistributedMainExperiment experiment = this.handler.requestExperiment(this.clientID);
        if(experiment == null) {
            return null;
        }

        experiment.setClientID(this.clientID);
        experiment.setDistributedConnectionHandler(this.handler);
        return experiment;
    }

    /**
     * Notifies the server of the finished state of the client.
     * <p>
     * @throws RemoteException
     */
    public void cleanup()
            throws RemoteException
    {
        DistributedClientStatus info = new DistributedClientStatus(DistributedClientStatus.EClientStatus.ClientFinished);
        this.handler.notifyStatus(this.clientID, info);
    }

    /**
     * Runs the distributed main experiment.
     * <p>
     * @param distributedExperiment Experiment to run.
     * <p>
     * @throws RemoteException
     * @throws Exception
     */
    public void runExperiment(DistributedMainExperiment distributedExperiment)
            throws RemoteException, Exception
    {

        distributedExperiment.initializeMainExperiment();
        distributedExperiment.runMainExperiment();
        distributedExperiment.cleanupMainExperiment();

    }
}
