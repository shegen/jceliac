/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment;

import jceliac.JCeliacGenericException;

/**
 * The basic interface for multi-threaded experiments.
 * <p>
 * The only specified method is used to return Features extracted by the feature
 * extraction method (which is run in multiple threads).
 * <p>
 *
 * @author shegen
 */
public interface IThreadedExperiment
{

    /**
     * This is called by the feature extraction threads when they finish.
     * <p>
     * @param f The set of features extracted by the specific feature extraction
     *          method called by the experiment. This features are then stored
     *          in a global structure within the experiment object.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public void threadFinish(Object f) throws JCeliacGenericException;
}
