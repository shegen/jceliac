/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment;

import jceliac.experiment.distributed.DistributedMainExperiment;
import java.io.Serializable;

/**
 * This class is used to cleanup an experiment after it finishes, aborts or was
 * aborted using a user interrupt.
 * <p>
 * This type of object is registered as shutdownHook within a MainExperiment to
 * handle cleanup. This is important especially for OnlineExperiments to
 * indicate the correct experiment state within the database.
 * <p>
 * @author shegen
 */
public class ExperimentCleanupThread
        extends Thread
        implements Serializable
{

    private MainExperiment experiment;
    private DistributedMainExperiment distributedExperiment;

    public ExperimentCleanupThread(MainExperiment exp)
    {
        this.experiment = exp;
    }

    public ExperimentCleanupThread(DistributedMainExperiment exp)
    {
        this.distributedExperiment = exp;
    }

    @Override
    public void run()
    {
        try {
            if(this.experiment != null) {
                this.experiment.cleanupMainExperiment();
            }
            if(this.distributedExperiment != null) {
                this.distributedExperiment.cleanupMainExperiment();
            }
        } catch(Exception e) {
        }
    }

}
