/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment;

import jceliac.*;

/**
 * Exception thrown when initialization of an Experiment fails.
 * <p>
 * @author shegen
 */
public class InitializeFailedException
        extends JCeliacLogableException
{

    public InitializeFailedException(String message)
    {
        super(message);
    }

    public InitializeFailedException()
    {
    }

    @Override
    public String getLocalizedMessage()
    {
        return "Experiment Initialization: " + super.getLocalizedMessage();
    }

}
