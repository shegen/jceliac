/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment;

import java.util.ArrayList;

/**
 * This holds the configuration parameters shared by the entire set of
 * experiments defined in a single .xml configuration file. This mainly includes
 * parameters for the distributed experiment type.
 * <p>
 * @author shegen
 */
public class GlobalTestConfigurationParameters
        extends AbstractParameters
        implements Cloneable
{

    // indicates wether the code will automatically distribute experiments across several clients.
    protected boolean distributeExperiments = false;

    // the client hostnames to distribute the experiments to, expected format is:
    // hostname1, hostname2, hostname3
    protected String clientHostnames = null;

    // hostname of rmi server
    protected String serverHostname;

    // the jarfile to execute at the clients
    protected String JARFilename = null;

    protected String SSHUser = "shegen";

    protected String SSHPassword = null;

    public boolean isDistributeExperiments()
    {
        return this.distributeExperiments;
    }

    public void setDistributeExperiments(boolean distributedExperiments)
    {
        this.distributeExperiments = distributedExperiments;
    }

    public String getJARFilename()
    {
        return JARFilename;
    }

    public void setJARFilename(String JARFilename)
    {
        this.JARFilename = JARFilename;
    }

    /**
     * Parses and returns the hostnames of clients as supplied by the XML
     * configuration file.
     * <p>
     * @return A list of hostnames as strings.
     */
    public ArrayList<String> getClientHostnames()
    {
        ArrayList<String> hostnames = new ArrayList<>();
        String[] tmp = this.clientHostnames.split(",");

        for(String t : tmp) {
            hostnames.add(t.replaceAll(" ", ""));
        }
        return hostnames;
    }

    public void setClientHostnames(String clientHostnames)
    {
        this.clientHostnames = clientHostnames;
    }

    public String getServerHostname()
    {
        return serverHostname;
    }

    public void setServerHostname(String serverHostname)
    {
        this.serverHostname = serverHostname;
    }

    public String getSSHUser()
    {
        return SSHUser;
    }

    public void setSSHUser(String SSHUser)
    {
        this.SSHUser = SSHUser;
    }

    public String getSSHPassword()
    {
        return SSHPassword;
    }

    public void setSSHPassword(String SSHPassword)
    {
        this.SSHPassword = SSHPassword;
    }

}
