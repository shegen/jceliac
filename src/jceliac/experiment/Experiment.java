/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment;

import jceliac.JCeliacGenericException;
import jceliac.classification.*;
import java.io.Serializable;
import jceliac.logging.*;
import jceliac.validation.generic.ValidationMethod;

/**
 * The base class for all types of experiments being conducted.
 * <p>
 * An experiment consists of multiple steps, including signal acquisition
 * feature extraction, classification (cross-validation). An experiment either
 * fails or succeeds, this is indicated by ExperimentState. Experiments are
 * usually run threaded (runExperimentThreaded), the single threaded version
 * (runExperiment) is obsolete and was only used for debugging purposes.
 * <p>
 * @author shegen
 */
public abstract class Experiment
        implements Serializable
{

    protected ExperimentParameters parameters;
    protected ExperimentState state = ExperimentState.FAILED;
    protected int subExperimentIdentifier = -1;
    
    /**
     * Defines the state of an experiment.
     */
    public static enum ExperimentState
    {

        FAILED,
        SUCCESS,
    }

    public static enum ExperimentType
    {
        OFFLINE ,
        PERFORMANCE,
        HIST_INTERSECT;
        
        public static Experiment.ExperimentType valueOfIgnoreCase(String stringValue)
                throws JCeliacGenericException
        {
            for(Experiment.ExperimentType value : Experiment.ExperimentType.values()) {
                if(value.name().equalsIgnoreCase(stringValue)) {
                    return value;
                }
            }
            throw new JCeliacGenericException("Unknown Experiment Type: " + stringValue);
        }
    }
    
    // Experiment Logic Code
    public Experiment(ExperimentParameters param)
            throws JCeliacGenericException
    {
        this.parameters = param;
        param.prepareExperiment();

    }

    /**
     * Acquires data and prepares methods.
     * <p>
     * @throws InitializeFailedException
     * @throws JCeliacGenericException
     */
    public abstract void initializeExperiment()
            throws InitializeFailedException, JCeliacGenericException;

    /**
     * Performs the entire experiment, feature extraction, optimization and
     * classification.
     * <p>
     * @return
     */
    public abstract IteratedClassificationResult runExperiment();

    /**
     * Performs the entire experiment in a multi-threaded fashion, feature
     * extraction, optimization and classification.
     * <p>
     * @return
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public abstract IteratedClassificationResult runExperimentThreaded()
            throws JCeliacGenericException;

    /**
     * Cleans up the experiment and writes statistics.
     * <p>
     * @param state
     */
    public abstract void cleanupExperiment(ExperimentState state);

    public ExperimentState getExperimentState()
    {
        return state;
    }

    /**
     * The loggers of an experiment, those are usable once the experiment is
     * initialized . The type of loggers is dependent on the type of
     * experiments, offline experiments use a console logger as well as a file
     * logger.
     */
    protected static JCeliacLogger[] logger;

    /**
     * Logs a message.
     * <p>
     * @param level  Depending on the current log level some messages are
     *               suppressed.
     * @param format Format string for log message.
     * @param args   Arguments for log message.
     */
    public static void log(int level, String format, Object... args)
    {
        if(Experiment.logger != null) {
            for(JCeliacLogger curLogger : Experiment.logger) {
                if(curLogger != null) {
                    curLogger.logMessage(format, level, args);
                }
            }
        }else {
            System.out.printf(format, args);
            System.out.println();
        }
    }

    /**
     * Logs an exception's stack strace for debugging.
     * <p>
     * @param level Depending on the current log level some messages are
     *              suppressed.
     * @param ex    Exception to log, the stack trace will be logged for
     *              debugging.
     */
    public static void logException(int level, Exception ex)
    {
        if(Experiment.logger != null) {
            for(JCeliacLogger curLogger : Experiment.logger) {
                if(curLogger != null) {
                    curLogger.logException(ex, level);
                }
            }
        }
    }

    public String getExperimentName()
    {
        if(this.subExperimentIdentifier != -1) {
            return parameters.getExperimentName() + "-" + this.subExperimentIdentifier;
        } else {
            return parameters.getExperimentName();
        }
    }
    
    public String getClassIdentifier(int num)
    {
        return parameters.getClassIdentifier(num);
    }

    public static JCeliacLogger[] getLogger()
    {
        return logger;
    }

    public static void setLogger(JCeliacLogger[] logger)
    {
        Experiment.logger = logger;
    }

    public ExperimentParameters getParameters()
    {
        return parameters;
    }

    public void setParameters(ExperimentParameters parameters)
    {
        this.parameters = parameters;
    }

    public int getSubExperimentIdentifier()
    {
        return subExperimentIdentifier;
    }

    public void setSubExperimentIdentifier(int subExperimentIdentifier)
    {
        this.subExperimentIdentifier = subExperimentIdentifier;
    }

    
}
