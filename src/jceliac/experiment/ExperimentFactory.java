/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment;


import jceliac.featureOptimization.FeatureOptimizationParameters;
import jceliac.experiment.distributed.DistributedMainExperiment;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.classification.classificationParameters.ClassificationParameters.ECrossValidationType;
import jceliac.experiment.distributed.DistributedOfflineExperiment;
import jceliac.classification.classificationParameters.*;
import jceliac.tools.parameter.parser.*;
import jceliac.logging.JCeliacLogger;
import jceliac.featureOptimization.*;
import jceliac.tools.parameter.xml.*;
import java.util.*;
import jceliac.*;
import jceliac.experiment.custom.HistIntersectExperiment;
import jceliac.experiment.custom.PerformanceExperiment;
import jceliac.validation.generic.ValidationMethod;
import jceliac.validation.generic.ValidationMethodParameters;


/**
 * Creates an Experiment type object based on input parameters.
 *
 * This class initializes and creates a MainExperiment with all SubExperiments
 * based on input parameters or XML configuration file.
 *
 * XXX: The entire CMD line code should be redone and improved. 
 * 
 * @author shegen
 */
public class ExperimentFactory {

    private static ArrayList<ExperimentParameters> allExperimentParameters;
    private static CommandLineParameters commandLineParameters;
    private static HashMap<String, Integer> mainExperiments;

    /**
     * Parses the user supplied command line arguments and if provided an XML configuration file.
     * This has to be called prior to creating subExperiments. Based on the parsed information the
     * corresponding parameter objects are created later. 
     * 
     * @param args Command line parameters. 
     * @return Returns parameters visible by all subExperiments (global parameters).
     * @throws XMLParseException
     * @throws InvalidInputException
     * @throws JCeliacGenericException 
     */
    public static GlobalTestConfigurationParameters parseExperimentConfigurations(String[] args)
            throws XMLParseException, InvalidInputException, JCeliacGenericException {

        CommandLineParameters cmdParameters;
        ExperimentFactory.mainExperiments = new HashMap<>();

        cmdParameters = parseInput(args);
        ExperimentFactory.commandLineParameters = cmdParameters;
        ExperimentFactory.allExperimentParameters = new ArrayList<>();

        // XML config is available
        if (cmdParameters.configXMLFile != null) {
            XMLParser.parse(cmdParameters.configXMLFile, cmdParameters.featureExtractionMethod, 
                            cmdParameters.classificationMethod, cmdParameters.optimizationMethod,
                            cmdParameters.generativeModel, cmdParameters.validationMethod);
            int count = XMLParser.getExperimentCount();

            GlobalTestConfigurationParameters globalParameters = XMLParser.getGlobalTestConfigurationParameters();
            if (cmdParameters.SSHPassword != null) {
                globalParameters.SSHPassword = cmdParameters.SSHPassword;
            }
            for (int num = 0; num < count; num++) {
                ExperimentParameters param = XMLParser.getExperimentParameters(num);
                if (ExperimentFactory.mainExperiments.containsKey(param.getExperimentName()) == false) {
                    ExperimentFactory.mainExperiments.put(param.getExperimentName(), num);
                }
                param.setCommandLineParameters(cmdParameters);
                createDefaultParameterObjects(param); // create default objects if needed
                createCMDParameterObjects(param);
                ExperimentFactory.allExperimentParameters.add(param);
                validateParameters(param);
                // check if SSH password was set on cmd line
                if (globalParameters.isDistributeExperiments()) {
                    if (globalParameters.getSSHPassword() == null) {
                        throw new InvalidInputException("SSH Password not supplied, use --password option.");
                    }
                }
            }
            Main.log(JCeliacLogger.LOG_INFO, "Queueing %d Main-Experiments with a total of %d Experiments!", XMLParser.getMainExperimentCount(), XMLParser.getExperimentCount());
            return globalParameters;
        } else {
            ExperimentParameters param = new ExperimentParameters();

            param.setCommandLineParameters(cmdParameters);
            createDefaultParameterObjects(param); // create default objects if needed
            createCMDParameterObjects(param);
            ExperimentFactory.allExperimentParameters.add(param);
            validateParameters(param);

            Main.log(JCeliacLogger.LOG_INFO, "Queueing 1 Main-Experiments with a total of 1 Experiments!");
            // return default global parameters
            return new GlobalTestConfigurationParameters();
        }
    }

    /**
     * 
     * @return A HashMap of main subExperiments with subExperiment name as key and number of sub-subExperiments as value. 
     */
    public static HashMap<String, Integer> getMainExperiments() {
        return ExperimentFactory.mainExperiments;
    }

    /**
     * This creates defaultcommand line  parameters objects if no config file was used
     * command line arguments always overrule the configs in the XML files.
     * 
     * @param param Experiment parameters object to fill.  
     * @throws JCeliacGenericException 
     */
    private static void createCMDParameterObjects(ExperimentParameters param)
            throws JCeliacGenericException {
        ParameterParser parser = new StringParameterParser();

        if (param.featureExtractionParamString != null && param.featureExtractionMethodName != null) {
            Main.log(JCeliacLogger.LOG_INFO, "CMD Overruling Feature Extraction Parameters!");
            FeatureExtractionParameters feParams = parser.parseFeatureExtractionParameters(param.featureExtractionParamString,
                    FeatureExtractionMethodFactory.mapFeatureExtractionMethodName(param.featureExtractionMethodName));
            param.featureExtractionParameters = feParams;

        }
        if (param.crossValidationMethodName != null) {
            param.classificationMethodParameters.setCrossValidationType(param.crossValidationMethodName);
        }

    }

     /**
     * This creates default parameters objects if no config file was used
     * command line arguments always overrule the configs in the XML files.
     * 
     * @param param Experiment parameters object to fill.  
     * @throws JCeliacGenericException 
     */
    private static void createDefaultParameterObjects(ExperimentParameters param)
            throws JCeliacGenericException {
        ParameterParser parser = new StringParameterParser();
        if (param.featureExtractionParameters == null && param.featureExtractionMethodName != null) {
            FeatureExtractionParameters feParams = parser.parseFeatureExtractionParameters(null,
                    FeatureExtractionMethodFactory.mapFeatureExtractionMethodName(param.featureExtractionMethodName));
            param.featureExtractionParameters = feParams;
        }
        if (param.classificationMethodParameters == null && param.classificationMethodName != null) {
            ClassificationParameters cParams = parser.parseClassificationMethodParameters(null,
                    ClassificationMethodFactory.mapClassificationMethodName(param.classificationMethodName));
            param.classificationMethodParameters = cParams;
        }
        if (param.optimizationMethodParameters == null && param.optimizationMethodName != null) {
            FeatureOptimizationParameters oParams = parser.parseOptimizationMethodParameters(null,
                    FeatureOptimizationMethodFactory.mapFeatureOptimizationMethodName(param.optimizationMethodName));
            param.optimizationMethodParameters = oParams;
        }
    }

    /**
     * This validates the parameters of an subExperiment.
     * Each subExperiment must have a testname, a training image set,
     * a feature extraction method, a classification method and an optimization method.
     * 
     * @param params Initialized subExperiment parameters. 
     * @throws InitializeFailedException 
     */
    private static void validateParameters(ExperimentParameters params)
            throws InitializeFailedException, JCeliacGenericException 
    {
        if (params.featureExtractionParameters == null || params.featureExtractionMethodName == null) {
            throw new InitializeFailedException("Missing Feature Extraction Method!");
        }
        if (params.classificationMethodParameters == null || params.classificationMethodName == null) {
            throw new InitializeFailedException("Missing Classification Method!");
        }
        if (params.optimizationMethodName == null || params.optimizationMethodParameters == null) {
            throw new InitializeFailedException("Missing Optimization Method (use NULL method)!");
        }
        if (params.experimentName == null) {
            throw new InitializeFailedException("Missing Experiment Name!");
        }
        if (params.trainingDataBaseDirectory == null) {
            throw new InitializeFailedException("Missing Data Directory!");
        }
        if (params.validationParameters == null ) {
            throw new InitializeFailedException("Missing Validation Type!");
        }
        if (params.testDataBaseDirectory != null 
                && (params.validationParameters.getCrossValidationType() == ValidationMethodParameters.ECrossValidationType.DISTINCT)) 
        {
            throw new InitializeFailedException("CV with separate Test Directory only makes sense for DISTINCT!");
        }
        if (params.testDataBaseDirectory == null && 
            ValidationMethod.EValidationTypes.valueOfIgnoreCase(params.validationMethodName) 
            == ValidationMethod.EValidationTypes.DistinctValidation) 
        {
            throw new InitializeFailedException("DISTINCT with no Test Directory does not make sense!");
        }
        if (params.allowForeignEvaluationClasses == true && params.offline == false) {
            throw new InitializeFailedException("Foreign classes are only allowed in offline experiments!");
        }
        params.classificationMethodParameters.validateParameters();

    }

    public static int getExperimentCount() {
        return allExperimentParameters.size();
    }

    /**
     * Creates the subExperiments based on the parsed information of user supplied arguments.
     * @param globalParams Global parameters for all subExperiments as parsed earlier. 
     * @return A list of Main Experiments
     * @throws jceliac.JCeliacGenericException
     */
    public static ArrayList<MainExperiment> createExperiments(GlobalTestConfigurationParameters globalParams)
            throws JCeliacGenericException {
        ArrayList<MainExperiment> allExperiments = new ArrayList<>();
        Experiment subExperiment;
        MainExperiment mainExperiment = null;
        for (int i = 0; i < ExperimentFactory.allExperimentParameters.size(); i++) {
            ExperimentParameters params = ExperimentFactory.allExperimentParameters.get(i);
            if (ExperimentFactory.mainExperiments.containsValue(i) ||
                ExperimentFactory.mainExperiments.isEmpty()) 
            {
                if (mainExperiment != null) {
                    allExperiments.add(mainExperiment);
                }
                if (globalParams.isDistributeExperiments() == true) {
                    mainExperiment = new DistributedMainExperiment(params.getExperimentName());
                } else {
                    mainExperiment = new MainExperiment(params.getExperimentName());
                }
            }
            if(mainExperiment == null) {
                throw new JCeliacGenericException("No main experiment was created");
            }
            if (globalParams.isDistributeExperiments()) {
                subExperiment = new DistributedOfflineExperiment(params);
            } else if (params.isVideoSequenceExperiment()) {
                subExperiment = new OfflineVideoSequenceExperiment(params);
            }else {
                switch(params.getExperimentType()) {
                    case OFFLINE:
                        subExperiment = new OfflineExperiment(params);
                        break;
                    case PERFORMANCE:
                        subExperiment = new PerformanceExperiment(params); 
                        break;
                    case HIST_INTERSECT:
                        subExperiment = new HistIntersectExperiment(params); 
                        break;
                    default:
                        throw new JCeliacGenericException("Unsupported experiment type!");   
                }
            }
            
            mainExperiment.addSubExperiment(subExperiment);
        }
        // add last subExperiment
        allExperiments.add(mainExperiment);
        return allExperiments;
    }

    /**
     * Creates a single subExperiment.
     * 
     * @param experimentIndex Index into subExperiment parameter list. 
     * @return
     * @throws JCeliacGenericException 
     */
    public static Experiment createExperiment(int experimentIndex)
            throws JCeliacGenericException {
        Experiment experiment;
        ExperimentParameters params = ExperimentFactory.allExperimentParameters.get(experimentIndex);

        switch(params.getExperimentType()) {
            case OFFLINE:
                experiment = new OfflineExperiment(params);
                break;
            case PERFORMANCE:
                experiment = new PerformanceExperiment(params); 
                break;
            case HIST_INTERSECT:
                experiment = new HistIntersectExperiment(params); 
                break;
            default:
                throw new JCeliacGenericException("Unsupported experiment type!");   
        }
        return experiment;
    }

    /**
     * The actual command line input parser.
     * @params args Command line arguments.
     * @throws InvalidInputException On illegal or missing input.
     */
    private static CommandLineParameters parseInput(String[] args)
            throws InvalidInputException {

        CommandLineParameters cmdParameters = new CommandLineParameters();
        if (args.length == 0) {
            printHelp();
            System.exit(0);
        }
        for (int i = 0; i < args.length; i++) {

            // parameters without argument
            if (args[i].equals("-h") || args[i].equals("--help")) {
                printHelp();
                System.exit(0);
            }
            if (args[i].equals("-I") || args[i].equals("--interactive")) {
                cmdParameters.interactive = true;
                continue;
            }
            if (args[i].equals("-off") || args[i].equals("--offline")) {
                cmdParameters.offline = true;
                continue;
            }
            if (args[i].equals("-on") || args[i].equals("--online")) {
                cmdParameters.offline = false;
                continue;
            }
            if (args[i].equals("--allow-foreign-evaluation-classes")) {
                cmdParameters.allowForeignEvaluationClasses = true;
                continue;
            }

            // parameters with argument
            try {
                if (args[i].equals("-p") || args[i].equals("--feature-extraction-params")) {
                    cmdParameters.featureExtractionParamString = args[i + 1];
                    i++;
                    continue;
                }
                if (args[i].equals("-x") || args[i].equals("--config-xml")) {
                    cmdParameters.configXMLFile = args[i + 1];
                    i++;
                    continue;
                }
                if (args[i].equals("-f") || args[i].equals("--feature-extraction-method")) {
                    cmdParameters.featureExtractionMethod = args[i + 1];
                    i++;
                    continue;
                }
                if (args[i].equals("-c") || args[i].equals("--classification-method")) {
                    cmdParameters.classificationMethod = args[i + 1];
                    i++;
                    continue;
                }
                if (args[i].equals("-m") || args[i].equals("--generative-model")) {
                    cmdParameters.generativeModel = args[i + 1];
                    i++;
                    continue;
                }
                if (args[i].equals("-n") || args[i].equals("--experiment-name")) {
                    cmdParameters.experimentName = args[i + 1];
                    i++;
                    continue;
                }

                if (args[i].equals("-d") || args[i].equals("--training-data-base-dir")) {
                    cmdParameters.trainingDataBaseDirectory = args[i + 1];
                    i++;
                    continue;
                }
                if (args[i].equals("-D") || args[i].equals("--test-data-base-dir")) {
                    cmdParameters.testDataBaseDirectory = args[i + 1];
                    i++;
                    continue;
                }
                if (args[i].equals("-v") || args[i].equals("--validation-method")) {
                    cmdParameters.validationMethod = args[i + 1];
                    i++;
                    continue;
                }
                if (args[i].equals("-l") || args[i].equals("--classification-params")) {
                    cmdParameters.classificationParamString = args[i + 1];
                    i++;
                    continue;
                }
                if (args[i].equals("-S") || args[i].equals("--Subset")) {
                    cmdParameters.featureSubset = args[i + 1];
                    i++;
                    continue;
                }
                if (args[i].equals("-O") || args[i].equals("--optimization-method")) {
                    cmdParameters.optimizationMethod = args[i + 1];
                    i++;
                    continue;
                }
                if (args[i].equals("--export-distance-matrix")) {
                    cmdParameters.exportDistanceMatrixFile = args[i + 1];
                    i++;
                    continue;
                }
                if (args[i].equals("--password")) {
                    cmdParameters.SSHPassword = args[i + 1];
                    i++;
                    continue;
                }

            } catch (IndexOutOfBoundsException e) {
                throw new InvalidInputException("Missing Parameter for: " + args[i]);
            }
            throw new InvalidInputException("Unknown Parameter: " + args[i]);

        }
        return cmdParameters;
    }

    // This should not be logged, so use printlns instead of log 
    private static void printHelp() {
        System.out.println("usage: " + "JCeliac" + " [OPTIONS]");
        System.out.println("Options are: ");
        System.out.println("  -h, --help                                 Prints this information.");
        System.out.println("  -p, --feature-extraction-params=STRING     Parameters for feature\n"
                + "                                             extraction in a string.  ");
        System.out.println("  -l, --classification-params=STRING         Parameters for classification\n"
                + "                                             Method.");
        System.out.println("  -x, --config-xml=FILE                      Parameters for the experiment are\n"
                + "                                             in an xml file.  ");
        System.out.println("  -f, --feature-extraction-method=METHOD     Specifies the feature extraction\n"
                + "                                             Method to use.");
        System.out.println("  -c, --classification-method=METHOD         Specifies the classification\n"
                + "                                             Method to use.");
        System.out.println("  -m, --generative-model=METHOD         Specifies the generative \n"
                + "                                             Model to use.");
        System.out.println("  -n, --experiment-name                      A unique name for the experiment.\n");
        System.out.println("  -off, --offline                            Do not use the database.\n");
        System.out.println("  -on,  --online                             Use DB to store results.\n");
        System.out.println("  -d, --training-data-base-dir=DIR           The training directory.\n");
        System.out.println("  -D, --test-data-base-dir=DIR               The test directory.\n");
        System.out.println("  -O, --optimization-method=METHOD           The Feature Optimization Method.\n");
        System.out.println("  -v, --validation-method=METHOD      The validation Method.\n");
        System.out.println("  --allow-foreign-evaluation-classes         Allow classes in the evalution set\n"
                + "                                             which are not in the training set.\n"
                + "                                             No classification rate is returned!\n");
        System.out.println("  --export-distance-matrix=FILE              Export a matrix of all distances\n"
                + "                                             between features to FILE.\n");
        System.out.println("  --password=SSH PASSWORD                    SSH Password used to spawn clients.\n");

    }

}
