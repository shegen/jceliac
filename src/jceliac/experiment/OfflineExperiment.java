/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment;

import jceliac.experiment.distributed.IDistributedExperimentConnectionHandler;
import java.util.concurrent.ExecutorCompletionService;
import jceliac.tools.transforms.wavelet.WaveletTransform.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import jceliac.tools.significance.McNemar;
import jceliac.JCeliacGenericException;
import java.util.concurrent.Executors;
import jceliac.featureOptimization.*;
import java.util.concurrent.Future;
import jceliac.featureExtraction.*;
import java.io.ObjectInputStream;
import jceliac.classification.*;
import java.io.FileInputStream;
import jceliac.tools.export.*;
import jceliac.tools.timer.*;
import jceliac.tools.data.*;
import jceliac.logging.*;
import java.io.File;
import java.util.*;
import jceliac.classification.model.GenerativeModel;
import jceliac.classification.model.GenerativeModelFactory;
import jceliac.classification.ClassificationMethod;
import jceliac.validation.generic.ValidationMethod;
import jceliac.validation.generic.ValidationMethodFactory;


/**
 * Subtype of Experiment without database support.
 * <p>
 * This type of experiment can be run offline. File logs are used to store
 * classification results. Single ClassificationOutcomes are not stored.
 * <p>
 * @author shegen
 */
public class OfflineExperiment
        extends Experiment
        implements IThreadedExperiment
{

    protected FeatureExtractionMethod extractionMethod;
    
    protected ClassificationMethod experimentalClassificationMethod;
    protected FeatureOptimizationMethod experimentalFeatureOptimizationMethod;

    protected ValidationMethod validationMethod;
    protected GenerativeModel model;

    protected ArrayList<DataReader> trainDataReaders; // one reader for each class
    protected ArrayList<DataReader> testDataReaders; // one reader for each class

    protected ArrayList<DiscriminativeFeatures> currentCollection;

    protected ArrayList<DiscriminativeFeatures> trainingFeatures;
    protected ArrayList<DiscriminativeFeatures> evaluationFeatures;

    public static int NUMBER_OF_THREADS = 4;

    protected int numberOfImages = 0;
    protected int finishedImages = 0;

    protected IDistributedExperimentConnectionHandler distributionHandler;

    public OfflineExperiment(ExperimentParameters param)
            throws JCeliacGenericException
    {
        super(param);  
    }

    @Override
    public void cleanupExperiment(ExperimentState state)
    {
        this.trainDataReaders = null;
        this.testDataReaders = null;
        this.trainingFeatures = null;
        this.evaluationFeatures = null;
        this.extractionMethod = null;
        this.currentCollection = null;
    }

    @Override
    public IteratedClassificationResult runExperiment()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Validates if the directory readres have the same number of classes and
     * also that the classes have the same index for training and evaluation
     * features.
     * <p>
     * @return True if everything is sane, false if there is a problem with the
     *         directory readers.
     */
    protected boolean compareReaders()
    {
        if(this.parameters.getTestClassBaseDirectories().isEmpty()) { // if no test data is there this is ok
            return true;
        }
        if(this.parameters.getTestClassBaseDirectories().size()
           != this.parameters.getTrainingClassBaseDirectories().size()) { // not the same number of classes
            return false;
        }
        // check if the classes have the same "alignment"
        for(int i = 0; i < this.parameters.getTrainingClassBaseDirectories().size(); i++) {
            String train = this.parameters.getTrainingBaseDirectoryForClass(i).getName();
            String test = this.parameters.getTestBaseDirectoryForClass(i).getName();
            if(train.equals(test) == false) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void initializeExperiment()
            throws InitializeFailedException, JCeliacGenericException
    {
        // this must not be initialized in the constructor; the logger is static
        // and will therefore be overwritten by each experiment initialization!
        // if it's done in the constructor we will end up with the logger of the last
        // experiment created by the MainExperiment!
        Experiment.logger = new JCeliacLogger[2];
        Experiment.logger[0] = new JCeliacConsoleLogger();
        if(this.subExperimentIdentifier == -1) {
            Experiment.logger[1] = new JCeliacFileLogger(this.parameters.getExperimentName());
        }else {
            Experiment.logger[1] = new JCeliacFileLogger(this.parameters.getExperimentName()+"-"+this.subExperimentIdentifier);
        }

        try {

            this.trainDataReaders = new ArrayList<>();
            this.testDataReaders = new ArrayList<>();

            for(int i = 0; i < this.parameters.getNumberOfTrainingClasses(); i++) {
                CachedDataReader tmp = new CachedDataReader(this.parameters.getTrainingBaseDirectoryForClass(i), false);
                trainDataReaders.add(tmp);
                this.numberOfImages += tmp.getNumberOfFiles();
            }
            for(int i = 0; i < this.parameters.getNumberOfTestClasses(); i++) {
                CachedDataReader tmp = new CachedDataReader(this.parameters.getTestBaseDirectoryForClass(i), false);
                this.testDataReaders.add(tmp);
                this.numberOfImages += tmp.getNumberOfFiles();
            }

            this.extractionMethod = FeatureExtractionMethodFactory.createFeatureExtractionMethod(
                    this.parameters.getFeatureExtractionMethodName(),
                    this.parameters.featureExtractionParameters);
            this.extractionMethod.logAnnotation();
            
            this.extractionMethod.initialize();

            this.model = GenerativeModelFactory.createGenerativeModel(this.parameters.getGenerativeModelName(),
                                                                      this.parameters.generativeModelParameters);
            
            this.experimentalClassificationMethod = ClassificationMethodFactory.
                                                    createExperimentalClassificationMethod(
                                                           this.parameters.getClassificationMethodName(),
                                                           this.parameters.classificationMethodParameters,
                                                           this.parameters.getNumberOfTotalClasses());
            
            
            this.experimentalFeatureOptimizationMethod = FeatureOptimizationMethodFactory.
                                                            createExperimentalFeatureOptimizationMethod(
                                                                this.parameters.getOptimizationMethodName(),
                                                                this.parameters.optimizationMethodParameters);
            
            this.validationMethod = ValidationMethodFactory.createValidationMethod(
                                this.parameters.getValidationMethodName(), this.parameters.validationParameters,
                                this.experimentalClassificationMethod, this.model, this.experimentalFeatureOptimizationMethod);
           

            this.extractionMethod.estimateParameters(this.trainDataReaders);
        } catch(InvalidInputException e) {
            throw new InitializeFailedException();
        } catch(Exception e) {
            throw new JCeliacGenericException(e);
        }
    }

    /**
     * Visualizes the progress of feature extraction.
     */
    private synchronized void visualizeProgress()
    {
        double percentage = ((double) this.finishedImages * 100.0d) / (double) this.numberOfImages;
        String progressString = "";
        for(int i = 0; i < 10; i++) {
            if(i >= percentage / 10.0d) {
                progressString += " ";
            } else {
                progressString += "#";
            }
        }
        if(this.finishedImages > 1) {
            System.out.print("\33[1A\33[2K");
        }
        System.out.format("Current Progress: [%s]  %.2f\n", progressString, percentage);
        System.out.flush();
    }

    /**
     * Logs the progress ofan experiment (feature computation).
     */
    private synchronized void logProgress()
    {
        double percentage = ((double) this.finishedImages * 100.0d) / (double) this.numberOfImages;
        Experiment.log(JCeliacLogger.LOG_INFO, "Current Progress:  %.2f\n", percentage);
    }

    /**
     * Called by feature extraction threads to notify the main threads of the
     * finish of a single thread.
     * <p>
     * @param f Feature computed by the extraction thread.
     * <p>
     * @throws JCeliacGenericException
     */
    @Override
    public synchronized void threadFinish(Object f)
            throws JCeliacGenericException
    {
        this.finishedImages++;
        this.visualizeProgress();
        ((DiscriminativeFeatures) f).setClassIdentifier(this.getClassIdentifier(((DiscriminativeFeatures) f).getClassNumber()));
    }

    protected int getNumberOfFiles(ArrayList<DataReader> readers)
    {
        int numberOfFiles = 0;
        for(DataReader reader : readers) {
            numberOfFiles += ((CachedDataReader) reader).getNumberOfFiles();
        }
        return numberOfFiles;
    }

    /**
     * Extracts the features by scheduling threads that actually perform the
     * feature extraction.
     * <p>
     * @param readers
     * @param numberOfClasses
     * @param isTraining
     * <p>
     * @return
     * <p>
     * @throws JCeliacGenericException
     */
    @SuppressWarnings({"unchecked", "ThrowFromFinallyBlock"})
    protected ArrayList<DiscriminativeFeatures>
            extractFeatures(ArrayList<DataReader> readers,
                            int numberOfClasses, boolean isTraining)
            throws JCeliacGenericException
    {
        ArrayList<DiscriminativeFeatures> extractedFeatures = new ArrayList<>();
        int procs = Runtime.getRuntime().availableProcessors();
        // this.currentCollection = featureCollection;
        int sampleCount = 0;
        ExecutorService threadExecutor = Executors.newScheduledThreadPool(procs);
        ExecutorCompletionService<DiscriminativeFeatures> completionService = new ExecutorCompletionService<>(threadExecutor);
        ArrayList<Future<DiscriminativeFeatures>> futures = new ArrayList<>();
        JCeliacGenericException caughtException = null;

        try {
            for(int classIndex = 0; classIndex < numberOfClasses; classIndex++) {
                boolean isOneClassTrainingClass = false;
                DataReader currentReader = readers.get(classIndex);

                while(currentReader.hasNext()) {
                    DataSource currentFile = (DataSource) currentReader.next();
                    ContentStream cstream = currentFile.createContentStream();
                    while(cstream.hasNext()) {
                        Frame content = cstream.next();

                        futures.add(
                                completionService.submit(
                                        this.extractionMethod.createFeatureExtractionThread(
                                                this, content, currentReader.getClassNumber(),
                                                isOneClassTrainingClass, isTraining)));
                        sampleCount++;
                    }
                }
            }
            for(Future<DiscriminativeFeatures> tmp : futures) {
                try {
                    Future<DiscriminativeFeatures> computedFuture = completionService.take();
                    DiscriminativeFeatures computedFeatures = computedFuture.get();
                    extractedFeatures.add(computedFeatures);
                    this.threadFinish(computedFeatures);
                } catch(InterruptedException | ExecutionException e) {
                    StackTraceElement[] els = e.getStackTrace();
                    caughtException = new JCeliacGenericException(e);
                    e.printStackTrace();
                    break;
                }
            }
        } catch(JCeliacGenericException e) {
            throw e;
        } finally {
            for(Future<DiscriminativeFeatures> future : futures) {
                future.cancel(true);
            }
            threadExecutor.shutdownNow();
            if(caughtException != null) {
                throw caughtException;
            }
        }
        if(extractedFeatures.size() > 0) {
            // sort the features, due to using threading we do not know i what order
            // features are extracted, in case of kfold-validation we need indices however
            // which are generated randomly; to avoid problems we sort the features
            // here
            Collections.sort(extractedFeatures);
            // normalize the features based on other features if needed by the method
            return this.extractionMethod.normalizeFeatures(extractedFeatures);
        }
        return extractedFeatures;
    }

    @Override
    public IteratedClassificationResult runExperimentThreaded()
            throws JCeliacGenericException
    {

        int procs = Runtime.getRuntime().availableProcessors();

        this.trainingFeatures = new ArrayList<>();
        this.evaluationFeatures = new ArrayList<>();
        Experiment.log(JCeliacLogger.LOG_INFO, "Extracting Features using %d Threads!", procs);
        this.extractionMethod.report();

        PerformanceTimer timer = new PerformanceTimer();
        timer.startTimer();

        this.trainingFeatures = this.extractFeatures(this.trainDataReaders, this.parameters.getNumberOfTrainingClasses(), true);
        this.evaluationFeatures = this.extractFeatures(this.testDataReaders, this.parameters.getNumberOfTestClasses(), false);
        
        // performanceAnalyzer = new PerformanceAnalyzer(this.evaluationFeatures);
        //performanceAnalyzer.analyze();
        
        
        this.extractionMethod.cleanup();

        timer.stopTimer();
        jceliac.tools.memory.MemoryInfos.dumpMemoryInfos();

        Experiment.log(JCeliacLogger.LOG_INFO, "Feature Extraction Finished Successfully.");
        Experiment.log(JCeliacLogger.LOG_INFO, "Features Extracted from %d (Training), %d (Test) Images and %d Classes!", 
                                               this.trainingFeatures.size(), this.evaluationFeatures.size(), this.parameters.getNumberOfTestClasses());
        Experiment.log(JCeliacLogger.LOG_INFO, "Features Extracted in %f Seconds.", timer.getSeconds());

        /* make room for data flush the caches */
        for(int i = 0; i < this.trainDataReaders.size(); i++) {
            this.trainDataReaders.get(i).cleanup();

        }
        for(int i = 0; i < this.testDataReaders.size(); i++) {
            this.testDataReaders.get(i).cleanup();
        }

        // export features
     //   DataExporter.exportScaleResponseFeatures(this.trainingFeatures, "/tmp/scaleResponse-training.csv");
     //   DataExporter.exportScaleResponseFeatures(this.evaluationFeatures, "/tmp/scaleResponse-evaluation.csv");
        
        PerformanceTimer cTimer = new PerformanceTimer();
        cTimer.startTimer();
        IteratedClassificationResult result = 
                this.validationMethod.validate(this.trainingFeatures, this.evaluationFeatures);
        cTimer.stopTimer();        

        if(this.parameters.exportDistanceMatrix()) {
            FeatureExport export = new FeatureExport();
            export.addFeatures(this.trainingFeatures);
            export.addFeatures(this.evaluationFeatures);
            export.exportFeatures(this.parameters.getExportDistanceMatrixFile());
        }

        Experiment.log(JCeliacLogger.LOG_INFO, "Classification done in %f", cTimer.getSeconds());
        if(this.parameters.allowForeignEvaluationClasses == false) {
            Experiment.log(JCeliacLogger.LOG_RESULT, "Classification Performance:");

            // ok it can now happen that we omit a class in the output. this is the case
            // if a training class is existent that is not a test class (only allowed in ONE_CLASS SVM)
            for(int classNum = 0; classNum < this.parameters.getNumberOfClassIdentifiers(); classNum++) {
                String classIdentifier = super.parameters.getClassName(classNum);
                if(result.hasClassIdentifier(classIdentifier)) {
                    Experiment.log(JCeliacLogger.LOG_RESULT, "Class-%d (%s): %.2f", classNum, classIdentifier,
                                   100 * result.getClassificationRate(classIdentifier));
                }
            }
            Experiment.log(JCeliacLogger.LOG_RESULT, "Overall Classification Rate: %.2f", 100 * result.getOverallClassificationRate());
            Experiment.log(JCeliacLogger.LOG_RESULT, "Standard Deviation: %.2f", 100 * result.getStandardDeviation());
            Experiment.log(JCeliacLogger.LOG_RESULT, "Number of Tied Outcomes: %d", result.getTieCount());
            Experiment.log(JCeliacLogger.LOG_RESULT, "Number of Guessed Outcomes: %d", result.getGuessCount());

            // compute ROC
            if(this.experimentalClassificationMethod.supportsROC() &&
               result.getNumberOfResults() == 1) 
            {
                ReceiverOperatorCharacteristic roc = result.computeROC();
                double[] rates = roc.getSingleRates();
                String ratesString = "ROC-Values: [";
                for(int i = 0; i < rates.length; i++) {
                    ratesString += rates[i] + " ";
                }
                ratesString += "]";
                Experiment.log(JCeliacLogger.LOG_RESULT, "%s", ratesString);
                Experiment.log(JCeliacLogger.LOG_RESULT, "Area under Curve (AUC): %.2f", roc.getAUC());
            }
          // OfflineResultExporter resultExporter = new OfflineResultExporter(result.getClassificationOutcomes(),
          //                                                                   this.parameters.getExperimentName() + "-Outcomes.log", this.parameters.getExperimentName() + "-Neighbors.log");
          //  resultExporter.exportResults();
          //  resultExporter.serializeResultsFile(result, this.parameters.getExperimentName() + "-Result.ser");

            ResultExporter exporter = new ResultExporter(this.parameters.getExperimentName());
            exporter.exportResult(result);
            
            if(this.parameters.useSignificanceTest() &&
               result.getNumberOfResults() == 1) 
            {
                try(FileInputStream fileInStream = new FileInputStream(this.parameters.getSignificanceResultFile());
                    ObjectInputStream objInStream = new ObjectInputStream(fileInStream)) {
                    Object o = objInStream.readObject();
                    ClassificationResult referenceResult = (ClassificationResult) o;
                    McNemar test = new McNemar(result.getSingleResult(0), referenceResult, 1);
                    boolean significance = test.testSignificance(this.parameters.getSignificanceLevel());
                    Experiment.log(JCeliacLogger.LOG_RESULT, "Significance Test (alpha = %.2f) -> %s", this.parameters.getSignificanceLevel(), significance == true ? "true" : "false");

                } catch(Exception e) {
                    throw new JCeliacGenericException(e);
                }
            }

            if(this.parameters.isExportFeaturesSerialized()) {
                // figure out training features base directory

                FeatureExporter trainingExporter
                                = new FeatureExporter(new File(this.parameters.trainingDataBaseDirectory),
                                                      this.trainingFeatures,
                                                      this.getExperimentName());
                trainingExporter.exportFeaturesSerialized();
                if(this.evaluationFeatures != null) {
                    FeatureExporter testExporter
                                    = new FeatureExporter(new File(this.parameters.testDataBaseDirectory),
                                                          this.evaluationFeatures,
                                                          this.getExperimentName());
                    testExporter.exportFeaturesSerialized();
                }

            }
            if(this.parameters.isExportFeaturesCSV()) {
                if(this.trainingFeatures.isEmpty() == false && this.evaluationFeatures.isEmpty() == false) {
                    FeatureExporter trainingExporter
                                    = new FeatureExporter(new File(this.parameters.trainingDataBaseDirectory),
                                                          this.trainingFeatures,
                                                          this.getExperimentName() + "-training");
                    trainingExporter.exportFeatureCSV();
                    FeatureExporter evaluationExporter
                                    = new FeatureExporter(new File(this.parameters.testDataBaseDirectory),
                                                          this.evaluationFeatures,
                                                          this.getExperimentName() + "-evaluation");
                    evaluationExporter.exportFeatureCSV();
                }else {
                     FeatureExporter trainingExporter
                                    = new FeatureExporter(new File(this.parameters.trainingDataBaseDirectory),
                                                          this.trainingFeatures,
                                                          this.getExperimentName());
                    trainingExporter.exportFeatureCSV();
                }
                
            }
            return result;
        } else {
            if(result.getNumberOfResults() == 1) {
                Experiment.log(JCeliacLogger.LOG_RESULT, "Evaluating with foreign classes:");
                ForeignClassesClassificationResultAnalyzer analyzer = new ForeignClassesClassificationResultAnalyzer(result.getSingleResult(0));
                analyzer.analyzeAndDisplay(this.parameters);
            }
            // foreign classes are only allowed in offline experiments, hence returning null
            // is ignored in the main experiment
            return null;
        }
    }

    public IDistributedExperimentConnectionHandler getDistributionHandler()
    {
        return distributionHandler;
    }

    public void setDistributionHandler(IDistributedExperimentConnectionHandler distributionHandler)
    {
        this.distributionHandler = distributionHandler;
    }

}
