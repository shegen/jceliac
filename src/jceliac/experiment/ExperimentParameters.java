/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment;

import jceliac.featureOptimization.FeatureOptimizationParameters;
import jceliac.featureOptimization.FeatureOptimizationParametersNULL;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.classification.classificationParameters.*;
import jceliac.video.strategyHandler.*;
import jceliac.tools.data.*;
import jceliac.logging.*;
import java.util.*;
import java.io.*;
import jceliac.*;
import jceliac.classification.model.GenerativeModelParameters;
import jceliac.experiment.Experiment.ExperimentType;
import jceliac.validation.generic.ValidationMethodParameters;

/**
 * The basic parameters shared by all Experiments.
 * <p>
 * This class contains parameters for an experiment. All experiments share some
 * common parameters like base directory, results handling type, and more.
 * <p>
 * The specific experiments will then use specific Parameters that dependant on
 * the used methods for feature extraction and classification.
 * <p>
 * @author shegen
 */
public class ExperimentParameters
        extends AbstractParameters
        implements Cloneable, Serializable
{

    protected ArrayList<ClassLabeledFile> trainingClassBaseDirectories;
    protected ArrayList<ClassLabeledFile> testClassBaseDirectories;
    // name of the training class in one-class SVM, must be in trainingClassBaseDirectories
    protected String oneClassTrainingClassName;
    protected CommandLineParameters cmdParameters;
    protected String featureExtractionParamString = null;
    protected String classificationMethodParamString = null;
    protected String configXMLFile = null;
    protected String featureExtractionMethodName = null;
    protected String classificationMethodName = null;
    protected String optimizationMethodName = null;
    protected String generativeModelName = null;
    protected String validationMethodName = null;
    
    protected String experimentType = "offline";
    protected String experimentName = null;
    protected String trainingDataBaseDirectory = null;
    protected String testDataBaseDirectory = null;
    protected String crossValidationMethodName = null;
    protected String exportDistanceMatrixFile = null;

    protected String significanceResultFile = null;
    protected double significanceLevel = 0.05;

    protected boolean exportFeaturesSerialized = false;
    protected boolean exportFeaturesCSV = false;

    protected boolean interactive = false;
    protected boolean offline = false;
    protected boolean videoSequenceExperiment = false;
    protected boolean allowForeignEvaluationClasses = false;
    protected int testClassCount = -1;
    protected int trainingClassCount = -1;
    
    public FeatureExtractionParameters featureExtractionParameters;
    protected ClassificationParameters classificationMethodParameters;
    protected FeatureOptimizationParameters optimizationMethodParameters;
    protected VideoSequenceHandlerParameters videoSequenceHandlerParamters;
    protected ValidationMethodParameters validationParameters;
    protected GenerativeModelParameters generativeModelParameters;

    protected String projectName = "Default-Project";
    protected HashMap<String, Integer> classIdentifiers;
    protected HashMap<Integer, String> classIdentifiersByClass;
    
    public boolean isVideoSequenceExperiment()
    {
        return videoSequenceExperiment;
    }

    public void setVideoSequenceExperiment(boolean videoSequenceExperiment)
    {
        this.videoSequenceExperiment = videoSequenceExperiment;
    }

    public String getConfigXMLFile()
    {
        return configXMLFile;
    }

    public String getClassificationMethodName()
    {
        return classificationMethodName;
    }

    public String getFeatureExtractionMethodName()
    {
        return featureExtractionMethodName;
    }

    public void setConfigXMLFile(String configXMLFile)
    {
        this.configXMLFile = configXMLFile;
    }

    public String getDataBaseDirectory()
    {
        return trainingDataBaseDirectory;
    }

    public void setDataBaseDirectory(String dataBaseDirectory)
    {
        this.trainingDataBaseDirectory = dataBaseDirectory;
    }

    public String getExperimentName()
    {
        return experimentName;
    }

    public void setExperimentName(String experimentName)
    {
        this.experimentName = experimentName;
    }

    public ExperimentType getExperimentType() 
            throws JCeliacGenericException
    {
        return Experiment.ExperimentType.valueOfIgnoreCase(this.experimentType);
    }

    public void setExperimentType(String experimentType) {
        this.experimentType = experimentType;
    }
   
    public String getFeatureExtractionParamString()
    {
        return featureExtractionParamString;
    }

    public void setFeatureExtractionParamString(String featureExtractionParamString)
    {
        this.featureExtractionParamString = featureExtractionParamString;
    }

    public boolean exportDistanceMatrix()
    {
        return this.exportDistanceMatrixFile != null;
    }

    public boolean isInteractive()
    {
        return interactive;
    }

    public void setInteractive(boolean interactive)
    {
        this.interactive = interactive;
    }

    public boolean isOffline()
    {
        return offline;
    }

    public void setOffline(boolean offline)
    {
        this.offline = offline;
    }

    public String getExportDistanceMatrixFile()
    {
        return exportDistanceMatrixFile;
    }

    public String getOneClassTrainingClassName()
    {
        return oneClassTrainingClassName;
    }

    public void setOneClassTrainingClassName(String oneClassTrainingClassName)
    {
        this.oneClassTrainingClassName = oneClassTrainingClassName;
    }

    public ArrayList<ClassLabeledFile> getTrainingClassBaseDirectories()
    {
        return trainingClassBaseDirectories;
    }

    public ArrayList<ClassLabeledFile> getTestClassBaseDirectories()
    {
        return testClassBaseDirectories;
    }

    public void setClassBaseDirectories(ArrayList<ClassLabeledFile> classBaseDirectories)
    {
        this.setClassBaseDirectories(classBaseDirectories);
    }

    public int getNumberOfTrainingClasses()
    {
        return this.trainingClassCount;
    }

    public void setNumberOfTrainingClasses(int num)
    {
        this.trainingClassCount = num;
    }

    public int getNumberOfTestClasses()
    {
        if(testClassCount == 0) {
            return trainingClassCount; // no test class (LOOCV e.g)
        }
        return testClassCount;
    }

    public int getNumberOfTotalClasses()
    {
        if(testClassCount == 0) {
            return trainingClassCount;
        } else {
            return (trainingClassCount + testClassCount);
        }

    }

    public void setNumberOfTestClasses(int testClassCount)
    {
        this.testClassCount = testClassCount;
    }

    public int getNumberOfClassIdentifiers()
    {
        return this.classIdentifiers.size();
    }

    public ClassLabeledFile getTrainingBaseDirectoryForClass(int classIndex)
    {
        return getTrainingClassBaseDirectories().get(classIndex);
    }

    public ClassLabeledFile getTestBaseDirectoryForClass(int classIndex)
    {
        if(this.testClassBaseDirectories.size() > classIndex) {
            return getTestClassBaseDirectories().get(classIndex);
        } else {
            return null;
        }
    }

    public String getProjectName()
    {
        return projectName;
    }

    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    public ExperimentParameters()
    {
    }

    /**
     * Prepares an experiment by preparing parameters, in particular it checks
     * the supplied data directories for sanity.
     * <p>
     * It also assigns class labels to each class in the data.
     * <p>
     * @throws JCeliacGenericException
     */
    public void prepareExperiment()
            throws JCeliacGenericException
    {
        ClassLabeledDirectoryReader reader = new ClassLabeledDirectoryReader();
        this.trainingClassBaseDirectories = reader.prepareBaseDirectories(this.trainingDataBaseDirectory);

        if(this.testDataBaseDirectory != null) {
            this.testClassBaseDirectories = reader.prepareBaseDirectories(this.testDataBaseDirectory);
            if(this.allowForeignEvaluationClasses == false) {
                // check that the subdirectories have exactly the same naming
                boolean check = validateDataDirectoryNames();
                if(check == false) {
                    throw new InitializeFailedException("Experiment cannot be initialized!");
                }
            }
        } else {
            this.testClassBaseDirectories = new ArrayList<>();
        }
        if(this.testClassBaseDirectories != null) {
            this.testClassCount = this.testClassBaseDirectories.size();
        }
        this.trainingClassCount = this.trainingClassBaseDirectories.size();
        this.assignClassLabels();
    }

    public String getClassName(int classNumber)
            throws JCeliacGenericException
    {
        return this.classIdentifiersByClass.get(classNumber);
    }

    /**
     * Assigns class labels to the training and evaluation directories, ensuring
     * they are the same on all hosts.
     */
    private void assignClassLabels()
    {
        this.classIdentifiers = new HashMap<>();
        this.classIdentifiersByClass = new HashMap<>();

        HashMap<String, Integer> uniqueClassNames = new HashMap<>();
        for(int index = 0; index < this.trainingClassBaseDirectories.size(); index++) {
            uniqueClassNames.put(this.trainingClassBaseDirectories.get(index).getName(), 0);
        }
        for(int index = 0; index < this.testClassBaseDirectories.size(); index++) {
            uniqueClassNames.put(this.testClassBaseDirectories.get(index).getName(), 0);
        }
        // sort class names alphanumerically, we have to do this to ensure the same class labeling on all computers
        // else, the results might vary due to different guessing in the classifiers!
        Set<String> unorderedClassNames = uniqueClassNames.keySet();
        ArrayList<String> orderedClassNames = new ArrayList<>(unorderedClassNames);
        Collections.sort(orderedClassNames);

        for(int index = 0; index < orderedClassNames.size(); index++) {
            String name = orderedClassNames.get(index);
            this.classIdentifiers.put(name, index);
            this.classIdentifiersByClass.put(index, name);
        }

        // assign class labels to each directory
        for(int index = 0; index < this.trainingClassBaseDirectories.size(); index++) {
            String name = this.trainingClassBaseDirectories.get(index).getName();
            int num = classIdentifiers.get(name);
            this.trainingClassBaseDirectories.get(index).setClassNumber(num);
        }
        if(this.testClassBaseDirectories != null) {
            for(int index = 0; index < this.testClassBaseDirectories.size(); index++) {
                String name = this.testClassBaseDirectories.get(index).getName();
                int num = classIdentifiers.get(name);
                this.testClassBaseDirectories.get(index).setClassNumber(num);
            }
        }
    }

    /**
     * Validates the names of directories for sanity.
     * <p>
     * @return True if directories can be used for classification, false if
     *         there is a problem.
     * <p>
     * @throws JCeliacGenericException
     */
    private boolean validateDataDirectoryNames()
            throws JCeliacGenericException
    {
        if(this.classificationMethodParameters instanceof ClassificationParametersSVM) {
            if(((ClassificationParametersSVM) classificationMethodParameters).getSVMType()
               == libsvm.svm_parameter.ONE_CLASS) {
                if(this.oneClassTrainingClassName == null) {
                    Main.log(JCeliacLogger.LOG_ERROR, "oneClassTrainingClassName not set, cannot optimize!");
                    return false;
                } else // check that the oneClassTrainingClassName is in the training set
                {
                    int i;
                    for(i = 0; i < this.trainingClassBaseDirectories.size(); i++) {
                        if(this.trainingClassBaseDirectories.get(i).getName().equals(this.oneClassTrainingClassName)) {
                            break;
                        }
                    }
                    if(i == this.trainingClassBaseDirectories.size()) {
                        Main.log(JCeliacLogger.LOG_ERROR, "oneClassTrainingClassName not found in trainingBaseDirectory!");
                        return false;
                    }
                }
                return true;

            }
        }
        for(int classNum = 0; classNum < this.trainingClassBaseDirectories.size(); classNum++) {
            File f1 = this.trainingClassBaseDirectories.get(classNum);
            File f2 = this.testClassBaseDirectories.get(classNum);

            if(f1.getName().equals(f2.getName()) == false) {
                Main.log(JCeliacLogger.LOG_ERROR, "Training and Test Class Labels do not match (%s vs. %s)!",
                         f1.getParent() + File.separatorChar + f1.getName(), f2.getParent() + File.separatorChar + f2.getName());
                return false;
            }
        }
        return true;
    }

    public void setCommandLineParameters(CommandLineParameters params)
            throws InitializeFailedException
    {
        if(this.featureExtractionParamString == null) {
            this.featureExtractionParamString = params.featureExtractionParamString;
        }
        if(this.classificationMethodParamString == null) {
            this.classificationMethodParamString = params.classificationParamString;
        }
        if(this.featureExtractionMethodName == null) {
            this.featureExtractionMethodName = params.featureExtractionMethod;
        }
        if(this.classificationMethodName == null) {
            this.classificationMethodName = params.classificationMethod;
        }
        if(this.configXMLFile == null) {
            this.configXMLFile = params.configXMLFile;
        }
        if(this.experimentName == null) {
            this.experimentName = params.experimentName;
        }

        if(params.optimizationMethod != null) {
            Main.log(JCeliacLogger.LOG_INFO, "CMD Overriding Optimization Method (%s).", params.optimizationMethod);
            this.optimizationMethodName = params.optimizationMethod;
        }
        if(params.generativeModel != null) {
            Main.log(JCeliacLogger.LOG_INFO, "CMD Overriding Generative Model (%s).", params.generativeModel);
            this.generativeModelName = params.generativeModel;
        }
        if(params.trainingDataBaseDirectory != null) {
            Main.log(JCeliacLogger.LOG_INFO, "CMD Overriding (Training) Data Directory (%s).", params.trainingDataBaseDirectory);
            this.trainingDataBaseDirectory = params.trainingDataBaseDirectory;
        }
        if(params.testDataBaseDirectory != null) {
            Main.log(JCeliacLogger.LOG_INFO, "CMD Overriding (Test) Data Directory (%s).", params.testDataBaseDirectory);
            this.testDataBaseDirectory = params.testDataBaseDirectory;
        }
        if(params.featureSubset != null) {
            Main.log(JCeliacLogger.LOG_INFO, "CMD Overriding Feature Subset! Using NULL Optimizer!");
            this.optimizationMethodParameters = new FeatureOptimizationParametersNULL();
            this.optimizationMethodParameters.setFeatureSubset(params.featureSubset);
            this.optimizationMethodName = "NULL";
        }
        /* 
         * XXX: This should be improved and is not actually meant to be used from the command line!
         */
        if(params.validationMethod != null) {
            Main.log(JCeliacLogger.LOG_INFO, "Validation Method: %s", params.validationMethod);
            this.validationMethodName = params.validationMethod;
            this.validationParameters = new ValidationMethodParameters();
            System.out.println(params.validationMethod );
            this.validationParameters.setCrossValidationType(this.validationMethodName);
        }
        if(params.exportDistanceMatrixFile != null) {
            this.exportDistanceMatrixFile = params.exportDistanceMatrixFile;
            Main.log(JCeliacLogger.LOG_INFO, "Exporting Feature Distance Matrix to: %s", params.exportDistanceMatrixFile);
        }
        // default is offline
        this.interactive = params.interactive;
        this.offline = params.offline;
        this.allowForeignEvaluationClasses = params.allowForeignEvaluationClasses;

    }

    public void setFeatureExtractionParameters(FeatureExtractionParameters params)
    {
        this.featureExtractionParameters = params;
    }

    public void setClassificationMethodParameters(ClassificationParameters params)
    {
        this.classificationMethodParameters = params;
    }

    public void setVideoSequenceHandlerParameters(VideoSequenceHandlerParameters videoSequenceHandlerParamters)
    {
        this.videoSequenceHandlerParamters = videoSequenceHandlerParamters;
    }

    public CommandLineParameters getCmdParameters()
    {
        return cmdParameters;
    }

    public void setCmdParameters(CommandLineParameters cmdParameters)
    {
        this.cmdParameters = cmdParameters;
    }

    public String getClassificationMethodParamString()
    {
        return classificationMethodParamString;
    }

    public void setClassificationMethodParamString(String classificationParamString)
    {
        this.classificationMethodParamString = classificationParamString;
    }

    public void setFeatureExtractionMethodName(String featureExtractionMethod)
    {
        this.featureExtractionMethodName = featureExtractionMethod;
    }

    public void setClassificationMethodName(String classificationMethod)
    {
        this.classificationMethodName = classificationMethod;
    }

    public String getClassIdentifier(int classNum)
    {
        return this.classIdentifiersByClass.get(classNum);
    }

    @Override
    public Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }

    public FeatureOptimizationParameters getOptimizationMethodParameters()
    {
        return optimizationMethodParameters;
    }

    public void setOptimizationMethodParameters(FeatureOptimizationParameters optimizationMethodParameters)
    {
        this.optimizationMethodParameters = optimizationMethodParameters;
    }

    public String getOptimizationMethodName()
    {
        return optimizationMethodName;
    }

    public void setOptimizationMethodName(String optimizationMethodName)
    {
        this.optimizationMethodName = optimizationMethodName;
    }

    public String getSignificanceResultFile()
    {
        return significanceResultFile;
    }

    public boolean useSignificanceTest()
    {
        return this.significanceResultFile != null;
    }

    public double getSignificanceLevel()
    {
        return significanceLevel;
    }

    public void setSignificanceLevel(double significanceLevel)
    {
        this.significanceLevel = significanceLevel;
    }

    public boolean isExportFeaturesSerialized()
    {
        return this.exportFeaturesSerialized;
    }

    public void setExportFeatures(boolean exportFeatures)
    {
        this.exportFeaturesSerialized = exportFeatures;
    }

    public boolean isExportFeaturesCSV()
    {
        return exportFeaturesCSV;
    }

    public void setExportFeaturesCSV(boolean exportFeaturesCSV)
    {
        this.exportFeaturesCSV = exportFeaturesCSV;
    }

    public String getGenerativeModelName()
    {
        return generativeModelName;
    }

    public void setGenerativeModelName(String generativeModelName)
    {
        this.generativeModelName = generativeModelName;
    }  

    public GenerativeModelParameters getGenerativeModelParameters()
    {
        return generativeModelParameters;
    }

    public void setGenerativeModelParameters(GenerativeModelParameters generativeModelParameters)
    {
        this.generativeModelParameters = generativeModelParameters;
    }
    public ValidationMethodParameters getValidationParameters()
    {
        return validationParameters;
    }

    public void setValidationMethodParameters(ValidationMethodParameters validationParameters)
    {
        this.validationParameters = validationParameters;
    }

    public String getValidationMethodName()
    {
        return validationMethodName;
    }

    public void setValidationMethodName(String validationMethodName)
    {
        this.validationMethodName = validationMethodName;
    } 
}
