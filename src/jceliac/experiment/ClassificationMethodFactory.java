/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment;

import jceliac.JCeliacGenericException;
import jceliac.classification.classificationParameters.ClassificationParameters;
import jceliac.classification.EClassificationMethod;
import jceliac.logging.JCeliacLogger;
import jceliac.classification.classificationParameters.ClassificationParametersKNN;
import jceliac.classification.ClassificationMethod;
import jceliac.classification.KNN.ClassificationMethodKNN;
import jceliac.classification.kernelSVM.ClassificationParametersKernelSVM;
import jceliac.classification.kernelSVM.ExperimentalClassificationMethodKernelSVM;
import jceliac.classification.linearSVM.ClassificationParametersLinearSVM;
import jceliac.classification.linearSVM.ExperimentalClassificationMethodLinearSVM;

/**
 * Factory for the creation of objects of type ClassificationMethod.
 * <p>
 * @author shegen
 */
public class ClassificationMethodFactory
{

    public static ClassificationMethod
            createExperimentalClassificationMethod(String methodString,
                                                   ClassificationParameters params,
                                                   int classCount)
            throws JCeliacGenericException
    {
        EClassificationMethod method = ClassificationMethodFactory.mapClassificationMethodName(methodString);
        switch(method) {
            case KNN:
                return new ClassificationMethodKNN(params);
            case KERNEL_SVM:
                return new ExperimentalClassificationMethodKernelSVM(params);
            case LINEAR_SVM:
                return new ExperimentalClassificationMethodLinearSVM(params);
            default:
                Experiment.log(JCeliacLogger.LOG_ERROR, "Invalid Classification Method!");
                throw new JCeliacGenericException("Invalid or unsupported classification method.");
        }
    }

    public static EClassificationMethod mapClassificationMethodName(String methodString)
            throws InitializeFailedException
    {
        return EClassificationMethod.valueOfIgnoreCase(methodString);
    }

    /**
     * Creates default classification method parameters based on method type.
     * <p>
     * @param method
     * <p>
     * @return Default classification method parameters.
     * <p>
     * @throws jceliac.experiment.InitializeFailedException
     */
    public static ClassificationParameters createParametersByMethod(String method)
            throws InitializeFailedException
    {
        EClassificationMethod type = ClassificationMethodFactory.mapClassificationMethodName(method);
        switch(type) {
            case KNN:
                return new ClassificationParametersKNN();
            case KERNEL_SVM:
                return new ClassificationParametersKernelSVM();
            case LINEAR_SVM:
                return new ClassificationParametersLinearSVM();
            default:
                throw new InitializeFailedException("Classification Method (" + method + ") is not supported!");
        }
    }

}
