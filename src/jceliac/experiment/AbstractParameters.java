/*
 * 
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at> 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * Each line should be prefixed with  * 
 */
package jceliac.experiment;

import java.lang.reflect.Field;
import jceliac.Main;
import jceliac.logging.JCeliacLogger;

/**
 * Abstract base class of all parameter type methods.
 * <p>
 * @author shegen
 */
public abstract class AbstractParameters
{

    /**
     * Checks by using reflection if the classification parameter object or it's
     * superclass has a field with corresponding name.
     * <p>
     * @param name User supplied name of a field.
     * <p>
     * @return True if the field exists, false if the field is not existent.
     */
    public boolean hasOption(String name)

    {
        Class c = this.getClass();
        Field[] fields = c.getDeclaredFields();
        for(Field field : fields) {
            if(name.equalsIgnoreCase(field.getName())) {
                return true;
            }
        }
        return this.superClassHasOption(this.getClass(), name);
    }

    /**
     * Checks by using reflection if the classification parameter object's
     * superclass has a field with corresponding name.
     * <p>
     * @param name User supplied name of a field.
     * <p>
     * @return True if the field exists, false if the field is not existent.
     */
    private boolean superClassHasOption(Class c, String name)
    {
        if(c.getSuperclass() == null) {
            return false;
        }
        Field[] fields = c.getSuperclass().getDeclaredFields();
        for(Field field : fields) {
            if(name.equalsIgnoreCase(field.getName())) {
                return true;
            }
        }
        return this.superClassHasOption(c.getSuperclass(), name);
    }

    /**
     * Sets the value of a field using reflection.
     * <p>
     * @param name  Name of the field.
     * @param value Value to set the field to.
     * <p>
     * @throws jceliac.experiment.InitializeFailedException
     */
    public void setOption(String name, Object value)
            throws InitializeFailedException
    {
        if(name == null || name.length() == 0) {
            return;
        }

        Class c = this.getClass();
        Field[] fields = c.getDeclaredFields();
        for(Field field : fields) {
            // we do not use getter or setter methods as we can access
            // our files in here!
            if(name.equalsIgnoreCase(field.getName())) {
                setOptionByType(field, value);
                return;
            }
        }
        superClassSetOption(c.getSuperclass(), name, value);

    }

    /**
     * Sets the value of a field in the super class using reflection.
     * <p>
     * @param name  Name of the field.
     * @param value Value to set the field to.
     * <p>
     * @throws jceliac.experiment.InitializeFailedException
     */
    public void superClassSetOption(Class c, String name, Object value)
            throws InitializeFailedException
    {
        if(name == null || name.length() == 0) {
            return;
        }
        if(c.getSuperclass() == null) {
            return;
        }
        
        Field[] fields = c.getDeclaredFields();
        for(Field field : fields) {
            // we do not use getter or setter methods as we can access
            // our files in here!
            if(name.equalsIgnoreCase(field.getName())) {
                setOptionByType(field, value);
                return;
            }
        }
        this.superClassSetOption(c.getSuperclass(), name, value);

    }

    /**
     * Figures out the type of a field and set's the value.
     * <p>
     * @param field Field to set value.
     * @param value Value to set field to.
     * <p>
     * @throws jceliac.experiment.InitializeFailedException
     */
    protected void setOptionByType(Field field, Object value)
            throws InitializeFailedException
    {
        Object type = field.getType();
        field.setAccessible(true);

        try {
            if(type.equals(Boolean.TYPE)) {
                boolean bValue;
                String v = (String) value;
                switch(v) {
                    case "true":
                        bValue = true;
                        break;
                    case "false":
                        bValue = false;
                        break;
                    default:
                        throw new InitializeFailedException("Boolean value neither true or false!");
                }

                field.set(this, bValue);
            } else if(type.equals(Integer.TYPE)) {
                int iValue;
                try {
                    iValue = Integer.parseInt((String) value);
                } catch(NumberFormatException e) {
                    double dValue = Double.parseDouble((String) value);
                    iValue = (int) dValue;
                }
                field.set(this, iValue);
            } else if(type.equals(Double.TYPE)) {
                double dValue;
                try {
                    dValue = Double.parseDouble((String) value);
                    field.set(this, dValue);
                } catch(NumberFormatException e) {
                    Main.log(JCeliacLogger.LOG_ERROR, "Setting parameter field failed: %s.", e.getMessage());

                }
            } else if(type.equals(int [].class)) {
                try {
                    int [] intValues = this.parseIntegerString((String) value);
                    field.set(this, intValues);
                } catch(NumberFormatException e) {
                    Main.log(JCeliacLogger.LOG_ERROR, "Setting parameter field failed: %s.", e.getMessage());
                }
            }
            else if(type.equals(double [].class)) {
                try {
                    double [] dValues = this.parseDoubleString((String) value);
                    field.set(this, dValues);
                } catch(NumberFormatException e) {
                    Main.log(JCeliacLogger.LOG_ERROR, "Setting parameter field failed: %s.", e.getMessage());
                }
            }
            else {
                field.set(this, value);
            }
        } catch(IllegalAccessException e) {
            throw new InitializeFailedException("Setting parameter field failed.");
        }
    }
    
    protected int [] parseIntegerString(String values) 
    {
        String [] stringValues = values.split(",");
        int [] intValues = new int[stringValues.length];
        for(int i = 0; i < intValues.length; i++) {
            intValues[i] = Integer.parseInt(stringValues[i]);
        }
        return intValues;
    }
    
    protected double [] parseDoubleString(String values) 
    {
        String [] stringValues = values.split(",");
        double [] dValues = new double[stringValues.length];
        for(int i = 0; i < dValues.length; i++) {
            dValues[i] = Double.parseDouble(stringValues[i]);
        }
        return dValues;
    }
}
