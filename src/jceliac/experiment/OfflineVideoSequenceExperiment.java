/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment;

import jceliac.video.strategyHandler.VideoSequenceHandler;
import jceliac.tools.transforms.wavelet.WaveletTransform.*;
import jceliac.JCeliacGenericException;
import java.util.*;
import jceliac.classification.IteratedClassificationResult;

/**
 * XXX: This code has to be rewritten for the new classifiers. 
 * Experiments based on Video Sequences are different from the standard
 * experiment types. We only perform distinct set evaluation using a set of
 * still images for training. The classification method needs more flexibility
 * because we classify a single video using multiple frames.
 * <p>
 * @author shegen
 */
@Deprecated
public class OfflineVideoSequenceExperiment
        extends OfflineExperiment
{

    private final ArrayList<Object> strategieOutcomes = new ArrayList<>();

    public OfflineVideoSequenceExperiment(ExperimentParameters param) throws
            JCeliacGenericException
    {
        super(param);
    }

    @Override
    public IteratedClassificationResult runExperiment()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

   /* XXX: This has to be rewritten if ever used. 
    @Override
    public ClassificationResult runExperimentThreaded() throws
            JCeliacGenericException
    {

        int procs = 1;
        int numberOfTrainingImages;
        Thread[] threadTable = new Thread[procs];

        this.trainingFeatures = new ArrayList<>();
        this.evaluationFeatures = new ArrayList<>();
        Experiment.log(JCeliacLogger.LOG_INFO, "Extracting Features using %d Threads!", procs);
        this.extractionMethod.report();

        PerformanceTimer timer = new PerformanceTimer();
        timer.startTimer();

        // Extract training features from images and train the classifiers
        numberOfTrainingImages = extractFeatures(trainingFeatures, trainDataReaders, this.parameters.getNumberOfTrainingClasses(), false);

        timer.stopTimer();
        jceliac.tools.memory.MemoryInfos.dumpMemoryInfos();

        Experiment.log(JCeliacLogger.LOG_INFO, "Feature Extraction Finished Successfully.");
        Experiment.log(JCeliacLogger.LOG_INFO, "Features Extracted from %d (Training) and %d Classes!", numberOfTrainingImages, this.parameters.getNumberOfTestClasses());
        Experiment.log(JCeliacLogger.LOG_INFO, "Features Extracted in %f Seconds.", timer.getSeconds());

        /* make room for data flush the caches */
       /* for(int i = 0; i < trainDataReaders.size(); i++) {
            this.trainDataReaders.get(i).cleanup();
        }
        /*
         * we set only training features and train for each extracted set of
         * features (i.e. for each frame). Pre-extraction of test features is no
         * option here.
         */
      
    /*    PerformanceTimer localTimer = new PerformanceTimer();
        localTimer.startTimer();
        /*
         * We use a meta object to handle the classification and feature
         * extraction when using video sequences. This gives us much more
         * flexibility without breaking the basic experiment API.
         */
   /*     for(int classIndex = 0; classIndex < testDataReaders.size(); classIndex++) {

            DataReader currentReader = testDataReaders.get(classIndex);
            while(currentReader.hasNext()) {

                DataSource currentFile = (DataSource) currentReader.next();
                ContentStream stream = currentFile.createContentStream();
                if((stream instanceof VideoContentStream) == false) {
                    Experiment.log(JCeliacLogger.LOG_ERROR, "Non-Movie Type Content Stream Encountered! Skipping!");
                    continue;
                }
                VideoContentStream cstream = (VideoContentStream) stream;

                VideoSequenceHandler vscHandler
                                     = new VideoSequenceHandler(this,
                                                                super.parameters.videoSequenceHandlerParamters,
                                                                this.classificationMethod,
                                                                this.extractionMethod,
                                                                this.optimizationMethod,
                                                                cstream);
                try {
                    boolean scheduled = false;
                    while(scheduled == false) {
                        scheduled = scheduleVideoHandlerThread(threadTable, vscHandler);
                    }
                } catch(Exception e) {
                    Experiment.log(JCeliacLogger.LOG_ERROR, e.getMessage());
                    throw new JCeliacGenericException(e);
                }
            }
        }
        localTimer.stopTimer();
        System.out.printf("Finished in %f Seconds\n", localTimer.getSeconds());

        AbstractOutcomeProcessingStrategy outcomeProcessingStrategy
                                          = StrategyFactory.createStrategyHandler(super.parameters.videoSequenceHandlerParamters.getOutcomeProcessingParameters());
        outcomeProcessingStrategy.perform(strategieOutcomes);

        PerformanceTimer cTimer = new PerformanceTimer();
        cTimer.startTimer();

        ClassificationResult result = this.classificationMethod.classify(this.optimizationMethod);
        cTimer.stopTimer();
        Experiment.log(JCeliacLogger.LOG_INFO, "Classification done in %f", cTimer.getSeconds());
        Experiment.log(JCeliacLogger.LOG_RESULT, "Classification Performance:");

        // ok it can now happen that we omit a class in the output. this is the case
        // if a training class is existent that is not a test class (only allowed in ONE_CLASS SVM)
        for(int classNum = 0; classNum < this.parameters.getNumberOfClassIdentifiers(); classNum++) {
            String classIdentifier = super.parameters.getClassName(classNum);
            if(result.hasClassIdentifier(classIdentifier)) {
                Experiment.log(JCeliacLogger.LOG_RESULT, "Class-%d (%s): %.2f", classNum, classIdentifier,
                               100 * result.getClassificationRate(classIdentifier));
            }
        }
        Experiment.log(JCeliacLogger.LOG_RESULT, "Overall Classification Rate: %.2f", 100 * result.getOverallClassificationRate());
        Experiment.log(JCeliacLogger.LOG_RESULT, "Number of Tied Outcomes: %d", result.getTieCount());
        Experiment.log(JCeliacLogger.LOG_RESULT, "Number of Guessed Outcomes: %d", result.getGuessCount());
        this.optimizationMethod.getOptimizedClassificationConfiguration().report();
        return result;

    }
    */

    protected boolean scheduleVideoHandlerThread(Thread[] threadTable, VideoSequenceHandler handler)
    {

        for(int index = 0; index < threadTable.length; index++) {
            if(threadTable[index] == null) {
                // schedule thread
                threadTable[index] = handler;
                handler.start();
                return true;  // signal scheduled
            } else if(threadTable[index].isAlive() == false) {
                threadTable[index] = handler;
                handler.start();
                return true;  // signal scheduled
            }
        }
        return false; // signal not scheduled
    }

    protected boolean threadsRunning(Thread[] threadTable)
            throws Exception
    {
        for(Thread thread : threadTable) {
            if(thread != null && thread.isAlive() == true) {
                thread.join();
            }
        }
        return false;
    }

    @Override
    public synchronized void threadFinish(Object o)
    {
        this.strategieOutcomes.add(o);
        this.notifyAll();
    }

}
