/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jceliac.experiment.custom;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationResult;
import jceliac.classification.IteratedClassificationResult;

/**
 *
 * @author shegen
 */
public class DoubleResultExporter 
{
    private final File outfile;

    public DoubleResultExporter(String experimentName)
    {
        String resultFilename = experimentName + ".performance";
        this.outfile = new File(resultFilename);
    }
    
    public void exportResult(ArrayList <Double> computationTimes)
            throws JCeliacGenericException
    {
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(this.outfile))) {
            for(int i = 0; i < computationTimes.size(); i++) {
                double time = computationTimes.get(i);
                writer.write(time+"\n");
            }
            writer.close();
        } catch(IOException e) {
            throw new JCeliacGenericException(e);
        }
    }
}

