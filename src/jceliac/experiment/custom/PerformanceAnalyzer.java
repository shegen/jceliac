/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.experiment.custom;

import java.util.ArrayList;
import jceliac.experiment.Experiment;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.math.MathTools;

/**
 *
 * @author shegen
 */
public class PerformanceAnalyzer 
{
    private final ArrayList <DiscriminativeFeatures> features;

    public PerformanceAnalyzer(ArrayList <DiscriminativeFeatures> features)
    {
        this.features = features;
    }
    
    public void analyze()
    {
        ArrayList <Double> allFeatureComputationTimes = new ArrayList <>();
        ArrayList <Double> allScalespaceComputationTimes = new ArrayList <>();
        ArrayList <Double> allTotalTimes = new ArrayList <>();
        
        for(DiscriminativeFeatures feature : this.features) {
            double featureComputationTime = feature.featureComputationTime;
            double scalepaceComputationTime = feature.scalespaceAndEstimationTime;
            double totalTime = feature.totalComputationTime;
            
            allFeatureComputationTimes.add(featureComputationTime);
            allScalespaceComputationTimes.add(scalepaceComputationTime);
            allTotalTimes.add(totalTime);
        }
        double meanTimeFeatures = ArrayTools.mean(allFeatureComputationTimes);
        double meanTimeScalespace = ArrayTools.mean(allScalespaceComputationTimes);
        double meanTotal = ArrayTools.mean(allTotalTimes);
        
        double stdevFeatures = MathTools.stdev(allFeatureComputationTimes);
        double stdevScalespace = MathTools.stdev(allScalespaceComputationTimes);
        double stdevTotal = MathTools.stdev(allTotalTimes);
        
        Experiment.log(JCeliacLogger.LOG_RESULT, "Performance MEAN (LBP): %f", meanTimeFeatures);
        Experiment.log(JCeliacLogger.LOG_RESULT, "Performance STD: %f", stdevFeatures);
        
        Experiment.log(JCeliacLogger.LOG_RESULT, "Performance MEAN (Scalespace): %f", meanTimeScalespace);
        Experiment.log(JCeliacLogger.LOG_RESULT, "Performance STD: %f", stdevScalespace);
        
        Experiment.log(JCeliacLogger.LOG_RESULT, "Performance MEAN (Total): %f", meanTotal);
        Experiment.log(JCeliacLogger.LOG_RESULT, "Performance STD: %f", stdevTotal);
        
    }
}
