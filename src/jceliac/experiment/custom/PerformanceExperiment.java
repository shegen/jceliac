/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jceliac.experiment.custom;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import jceliac.JCeliacGenericException;
import jceliac.classification.IteratedClassificationResult;
import jceliac.experiment.Experiment;
import jceliac.experiment.ExperimentParameters;
import jceliac.experiment.FeatureExtractionMethodFactory;
import jceliac.experiment.OfflineExperiment;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.SALBP.FeatureExtractionMethodScaleAdaptiveLBP;
import jceliac.featureExtraction.SALBP.FeatureExtractionParametersScaleAdaptiveLBP;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.ContentStream;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.DataSource;
import jceliac.tools.data.Frame;
import jceliac.tools.filter.scalespace.ScalespaceParameters;
import jceliac.tools.math.MathTools;
import jceliac.tools.timer.PerformanceTimer;

/**
 *
 * @author shegen
 */
public class PerformanceExperiment 
    extends OfflineExperiment 
{

    public PerformanceExperiment(ExperimentParameters param) 
            throws JCeliacGenericException 
    {
        super(param);
    }

    @Override
    public IteratedClassificationResult
            runExperimentThreaded()
            throws JCeliacGenericException
    {

        this.evaluationFeatures = new ArrayList<>();
        this.extractionMethod.report();

        ArrayList<Double> computationTimes = new ArrayList<>();
        ArrayList<Double> allFeatureComputationTimes = new ArrayList<>();
        ArrayList<Double> allScalespaceComputationTimes = new ArrayList<>();
        ArrayList<Double> allOrientationComputationTimes = new ArrayList<>();
        ArrayList<Double> allTotalTimes = new ArrayList<>();

        double step = 0.25;
        ScalespaceParameters scalespaceParameters = new ScalespaceParameters(Math.sqrt(2.0d),
                                                                             -4,
                                                                             8,
                                                                             step,
                                                                             2.1214);

        try {
            for(int iteration = 0; iteration <= 20; iteration++) {
/*
                ((FeatureExtractionParametersScaleAdaptiveLBP) this.parameters.featureExtractionParameters).setScalespaceStep(step);
                this.extractionMethod = FeatureExtractionMethodFactory.createFeatureExtractionMethod(
                        this.parameters.getFeatureExtractionMethodName(),
                        this.parameters.featureExtractionParameters);*/
                this.initializeExperiment();

                PerformanceTimer timer = new PerformanceTimer();
                timer.startTimer();
                this.evaluationFeatures = this.extractFeatures(this.testDataReaders, this.parameters.getNumberOfTestClasses(), false);
                timer.stopTimer();

                if(iteration > 0) { // ignore the first iteration let the JIT compiler optimize the code first
                    double timePerImage = timer.getSeconds() / this.evaluationFeatures.size();
                    computationTimes.add(timePerImage);
                    double curMean = ArrayTools.mean(computationTimes);
                    double stdev = MathTools.stdev(computationTimes);
                    double standardError = stdev / Math.sqrt(iteration);

                    Experiment.log(JCeliacLogger.LOG_INFO, "Iteration: %d, current MEAN: %f, current Standard Error: %f", iteration, curMean, standardError);

                    for(DiscriminativeFeatures feature : this.evaluationFeatures) {
                        allFeatureComputationTimes.add(feature.featureComputationTime);
                        allScalespaceComputationTimes.add(feature.scalespaceAndEstimationTime);
                        allTotalTimes.add(feature.totalComputationTime);
                        allOrientationComputationTimes.add(feature.orientationTime);
                    }
                }

            //PerformanceAnalyzer analyzer = new PerformanceAnalyzer(this.evaluationFeatures);
                //analyzer.analyze();
                this.evaluationFeatures = new ArrayList<>();
                System.gc();
                for(DataReader reader : this.testDataReaders) {
                    reader.reset();
                }
            }
            double meanTotal = ArrayTools.mean(allTotalTimes);
            double meanSS = ArrayTools.mean(allScalespaceComputationTimes);
            double meanLBP = ArrayTools.mean(allFeatureComputationTimes);
            double meanOrientation = ArrayTools.mean(allOrientationComputationTimes);

            double stdTotal = MathTools.stdev(allTotalTimes);
            double stdSS = MathTools.stdev(allScalespaceComputationTimes);
            double stdLBP = MathTools.stdev(allFeatureComputationTimes);

            double percentageLBP = meanLBP / meanTotal;
            double percentageSS = meanSS / meanTotal;
            double percentageOrientation = meanOrientation / meanTotal;
        //    Experiment.log(JCeliacLogger.LOG_INFO, "Perc. Scalespace: %f, Perc. LBP: %f, Perc. Orientation: %f \n", percentageSS, percentageLBP, percentageOrientation);

            this.extractionMethod.cleanup();
            jceliac.tools.memory.MemoryInfos.dumpMemoryInfos();

            DoubleResultExporter exporter = new DoubleResultExporter(this.parameters.getExperimentName());
            exporter.exportResult(computationTimes);
            return new IteratedClassificationResult();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }


        
    @SuppressWarnings({"unchecked", "ThrowFromFinallyBlock"})
    @Override
    protected ArrayList<DiscriminativeFeatures>
            extractFeatures(ArrayList<DataReader> readers,
                            int numberOfClasses, boolean isTraining)
            throws JCeliacGenericException
    {
        ArrayList<DiscriminativeFeatures> extractedFeatures = new ArrayList<>();
        int procs = 4; //Runtime.getRuntime().availableProcessors();
        // this.currentCollection = featureCollection;
        int sampleCount = 0;
        ExecutorService threadExecutor = Executors.newScheduledThreadPool(procs);
        ExecutorCompletionService<DiscriminativeFeatures> completionService = new ExecutorCompletionService<>(threadExecutor);
        ArrayList<Future<DiscriminativeFeatures>> futures = new ArrayList<>();
        JCeliacGenericException caughtException = null;

        try {
            for(int classIndex = 0; classIndex < numberOfClasses; classIndex++) {
                boolean isOneClassTrainingClass = false;
                DataReader currentReader = readers.get(classIndex);

                while(currentReader.hasNext()) {
                    DataSource currentFile = (DataSource) currentReader.next();
                    ContentStream cstream = currentFile.createContentStream();
                    while(cstream.hasNext()) {
                        Frame content = cstream.next();

                        futures.add(
                                completionService.submit(
                                        this.extractionMethod.createFeatureExtractionThread(
                                                this, content, currentReader.getClassNumber(),
                                                isOneClassTrainingClass, isTraining)));
                        sampleCount++;
                    }
                }
            }
            for(Future<DiscriminativeFeatures> tmp : futures) {
                try {
                    Future<DiscriminativeFeatures> computedFuture = completionService.take();
                    DiscriminativeFeatures computedFeatures = computedFuture.get();
                    extractedFeatures.add(computedFeatures);
                } catch(InterruptedException | ExecutionException e) {
                    StackTraceElement[] els = e.getStackTrace();
                    caughtException = new JCeliacGenericException(e);
                    e.printStackTrace();
                    break;
                }
            }
        } catch(JCeliacGenericException e) {
            throw e;
        } finally {
            for(Future<DiscriminativeFeatures> future : futures) {
                future.cancel(true);
            }
            threadExecutor.shutdownNow();
            if(caughtException != null) {
                throw caughtException;
            }
        }
        return extractedFeatures;
    }
    
    
}
