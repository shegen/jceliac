/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jceliac.experiment.custom;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.classification.IteratedClassificationResult;
import jceliac.classification.model.GenerativeModelFactory;
import jceliac.experiment.ClassificationMethodFactory;
import jceliac.experiment.Experiment;
import jceliac.experiment.ExperimentParameters;
import jceliac.experiment.FeatureExtractionMethodFactory;
import jceliac.experiment.InitializeFailedException;
import jceliac.experiment.InvalidInputException;
import jceliac.experiment.OfflineExperiment;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.LBP.FeatureExtractionMethodLBP;
import jceliac.featureExtraction.LBP.FeatureExtractionParametersLBP;
import jceliac.featureOptimization.FeatureOptimizationMethodFactory;
import jceliac.logging.JCeliacConsoleLogger;
import jceliac.logging.JCeliacFileLogger;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.arrays.ArrayTools;
import jceliac.tools.data.CachedDataReader;
import jceliac.tools.data.ContentStream;
import jceliac.tools.data.DataReader;
import jceliac.tools.data.DataSource;
import jceliac.tools.data.Frame;
import jceliac.tools.math.MathTools;
import jceliac.validation.generic.ValidationMethodFactory;

/**
 *
 * @author shegen
 */
public class HistIntersectExperiment 
    extends OfflineExperiment 
{

    public HistIntersectExperiment(ExperimentParameters param) throws JCeliacGenericException {
        super(param);
    }

    @Override
    public void initializeExperiment() 
            throws InitializeFailedException, JCeliacGenericException 
    {
        Experiment.logger = new JCeliacLogger[2];
        Experiment.logger[0] = new JCeliacConsoleLogger();
        if(this.subExperimentIdentifier == -1) {
            Experiment.logger[1] = new JCeliacFileLogger(this.parameters.getExperimentName());
        }else {
            Experiment.logger[1] = new JCeliacFileLogger(this.parameters.getExperimentName()+"-"+this.subExperimentIdentifier);
        }
         try {

            this.trainDataReaders = new ArrayList<>();
            this.testDataReaders = new ArrayList<>();

            for(int i = 0; i < this.parameters.getNumberOfTrainingClasses(); i++) {
                CachedDataReader tmp = new CachedDataReader(this.parameters.getTrainingBaseDirectoryForClass(i), false);
                trainDataReaders.add(tmp);
                this.numberOfImages += tmp.getNumberOfFiles();
            }
            for(int i = 0; i < this.parameters.getNumberOfTestClasses(); i++) {
                CachedDataReader tmp = new CachedDataReader(this.parameters.getTestBaseDirectoryForClass(i), false);
                this.testDataReaders.add(tmp);
                this.numberOfImages += tmp.getNumberOfFiles();
            }
        } catch(Exception e) {
            throw new JCeliacGenericException(e);
        }
      
    }

    
    @Override
    public IteratedClassificationResult runExperimentThreaded() 
            throws JCeliacGenericException 
    {
       
        //this.initializeExperiment();
        FeatureExtractionParametersLBP lbpParams = new FeatureExtractionParametersLBP();
        lbpParams.setMinScale(1);
        lbpParams.setMaxScale(1);
        FeatureExtractionMethodLBP lbp = new FeatureExtractionMethodLBP(lbpParams);
        lbp.initialize();
      
        ArrayList <DiscriminativeFeatures> trainingFeatures = new ArrayList <>();
        for (int classIndex = 0; classIndex < this.parameters.getNumberOfTrainingClasses(); classIndex++) 
        {
            DataReader currentReader = this.trainDataReaders.get(classIndex);
            while (currentReader.hasNext()) {
                DataSource currentFile = (DataSource) currentReader.next();
                ContentStream cstream = currentFile.createContentStream();
                while (cstream.hasNext()) {
                    Frame content = cstream.next();
                    DiscriminativeFeatures features = lbp.extractFeatures(content);
                    features.setClassNumber(classIndex);
                    trainingFeatures.add(features);
                }
            }
        }
        
        lbpParams = new FeatureExtractionParametersLBP();
        lbpParams.setMinScale(2);
        lbpParams.setMaxScale(2);
        lbp = new FeatureExtractionMethodLBP(lbpParams);
        lbp.initialize();
          
        ArrayList <DiscriminativeFeatures> evalFeatures = new ArrayList <>();
        for (int classIndex = 0; classIndex < this.parameters.getNumberOfTestClasses(); classIndex++) 
        {
            DataReader currentReader = this.testDataReaders.get(classIndex);
            while (currentReader.hasNext()) {
                DataSource currentFile = (DataSource) currentReader.next();
                ContentStream cstream = currentFile.createContentStream();
                while (cstream.hasNext()) {
                    Frame content = cstream.next();
                    DiscriminativeFeatures features = lbp.extractFeatures(content);
                    features.setClassNumber(classIndex);
                    evalFeatures.add(features);
                }
            }
        }
        
        ArrayList <Double> intraClassDistances = new ArrayList <>();
        ArrayList <Double> interClassDistances = new ArrayList <>();
        double [][] distances = new double[trainingFeatures.size()][evalFeatures.size()];
        for(int i = 0; i < trainingFeatures.size(); i++) {
            for(int j = 0; j < evalFeatures.size(); j++) {
                distances[i][j] = trainingFeatures.get(i).distanceTo(evalFeatures.get(j));
                if(trainingFeatures.get(i).getClassNumber() == evalFeatures.get(j).getClassNumber()) {
                    intraClassDistances.add(distances[i][j]);
                }else {
                    interClassDistances.add(distances[i][j]);
                }
             }
        }
        double intraClassMean = ArrayTools.mean(intraClassDistances);
        double intraClassStd = MathTools.stdev(intraClassDistances);
        
        double interClassMean = ArrayTools.mean(interClassDistances);
        double interClassStd = MathTools.stdev(interClassDistances);
         
        System.out.println("Intra class mean: "+intraClassMean+ " std: " +intraClassStd);
        System.out.println("Inter class: mean: "+interClassMean + " std: "+interClassStd);
        return new IteratedClassificationResult();
    }
    
    
}
