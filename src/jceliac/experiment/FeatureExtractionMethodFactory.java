/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.experiment;

import jceliac.featureExtraction.temporalFeatures.FeatureExtractionParametersTemporalStatistics;
import jceliac.featureExtraction.WTLBP.FeatureExtractionParametersWTLBP;
import jceliac.featureExtraction.SALBP.FeatureExtractionParametersScaleAndOrientationAdaptiveLBP;
import jceliac.featureExtraction.affineScaleLBPScaleBias.FeatureExtractionParametersScaleLBP;
import jceliac.featureExtraction.SALBP.FeatureExtractionParametersScaleAdaptiveLBP;
import jceliac.featureExtraction.fractal.multiFractalSpectrum.FeatureExtractionParametersMultiFractalSpectrum;
import jceliac.featureExtraction.LTP.FeatureExtractionParametersLTP;
import jceliac.featureExtraction.LBPC.FeatureExtractionParametersLBPC;
import jceliac.featureExtraction.LBP.FeatureExtractionParametersLBP;
import jceliac.featureExtraction.FLBP.FeatureExtractionParametersFLBP;
import jceliac.featureExtraction.ELTP.FeatureExtractionParametersELTP;
import jceliac.featureExtraction.ELBP.FeatureExtractionParametersELBP;
import jceliac.featureExtraction.EFLBP.FeatureExtractionParametersEFLBP;
import jceliac.featureExtraction.corticalModel.FeatureExtractionParametersCorticalModel;
import jceliac.featureExtraction.COOCLBP.FeatureExtractionParametersCOOCLBP;
import jceliac.featureExtraction.COOCLBP.FeatureExtractionParametersCOOCFLBP;
import jceliac.featureExtraction.affineScaleLBPScaleBias.FeatureExtractionParametersAffineAdaptiveLBP;
import jceliac.featureExtraction.corticalModel.FeatureExtractionParametersCorticalModel.ECorticalModelType;
import jceliac.featureExtraction.SALBP.FeatureExtractionMethodScaleAndOrientationAdaptiveLBP;
import jceliac.featureExtraction.fractal.multiFractalSpectrum.FeatureExtractionMethodMultiFractalSpectrum;
import jceliac.featureExtraction.affineScaleLBPScaleBias.FeatureExtractionMethodAffineScaleLBPScaleBias;
import jceliac.featureExtraction.affineScaleLBPScaleBias.FeatureExtractionMethodAffineAdaptiveLBP;
import jceliac.featureExtraction.SALBP.FeatureExtractionMethodScaleAdaptiveLBP;
import jceliac.featureExtraction.temporalFeatures.FeatureExtractionMethodTemporalStatistics;
import jceliac.featureExtraction.FeatureExtractionParameters;
import jceliac.featureExtraction.affineScaleLBP.FeatureExtractionMethodAffineScaleLBP;
import jceliac.featureExtraction.corticalModel.FeatureExtractionMethodCorticalModel;
import jceliac.featureExtraction.FeatureExtractionMethod.EFeatureExtractionMethod;
import jceliac.featureExtraction.COOCLBP.FeatureExtractionMethodCooccurrenceFLBP;
import jceliac.featureExtraction.COOCLBP.FeatureExtractionMethodCooccurrenceLBP;
import jceliac.featureExtraction.WTLBP.FeatureExtractionMethodWTLBP;
import jceliac.featureExtraction.EFLBP.FeatureExtractionMethodEFLBP;
import jceliac.featureExtraction.LBPC.FeatureExtractionMethodLBPC;
import jceliac.featureExtraction.FLBP.FeatureExtractionMethodFLBP;
import jceliac.featureExtraction.ELTP.FeatureExtractionMethodELTP;
import jceliac.featureExtraction.ELBP.FeatureExtractionMethodELBP;
import jceliac.featureExtraction.LTP.FeatureExtractionMethodLTP;
import jceliac.featureExtraction.LBP.FeatureExtractionMethodLBP;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.ECM.FeatureExtractionMethodECM;
import jceliac.featureExtraction.ECM.FeatureExtractionParametersECM;
import jceliac.featureExtraction.contrast.FeatureExtractionMethodContrast;
import jceliac.featureExtraction.contrast.FeatureExtractionParametersContrast;
import jceliac.featureExtraction.HOG.FeatureExtractionMethodHOG;
import jceliac.featureExtraction.HOG.FeatureExtractionParametersHOG;
import jceliac.featureExtraction.LiLBP.FeatureExtractionMethodLiLBP;
import jceliac.featureExtraction.LiLBP.FeatureExtractionParametersLiLBP;
import jceliac.featureExtraction.SIFT.FeatureExtractionMethodDenseSIFT;
import jceliac.featureExtraction.SIFT.FeatureExtractionParametersDenseSIFT;
import jceliac.featureExtraction.affineInvariantRegions.FeatureExtractionMethodAffineInvariantRegions;
import jceliac.featureExtraction.affineInvariantRegions.FeatureExtractionParametersAffineInvariantRegions;
import jceliac.featureExtraction.dominantScale.FeatureExtractionMethodDominantScale;
import jceliac.featureExtraction.dominantScale.FeatureExtractionParametersDominantScale;
import jceliac.featureExtraction.dtcwt.FeatureExtractionMethodD3TCWTCyclicShift;
import jceliac.featureExtraction.dtcwt.FeatureExtractionMethodD3TCWTLocal;
import jceliac.featureExtraction.dtcwt.FeatureExtractionMethodDTCWT;
import jceliac.featureExtraction.dtcwt.FeatureExtractionMethodDTCWTLogpolar;
import jceliac.featureExtraction.dtcwt.FeatureExtractionParametersD3TCWTCyclicShift;
import jceliac.featureExtraction.dtcwt.FeatureExtractionParametersD3TCWTLocal;
import jceliac.featureExtraction.dtcwt.FeatureExtractionParametersDTCWT;
import jceliac.featureExtraction.dtcwt.FeatureExtractionParametersDTCWTLogpolar;
import jceliac.featureExtraction.experimental.FeatureExtractionMethodScaleResponse;
import jceliac.featureExtraction.experimental.FeatureExtractionParametersScaleResponse;
import jceliac.featureExtraction.fractal.multiFractalSpectrum.FeatureExtractionMethodMultiFractalSpectrumMR8;
import jceliac.featureExtraction.fractal.multiFractalSpectrum.FeatureExtractionParametersMultiFractalSpectrumMR8;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.parameter.xml.*;

/**
 * The factory for creating objects of type FeatureExtractionMethod.
 * <p>
 * @author shegen
 */
public class FeatureExtractionMethodFactory
{

    public static FeatureExtractionMethod
            createFeatureExtractionMethod(EFeatureExtractionMethod type,
                                          FeatureExtractionParameters parameters)
            throws InitializeFailedException, JCeliacGenericException
    {
        switch(type) {
            case LBP:
                return new FeatureExtractionMethodLBP(parameters);

            case LBPC:
                return new FeatureExtractionMethodLBPC(parameters);

            case ELBP:
                return new FeatureExtractionMethodELBP(parameters);

            case LTP:
                return new FeatureExtractionMethodLTP(parameters);

            case ELTP:
                return new FeatureExtractionMethodELTP(parameters);

            case WTLBP:
                return new FeatureExtractionMethodWTLBP(parameters);

            case FLBP:
                return new FeatureExtractionMethodFLBP(parameters);

            case TEMPORAL_STATISTICS:
                return new FeatureExtractionMethodTemporalStatistics(parameters);

            case SALBP:
            case SALTP:
            case SAFLBP:
                return new FeatureExtractionMethodScaleAdaptiveLBP(parameters);

            case SOALBP:
            case SOALTP:
            case SOAFLBP:
                return new FeatureExtractionMethodScaleAdaptiveLBP(parameters);

            // those methods are still experimental and not well tested, do not use in this state!
            case AALBP:
                return new FeatureExtractionMethodAffineAdaptiveLBP(parameters);

            case EFLBP:
                return new FeatureExtractionMethodEFLBP(parameters);

            case COOCLBP:
                return new FeatureExtractionMethodCooccurrenceLBP(parameters);

            case COOCFLBP:
                return new FeatureExtractionMethodCooccurrenceFLBP(parameters);

            case AFFINESCALELBP:
                return new FeatureExtractionMethodAffineScaleLBP(parameters);

            case SCALEBIAS:
                return new FeatureExtractionMethodAffineScaleLBPScaleBias(parameters);

            case MULTI_FRACTAL_SPECTRUM:
                return new FeatureExtractionMethodMultiFractalSpectrum(parameters);
                
            case MULTI_FRACTAL_SPECTRUM_MR8:
                return new FeatureExtractionMethodMultiFractalSpectrumMR8(parameters);
           
            case SCM:
            case ICM:
                return new FeatureExtractionMethodCorticalModel(parameters);
                
            case DTCWT_CLASSIC:
                return new FeatureExtractionMethodDTCWT(parameters);
                
            case D3TCWT_LOCAL:
                return new FeatureExtractionMethodD3TCWTLocal(parameters);
            
            case D3TCWT_CYCLIC_SHIFT:
                return new FeatureExtractionMethodD3TCWTCyclicShift(parameters);
                
            case DTCWT_LOGPOLAR:
                return new FeatureExtractionMethodDTCWTLogpolar(parameters);
                
            case CONTRAST:
                return new FeatureExtractionMethodContrast(parameters);
            
            case HOG:
                return new FeatureExtractionMethodHOG(parameters);
                
            case ECM:
                return new FeatureExtractionMethodECM(parameters);
                
            case DOMINANT_SCALE:
                return new FeatureExtractionMethodDominantScale(parameters);
                
            case SCALE_RESPONSE:
                return new FeatureExtractionMethodScaleResponse(parameters);
                
            case LI_LBP:
                return new FeatureExtractionMethodLiLBP(parameters);
                
            case DSIFT:
                return new FeatureExtractionMethodDenseSIFT(parameters);
                
            case AFFINE_REGIONS:
                return new FeatureExtractionMethodAffineInvariantRegions(parameters);
                
            default:
                Experiment.log(JCeliacLogger.LOG_ERROR, "Unknown Feature Extraction Method!");
                throw new InitializeFailedException("Unknown Feature Extraction Method");
        }
    }

    public static FeatureExtractionMethod createFeatureExtractionMethod(String methodString,
                                                                        FeatureExtractionParameters parameters)
            throws Exception
    {
        EFeatureExtractionMethod method = FeatureExtractionMethodFactory.mapFeatureExtractionMethodName(methodString);
        return FeatureExtractionMethodFactory.createFeatureExtractionMethod(method, parameters); 
    }

    protected static EFeatureExtractionMethod mapFeatureExtractionMethodName(String methodString)
            throws JCeliacGenericException
    {
        return EFeatureExtractionMethod.valueOfIgnoreCase(methodString);
    }

    public static FeatureExtractionParameters createParametersByMethod(String method)
            throws XMLParseException
    {
        FeatureExtractionParameters parameters;

        if(method.equalsIgnoreCase("LBP")) {
            parameters = new FeatureExtractionParametersLBP();
            return parameters;
        }
        if(method.equalsIgnoreCase("LTP")) {
            parameters = new FeatureExtractionParametersLTP();
            return parameters;
        }
        if(method.equalsIgnoreCase("LBPC")) {
            parameters = new FeatureExtractionParametersLBPC();
            return parameters;
        }
        if(method.equalsIgnoreCase("ELBP")) {
            parameters = new FeatureExtractionParametersELBP();
            return parameters;
        }
        if(method.equalsIgnoreCase("ELTP")) {
            parameters = new FeatureExtractionParametersELTP();
            return parameters;
        }
        if(method.equalsIgnoreCase("WTLBP")) {
            parameters = new FeatureExtractionParametersWTLBP();
            return parameters;
        }
        if(method.equalsIgnoreCase("FLBP")) {
            parameters = new FeatureExtractionParametersFLBP();
            return parameters;
        }
        if(method.equalsIgnoreCase("EFLBP")) {
            parameters = new FeatureExtractionParametersEFLBP();
            return parameters;
        }
        if(method.equalsIgnoreCase("COOCLBP")) {
            parameters = new FeatureExtractionParametersCOOCLBP();
            return parameters;
        }
        if(method.equalsIgnoreCase("COOCFLBP")) {
            parameters = new FeatureExtractionParametersCOOCFLBP();
            return parameters;
        }
        if(method.equalsIgnoreCase("AFFINESCALELBP")) {
            parameters = new FeatureExtractionParametersScaleLBP();
            return parameters;
        }
        if(method.equalsIgnoreCase("TEMPORAL_STATISTICS")) {
            parameters = new FeatureExtractionParametersTemporalStatistics();
            return parameters;
        }
        if(method.equalsIgnoreCase("SCALEBIAS")) {
            parameters = new FeatureExtractionParametersLTP();
            return parameters;
        }
        if(method.equalsIgnoreCase("SALBP")) {
            parameters = new FeatureExtractionParametersScaleAdaptiveLBP();
            ((FeatureExtractionParametersScaleAdaptiveLBP) parameters).setExtractionMethod("LBP");
            return parameters;
        }
        if(method.equalsIgnoreCase("SALTP")) {
            parameters = new FeatureExtractionParametersScaleAdaptiveLBP();
            ((FeatureExtractionParametersScaleAdaptiveLBP) parameters).setExtractionMethod("LTP");
            return parameters;
        }
        if(method.equalsIgnoreCase("SAFLBP")) {
            parameters = new FeatureExtractionParametersScaleAdaptiveLBP();
            ((FeatureExtractionParametersScaleAdaptiveLBP) parameters).setExtractionMethod("FLBP");
            return parameters;
        }
        if(method.equalsIgnoreCase("AALBP")) {
            parameters = new FeatureExtractionParametersAffineAdaptiveLBP();
            return parameters;
        }
        if(method.equalsIgnoreCase("SOALBP")) {
            parameters = new FeatureExtractionParametersScaleAdaptiveLBP();
            ((FeatureExtractionParametersScaleAdaptiveLBP)parameters).setAdaptOrientation(true);
            ((FeatureExtractionParametersScaleAdaptiveLBP) parameters).setExtractionMethod("LBP");
            return parameters;
        }
        if(method.equalsIgnoreCase("SOALTP")) {
            parameters = new FeatureExtractionParametersScaleAdaptiveLBP();
            ((FeatureExtractionParametersScaleAdaptiveLBP)parameters).setAdaptOrientation(true);
            ((FeatureExtractionParametersScaleAdaptiveLBP) parameters).setExtractionMethod("LTP");
            return parameters;
        }
        if(method.equalsIgnoreCase("SOAFLBP")) {
            parameters = new FeatureExtractionParametersScaleAdaptiveLBP();
            ((FeatureExtractionParametersScaleAdaptiveLBP)parameters).setAdaptOrientation(true);
            ((FeatureExtractionParametersScaleAdaptiveLBP) parameters).setExtractionMethod("FLBP");
            return parameters;
        } 
        if(method.equalsIgnoreCase("MULTI_FRACTAL_SPECTRUM")) {
            parameters = new FeatureExtractionParametersMultiFractalSpectrum();
            return parameters;
        }
        if(method.equalsIgnoreCase("MULTI_FRACTAL_SPECTRUM_MR8")) {
            parameters = new FeatureExtractionParametersMultiFractalSpectrumMR8();
            return parameters;
        }
        if(method.equalsIgnoreCase("ICM")) {
            parameters = new FeatureExtractionParametersCorticalModel(ECorticalModelType.ICM);
            ((FeatureExtractionParametersCorticalModel)parameters).setModelType("ICM");
            return parameters;
        }
        if(method.equalsIgnoreCase("SCM")) {
            parameters = new FeatureExtractionParametersCorticalModel(ECorticalModelType.SCM);
            ((FeatureExtractionParametersCorticalModel)parameters).setModelType("SCM");
            return parameters;
        }
        if(method.equalsIgnoreCase("DTCWT_CLASSIC")) {
            parameters = new FeatureExtractionParametersDTCWT();
            return parameters;
        }
        if(method.equalsIgnoreCase("D3TCWT_LOCAL")) {
            parameters = new FeatureExtractionParametersD3TCWTLocal();
            return parameters;
        }
        if(method.equalsIgnoreCase("D3TCWT_CYCLIC_SHIFT")) {
            parameters = new FeatureExtractionParametersD3TCWTCyclicShift();
            return parameters;
        }
        if(method.equalsIgnoreCase("DTCWT_LOGPOLAR")) {
            parameters = new FeatureExtractionParametersDTCWTLogpolar();
            return parameters;
        }
        if(method.equalsIgnoreCase("CONTRAST")) {
            parameters = new FeatureExtractionParametersContrast();
            return parameters;
        }
        if(method.equalsIgnoreCase("HOG")) {
            parameters = new FeatureExtractionParametersHOG();
            return parameters;
        }
        if(method.equalsIgnoreCase("ECM")) {
            parameters = new FeatureExtractionParametersECM();
            return parameters;
        }
        if(method.equalsIgnoreCase("DOMINANT_SCALE")) {
            parameters = new FeatureExtractionParametersDominantScale();
            return parameters;
        }
        if(method.equalsIgnoreCase("SCALE_RESPONSE")) {
            parameters = new FeatureExtractionParametersScaleResponse();
            return parameters;
        }
        if(method.equalsIgnoreCase("LI_LBP")) {
            parameters = new FeatureExtractionParametersLiLBP();
            return parameters;
        }
        if(method.equalsIgnoreCase("DSIFT")) {
            parameters = new FeatureExtractionParametersDenseSIFT();
            return parameters;
        }
        if(method.equalsIgnoreCase("AFFINE_REGIONS")) {
            parameters = new FeatureExtractionParametersAffineInvariantRegions();
            return parameters;
        }
        else {
            throw new XMLParseException("Feature Extraction Method (" + method + ") not supported!");
        }

    }

}
