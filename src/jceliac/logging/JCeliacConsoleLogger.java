/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.logging;

/**
 *
 * @author shegen
 */
public class JCeliacConsoleLogger extends JCeliacLogger {

    @Override
    protected void doLogMessage(String format, Object... args) {
        // Console logger logs the output on the console, WOW!
        System.out.printf(format, args);
    }

    @Override
    protected void doLogMessage(String message, Exception ex) {
        System.out.println(message);
        ex.printStackTrace();
    }
    public JCeliacConsoleLogger() {
        super();
    }
}
