/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.logging;

import java.io.*;
import jceliac.JCeliacGenericException;

/**
 *
 * @author shegen
 */
public class JCeliacFileLogger
        extends JCeliacLogger
{

    private String testname;
    private BufferedWriter writer;

    public JCeliacFileLogger(String testname)
    {
        try {
            this.testname = testname;
            FileWriter fw = new FileWriter(this.testname + ".log", true);
            this.writer = new BufferedWriter(fw);
        } catch(IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected synchronized void doLogMessage(String message, Object... args)
    {
        try {
            String formattedString = String.format(message, args);
            this.writer.write(formattedString);
            this.writer.flush();
        } catch(Exception e) {
        }
    }

    @Override
    protected void doLogMessage(String message, Exception ex)
    {
        try {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            String stackTrace = sw.toString();
            this.writer.write(message);
            this.writer.write(stackTrace);
            this.writer.flush();
        } catch(IOException e) {
            
        }
    }

    @Override
    protected void finalize() throws Throwable
    {
        this.writer.close();
        super.finalize();
    }
}
