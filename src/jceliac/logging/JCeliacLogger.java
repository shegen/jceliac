/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.logging;

import java.util.*;
import java.text.*;
import java.net.*;
import jceliac.*;

/**
 *
 * @author shegen
 */
public abstract class JCeliacLogger
{

    public final static int LOG_ERROR = 1; // errors
    public final static int LOG_INFO = (1 << 2); // info
    public final static int LOG_HINFO = (1 << 3); // heavy info
    public final static int LOG_DEBUG = (1 << 4); // debug output
    public final static int LOG_HDEBUG = (1 << 5); // heavy debugging
    public final static int LOG_HHDEBUG = (1 << 6); // ultra heavy debugging, use this with care!
    public final static int LOG_RESULT = (1 << 7);  // results, this should always be on
    public final static int LOG_WARN = (1 << 8);

    protected int currentLogMask = (LOG_ERROR | LOG_RESULT | LOG_INFO | LOG_WARN); // default

    public JCeliacLogger()
    {
        //  printLogLevel();
    }

    protected void printLogLevel()
    {
        String logMask = "";
        if((this.currentLogMask & LOG_ERROR) != 0) {
            logMask += "LOG_ERROR";
        }
        if((this.currentLogMask & LOG_INFO) != 0) {
            logMask += "|LOG_INFO";
        }
        if((this.currentLogMask & LOG_HINFO) != 0) {
            logMask += "|LOG_HINFO";
        }
        if((this.currentLogMask & LOG_DEBUG) != 0) {
            logMask += "|LOG_DEBUG";
        }
        if((this.currentLogMask & LOG_HDEBUG) != 0) {
            logMask += "|LOG_HDEBUG";
        }
        if((this.currentLogMask & LOG_HHDEBUG) != 0) {
            logMask += "|LOG_HHDEBUG";
        }
        if((this.currentLogMask & LOG_RESULT) != 0) {
            logMask += "|LOG_RESULT";
        }
        if((this.currentLogMask & LOG_WARN) != 0) {
            logMask += "|LOG_WARN";
        }

        logMessage("Current Log Mask set to: " + "(" + logMask + ")", LOG_INFO);
    }

    public void addLogLevel(int level)
    {
        this.currentLogMask |= level;
        printLogLevel();
    }

    public void removeLogLevel(int level)
    {
        if((level & LOG_ERROR) != 0) {
            logMessage("Log Level LOG_ERROR can not be removed!", LOG_ERROR);
            return;
        }
        if((level & LOG_RESULT) != 0) {
            logMessage("Log Level LOG_RESULT can not be removed!", LOG_ERROR);
            return;
        }
        this.currentLogMask &= ~level;
        printLogLevel();
    }

    protected abstract void doLogMessage(String message, Object... args);

    protected abstract void doLogMessage(String message, Exception ex);

    public synchronized void logMessage(String format, int level, Object... args) 
    {
        try {
            String.format(format, args);
        } catch(IllegalFormatException | NullPointerException e)  {
            this.logMessage("Invalid logger-string identified: \"%s\"\n", LOG_ERROR,  format);
            return;
        }
        String checkedFormat = format;
        if((this.currentLogMask & level) != 0) {
            String prefix;
            switch(level) {
                case LOG_ERROR:
                    prefix = "ERROR: ";
                    break;

                case LOG_INFO:
                    prefix = "INFO: ";
                    break;

                case LOG_HINFO:
                    prefix = "HINFO: ";
                    break;

                case LOG_DEBUG:
                    prefix = "DEBUG: ";
                    break;

                case LOG_HDEBUG:
                    prefix = "HDEBUG: ";
                    break;

                case LOG_HHDEBUG:
                    prefix = "HHDEBUG: ";
                    break;

                case LOG_RESULT:
                    prefix = "RESULT: ";
                    break;

                case LOG_WARN:
                    prefix = "WARN: ";
                    break;

                default:
                    prefix = "";
                    break;

            }
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String timestamp = fmt.format(cal.getTime());
            String hostname = getHostname();
            String callingMethod = new Throwable().fillInStackTrace().getStackTrace()[2].getMethodName();
            String callingClass = new Throwable().fillInStackTrace().getStackTrace()[2].getClassName();

            if(level == LOG_ERROR) {
                this.doLogMessage("[" + hostname + ": " + timestamp + "] " + callingClass + "." + callingMethod + "():" + " " + prefix + checkedFormat + "\n", args);
            } else {
                this.doLogMessage("[" + hostname + ": " + timestamp + "] " + prefix + checkedFormat + "\n", args);
            }
        }
    }

    public void logException(JCeliacLogableException ex, int level)
    {
        if((this.currentLogMask & level) != 0) {
            String prefix;
            switch(level) {
                case LOG_ERROR:
                    prefix = "ERROR: ";
                    break;

                case LOG_INFO:
                    prefix = "INFO: ";
                    break;

                case LOG_HINFO:
                    prefix = "HINFO: ";
                    break;

                case LOG_DEBUG:
                    prefix = "DEBUG: ";
                    break;

                case LOG_HDEBUG:
                    prefix = "HDEBUG: ";
                    break;

                case LOG_HHDEBUG:
                    prefix = "HHDEBUG: ";
                    break;

                case LOG_RESULT:
                    prefix = "RESULT: ";
                    break;
                    
                case LOG_WARN:
                    prefix = "WARN: ";
                    break;
                    
                default:
                    prefix = "";
                    break;

            }
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String timestamp = fmt.format(cal.getTime());
            String hostname = getHostname();

            this.doLogMessage("[" + hostname + ": " + timestamp + "] " + prefix + ex.getMessage() + "\n");
        }
    }

    public void logException(Exception ex, int level)
    {
        if((this.currentLogMask & level) != 0) {
            String prefix;
            switch(level) {
                case LOG_ERROR:
                    prefix = "ERROR: ";
                    break;

                case LOG_INFO:
                    prefix = "INFO: ";
                    break;

                case LOG_HINFO:
                    prefix = "HINFO: ";
                    break;

                case LOG_DEBUG:
                    prefix = "DEBUG: ";
                    break;

                case LOG_HDEBUG:
                    prefix = "HDEBUG: ";
                    break;

                case LOG_HHDEBUG:
                    prefix = "HHDEBUG: ";
                    break;

                case LOG_RESULT:
                    prefix = "RESULT: ";
                    break;

                case LOG_WARN:
                    prefix = "WARN: ";
                    break;
                    
                default:
                    prefix = "";
                    break;

            }
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String timestamp = fmt.format(cal.getTime());
            String hostname = getHostname();

            this.doLogMessage("[" + hostname + ": " + timestamp + "] " + prefix, ex);
        }
    }

    private String getHostname()
    {
        try {
            InetAddress addr = InetAddress.getLocalHost();
            String hostname = addr.getHostName();
            return hostname;
        } catch(UnknownHostException e) {
            return "";
        }
    }
}
