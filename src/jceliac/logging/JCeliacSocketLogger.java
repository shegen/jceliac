/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.logging;

import java.net.InetSocketAddress;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketAddress;
import java.io.StringWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Logs the output to a UDP socket.
 * <p>
 * @author shegen
 */
public class JCeliacSocketLogger
        extends JCeliacLogger
{

    public static final int LOGGER_PORT = 4444;
    public static final int PROGRESS_PORT = 4445;
    private final DatagramSocket serverSocket;

    public JCeliacSocketLogger()
            throws IOException
    {
        this.serverSocket = new DatagramSocket();

    }

    private void write(String message, int port)
    {
        try {
            SocketAddress socketAddr = new InetSocketAddress("localhost", port);
            byte[] byteMessage = message.getBytes();
            DatagramPacket datagram = new DatagramPacket(byteMessage, byteMessage.length, socketAddr);
            this.serverSocket.send(datagram);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doLogMessage(String message, Object... args)
    {
        try {
            String formattedString = String.format(message, args);
            this.write(formattedString, LOGGER_PORT);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doLogMessage(String message, Exception ex)
    {
        try {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            String stackTrace = sw.toString();
            this.write(message, LOGGER_PORT);
            this.write(stackTrace, LOGGER_PORT);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void writeProgress(String message)
    {
        try {
            this.write(message, PROGRESS_PORT);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}
