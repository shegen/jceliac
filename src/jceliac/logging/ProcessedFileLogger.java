/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.logging;

import java.io.*;
import java.util.*;

/**
 *
 * @author shegen
 */
public class ProcessedFileLogger
{

    private final BufferedWriter writer;
    private final ArrayList<String> processedFiles;

    public ProcessedFileLogger(String filename) throws IOException
    {
        this.writer = new BufferedWriter(new FileWriter(filename, true));
        try(BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            this.processedFiles = new ArrayList<>();

            String nextLine;
            while((nextLine = reader.readLine()) != null) {
                this.processedFiles.add(nextLine);
            }
        }
    }

    public boolean wasProcessed(String filename)
    {
        for(int i = 0; i < this.processedFiles.size(); i++) {
            String tmp = this.processedFiles.get(i);
            if(tmp.equals(filename)) {
                return true;
            }
        }
        return false;
    }

    public void logProcessed(String filename)
    {
        try {
            this.processedFiles.add(filename);
            this.writer.write(filename);
            this.writer.newLine();
            this.writer.flush();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public void cleanup() throws IOException
    {
        this.writer.close();
    }
}
