/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac;

import java.lang.reflect.*;
import jceliac.logging.JCeliacLogger;

/**
 * Implements the basic reflection mechanism used within all parameter type
 * subclasses.
 * <p>
 * @author shegen
 */
public abstract class CloneableReflectableParameters
        implements Cloneable
{

    @Override
    public Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }

    /**
     * Checks by using reflection if the corresponding Parameter object has the
     * name of a certain field.
     * <p>
     * @param name the name of the field to check
     * <p>
     * @return boolean indicating if field was found
     */
    public boolean hasOption(String name)
    {
        try {
            
            Class c = this.getClass();
            Field[] fields = c.getDeclaredFields();

            for(Field field : fields) {
                if(name.equalsIgnoreCase(field.getName())) {
                    return true;
                }
            }
            return superClassHasOption(name);
        } catch(SecurityException e) {
            Main.log(JCeliacLogger.LOG_WARN, "Could not query parameter field: %s", e.getMessage());
        }
        return false;
    }

    /**
     * Checks if the superclass has the specific option. This is used if
     * parameters are shared between methods based no a superclass.
     * <p>
     * @param name the name of the option
     * <p>
     * @return boolean indicating if the option exists in the superclass
     */
    private boolean superClassHasOption(String name)
    {
        try {

            Class c = this.getClass();
            Field[] fields = c.getSuperclass().getDeclaredFields();
            for(Field field : fields) {
                if(name.equalsIgnoreCase(field.getName())) {
                    return true;
                }
            }
        } catch(SecurityException e) {
            Main.log(JCeliacLogger.LOG_WARN, "Could not query superclass parameter field: %s", e.getMessage());
        }
        return false;

    }

    /**
     * Set the field name to value.
     * <p>
     * @param name  the name of the field to set
     * @param value the value to set the field to
     */
    public void setOption(String name, Object value)
    {
        if(name == null || name.length() == 0) {
            return;
        }

        try {

            Class c = this.getClass();
            Field[] fields = c.getDeclaredFields();

            for(Field field : fields) {
                /* We do not use getter or setter methods as we can access
                 * our files in here!
                 */
                if(name.equalsIgnoreCase(field.getName())) {
                    setOptionByType(field, value);
                    return;
                }
            }
            superClassSetOption(name, value);
        } catch(Exception e) {
            Main.log(JCeliacLogger.LOG_WARN, "Could not set parameter field: %s", e.getMessage());
        }
    }

    /**
     * Set the field name of the superclass to value.
     * <p>
     * @param name  the name of the field to set
     * @param value the value to set the field to
     */
    public void superClassSetOption(String name, Object value)
    {
        if(name == null || name.length() == 0) {
            return;
        }

        try {

            Class c = this.getClass();
            Field[] fields = c.getSuperclass().getDeclaredFields();
            for(Field field : fields) {
                // we do not use getter or setter methods as we can access
                // our files in here!
                if(name.equalsIgnoreCase(field.getName())) {
                    setOptionByType(field, value);
                    break;
                }
            }
            
        } catch(Exception e) {
            Main.log(JCeliacLogger.LOG_WARN, "Could not set superclass parameter field: %s", e.getMessage());
        }
    }

    /**
     * This method is actually used to set the fields to the specific type. The
     * method converts the strings "true" and "false" to boolean.
     * <p>
     * @param field  the name of the field to set
     * @param value the value to set the field to
     * 
     * @throws Exception 
     */
    protected void setOptionByType(Field field, Object value)
            throws Exception
    {
        Object type = field.getType();

        if(type.equals(Boolean.TYPE)) {
            boolean bValue;
            String v = (String) value;
            switch(v) {
                case "true":
                    bValue = true;
                    break;
                case "false":
                    bValue = false;
                    break;
                default:
                    throw new Exception("Boolean value neither true or false!");
            }

            field.set(this, bValue);
        } else if(type.equals(Integer.TYPE)) {
            int iValue;
            try {
                iValue = Integer.parseInt((String) value);
            } catch(NumberFormatException e) // this handles double implicitly
            {
                double dValue = Double.parseDouble((String) value);
                iValue = (int) dValue;
            }
            field.set(this, iValue);
        } else if(type.equals(Double.TYPE)) {
            double dValue = Double.parseDouble((String) value);
            field.set(this, dValue);
        } else {
            field.set(this, value);
        }
    }

}
