/*
 *  JCeliac Copyright (C) 2010-2011 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac;

import jceliac.experiment.distributed.DistributedMainExperiment;
import com.jcraft.jsch.JSchException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import jceliac.experiment.distributed.DistributedExperimentServer;
import jceliac.experiment.distributed.DistributedExperimentClient;
import jceliac.experiment.Experiment.ExperimentState;
import jceliac.tools.parameter.xml.*;
import jceliac.experiment.*;
import jceliac.logging.*;
import java.util.*;

/**
 * Main class of JCeliac.
 * <p>
 * @author shegen
 */
public class Main
{

    public Main()
    {
    }
    public static JCeliacLogger logger;
    public static JCeliacLogger fileLogger;
    public static JCeliacSocketLogger socketLogger;

    public static void log(int level, String format, Object... args)
    {
        Main.logger.logMessage(format, level, args);
        if(Main.fileLogger != null) {
            Main.fileLogger.logMessage(format, level, args);
        }
        if(Main.socketLogger != null) {
            Main.socketLogger.logMessage(format, level, args);
        }
    }

    public static void logException(int level, JCeliacLogableException ex)
    {
        Main.logger.logException(ex, level);
        if(Main.fileLogger != null) {
            Main.fileLogger.logException(ex, level);
        }
        if(Main.socketLogger != null) {
            Main.socketLogger.logException(ex, level);
        }
    }

    public static void logException(int level, Exception ex)
    {
        Main.logger.logException(ex, level);
        if(Main.fileLogger != null) {
            Main.fileLogger.logException(ex, level);
        }
        if(Main.socketLogger != null) {
            Main.socketLogger.logException(ex, level);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        if(args.length == 2 && args[0].equals("-rmi-client")) {
            Main.logger = new JCeliacConsoleLogger();
          
            try {
                // args[1] is the ip of the rmi-server 
                DistributedExperimentClient rmiClient = new DistributedExperimentClient(args[1]);
                rmiClient.initialize();

                while(true) {
                    DistributedMainExperiment distributedExperiment = rmiClient.requestExperiment();
                    if(distributedExperiment == null) {
                        break;
                    }
                    rmiClient.runExperiment(distributedExperiment);
                }

            } catch(Exception e) {
                Main.log(JCeliacLogger.LOG_ERROR, "Experiment Failed: ", e.getLocalizedMessage());
                Main.logException(JCeliacLogger.LOG_ERROR, e);
            }
        } else {

            Main.logger = new JCeliacConsoleLogger();
            ExperimentState state = ExperimentState.SUCCESS;
            ArrayList<MainExperiment> mainExperiments;

            try {
                GlobalTestConfigurationParameters globalParameters
                                                  = ExperimentFactory.parseExperimentConfigurations(args);
                mainExperiments = ExperimentFactory.createExperiments(globalParameters);

                while(mainExperiments.size() > 0) {
                    // always pick first experiment in list, because we just removed the prior experiment
                    MainExperiment currentMainExperiment = mainExperiments.get(0);
                    if(globalParameters.isDistributeExperiments()) {
                        Main.runDistributedExperiment(mainExperiments, globalParameters);
                        mainExperiments.clear();
                    } else {
                        int subExpCount = currentMainExperiment.getSubExperimentCount();
                        if(subExpCount > 0) {
                            // a new main experiment is started, add a database table
                            fileLogger = new JCeliacFileLogger(currentMainExperiment.getExperimentName());
                            Experiment exp = null;
                            try {
                                currentMainExperiment.initializeMainExperiment();
                                currentMainExperiment.runMainExperiment();
                                currentMainExperiment.cleanupMainExperiment();
                            }  catch(InitializeFailedException e) {
                                Main.log(JCeliacLogger.LOG_ERROR, "Initialization failed: (%s)", e.getLocalizedMessage());
                                Main.log(JCeliacLogger.LOG_ERROR, "Skipping Sub-Experiments for \"%s\"", currentMainExperiment.getExperimentName());

                                if(exp != null) {
                                    exp.cleanupExperiment(ExperimentState.FAILED);
                                }
                                currentMainExperiment.cleanupMainExperiment();
                            } catch(Exception e) {
                                Main.log(JCeliacLogger.LOG_ERROR, e.getMessage());
                                Main.logException(JCeliacLogger.LOG_ERROR, e);
                                if(exp != null) {
                                    exp.cleanupExperiment(ExperimentState.FAILED);
                                }
                                currentMainExperiment.cleanupMainExperiment();
                            } finally {
                                // free up references of main experiment
                                mainExperiments.remove(currentMainExperiment);
                            }
                        }
                    }
                }
            } catch(InvalidInputException e) {
                state = ExperimentState.FAILED;
                Main.logException(JCeliacLogger.LOG_ERROR, e);
            } catch(XMLParseException e) {
                state = ExperimentState.FAILED;
                Main.log(JCeliacLogger.LOG_ERROR, "Experiment initialization failed!");
            } catch(JCeliacLogableException e) {
                Main.logException(JCeliacLogger.LOG_ERROR, e);
                Main.log(JCeliacLogger.LOG_ERROR, "Experiment Initialization: %s", e.getMessage());
            } catch(Exception e) {
                Main.log(JCeliacLogger.LOG_ERROR, "Unknown Problem during Initialization: %s", e.getMessage());
                Main.logException(JCeliacLogger.LOG_ERROR, e);
            }
        }
    }

    private static void runDistributedExperiment(ArrayList<MainExperiment> experiments,
                                                 GlobalTestConfigurationParameters globalParameters)
            throws Exception
    {
        Main.logger = new JCeliacConsoleLogger();
        Main.socketLogger = new JCeliacSocketLogger();
        Main.fileLogger = new JCeliacFileLogger("Distributed-Experiment-Debug.log");
        
        
        DistributedExperimentServer server = null;
        try {
            server = new DistributedExperimentServer(globalParameters, experiments);
            server.runExperiments();
        } catch(JSchException | InterruptedException | MalformedURLException | 
                RemoteException | JCeliacGenericException e) {
            throw e;
        } finally {
            if(server != null) {
                server.cleanup();
            }
        }

    }
}
