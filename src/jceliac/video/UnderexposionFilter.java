/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video;

import java.util.ArrayList;
import jceliac.tools.data.Frame;

/**
 * Filters under-exposed frames.
 * <p>
 * @author shegen
 */
public class UnderexposionFilter
        extends FrameFilter
{

    @Override
    public void filter(ArrayList<Frame> sequence)
    {
        ArrayList<Frame> pruneList = new ArrayList<>();

        for(Frame frame : sequence) {
            boolean accept = calculateHistogram(0, frame.getWidth(), 0, frame.getHeight(), frame.getGrayData());
            if(accept == false) {
                pruneList.add(frame);
            }
        }
        for(Frame frame : pruneList) {
            sequence.remove(frame);
        }
    }

    /**
     * Calculates histogram of image region to decide if the image is
     * under-exposed.
     * <p>
     * @param startX      Start position, x.
     * @param endX        End position, x.
     * @param startY      Start position, y.
     * @param endY        End position, y.
     * @param imageRegion Raw image data.
     * <p>
     * @return True if image is under-exposed, false else.
     */
    private boolean calculateHistogram(int startX, int endX, int startY, int endY, double[][] imageRegion)
    {
        int pixelCount = (endX - startX) * (endY - startY);
        int blackCount = 0;

        int maxBlackCount = (int) ((double) pixelCount * 0.05);

        for(int i = startX; i < endX; i++) {
            for(int j = startY; j < endY; j++) {
                int value = (int) Math.round(imageRegion[i][j]);
                if(value <= 10) {
                    blackCount++;
                    if(blackCount > maxBlackCount) {
                        return false;
                    }
                }

            }
        }
        return true;
    }
}
