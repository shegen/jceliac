/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video;

import java.awt.image.BufferedImage;
import jceliac.tools.data.Frame;
import java.awt.image.Raster;
import java.util.ArrayList;

/**
 * This cuts out parts of the image that do not contain black borders or meta
 * information such as Schrift.
 * <p>
 * @author shegen
 */
public class CropFilter
        extends FrameFilter
{

    private final int WINDOW_SIZE = 400;
    private final int BIG_ENDO_X = 190;
    private final int BIG_ENDO_Y = 85;
    private final int SMALL_ENDO_X = 230;
    private final int SMALL_ENDO_Y = 75;
    private final int TEST_X = 130;
    private final int TEST_Y = 350;
    private final int TEST_WINDOW_SIZE = 50;
    private final int ENDO_TYPE_BIG = 1;
    private final int ENDO_TYPE_SMALL = 2;

    @Override
    public void filter(ArrayList<Frame> sequence)
    {
        for(Frame f : sequence) {
            int endoType = getEndoType(f);
            switch(endoType) {
                case ENDO_TYPE_BIG:
                    f.crop(BIG_ENDO_X, BIG_ENDO_Y, WINDOW_SIZE);
                    break;

                case ENDO_TYPE_SMALL:
                    f.crop(SMALL_ENDO_X, SMALL_ENDO_Y, WINDOW_SIZE);
            }
        }
    }

    protected int getEndoType(Frame frame)
    {
        double[][] grayData = frame.getGrayData();

        // the small endo has only black pixels in this region
        for(int i = TEST_X; i < TEST_X + TEST_WINDOW_SIZE; i++) {
            for(int j = TEST_Y; j < TEST_Y + TEST_WINDOW_SIZE; j++) {
                if(grayData[i][j] != 255) {
                    return ENDO_TYPE_BIG;
                }
            }
        }
        return ENDO_TYPE_SMALL;
    }

    protected int getEndoType(BufferedImage image)
    {

        // the small endo has only black pixels in this region
        for(int i = TEST_X; i < TEST_X + TEST_WINDOW_SIZE; i++) {
            for(int j = TEST_Y; j < TEST_Y + TEST_WINDOW_SIZE; j++) {
                if(image.getRGB(i, j) != 255) {
                    return ENDO_TYPE_BIG;
                }
            }
        }
        return ENDO_TYPE_SMALL;
    }

    public double[] crop1D(BufferedImage image, int colorChannel)
    {
        int endoType = this.getEndoType(image);
        Raster raster;

        switch(endoType) {
            case ENDO_TYPE_BIG:
                raster = image.getData();
                return raster.getSamples(BIG_ENDO_X, BIG_ENDO_Y, WINDOW_SIZE, WINDOW_SIZE, colorChannel, (double[]) null);

            default:
                raster = image.getData();
                return raster.getSamples(SMALL_ENDO_X, SMALL_ENDO_Y, WINDOW_SIZE, WINDOW_SIZE, colorChannel, (double[]) null);
        }
    }
}
