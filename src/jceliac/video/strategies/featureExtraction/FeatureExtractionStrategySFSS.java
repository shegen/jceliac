/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies.featureExtraction;

import jceliac.video.strategies.AbstractFeatureExtractionStrategy.EFeatureExtractionStrategy;
import jceliac.featureExtraction.FeatureExtractionMethod;
import jceliac.CloneableReflectableParameters;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.JCeliacGenericException;
import jceliac.video.strategyHandler.*;
import jceliac.video.strategies.*;
import jceliac.tools.data.*;
import java.util.ArrayList;

/**
 *
 * @author shegen
 */
public class FeatureExtractionStrategySFSS
        extends AbstractFeatureExtractionStrategy
{

    public FeatureExtractionStrategySFSS(CloneableReflectableParameters parameters,
                                         FeatureExtractionMethod featureExtractionMethod)
    {
        super(EFeatureExtractionStrategy.SeparateFramesSeparateSegments, parameters, featureExtractionMethod);
    }

    /**
     * Extracts features from all frames and all segments separately.
     * <p>
     * @param segmentedFrames Segmented frames to extract features from.
     * <p>
     * @return List of extracted features.
     * <p>
     * @throws JCeliacGenericException
     */
    @Override
    protected ArrayList<DiscriminativeFeatures> doPerform(ArrayList<SegmentedFrame> segmentedFrames)
            throws JCeliacGenericException
    {
        ArrayList<DiscriminativeFeatures> separateFeatures = new ArrayList<>();

        for(SegmentedFrame currentSegmentedFrame : segmentedFrames) {
            // loop through all segments
            while(currentSegmentedFrame.hasNext()) {
                Frame segment = currentSegmentedFrame.next();
                DiscriminativeFeatures currentFeatures = this.featureExtractionMethod.extractFeatures(segment);
                separateFeatures.add(currentFeatures);
            }
        }
        return separateFeatures;
    }

    @Override
    public Object mapStrategyNameToEnumType(String name)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
