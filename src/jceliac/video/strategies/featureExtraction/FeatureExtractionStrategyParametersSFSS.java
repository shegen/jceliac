/*
 *  JCeliac Copyright (C) 2010-204 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies.featureExtraction;

import jceliac.video.strategies.AbstractFeatureExtractionStrategy.EFeatureExtractionStrategy;
import jceliac.video.strategies.parameters.*;

/**
 * Parameters for Separate Frames Separate Segments (SFSS) feature extraction
 * strategy.
 * <p>
 * @author shegen
 */
public class FeatureExtractionStrategyParametersSFSS
        extends FeatureExtractionStrategyParameters
{

    public FeatureExtractionStrategyParametersSFSS()
    {
        super(EFeatureExtractionStrategy.SeparateFramesSeparateSegments);
    }

    public FeatureExtractionStrategyParametersSFSS(EFeatureExtractionStrategy strategy)
    {
        super(strategy);
    }

}
