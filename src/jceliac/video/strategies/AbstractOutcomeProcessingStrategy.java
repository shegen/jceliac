/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies;

import jceliac.CloneableReflectableParameters;
import jceliac.JCeliacGenericException;
import java.util.*;


/**
 * This class does something with a combined outcome!
 *
 * @author shegen
 */
public abstract class AbstractOutcomeProcessingStrategy extends AbstractStrategy {

    public static enum EOutcomeProcessingStrategy {

        DisplayFeatures,
        Null,
        Default
    }

    protected EOutcomeProcessingStrategy strategy;

    public AbstractOutcomeProcessingStrategy(EOutcomeProcessingStrategy strategy, CloneableReflectableParameters parameters) {
        super(AbstractStrategy.EStrategyType.OUTCOME_PROCESSING_STRATEGY, parameters);
        this.strategy = strategy;
    }

    @Override
    public Object mapStrategyNameToEnumType(String name) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object perform(Object o) throws JCeliacGenericException {
        doPerform((ArrayList<?>) o);
        return null;
    }

    protected abstract void doPerform(ArrayList<?> o) throws JCeliacGenericException;

    public EOutcomeProcessingStrategy getStrategy() {
        return strategy;
    }

}
