/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies.outcomeCombination;

import java.util.*;

/**
 * The standard combined outcome created by the outcome combination strategies.
 * <p>
 * @author shegen
 */
public class CombinedOutcome
{

    private final String signalIdentifier;
    private final HashMap<Integer, Integer> classCounts;
    private final int estimatedClass;
    private double certainty = 1;

    public CombinedOutcome(String signalIdentifier, HashMap<Integer, Integer> classCounts, int estimatedClass)
    {
        this.signalIdentifier = signalIdentifier;
        this.classCounts = classCounts;
        this.estimatedClass = estimatedClass;
    }

    public HashMap<Integer, Integer> getClassCounts()
    {
        return classCounts;
    }

    public int getEstimatedClass()
    {
        return estimatedClass;
    }

    public String getSignalIdentifier()
    {
        return signalIdentifier;
    }

    public double getCertainty()
    {
        return certainty;
    }

    public void setCertainty(double certainty)
    {
        this.certainty = certainty;
    }

}
