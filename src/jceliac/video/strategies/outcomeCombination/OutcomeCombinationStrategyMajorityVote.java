/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies.outcomeCombination;

import jceliac.video.strategies.AbstractOutcomeCombinationStrategy.EOutcomeCombinationStrategy;
import jceliac.classification.ClassificationOutcome;
import jceliac.CloneableReflectableParameters;
import jceliac.JCeliacGenericException;
import jceliac.video.strategyHandler.*;
import jceliac.video.strategies.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.*;


/**
 *
 * @author shegen
 */
public class OutcomeCombinationStrategyMajorityVote
        extends AbstractOutcomeCombinationStrategy
{

    public OutcomeCombinationStrategyMajorityVote(CloneableReflectableParameters parameters)
    {
        super(EOutcomeCombinationStrategy.MajorityVote, parameters);
    }

    @ArgumentType(typeIdentifiers = {"ClassificationOutcome"})
    @ReturnType(typeIdentifier = "CombinedOutcome")
    @Override
    protected Object doPerform(ArrayList<?> o) throws JCeliacGenericException
    {
        @SuppressWarnings("unchecked")
        ArrayList<ClassificationOutcome> outcomes = (ArrayList<ClassificationOutcome>) o;
        HashMap<Integer, Integer> classCounts = new HashMap<>();

        for(ClassificationOutcome tmp : outcomes) {
            Integer classID = tmp.getEstimatedClass();
            if(classCounts.containsKey(classID)) {
                Integer count = classCounts.get(classID);
                count = count + 1;
                classCounts.put(classID, count);
            } else {
                classCounts.put(classID, 1);
            }
        }
        // find maximum 
        Integer maxClassID = -1;
        int maxClassCount = 0;
        boolean tied = false;

        Set<Map.Entry<Integer, Integer>> entries = classCounts.entrySet();
        for(Map.Entry tmp : entries) {
            Integer classID = (Integer) tmp.getKey();
            Integer classCount = (Integer) tmp.getValue();
            if(classCount == maxClassCount) {
                tied = true;
                maxClassID = classID;
                maxClassCount = classCount;
            } else if(classCount > maxClassCount) {
                maxClassID = classID;
                maxClassCount = classCount;
            }
        }
        // calculates certainty    
        int sum = 0;
        double certainty = 0;
        if(tied == false) {
            for(Map.Entry tmp : entries) {
                Integer key = (Integer) tmp.getKey();
                Integer value = (Integer) tmp.getValue();
                if(key == maxClassID) {
                    certainty = (double) value / outcomes.size();
                    break;
                }
            }
        }
        CombinedOutcome combinedOutcome = new CombinedOutcome(outcomes.get(0).getFeatures().getSignalIdentifier(),
                                                              classCounts, maxClassID);
        combinedOutcome.setCertainty(certainty);
        return combinedOutcome;
    }

    @Override
    public Object mapStrategyNameToEnumType(String name)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
