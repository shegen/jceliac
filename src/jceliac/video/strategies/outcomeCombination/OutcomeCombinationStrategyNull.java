/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies.outcomeCombination;

import jceliac.CloneableReflectableParameters;
import jceliac.JCeliacGenericException;
import jceliac.video.strategyHandler.*;
import jceliac.video.strategies.*;
import java.util.ArrayList;

/**
 * Does not combine outcomes.
 * <p>
 * @author shegen
 */
public class OutcomeCombinationStrategyNull
        extends AbstractOutcomeCombinationStrategy
{

    public OutcomeCombinationStrategyNull(CloneableReflectableParameters parameters)
    {
        super(EOutcomeCombinationStrategy.Null, parameters);
    }

    @ReturnType(typeIdentifier = "Input")
    @ArgumentType(typeIdentifiers = {"Any"})
    @Override
    protected Object doPerform(ArrayList<?> o) throws JCeliacGenericException
    {
        return o;
    }

    @Override
    public Object mapStrategyNameToEnumType(String name)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
