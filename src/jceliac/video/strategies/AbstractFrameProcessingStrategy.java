/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies;

import jceliac.video.strategies.AbstractStrategy.*;
import jceliac.tools.data.*;
import jceliac.video.*;

import jceliac.*;

/**
 * The frame processing strategy how frames are extracted form a frame sequence.
 * Single frames, or building the mean over a number of frames are possible
 * strategies.
 * <p>
 * The input to the frame processing strategy is a movie content stream. All
 * strategies then return a frame sequence.
 * <p>
 * @author shegen
 */
public abstract class AbstractFrameProcessingStrategy
        extends AbstractStrategy
{

    public static enum EFrameProcessingStrategy
    {

        Downsample,
        Default
    }

    protected EFrameProcessingStrategy strategy;

    public AbstractFrameProcessingStrategy(EFrameProcessingStrategy strategy, CloneableReflectableParameters parameters)
    {
        super(AbstractStrategy.EStrategyType.FRAME_PROCESSING_STRATEGY, parameters);
        this.strategy = strategy;
    }

    /*
     * We supply the content stream to the segmentation strategy. Some
     * strategies might use a sequence of frames to create a lower number of
     * average frames for example.
     */
    @Override
    public Object perform(Object o) throws JCeliacGenericException
    {
        return doPerform((VideoContentStream) o);
    }

    /* The FrameProcessing strategy picks up a content Stream and generates the
     * next FrameSequence that is used by the other strategies.
     */
    protected abstract FrameSequence doPerform(VideoContentStream stream) throws
            JCeliacGenericException;

    public EFrameProcessingStrategy getStrategy()
    {
        return this.strategy;
    }
}
