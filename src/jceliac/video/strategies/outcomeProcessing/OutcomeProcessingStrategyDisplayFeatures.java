/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies.outcomeProcessing;

import java.util.ArrayList;
import jceliac.CloneableReflectableParameters;
import jceliac.JCeliacGenericException;
import jceliac.video.strategies.*;
import jceliac.video.strategyHandler.*;
import jceliac.featureExtraction.*;

/**
 * Outcome processing strategy that simply displays features.
 * <p>
 * @author shegen
 */
public class OutcomeProcessingStrategyDisplayFeatures
        extends AbstractOutcomeProcessingStrategy
{

    public OutcomeProcessingStrategyDisplayFeatures(CloneableReflectableParameters parameters)
    {
        super(EOutcomeProcessingStrategy.DisplayFeatures, parameters);
    }

    @ArgumentType(typeIdentifiers = {"Features"})
    @Override
    protected void doPerform(ArrayList<?> o) throws JCeliacGenericException
    {
        @SuppressWarnings("unchecked")
        ArrayList<DiscriminativeFeatures> features = (ArrayList<DiscriminativeFeatures>) o;
        for(DiscriminativeFeatures tmp : features) {
            double[] vec = tmp.getAllFeatureVectorData();
            for(int i = 0; i < vec.length; i++) {
                System.out.println(vec[i]);
            }

        }
    }

}
