/*
 *  JCeliac Copyright (C) 2010-2011 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies.classification;

import jceliac.video.strategies.AbstractClassificationStrategy.EClassificationStrategy;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;
import jceliac.classification.ClassificationOutcome;
import jceliac.CloneableReflectableParameters;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.video.strategyHandler.*;
import jceliac.featureOptimization.*;
import jceliac.video.strategies.*;
import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationMethod;

/**
 *
 * @author shegen
 */
public class ClassificationStrategySeparateFeatures
        extends AbstractClassificationStrategy
{

    
    public ClassificationStrategySeparateFeatures(CloneableReflectableParameters parameters,
                                                  ClassificationMethod classificationMethod,
                                                  FeatureOptimizationMethod optimizationMethod)
            throws JCeliacGenericException
    {
        super(EClassificationStrategy.SeparateFeatures, parameters, classificationMethod, optimizationMethod);
        // train the classifier based on the training set (k-Value,...)
        // XXX: write some code for validation
        
    }

    private ArrayList<?> doPerformTyped(ArrayList<?> allFeatures)
            throws JCeliacGenericException
    {
        ArrayList<ClassificationOutcome> outcomes = new ArrayList<>();
        Integer[] testIndex = new Integer[1];
        testIndex[0] = 0;

        DiscriminativeFeatures tmpFeature = (DiscriminativeFeatures) allFeatures.get(0);
        FeatureVectorSubsetEncoding encoding = new FeatureVectorSubsetEncoding(tmpFeature.getFeatureVectorDimensionality());
        encoding.setAll();

        for(int i = 0; i < allFeatures.size(); i++) {
            DiscriminativeFeatures tmp = (DiscriminativeFeatures) allFeatures.get(i);
            // XXX needs new classification and validation code
            
           /* ArrayList<ClassificationOutcome> outcomesCurrent = mapping.mapSample(tmp);
            for(ClassificationOutcome tmpOutcome : outcomesCurrent) {
                tmpOutcome.setFeatureVector(tmp);
                outcomes.add(tmpOutcome);
            }*/
        }
        return outcomes;
    }

    @ReturnType(typeIdentifier = "ClassificationOutcome")
    @Override
    protected ArrayList<?> doPerform(ArrayList<?> allFeatures)
            throws JCeliacGenericException
    {
        return doPerformTyped(allFeatures);
    }

    @Override
    public Object mapStrategyNameToEnumType(String name)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
