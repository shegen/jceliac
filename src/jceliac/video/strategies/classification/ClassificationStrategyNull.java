/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies.classification;

import jceliac.CloneableReflectableParameters;
import jceliac.video.strategyHandler.*;
import jceliac.video.strategies.*;
import java.util.ArrayList;
import jceliac.classification.ClassificationMethod;
import jceliac.featureOptimization.FeatureOptimizationMethod;

/**
 * Dummy classification strategy doing nothing.
 * <p>
 * @author shegen
 */
public class ClassificationStrategyNull
        extends AbstractClassificationStrategy
{

    public ClassificationStrategyNull(CloneableReflectableParameters parameters,
                                      ClassificationMethod classificationMethod, 
                                      FeatureOptimizationMethod optimizationMethod)
    {
        super(EClassificationStrategy.Null, parameters, classificationMethod, optimizationMethod);
    }

    @ReturnType(typeIdentifier = "Input")
    @Override
    protected ArrayList<?> doPerform(ArrayList<?> f)
    {
        return f;
    }

    @Override
    public Object mapStrategyNameToEnumType(String name)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
