/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies.classification;

import jceliac.video.strategies.AbstractClassificationStrategy.EClassificationStrategy;
import jceliac.video.strategies.parameters.ClassificationStrategyParameters;

/**
 * Parameters for dummy classification strategy.
 * <p>
 * @author shegen
 */
public class ClassificationStrategyParametersNull
        extends ClassificationStrategyParameters
{

    public ClassificationStrategyParametersNull()
    {
        super(EClassificationStrategy.Null);
    }

}
