/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies.segmentation;

import jceliac.video.strategies.AbstractSegmentationStrategy.ESegmentationStrategy;
import jceliac.video.strategyHandler.SegmentedFrame;
import jceliac.CloneableReflectableParameters;
import jceliac.JCeliacGenericException;
import jceliac.video.FrameSequence;
import jceliac.video.strategies.*;
import jceliac.tools.data.Frame;
import java.util.ArrayList;

/**
 * Returns the entire frame.
 * <p>
 * @author shegen
 */
public class SegmentationStrategySingleSegment
        extends AbstractSegmentationStrategy
{

    public SegmentationStrategySingleSegment(CloneableReflectableParameters parameters)
    {
        super(ESegmentationStrategy.SingleSegment, parameters);
    }

    @Override
    protected ArrayList<SegmentedFrame> doPerform(FrameSequence s) throws
            JCeliacGenericException
    {
        ArrayList<SegmentedFrame> segmentedFrames = new ArrayList<>();

        while(s.hasNext()) {
            Frame tmp = s.next();
            SegmentedFrame segments = new SegmentedFrame();
            segments.addSegment(tmp);
            segmentedFrames.add(segments);
        }
        return segmentedFrames;
    }

}
