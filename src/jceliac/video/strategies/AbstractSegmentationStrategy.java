/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies;

import jceliac.video.strategyHandler.SegmentedFrame;
import jceliac.video.*;
import java.util.*;
import jceliac.*;

/**
 * The segmentation strategies expect a frame sequence as input. It returns a
 * number of FrameSegments depending on the actual strategy.
 * <p>
 * @author shegen
 */
public abstract class AbstractSegmentationStrategy
        extends AbstractStrategy
{

    public static enum ESegmentationStrategy
    {

        TileAdjacentSquares,
        SingleSegment,
        Default;

    }

    protected ESegmentationStrategy strategy;

    public AbstractSegmentationStrategy(ESegmentationStrategy segmentationStrategy,
                                        CloneableReflectableParameters parameters)
    {
        super(AbstractStrategy.EStrategyType.SEGMENTATION_STRATEGY, parameters);
        this.strategy = segmentationStrategy;
    }

    /*
     * We supply the content stream to the segmentation strategy. Some
     * strategies might use a sequence of frames to create a lower number of
     * average frames for example.
     */
    @Override
    public Object perform(Object o) throws JCeliacGenericException
    {
        return doPerform((FrameSequence) o);
    }

    protected abstract ArrayList<SegmentedFrame> doPerform(FrameSequence s)
            throws JCeliacGenericException;

    public ESegmentationStrategy getStrategy()
    {
        return this.strategy;
    }

    @Override
    public Object mapStrategyNameToEnumType(String name)
    {
        return ESegmentationStrategy.valueOf(name);
    }

}
