/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies;

import jceliac.video.strategies.AbstractSegmentationStrategy.*;
import jceliac.video.strategyHandler.SegmentedFrame;
import jceliac.tools.data.Frame;
import java.util.ArrayList;
import jceliac.video.*;
import jceliac.*;

/**
 *
 * @author shegen
 */
public class SegmentationStrategyTileAdjacentSquares
        extends AbstractSegmentationStrategy
{

    public SegmentationStrategyTileAdjacentSquares(CloneableReflectableParameters parameters)
    {
        super(ESegmentationStrategy.TileAdjacentSquares, parameters);
    }

    /**
     * The segmentation has to prepare data for the entire sequence, because at
     * this point we do not know what feature extraction strategy is used.
     * <p>
     * @param sequence Sequence of frames to segment.
     * <p>
     * @return List of segmented frames.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    @Override
    protected ArrayList<SegmentedFrame> doPerform(FrameSequence sequence) throws
            JCeliacGenericException
    {
        ArrayList<SegmentedFrame> ret = new ArrayList<>();
        while(sequence.hasNext()) {
            ret.add(SegmentationStrategyTileAdjacentSquares.extractAdjacentSquares(sequence.next(), 5));
        }
        return ret;
    }

    /**
     * Extract squared adjacent tiles from the image.
     * <p>
     * @param frame Frame that is segmented.
     * @param dim   Dimension of tiles.
     * <p>
     * @return Segmented frame.
     * <p>
     * @throws JCeliacGenericException
     */
    private static SegmentedFrame extractAdjacentSquares(Frame frame, int dim)
            throws JCeliacGenericException
    {
        SegmentedFrame tiledFrame = new SegmentedFrame();

        // check if dimension of tiling is possible
        if(frame.getWidth() % dim != 0 || frame.getHeight() % dim != 0) {
            throw new JCeliacGenericException("Frame is not a multiple of Tile Dimension (" + dim + ")");
        }
        for(int x = 0; x < frame.getWidth(); x += dim) {
            for(int y = 0; y < frame.getHeight(); y += dim) {

                Frame tmp = new Frame(frame.getParentFile(), frame.getFrameIdentifier());

                // extract tiles
                double[][] redData = extract(tmp.getRedData(), x, y, dim);
                double[][] greenData = extract(tmp.getGreenData(), x, y, dim);
                double[][] blueData = extract(tmp.getBlueData(), x, y, dim);
                double[][] grayData = extract(tmp.getGrayData(), x, y, dim);

                tmp.setRedData(redData);
                tmp.setGreenData(greenData);
                tmp.setBlueData(blueData);
                tmp.setGrayData(grayData);

                tiledFrame.addSegment(tmp);
            }
        }
        return tiledFrame;
    }

    /**
     * Extract sub-image from raw image data.
     * <p>
     * @param data      Raw image data.
     * @param x         Upper left position of extracted area, x.
     * @param y         Upper left position of extracted area, y.
     * @param dimension Dimension of extracted area.
     * <p>
     * @return Deep copy of extracted area.
     */
    private static double[][] extract(double[][] data, int x, int y, int dimension)
    {
        double[][] copy = new double[dimension][dimension];

        // copy the 2-dimensional array
        for(int rows = 0; rows < dimension; rows++) {
            System.arraycopy(data[x + rows], y, copy[rows], 0, dimension);
        }
        return data;
    }

}
