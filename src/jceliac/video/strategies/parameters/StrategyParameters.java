/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies.parameters;

import jceliac.*;

/**
 * Base for strategy parameters.
 * <p>
 * @author shegen
 */
public class StrategyParameters
        extends CloneableReflectableParameters
{

    protected ClassificationStrategyParameters classificationParameters;
    protected FeatureExtractionStrategyParameters featureExtractionParameters;
    protected FrameProcessingStrategyParameters frameProcessingParameters;
    protected OutcomeCombinationStrategyParameters outcomeCombinationParameters;
    protected SegmentationStrategyParameters segmentationParameters;

    public void setClassificationParameters(ClassificationStrategyParameters classificationParameters)
    {
        this.classificationParameters = classificationParameters;
    }

    public void setFeatureExtractionParameters(FeatureExtractionStrategyParameters featureExtractionParameters)
    {
        this.featureExtractionParameters = featureExtractionParameters;
    }

    public void setFrameProcessingParameters(FrameProcessingStrategyParameters frameProcessingParameters)
    {
        this.frameProcessingParameters = frameProcessingParameters;
    }

    public void setOutcomeCombinationParameters(OutcomeCombinationStrategyParameters outcomeCombinationParameters)
    {
        this.outcomeCombinationParameters = outcomeCombinationParameters;
    }

    public void setSegmentationParameters(SegmentationStrategyParameters segmentationParameters)
    {
        this.segmentationParameters = segmentationParameters;
    }

    public ClassificationStrategyParameters getClassificationParameters()
    {
        return classificationParameters;
    }

    public FeatureExtractionStrategyParameters getFeatureExtractionParameters()
    {
        return featureExtractionParameters;
    }

    public FrameProcessingStrategyParameters getFrameProcessingParameters()
    {
        return frameProcessingParameters;
    }

    public OutcomeCombinationStrategyParameters getOutcomeCombinationParameters()
    {
        return outcomeCombinationParameters;
    }

    public SegmentationStrategyParameters getSegmentationParameters()
    {
        return segmentationParameters;
    }

}
