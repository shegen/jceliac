/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies.parameters;

import jceliac.video.strategies.AbstractSegmentationStrategy.ESegmentationStrategy;
import jceliac.*;

/**
 * Base parameters for segmentation strategy.
 * <p>
 * @author shegen
 */
public class SegmentationStrategyParameters
        extends CloneableReflectableParameters
{

    protected final ESegmentationStrategy strategy;

    public SegmentationStrategyParameters(ESegmentationStrategy strategy)
    {
        this.strategy = strategy;
    }

    public ESegmentationStrategy getStrategy()
    {
        return strategy;
    }

}
