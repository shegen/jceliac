/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies.parameters;

import jceliac.video.strategies.AbstractFrameProcessingStrategy.EFrameProcessingStrategy;

/**
 * Parameters for downsampling frame processing strategy.
 * <p>
 * @author shegen
 */
public class DownsampleFrameProcessingStrategyParameters
        extends FrameProcessingStrategyParameters
{   // ratio of 1 means all frames are extracted, 0.5 means each second frame is extracted, ... 

    public double downsamplingRatio = 1;

    public DownsampleFrameProcessingStrategyParameters(EFrameProcessingStrategy strategy)
    {
        super(strategy);
    }

    public double getDownsamplingRatio()
    {
        return downsamplingRatio;
    }

    public void setDownsamplingRatio(double downsamplingRatio)
    {
        this.downsamplingRatio = downsamplingRatio;
    }
}
