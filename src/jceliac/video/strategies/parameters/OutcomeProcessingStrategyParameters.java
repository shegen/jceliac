/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies.parameters;

import jceliac.video.strategies.AbstractOutcomeProcessingStrategy.EOutcomeProcessingStrategy;
import jceliac.*;

/**
 * Base parameters for outcome processing strategy.
 * <p>
 * @author shegen
 */
public class OutcomeProcessingStrategyParameters
        extends CloneableReflectableParameters
{

    protected final EOutcomeProcessingStrategy strategy;

    public OutcomeProcessingStrategyParameters(EOutcomeProcessingStrategy strategy)
    {
        this.strategy = strategy;
    }

    public EOutcomeProcessingStrategy getStrategy()
    {
        return strategy;
    }

}
