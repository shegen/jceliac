/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies;

import jceliac.*;

/**
 * A Strategy defines a certain behavior used within the
 * VideoSequenceClassificationHandler.
 * <p>
 *
 * @author shegen
 */
public abstract class AbstractStrategy
{

    public static enum EStrategyType
    {

        SEGMENTATION_STRATEGY, // this handles the "segmentation" of single frames
        FEATURE_EXTRACTION_STRATEGY, // this handles how features are extracted from a set of frames
        CLASSIFICATION_STRATEGY, // this handles how the classification is performed from a set of features
        FRAME_PROCESSING_STRATEGY, // this handles how frames are extracted from the stream and frame sequences are built
        OUTCOME_PROCESSING_STRATEGY,
        OUTCOME_COMBINATION_STRATEGY
    }

    protected String strategyName;
    protected EStrategyType strategyType;
    protected CloneableReflectableParameters parameters;

    protected AbstractStrategy(EStrategyType strategyType, CloneableReflectableParameters parameters)
    {
        this.strategyType = strategyType;
        this.parameters = parameters;
    }

    public String getStrategyName()
    {
        return strategyName;
    }

    public void setStrategyName(String strategyName)
    {
        this.strategyName = strategyName;
    }

    public EStrategyType getStrategyType()
    {
        return strategyType;
    }

    public void setStrategyType(EStrategyType strategyType)
    {
        this.strategyType = strategyType;
    }

    public abstract Object perform(Object o) throws JCeliacGenericException;

    public abstract Object mapStrategyNameToEnumType(String name);
}
