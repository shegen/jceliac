/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies.frameProcessing;

import jceliac.video.strategies.AbstractFrameProcessingStrategy.EFrameProcessingStrategy;
import jceliac.video.strategies.parameters.*;

/**
 * Parameter for frame processing strategy.
 * <p>
 * @author shegen
 */
public class FrameProcessingStrategyParametersDownsampling
        extends FrameProcessingStrategyParameters
{

    public double downsamplingRatio = 1.0d;

    public FrameProcessingStrategyParametersDownsampling()
    {
        super(EFrameProcessingStrategy.Downsample);
    }

    public double getDownsamplingRatio()
    {
        return downsamplingRatio;
    }

    public void setDownsamplingRatio(double downsamplingRatio)
    {
        this.downsamplingRatio = downsamplingRatio;
    }

}
