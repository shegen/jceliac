/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies.frameProcessing;

import jceliac.tools.data.VideoContentStream;
import jceliac.video.FrameSequence;
import jceliac.video.strategies.*;
import jceliac.tools.data.*;
import java.util.*;
import jceliac.*;

/**
 * This is a simple FrameProcessingStrategy. It simply extracts frame using a
 * supplied ratio. This can be used for down sampling. Extracted frames are
 * either discarded or returned untouched.
 * <p>
 * @author shegen
 */
public class FrameProcessingStrategyDownsampling
        extends AbstractFrameProcessingStrategy
{

    private final FrameProcessingStrategyParametersDownsampling myParameters;

    public FrameProcessingStrategyDownsampling(CloneableReflectableParameters parameters)
    {
        super(FrameProcessingStrategyDownsampling.EFrameProcessingStrategy.Downsample, parameters);
        this.myParameters = (FrameProcessingStrategyParametersDownsampling) parameters;

    }

    @Override
    protected FrameSequence doPerform(VideoContentStream stream) throws
            JCeliacGenericException
    {
        FrameSequence sequence = new FrameSequence();
        if(myParameters.getDownsamplingRatio() == 0) {
            throw new JCeliacGenericException("Downsampling Ratio is 0.0d");
        }
        ArrayList<Integer> indices = computeIndices();
        // find the max index, the index are increasing monotonically
        int maxIndex = (int) indices.get(indices.size() - 1);

        // we know that we need exactly maxIndex number of frames to create a 
        // frame sequence of size FRAMES_PER_SECOND
        int curIndex = 0;
        for(int i = 0; i < maxIndex; curIndex++) {
            if(stream.hasNext() == false) {
                break;
            }
            Frame tmp;
            do {
                tmp = stream.next();
            } while(stream.hasNext() && curIndex < indices.size()
                    && (int) indices.get(curIndex) != i++);
            sequence.addFrame(tmp);
        }
        return sequence;
    }

    private ArrayList<Integer> computeIndices()
    {
        ArrayList<Integer> indices = new ArrayList<>();
        for(int i = 0; indices.size() < FrameSequence.FRAMES_PER_SEQUENCE; i++) {
            int index = (int) Math.round(i / myParameters.getDownsamplingRatio());
            indices.add(index);
        }
        return indices;
    }

    @Override
    public Object mapStrategyNameToEnumType(String name)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
