/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies;

import jceliac.featureExtraction.*;
import jceliac.video.strategyHandler.*;
import java.util.*;
import jceliac.*;

/**
 * The feature extraction strategies expect a frame sequence as input and
 * returns a number of features.
 * <p>
 * @author shegen
 */
public abstract class AbstractFeatureExtractionStrategy
        extends AbstractStrategy
{

    public static enum EFeatureExtractionStrategy
    {

        SeparateFramesSeparateSegments, // extract features for each single frame from each segment separately
        Default;

    }
    protected EFeatureExtractionStrategy strategy;
    protected FeatureExtractionMethod featureExtractionMethod;

    public AbstractFeatureExtractionStrategy(EFeatureExtractionStrategy strategy,
                                             CloneableReflectableParameters parameters,
                                             FeatureExtractionMethod featureExtractionMethod)
    {
        super(AbstractStrategy.EStrategyType.FEATURE_EXTRACTION_STRATEGY, parameters);
        this.strategy = strategy;
        this.featureExtractionMethod = featureExtractionMethod;
    }

    public EFeatureExtractionStrategy getStrategy()
    {
        return strategy;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object perform(Object o) throws JCeliacGenericException
    {
        return doPerform((ArrayList<SegmentedFrame>) o);
    }

    protected abstract ArrayList<DiscriminativeFeatures> doPerform(ArrayList<SegmentedFrame> f) throws JCeliacGenericException;

}
