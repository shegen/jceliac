/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies;

import java.util.*;
import jceliac.*;

/**
 * This class handles the classification outcomes collected for each
 * FrameSequence and decides what to do with those. E.g. building statistics or
 * voting ...
 * <p>
 * @author shegen
 */
public abstract class AbstractOutcomeCombinationStrategy
        extends AbstractStrategy
{

    public static enum EOutcomeCombinationStrategy
    {

        MajorityVote,
        Null,
        Default  // MajorityVote
    }

    protected EOutcomeCombinationStrategy strategy;

    public AbstractOutcomeCombinationStrategy(EOutcomeCombinationStrategy strategy, CloneableReflectableParameters parameters)
    {
        super(AbstractStrategy.EStrategyType.OUTCOME_COMBINATION_STRATEGY, parameters);
        this.strategy = strategy;
    }

    /*
     * We supply the content stream to the segmentation strategy. Some
     * strategies might use a sequence of frames to create a lower number of
     * average frames for example.
     */
    @Override
    public Object perform(Object o) throws JCeliacGenericException
    {
        return doPerform((ArrayList<?>) o);
    }

    /* The OutcomeCombination strategy picks up a set of Classifcation outcomes
     * and generates a decision. I don't know how the return value looks like
     * yet :)
     */
    protected abstract Object doPerform(ArrayList<?> o) throws JCeliacGenericException;

    public EOutcomeCombinationStrategy getStrategy()
    {
        return this.strategy;
    }

}
