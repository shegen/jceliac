/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategies;

import java.util.*;
import jceliac.*;
import jceliac.classification.ClassificationMethod;
import jceliac.featureOptimization.FeatureOptimizationMethod;

/**
 * Base class for classification strategies.
 * <p>
 * @author shegen
 */
public abstract class AbstractClassificationStrategy
        extends AbstractStrategy
{

    public static enum EClassificationStrategy
    {
        SeparateFeatures, // classify each set of extracted features
        Default,
        Null
    }
    protected EClassificationStrategy strategy;
    protected ClassificationMethod classificationMethod;
    protected FeatureOptimizationMethod optimizationMethod;

    public AbstractClassificationStrategy(EClassificationStrategy strategy,
                                          CloneableReflectableParameters parameters,
                                          ClassificationMethod classificationMethod,
                                          FeatureOptimizationMethod optimizationMethod)
    {
        super(AbstractStrategy.EStrategyType.CLASSIFICATION_STRATEGY, parameters);
        this.strategy = strategy;
        this.classificationMethod = classificationMethod;
        this.optimizationMethod = optimizationMethod;
    }

    public EClassificationStrategy getStrategy()
    {
        return strategy;
    }

    @Override
    public Object perform(Object o) throws JCeliacGenericException
    {
        return doPerform((ArrayList<?>) o);
    }

    /**
     * Performs the strategy.
     * @param f Strategy parameters. 
     * @return Result of strategy.
     * @throws jceliac.JCeliacGenericException
     */
    protected abstract ArrayList<?> doPerform(ArrayList<?> f) throws JCeliacGenericException;

}
