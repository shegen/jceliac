/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video;

import jceliac.tools.filter.LowpassFilter;
import jceliac.tools.data.Frame;
import jceliac.tools.methods.*;
import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.Main;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.data.*;

/**
 * Return frames in close proximity (in time) to a snapshot taken by a
 * physician. Snapshots are characterized by a 1-2 period without any content
 * change of the frames.
 * <p>
 * @author shegen
 */
public class SnapshotFilter
        extends FrameFilter
{

    public static final int WINDOW_SIZE = 20;
    public static final double FRAME_RATE = 50;

    @Override
    public void filter(ArrayList<Frame> sequence)
    {
        try {
            if(sequence.size() < 2) {
                sequence.clear();
                return;
            }

            LowpassFilter gaussian = new LowpassFilter(LowpassFilter.EFilterType.FILTER_GAUSSIAN, 5, 0.5);

            ArrayList<Double> psnrs = new ArrayList<>();
            for(int i = 0; i < sequence.size() - 2; i++) {
                Frame frame1 = sequence.get(i);
                Frame frame2 = sequence.get(i + 1);       // we usually have two equal consecutive frames

                try {
                    double[][] d1;
                    double[][] d2;

                    d1 = frame1.getGrayData();
                    d1 = gaussian.filterData(d1);

                    d2 = frame2.getGrayData();
                    d2 = gaussian.filterData(d2);

                    double psnr1 = SignalMetrics.computePSNR(d1, d2);
                    if(psnr1 != 1000) {
                        psnrs.add(psnr1);
                    }

                } catch(JCeliacGenericException e) {
                    Main.logException(JCeliacLogger.LOG_WARN, e);
                }
            }
            double mean = 0;
            for(Double tmp : psnrs) {
                mean = mean + tmp;
            }
            mean = mean / psnrs.size();

            if(mean < 60) { // prune sequence
                sequence.clear();
            } else {
                System.out.println("Snapshot detected!");
                System.out.println("Start Frame: " + sequence.get(0).getFrameIdentifier());
                System.out.println("End Frame: " + sequence.get(sequence.size() - 1).getFrameIdentifier());
                System.out.println("Estimated Start Time: " + sequence.get(0).getFrameIdentifier() / 50);
                sequence.clear();
            }
        } catch(JCeliacGenericException e) {
            Main.logException(JCeliacLogger.LOG_WARN, e);
        }
    }

    public static void evaluateSnapshots(String movieName, ArrayList<PSNRInfo> data)
    {
        ArrayList<Double> means = new ArrayList<>();
        int shotCount = 0;

        if(data.size() < SnapshotFilter.WINDOW_SIZE) {
            return;
        }
        for(int i = 0; i < data.size(); i++) {
            System.out.println(data.get(i).psnr);
        }
        double mean = 0;
        for(int i = 0; i < SnapshotFilter.WINDOW_SIZE; i++) {
            mean += data.get(i).psnr;
        }
        mean /= SnapshotFilter.WINDOW_SIZE;

        for(int index = SnapshotFilter.WINDOW_SIZE; index < data.size() - 1; index++) {
            // compute the mean iteratively for each window (shift by 1) using the previous mean
            double x1 = data.get(index - SnapshotFilter.WINDOW_SIZE).psnr;
            double xn = data.get(index + 1).psnr;

            mean = mean - (x1 / SnapshotFilter.WINDOW_SIZE);
            mean += (xn / SnapshotFilter.WINDOW_SIZE);

            means.add(mean);
        }
        int count = 0;
        for(int index = 0; index < means.size(); index++) {
            if(means.get(index) > 50) {
                count++;
            } else {
                if(count > 20) {
                    int ind = movieName.lastIndexOf(".mpg");
                    if(ind == -1) {
                        ind = movieName.lastIndexOf(".MPG");
                        if(ind == -1) {
                            ind = movieName.lastIndexOf(".Mpg");
                            if(ind == -1) {
                                ind = movieName.length();
                            }
                        }
                    }

                    System.out.println("ffmpeg -ss " + getTimeForFrameNumberInSeconds(data.get(index).frameID) + " -i " + movieName + " -t 10 -vcodec copy " + "-o " + movieName.substring(0, ind) + "-shot" + shotCount + ".mpg");
                    shotCount++;
                }
                count = 0;
            }
        }
    }

    private static String getTimeForFrameNumberInSeconds(int frameNumber)
    {
        double pos = ((double) frameNumber) / SnapshotFilter.FRAME_RATE;

        int seconds = (int) pos;
        return "" + seconds;
    }
}
