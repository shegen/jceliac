/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video;

import com.xuggle.xuggler.IVideoPicture;
import java.awt.image.BufferedImage;
import jceliac.tools.data.Frame;
import java.util.*;
import jceliac.JCeliacGenericException;
import jceliac.Main;
import jceliac.logging.JCeliacLogger;

/**
 * This is the worker thread that decodes video frames. 
 * 
 * @author shegen
 */
public class DecoderThread
        extends Thread
{

    private final int FRAME_BUFFER_SIZE = 50; // number of frames to buffer
    private final MPEG2Decoder decoder;
    private ArrayList<Frame> frameBuffer;
    private boolean finished = false;
    protected static DecoderThread thread = null;
    private boolean running;

    public static synchronized DecoderThread createDecoderThread(MPEG2Decoder decoder, int numFrames)
    {
        if(DecoderThread.thread == null) {
            DecoderThread t = new DecoderThread(decoder, numFrames);
            t.start();
            return t;
        } else {
            if(DecoderThread.thread.isAlive() == false) {
                DecoderThread t = new DecoderThread(decoder, numFrames);
                t.start();
                return t;
            } else {
                return DecoderThread.thread;
            }
        }
    }

    private DecoderThread(MPEG2Decoder decoder, int numFrames)
    {
        this.decoder = decoder;
        this.frameBuffer = new ArrayList<>();
    }

    /**
     * Performs the decoding and conversion of video frames.
     */
    @Override
    public synchronized void run()
    {
        try {

            while(true) {
                while(this.frameBuffer.size() < FRAME_BUFFER_SIZE) {
                    this.running = true;
                    IVideoPicture picture = this.decoder.decodeNextPicture();
                    BufferedImage image = this.decoder.convertPicture(picture);
                    if(image == null) {
                        continue;
                    }
                    Frame frameTmp = Frame.convertBufferedImageToFrameOptimized(image);
                    this.frameBuffer.add(frameTmp);
                }
                // do not fill beyond 50 Frames
                while(this.decoder.getFrameBufferSize() > FRAME_BUFFER_SIZE) {
                    this.running = false;
                    this.wait();
                }
                this.decoder.addFramesToBuffer(this.frameBuffer);
                this.frameBuffer = new ArrayList<>();
            }
        } catch(MPEG2FinishedException e) {
            this.decoder.addFramesToBuffer(this.frameBuffer);
            this.frameBuffer = new ArrayList<>();
            this.finished = true;
        } catch(InterruptedException | JCeliacGenericException e) {
            Main.logException(JCeliacLogger.LOG_ERROR, e);
        }
    }

    public boolean isFinished()
    {
        return finished;
    }

    public boolean isRunning()
    {
        return this.running;
    }
}
