/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video;

import jceliac.tools.data.Frame;
import java.util.ArrayList;
import java.util.List;

/**
 * Filters frames with high movement. 
 * 
 * @author shegen
 */
public class MotionFrameFilter
        extends FrameFilter
{

    public static final int WINDOW_SIZE = 3;
    public static final int PSNR_WINDOW_SIZE = 5;

    @Override
    public void filter(ArrayList<Frame> sequence)
    {
        ArrayList<Frame> pruneList = new ArrayList<>();
        for(int index = 0; index < sequence.size() - MotionFrameFilter.PSNR_WINDOW_SIZE; index += MotionFrameFilter.PSNR_WINDOW_SIZE) {
            List<Frame> frameList = sequence.subList(index, index + MotionFrameFilter.PSNR_WINDOW_SIZE);
            double psnr = windowedPSNR(frameList);
            System.out.println("PSNR (win=10): " + psnr);

            if(psnr < 25) {
                for(int i = 0; i < MotionFrameFilter.PSNR_WINDOW_SIZE; i++) {
                    pruneList.add(sequence.get(index + i));
                }
            } else // prune all frames except the mid frame
            {
                int mid = (MotionFrameFilter.PSNR_WINDOW_SIZE / 2) + 1;
                for(int i = 0; i < MotionFrameFilter.PSNR_WINDOW_SIZE; i++) {
                    if(i != mid) {
                        pruneList.add(sequence.get(index + i));
                    }
                }
            }
        }
        int rem = sequence.size() % MotionFrameFilter.PSNR_WINDOW_SIZE;
        if(rem != 0) // remove the unlucky frames not fitting in the window
        {
            sequence.subList(sequence.size() - rem, sequence.size()).clear();
        }
        for(Frame f : pruneList) {
            sequence.remove(f);
        }

    }

    private double windowedPSNR(List<Frame> frameList)
    {
        double psnrSum = 0;
        int count = 0;

        for(int i = 0; i < frameList.size(); i++) {
            for(int j = i + 1; j < frameList.size(); j++) {
                double[][] c1 = frameList.get(i).getGrayData();
                double[][] c2 = frameList.get(j).getGrayData();
                psnrSum += PSNR(c1, c2);
                count++;
            }
        }
        return psnrSum / count;
    }

    private double PSNR(double[][] a1, double[][] a2)
    {
        double mse = MSE(a1, a2);
        return 10 * Math.log10(65025.0d / mse);
    }

    private double MSE(double[][] a1, double[][] a2)
    {
        double sum = 0;

        for(int i = MotionFrameFilter.PSNR_WINDOW_SIZE; i < a1.length - MotionFrameFilter.PSNR_WINDOW_SIZE; i++) {
            for(int j = MotionFrameFilter.PSNR_WINDOW_SIZE; j < a1[0].length - MotionFrameFilter.PSNR_WINDOW_SIZE; j++) {
                sum += ((a1[i][j] - a2[i][j]) * (a1[i][j] - a2[i][j]));
            }
        }
        return sum / (a1.length * a1[0].length);
    }

    private double windowedDifference(double[][] a1, double[][] a2, int x, int y, int size)
    {
        double sum1 = 0;
        double sum2 = 0;

        for(int i = x - size; i < x + size; i++) {
            for(int j = y - size; j < y + size; j++) {
                sum1 += a1[i][j];
                sum2 += a2[i][j];
            }
        }
        return (sum1 - sum2) * (sum1 - sum2);
    }
}
