/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video;

import jceliac.tools.data.*;
import java.util.*;
import jceliac.JCeliacGenericException;

/**
 * Represents a sequence of frames that is used as a "chunk" of work.
 * <p>
 * @author shegen
 */
public class FrameSequence
{

    public static int FRAMES_PER_SEQUENCE = 50;

    private final ArrayList<Frame> sequence;
    private int currentFrameID = 0;
    private int currentIndex = 0;

    public FrameSequence()
    {
        sequence = new ArrayList<>();
    }

    public void addFrame(Frame f)
            throws JCeliacGenericException
    {
        // ensure the frames are added monotonically
        if(f.getFrameIdentifier() <= this.currentFrameID) {
            throw new JCeliacGenericException("Frame is before or equal to last Frame in Sequence");
        }
        this.sequence.add(f);
        this.currentFrameID = f.getFrameIdentifier();
    }

    public int getSequenceLength()
    {

        return this.sequence.size();
    }

    public Frame getFrame(int index)
    {
        return this.sequence.get(index);
    }

    public boolean hasNext()
    {
        return (this.currentIndex < this.sequence.size());
    }

    public Frame next()
    {
        Frame nextFrame = this.sequence.get(this.currentIndex);
        this.currentIndex++;
        return nextFrame;
    }

    public Frame get(int index)
    {
        return sequence.get(index);
    }

    public void pruneAndFilter()
    {
        CropFilter filter = new CropFilter();
        filter.filter(this.sequence);
    }

}
