/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video;

import jceliac.tools.data.Frame;
import java.util.ArrayList;

/**
 *
 * @author shegen
 */
public class DownsamplingFrameFilter
        extends FrameFilter
{

    public static final double DOWNSAMPLING_RATE = 2; // remove every 2nd frame

    @Override
    public void filter(ArrayList<Frame> sequence)
    {
        ArrayList<Frame> pruneList = new ArrayList<>();
        for(int index = 0; index < sequence.size(); index++) {
            if((index % DOWNSAMPLING_RATE) != 0) {
                pruneList.add(sequence.get(index));
            }
        }
        for(Frame f : pruneList) {
            sequence.remove(f);
        }
    }
}
