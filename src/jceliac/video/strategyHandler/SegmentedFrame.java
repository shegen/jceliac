/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategyHandler;

import jceliac.tools.data.*;
import java.util.*;

/**
 * For now we only support squared and rectangle-shaped segments (thats because
 * our feature extraction methods only support that at this point), boring ...
 * <p>
 * @author shegen
 */
public class SegmentedFrame
{

    private final ArrayList<Frame> segments = new ArrayList<>();
    private int index = 0;

    public int getNumberOfSegments()
    {
        return this.segments.size();
    }

    public void addSegment(Frame segment)
    {
        this.segments.add(segment);
    }

    public boolean hasNext()
    {
        return this.index < this.segments.size();
    }

    public Frame next()
    {
        Frame segment = this.segments.get(this.index);
        this.index++;
        return segment;
    }
}
