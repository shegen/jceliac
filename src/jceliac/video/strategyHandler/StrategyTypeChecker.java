/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategyHandler;

import jceliac.video.strategies.*;
import jceliac.experiment.*;
import java.lang.reflect.*;
import jceliac.logging.*;
import java.util.*;

/**
 *
 * @author shegen
 */
public class StrategyTypeChecker
{

    @SuppressWarnings("unchecked")
    public static boolean checkTypes(AbstractClassificationStrategy classificationStrategy,
                                     AbstractOutcomeCombinationStrategy outcomeCombinationStrategy,
                                     AbstractOutcomeProcessingStrategy outcomeProcessingStrategy)
    {
        try {
            Class classificationStrategyClass = classificationStrategy.getClass();
            Method classificationStrategyMethod = classificationStrategyClass.getDeclaredMethod("doPerform", ArrayList.class);
            ReturnType classificationReturnType = classificationStrategyMethod.getAnnotation(ReturnType.class);
            String classificationReturnTypeIdentifier = classificationReturnType.typeIdentifier();

            Class outcomeCombinationClass = outcomeCombinationStrategy.getClass();
            Method outcomeCombinationMethod = outcomeCombinationClass.getDeclaredMethod("doPerform", ArrayList.class);
            ArgumentType outcomeCombinationArgumentType = outcomeCombinationMethod.getAnnotation(ArgumentType.class);
            ReturnType outcomeCombinationReturnType = outcomeCombinationMethod.getAnnotation(ReturnType.class);
            String[] outcomeCombinationArgumentTypeIdentifier = outcomeCombinationArgumentType.typeIdentifiers();
            String outcomeCombinationReturnTypeIdentifier = outcomeCombinationReturnType.typeIdentifier();

            Class outcomeProcessingClass = outcomeProcessingStrategy.getClass();
            Method outcomeProcessingMethod = outcomeProcessingClass.getDeclaredMethod("doPerform", ArrayList.class);
            ArgumentType outcomeProcessingArgumentType = outcomeProcessingMethod.getAnnotation(ArgumentType.class);
            String[] outcomeProcessingArgumentTypeIdentifier = outcomeProcessingArgumentType.typeIdentifiers();

            /* If a strategy accepts ANY as input, it's output is the same that
             * as it's input. This means that it's output is the same type as
             * the output of the predecessing strategy defines as type INPUT in
             * this case: ClassificationStrategy -> Features
             * OutcomeCombinationStrategy -> ReturnValue of
             * ClassificationStrategy OutcomeProcessingStrategy -> ReturnValue
             * of OutcomeCombinationStrategy
             */
            if(classificationReturnTypeIdentifier.compareTo("Input") == 0) {
                classificationReturnTypeIdentifier = "Features";
            }
            if(outcomeCombinationReturnTypeIdentifier.compareTo("Input") == 0) {
                outcomeCombinationReturnTypeIdentifier = classificationReturnTypeIdentifier;
            }
            // check if chains are ok, "Any" accepts all types

            return StrategyTypeChecker.checkChainedTypes(classificationReturnTypeIdentifier, outcomeCombinationArgumentTypeIdentifier,
                                                         outcomeCombinationReturnTypeIdentifier, outcomeProcessingArgumentTypeIdentifier);

        } catch(NoSuchMethodException | SecurityException e) {
            return false;
        }
    }

    /**
     * Checks if the argument types of the strategies are supported.
     * ClassificationStrategy -> OutcomeCombinationStrategy ->
     * OutcomeProcessingStrategy
     * <p>
     * @param classificationStrategyOutput
     * @param outcomeCombinationStrategyInputs
     * @param outcomeCombinationStrategyOutput
     * @param outcomeProcessingStrategyInput <p>
     * @return
     */
    private static boolean checkChainedTypes(String classificationStrategyOutput, String[] outcomeCombinationStrategyInputs,
                                             String outcomeCombinationStrategyOutput, String[] outcomeProcessingStrategyInput)
    {

        if(false == typesMatch(classificationStrategyOutput, outcomeCombinationStrategyInputs)) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "ClassificationStrategy output not compatible with OutcomeCombinationStrategy input!");
            return false;
        }
        if(false == typesMatch(outcomeCombinationStrategyOutput, outcomeProcessingStrategyInput)) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "OutcomeCombinationStrategy output not compatible with OutcomeProcessingStrategy input!");
            return false;
        }
        return true;
    }

    private static boolean typesMatch(String output, String[] input)
    {
        // Anything is accepted
        if(input[0].compareTo("Any") == 0) {
            return true;
        }
        for(String tmp : input) {
            if(output.compareTo(tmp) == 0) {
                return true;
            }
        }
        return false;
    }
}
