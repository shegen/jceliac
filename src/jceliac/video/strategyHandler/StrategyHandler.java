/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategyHandler;

import jceliac.video.strategies.AbstractStrategy;
import jceliac.tools.data.VideoContentStream;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.*;
import jceliac.video.*;
import java.util.*;

/**
 * This handles the execution of abstract strategies. All strategies are applied
 * a number of consecutive frames (with an upper bound on the number of frames).
 * Those frames are then supplied to the strategies.
 * <p>
 * This class only knows that the first call to the segmentationStrategyQueue
 * expects a VideoContentStream object.
 * <p>
 * @author shegen
 */
public class StrategyHandler
{

    private AbstractStrategy segmentationStrategy;
    private AbstractStrategy featureExtractionStrategy;
    private AbstractStrategy classificationMethodStrategy;
    private AbstractStrategy frameProcessingMethodStrategy;

    // content stream for a single sequence of strategies
    private final VideoContentStream stream;

    public StrategyHandler(VideoContentStream stream)
    {
        this.stream = stream;
    }

    public void setSegmentationStrategy(AbstractStrategy strategy)
    {
        this.segmentationStrategy = strategy;
    }

    public void setFeatureExtractionStrategy(AbstractStrategy strategy)
    {
        this.featureExtractionStrategy = strategy;
    }

    // we only accept a single classification method strategy
    public void setClassificationMethodStrategy(AbstractStrategy strategy)
    {
        this.classificationMethodStrategy = strategy;
    }

    public void setFrameProcessingMethodStrategy(AbstractStrategy frameProcessingMethodStrategy)
    {
        this.frameProcessingMethodStrategy = frameProcessingMethodStrategy;
    }

    /** 
     * This executes the strategies for a single FrameSequence using the
     * supplied VideoContentStream.
     * @return 
     * @throws jceliac.JCeliacGenericException
     */
    public Object executeStrategies() throws JCeliacGenericException
    {
        ArrayList<DiscriminativeFeatures> retValues = new ArrayList<>();

        // process frames and create the next frame sequence
        FrameSequence sequence = (FrameSequence) this.frameProcessingMethodStrategy.perform(this.stream);
        // repeat as long as their are frames left in the sequence 
        while(sequence.hasNext()) {
            // let the segmentation strategy decide how much frames are segmented at once
            FrameSequence segmentedSequence = (FrameSequence) this.segmentationStrategy.perform(sequence);
            // let the feature extraction strategy decide how much segmented frames are used at once
            while(segmentedSequence.hasNext()) {
                DiscriminativeFeatures ret = (DiscriminativeFeatures) this.featureExtractionStrategy.perform(segmentedSequence);
                // combine all features
                retValues.add(ret);
            }
        }
        // let the classification method decide how the features are handled for this single frame sequence
        return this.classificationMethodStrategy.perform(retValues);

    }
}
