/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategyHandler;

import jceliac.video.strategies.parameters.*;
import java.io.Serializable;
import jceliac.*;

/**
 * Parameters for video sequence handler.
 * <p>
 * @author shegen
 */
public class VideoSequenceHandlerParameters
        extends CloneableReflectableParameters
        implements Serializable
{

    private ClassificationStrategyParameters classificationParameters;
    private FeatureExtractionStrategyParameters featureExtractionParameters;
    private FrameProcessingStrategyParameters frameProcessingParameters;
    private OutcomeCombinationStrategyParameters outcomeCombinationParameters;
    private SegmentationStrategyParameters segmentationParameters;
    private OutcomeProcessingStrategyParameters outcomeProcessingParameters;

    public ClassificationStrategyParameters getClassificationParameters()
    {
        return classificationParameters;
    }

    public void setClassificationParameters(ClassificationStrategyParameters classificationParameters)
    {
        this.classificationParameters = classificationParameters;
    }

    public FeatureExtractionStrategyParameters getFeatureExtractionParameters()
    {
        return featureExtractionParameters;
    }

    public void setFeatureExtractionParameters(FeatureExtractionStrategyParameters featureExtractionParameters)
    {
        this.featureExtractionParameters = featureExtractionParameters;
    }

    public FrameProcessingStrategyParameters getFrameProcessingParameters()
    {
        return frameProcessingParameters;
    }

    public void setFrameProcessingParameters(FrameProcessingStrategyParameters frameProcessingParameters)
    {
        this.frameProcessingParameters = frameProcessingParameters;
    }

    public OutcomeCombinationStrategyParameters getOutcomeCombinationParameters()
    {
        return outcomeCombinationParameters;
    }

    public void setOutcomeCombinationParameters(OutcomeCombinationStrategyParameters outcomeCombinationParameters)
    {
        this.outcomeCombinationParameters = outcomeCombinationParameters;
    }

    public SegmentationStrategyParameters getSegmentationParameters()
    {
        return segmentationParameters;
    }

    public void setSegmentationParameters(SegmentationStrategyParameters segmentationParameters)
    {
        this.segmentationParameters = segmentationParameters;
    }

    public OutcomeProcessingStrategyParameters getOutcomeProcessingParameters()
    {
        return outcomeProcessingParameters;
    }

    public void setOutcomeProcessingParameters(OutcomeProcessingStrategyParameters outcomeProcessingParameters)
    {
        this.outcomeProcessingParameters = outcomeProcessingParameters;
    }

}
