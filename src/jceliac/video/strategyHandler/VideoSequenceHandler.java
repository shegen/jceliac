/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategyHandler;

import jceliac.video.strategies.AbstractStrategy;
import jceliac.JCeliacGenericException;
import jceliac.logging.JCeliacLogger;
import jceliac.experiment.Experiment;
import jceliac.featureExtraction.*;
import jceliac.video.strategies.*;
import jceliac.experiment.*;
import jceliac.tools.data.*;
import jceliac.video.*;
import java.util.*;
import jceliac.Main;
import jceliac.classification.ClassificationMethod;
import jceliac.featureOptimization.FeatureOptimizationMethod;

/**
 * This class handles the classification of video sequences. Using this approach
 * we do not break the experiment API and can evaluate different classification
 * strategies of the videos.
 * <p>
 * Video sequences have different demands on the code. We have to decide on: -
 * what part of the image is used for feature extraction - how many sub-images
 * do we use for feature extraction - how do we decide on the entire video
 * sequence (majority voting, ...) - do we use multiple frames to extract
 * features or single frames - and many more
 * <p>
 * @author shegen
 */
public class VideoSequenceHandler
        extends Thread
{

    private final ClassificationMethod classificationMethod;
    private final FeatureExtractionMethod featureExtractionMethod;
    private final FeatureOptimizationMethod optimizationMethod;
    private final VideoContentStream contentStream;
    private final VideoSequenceHandlerParameters myParameters;

    private final AbstractStrategy segmentationStrategy;
    private final AbstractStrategy featureExtractionStrategy;
    private final AbstractClassificationStrategy classificationStrategy;
    private final AbstractStrategy frameProcessingStrategy;
    private final AbstractOutcomeCombinationStrategy outcomeCombinationStrategy;
    private final AbstractOutcomeProcessingStrategy outcomeProcessingStrategy;

    private final IThreadedExperiment experiment;

    public VideoSequenceHandler(IThreadedExperiment experiment,
                                VideoSequenceHandlerParameters params,
                                ClassificationMethod classificationMethod,
                                FeatureExtractionMethod featureExtractionMethod,
                                FeatureOptimizationMethod optimizationMethod,
                                VideoContentStream contentStream)
            throws JCeliacGenericException
    {
        this.classificationMethod = classificationMethod;
        this.featureExtractionMethod = featureExtractionMethod;
        this.optimizationMethod = optimizationMethod;
        this.contentStream = contentStream;
        this.myParameters = params;

        this.segmentationStrategy = StrategyFactory.createStrategyHandler(params.getSegmentationParameters());
        this.featureExtractionStrategy = StrategyFactory.createStrategyHandler(params.getFeatureExtractionParameters(),
                                                                               featureExtractionMethod);
        this.classificationStrategy = StrategyFactory.createStrategyHandler(params.getClassificationParameters(),
                                                                            classificationMethod,
                                                                            optimizationMethod);
        this.frameProcessingStrategy = StrategyFactory.createStrategyHandler(params.getFrameProcessingParameters());
        this.outcomeCombinationStrategy = StrategyFactory.createStrategyHandler(params.getOutcomeCombinationParameters());
        this.outcomeProcessingStrategy = StrategyFactory.createStrategyHandler(params.getOutcomeProcessingParameters());

        this.experiment = experiment;

        boolean ret = StrategyTypeChecker.checkTypes((AbstractClassificationStrategy) this.classificationStrategy,
                                                     (AbstractOutcomeCombinationStrategy) this.outcomeCombinationStrategy,
                                                     (AbstractOutcomeProcessingStrategy) this.outcomeProcessingStrategy);
        if(false == ret) {
            throw new JCeliacGenericException("Incompatible Strategy-Chain!");
        }
    }

    @Override
    public void run()
    {
        try {
            Experiment.log(JCeliacLogger.LOG_INFO, "Starting Video Handler Thread!");
            Object output = perform();
            this.experiment.threadFinish(output);
            Experiment.log(JCeliacLogger.LOG_INFO, "Video Handler Thread finished!");
        } catch(JCeliacGenericException e) {
            Main.logException(JCeliacLogger.LOG_ERROR, e);
        }
    }

    /*
     * Execute the strategies for a single video sequence.
     */
    @SuppressWarnings("unchecked")
    public Object perform()
            throws JCeliacGenericException
    {
        ArrayList<Object> classificationOutcomes = new ArrayList<>();
        while(this.contentStream.hasNext()) {
            FrameSequence sequence = (FrameSequence) this.frameProcessingStrategy.perform(this.contentStream);
            ArrayList<SegmentedFrame> segmentedFrames = (ArrayList<SegmentedFrame>) this.segmentationStrategy.perform(sequence);
            ArrayList<DiscriminativeFeatures> extractedFeatures = (ArrayList<DiscriminativeFeatures>) this.featureExtractionStrategy.perform((ArrayList<SegmentedFrame>) segmentedFrames);
            ArrayList<?> tmpOutcomes = (ArrayList<?>) this.classificationStrategy.perform((ArrayList<DiscriminativeFeatures>) extractedFeatures);
            classificationOutcomes.addAll(tmpOutcomes);
        }
        return this.outcomeCombinationStrategy.perform((ArrayList<Object>) classificationOutcomes);
    }
}
