/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategyHandler;

import jceliac.video.strategies.outcomeCombination.*;
import jceliac.video.strategies.featureExtraction.*;
import jceliac.video.strategies.outcomeProcessing.*;
import jceliac.video.strategies.frameProcessing.*;
import jceliac.video.strategies.classification.*;
import jceliac.video.strategies.segmentation.*;
import jceliac.video.strategies.parameters.*;
import jceliac.JCeliacGenericException;
import jceliac.featureOptimization.*;
import jceliac.featureExtraction.*;
import jceliac.video.strategies.*;
import jceliac.classification.ClassificationMethod;

/**
 * Creates strategies based on user supplied configurations.
 * <p>
 * @author shegen
 */
public class StrategyFactory
{

    public static AbstractFrameProcessingStrategy createStrategyHandler(FrameProcessingStrategyParameters parameters)
            throws JCeliacGenericException
    {
        switch(parameters.getStrategy()) {
            case Downsample:
            case Default:
                return new FrameProcessingStrategyDownsampling(parameters);
        }
        throw new JCeliacGenericException("Unknown Frame Processing Strategy!");
    }

    public static AbstractSegmentationStrategy createStrategyHandler(SegmentationStrategyParameters parameters)
            throws JCeliacGenericException
    {
        switch(parameters.getStrategy()) {
            case TileAdjacentSquares:
                throw new UnsupportedOperationException("Tile Adjacent Squares not yet implemented!");
            case SingleSegment:
            case Default:
                return new SegmentationStrategySingleSegment(parameters);
        }
        throw new JCeliacGenericException("Unknown Segmentation Strategy!");
    }

    public static AbstractOutcomeCombinationStrategy createStrategyHandler(OutcomeCombinationStrategyParameters parameters)
            throws JCeliacGenericException
    {
        switch(parameters.getStrategy()) {
            case MajorityVote:
            case Default:
                return new OutcomeCombinationStrategyMajorityVote(parameters);
            case Null:
                return new OutcomeCombinationStrategyNull(parameters);
        }
        throw new JCeliacGenericException("Unknown Outcome Combination Strategy!");
    }

    public static AbstractFeatureExtractionStrategy createStrategyHandler(FeatureExtractionStrategyParameters parameters,
                                                                          FeatureExtractionMethod method)
            throws JCeliacGenericException
    {
        switch(parameters.getStrategy()) {
            case SeparateFramesSeparateSegments:
            case Default:
                return new FeatureExtractionStrategySFSS(parameters, method);
        }
        throw new JCeliacGenericException("Unknown Feature Extraction Strategy!");
    }

    public static AbstractClassificationStrategy createStrategyHandler(ClassificationStrategyParameters parameters,
                                                                       ClassificationMethod method,
                                                                       FeatureOptimizationMethod optimizationMethod)
            throws JCeliacGenericException
    {
        switch(parameters.getStrategy()) {
            case Null:
                return new ClassificationStrategyNull(parameters, method, optimizationMethod);
            case SeparateFeatures:
            case Default:
                return new ClassificationStrategySeparateFeatures(parameters, method, optimizationMethod);
        }
        throw new JCeliacGenericException("Unknown Feature Extraction Strategy!");
    }

    public static AbstractOutcomeProcessingStrategy createStrategyHandler(
            OutcomeProcessingStrategyParameters parameters)
            throws JCeliacGenericException
    {
        switch(parameters.getStrategy()) {
            case Null:
            case Default:
                return new OutcomeProcessingStrategyNull(parameters);
            case DisplayFeatures:
                return new OutcomeProcessingStrategyDisplayFeatures(parameters);
        }
        throw new JCeliacGenericException("Unknown Feature Extraction Strategy!");
    }
}
