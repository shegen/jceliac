/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video.strategyHandler;

import java.lang.annotation.*;

/**
 *
 * @author shegen
 */
@Retention(value = RetentionPolicy.RUNTIME) // annotation is retained at runtime
public @interface ReturnType
{
    /* The typeIdentifier identifies the name of the used type as a string
     * literal. We have one extra type defined: - "Input" defines that the
     * output type is the same as the input type.
     */

    String typeIdentifier();
}
