/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video;

import java.util.*;
import jceliac.tools.data.*;

/**
 * Removes identical frames from a frame Sequence.
 * <p>
 * @author shegen
 */
public class IdenticalFrameFilter
        extends FrameFilter
{

    @Override
    public void filter(ArrayList<Frame> sequence)
    {
        ArrayList<Frame> pruneList = new ArrayList<>();
        for(int i = 0; i < sequence.size(); i++) {
            for(int j = 0; j < sequence.size(); j++) {
                if(i == j) {
                    continue;
                }

                Frame f1, f2;
                f1 = sequence.get(i);
                f2 = sequence.get(j);
                if(f1.equals(f2)) {
                    pruneList.add(f1);
                }
            }
        }// have to delete from here else this would be concurrentModificationException
        for(Frame f : pruneList) {
            sequence.remove(f);
        }
    }
}
