/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.video;

import com.xuggle.xuggler.video.ConverterFactory;
import com.xuggle.xuggler.video.IConverter;
import com.xuggle.xuggler.IVideoPicture;
import com.xuggle.xuggler.IStreamCoder;
import jceliac.JCeliacGenericException;
import com.xuggle.xuggler.IContainer;
import java.awt.image.BufferedImage;
import com.xuggle.xuggler.IPacket;
import com.xuggle.xuggler.IStream;
import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.IError;
import jceliac.tools.data.Frame;
import com.xuggle.xuggler.*;
import java.util.*;

/**
 * MPEG2 Decoder based on xuggler / FFMPEG. shegen
 * <p>
 * @author shegen
 */
public class MPEG2Decoder
{

    private final String url;
    private final IContainer container;
    private final boolean mCloseOnEofOnly = false;
    private int processedFrames = 0;
    private final ArrayList<Frame> frameBuffer;
    private boolean finished = false;
    private int videoStreamID;
    private final int MIN_FRAME_BUFFER_SIZE = 5; // this should be greater than the frame sequence size

    public MPEG2Decoder(String url)
    {
        this.url = url;
        this.container = IContainer.make().copyReference();
        this.frameBuffer = new ArrayList<>();
    }

    public void init()
    {
        if(!IVideoResampler.isSupported(
                IVideoResampler.Feature.FEATURE_COLORSPACECONVERSION)) {
            throw new RuntimeException("you must install the GPL version"
                                       + " of Xuggler (with IVideoResampler support) for "
                                       + "this demo to work");
        }

    }

    /**
     * Retrives the correct stream coder from the container.
     * <p>
     * @return StreamCoder for type video.
     * <p>
     * @throws MPEG2FinishedException
     */
    private IStreamCoder getVideoStreamCoder() throws MPEG2FinishedException
    {
        // test valid stream index
        int numStreams = getContainer().getNumStreams();
        for(int streamIndex = 0; streamIndex < numStreams; streamIndex++) {
            IStream stream = getContainer().getStream(streamIndex);
            IStreamCoder coder = stream.getStreamCoder();
            ICodec.Type type = coder.getCodecType();

            if(type == ICodec.Type.CODEC_TYPE_VIDEO) {
                if(coder.isOpen() == false) {
                    int ret = coder.open();
                    if(ret < 0) {
                        throw new MPEG2FinishedException();
                    } else {
                        this.videoStreamID = streamIndex;
                        return coder;
                    }
                } else {
                    this.videoStreamID = streamIndex;
                    return coder;
                }
            }
        }
        throw new MPEG2FinishedException();
    }

    /**
     * Retrieves the next frame.
     * <p>
     * @return Next decoded frame.
     * <p>
     * @throws JCeliacGenericException
     */
    public synchronized Frame nextFrame()
            throws JCeliacGenericException
    {
        try {
            IVideoPicture picture = this.decodeNextPicture();

            BufferedImage image = this.convertPicture(picture);
            Frame frame = Frame.convertBufferedImageToFrameOptimized(image);
            this.processedFrames++;
            frame.setFrameIdentifier(this.processedFrames);
            return frame;
        } catch(MPEG2FinishedException e) {
            this.finished = true;
            return null;
        }
    }

    /**
     * Decodes the next picture from the stream.
     * <p>
     * @return Next decoded video picture.
     * <p>
     * @throws MPEG2FinishedException
     */
    protected IVideoPicture decodeNextPicture()
            throws MPEG2FinishedException
    {
        // if the container is not yet been opend, open it
        if(!isOpen()) {
            open();
        }
        IPacket packet = IPacket.make();
        IStreamCoder videoCoder = this.getVideoStreamCoder();
        IVideoResampler resampler;

        int rv;
        while((rv = getContainer().readNextPacket(packet)) >= 0) {

            if(packet.getStreamIndex() == this.videoStreamID) {
                IVideoPicture picture = IVideoPicture.make(videoCoder.getPixelType(),
                                                           videoCoder.getWidth(),
                                                           videoCoder.getHeight());
                int offset = 0;
                while(offset < packet.getSize()) {
                    int r = videoCoder.decodeVideo(picture, packet, offset);
                    if(r < 0) {
                        throw new RuntimeException("Error decoding Video!");
                    }
                    offset += r;
                    if(picture.isComplete()) {

                        // Convert to correct format
                        if(videoCoder.getPixelType() != IPixelFormat.Type.BGR24) {
                            // if this stream is not in BGR24, we're going to need to
                            // convert it.  The VideoResampler does that for us.
                            resampler = IVideoResampler.make(videoCoder.getWidth(),
                                                             videoCoder.getHeight(), IPixelFormat.Type.BGR24,
                                                             videoCoder.getWidth(), videoCoder.getHeight(), videoCoder.getPixelType());
                            if(resampler == null) {
                                throw new RuntimeException("could not create color space "
                                                           + "resampler for: " + this.url);
                            }

                            IVideoPicture newPic = IVideoPicture.make(resampler.getOutputPixelFormat(),
                                                                      picture.getWidth(), picture.getHeight());
                            if(resampler.resample(newPic, picture) < 0) {
                                throw new RuntimeException("could not resample video from: "
                                                           + this.url);
                            }

                            if(newPic.getPixelType() != IPixelFormat.Type.BGR24) {
                                throw new RuntimeException("could not decode video"
                                                           + " as BGR 24 bit data in: " + this.url);
                            }
                        }

                        return picture;

                    }
                }
            }
        }
        if(rv < 0) {
            IError error = IError.make(rv);
            // if this is an end of file, or unknow, call close

            if(!this.mCloseOnEofOnly || IError.Type.ERROR_EOF == error.getType()) {
                videoCoder.close();
                throw new MPEG2FinishedException();
            }
            return null;
        }
        throw new RuntimeException("Video decoding failed!");
    }

    /**
     * Converts a video picture to a buffered image.
     * <p>
     * @param picture
     * <p>
     * @return
     */
    protected BufferedImage convertPicture(IVideoPicture picture)
    {
        if(picture == null) {
            return null;
        }
        ConverterFactory.Type convType = ConverterFactory.findRegisteredConverter(ConverterFactory.XUGGLER_BGR_24);
        if(convType == null) {
            throw new UnsupportedOperationException("No converter found!");
        }
        IConverter converter = ConverterFactory.createConverter(convType.getDescriptor(), picture);
        BufferedImage image = converter.toImage(picture);
        return image;
    }

    /**
     * Get the underlying media {@link IContainer} that the {@link IMediaCoder}
     * is reading from or writing to. The returned {@link IContainer} can be
     * further interrogated for media stream details.
     * <p>
     * @return the media container.
     */
    public final IContainer getContainer()
    {
        return this.container == null ? null : this.container.copyReference();
    }

    public boolean isOpen()
    {
        return getContainer().isOpened();
    }

    public void open()
    {
        if(getContainer().open(this.url, IContainer.Type.READ, null) < 0) {
            throw new RuntimeException("could not open: " + this.url);
        }

    }

    public void close()
    {
        if(getContainer().close() < 0) {
            throw new RuntimeException("error failed close IContainer ");
        }
    }

    public int getCurrentFrameNumber()
    {
        return this.processedFrames;
    }

    public int getFrameBufferSize()
    {
        return this.frameBuffer.size();
    }

    /**
     * This method is called by the DecoderThread to push new frames into the
     * frame buffer. The nextFrame() method blocks if no decoded frames are
     * there to return. This method wakes it up.
     * <p>
     * @param frames List of decoded frames added to the buffer.
     */
    public synchronized void addFramesToBuffer(ArrayList<Frame> frames)
    {
        this.frameBuffer.addAll(frames);
        this.notifyAll(); // wake up the wait() from nextFrame()
    }

    public synchronized boolean bufferContainsFrames()
    {
        return this.frameBuffer.size() > 0;
    }

    public boolean isIsFinished()
    {
        return this.finished;
    }
}
