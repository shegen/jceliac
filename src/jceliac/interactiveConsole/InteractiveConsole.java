/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.interactiveConsole;

import java.util.Scanner;

/**
 * This implements an interactive console to configure tests.
 * <p>
 * @author shegen
 */
public class InteractiveConsole
{

    private final CommandHandler handlerChain;

    public InteractiveConsole()
    {
        CommandHandler unknownCommand = new UnknownCommandHandler(null);
        this.handlerChain = new HelpCommandHandler(unknownCommand);
    }

    public void run()
    {

        String cmdLine;
        try(Scanner sc = new Scanner(System.in)) {
            while(true) {

                printCMDLine();
                cmdLine = sc.nextLine();

                String[] args = cmdLine.split("\\s+");

                if(args[0].equalsIgnoreCase("exit")) {
                    break;
                }

                if(args.length == 2) {
                    handlerChain.handleCommand(args[0], args[1]);
                } else {
                    handlerChain.handleCommand(args[0], "");
                }
            }
        }
    }

    private void printCMDLine()
    {
        System.out.print("celiac console:> ");
    }
}
