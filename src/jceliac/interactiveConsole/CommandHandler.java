/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.interactiveConsole;

/**
 * Chain of Responsibility.
 *
 * @author shegen
 */
public abstract class CommandHandler {

    protected String command = null;
    protected CommandHandler next = null;

    public CommandHandler(CommandHandler next) {
        this.next = next;
    }

    protected abstract void handle(String argument);

    public void handleCommand(String command, String argument) {
        if (this.command.equalsIgnoreCase(command)) {
            handle(argument);
        } else {
            if (this.next != null) {
                this.next.handle(argument);
            }
        }
    }
}
