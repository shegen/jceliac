/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification;

import java.io.Serializable;
import jceliac.experiment.*;
import jceliac.logging.*;

/**
 * This class stores a configuration of a classification method.
 * <p>
 * This represents the classification method configuration at the time a result
 * was achieved. This will be associated with a ClassificationResult and will be
 * used while logging results.
 * <p>
 * @author shegen
 */
public abstract class ClassificationConfiguration
        implements Serializable
{

    /**
     *
     * @return the name of the specific classification method
     */
    protected abstract String getClassificationMethodName();

    /**
     * Implements method specific logging.
     */
    public abstract void doReport();

    /**
     * Called by the experiments to log common method parameters.
     */
    public void report()
    {
        Experiment.log(JCeliacLogger.LOG_INFO, "Classification Method: %s", getClassificationMethodName());
        doReport();
    }
}
