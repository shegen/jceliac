/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification.classificationParameters;

import jceliac.classification.classificationParameters.ClassificationParametersKNN.ENeighborEvaluationMode;
import java.io.Serializable;
import jceliac.experiment.*;

/**
 * The basic ClassificationParameters such as cross-validation type,
 * optimization type and number of folds.
 * <p>
 * ToDo: Somehow it seems that these information is not necessarily
 * classification specific. Maybe these should be re factored.
 * <p>
 * @author shegen
 */
public abstract class ClassificationParameters
        extends AbstractParameters
        implements Cloneable, Serializable
{

    /**
     * Defines a type of cross validation.
     */
    public static enum ECrossValidationType
    {

        /**
         * Leave One Out Cross Validation.
         */
        LOOCV,
        /**
         * Distinct Sets for Training and Evaluation.
         */
        DISTINCT,
        /**
         * Leave One Patient Out Cross Validation.
         */
        LOPO,
        /**
         * K-Fold Cross Validation.
         */
        KFOLD,
        /**
         * K-Fold Cross Validation combined with Leave One Patient Out Cross
         * Validation.
         */
        KFOLD_LOPO,
        /**
         * Distinct set for Training and Evaluation, uses best global value for
         * k (k-NN).
         */
        DISTINCT_BEST_GLOBAL_K;

        public static ECrossValidationType valueOfIgnoreCase(String name)
                throws InitializeFailedException
        {
            for(ECrossValidationType value : ECrossValidationType.values()) {
                if(value.name().equalsIgnoreCase(name)) {
                    return value;
                }
            }
            throw new InitializeFailedException("Unknown Classification Method: " + name);
        }
    };

    /**
     * Defines the type of optimization.
     */
    public static enum EOptimizationType
    {

        /**
         * Inner optimization optimizes parameters based on an extra cross
         * validation, this is the fairest method and should be used.
         */
        INNER,
        /**
         * Outer optimization optimizes parameters based on the result of the
         * cross validation that is used to compute the results, should not be
         * used to avoid over-fitting.
         */
        OUTER;

        public static EOptimizationType valueOfIgnoreCase(String name)
                throws InitializeFailedException
        {
            for(EOptimizationType value : EOptimizationType.values()) {
                if(value.name().equalsIgnoreCase(name)) {
                    return value;
                }
            }
            throw new InitializeFailedException("Unknown Classification Method: " + name);
        }

    };

    protected String crossValidationType = "LOOCV";
    protected String optimizationType = "OUTER";
    protected int kFolds = -1;

    public int getkFolds()
    {
        return kFolds;
    }

    public void setkFolds(int kFolds)
    {
        this.kFolds = kFolds;
    }

    /**
     * Maps from String to ECrossValidationType.
     * <p>
     * @param typeString Name of cross validation type.
     * <p>
     * @return Enum type of cross validation.
     * <p>
     * @throws InitializeFailedException
     */
    public static ECrossValidationType mapCrossValidationType(String typeString)
            throws InitializeFailedException
    {

        try {
            return ECrossValidationType.valueOfIgnoreCase(typeString);
        } catch(IllegalArgumentException e) {
            throw new InitializeFailedException(e.getMessage());
        }
    }

    /**
     * Maps from cross validation enum type to the types identifier as string.
     * <p>
     * @param type Cross validation enum type.
     * <p>
     * @return Type identifier as string.
     */
    public static String mapCrossValidationType(ECrossValidationType type)
    {
        return type.name();
    }

    /**
     * Maps from String to EOptimizationType.
     * <p>
     * @param typeString Name of optimization type.
     * <p>
     * @return Enum type of optimziation type.
     * <p>
     * @throws InitializeFailedException
     */
    public static EOptimizationType mapOptimizationType(String typeString)
            throws InitializeFailedException
    {
        try {
            return EOptimizationType.valueOf(typeString);
        } catch(IllegalArgumentException e) {
            throw new InitializeFailedException(e.getMessage());
        }
    }

    /**
     * Maps from optimziation enum type to the types identifier as string.
     * <p>
     * @param type Optimization enum type.
     * <p>
     * @return Type identifier as string.
     */
    public static String mapOptimizationType(EOptimizationType type)
    {
        return type.name();
    }

    /**
     * Map from string to evaluation mode (knn) enum.
     * <p>
     * @param modeString String name of evaluation mode (differs from enum type
     *                   name).
     * <p>
     * @return Enum type corresponding to the mode string.
     * <p>
     * @throws InitializeFailedException If the mode string can not be mapped to
     *                                   the enum type.
     */
    public static ENeighborEvaluationMode mapNeighborEvaluationMode(String modeString)
            throws InitializeFailedException
    {

        return ENeighborEvaluationMode.valueOfIgnoreCase(modeString);

    }

    /**
     * Map from neighborhood evaluation mode (knn) to string identifier.
     * <p>
     * @param mode Neighborhood evaluation mode enum type.
     * <p>
     * @return String identifier associated with enum type.
     * <p>
     */
    public static String mapNeighborEvaluationMode(ENeighborEvaluationMode mode)
    {
        return mode.stringName();
    }

    public ECrossValidationType getCrossValidationType()
            throws InitializeFailedException
    {
        return mapCrossValidationType(this.crossValidationType);
    }

    public void setCrossValidationType(String crossValidationType)
            throws InitializeFailedException
    {
        this.crossValidationType = crossValidationType;
        mapCrossValidationType(crossValidationType); // check validity
    }

    public EOptimizationType getOptimizationType()
            throws InitializeFailedException
    {
        return mapOptimizationType(this.optimizationType);
    }

    public void validateParameters()
            throws InitializeFailedException
    {
        mapCrossValidationType(this.crossValidationType);
    }

    @Override
    public Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }

}
