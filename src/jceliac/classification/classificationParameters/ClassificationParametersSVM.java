/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification.classificationParameters;

import jceliac.experiment.*;
import jceliac.logging.*;
import jceliac.*;

/**
 * Parameters of the SVM classifier.
 * <p>
 * @author shegen
 */
public class ClassificationParametersSVM
        extends ClassificationParameters
{

    // Parameters for One Class SVM are only nu
    protected String SVMType = "NU_SVC";

    /**
     * Fraction of samples to be within estimated region,
     * teset from 0.1 to 0.8.
     */
    protected double nu = 0.4;      
    protected double epsilon = 0.01;

    /* 
     * Slack variable in C-SVM, however we are using nu-SVM,
     * this controlls the tradeoff between a wide margin and
     * classifier error, test in magnitued of 10-4 to 1.5 or so.
     * 
     */
    protected double C = 0.5;
    
    /**
     * Controls how smooth or contorted the decision boundary will be. 
     * Smaller gammas lead to smoother decision boundaries while larger gammas tend to overfit the svm.
     */
    protected double gamma = 0.001;
    protected String kernelType = "RBF";

    /**
     * Maps kernel string identifiers to libsvm kernel ids.
     * <p>
     * @return libsvm kernel type as integer.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public int getKernelType()
            throws JCeliacGenericException
    {
        if(this.kernelType.compareToIgnoreCase("RBF") == 0) {
            return libsvm.svm_parameter.RBF;
        }
        if(this.kernelType.compareToIgnoreCase("HISTOGRAM_INTERSECTION") == 0) {
            return libsvm.svm_parameter.HISTOGRAM_INTERSECTION;
        }
        Main.log(JCeliacLogger.LOG_ERROR, "", this.kernelType);
        throw new JCeliacGenericException("Unknown SVM Kernel Type");
    }

    /**
     * Maps string representation of SVM type to libsvm type.
     * <p>
     * @return libsvm type of the SVM string identifier.
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public int getSVMType()
            throws JCeliacGenericException
    {
        if(this.SVMType.compareToIgnoreCase("ONE_CLASS") == 0) {
            return libsvm.svm_parameter.ONE_CLASS;
        }

        if(this.SVMType.compareToIgnoreCase("NU_SVC") == 0) {
            return libsvm.svm_parameter.NU_SVC;
        }
        Main.log(JCeliacLogger.LOG_ERROR, "Unknown SVM Type (%s)!", SVMType);
        throw new JCeliacGenericException("Unknown SVM Type");
    }

    public void setSVMType(String type)
    {
        this.SVMType = type;
    }

    public double getEpsilon()
    {
        return this.epsilon;
    }

    public void setEpsilon(double epsilon)
    {
        this.epsilon = epsilon;
    }

    public double getNu()
    {
        return nu;
    }

    public void setNu(double nu)
            throws JCeliacGenericException
    {
        if(nu > 1) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Nu values must be in [0,1]");
            throw new JCeliacGenericException("Nu values must be in [0,1]");
        }
        this.nu = nu;
    }

    public double getC()
    {
        return C;
    }

    public void setC(double C)
    {
        this.C = C;
    }

    public double getGamma()
    {
        return gamma;
    }

    public void setGamma(double gamma)
    {
        this.gamma = gamma;
    }
}
