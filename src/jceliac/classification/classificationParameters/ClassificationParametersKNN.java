/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification.classificationParameters;

import jceliac.experiment.InitializeFailedException;

/**
 * Specific parameters for kNN classifiers.
 * <p>
 * @author shegen
 */
public class ClassificationParametersKNN
        extends ClassificationParameters
{

    /**
     * Distance type to use.
     */
    public static enum EDistanceType
    {

        /**
         * Histogram Intersection.
         */
        HISTOGRAM_INTERSECT, // Histogram Intersection

        /**
         * Euclidean Distance.
         */
        EUCLIDEAN
    }

    /**
     * Defines how the k-values of k-NN are handled.
     */
    public static enum ENeighborEvaluationMode
    {

        /**
         * Best value for k (with highest classification result) between kMin
         * and kMax.
         */
        BEST_K_VALUE("BEST"),
        /**
         * Mean classification accuracy over all k-values between kMin and kMax.
         */
        MEAN_STD_OVER_RANGE("MEAN");

        private final String stringName;

        private ENeighborEvaluationMode(String stringName)
        {
            this.stringName = stringName;

        }

        public String stringName()
        {
            return this.stringName;
        }

        public static ENeighborEvaluationMode valueOfIgnoreCase(String stringName)
                throws InitializeFailedException
        {
            for(ENeighborEvaluationMode mode : ENeighborEvaluationMode.values()) {
                if(mode.stringName.equalsIgnoreCase(stringName)) {
                    return mode;
                }
            }
            throw new InitializeFailedException("Can not map  " + stringName + " to a enum type.");
        }

    };
    // 

    /**
     * We mainly use LBP based features, therefore we use histogram intersection
     * by default.
     */
    protected EDistanceType distanceType = EDistanceType.HISTOGRAM_INTERSECT;

    /**
     * Default mode for handling neighbors in k-NN. I changed the default from
     * best k-value to the mean on 16.01.2014, because it's much fairer and we
     * have been using this for over a year now anyways.
     */
    protected String neighborEvaluationMode = ClassificationParameters.mapNeighborEvaluationMode(ENeighborEvaluationMode.MEAN_STD_OVER_RANGE);
    protected int kMax = 25;
    protected int kMin = 1;

    public int getkMax()
    {
        return kMax;
    }

    public void setkMax(int kMAX)
    {
        this.kMax = kMAX;
    }

    public int getkMin()
    {
        return kMin;
    }

    public void setkMin(int kMin)
    {
        this.kMin = kMin;
    }

    public EDistanceType getDistanceType()
    {
        return distanceType;
    }

    public void setDistanceType(EDistanceType distanceType)
    {
        this.distanceType = distanceType;
    }

    public ENeighborEvaluationMode getNeighborEvaluationMode()
            throws InitializeFailedException
    {

        return ClassificationParameters.mapNeighborEvaluationMode(neighborEvaluationMode);
    }

    public void setNeighborEvaluationMode(ENeighborEvaluationMode mode)
            throws InitializeFailedException
    {
        this.neighborEvaluationMode = ClassificationParameters.mapNeighborEvaluationMode(mode);
    }

}
