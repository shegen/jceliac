/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.classification.KNN;

import jceliac.classification.IterableClassificationParameterOptimizer;
import jceliac.classification.ClassificationMethod;
import jceliac.classification.ClassificationMapping;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.classificationParameters.ClassificationParameters;
import jceliac.classification.classificationParameters.ClassificationParametersKNN;
import jceliac.experiment.Experiment;
import jceliac.experiment.InitializeFailedException;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
public class ClassificationMethodKNN 
    extends ClassificationMethod
{  
    private final ClassificationParametersKNN myParameters;
    private FeatureCollection currentlyTrainedTrainingFeatures;
    private FeatureCollection currentlyTrainedEvaluationFeatures;
    private TrainedFeatureDistances trainedDistances;
    
    public ClassificationMethodKNN(ClassificationParameters parameters)
    {
        super(parameters);
        this.myParameters = (ClassificationParametersKNN) parameters;
    }

    @Override
    public synchronized void hint(FeatureCollection trainingFeatures, 
                                  FeatureCollection evaluationFeatures)
            throws JCeliacGenericException
    {
        if(evaluationFeatures == null || evaluationFeatures.isEmpty()) {
            this.computeDistancesForCV(trainingFeatures);
        }
        else if(evaluationFeatures.isEmpty() == false &&
                trainingFeatures.isEmpty() == false)
        {
            this.computeDistancesForDistinctValidation(trainingFeatures, evaluationFeatures);
        }
    }

    private void computeDistancesForCV(FeatureCollection trainingFeatures) 
            throws JCeliacGenericException
    {
        ForkJoinDistanceComputationCV distanceComputation = new ForkJoinDistanceComputationCV(trainingFeatures.getFeatures(), 0, trainingFeatures.size());
        ForkJoinPool pool = new ForkJoinPool();
        HashMap <Long, LabeledDistance[]> distances = pool.invoke(distanceComputation);
        pool.shutdownNow();
        
        this.currentlyTrainedTrainingFeatures = trainingFeatures;
        this.trainedDistances = new TrainedFeatureDistances(distances, trainingFeatures); 
    }
    
    private void computeDistancesForDistinctValidation(FeatureCollection trainingFeatures, 
                                                       FeatureCollection evaluationFeatures) 
            throws JCeliacGenericException
    {
        ForkJoinDistanceComputationDistinctValidation distanceComputation = 
            new ForkJoinDistanceComputationDistinctValidation(trainingFeatures.getFeatures(),
                                                              evaluationFeatures.getFeatures(), 
                                                              0, evaluationFeatures.size());
        ForkJoinPool pool = new ForkJoinPool();
        HashMap <Long, LabeledDistance[]> distances = pool.invoke(distanceComputation);
        pool.shutdownNow();
        
        this.currentlyTrainedTrainingFeatures = trainingFeatures;
        this.trainedDistances = new TrainedFeatureDistances(distances, trainingFeatures); 
    }
    
    /*
     * this is the portion of data that is to be handed over to the cpu to have
     * an even balancing of work among cores; this is hardcoded due to lack of
     * time an algorithm for adaptively finding those values is:
     *
     * given N feature vectors, the total area of computed points is (N*N)/2
     * because we use the fact that a metric has to be symmetric and reflexive.
     * hence the chunk of work for each core or cpu should be ((N*N)/2 ) /
     * #CORES step1: start with the first offset factor and compute the width
     * such that the area starting at xLeft = 0 equals ((N*N)/2 ) / #CORES step
     * 2-N: from there on compute the next width setting xLeft to the width of
     * the last chunk done XXX: this should be implemented
     */
    private final double[] OFFSET_FACTOR_TABLE_4CORE = {0.500, 0.700, 0.865, 1.0};
    private final double[] OFFSET_FACTOR_TABLE_8CORE = {0.355, 0.502, 0.615, 0.712, 0.795, 0.870, 0.938, 1.0};

  
   /**
     * Distributes the work of computing feature vector distances in a
     * non-linear fashion such that each cpu-core has an equal number of work.
     * This is needed if cross validation is performed and only the upper
     * triangle matrix is computed instead of the entire distance matrix.
     * <p>
     * @param featureCount Number of feature vectors.
     * @param procs        Number of cpu cores in use.
     * <p>
     * @return Indices into the matrix of feature vectors that define the
     *         portions of work for each cpu core.
     * <p>
     * @throws JCeliacGenericException
     */
    private int[] distributeDataOffsetsNonLinear(int featureCount, int procs)   
    {
        int[] offsets = new int[procs];
        double[] offsetFactors;
        switch(procs) {
            case 4:
                offsetFactors = OFFSET_FACTOR_TABLE_4CORE;
                break;
            case 8:
                offsetFactors = OFFSET_FACTOR_TABLE_8CORE;
                break;
            default:
                Experiment.log(JCeliacLogger.LOG_INFO, "Adaptiv chunk size distribution not yet implemented, falling back to linear!");
                return null;
        }
        for(int i = 0; i < procs; i++) {
            offsets[i] = (int) Math.ceil(featureCount * offsetFactors[i]);
        }
        return offsets;
    }

    /**
     * Distributes the work for computing feature vector distances linearly.
     * <p>
     * @param featureCount Number of feature vectors.
     * @param procs        Number of cpu cores in use.
     * <p>
     * @return Indices into the matrix of feature vectors that define the
     *         portions of work for each cpu core.
     * <p>
     * @throws JCeliacGenericException
     */
    private int[] distributeDataOffsetsLinear(int featureCount, int procs)
            throws JCeliacGenericException
    {
        int[] offsets = new int[procs];
        double offsetFactor = (double) featureCount / (double) procs;
        for(int i = 0; i < procs; i++) {
            offsets[i] = (int) Math.ceil((i + 1) * offsetFactor);
        }
        return offsets;
    }

    /**
     * This optimizes the performance of knn as the computed distances using another
     * ClassificationConfiguration can possibly be reused if the training data did not change. 
     * If methods can not re train or there is no benefit of re-training with a different
     * configuration, 
     * 
     * @param trainedMapping
     * @param configuration
     * @return 
     */
    @Override
    public ClassificationMapping reTrainMapping(ClassificationMapping trainedMapping,
                                                            ClassificationConfiguration configuration)
    {
        // copy the trained mapping and set the correct configuration
        ClassificationMappingKNN clonedMapping = (ClassificationMappingKNN)trainedMapping.newInstance();
        clonedMapping.reTrainMapping(configuration);
        return clonedMapping;
    }
    

    @Override
    public ClassificationMapping train(FeatureCollection trainingFeatures, 
                                                   ClassificationConfiguration configuration)
    {
       // not a lot to do in case of knn
        ClassificationMappingKNN mapping = 
            new ClassificationMappingKNN(trainingFeatures, 
                                                     (ClassificationConfigurationKNN) configuration,
                                                     this.trainedDistances);
        mapping.trainMapping();
        return mapping;
    }

    
    @Override
    public IterableClassificationParameterOptimizer initializeOptimization()
    {
       return new IterableClassificationParameterOptimizerKNN(this.myParameters);
    }  

    @Override
    public EClassificationMethodType getClassificationMethodType()
    {
        return EClassificationMethodType.KNN;
    }
    
    private class ForkJoinDistanceComputationCV 
                extends RecursiveTask <HashMap <Long, LabeledDistance[]>>
    {
        protected ArrayList <DiscriminativeFeatures> fullFeatureList;

        protected int workThreshold;
        protected int startIndex;
        protected int length;
        
        public ForkJoinDistanceComputationCV(ArrayList<DiscriminativeFeatures> fullFeatureList, 
                                           int startIndex, int length)
        {
            this.fullFeatureList = fullFeatureList;
            this.startIndex = startIndex;
            this.length = length;
            
            // compute work threshold based on number of cores
            int numCores = Runtime.getRuntime().availableProcessors();
            this.workThreshold = (this.fullFeatureList.size()/ numCores);
        }

        @Override
        protected HashMap <Long, LabeledDistance[]> compute()
        {
            if(this.length <= this.workThreshold)
            {
                return this.computeDirectly();
            }
            int split = this.length / 2;
            ForkJoinDistanceComputationCV left = new ForkJoinDistanceComputationCV(this.fullFeatureList, this.startIndex, split);
            ForkJoinDistanceComputationCV right = new ForkJoinDistanceComputationCV(this.fullFeatureList, this.startIndex+split, this.length - split);
            invokeAll(left, right);
            HashMap <Long, LabeledDistance[]> leftDistances = left.join();
            HashMap <Long, LabeledDistance[]> rightDistances = right.join();
            
            HashMap <Long, LabeledDistance[]> joinedDistances = new HashMap <>();
            joinedDistances.putAll(leftDistances);
            joinedDistances.putAll(rightDistances);
            return joinedDistances;
        }

        protected HashMap <Long, LabeledDistance[]> computeDirectly()
        {
            HashMap <Long, LabeledDistance[]> distances = new HashMap <>(); 
            try {
                ArrayList<DiscriminativeFeatures> workFeatureList = 
                    new ArrayList <>(this.fullFeatureList.subList(this.startIndex, this.startIndex + this.length));
                
                for(DiscriminativeFeatures workFeature : workFeatureList) {
                    ArrayList <LabeledDistance> curDistanceList = new ArrayList <>();
                    
                    for(DiscriminativeFeatures tmpFullFeature : this.fullFeatureList) {
                        
                        double distance = workFeature.distanceTo(tmpFullFeature);
                        LabeledDistance labeledDistance = new LabeledDistance(distance, tmpFullFeature.getClassNumber(),
                                                                              workFeature.getClassNumber(), tmpFullFeature,
                                                                              workFeature.getFeatureID(), tmpFullFeature.getFeatureID());
                        curDistanceList.add(labeledDistance);
                    }
                    LabeledDistance[] distancesArray = curDistanceList.toArray(new LabeledDistance[curDistanceList.size()]);
                    Arrays.parallelSort(distancesArray);
                    distances.put(workFeature.getFeatureID(), distancesArray);
                }
                return distances;
            } catch(JCeliacGenericException e) {
                Experiment.logException(JCeliacLogger.LOG_ERROR, e);
            }
            return null;
        }  
    }
    
    
     private class ForkJoinDistanceComputationDistinctValidation 
                extends RecursiveTask <HashMap <Long, LabeledDistance[]>>
    {
        protected ArrayList <DiscriminativeFeatures> trainingFeatures;
        protected ArrayList <DiscriminativeFeatures> evaluationFeatures;

        protected int workThreshold;
        protected int evaluationStartIndex;
        protected int evaluationLength;
        
        
        public ForkJoinDistanceComputationDistinctValidation(
                                           ArrayList<DiscriminativeFeatures> trainingFeatures,
                                           ArrayList<DiscriminativeFeatures> evaluationFeatures,
                                           int evaluationStartIndex, int evaluationLength)
        {
            this.trainingFeatures = trainingFeatures;
            this.evaluationFeatures = evaluationFeatures;
            this.evaluationLength = evaluationLength;
            this.evaluationStartIndex = evaluationStartIndex;
            
            // compute work threshold based on number of cores
            int numCores = Runtime.getRuntime().availableProcessors();
            this.workThreshold = (this.evaluationFeatures.size() / numCores);
        }

        @Override
        protected HashMap <Long, LabeledDistance[]> compute()
        {
            if(this.evaluationLength <= this.workThreshold)
            {
                return this.computeDirectly();
            }
            int split = this.evaluationLength / 2;
            ForkJoinDistanceComputationDistinctValidation left = 
                new ForkJoinDistanceComputationDistinctValidation(this.trainingFeatures, 
                                                                  this.evaluationFeatures, 
                                                                  this.evaluationStartIndex, split);
            ForkJoinDistanceComputationDistinctValidation right = 
                new ForkJoinDistanceComputationDistinctValidation(this.trainingFeatures, 
                                                                  this.evaluationFeatures, 
                                                                  this.evaluationStartIndex+split, 
                                                                  this.evaluationLength - split);
            invokeAll(left, right);
            HashMap <Long, LabeledDistance[]> leftDistances = left.join();
            HashMap <Long, LabeledDistance[]> rightDistances = right.join();
            
            HashMap <Long, LabeledDistance[]> joinedDistances = new HashMap <>();
            joinedDistances.putAll(leftDistances);
            joinedDistances.putAll(rightDistances);
            return joinedDistances;
        }

        protected HashMap <Long, LabeledDistance[]> computeDirectly()
        {
            HashMap <Long, LabeledDistance[]> distances = new HashMap <>(); 
            try {
                ArrayList<DiscriminativeFeatures> workFeatureList = 
                    new ArrayList <>(this.evaluationFeatures.subList(
                                                this.evaluationStartIndex, 
                                                this.evaluationStartIndex + this.evaluationLength));
                
                for(DiscriminativeFeatures workFeature : workFeatureList) {
                    ArrayList <LabeledDistance> curDistanceList = new ArrayList <>();
                    
                    for(DiscriminativeFeatures tmpTrainingFeature : this.trainingFeatures) {
                        
                        double distance = workFeature.distanceTo(tmpTrainingFeature);
                        LabeledDistance labeledDistance = new LabeledDistance(distance, tmpTrainingFeature.getClassNumber(),
                                                                              workFeature.getClassNumber(), tmpTrainingFeature,
                                                                              workFeature.getFeatureID(), tmpTrainingFeature.getFeatureID());
                        curDistanceList.add(labeledDistance);
                    }
                    LabeledDistance[] distancesArray = curDistanceList.toArray(new LabeledDistance[curDistanceList.size()]);
                    Arrays.parallelSort(distancesArray);
                    distances.put(workFeature.getFeatureID(), distancesArray);
                }
                return distances;
            } catch(JCeliacGenericException e) {
                Experiment.logException(JCeliacLogger.LOG_ERROR, e);
            }
            return null;
        }  
    }

    @Override
    public boolean supportsROC()
            throws InitializeFailedException
    {
       return (this.myParameters.getNeighborEvaluationMode() == 
               ClassificationParametersKNN.ENeighborEvaluationMode.MEAN_STD_OVER_RANGE);
    }
    
    
}
