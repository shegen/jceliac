/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification.KNN;

import jceliac.featureExtraction.DiscriminativeFeatures;

/**
 * LabeledDistance wraps the distance between two samples with additional
 * information.
 * <p>
 * The additional information is the class label as well as a reference to the
 * feature vector of the neighbor associated with the distance.
 * <p>
 * @author shegen
 */
public class LabeledDistance
        implements Comparable, Cloneable
{
    // signals that the current distance is valid for classification, this can be set to false if the training set changed
    public boolean isValid = true;
    public double distance;
    public int neighborClassLabel;
    public int myClassLabel;
    public DiscriminativeFeatures originalNeighbor;
    
    public long featureVectorID;
    public long neighborFeatureVectorID;
    

    /**
     *
     * @param distance         Distance between two feature vectors.
     * @param neighborClassNumber      Class number of feature vector.
     * @param myClassNumber
     * @param originalNeighbor Neighbor defining distance.
     * @param featureVectorID
     * @param neighborFeatureVectorID
     */
    public LabeledDistance(double distance, int neighborClassNumber, 
                           int myClassNumber, DiscriminativeFeatures originalNeighbor,
                           long featureVectorID, long neighborFeatureVectorID)
    {
        this.distance = distance;
        this.originalNeighbor = originalNeighbor;
        this.neighborClassLabel = neighborClassNumber;
        this.myClassLabel = myClassNumber;
        this.featureVectorID = featureVectorID;
        this.neighborFeatureVectorID = neighborFeatureVectorID;
    }

    @Override
    public int compareTo(Object o)
    {
        LabeledDistance neighbor = (LabeledDistance) o;
        if(this.distance < neighbor.distance) {
            return -1; // order by smallest first
        } else if(this.distance == neighbor.distance) {
            return 0;
        }
        return 1;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        LabeledDistance cloned = (LabeledDistance) super.clone();
        // share the same reference to the neighboring feature vector
        cloned.originalNeighbor = this.originalNeighbor;
        return cloned;
    }

}
