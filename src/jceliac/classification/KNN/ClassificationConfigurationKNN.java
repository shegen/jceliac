/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification.KNN;

import jceliac.classification.classificationParameters.ClassificationParametersKNN.ENeighborEvaluationMode;
import jceliac.classification.*;
import jceliac.experiment.*;
import jceliac.logging.*;

/**
 * This contains the classification configuration of a KNN classifier. Which is
 * in fact only the value of k.
 * <p>
 * @author shegen
 */
public class ClassificationConfigurationKNN
        extends ClassificationConfiguration
{

    private final int kValue;
    private final int kMin;
    private final int kMax;

    private ENeighborEvaluationMode evaluationMode;

    /**
     *
     * @param evaluationMode Defines the mode of evaluation, best global k-value
     *                       or the mean and standard deviation over all values.
     * @param kMin
     * @param kMax
     * @param kValue
     */
    public ClassificationConfigurationKNN(ENeighborEvaluationMode evaluationMode,
                                          int kMin,
                                          int kMax,
                                          int kValue)
    {
        super();
        this.evaluationMode = evaluationMode;
        this.kMin = kMin;
        this.kMax = kMax;
        this.kValue = kValue; 
    }

    public ClassificationConfigurationKNN()
    {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Logs the used value of k.
     */
    @Override
    public void doReport()
    {
        Experiment.log(JCeliacLogger.LOG_INFO, "k-Value: %d", kValue);
    }

    @Override
    protected String getClassificationMethodName()
    {
        return "kNN";
    }

    public int getkValue()
    {
        if(this.getEvaluationMode() == ENeighborEvaluationMode.MEAN_STD_OVER_RANGE) {
            return -1;
        }else {
            return this.kValue;
        }
    }

    public ENeighborEvaluationMode getEvaluationMode()
    {
        return evaluationMode;
    }

    public int getkMax()
    {
        return kMax;
    }

    public int getkMin()
    {
        return kMin;
    }
}
