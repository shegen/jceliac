/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.classification.KNN;

import jceliac.classification.IterableClassificationParameterOptimizer;
import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.ClassificationResult;
import jceliac.classification.KNN.ClassificationConfigurationKNN;
import jceliac.classification.classificationParameters.ClassificationParameters;
import jceliac.classification.classificationParameters.ClassificationParametersKNN;
import jceliac.experiment.InitializeFailedException;

/**
 * Does not implement batch optimization as the knn uses reTrainedMappings
 * to retain stored distances, hence multi-threaded training does not improve
 * the performance. 
 * 
 * @author shegen
 */
public class IterableClassificationParameterOptimizerKNN 
    extends IterableClassificationParameterOptimizer
{

    protected ClassificationParametersKNN myParameters;
    protected int currentKValue;
    
    
    public IterableClassificationParameterOptimizerKNN(ClassificationParameters parameters)
    {
        super(parameters);
        this.myParameters = (ClassificationParametersKNN)parameters;
    }
    
    @Override
    public ClassificationConfiguration startOptimization()
            throws InitializeFailedException
    {
        this.currentKValue = this.myParameters.getkMin();
        ClassificationConfigurationKNN config
                                       = new ClassificationConfigurationKNN(this.myParameters.getNeighborEvaluationMode(),
                                                                            this.myParameters.getkMin(),
                                                                            this.myParameters.getkMax(),
                                                                            this.currentKValue);
        return config;
    }

    @Override
    public ClassificationConfiguration iterateOptimization(ClassificationResult result,
                                                           ClassificationConfiguration resultConfig)
            throws InitializeFailedException
    {
        this.currentKValue++;
        ClassificationConfigurationKNN config
                                       = new ClassificationConfigurationKNN(this.myParameters.getNeighborEvaluationMode(),
                                                                            this.myParameters.getkMin(),
                                                                            this.myParameters.getkMax(),
                                                                            this.currentKValue);
        return config;
    }

    @Override
    public boolean iterationConverged()
            throws InitializeFailedException
    {
        // if mean of range is computed we do not optimize anything hence we are converged
        if(this.myParameters.getNeighborEvaluationMode() == 
           ClassificationParametersKNN.ENeighborEvaluationMode.MEAN_STD_OVER_RANGE) {
            return true;
        }
        if(this.currentKValue > this.myParameters.getkMax()) {
            return true;
        }
        return false;
    }   
}
