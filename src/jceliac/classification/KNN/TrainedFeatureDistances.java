/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at> 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.classification.KNN;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;

/**
 *
 * @author shegen
 */
public class TrainedFeatureDistances
{
    // key = ID of evaluation 
    protected HashMap <Long, LabeledDistance[]> trainedDistances;
    protected ArrayList <Long> trainedFeatureIDs;
    
    public TrainedFeatureDistances(HashMap<Long, LabeledDistance[]> trainedDistances, 
                                   FeatureCollection trainedFeatures)
            throws JCeliacGenericException
    {
        this.trainedDistances = trainedDistances;
        this.trainedFeatureIDs = new ArrayList<>(trainedFeatures.getFeatures().
                                                                 stream().map(a -> a.getFeatureID()).
                                                                 collect(Collectors.toList()));
    }
    
    public boolean containsKey(Long key) {
        return this.trainedDistances.containsKey(key);
    }
    
    public LabeledDistance [] get(Long key) {
        return this.trainedDistances.get(key);
    }
    
    /**
     * Runs through the list of labeled distances and invalidates all distances
     * which are not in the training features. 
     * 
     * @param trainingFeatures 
     */
    public void train(FeatureCollection trainingFeatures)
    {
        TreeSet <Long> featureIDs = trainingFeatures.getFeatureIDs();
        for(LabeledDistance [] distances : this.trainedDistances.values()) {
            for(int i = 0; i < distances.length; i++) {
                if(distances[i].featureVectorID == distances[i].neighborFeatureVectorID) {
                    distances[i].isValid = false;
                }
                // if the distance is not a valid training feature set to invalid
                if(false == featureIDs.contains(distances[i].featureVectorID) &&
                   false == featureIDs.contains(distances[i].neighborFeatureVectorID)) {
                    distances[i].isValid = false;
                }else {
                    distances[i].isValid = true;
                }
            }
        }
    }
    
    /** 
     * Checks if the trained distance is still valid, this is the case if all
     * training features are contained in the trained distance. 
     * 
     * @param trainingFeatures
     * @return 
     */
    public boolean isValidForTrainingData(FeatureCollection trainingFeatures)
    {
        for(DiscriminativeFeatures tmp : trainingFeatures)
        {
            if(false == this.trainedFeatureIDs.contains(tmp.getFeatureID())) {
                return false;
            }
        }
        return true;
    }
}
