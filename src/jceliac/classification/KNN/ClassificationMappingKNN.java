/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification.KNN;

import jceliac.classification.ClassificationMapping;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.ClassificationOutcome;
import jceliac.classification.classificationParameters.ClassificationParametersKNN;
import jceliac.experiment.Experiment;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.arrays.ArrayTools;

/**
 *
 * @author shegen
 */
public class ClassificationMappingKNN
        extends ClassificationMapping
{

    // cache for distances, this must be invalidated on a new training()
    // key is an evaluation featureID, value is the sorted list of neighbors of this specific feature
    // neighbor cache is shared across all trained mapping, hence it has to be concurrent
    protected ConcurrentHashMap<Long, LabeledDistance[]> neighborCache;
    protected int[] trainingClassFrequencies;
    protected int numberOfClasses;
    protected Random rand;
    protected long seed = 1234567890;
    private int MAX_CACHE_NEIGHBORS = 50;
    private int maxAllowedKValue = -1;
 
    protected TrainedFeatureDistances trainedDistances;
    protected ClassificationConfigurationKNN myConfiguration;
    public ClassificationMappingKNN(FeatureCollection trainingFeatures,
                                                ClassificationConfigurationKNN configuration,
                                                TrainedFeatureDistances trainedDistances)
    {
        super(trainingFeatures, configuration);
        this.neighborCache = new ConcurrentHashMap<>();
        this.trainedDistances = trainedDistances;
        this.myConfiguration = configuration;
    }

    /**
     * This creates a copy of the current mapping, but shares the
     * training class frequencies, number of classes and also 
     * the neighbor cache. This should only be called in reTrainMapping()
     * @return 
     */
    @Override
    public ClassificationMapping newInstance()
    {
        /**
         * Training features and configuration are immutable.
         * 
         */
       ClassificationMappingKNN newMapping = 
                        new ClassificationMappingKNN(this.trainingFeatures, 
                                                                 (ClassificationConfigurationKNN)this.configuration,
                                                                 this.trainedDistances);
       newMapping.neighborCache = this.neighborCache;
       newMapping.trainingClassFrequencies = this.trainingClassFrequencies;
       newMapping.numberOfClasses = this.numberOfClasses;
       return newMapping;
    }

    /**
     * Trains the classifier mapping by figuring out class frequencies to
     * resolve ties.
     */
    @Override
    protected void trainMapping()
    {
        this.rand = new Random(this.seed);
        // figure out number of classes
        HashMap<Integer, Integer> classLabelCount = new HashMap<>();
        for(DiscriminativeFeatures f : this.trainingFeatures) {
            classLabelCount.put(f.getClassNumber(), 1);
        }
        this.numberOfClasses = classLabelCount.size();
        // compute out class frequencies
        this.trainingClassFrequencies = new int[this.numberOfClasses];
        for(DiscriminativeFeatures f : this.trainingFeatures) {
            this.trainingClassFrequencies[f.getClassNumber()]++;
        }
        // store no more than 50 neighbors per feature vector
        this.MAX_CACHE_NEIGHBORS = Math.min(this.trainingFeatures.size(), 50);
        if(this.trainedDistances != null) {
            if(false == this.trainedDistances.isValidForTrainingData(this.trainingFeatures)) {
                this.trainedDistances = null;
            }else {
                this.trainedDistances.train(this.trainingFeatures);
            }
        }
        this.maxAllowedKValue = ArrayTools.min(this.trainingClassFrequencies);
    }

    /**
     * Trains the classifier mapping by figuring out class frequencies to
     * resolve ties.
     * <p>
     * @param configuration The new configuration fo the mapping.
     */
    @Override
    protected void reTrainMapping(ClassificationConfiguration configuration)
    {
        this.rand = new Random(this.seed);
        this.configuration = (ClassificationConfigurationKNN) configuration;
    }
    
    @Override
    public ArrayList<ClassificationOutcome> map(DiscriminativeFeatures evaluationFeatures)
            throws JCeliacGenericException
    {
        if(this.myConfiguration.getkMax() > MAX_CACHE_NEIGHBORS) {
            Experiment.log(JCeliacLogger.LOG_WARN, "Maximum k-Value > MAX_CACHE_NEIGHBORS, classification could be slow!");
        }
        
        ArrayList<ClassificationOutcome> ret = new ArrayList<>();
        if(this.myConfiguration.getEvaluationMode()
           == ClassificationParametersKNN.ENeighborEvaluationMode.BEST_K_VALUE) {
            ClassificationOutcome outcome = this.classifySingleFeatureVectorFast(evaluationFeatures,
                                                                                 this.myConfiguration.getkValue(),
                                                                                 // for now just request k-neighbors
                                                                                 this.myConfiguration.getkValue());
            ret.add(outcome);
        } else if(this.myConfiguration.getEvaluationMode()
                  == ClassificationParametersKNN.ENeighborEvaluationMode.MEAN_STD_OVER_RANGE) {
            int maxK = Math.min(this.maxAllowedKValue, this.myConfiguration.getkMax());
            for(int kValue = this.myConfiguration.getkMin(); 
                    kValue <= maxK; 
                    kValue++) {
                ClassificationOutcome outcome = this.classifySingleFeatureVectorFast(evaluationFeatures,
                                                                                     kValue,
                                                                                     kValue);
                ret.add(outcome);
            }
        }
        return ret;
    }
    
    @SuppressWarnings("unchecked")
    private LabeledDistance[] computeLabeledDistances(DiscriminativeFeatures evalFeature,
                                                      int numFeatures)
            throws JCeliacGenericException
    {
        LabeledDistance[] distances = new LabeledDistance[this.trainingFeatures.size()];
        int index = 0;
        
        for(DiscriminativeFeatures trainFeature : this.trainingFeatures) {
            double distance = evalFeature.distanceTo(trainFeature);
            LabeledDistance labeledDistance = new LabeledDistance(distance, trainFeature.getClassNumber(),
                                                                  evalFeature.getClassNumber(), trainFeature,
                                                                  evalFeature.getFeatureID(), trainFeature.getFeatureID());
            distances[index] = labeledDistance;
            index++;
        }
        Arrays.parallelSort(distances);
        LabeledDistance[] ret = new LabeledDistance[numFeatures];
        System.arraycopy(distances, 0, ret, 0, Math.min(distances.length, ret.length));
        return ret;
    }
    
    /**
     * Resolves a tie based on class prior probabilities (class frequencies).
     * Based on the class frequencies of the training data, a random value is
     * used to resolve a tie with appropriate probabilities.
     */
    private int resolveTie(boolean[] tiedClasses, int numTiedClasses)
            throws JCeliacGenericException
    { 
        int[] classLabelMapping = new int[numTiedClasses];
        int index = 0;
        for(int i = 0; i < tiedClasses.length; i++) {
            if(true == tiedClasses[i]) {
                classLabelMapping[index] = i;
                index++;
            }
        }
        int numFeatures = 0;
        for(int i = 0; i < numTiedClasses; i++) {
            numFeatures += this.trainingClassFrequencies[classLabelMapping[i]];
        }
        // compute borders for assigning a class using random values
        // by computing the cumulative sum and normalizing the frequencies
        double[] trainingClassFrequencyBorder = new double[numTiedClasses];
        for(int i = 0; i < numTiedClasses; i++) {
            double cumulativeSum = 0;
            for(int j = 0; j <= i; j++) {
                cumulativeSum += this.trainingClassFrequencies[classLabelMapping[j]];
            }
            trainingClassFrequencyBorder[i] = cumulativeSum / numFeatures;
        }
        // generate random value
        double randValue = this.rand.nextDouble();
        // decide tie
        for(int i = 0; i < trainingClassFrequencyBorder.length; i++) {
            if(randValue <= trainingClassFrequencyBorder[i]) {
                return classLabelMapping[i];
            }
        }
        // should not be reached
        throw new JCeliacGenericException("Supposed to be unreached, seems like a BUG!");
    }
    
    private ClassificationOutcome classifySingleFeatureVectorFast(DiscriminativeFeatures evalFeature,
                                                                  int kValue,
                                                                  int numberOfRequestedNeighbors)
            throws JCeliacGenericException
    {
        if(Math.max(kValue, numberOfRequestedNeighbors) > this.trainingFeatures.size()) {
            throw new JCeliacGenericException("kValue or number of requested neighbors > number of training features!");
        }
        int[] classCounts = new int[this.numberOfClasses];
        ClassificationOutcome outcome = new ClassificationOutcome();
        LabeledDistance[] allNeighbors;
        
        if(this.trainedDistances != null && 
           this.trainedDistances.containsKey(evalFeature.getFeatureID())) 
        {
            allNeighbors = this.trainedDistances.get(evalFeature.getFeatureID());
        }else {
            allNeighbors = this.computeLabeledDistances(evalFeature, Math.max(MAX_CACHE_NEIGHBORS, kValue));
        }
       
       
        if(numberOfRequestedNeighbors > 0) {
            int exportedNeighbors = 0;
            // store the n nearest neighbors in the outcome
            for(int i = 0; i < allNeighbors.length && exportedNeighbors < numberOfRequestedNeighbors; i++) {
                if(allNeighbors[i].isValid) {
                    outcome.addNeighbor(exportedNeighbors, allNeighbors[i].neighborClassLabel);
                    exportedNeighbors++;
                }
            }
        }
       
        int consideredNeighbors = 0;
        for(int i = 0; i < allNeighbors.length && consideredNeighbors < kValue; i++) {
            if(allNeighbors[i].isValid) {
                int label = allNeighbors[i].neighborClassLabel;
                classCounts[label]++;
                consideredNeighbors++;
            }
        }
        int max = -1;
        int maxClassNumber = -1;
        for(int i = 0; i < numberOfClasses; i++) {
            if(classCounts[i] > max) {
                max = classCounts[i];
                maxClassNumber = i;
            }
        }
        boolean[] tiedClasses = new boolean[numberOfClasses];
        int tieCount = 0;

        // verify that there is no tie
        for(int i = 0; i < numberOfClasses; i++) {
            if(classCounts[i] == max) {
                tiedClasses[i] = true;
                tieCount++;
            } else {
                tiedClasses[i] = false;
            }
        }
        if(tieCount >= 2) // we have a tie between classes decide by using the class frequencies
        {
            int tiedClass = this.resolveTie(tiedClasses, tieCount);
            outcome.setEstimatedClass(tiedClass);
            outcome.setTied(true);
            return outcome;
        } else {
            outcome.setEstimatedClass(maxClassNumber);
            return outcome;
        }
    }
    
    
}
