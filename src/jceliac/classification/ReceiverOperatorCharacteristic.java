/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification;

/**
 * Implements the Receiver Operator Characteristic measure.
 * <p>
 * @author shegen
 */
public class ReceiverOperatorCharacteristic
{

    private final int[] ROCcorrect;
    private final int[] ROCwrong;

    /**
     *
     * @param ROCcorrect Number of correct outcomes per k-value.
     * @param ROCwrong   Number of incorrect outcomes per k-value.
     */
    public ReceiverOperatorCharacteristic(int[] ROCcorrect, int[] ROCwrong)
    {
        this.ROCcorrect = ROCcorrect;
        this.ROCwrong = ROCwrong;
    }

    /**
     *
     * @return The ROC rates per index.
     */
    public double[] getSingleRates()
    {
        double[] rates = new double[ROCcorrect.length];
        for(int i = 0; i < rates.length; i++) {
            rates[i] = (this.ROCcorrect[i] * 100) / (this.ROCwrong[i] + this.ROCcorrect[i]);
        }
        return rates;
    }

    /**
     *
     * @return The Area under Curve of the ROC.
     */
    public double getAUC()
    {
        double[] rates = this.getSingleRates();
        // use trapezoidal integration
        double area = 0;

        for(int i = 0; i < rates.length-1; i++) {
            area += 0.5 * (rates[i] + rates[i + 1]);
        }
        area  = area / (rates.length-1);
        return area;
    }
}
