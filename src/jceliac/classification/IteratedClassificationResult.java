/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */
package jceliac.classification;

import java.io.File;
import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;
import jceliac.tools.math.MathTools;

/**
 *
 * @author shegen
 */
public class IteratedClassificationResult
{

    private final ArrayList<ClassificationResult> resultsPerIteration;

    public IteratedClassificationResult()
    {
        this.resultsPerIteration = new ArrayList<>();
    }

    public ClassificationResult getSingleResult(int index)
    {
        return this.resultsPerIteration.get(index);
    }

    public int getNumberOfResults()
    {
        return this.resultsPerIteration.size();
    }

    public void addSingleResult(ClassificationResult result)
    {
        this.resultsPerIteration.add(result);
    }

    /**
     * Queries the classification accuracy of a single class based on a class
     * identifier.
     * <p>
     * @param identifier Identifier of a class.
     * <p>
     * @return A double value between 0 and 1 corresponding to the
     *         classification accuracy of a single class.
     */
    public double getClassificationRate(String identifier)
    {
        double overallRate = 0;
        for(int i = 0; i < this.resultsPerIteration.size(); i++) {
            ClassificationResult result = this.resultsPerIteration.get(i);
            double rate = result.getClassificationRate(identifier);
            overallRate += rate;
        }
        overallRate /= this.resultsPerIteration.size();
        return overallRate;
    }

    /**
     * Queries the classification accuracy of a single class based on a class
     * number.
     * <p>
     * @param classNumber Number of class.
     * <p>
     * @return A double value between 0 and 1 corresponding to the
     *         classification accuracy of a single class.
     */
    public double getClassificationRate(int classNumber)
    {
        double overallRate = 0;
        for(int i = 0; i < this.resultsPerIteration.size(); i++) {
            ClassificationResult result = this.resultsPerIteration.get(i);
            double rate = result.getClassificationRate(classNumber);
            overallRate += rate;
        }
        overallRate /= this.resultsPerIteration.size();
        return overallRate;
    }

    /**
     * This methods returns the overall classification rate which is the number
     * of correct classified images divided by the number of all classified
     * images.
     * <p>
     * @return A double value between 0 and 1 indicating the classification
     *         accuracy.
     */
    public double getOverallClassificationRate()
    {
        double overallRate = 0;
        for(int i = 0; i < this.resultsPerIteration.size(); i++) {
            ClassificationResult result = this.resultsPerIteration.get(i);
            double rate = result.getOverallClassificationRate();
            overallRate += rate;
        }
        overallRate /= this.resultsPerIteration.size();
        return overallRate;
    }

    public double getStandardDeviation()
    {
        double[] rates = new double[this.resultsPerIteration.size()];
        for(int i = 0; i < this.resultsPerIteration.size(); i++) {
            ClassificationResult result = this.resultsPerIteration.get(i);
            double rate = result.getOverallClassificationRate();
            rates[i] = rate;
        }
        return MathTools.stdev(rates);
    }

    public FeatureVectorSubsetEncoding getFeatureSubset()
            throws JCeliacGenericException
    {
        if(this.resultsPerIteration.size() != 1) {
            throw new JCeliacGenericException("Looks like iterated result is used for optimization with more than 1 iteration!");
        } else {
            return this.resultsPerIteration.get(0).getFeatureSubset();
        }
    }

    public ClassificationConfiguration getConfiguration()
            throws JCeliacGenericException
    {
        if(this.resultsPerIteration.size() != 1) {
            throw new JCeliacGenericException("Looks like iterated result is used for optimization with more than 1 iteration!");
        } else {
            return this.resultsPerIteration.get(0).getConfiguration();
        }
    }

    public boolean hasClassIdentifier(String identifier)
    {
        for(int i = 0; i < this.resultsPerIteration.size(); i++) {
            if(this.resultsPerIteration.get(0).hasClassIdentifier(identifier)) {
                return true;
            }
        }
        return false;
    }

    public int getTieCount()
    {
        int ties = 0;
        for(int i = 0; i < this.resultsPerIteration.size(); i++) {
            ties += this.resultsPerIteration.get(i).getTieCount();
        }
        return ties;
    }

    public int getGuessCount()
    {
        int guesses = 0;
        for(int i = 0; i < this.resultsPerIteration.size(); i++) {
            guesses += this.resultsPerIteration.get(i).getGuessCount();
        }
        return guesses;
    }

    public ReceiverOperatorCharacteristic computeROC()
            throws JCeliacGenericException
    {
        if(this.resultsPerIteration.size() == 1) {
            return this.resultsPerIteration.get(0).computeROC();
        } else {
            throw new JCeliacGenericException("Iterated result treated as single result.");
        }
    }

    public ArrayList<ClassificationOutcome> getClassificationOutcomes()
    {
        ArrayList<ClassificationOutcome> allOutcomes = new ArrayList<>();
        for(int i = 0; i < this.resultsPerIteration.size(); i++) {
            allOutcomes.addAll(this.resultsPerIteration.get(i).getClassificationOutcomes());
        }
        return allOutcomes;
    }

    public void clearData()
    {
        ArrayList<ClassificationOutcome> allOutcomes = new ArrayList<>();
        for(int i = 0; i < this.resultsPerIteration.size(); i++) {
            this.resultsPerIteration.get(i).clearData();
        }
    }
    
    public void exportAccuracies(File outfile) {
        
    }
}
