/*
 * 
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at> 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * Each line should be prefixed with  * 
 */
package jceliac.classification;

import jceliac.experiment.InitializeFailedException;

/**
 * Defines types for implemented classification methods.
 */
public enum EClassificationMethod
{

    /**
     * k-nearest neighbor classifier
     */
    KNN, 
    /**
     * Kernel SVM based on libsvm. 
     */
    KERNEL_SVM,
    
    /**
     * Linear SVM using liblinear. 
     */
    LINEAR_SVM;

    
    public static EClassificationMethod valueOfIgnoreCase(String name)
            throws InitializeFailedException
    {
        for(EClassificationMethod value : EClassificationMethod.values()) {
            if(value.name().equalsIgnoreCase(name)) {
                return value;
            }
        }
        throw new InitializeFailedException("Unknown Classification Method: " + name);
    }

}
