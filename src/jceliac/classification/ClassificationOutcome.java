/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification;


import jceliac.featureOptimization.*;
import jceliac.featureExtraction.*;
import java.io.Externalizable;
import java.io.ObjectOutput;
import java.io.IOException;
import java.io.ObjectInput;
import java.util.*;

/**
 * This class holds the information about single classification outcomes.
 * <p>
 * This is stored in OnlineExperiments in the corresponding database table. It is also 
 * used to compute the mean classification accuracies of multiple k-values. 
 * <p>
 * @author shegen
 */
public class ClassificationOutcome
        implements Externalizable
{
    /**
     * The feature vector of this specific outcome.
     */
    protected DiscriminativeFeatures features;

    /**
     * The class estimated for the feature vector by the classifier.
     */
    private int estimatedClass;

    /**
     * Indicates if the estimated class was guessed due to an unresolvable tie.
     */
    protected boolean guessed = false;

    /**
     * Indicates the estimated class was guessed resolving a tie using class frequencies. 
     */
    protected boolean tied = false;

    /**
     * A list of the nearest neighbors (k-NN only).
     */
    protected ArrayList<Integer> nearestNeighbors;

    /**
     * The used feature subset.
     */
    protected FeatureVectorSubsetEncoding featureSubset;

    /**
     * Standard Constructor. 
     */
    public ClassificationOutcome()
    {
        this.nearestNeighbors = new ArrayList<>();
    }

    /**
     * Add a neighbor to a specific position for a specific class.
     * @param pos Position of neighbor. 
     * @param classIndex Indicates the class of the neighbor. 
     */
    public void addNeighbor(int pos, int classIndex)
    {
        this.nearestNeighbors.add(pos, classIndex);
    }

    /**
     * 
     * @return The estimated class. 
     */
    public int getEstimatedClass()
    {
        return estimatedClass;
    }

    /**
     *
     * @return The feature specific to the outcome. 
     */
    public DiscriminativeFeatures getFeatures()
    {
        return this.features;
    }

    /**
     *
     * @return The list of nearest neighbors. 
     */
    public ArrayList<Integer> getNearestNeighbors()
    {
        return this.nearestNeighbors;
    }

    /**
     * Sets the estimated class by the classifier for the specific feature vector.
     * @param estimatedClass The estimated class. 
     */
    public void setEstimatedClass(int estimatedClass)
    {
        this.estimatedClass = estimatedClass;
    }

    /**
     * 
     * @param featureVector Sets the feature vector for the outcome.
     */
    public void setFeatureVector(DiscriminativeFeatures featureVector)
    {
        this.features = featureVector;
    }

    /**
     * @return A boolean value indicating if the estimated class was guessed due to an unresolvable tie.
     */
    public boolean isGuessed()
    {
        return this.guessed;
    }

    /**
     * @param guessed A boolean value indicating if the estimated class was guessed due to an unresolvable tie.
     */
    public void setGuessed(boolean guessed)
    {
        this.guessed = guessed;
    }

    /**
     * @return A boolean value indicating if the estimated class was subject to a resolvable tie. 
     */
    public boolean isTied()
    {
        return this.tied;
    }

    /**
     * @param tied A boolean value indicating if the estimation was subject to a resolvable tie. 
     */
    public void setTied(boolean tied)
    {
        this.tied = tied;
    }

    /**
     * Returns the feature subset used during classification. 
     * @return The current feature vector subset. 
     */
    public FeatureVectorSubsetEncoding getFeatureSubset()
    {
        return this.featureSubset;
    }

    public void setFeatureSubset(FeatureVectorSubsetEncoding featureSubset)
    {
        this.featureSubset = featureSubset;
    }

    /**
     *
     * @return A boolean value indicating if the estimated class is the correct class.
     */
    public boolean isCorrect()
    {
        return this.estimatedClass == this.features.getClassNumber();
    }

    /*
     * Implementation of the Externalizable Interface
     */
    @Override
    public void readExternal(ObjectInput oi) throws IOException,
                                                    ClassNotFoundException
    {
        this.estimatedClass = (Integer) oi.readObject();
        this.features = (DiscriminativeFeatures) oi.readObject();
    }

    @Override
    public void writeExternal(ObjectOutput oo) throws IOException
    {
        oo.writeObject(this.estimatedClass);
        oo.writeObject(this.features);
    }

}
