/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.classification.kernelSVM;

import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.kernelSVM.ClassificationParametersKernelSVM.ESVMKernelType;
import jceliac.experiment.Experiment;
import jceliac.classification.kernelSVM.ClassificationParametersKernelSVM.ESVMType;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
public class ClassificationConfigurationKernelSVM 
    extends ClassificationConfiguration
{

    protected double gamma;         // gamma in kernel function, default 1/num_features
    protected double C;             // cost parmeter in C-SVM, default 1000
    protected double nu;            // fraction of support vectors in ONE_CLASS SVM, default 0.5
    protected ESVMType SVMType;
    protected ESVMKernelType kernelType;
    
    public ClassificationConfigurationKernelSVM(ESVMType SVMType,
                                                double gamma, 
                                                double C, 
                                                double nu,
                                                ESVMKernelType kernelType)
    {
        this.SVMType = SVMType;
        this.gamma = gamma;
        this.C = C;
        this.nu = nu;
        this.kernelType = kernelType;
    }

    @Override
    protected String getClassificationMethodName()
    {
        return "Kernel SVM";
    }
  
    /**
     * Logs the classification configuratino of the SVM.
     */
    @Override
    public void doReport()
    {
        Experiment.log(JCeliacLogger.LOG_INFO, "C: %f", this.C);
        Experiment.log(JCeliacLogger.LOG_INFO, "gamma: %f", this.gamma);
        Experiment.log(JCeliacLogger.LOG_INFO, "nu: %f", this.nu);
    }

    public double getGamma()
    {
        return gamma;
    }

    public double getC()
    {
        return C;
    }

    public double getNu()
    {
        return nu;
    }

    public int getKernelType()
    {
        return kernelType.getValue();
    }
    
}