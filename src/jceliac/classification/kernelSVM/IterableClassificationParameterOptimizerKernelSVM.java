/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.classification.kernelSVM;

import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.ClassificationResult;
import jceliac.classification.classificationParameters.ClassificationParameters;
import jceliac.classification.IterableClassificationParameterOptimizer;

/**
 * This actually does nothing yet. 
 * 
 * @author shegen
 */
public class IterableClassificationParameterOptimizerKernelSVM
    extends IterableClassificationParameterOptimizer
{

    protected ClassificationParametersKernelSVM myParameters;
    public IterableClassificationParameterOptimizerKernelSVM(ClassificationParameters parameters)
    {
        super(parameters);
        this.myParameters = (ClassificationParametersKernelSVM) parameters;
    }

    @Override
    public boolean iterationConverged() throws JCeliacGenericException
    {
      return true;
    }

    @Override
    public ClassificationConfiguration startOptimization() 
            throws JCeliacGenericException
    {
        ClassificationConfigurationKernelSVM configuration = 
                new ClassificationConfigurationKernelSVM(this.myParameters.getSvmType(),
                                                         this.myParameters.getGamma(),
                                                         this.myParameters.getC(),
                                                         this.myParameters.getNu(),
                                                         this.myParameters.getSvmKernelType());
        return configuration;
        
    }

    @Override
    public ClassificationConfiguration iterateOptimization(ClassificationResult result, 
                                                           ClassificationConfiguration config)
            throws JCeliacGenericException
    {
        return config;
    }
    
}
