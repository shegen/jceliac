/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification.kernelSVM;

import jceliac.JCeliacGenericException;
import jceliac.classification.classificationParameters.ClassificationParameters;
import jceliac.experiment.Experiment;
import jceliac.logging.JCeliacLogger;
import libsvm.svm_parameter;

/**
 *
 * @author shegen
 */
public class ClassificationParametersKernelSVM
        extends ClassificationParameters
{

    protected double gamma = Double.NaN; // default will be 1 / num features
    protected double nu = 0.5;
    protected double C = 1d; 
    protected String kernelType = "RBF";
    protected String svmType = "C_SVC";
    protected String oneClassTrainingClassName = "";
    
    public ClassificationParametersKernelSVM()
    {
    }

    public static enum ESVMType
    {

        C_SVC(libsvm.svm_parameter.C_SVC),
        ONE_CLASS(libsvm.svm_parameter.ONE_CLASS);

        private final int value;

        private ESVMType(int value)
        {
            this.value = value;
        }

        public int getValue()
        {
            return value;
        }

        public static ClassificationParametersKernelSVM.ESVMType valueOfIgnoreCase(String s)
        {
            for(ClassificationParametersKernelSVM.ESVMType type : ClassificationParametersKernelSVM.ESVMType.values()) {
                if(type.name().equalsIgnoreCase(s)) {
                    return type;
                }
            }
            Experiment.log(JCeliacLogger.LOG_WARN, "Unknown SVM Type, using C_SVC");
            return C_SVC;
        }
    }
    
    public static enum ESVMKernelType
    {

        RBF(libsvm.svm_parameter.RBF),
        HISTOGRAM_INTERSECTION(libsvm.svm_parameter.HISTOGRAM_INTERSECTION),
        LINEAR(libsvm.svm_parameter.LINEAR);

        private final int value;

        private ESVMKernelType(int value)
        {
            this.value = value;
        }

        public int getValue()
        {
            return value;
        }

        public static ClassificationParametersKernelSVM.ESVMKernelType valueOfIgnoreCase(String s)
                throws JCeliacGenericException
        {
            for(ClassificationParametersKernelSVM.ESVMKernelType type : 
                ClassificationParametersKernelSVM.ESVMKernelType.values()) {
                if(type.name().equalsIgnoreCase(s)) {
                    return type;
                }
            }
            throw new JCeliacGenericException("Unknown SVM Kernel Type");
        }
    }

    public double getC()
    {
        return this.C;
    }

    public double getGamma()
    {
        return gamma;
    }

    public ESVMType getSvmType()
    {
        return ClassificationParametersKernelSVM.ESVMType.valueOfIgnoreCase(this.svmType);
    }
    
    public ESVMKernelType getSvmKernelType()
            throws JCeliacGenericException
    {
        return ClassificationParametersKernelSVM.ESVMKernelType.valueOfIgnoreCase(this.kernelType);
    }

    public double getNu()
    {
        return nu;
    }

    public String getOneClassTrainingClassName()
    {
        return oneClassTrainingClassName;
    }
}
