/*
 *  JCeliac Copyright (C) 2010-2011 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification.kernelSVM;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.ClassificationOutcome;
import jceliac.classification.ClassificationOutcomeOneClass;
import jceliac.classification.ClassificationMapping;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_problem;

/**
 *
 * @author shegen
 */
public class ExperimentalClassificationMappingKernelSVM
        extends ClassificationMapping
{
    private svm_model trainedModel;
    private svm_parameter svmConfiguration;
    protected ClassificationConfigurationKernelSVM myConfiguration;
    protected int oneClassTrainingClassNumber;

    public ExperimentalClassificationMappingKernelSVM(FeatureCollection trainingFeatures,
                                                      ClassificationConfiguration configuration)
    {
        super(trainingFeatures, configuration);
        this.myConfiguration = (ClassificationConfigurationKernelSVM) configuration;
        this.svmConfiguration = this.createSVMConfiguration(this.myConfiguration);
    }

    private synchronized svm_parameter
            createSVMConfiguration(ClassificationConfigurationKernelSVM configuration)
    {
        svm_parameter svmConfig = new svm_parameter();

        // set parameters for libsvm, parameters that are not configurable by the
        // user are set to the default values of libsvm
        svmConfig = new svm_parameter();
        svmConfig.svm_type = configuration.SVMType.getValue();
        svmConfig.kernel_type = configuration.getKernelType(); 
        svmConfig.degree = 3;
        svmConfig.coef0 = 0; // coef0 in kernel funtion
        svmConfig.nu = configuration.getNu();
        svmConfig.cache_size = 100; // cache memory size in MB
        svmConfig.C = configuration.getC();
        if(Double.isNaN(configuration.getGamma())) {
           svmConfig.gamma = 1.0d / this.trainingFeatures.size(); // default
        }else {
           svmConfig.gamma = configuration.getGamma();
        }
        svmConfig.eps = 0.01;
        svmConfig.p = 0.1;
        svmConfig.shrinking = 1; // use shrinink heuristic
        svmConfig.probability = 0; // do not train probability estimates
        svmConfig.nr_weight = 0;
        svmConfig.weight_label = new int[0];
        svmConfig.weight = new double[0];

        return svmConfig;
    }

    @Override
    public ClassificationMapping newInstance()
    {
        ExperimentalClassificationMappingKernelSVM newMapping
                                                   = new ExperimentalClassificationMappingKernelSVM(this.trainingFeatures,
                                                                                                    this.configuration);
        return newMapping;
    }

    @Override
    protected synchronized void trainMapping()
            throws JCeliacGenericException
    {
        svm_node[][] svmTrainingFeatures = new svm_node[this.trainingFeatures.size()][];
        double[] groundTruth = new double[this.trainingFeatures.size()];
        
        if(this.myConfiguration.SVMType == ClassificationParametersKernelSVM.ESVMType.ONE_CLASS)
        {
            boolean valid = this.isValidForOneClassSVM(this.trainingFeatures);
            if(valid == false) {
                throw new JCeliacGenericException("ONE_CLASS SVM training set expected to consist of only one class!");
            }
            this.oneClassTrainingClassNumber = this.trainingFeatures.get(0).getClassNumber();
        }
        
        for(int index = 0; index < this.trainingFeatures.size(); index++) {
            DiscriminativeFeatures feature = this.trainingFeatures.get(index);
            svmTrainingFeatures[index] = feature.getNormalizedFeaturesForSVM();
            groundTruth[index] = (double) feature.getClassNumber();
        }
        svm_problem problem = new libsvm.svm_problem();
        problem.l = svmTrainingFeatures.length; // number of feature vectors
        problem.x = svmTrainingFeatures; // feature vectors
        problem.y = groundTruth; // ground truth
        this.trainedModel = libsvm.svm.svm_train(problem, this.svmConfiguration);
    }

    /**
     * Checks if all supplied features are the supplied training class. 
     * 
     * @param features
     * @return 
     */
    private boolean isValidForOneClassSVM(FeatureCollection features)
    {
        int classNum = -1;
        for(DiscriminativeFeatures tmp : features) {
            if(classNum == -1) {
                classNum = tmp.getClassNumber();
                continue;
            }
            if(classNum != tmp.getClassNumber()) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    protected synchronized void reTrainMapping(ClassificationConfiguration configuration)
            throws JCeliacGenericException
    {
        this.myConfiguration = (ClassificationConfigurationKernelSVM) configuration;
        this.svmConfiguration = this.createSVMConfiguration(this.myConfiguration);
        this.trainMapping();
    }

    @Override
    public ArrayList<ClassificationOutcome> map(DiscriminativeFeatures evaluationFeatures)
            throws JCeliacGenericException
    {
        svm_node[] svmEvaluationFeatures = evaluationFeatures.getNormalizedFeaturesForSVM();

        int prediction = (int) libsvm.svm.svm_predict(this.trainedModel, svmEvaluationFeatures);
        ClassificationOutcome outcome;

        if(this.myConfiguration.SVMType == ClassificationParametersKernelSVM.ESVMType.ONE_CLASS) {
            outcome = new ClassificationOutcomeOneClass();
            int sampleGroundTruth = evaluationFeatures.getClassNumber();
            if(prediction == 1 && sampleGroundTruth == this.oneClassTrainingClassNumber) // correctly classified
            {
                ((ClassificationOutcomeOneClass) outcome).setCorrectlyEstimated(true);
            } else if(prediction == 1 && sampleGroundTruth != this.oneClassTrainingClassNumber) // incorrectly classified
            {
                ((ClassificationOutcomeOneClass) outcome).setCorrectlyEstimated(false);
            } else if(prediction == -1 && sampleGroundTruth != this.oneClassTrainingClassNumber) // correctly classified
            {
                ((ClassificationOutcomeOneClass) outcome).setCorrectlyEstimated(true);
            } else if(prediction == -1 && sampleGroundTruth == this.oneClassTrainingClassNumber) // incorrectly classified
            { 
                ((ClassificationOutcomeOneClass) outcome).setCorrectlyEstimated(false);
            }
        } else {
            outcome = new ClassificationOutcome();
            outcome.setEstimatedClass(prediction);
        }
        outcome.setEstimatedClass(prediction);
        ArrayList<ClassificationOutcome> ret = new ArrayList<>();
        ret.add(outcome);
        return ret;
    }
}
