/*
 *  JCeliac Copyright (C) 2010-2011 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.classification.kernelSVM;

import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.classificationParameters.ClassificationParameters;
import jceliac.classification.ClassificationMapping;
import jceliac.classification.ClassificationMethod;
import jceliac.classification.IterableClassificationParameterOptimizer;
import jceliac.featureExtraction.FeatureCollection;

/**
 *
 * @author shegen
 */
public class ExperimentalClassificationMethodKernelSVM
    extends ClassificationMethod
{

    protected ClassificationParametersKernelSVM myParameters;
    public ExperimentalClassificationMethodKernelSVM(ClassificationParameters parameters)
    {
        super(parameters);
        this.myParameters = (ClassificationParametersKernelSVM)parameters;
    }

    @Override
    public void hint(FeatureCollection trainingFeatures, FeatureCollection evaluationFeatures)
            throws JCeliacGenericException
    {
        // nothing to do for SVM
    }

    @Override
    public ClassificationMapping train(FeatureCollection trainingFeatures, 
                                                   ClassificationConfiguration configuration)
            throws JCeliacGenericException
    {
        ExperimentalClassificationMappingKernelSVM svmMapping = 
                                        new ExperimentalClassificationMappingKernelSVM(trainingFeatures,
                                                                                 configuration);
        svmMapping.trainMapping();
        return svmMapping;
    }

    @Override
    public ClassificationMapping reTrainMapping(ClassificationMapping trainedMapping, 
                                                            ClassificationConfiguration configuration)
            throws JCeliacGenericException
    {
        // copy the trained mapping and set the correct configuration
        ExperimentalClassificationMappingKernelSVM clonedMapping = 
                                        (ExperimentalClassificationMappingKernelSVM)trainedMapping.newInstance();
        clonedMapping.reTrainMapping(configuration);
        return clonedMapping;
    }

    @Override
    public IterableClassificationParameterOptimizer initializeOptimization()
    {
        return new IterableClassificationParameterOptimizerKernelSVM(this.parameters);
    }

    @Override
    public EClassificationMethodType getClassificationMethodType()
            throws JCeliacGenericException
    {
        if(this.myParameters.getSvmType() == ClassificationParametersKernelSVM.ESVMType.ONE_CLASS) {
            return EClassificationMethodType.KERNEL_ONE_CLASS_SVM;
        }else {
            return EClassificationMethodType.KERNEL_SVM;
        }
    }

    @Override
    public boolean supportsROC()
    {
        return false;
    }
    
}
