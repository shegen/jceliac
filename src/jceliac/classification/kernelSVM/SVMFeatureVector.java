/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification.kernelSVM;

import de.bwaldvogel.liblinear.Feature;
import libsvm.svm_node;

/**
 * This class is used to extend the svm_node defined by libsvm.
 * <p>
 * We need this to indicate whether the svm_node which is basically a single
 * feature is marked is a training feature for one-class SVM.
 * <p>
 * @author shegen
 */
public class SVMFeatureVector
        extends svm_node implements Feature
{

    public boolean oneClassTrainingFeature;

    @Override
    public int getIndex()
    {
        return super.index;
    }

    @Override
    public double getValue()
    {
        return super.value;
    }

    @Override
    public void setValue(double d)
    {
        super.value = d;
    }  
}
