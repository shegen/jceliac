/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */


package jceliac.classification;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;

/**
 *
 * @author shegen
 */
public abstract class ClassificationMapping
    implements Cloneable 
{

    
    protected final FeatureCollection trainingFeatures;
    protected ClassificationConfiguration configuration;
    
    public ClassificationMapping(FeatureCollection trainingFeatures,
                                             ClassificationConfiguration configuration)
    {
        // the features must not change from here on, if they do, a new mapping has to be trained
        // and calling reTrainMapping() the mapping will lead to a wrong mapping
        this.trainingFeatures = trainingFeatures;
        this.configuration = configuration;
    }
    
    public abstract ClassificationMapping newInstance();
    public abstract ArrayList <ClassificationOutcome> map(DiscriminativeFeatures evaluationFeatures)
            throws JCeliacGenericException;
    
    protected abstract void trainMapping() throws JCeliacGenericException;
    protected abstract void reTrainMapping(ClassificationConfiguration configuration) throws JCeliacGenericException;

    @Override
    protected Object clone() 
            throws CloneNotSupportedException
    {
        return super.clone(); 
    }
    
    
}
