/*
 *  JCeliac Copyright (C) 2010-2011 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification;

import jceliac.featureOptimization.FeatureVectorSubsetEncoding;
import java.io.Externalizable;
import java.io.ObjectOutput;
import java.io.IOException;
import java.io.ObjectInput;
import java.util.*;
import jceliac.JCeliacGenericException;

/**
 * The classification result of a single validation.
 * <p>
 * This contains the ClassificationOutcomes of each input as well as the class
 * rates and the ClassificationMethodConfiguration.
 * <p>
 * @author shegen
 */
public class ClassificationResult
        implements Comparable<ClassificationResult>,
                   Externalizable
{

    private final ArrayList<Double> classCorrectCounts;
    private final ArrayList<Double> classFalseCounts;
    private double overallCorrectCounts = 0;
    private double overallFalseCounts = 0;
    private final HashMap<String, Integer> classIdentifiers;
    private final HashMap<Integer, String> classIdentifiersByClass;
    private ArrayList<ClassificationOutcome> classificationOutcomes;
    private ClassificationConfiguration configuration;
    private FeatureVectorSubsetEncoding featureSubset;
    private int classCount;
    private int tieCount;
    private int guessCount;

    /**
     *
     * @param classCount Number of classes in the classification scenario.
     */
    public ClassificationResult(int classCount)
    {
        this.classificationOutcomes = new ArrayList<>();
        this.classCorrectCounts = new ArrayList<>();
        this.classFalseCounts = new ArrayList<>();
        this.classIdentifiers = new HashMap<>();
        this.classIdentifiersByClass = new HashMap<>();
        this.classCount = classCount;
    }

    /**
     * Needed for the Externalizable interface.
     */
    public ClassificationResult()
    {
        this.classificationOutcomes = new ArrayList<>();
        this.classCorrectCounts = new ArrayList<>();
        this.classFalseCounts = new ArrayList<>();
        this.classIdentifiers = new HashMap<>();
        this.classIdentifiersByClass = new HashMap<>();
    }

    /**
     * Adds the classification outcome of a single feature vector.
     * <p>
     * @param o Classification Outcome
     */
    public void addOutcome(ClassificationOutcome o)
    {
        this.classificationOutcomes.add(o);
        int realClass = o.getFeatures().getClassNumber();
        this.classIdentifiers.put(o.getFeatures().getClassIdentifier(), realClass);
        this.classIdentifiersByClass.put(realClass, o.getFeatures().getClassIdentifier());

        while(this.classFalseCounts.size() <= realClass) {
            this.classFalseCounts.add(new Double(0));
        }
        while(this.classCorrectCounts.size() <= realClass) {
            this.classCorrectCounts.add(new Double(0));
        }
        if(o.isTied()) {
            tieCount++;
        }
        if(o.isGuessed()) {
            guessCount++;
        }
        if(o.isCorrect() == false) {
            this.overallFalseCounts++;
            Double count = this.classFalseCounts.get(realClass);
            count++;
            this.classFalseCounts.set(realClass, count);
        } else {
            this.overallCorrectCounts++;
            Double count = this.classCorrectCounts.get(realClass);
            count++;
            this.classCorrectCounts.set(realClass, count);
        }
    }
    
    public void addOutcomeList(ArrayList <ClassificationOutcome> outcomes)
    {
        for(ClassificationOutcome tmpOutcome : outcomes) {
            this.addOutcome(tmpOutcome);
        }
    }

    /**
     * This methods returns the overall classification rate which is the number
     * of correct classified images divided by the number of all classified
     * images.
     * <p>
     * @return A double value between 0 and 1 indicating the classification
     *         accuracy.
     */
    public double getOverallClassificationRate()
    {
        if((this.overallCorrectCounts + this.overallFalseCounts) == 0) {
            return 0;
        }
        // round to 5 digits
        double res = (this.overallCorrectCounts) / (this.overallCorrectCounts + this.overallFalseCounts);
        int resInt = (int)Math.round(res * 10000);
        res = resInt / 10000.d;
        return res;
    }

    /**
     * Returns the mean classification rate of the rates of all classes.
     * <p>
     * @return A double value between 0 and 1 indicating the mean rate.
     * <p>
     * @throws JCeliacGenericException
     */
    public double getMeanClassificationRate()
            throws JCeliacGenericException
    {
        double avgRate = 0;
        for(int classIndex = 0; classIndex < this.classIdentifiers.size(); classIndex++) {
            String identifier = classIdentifiersByClass.get(classIndex);
            avgRate += getClassificationRate(identifier);
        }
        return (avgRate / this.classCount);
    }

    /**
     *
     * @param identifier Identifier of a class (class name).
     * <p>
     * @return True if the identifier exists.
     */
    public boolean hasClassIdentifier(String identifier)
    {
        return this.classIdentifiers.containsKey(identifier);
    }

    /**
     * Queries the classification accuracy of a single class based on a class
     * identifier.
     * <p>
     * @param identifier Identifier of a class.
     * <p>
     * @return A double value between 0 and 1 corresponding to the
     *         classification accuracy of a single class.
     */
    public double getClassificationRate(String identifier)
    {
        if(this.classIdentifiers.containsKey(identifier) == false) {
            return -1;
        }
        int classNumber = this.classIdentifiers.get(identifier);
        if((this.classCorrectCounts.get(classNumber) + this.classFalseCounts.get(classNumber)) == 0) {
            return 0;
        }
        double res = (this.classCorrectCounts.get(classNumber))
                      / (this.classCorrectCounts.get(classNumber) + this.classFalseCounts.get(classNumber));
         // round to 5 digits
        int resInt = (int)Math.round(res * 10000);
        res = resInt / 10000.d;
        return res;
    }

    /**
     * Queries the classification accuracy of a single class based on a class
     * number.
     * <p>
     * @param classNumber Number of class.
     * <p>
     * @return A double value between 0 and 1 corresponding to the
     *         classification accuracy of a single class.
     */
    public double getClassificationRate(int classNumber)
    {
        if(this.classIdentifiersByClass.containsKey(classNumber) == false) {
            return -1; // stored as -1 in db
        }
        if((this.classCorrectCounts.get(classNumber) + this.classFalseCounts.get(classNumber)) == 0) {
            return 0;
        }
        double res = (this.classCorrectCounts.get(classNumber))
                      / (this.classCorrectCounts.get(classNumber) + this.classFalseCounts.get(classNumber));
        int resInt = (int)Math.round(res * 10000);
        res = resInt / 10000.d;
        return res;
    }

    /**
     * Queries the classification accuracy of a single class based on a class
     * number.
     * <p>
     * @param o Classification result to compare.
     * <p>
     * @return -1 if this result is better than the result compared to, 1 if
     *         this result is worse and 0 if they are equal
     */
    @Override
    public int compareTo(ClassificationResult o)
    {
        ClassificationResult result = (ClassificationResult) o;

        double myRate = this.getOverallClassificationRate();
        double theirRate = result.getOverallClassificationRate();

        if(myRate > theirRate) {
            return -1;
        } else if(theirRate > myRate) {
            return 1;
        } else {
            return 0;
        }
    }

    public ClassificationConfiguration getConfiguration()
    {
        return configuration;
    }

    public void setConfiguration(ClassificationConfiguration configuration)
    {
        this.configuration = configuration;
    }

    public ArrayList<ClassificationOutcome> getClassificationOutcomes()
    {
        return classificationOutcomes;
    }

    public int getTieCount()
    {
        return this.tieCount;
    }

    public int getGuessCount()
    {
        return guessCount;
    }

    public FeatureVectorSubsetEncoding getFeatureSubset()
    {
        return this.featureSubset;
    }

    public void setFeatureSubset(FeatureVectorSubsetEncoding featureSubset)
    {
        this.featureSubset = featureSubset;
    }

    /**
     * Reduces a list of outcomes per feature vector to a single outcome. This
     * is used to compute the average rates over multiple k-values.
     * <p>
     * @return A list of classification outcomes with a single outcome per
     *         feature vector.
     */
    private ArrayList<ClassificationOutcome> reduceToSingleOutcomes()
    {
        HashMap<String, ClassificationOutcome> outcomesByImage = new HashMap<>();
        ArrayList<ClassificationOutcome> outcomes = new ArrayList<>();

        for(ClassificationOutcome outTmp : this.classificationOutcomes) {
            if(outcomesByImage.containsKey(outTmp.getFeatures().getSignalIdentifier()) == false) {
                outcomesByImage.put(outTmp.getFeatures().getSignalIdentifier(), null);
                outcomes.add(outTmp);
            }
        }
        return outcomes;
    }

    /**
     * Computes the Receiver Operator Characteristic of a result.
     * <p>
     * @return The Receiver Operator Characteristic
     */
    public ReceiverOperatorCharacteristic computeROC()
    {
        int neighborsMax = this.classificationOutcomes.get(0).getNearestNeighbors().size();
        int[] ROCcorrect = new int[neighborsMax + 2];
        int[] ROCwrong = new int[neighborsMax + 2];

        ArrayList<ClassificationOutcome> singleOutcomePerImage = this.reduceToSingleOutcomes();
       // singleOutcomePerImage = this.classificationOutcomes;

        // classify and store each outcome per k
        for(ClassificationOutcome outTmp : singleOutcomePerImage) {
            // count how many neighbors of thie feature were correctly classified
            int correctNeighbors = outTmp.getNearestNeighbors().stream()
                    .map((a) -> a == outTmp.getFeatures().getClassNumber() ? 1 : 0)
                    .reduce((a, b) -> a + b).get();

            // if kExpected neighbors are the correct class, the feature was correctly classified
            int index = 0;
            for(int kExpected = neighborsMax + 1; kExpected >= 0; kExpected--) {
                if(correctNeighbors >= kExpected) {
                    ROCcorrect[index++]++;
                } else {
                    ROCwrong[index++]++;
                }
            }
        }
        return new ReceiverOperatorCharacteristic(ROCcorrect, ROCwrong);
    }

    /**
     * Implementation of the Externalizable Interface. Used only for the McNemar
     * Tests, so we only export information needed by the McNemar tests.
     * <p>
     * @param oi Object to read.
     * <p>
     * @throws java.lang.ClassNotFoundException
     */
    @Override
    @SuppressWarnings("unchecked")
    public void readExternal(ObjectInput oi) throws IOException,
                                                    ClassNotFoundException
    {
        // the order must match the order of the writes
        this.classificationOutcomes = (ArrayList<ClassificationOutcome>) oi.readObject();

    }

    /**
     * Implementation of the Externalizable Interface. Used only for the McNemar
     * Tests, so we only export information needed by the McNemar tests.
     * <p>
     * @param oo Object to write.
     */
    @Override
    public void writeExternal(ObjectOutput oo) throws IOException
    {
        oo.writeObject(this.classificationOutcomes);

    }

    /**
     * Clears unused data to save memory.
     */
    public void clearData()
    {
        this.classificationOutcomes = null;
    }

}
