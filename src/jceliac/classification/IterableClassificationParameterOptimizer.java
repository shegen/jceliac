/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.classification;

import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.ClassificationResult;
import jceliac.classification.classificationParameters.ClassificationParameters;

/**
 *
 * @author shegen
 */
public abstract class IterableClassificationParameterOptimizer
{
    
    protected ClassificationParameters parameters;

    public IterableClassificationParameterOptimizer(ClassificationParameters parameters)
    {
        this.parameters = parameters;
    }

    public abstract boolean iterationConverged() throws JCeliacGenericException;
    public abstract ClassificationConfiguration startOptimization() throws JCeliacGenericException;
    public abstract ClassificationConfiguration iterateOptimization(ClassificationResult result, ClassificationConfiguration config) 
            throws JCeliacGenericException;
     
    /** 
     * If the optimization methods does know the next set of tried parameters,
     * it can overwrite this method to generate a batch of configurations
     * which will be handled multi-threaded by the validator. 
     * 
     * @return
     * @throws JCeliacGenericException 
     */
    public ClassificationConfiguration [] startBatchOptimization() 
            throws JCeliacGenericException
    {
        ClassificationConfiguration singleConfiguration = this.startOptimization();
        return new ClassificationConfiguration[] {singleConfiguration};
    }
    
    /**
     * Fallback implementation, if the parameters are a batch job and
     * this method was not overwritten it will fail.
     * 
     * @param results
     * @param configs
     * @return
     * @throws JCeliacGenericException 
     */
    public ClassificationConfiguration [] iterateBatchOptimization(ClassificationResult [] results, 
                                                                   ClassificationConfiguration [] configs)
            throws JCeliacGenericException 
    {
        if(results.length > 1) {
            throw new JCeliacGenericException("Iteration of batch optimization neither fallback nor implemented!");
        }
        ClassificationConfiguration singleConfiguration = this.iterateOptimization(results[0], configs[0]);
        return new ClassificationConfiguration[] {singleConfiguration};
    }

}
