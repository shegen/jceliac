/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification;

import jceliac.JCeliacGenericException;
import jceliac.classification.classificationParameters.ClassificationParameters;
import jceliac.experiment.InitializeFailedException;
import jceliac.featureExtraction.FeatureCollection;

/**
 *
 * @author shegen
 */
public abstract class ClassificationMethod
{

    protected ClassificationParameters parameters;

    public static enum EClassificationMethodType
    {
        KNN,
        KERNEL_ONE_CLASS_SVM,
        KERNEL_SVM,
        LINEAR_SVM;
    }

    public ClassificationMethod(ClassificationParameters parameters)
    {
        this.parameters = parameters;
    }

    /**
     * This can be used to do some prior optimizations before training.
     * <p>
     * @param trainingFeatures
     * @param evaluationFeatures
     * <p>
     * @throws jceliac.JCeliacGenericException
     */
    public abstract void hint(FeatureCollection trainingFeatures,
                              FeatureCollection evaluationFeatures)
            throws JCeliacGenericException;

    /**
     * Train the classifier on the supplied data with given feature subset and
     * classification configuration.
     * <p>
     * @param trainingFeatures
     * @param configuration
     * <p>
     * @return
     * @throws jceliac.JCeliacGenericException
     */
    public abstract ClassificationMapping
            train(FeatureCollection trainingFeatures,
                  ClassificationConfiguration configuration)
            throws JCeliacGenericException;

    /**
     * Our api offers this method to retrain an allready trained mapping using a
     * different configuration, this can be used to optimize speed of the
     * methods and needs requires that the training dit not change.
     * <p>
     * @param trainedMapping
     * @param configuration
     * <p>
     * @return
     * @throws jceliac.JCeliacGenericException
     */
    public abstract ClassificationMapping
            reTrainMapping(ClassificationMapping trainedMapping,
                           ClassificationConfiguration configuration)
            throws JCeliacGenericException;

    /**
     * Initialize the classification parameter optimization.
     * <p>
     * @return
     */
    public abstract IterableClassificationParameterOptimizer
            initializeOptimization();

    public abstract EClassificationMethodType getClassificationMethodType() throws JCeliacGenericException;

    public abstract boolean supportsROC() throws InitializeFailedException;
}
