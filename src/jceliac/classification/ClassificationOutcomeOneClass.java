/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification;

/**
 * This is the modified ClassificationOutcome used for One-Class classifiers.
 * <p>
 * The outcome is either correct and stored as 1 or incorrect and stored as -1.
 * <p>
 * @author shegen
 */
public class ClassificationOutcomeOneClass
        extends ClassificationOutcome
{

    private boolean correctlyEstimated;

    /**
     *
     * @return True if estimated as member of the training class and the feature
     *         vector is actually member of the training class.
     */
    public boolean isCorrectlyEstimated()
    {
        return correctlyEstimated;
    }

    public void setCorrectlyEstimated(boolean correctlyEstimated)
    {
        this.correctlyEstimated = correctlyEstimated;
    }

    @Override
    public boolean isCorrect()
    {
        return this.correctlyEstimated;
    }

    /**
     *
     * @return One-Class SVM does not estimate a class number, returns -1 as a
     *         consquence.
     */
    @Override
    public int getEstimatedClass()
    {
       // Experiment.log(JCeliacLogger.LOG_ERROR, "One-Class SVM does not estimate a class number!");
        return -1;
    }
}
