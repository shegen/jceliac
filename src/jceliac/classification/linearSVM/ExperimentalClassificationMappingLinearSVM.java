/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.classification.linearSVM;

import de.bwaldvogel.liblinear.*;
import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.ClassificationOutcome;
import jceliac.classification.kernelSVM.SVMFeatureVector;
import jceliac.classification.ClassificationMapping;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;

/**
 *
 * @author shegen
 */
public class ExperimentalClassificationMappingLinearSVM
    extends ClassificationMapping
{
    protected Model trainedModel;
    protected ClassificationConfigurationLinearSVM myConfiguration;
    
    public ExperimentalClassificationMappingLinearSVM(FeatureCollection trainingFeatures, 
                                                      ClassificationConfiguration configuration)
    {
        super(trainingFeatures, configuration);
        this.myConfiguration = (ClassificationConfigurationLinearSVM)configuration;
    }

    
    @Override
    public ClassificationMapping newInstance()
    {
        ExperimentalClassificationMappingLinearSVM newMapping = 
            new ExperimentalClassificationMappingLinearSVM(this.trainingFeatures, this.configuration);
        return newMapping;
    }

    @Override
    public ArrayList<ClassificationOutcome> map(DiscriminativeFeatures evaluationFeatures)
            throws JCeliacGenericException
    {
        SVMFeatureVector[] svmEvaluationFeatures = evaluationFeatures.getNormalizedFeaturesForSVM();

        int prediction = (int) Linear.predict(this.trainedModel, svmEvaluationFeatures);
        ClassificationOutcome outcome = new ClassificationOutcome();
        outcome.setEstimatedClass(prediction);
        
        ArrayList<ClassificationOutcome> ret = new ArrayList<>();
        ret.add(outcome);
        return ret;
    }

    @Override
    protected void trainMapping() throws JCeliacGenericException
    {
        SVMFeatureVector [][] svmTrainingFeatures = new SVMFeatureVector[this.trainingFeatures.size()][];
        double[] groundTruth = new double[this.trainingFeatures.size()];
        
        for(int index = 0; index < this.trainingFeatures.size(); index++) {
            DiscriminativeFeatures feature = this.trainingFeatures.get(index);
            svmTrainingFeatures[index] = feature.getNormalizedFeaturesForSVM();
            groundTruth[index] = (double) feature.getClassNumber();
        }
        Problem problem = new Problem();
        problem.n = svmTrainingFeatures[0].length; // dimension of feature vector
        problem.l = svmTrainingFeatures.length; // number of feature vectors
        problem.x = svmTrainingFeatures; // feature vectors
        problem.y = groundTruth; // ground truth
        
        SolverType solver = SolverType.getById(this.myConfiguration.getSolverID()); // default is L2R_LR = 0, sometimes L2R_L2LOSS_SVC = 2 is used
        Parameter parameter = new Parameter(solver, this.myConfiguration.getC(), this.myConfiguration.getEps());
        Linear.disableDebugOutput();
        this.trainedModel = Linear.train(problem, parameter);  
    }

    @Override
    protected void reTrainMapping(ClassificationConfiguration configuration)
            throws JCeliacGenericException
    {
        this.myConfiguration = (ClassificationConfigurationLinearSVM) configuration;
        this.trainMapping();
    }
    
}
