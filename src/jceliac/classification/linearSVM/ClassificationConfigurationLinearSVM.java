/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.classification.linearSVM;

import jceliac.classification.ClassificationConfiguration;

/**
 *
 * @author shegen
 */
public class ClassificationConfigurationLinearSVM
    extends ClassificationConfiguration
{
    protected double eps;
    protected double C;
    protected int solverID;

    public ClassificationConfigurationLinearSVM(double eps, double C, int solverID)
    {
        this.eps = eps;
        this.C = C;
        this.solverID = solverID;
    }

    @Override
    protected String getClassificationMethodName()
    {
        return "LINEAR SVM";
    }

    @Override
    public void doReport()
    {
       
    }

    public double getEps()
    {
        return eps;
    }

    public double getC()
    {
        return C;
    }

    public int getSolverID()
    {
        return solverID;
    }
}
