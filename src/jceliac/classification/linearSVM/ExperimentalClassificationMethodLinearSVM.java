/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */


package jceliac.classification.linearSVM;

import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.classificationParameters.ClassificationParameters;
import jceliac.classification.ClassificationMapping;
import jceliac.classification.ClassificationMethod;
import jceliac.classification.IterableClassificationParameterOptimizer;
import jceliac.featureExtraction.FeatureCollection;

/**
 *
 * @author shegen
 */
public class ExperimentalClassificationMethodLinearSVM
    extends ClassificationMethod
{

    public ExperimentalClassificationMethodLinearSVM(ClassificationParameters parameters)
    {
        super(parameters);
    }

    
    @Override
    public void hint(FeatureCollection trainingFeatures, FeatureCollection evaluationFeatures)
            throws JCeliacGenericException
    {
        // nothing to do
    }

    @Override
    public ClassificationMapping train(FeatureCollection trainingFeatures, 
                                                   ClassificationConfiguration configuration)
            throws JCeliacGenericException
    {
        ExperimentalClassificationMappingLinearSVM svmMapping = 
                                        new ExperimentalClassificationMappingLinearSVM(trainingFeatures,
                                                                                 configuration);
        svmMapping.trainMapping();
        return svmMapping;
    }

    @Override
    public ClassificationMapping reTrainMapping(ClassificationMapping trainedMapping, 
                                                            ClassificationConfiguration configuration)
            throws JCeliacGenericException
    {
        // copy the trained mapping and set the correct configuration
        ExperimentalClassificationMappingLinearSVM clonedMapping = 
                                        (ExperimentalClassificationMappingLinearSVM)trainedMapping.newInstance();
        clonedMapping.reTrainMapping(configuration);
        return clonedMapping;
    }

    @Override
    public IterableClassificationParameterOptimizer initializeOptimization()
    {
       return new IterableClassificationParameterOptimizerLinearSVM(this.parameters);
    }

    @Override
    public EClassificationMethodType getClassificationMethodType() 
            throws JCeliacGenericException
    {
        return EClassificationMethodType.LINEAR_SVM;
    }

    @Override
    public boolean supportsROC()
    {
        return false;
    }
 
    
}
