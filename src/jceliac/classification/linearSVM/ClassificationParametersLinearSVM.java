/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.classification.linearSVM;

import jceliac.classification.classificationParameters.ClassificationParameters;

/**
 *
 * @author shegen
 */
public class ClassificationParametersLinearSVM
    extends ClassificationParameters
{
    protected double eps = 0.01;
    protected double [] C = {1};
    protected int solverID = 0;

    public double getEps()
    {
        return eps;
    }

    public int getSolverID()
    {
        return solverID;
    }
    
    
   
}
