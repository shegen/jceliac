/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.classification.linearSVM;

import jceliac.JCeliacGenericException;
import jceliac.classification.ClassificationConfiguration;
import jceliac.classification.ClassificationResult;
import jceliac.classification.classificationParameters.ClassificationParameters;
import jceliac.classification.IterableClassificationParameterOptimizer;

/**
 * This actually does nothing yet. 
 * 
 * @author shegen
 */
public class IterableClassificationParameterOptimizerLinearSVM
    extends IterableClassificationParameterOptimizer
{

    private int currentCIndex = 0;
    
    protected ClassificationParametersLinearSVM myParameters;
    public IterableClassificationParameterOptimizerLinearSVM(ClassificationParameters parameters)
    {
        super(parameters);
        this.myParameters = (ClassificationParametersLinearSVM) parameters;
    }

    @Override
    public boolean iterationConverged() throws JCeliacGenericException
    {
        if(this.currentCIndex >= this.myParameters.C.length) {
            return true;
        }
        return false;
    }

    @Override
    public ClassificationConfiguration startOptimization() 
            throws JCeliacGenericException
    {
        
        double C = this.myParameters.C[this.currentCIndex];
        this.currentCIndex++;
        ClassificationConfigurationLinearSVM configuration = 
                new ClassificationConfigurationLinearSVM(this.myParameters.getEps(),
                                                         C,
                                                         this.myParameters.getSolverID());
        return configuration;
        
    }

    @Override
    public ClassificationConfiguration iterateOptimization(ClassificationResult result, 
                                                           ClassificationConfiguration config)
            throws JCeliacGenericException
    {
        if(this.currentCIndex < this.myParameters.C.length) {
            double C = this.myParameters.C[this.currentCIndex];
            ClassificationConfigurationLinearSVM configuration = 
                    new ClassificationConfigurationLinearSVM(this.myParameters.getEps(),
                                                             C,
                                                             this.myParameters.getSolverID());
            this.currentCIndex++;
            return configuration;
        }else {
            return config;
        }
    }
    
}
