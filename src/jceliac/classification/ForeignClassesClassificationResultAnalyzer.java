/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification;

import jceliac.experiment.Experiment;
import jceliac.logging.JCeliacLogger;
import jceliac.experiment.*;
import java.util.Map.*;
import java.util.*;

/**
 * This is used for SVM with foreign classes, classes that are not in the
 * training set. AS one class SVM does not assign a class number, this is used
 * to compute the classification accuracy for one class SVM.
 * <p>
 * @author shegen
 */
public class ForeignClassesClassificationResultAnalyzer
{

    private final ClassificationResult result;

    /**
     *
     * @param result Classification result to analyze.
     */
    public ForeignClassesClassificationResultAnalyzer(ClassificationResult result)
    {
        this.result = result;
    }

    /**
     *
     * @param parameters Parameters of the experiment used to compute the
     *                   classification result to analyze.
     */
    public void analyzeAndDisplay(ExperimentParameters parameters)
    {
        ArrayList<ClassificationOutcome> outcomes = this.result.getClassificationOutcomes();
        HashMap<String, HashMap<String, Integer>> outcomesPerClassIdentifier = new HashMap<>();
        int correctCount = 0;
        int incorrectCount = 0;

        // build a hashmap of hashmaps for each class identifier in the evaluation set
        for(ClassificationOutcome tmpOutcome : outcomes) {
            String classID = tmpOutcome.getFeatures().getClassIdentifier();
            if(outcomesPerClassIdentifier.containsKey(classID) == false) {
                HashMap<String, Integer> resultMap = new HashMap<>();
                outcomesPerClassIdentifier.put(classID, resultMap);
            }
        }
        for(ClassificationOutcome tmpOutcome : outcomes) {
            String classID = tmpOutcome.getFeatures().getClassIdentifier();

            // get the correct hashmap for this class identifier
            HashMap<String, Integer> resultMap = outcomesPerClassIdentifier.get(classID);

            // update the current result hashmap
            // one class SVM does not predict a class number, it only predicts yes or no
            String estimatedClassIdentifier;
            if(tmpOutcome instanceof ClassificationOutcomeOneClass) {
                boolean correct = tmpOutcome.isCorrect();
                // it is correct, this must be the training class
                if(correct) {
                    estimatedClassIdentifier = "Correct";
                    correctCount++;
                } else {
                    estimatedClassIdentifier = "Incorrect";
                    incorrectCount++;
                }
            } else {
                // not one-class 
                int estimatedClassNumber = tmpOutcome.getEstimatedClass();
                estimatedClassIdentifier = parameters.getClassIdentifier(estimatedClassNumber);
            }
            if(resultMap.containsKey(estimatedClassIdentifier)) {
                Integer count = resultMap.get(estimatedClassIdentifier);
                count++;
                resultMap.put(estimatedClassIdentifier, count);
            } else {
                resultMap.put(estimatedClassIdentifier, 1);
            }
        }
        // display the results
        Set<Entry<String, HashMap<String, Integer>>> entrySet = outcomesPerClassIdentifier.entrySet();
        for(Entry<String, HashMap<String, Integer>> evalutionClassEntry : entrySet) {
            Experiment.log(JCeliacLogger.LOG_RESULT, "Class Identifier: " + evalutionClassEntry.getKey());
            HashMap<String, Integer> classSpecificResultMap = evalutionClassEntry.getValue();

            // loop through all result classes of the evaluation class
            for(Entry<String, Integer> resultClassEntry : classSpecificResultMap.entrySet()) {
                Experiment.log(JCeliacLogger.LOG_RESULT, "Estimated as Class %s in %d cases.", resultClassEntry.getKey(), resultClassEntry.getValue());
            }
        }

        int all = correctCount + incorrectCount;
        Experiment.log(JCeliacLogger.LOG_RESULT, "Accuracy: %f", (correctCount * 100.0d / all));
    }

}
