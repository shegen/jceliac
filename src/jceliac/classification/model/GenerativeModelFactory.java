/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification.model;

import jceliac.JCeliacGenericException;
import jceliac.classification.model.bow.BagOfVisualWordsModel;
import jceliac.classification.model.bow.BagOfVisualWordsParameters;
import jceliac.experiment.Experiment;
import jceliac.experiment.InvalidInputException;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.parameter.xml.XMLParseException;

/**
 *
 * @author shegen
 */
public class GenerativeModelFactory
{
    public static GenerativeModel createGenerativeModel(GenerativeModel.EGenerativeModel type,
                                                        GenerativeModelParameters parameters)
            throws InvalidInputException
    {
        switch(type) {
            case BAG_OF_WORDS:
                return new BagOfVisualWordsModel(parameters);
            
            case NULL:
                return new NullGenerativeModel(parameters);
                
            default:
                Experiment.log(JCeliacLogger.LOG_ERROR, "Unknown Generative Model!");
                throw new InvalidInputException();
        }
    }
    
    public static GenerativeModelParameters createParametersByMethod(String method)
            throws XMLParseException
    {
        GenerativeModelParameters parameters;
        
        if(method.equalsIgnoreCase("BAG_OF_WORDS")) {
            parameters = new BagOfVisualWordsParameters();
            return parameters;
        }
        if(method.equalsIgnoreCase("NULL")) {
            return new NullGenerativeModelParameters();
        }
        else {
            throw new XMLParseException("Generative Model (" + method + ") not supported!");
        }
    }
    
    public static GenerativeModel createGenerativeModel(String methodString,
                                                        GenerativeModelParameters parameters)
            throws JCeliacGenericException
    {
        GenerativeModel.EGenerativeModel method = GenerativeModel.EGenerativeModel.valueOfIgnoreCase(methodString);
        return GenerativeModelFactory.createGenerativeModel(method, parameters); 
    }
}
