/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.classification.model.bow;

import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.AbstractFeatureVector;

/**
 *
 * @author shegen
 */
public class TextonFrequencyHistogram 
    extends AbstractFeatureVector
{

    private double [] histogram;

    @Override
    public void setData(double[] data)
    {
        this.histogram = new double[data.length];
        // normalize features
        double sum = 0;
        for(int i = 0; i < data.length; i++) {
            sum += data[i];
        }
        for(int i = 0; i < data.length; i++) {
            this.histogram[i] = data[i] / sum;
        }
    }

    @Override
    public double[] getFeatureData() throws JCeliacGenericException
    {
        return this.histogram;
    }
    
}
