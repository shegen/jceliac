/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at> 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.classification.model.bow;

import java.util.ArrayList;
import java.util.HashMap;
import jceliac.JCeliacGenericException;
import jceliac.classification.model.GenerativeModel;
import jceliac.classification.model.GenerativeModelFeatureMapping;
import jceliac.classification.model.GenerativeModelParameters;
import jceliac.experiment.Experiment;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FixedFeatureVector;
import jceliac.featureExtraction.FeatureCollection;
import jceliac.featureExtraction.StandardEuclideanFeatures;
import jceliac.logging.JCeliacLogger;
import jceliac.tools.cluster.KMeansClustering;
import jceliac.tools.cluster.KMeansDataPoint;

/**
 *
 * @author shegen
 */
public class BagOfVisualWordsModel extends GenerativeModel 
{

    private final ArrayList <DiscriminativeFeatures> textonDictionary;
    private final BagOfVisualWordsParameters myParameters;

    public BagOfVisualWordsModel(GenerativeModelParameters myParameters)
    {
        super(myParameters);
        this.myParameters = (BagOfVisualWordsParameters) myParameters;
        this.textonDictionary = new ArrayList <>();
    }
    
    @Override
    public void initializeModel()
    {
    }

    /**
     * Trains the model by computing the dictionary for the BoW. 
     * Features are aggregated by using k-means clustering,
     * the resulting cluster centers are the textons used in the
     * dictionary.
     * 
     * 
     * @param features 
     * @return  
     * @throws jceliac.JCeliacGenericException 
     */
    @Override
    public GenerativeModelFeatureMapping trainModel(FeatureCollection features)
            throws JCeliacGenericException
    {
        HashMap <Integer, ArrayList <DiscriminativeFeatures>> featuresByClass =
            this.collectFeaturesByClass(features);
       
        this.reduceFeatures(featuresByClass);
        for(Integer key : featuresByClass.keySet()) {
            this.computeTextonsForClass(featuresByClass.get(key), key);
        }
        return new BagOfVisualWordsFeatureMapping(this.textonDictionary);
    }
    
    private void computeTextonsForClass(ArrayList <DiscriminativeFeatures> featuresByClass, 
                                        int classNumber)
            throws JCeliacGenericException
    {
        // collect all features as list
        ArrayList <double[]> featureVectorsByClass = new ArrayList <>();
        for(DiscriminativeFeatures tmpFeatures : featuresByClass) {
            featureVectorsByClass.addAll(tmpFeatures.getLocalFeatureVectorData());
        }
        KMeansClustering kmeans = 
            new KMeansClustering(featureVectorsByClass, this.myParameters.getNumClusters(), 
                                 this.myParameters.getMaxIterations());
        kmeans.cluster();
        KMeansDataPoint [] centers = kmeans.getClusterCenters();
        for(KMeansDataPoint center : centers) {
            FixedFeatureVector vector = new FixedFeatureVector();
            vector.setData(center.dataVector);
            DiscriminativeFeatures texton = new StandardEuclideanFeatures();
            texton.addAbstractFeature(vector);
            texton.setSignalIdentifier("");
            texton.setClassNumber(classNumber);
            this.textonDictionary.add(texton);
        }
    }
    
    private HashMap <Integer, ArrayList <DiscriminativeFeatures>> 
            collectFeaturesByClass(FeatureCollection features)
    {
         // order features by class
        HashMap <Integer, ArrayList <DiscriminativeFeatures>> featuresByClass = new HashMap<>();
        for(DiscriminativeFeatures tmp : features) {
            int classNumber = tmp.getClassNumber();
            if(false == featuresByClass.containsKey(classNumber)) {
                ArrayList <DiscriminativeFeatures> fTmp = new ArrayList <>();
                fTmp.add(tmp);
                featuresByClass.put(classNumber, fTmp);
            }else {
                ArrayList <DiscriminativeFeatures> fTmp = featuresByClass.get(classNumber);
                fTmp.add(tmp);
                featuresByClass.put(classNumber, fTmp);
            }
        }
        return featuresByClass;
    }
                    
    private void reduceFeatures(HashMap<Integer, ArrayList<DiscriminativeFeatures>> featuresByClass)
    {
        double trainingFractionPerClass = this.myParameters.getTrainingDataFractionPerClass();
        if(trainingFractionPerClass > 1) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Value of trainingFractionPerClass > 1.0, this is a configuration error, assuming 1.0.");
            trainingFractionPerClass = 1.0d;
        }
        for(Integer key : featuresByClass.keySet()) {
            ArrayList<DiscriminativeFeatures> featuresOfClass = featuresByClass.get(key);
            int maxIndex = (int) (featuresOfClass.size() * trainingFractionPerClass);
            if(maxIndex == 0) { // ensure at least one feature per class
                maxIndex = 1;
            }
            ArrayList<DiscriminativeFeatures> featuresOfClassSublist
                                              = new ArrayList<>(featuresOfClass.subList(0, maxIndex));
            featuresByClass.put(key, featuresOfClassSublist);
        }
    }
}
