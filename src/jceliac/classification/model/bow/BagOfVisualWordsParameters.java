/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.classification.model.bow;

import jceliac.classification.model.GenerativeModelParameters;
import jceliac.experiment.Experiment;
import jceliac.logging.JCeliacLogger;


/**
 *
 * @author shegen
 */
public class BagOfVisualWordsParameters extends GenerativeModelParameters
{

    /**
     * Fraction of data from the training set to use, to train the model.
     * Training the model can be slow, we hence give this option to improve
     * the performance. 
     */
    protected double trainingDataFractionPerClass = 0.5;
    
    
    protected int numClusters = 10;
    protected int maxIterations = 200;
    
    public int getNumClusters()
    {
        return this.numClusters;
    }

    public int getMaxIterations()
    {
        return this.maxIterations;
    }

    public double getTrainingDataFractionPerClass()
    {
        return trainingDataFractionPerClass;
    }

    @Override
    public void report()
    {
        super.report();
        Experiment.log(JCeliacLogger.LOG_INFO, "Number of Clusters: " + this.numClusters);
        Experiment.log(JCeliacLogger.LOG_INFO, "Number of Clusters: " + this.maxIterations);
    }

    @Override
    public String getMethodName()
    {
        return "Bag of Visual Words";
    }
    
}
