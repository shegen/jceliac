/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

package jceliac.classification.model.bow;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.classification.model.GenerativeModelFeatureMapping;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.Distances;
import jceliac.featureExtraction.FeatureCollection;

/**
 *
 * @author shegen
 */
public class BagOfVisualWordsFeatureMapping
    extends GenerativeModelFeatureMapping
{

    private final double [][] textonDictionary;

    public BagOfVisualWordsFeatureMapping(ArrayList<DiscriminativeFeatures> textonDictionary)
            throws JCeliacGenericException
    {
        this.textonDictionary = new double[textonDictionary.size()][];
        for(int textonIndex = 0; textonIndex < textonDictionary.size(); textonIndex++) {
            this.textonDictionary[textonIndex] = textonDictionary.get(textonIndex).getAllFeatureVectorData();
        }
    }
    
    @Override
    public FeatureCollection mapFeatures(FeatureCollection features)
            throws JCeliacGenericException
    {
        ArrayList <DiscriminativeFeatures> mappedFeaturesList = new ArrayList <>();
        for(DiscriminativeFeatures curFeatures: features) 
        {
            ArrayList <double []> localFeatures = curFeatures.getLocalFeatureVectorData();
            TextonFrequencyHistogram frequencyHistogram = this.computeTextonFrequency(localFeatures);
            TextonFrequencyFeatures mappedFeatures = new TextonFrequencyFeatures(curFeatures);
            mappedFeatures.addAbstractFeature(frequencyHistogram);
            mappedFeaturesList.add(mappedFeatures);
        }
        return FeatureCollection.createClonedFeatureCollection(mappedFeaturesList);
    }

    @Override
    public DiscriminativeFeatures mapFeatures(DiscriminativeFeatures features)
            throws JCeliacGenericException
    {
        ArrayList <double []> localFeatures = features.getLocalFeatureVectorData();
        TextonFrequencyHistogram frequencyHistogram = this.computeTextonFrequency(localFeatures);
        TextonFrequencyFeatures mappedFeatures = new TextonFrequencyFeatures(features);
        mappedFeatures.addAbstractFeature(frequencyHistogram);
        return mappedFeatures;
    }
    
    private TextonFrequencyHistogram computeTextonFrequency(ArrayList <double []> localFeatures)
            throws JCeliacGenericException
    {
        double [] frequencyHist = new double[this.textonDictionary.length];
        for(double [] vec : localFeatures) 
        {
            int textonIndex = this.findClosestTexton(vec);
            frequencyHist[textonIndex] = frequencyHist[textonIndex] + 1.0d;
        }
        TextonFrequencyHistogram ret = new TextonFrequencyHistogram();
        ret.setData(frequencyHist);
        return ret;
    }
    private int findClosestTexton(double [] vector)
            throws JCeliacGenericException
    {
        double [] vectorData = vector;
        double minDistance = Double.POSITIVE_INFINITY;
        int minIndex = -1;
        for(int textonIndex = 0; textonIndex < this.textonDictionary.length; textonIndex++) {
            double distance = Distances.squaredEuclideanDistance(vectorData, this.textonDictionary[textonIndex]);
            if(distance < minDistance) {
                minDistance = distance;
                minIndex = textonIndex;
            }
        }
        return minIndex;
    }
}
