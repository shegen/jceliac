/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jceliac.classification.model;

/**
 *
 * @author shegen
 */
public class NullGenerativeModelParameters extends GenerativeModelParameters
{

    // do not report anything
    @Override
    public void report()
    {
    }

    
    @Override
    public String getMethodName()
    {
        return "NULL";
    }
    
}
