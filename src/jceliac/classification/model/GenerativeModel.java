/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.classification.model;

import java.util.ArrayList;
import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;
import jceliac.featureOptimization.FeatureVectorSubsetEncoding;


/**
 * 
 * @author shegen
 */
public abstract class GenerativeModel
{
    
    public static enum EGenerativeModel
    {
        BAG_OF_WORDS,
        NULL;

        public static GenerativeModel.EGenerativeModel valueOfIgnoreCase(String stringValue)
                throws JCeliacGenericException
        {
            for(GenerativeModel.EGenerativeModel value : GenerativeModel.EGenerativeModel.values()) {
                if(value.name().equalsIgnoreCase(stringValue)) {
                    return value;
                }
            }
            throw new JCeliacGenericException("Unknown EGenerativeModel: " + stringValue);
        }
    }
    
    
    protected GenerativeModelParameters parameters;
    public GenerativeModel(GenerativeModelParameters parameters)
    {
        this.parameters = parameters;
    }
    
    /** 
     * Initialization of the model.
     */
    public abstract void initializeModel();
    
    /**
     * Trains the model based on supplied generative features.
     * @param features A list of generative features used to train the model. 
     * @return  
     * @throws jceliac.JCeliacGenericException  
     */
    public abstract GenerativeModelFeatureMapping trainModel(FeatureCollection features) 
            throws JCeliacGenericException;
    
   
}
