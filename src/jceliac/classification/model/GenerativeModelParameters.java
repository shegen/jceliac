/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at> 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */
package jceliac.classification.model;

import java.io.Serializable;
import jceliac.experiment.AbstractParameters;
import jceliac.experiment.Experiment;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
public abstract class GenerativeModelParameters 
                      extends AbstractParameters
                      implements Cloneable, Serializable
{
    
    @Override
    public Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }

    public void report() 
    {
        Experiment.log(JCeliacLogger.LOG_INFO, "Generative Model Parameters: ");
        Experiment.log(JCeliacLogger.LOG_INFO, "Generative Model Type: %s", this.getMethodName());
     
    }
    public abstract String getMethodName();
}
