/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.classification.model;

import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;

/**
 *
 * @author shegen
 */
public class GenerativeModelFeatureMappingNULL 
    extends GenerativeModelFeatureMapping
{
    @Override
    public FeatureCollection mapFeatures(FeatureCollection features)
    {
        return features;
    } 

    @Override
    public DiscriminativeFeatures mapFeatures(DiscriminativeFeatures features) 
            throws JCeliacGenericException
    {
        return features;
    }   
}
