/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.classification.model;

import jceliac.JCeliacGenericException;
import jceliac.featureExtraction.DiscriminativeFeatures;
import jceliac.featureExtraction.FeatureCollection;

/**
 *
 * @author shegen
 */
public abstract class GenerativeModelFeatureMapping
{

   /**
     * Labels the local features using the trained dictionary and creates
     * a discriminative feature used for classification. The type of discriminative
     * feature should be implemented by the specific model. 
     * 
     * @param features A generative feature used to compute the discriminative feature. 
     * @return A discriminative feature based on the trained model. 
     * @throws jceliac.JCeliacGenericException 
     */
    public abstract FeatureCollection mapFeatures(final FeatureCollection features)
           throws JCeliacGenericException;
    
    public abstract DiscriminativeFeatures mapFeatures(final DiscriminativeFeatures features)
           throws JCeliacGenericException;
}
