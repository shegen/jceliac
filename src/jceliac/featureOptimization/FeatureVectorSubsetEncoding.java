/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureOptimization;

import java.io.Serializable;
import java.util.*;
import jceliac.experiment.Experiment;
import jceliac.logging.JCeliacLogger;

/**
 * Represents a feature vector subset.
 * <p>
 * @author shegen
 */
public class FeatureVectorSubsetEncoding
        implements Serializable
{

    private final BitSet featureSubset;
    private final int featureCount;

    public FeatureVectorSubsetEncoding(int numBits)
    {
        this.featureSubset = new BitSet(numBits);
        this.featureCount = numBits;
    }

    public FeatureVectorSubsetEncoding(FeatureVectorSubsetEncoding sub)
    {
        this.featureSubset = (BitSet) sub.getEntireFeatureSubset().clone();
        this.featureCount = sub.getFeatureCount();
    }

    public void setFeatureIndices(FeatureVectorSubsetEncoding other)
    {
        this.featureSubset.or(other.featureSubset);
    }
    
    public void setFeatureIndex(int num)
    {
        featureSubset.set(num);
    }

    public void clearFeatureIndex(int num)
    {
        featureSubset.clear(num);
    }

    public boolean isSet(int num)
    {
        return featureSubset.get(num);
    }

    public int getFeatureCount()
    {
        return this.featureCount;
    }

    public BitSet getEntireFeatureSubset()
    {
        return this.featureSubset;
    }

    public void setAll()
    {
        this.featureSubset.set(0, featureCount);
    }
    
    public boolean allSet()
    {
        return this.featureSubset.cardinality() == this.featureCount;
    }

    @Override
    public String toString()
    {
        return this.featureSubset.toString();
    }

    public int bitCount()
    {
        return this.featureSubset.length();
    }

    @Override
    public int hashCode()
    {
        int hash = this.featureSubset.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object o)
    {
        if(!(o instanceof FeatureVectorSubsetEncoding)) {
            Experiment.log(JCeliacLogger.LOG_ERROR, "Equals called with invalid parameter on FeatureVectorSubsetEncoding!");
        }
        FeatureVectorSubsetEncoding them = (FeatureVectorSubsetEncoding)o;
        return this.featureSubset.equals(them.featureSubset);
 
    }
}
