/*
 * JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 * The source code of this software is not yet made available to the open source 
 * community. If you obtained this source code without permission of the author 
 * please remove all the source files.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */
package jceliac.featureOptimization;

import jceliac.experiment.Experiment;
import jceliac.experiment.InvalidInputException;
import jceliac.featureOptimization.FeatureOptimizationMethod.EFeatureOptimizationMethod;
import jceliac.logging.JCeliacLogger;

/**
 *
 * @author shegen
 */
public class FeatureOptimizationMethodFactory
{

    public static FeatureOptimizationMethod
            createExperimentalFeatureOptimizationMethod(EFeatureOptimizationMethod type,
                                                        FeatureOptimizationParameters parameters)
            throws InvalidInputException
    {
        switch(type) {
            case SFS:
                return new FeatureOptimizationMethodSFS(parameters);

            case SBS:
                return new FeatureOptimizationMethodSBS(parameters);

            case NULL:
                return new FeatureOptimizationMethodNULL(parameters);

            default:
                Experiment.log(JCeliacLogger.LOG_ERROR, "Unknown Feature Extraction Method!");
                throw new InvalidInputException();
        }
    }

    public static FeatureOptimizationMethod
            createExperimentalFeatureOptimizationMethod(String methodString,
                                                        FeatureOptimizationParameters parameters)
            throws Exception
    {
        EFeatureOptimizationMethod method = FeatureOptimizationMethodFactory.mapFeatureOptimizationMethodName(methodString);
        switch(method) {
            case SFS:
                return new FeatureOptimizationMethodSFS(parameters);

            case SBS:
                return new FeatureOptimizationMethodSBS(parameters);

            case NULL:
                return new FeatureOptimizationMethodNULL(parameters);

            default:
                Experiment.log(JCeliacLogger.LOG_ERROR, "Unknown Feature Optimization Method!");
                throw new InvalidInputException();
        }
    }

    public static EFeatureOptimizationMethod mapFeatureOptimizationMethodName(String methodString)
    {
        return EFeatureOptimizationMethod.valueOf(methodString);
    }
}
