/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */

package jceliac.featureOptimization;

/**
 *
 * @author shegen
 */
public abstract class FeatureOptimizationMethod
{
    protected FeatureOptimizationParameters parameters;

    public FeatureOptimizationMethod(FeatureOptimizationParameters parameters)
    {
        this.parameters = parameters;
    }
    
    public abstract IterableFeatureOptimizer initializeOptimization(int featureDimension);
   
    public static enum EFeatureOptimizationMethod
    {

        /**
         * Sequential Forward Search.
         */
        SFS,
        /**
         * Sequential Backward Search.
         */
        SBS,
        /**
         * No optimization (fixed subset).
         */
        NULL
    }
}
