/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureOptimization;

import jceliac.featureOptimization.*;
import java.io.Serializable;
import jceliac.*;

/**
 * Basis for feature subset optimization parameters.
 * <p>
 * @author shegen
 */
public class FeatureOptimizationParameters
        extends CloneableReflectableParameters
        implements Serializable
{
    // this will be mapped to a FeatureVectorSubsetEncoding in the get() method
    public String featureSubset;

    public void setFeatureSubset(String s)
    {
        this.featureSubset = s;
    }

    public FeatureVectorSubsetEncoding getFeatureSubset(int paramDim)
            throws JCeliacGenericException
    {
        if(this.featureSubset == null) {
            FeatureVectorSubsetEncoding encoding = new FeatureVectorSubsetEncoding(paramDim);
            encoding.setAll();
            return encoding;
        }
        // parse encoding
        return parseEncoding(this.featureSubset, paramDim);
    }

    // e.g.: "0,1,12,13" whitespaces are ignored
    private FeatureVectorSubsetEncoding parseEncoding(String encoding, int dimension)
            throws JCeliacGenericException
    {
        String[] vectors = encoding.split(",");
        FeatureVectorSubsetEncoding parsedEncoding = new FeatureVectorSubsetEncoding(dimension);
        try {
            for(String tmp : vectors) {
                int featureIndex = Integer.parseInt(tmp.trim());
                parsedEncoding.setFeatureIndex(featureIndex);
            }
        } catch(NumberFormatException e) {
            throw new JCeliacGenericException(e);
        }
        return parsedEncoding;
    }
}
