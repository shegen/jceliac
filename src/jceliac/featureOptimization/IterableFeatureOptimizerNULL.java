/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */


package jceliac.featureOptimization;

import jceliac.classification.ClassificationResult;

/**
 *
 * @author shegen
 */
public class IterableFeatureOptimizerNULL extends IterableFeatureOptimizer
{
    private final int featureVectorDimension;

    public IterableFeatureOptimizerNULL(int featureVectorDimension)
    {
        this.featureVectorDimension = featureVectorDimension;
    }
    
    @Override
    public FeatureVectorSubsetEncoding iterateOptimization(ClassificationResult result, 
                                                           FeatureVectorSubsetEncoding encoding)
    {
        FeatureVectorSubsetEncoding subset = new FeatureVectorSubsetEncoding(this.featureVectorDimension);
        subset.setAll();
        return subset;
    }

    @Override
    public boolean iterationConverged()
    {
        return true;
    }

    @Override
    public FeatureVectorSubsetEncoding startOptimization()
    {
        FeatureVectorSubsetEncoding subset = new FeatureVectorSubsetEncoding(this.featureVectorDimension);
        subset.setAll();
        return subset;
    }
    
}
