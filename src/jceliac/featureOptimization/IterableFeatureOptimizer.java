/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jceliac.featureOptimization;

import jceliac.classification.ClassificationResult;

/**
 *
 * @author shegen
 */
public abstract class IterableFeatureOptimizer
{
    public abstract FeatureVectorSubsetEncoding iterateOptimization(ClassificationResult result, 
                                                                    FeatureVectorSubsetEncoding encoding);
    public abstract boolean iterationConverged();
    public abstract FeatureVectorSubsetEncoding startOptimization();
}
