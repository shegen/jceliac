/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac.featureOptimization;

import java.util.ArrayList;
import jceliac.classification.ClassificationResult;

/**
 *
 * @author shegen
 */
public class FeatureOptimizationMethodSFS
        extends FeatureOptimizationMethod
{

    public FeatureOptimizationMethodSFS(FeatureOptimizationParameters parameters)
    {
        super(parameters);
    }

    @Override
    public IterableFeatureOptimizer initializeOptimization(int featureDimension)
    {
        return new IterableFeatureOptimizerSFS(featureDimension);
    }

    class IterableFeatureOptimizerSFS
            extends IterableFeatureOptimizer
    {

        private final int featureDimension;
        private int currentDimensionOfBestFeatureSubset = 0;
        private boolean converged = false;
        private boolean newGlobalMaximumFound = false;
        
        private final ArrayList<FeatureVectorSubsetEncoding> candidateSet = new ArrayList<>();
        private final FeatureVectorSubsetEncoding bestSubset;

        private ClassificationResult globalMaximumResult = null;
        private FeatureVectorSubsetEncoding globalMaximumFeatureSubset = null;

        public IterableFeatureOptimizerSFS(int featureDimension)
        {
            this.featureDimension = featureDimension;
            this.bestSubset = new FeatureVectorSubsetEncoding(this.featureDimension);
        }

        @Override
        public FeatureVectorSubsetEncoding iterateOptimization(ClassificationResult result,
                                                               FeatureVectorSubsetEncoding subset)
        {

            if(this.globalMaximumResult == null
               || (result.getOverallClassificationRate()
                   > this.globalMaximumResult.getOverallClassificationRate())) {
                this.globalMaximumResult = result;
                this.globalMaximumFeatureSubset = subset;
                this.newGlobalMaximumFound = true;
            }

            if(this.candidateSet.isEmpty()) {
                this.currentDimensionOfBestFeatureSubset++;
                if(this.newGlobalMaximumFound == false) { // converged
                    this.converged = true;
                    return null; // nothing to do anymore
                }
                this.bestSubset.setFeatureIndices(this.globalMaximumFeatureSubset);
                this.generateCandidateSet();
                this.newGlobalMaximumFound = false;

            }
            if(this.candidateSet.isEmpty()) {
                return null;
            }
            return this.candidateSet.remove(0);

        }

        @Override
        public boolean iterationConverged()
        {

            return this.currentDimensionOfBestFeatureSubset >= this.featureDimension
                   || this.converged;
        }

        @Override
        public FeatureVectorSubsetEncoding startOptimization()
        {
            this.currentDimensionOfBestFeatureSubset = 1;
            this.generateCandidateSet();

            // start of with first feature
            FeatureVectorSubsetEncoding subset = this.candidateSet.remove(0);
            return subset;
        }

        private void generateCandidateSet()
        {
            for(int i = 0; i < this.featureDimension; i++) {
                FeatureVectorSubsetEncoding subset;

                // set current maxima
                if(this.bestSubset.bitCount() > 0) {
                    subset = new FeatureVectorSubsetEncoding(this.bestSubset);
                } else {
                    subset = new FeatureVectorSubsetEncoding(this.featureDimension);
                }
                subset.setFeatureIndex(i);
                if(this.candidateSet.contains(subset) == false
                   && subset.bitCount() == this.currentDimensionOfBestFeatureSubset) {
                    this.candidateSet.add(subset);
                }
            }
        }
    }
}
