/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package jceliac;

/**
 * The general Exception thrown in the source code if anything went wrong
 * without the possibility of recovery.
 * <p>
 * @author shegen
 */
@SuppressWarnings("serial")
public class JCeliacGenericException
        extends Exception
{

    private String message = "";

    public JCeliacGenericException(Exception e) {
        this.setStackTrace(e.getStackTrace());
        this.message = e.getMessage();
    }
    public JCeliacGenericException(String message)
    {
        this.message = message;
        
    }
    public JCeliacGenericException()
    {
    }

    @Override
    public String getLocalizedMessage()
    {
        return this.message;
    }

    @Override
    public String getMessage()
    {
        return this.message;
    }
    
    

}
