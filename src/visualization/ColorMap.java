/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package visualization;

import java.util.ArrayList;
import java.util.Collections;
import jceliac.JCeliacGenericException;

/**
 * Implements a colormap, such as used by imagesc in matlab.
 * <p>
 * @author shegen
 */
public class ColorMap
{

    public enum ColorMapType
    {

        jet, // jet color map of matlab
        gray // gray color map of matlab
    };

    // support colors define points to interpolate between
    private static class SupportColor
            implements Comparable<SupportColor>
    {

        private final int index;          // the index within the colormap the support color is at
        private final double colorData;       // the original data value the color maps to directly

        // the color portions of the displayed color
        private final int redValue;
        private final int greenValue;
        private final int blueValue;

        public SupportColor(int index, double colorData, int redValue, int greenValue, int blueValue)
        {
            this.index = index;
            this.colorData = colorData;
            this.redValue = redValue;
            this.greenValue = greenValue;
            this.blueValue = blueValue;
        }

        @Override
        public int compareTo(SupportColor t)
        {
            return (this.index - t.index);
        }

    }

    // this is specific to the color map
    private final ArrayList<SupportColor> supportColors;

    private ColorMap()
    {
        this.supportColors = new ArrayList<>();
    }

    protected void init()
    {
        Collections.sort(this.supportColors);
    }

    /**
     * Maps an input value to a color.
     * <p>
     * @param value Input value of data.
     * <p>
     * @return Color to display value as.
     */
    public int[] mapValueToColor(double value)
    {
        SupportColor[] sc = this.getSupportColorsForValue(value);
        return this.interpolateColor(sc[0], sc[1], value);
    }

    /**
     * Interpolates color for a value based on a color map.
     * <p>
     * @param left  Left interpolation support point.
     * @param right Right interpolation support point.
     * @param value Value to map to color.
     * <p>
     * @return Color for value.
     */
    private int[] interpolateColor(SupportColor left, SupportColor right, double value)
    {
        double width = (right.colorData - left.colorData);
        if(width == 0) {
            int[] ret = {left.redValue, left.greenValue, left.blueValue};
            return ret;
        }

        double kRed = (right.redValue - left.redValue) / (right.colorData - left.colorData);
        int redInterpolatedValue = (int) Math.round(left.redValue + ((value - left.colorData) * kRed));

        double kGreen = (right.greenValue - left.greenValue) / (right.colorData - left.colorData);
        int greenInterpolatedValue = (int) Math.round(left.greenValue + ((value - left.colorData) * kGreen));

        double kBlue = (right.blueValue - left.blueValue) / (right.colorData - left.colorData);
        int blueInterpolatedValue = (int) Math.round(left.blueValue + ((value - left.colorData) * kBlue));

        int[] ret = {redInterpolatedValue, greenInterpolatedValue, blueInterpolatedValue};
        return ret;
    }

    /**
     * Computes the support points to interpolate a color for a value.
     * <p>
     * @param value Value to map to a color.
     * <p>
     * @return Support points for interpolation.
     */
    private SupportColor[] getSupportColorsForValue(double value)
    {
        SupportColor left = null;
        SupportColor right = null;
        for(SupportColor cTmp : this.supportColors) {
            if(Math.abs(cTmp.colorData - value) < 1e-10 || cTmp.colorData < value) {
                left = cTmp;
            }
            if(right == null && (Math.abs(cTmp.colorData - value) < 1e-10 || cTmp.colorData > value)) {
                right = cTmp;
            }
        }
        if(right == null) {
            right = this.supportColors.get(this.supportColors.size() - 1);
        }
        SupportColor[] ret = new SupportColor[2];
        ret[0] = left;
        ret[1] = right;
        return ret;
    }

    /**
     * Creates a color map.
     * <p>
     * @param type Specifies type of color map.
     * @param data Data that is displayed.
     * <p>
     * @return Color map for data.
     * <p>
     * @throws JCeliacGenericException
     */
    public static ColorMap createColorMap(ColorMapType type, double[][] data)
            throws JCeliacGenericException
    {

        // figure out range of data
        double min = ColorMap.getMinimum(data);
        double max = ColorMap.getMaximum(data);

        switch(type) {
            case jet:
                return createColorMapJet(min, max, 64);
            case gray:
                return createColorMapGray(min, max, 64);
        }
        throw new JCeliacGenericException("Unknown ColorMap");
    }

    private static double getMinimum(double[][] data)
    {
        double min = Double.MAX_VALUE;
        for(double[] tmp : data) {
            for(int j = 0; j < data[0].length; j++) {
                if(tmp[j] < min) {
                    min = tmp[j];
                }
            }
        }
        return min;
    }

    private static double getMaximum(double[][] data)
    {
        double max = Double.MIN_VALUE;
        for(double[] tmp : data) {
            for(int j = 0; j < data[0].length; j++) {
                if(tmp[j] > max) {
                    max = tmp[j];
                }
            }
        }
        return max;
    }

    private void addSupportColor(SupportColor color)
    {
        this.supportColors.add(color);
    }

    /**
     * Creates the JET color map.
     * <p>
     * @param minValue      Minimum value in data.
     * @param maxValue      Maximum value in data.
     * @param colorMapWidth Width of color map.
     * <p>
     * @return Color map for data.
     */
    private static ColorMap createColorMapJet(double minValue, double maxValue, int colorMapWidth)
    {

        double colorStep = maxValue / (double) colorMapWidth;

        SupportColor s1 = new SupportColor(0, minValue, 0, 0, 143);
        SupportColor s2 = new SupportColor(7, 7 * colorStep, 0, 0, 255);
        SupportColor s3 = new SupportColor(23, 23 * colorStep, 0, 255, 255);
        SupportColor s4 = new SupportColor(39, 39 * colorStep, 255, 255, 0);
        SupportColor s5 = new SupportColor(55, 55 * colorStep, 255, 0, 0);
        SupportColor s6 = new SupportColor(63, 63 * colorStep, 128, 0, 0);

        ColorMap map = new ColorMap();
        map.addSupportColor(s1);
        map.addSupportColor(s2);
        map.addSupportColor(s3);
        map.addSupportColor(s4);
        map.addSupportColor(s5);
        map.addSupportColor(s6);

        map.init();
        return map;
    }

    /**
     * Creates the GRAY color map.
     * <p>
     * @param minValue      Minimum value in data.
     * @param maxValue      Maximum value in data.
     * @param colorMapWidth Width of color map.
     * <p>
     * @return Color map for data.
     */
    private static ColorMap createColorMapGray(double minValue, double maxValue, int colorMapWidth)
    {

        double colorStep = maxValue / (double) colorMapWidth;

        SupportColor s1 = new SupportColor(0, minValue, 0, 0, 0);
        SupportColor s2 = new SupportColor(63, 63 * colorStep, 255, 255, 255);

        ColorMap map = new ColorMap();
        map.addSupportColor(s1);
        map.addSupportColor(s2);

        map.init();
        return map;
    }

}
