/*
 *  JCeliac Copyright (C) 2010-2014 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package visualization;

import jceliac.JCeliacGenericException;
import jceliac.logging.JCeliacLogger;
import java.awt.Cursor;
import jceliac.Main;

/**
 *
 * @author shegen
 */
public class VisualizationFrame
        extends javax.swing.JFrame
{

    private int[][][] scaledColoredData;
    private double[][] unscaledData;

    /**
     * Creates new form VisualizationFrame
     */
    public VisualizationFrame()
    {
        initComponents();
    }

    public void imagescArray(double[][] array, ColorMap.ColorMapType type)
            throws JCeliacGenericException
    {

        this.unscaledData = array;
        int[][][] scaledArray = this.scaleData(array, ColorMap.createColorMap(type, array));
        this.scaledColoredData = scaledArray;
        int[][][] scaledImage = this.scaleImageToPanelSize(scaledArray);
        this.paintablePanel1.imagescArray(scaledImage, scaledImage.length, scaledImage[0].length);

        this.paintablePanel1.setVisible(true);
    }

    private int[][][] scaleImageToPanelSize(int[][][] scaledData)
            throws JCeliacGenericException
    {
        int width = this.paintablePanel1.getWidth();
        int height = this.paintablePanel1.getHeight();

        int[][][] scaledImage = new int[width][height][3];
        double scaleFactorWidth = (double) (scaledData.length - 1) / (double) (scaledImage.length - 1);
        double scaleFactorHeight = (double) (scaledData[0].length - 1) / (double) (scaledImage[0].length - 1);

        for(int x = 0; x < scaledImage.length; x++) {
            for(int y = 0; y < scaledImage[0].length; y++) {
                double posX = (double) x * scaleFactorWidth;
                double posY = (double) y * scaleFactorHeight;

                // interpolate at position in scaledData
                double interpolatedValueRed = this.getInterpolatedPixelValueFast(scaledData, 0, posX, posY);
                double interpolatedValueGreen = this.getInterpolatedPixelValueFast(scaledData, 1, posX, posY);
                double interpolatedValueBlue = this.getInterpolatedPixelValueFast(scaledData, 2, posX, posY);

                scaledImage[x][y][0] = (int) Math.round(interpolatedValueRed);
                scaledImage[x][y][1] = (int) Math.round(interpolatedValueGreen);
                scaledImage[x][y][2] = (int) Math.round(interpolatedValueBlue);

            }
        }
        return scaledImage;
    }

    protected double getInterpolatedPixelValueFast(final int[][] data, double x, double y)
            throws JCeliacGenericException
    {

        int fx = (int) x;
        int fy = (int) y;
        int cx = (int) Math.ceil(x);
        int cy = (int) Math.ceil(y);

        double ty = y - fy;
        double tx = x - fx;

        double invtx = 1 - tx;
        double invty = 1 - ty;

        double w1 = invtx * invty;
        double w2 = tx * invty;
        double w3 = invtx * ty;
        double w4 = tx * ty;

        double ret = w1 * data[fx][fy] + w2 * data[cx][fy] + w3 * data[fx][cy] + w4 * data[cx][cy];
        return ret;
    }

    protected double getInterpolatedPixelValueFast(final int[][][] data, int channel, double x, double y)
            throws JCeliacGenericException
    {

        int fx = (int) x;
        int fy = (int) y;
        int cx = (int) Math.ceil(x);
        int cy = (int) Math.ceil(y);

        double ty = y - fy;
        double tx = x - fx;

        double invtx = 1 - tx;
        double invty = 1 - ty;

        double w1 = invtx * invty;
        double w2 = tx * invty;
        double w3 = invtx * ty;
        double w4 = tx * ty;

        double ret = w1 * data[fx][fy][channel] + w2 * data[cx][fy][channel] + w3 * data[fx][cy][channel] + w4 * data[cx][cy][channel];
        return ret;
    }

    private int[][] scaleData(double[][] array)
    {
        // find max and min inside array
        double max = Double.MIN_VALUE;
        double min = Double.MAX_VALUE;
        for(double[] tmp : array) {
            for(int j = 0; j < array[0].length; j++) {
                if(tmp[j] > max) {
                    max = tmp[j];
                }
                if(tmp[j] < min) {
                    min = tmp[j];
                }
            }
        }
        double width = max - min;
        double k = 255.0d / width;
        int[][] scaledBuffer = new int[array.length][array[0].length];
        for(int i = 0; i < array.length; i++) {
            for(int j = 0; j < array[0].length; j++) {
                double s = k * (array[i][j] - min);
                int scaledData = (int) Math.round(s);
                scaledBuffer[i][j] = scaledData;
            }
        }
        return scaledBuffer;
    }

    private int[][][] scaleData(double[][] array, ColorMap map)
    {
        int[][][] data = new int[array.length][array[0].length][3];
        for(int i = 0; i < array.length; i++) {
            for(int j = 0; j < array[0].length; j++) {
                int[] coloredData = map.mapValueToColor(array[i][j]);
                data[i][j][0] = coloredData[0];
                data[i][j][1] = coloredData[1];
                data[i][j][2] = coloredData[2];
            }
        }
        return data;

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jDialog1 = new javax.swing.JDialog();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        paintablePanel1 = new visualization.PaintablePanel();

        jPopupMenu1.setMaximumSize(new java.awt.Dimension(32, 32));
        jPopupMenu1.setMinimumSize(new java.awt.Dimension(32, 32));

        jDialog1.setUndecorated(true);

        jLabel1.setText("X:");

        jLabel2.setText("R:");

        jLabel3.setText("Value:");

        jLabel4.setText("Y:");

        jLabel5.setText("G:");

        jLabel6.setText("B:");

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jDialog1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel5))
                            .addGroup(jDialog1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel4)))
                        .addGap(27, 27, 27)
                        .addComponent(jLabel6))
                    .addComponent(jLabel3))
                .addContainerGap(44, Short.MAX_VALUE))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addGap(3, 3, 3)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.Y_AXIS));

        jToolBar1.setRollover(true);

        jButton1.setText("Data Cursor");
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        getContentPane().add(jToolBar1);

        paintablePanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                paintablePanel1MouseClicked(evt);
            }
        });
        paintablePanel1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                paintablePanel1MouseDragged(evt);
            }
        });

        javax.swing.GroupLayout paintablePanel1Layout = new javax.swing.GroupLayout(paintablePanel1);
        paintablePanel1.setLayout(paintablePanel1Layout);
        paintablePanel1Layout.setHorizontalGroup(
            paintablePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        paintablePanel1Layout.setVerticalGroup(
            paintablePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 269, Short.MAX_VALUE)
        );

        getContentPane().add(paintablePanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formComponentResized(java.awt.event.ComponentEvent evt)//GEN-FIRST:event_formComponentResized
    {//GEN-HEADEREND:event_formComponentResized

        if(this.scaledColoredData != null) {
            try {
                this.paintablePanel1.repaint();

                // update position of the DataInformationDialog#
                int width = this.paintablePanel1.getWidth() - 1;
                int height = this.paintablePanel1.getHeight() - 1;

                int origX = this.diag.getOriginalPositionX();
                int origY = this.diag.getOriginalPositionY();

                double scaleX = width / (this.scaledColoredData.length - 1);
                double scaleY = height / (this.scaledColoredData[0].length - 1);

                int newX = (int) Math.round(scaleX * origX);
                int newY = (int) Math.round(scaleY * origY);

                this.diag.setLocation((int) Math.round(this.paintablePanel1.getLocationOnScreen().getX() + newX), (int) Math.round(this.paintablePanel1.getLocationOnScreen().getY() + newY));

            } catch(Exception e) {
                Main.logException(JCeliacLogger.LOG_ERROR, e);
            }
        }
    }//GEN-LAST:event_formComponentResized

    @SuppressWarnings("deprecation")
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton1ActionPerformed
    {//GEN-HEADEREND:event_jButton1ActionPerformed
        this.setCursor(Cursor.CROSSHAIR_CURSOR);
    }//GEN-LAST:event_jButton1ActionPerformed

    private DataInformationDialog diag;

    @SuppressWarnings("deprecation")
    private void paintablePanel1MouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_paintablePanel1MouseClicked
    {//GEN-HEADEREND:event_paintablePanel1MouseClicked
        if(this.getCursorType() == Cursor.CROSSHAIR_CURSOR) {
            double x = evt.getX();
            double y = evt.getY();

            // compute the current scaling factors
            double scaleX = ((double) this.paintablePanel1.getWidth() - 1) / ((double) this.scaledColoredData.length - 1);
            double scaleY = ((double) this.paintablePanel1.getHeight() - 1) / ((double) this.scaledColoredData[0].length - 1);

            final int posX = (int) Math.round(x / scaleX);
            final int posY = (int) Math.round(y / scaleY);

            final int evtX = evt.getXOnScreen();
            final int evtY = evt.getYOnScreen();

            if(this.diag != null) {
                this.diag.setVisible(false);
            }
            this.diag = new DataInformationDialog(VisualizationFrame.this, false, posX, posY, null, this.unscaledData[posX][posY]);
            this.diag.setSize(130, 35);

            this.diag.setLocation(evtX + 5, evtY - this.diag.getHeight() - 5);
            this.diag.setVisible(true);
            this.paintablePanel1.repaint();
        }
    }//GEN-LAST:event_paintablePanel1MouseClicked

    private void paintablePanel1MouseDragged(java.awt.event.MouseEvent evt)//GEN-FIRST:event_paintablePanel1MouseDragged
    {//GEN-HEADEREND:event_paintablePanel1MouseDragged

        double x = evt.getX();
        double y = evt.getY();

        // compute the current scaling factors
        double scaleX = (this.paintablePanel1.getWidth() - 1) / (this.scaledColoredData.length - 1);
        double scaleY = (this.paintablePanel1.getHeight() - 1) / (this.scaledColoredData[0].length - 1);

        final int posX = (int) Math.round(x / scaleX);
        final int posY = (int) Math.round(y / scaleY);

        final int evtX = evt.getXOnScreen();
        final int evtY = evt.getYOnScreen();

        if(this.diag.getOriginalPositionX() == posX && this.diag.getOriginalPositionY() == posY) {
            return;
        }
        this.diag.close();
        this.diag = new DataInformationDialog(VisualizationFrame.this, false, posX, posY, null, this.unscaledData[posX][posY]);
        this.diag.setSize(130, 35);
        this.diag.setLocation(evtX, evtY);
        this.diag.setVisible(true);
    }//GEN-LAST:event_paintablePanel1MouseDragged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JToolBar jToolBar1;
    private visualization.PaintablePanel paintablePanel1;
    // End of variables declaration//GEN-END:variables
}
