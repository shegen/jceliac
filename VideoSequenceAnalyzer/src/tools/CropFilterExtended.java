/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package tools;

import data.LPFrame;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import jceliac.tools.data.Frame;
import jceliac.video.CropFilter;

/**
 *
 * @author shegen
 */
public class CropFilterExtended extends CropFilter
{

    private final int WINDOW_SIZE = 400;
    private final int BIG_ENDO_X = 190;
    private final int BIG_ENDO_Y = 85;
    private final int SMALL_ENDO_X = 230;
    private final int SMALL_ENDO_Y = 75;
    private final int TEST_X = 130;
    private final int TEST_Y = 350;
    private final int TEST_WINDOW_SIZE = 50;
    private final int ENDO_TYPE_BIG = 1;
    private final int ENDO_TYPE_SMALL = 2;

    
    
    @Override
    public double[] crop1D(BufferedImage image, int colorChannel)
    {
        int endoType = this.getEndoType(image);
        Rectangle rect;
        Raster raster;
        
        int w = image.getWidth();
        int h = image.getHeight();
        switch(endoType)
        {
            case ENDO_TYPE_BIG:
                raster = image.getData();
                BufferedImage croppedImage = image.getSubimage(BIG_ENDO_X, BIG_ENDO_Y, WINDOW_SIZE, WINDOW_SIZE);
                return croppedImage.getRaster().getSamples(0, 0, croppedImage.getWidth(), croppedImage.getHeight(), colorChannel, (double []) null);
                
              //  return raster.getSamples(BIG_ENDO_X, BIG_ENDO_Y, WINDOW_SIZE, WINDOW_SIZE, colorChannel, (double [])null);
            
            default:
                raster = image.getData();
                return raster.getSamples(SMALL_ENDO_X, SMALL_ENDO_Y, WINDOW_SIZE, WINDOW_SIZE, colorChannel, (double [])null);    
        }
    }
    
    public BufferedImage crop(BufferedImage image)
    {
        int endoType = this.getEndoType(image);
        BufferedImage croppedImage;
        
        switch(endoType)
        {
            case ENDO_TYPE_BIG:
                croppedImage = image.getSubimage(BIG_ENDO_X, BIG_ENDO_Y, WINDOW_SIZE, WINDOW_SIZE);
                return croppedImage;  
             
            
            default:
                croppedImage = image.getSubimage(SMALL_ENDO_X, SMALL_ENDO_Y, WINDOW_SIZE, WINDOW_SIZE);
                return croppedImage;    
        }
    }
    
    protected int getEndoType(LPFrame frame)
    
    {   
        int[][] grayData = frame.getGrayData();

        // the small endo has only black pixels in this region
        for (int i = TEST_X; i < TEST_X + TEST_WINDOW_SIZE; i++) {
            for (int j = TEST_Y; j < TEST_Y + TEST_WINDOW_SIZE; j++) {
                if (grayData[i][j] != 255) {
                    return ENDO_TYPE_BIG;
                }
            }
        }
        return ENDO_TYPE_SMALL;
    }
     
     
    public Frame crop(Frame frame)
    {
        int endoType = getEndoType(frame);
        switch (endoType) {
            case ENDO_TYPE_BIG:
                frame.crop(BIG_ENDO_X, BIG_ENDO_Y, WINDOW_SIZE);
                break;

            case ENDO_TYPE_SMALL:
                frame.crop(SMALL_ENDO_X, SMALL_ENDO_Y, WINDOW_SIZE);
        }
        return frame;
    }
    
    public LPFrame crop(LPFrame frame)
    {
        int endoType = getEndoType(frame);
        switch (endoType) {
            case ENDO_TYPE_BIG:
                frame.crop(BIG_ENDO_X, BIG_ENDO_Y, WINDOW_SIZE);
                break;

            case ENDO_TYPE_SMALL:
                frame.crop(SMALL_ENDO_X, SMALL_ENDO_Y, WINDOW_SIZE);
        }
        return frame;
    }
    
    
}
