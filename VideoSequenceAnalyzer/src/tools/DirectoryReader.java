/*
 *  JCeliac Copyright (C) 2010-2011 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package tools;

import java.util.*;
import java.io.*;


/**
 *
 * @author shegen
 */
public class DirectoryReader
{

    // this method identifies the classes distinguished by subdirectories
    // and returns the base directory of each separate class;
    public final ArrayList<File> prepareBaseDirectories(String path)
            throws Exception
    {
        ArrayList<File> baseDirectories = new ArrayList<File>();
        try {
            File parentDirectory = new File(path);
            if (parentDirectory == null) {
                //  Main.log(JCeliacLogger.LOG_ERROR,"Invalid Image Base Directory!");
                throw new Exception("Data Directory does not exist!");
            }
            if (parentDirectory.isDirectory() == false) {
                //  Main.log(JCeliacLogger.LOG_ERROR,"Invalid Image Base Directory!");
                throw new Exception("Data Directory is not a Directory!");
            }
            ArrayList<File> root = new ArrayList<File>();
            root.add(parentDirectory);
            collectSubdirectories(root, baseDirectories);
            return baseDirectories;
        } catch (Exception e) {
            if (e instanceof Exception) {
                throw e;
            }
            String message = e.getMessage();
            throw new Exception((message == null ? "Unknown Error" : message));
        }
    }

    private void collectSubdirectories(ArrayList<File> directories, ArrayList<File> baseDirectories)
    {
        FileFilter directoryFilter = new DirectoryFileFilter();
        ArrayList<File> subDirectories = new ArrayList<File>();

        // done if no subdirectories are left
        if (directories.isEmpty()) {
            return;
        }
        for (int index = 0; index < directories.size(); index++) {
            File currentDirectory = directories.get(index);
            File[] childs = (File[]) currentDirectory.listFiles(directoryFilter);

            if (childs.length == 0) // no subdirectories, this is the base directory of a class
            {
                baseDirectories.add(currentDirectory);
            } else {
                for (int i = 0; i < childs.length; i++) {
                    subDirectories.add(new File(childs[i].toURI()));
                }
            }
        }
        collectSubdirectories(subDirectories, baseDirectories);
    }

    private class DirectoryFileFilter implements FileFilter
    {

        public boolean accept(File pathname)
        {
            return (pathname.isDirectory() == true);
        }
    }
}
