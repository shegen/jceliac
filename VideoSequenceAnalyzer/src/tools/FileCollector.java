/*
 *  JCeliac Copyright (C) 2010-2011 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package tools;

import java.io.*;
import java.util.*;

/**
 * This class starts from a base directory and collects all files within this directory
 * and all its subdirectories, and their subdirectories recursively.
 * 
 * @author shegen
 */
public class FileCollector
{
    private File parentDirectory;
    private ArrayList <File> collectedFiles;
    private int fileType = FILE_TYPE_VOB;
    
    public static final int FILE_TYPE_VOB = 1;
    public static final int FILE_TYPE_PNG = 2;
    
    
    public FileCollector(File file, int fileType) 
            throws FileCollectorException
    {
            this.parentDirectory = file;
            if(this.parentDirectory.exists() == false ||  this.parentDirectory.isDirectory() == false) {
                throw new FileCollectorException();
            }  
            this.fileType = fileType;
    }

    public ArrayList<File> getCollectedFiles()
    {
        return collectedFiles;
    }
    
    public void collectFiles() {
        this.collectedFiles = new ArrayList<File>();
        collectFilesRecursively(this.parentDirectory);
    }
    
    private void collectFilesRecursively(File currentDirectory) {
        File [] subdirectories = getSubdirectoriesForDirectory(currentDirectory);
        File [] files = getFilesForDirectory(currentDirectory);
        
        // add files to collected file list
        this.collectedFiles.addAll(Arrays.asList(files));
        
        // go through all subdirectories
        for(int i = 0; i < subdirectories.length; i++) {
            collectFilesRecursively(subdirectories[i]);
        }
    }
    
    private File [] getFilesForDirectory(File directory) {
        File [] files = null;
        
        switch(this.fileType)
        {
            case FileCollector.FILE_TYPE_PNG:
                files = directory.listFiles(new FileFilterPNG());
                break;
                
            case FileCollector.FILE_TYPE_VOB:
                files = directory.listFiles(new FileFilterVOB());
                break;
                
        }
        return files;
    }
    
    private File [] getSubdirectoriesForDirectory(File directory) {
        File [] files = directory.listFiles(new DirectoryFilter());
        return files;
    }
    
    
    private class FileFilterVOB implements FileFilter {

        public boolean accept(File file)
        {
            if(file.getName().toLowerCase().endsWith(".vob")) {
                return true;
            }
            return false;
        }
    }
    
    private class FileFilterPNG implements FileFilter {

        public boolean accept(File file)
        {
            if(file.getName().toLowerCase().endsWith(".png")) {
                return true;
            }
            return false;
        }
    }
      
    
    private class DirectoryFilter implements FileFilter {

        public boolean accept(File file)
        {
            if(file.isDirectory()) {
                return true;
            }
            return false;
        }
        
    }
    
}
