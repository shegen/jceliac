/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package tools;

/**
 *
 * @author shegen
 */
public class ArrayTools
{
    
   /* public static double [] convert2Dto1D(double [][] array) 
    {
        int w = array.length;
        int h = array[0].length;
        
        double [] array1D = new double[w*h];
        for(int i = 0; i < w; i++) {
            for(int j = 0; j < h; h++) {
                array1D[j*w+i] = array[i][j];
            }
        }
        return array1D;
    }*/
    
    
    public static double [] convert2Dto1D(double [][] array) 
    {
        int w = array.length;
        int h = array[0].length;
        
        double [] array1D = new double[w*h];
        int pos = 0;
        for(int i = 0; i < w; i++) {
               
               System.arraycopy(array[i], 0, array1D, pos, h);
               pos += h;
        }
        return array1D;
    }
    
    public static double [] convert2Dto1D(int [][] array) 
    {
        int w = array.length;
        int h = array[0].length;
        
        double [] array1D = new double[w*h];
        int pos = 0;
        for(int i = 0; i < w; i++) {
               
               System.arraycopy(array[i], 0, array1D, pos, h);
               pos += h;
        }
        return array1D;
    }
    
    
    public static double[][] convert1Dto2D(double [] array, int width, int height)
    {
        double [][] array2D = new double[width][height];
        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                array2D[i][j] = array[j*width+i];
            }
        }
        return array2D;
    }
    
    public static double [][] convertShortToDouble(short [][]data) 
    {
        double [][] outData = new double[data.length][data[0].length];
        for(int i = 0; i < data.length; i++) {
            for(int j = 0; j < data[0].length; j++) {
                outData[i][j] = (double)data[i][j];
            }
        }
        return outData;
    }
    
    public static double [][] convertIntToDouble(int [][]data) 
    {
        double [][] outData = new double[data.length][data[0].length];
        for(int i = 0; i < data.length; i++) {
            for(int j = 0; j < data[0].length; j++) {
                outData[i][j] = (double)data[i][j];
            }
        }
        return outData;
    }
    
    public static int [][] convertDoubleToInt(double [][] data) 
    {
        int [][] outData = new int[data.length][data[0].length];
        for(int i = 0; i < data.length; i++) {
            for(int j = 0; j < data[0].length; j++) {
                outData[i][j] = (int)data[i][j];
            }
        }
        return outData;
    }
    
    public static int [][] cloneArray2D(int [][] array)
    {
        int [][] copy = array.clone();
        
        for(int i = 0; i < copy.length; i++) {
            copy[i] = array[i].clone();
        }
        return copy;
    }
    
    public static int minIndex(int [] array)
    {
        int minValue = Integer.MAX_VALUE;
        int minIndex = -1;
        
        for(int i = 0; i < array.length; i++) {
            if(array[i] < minValue) {
                minIndex = i;
                minValue = array[i];
            }
        }
        return minIndex;
    }
    
    public static int minValue(int [] array)
    {
        int minValue = Integer.MAX_VALUE;
     
        for(int i = 0; i < array.length; i++) {
            if(array[i] < minValue) {
                minValue = array[i];
            }
        }
        return minValue;
    }
    
}
