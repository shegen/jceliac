/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package deinterlace.textureDetection;

import data.LPFrame;
import deinterlace.DeinterlacingFilter;
import jceliac.experiment.Experiment;
import tools.ArrayTools;

/**
 * Implements the method presented in: 
 * "Motion Adaptive Deinterlacing of Video Data with Texture Detection" 
 * by Sheng, Zhang, Zhang and Li
 * @author shegen
 */
public class MATextureDetectionDeinterlacer extends DeinterlacingFilter
{

    private final int MOTION_THRESHOLD = 50;
    private final double TEXTURE_THRESHOLD = 100.0d;

    @Override
    public int getRequiredNumberOfFutureFrames()
    {
        return 1;
    }

    @Override
    public int getRequiredNumberOfPastFrames()
    {
        return 2;
    }

    @Override
    public LPFrame performDeinterlace(boolean topField) throws Exception
    {
        LPFrame deinterlacedFrame = super.copyGoodData(topField, this.currentFrame);
        
        int [][] currentRedData   = this.currentFrame.getRedData();
        int [][] currentGreenData = this.currentFrame.getGreenData();
        int [][] currentBlueData  = this.currentFrame.getBlueData();
        int [][] currentGrayData  = this.currentFrame.getGrayData();
        
        int [][] prev2RedData   = this.pastFrames.get(0).getRedData();
        int [][] prev2GreenData = this.pastFrames.get(0).getGreenData();
        int [][] prev2BlueData  = this.pastFrames.get(0).getBlueData();
        int [][] prev2GrayData  = this.pastFrames.get(0).getGrayData();
        
        int [][] prevRedData   = this.pastFrames.get(1).getRedData();
        int [][] prevGreenData = this.pastFrames.get(1).getGreenData();
        int [][] prevBlueData  = this.pastFrames.get(1).getBlueData();
        int [][] prevGrayData  = this.pastFrames.get(1).getGrayData();
        
        int [][] nextRedData   = this.futureFrames.get(0).getRedData();
        int [][] nextGreenData = this.futureFrames.get(0).getGreenData();
        int [][] nextBlueData  = this.futureFrames.get(0).getBlueData();
        int [][] nextGrayData  = this.futureFrames.get(0).getGrayData();
        
        deinterlacedFrame.setRedData(this.deinterlaceChannel(topField, currentRedData, prevRedData, prev2RedData, nextRedData, deinterlacedFrame.getRedData()));
        deinterlacedFrame.setGreenData(this.deinterlaceChannel(topField, currentGreenData, prevGreenData, prev2GreenData, nextGreenData, deinterlacedFrame.getGreenData()));
        deinterlacedFrame.setBlueData(this.deinterlaceChannel(topField, currentBlueData, prevBlueData, prev2BlueData, nextBlueData, deinterlacedFrame.getBlueData()));
        deinterlacedFrame.setGrayData(this.deinterlaceChannel(topField, currentGrayData, prevGrayData, prev2GrayData, nextGrayData, deinterlacedFrame.getGrayData()));
        
        return deinterlacedFrame;

    }

    private int[][] deinterlaceChannel(boolean topField, int[][] cur, int[][] prev2, int[][] prev, int[][] next, int [][] deinterlacedData)
            throws Exception
    {
        boolean[][] motionInformation = this.detectMotion(topField, cur, prev2, prev, next);
        boolean[][] textureInformation = this.detectTexture(topField, cur);

        if (topField) {
            for (int row = 3; row < cur.length - 3; row += 2) {
                for (int col = 2; col < cur[0].length-2; col++) {
                    int deinterlacedPixelValue;
                    
                    if(motionInformation[row][col]) {
                        if(textureInformation[row][col]) {
                            deinterlacedPixelValue = this.threeDimensionalELAFilter(cur, prev, next, row, col);
                        }else {
                            deinterlacedPixelValue = this.verticalTemporalFilter(cur, prev, row, col);
                        }
                    }else {
                        if(textureInformation[row][col]) {
                            deinterlacedPixelValue = this.modifiedELAFilter(cur, prev, row, col);
                        }else {
                            deinterlacedPixelValue = this.verticalTemporalMedianFilter(cur, prev, row, col);
                        }
                    }
                    deinterlacedData[row][col] = deinterlacedPixelValue;
                }
                
            }
        } else {
            for (int row = 4; row < cur.length - 3; row += 2) {
                for (int col = 2; col < cur[0].length-2; col++) {
                    int deinterlacedPixelValue;
                    
                    if(motionInformation[row][col]) {
                        if(textureInformation[row][col]) {
                             deinterlacedPixelValue = this.threeDimensionalELAFilter(cur, prev, next, row, col);
                        }else {
                            deinterlacedPixelValue = this.verticalTemporalFilter(cur, prev, row, col);
                        }
                    }else {
                        if(textureInformation[row][col]) {
                            deinterlacedPixelValue = this.modifiedELAFilter(cur, prev, row, col);
                        }else {
                            deinterlacedPixelValue = this.verticalTemporalMedianFilter(cur, prev, row, col);
                        }
                    }
                    deinterlacedData[row][col] = deinterlacedPixelValue;
                }
            }
        }
        return deinterlacedData;
    }

    private int interpolateTemporal(int direction, int[][] prev, int[][] next, int row, int col)
            throws Exception
    {
        switch (direction) {
            case 0:
                return (prev[row - 2][col - 1] + next[row + 2][col + 1]) >> 1;
            case 1:
                return (prev[row - 2][col] + next[row + 2][col]) >> 1;
            case 2:
                return (prev[row - 2][col + 1] + next[row + 2][col - 1]) >> 1;
            case 3:
                return (prev[row + 2][col - 1] + next[row - 2][col + 1]) >> 1;
            case 4:
                return (prev[row + 2][col] + next[row - 2][col]) >> 1;
            case 5:
                return (prev[row + 2][col + 1] + next[row - 2][col - 1]) >> 1;
        }
        throw new Exception("Undefined Direction!");
    }

    private int interpolateSpatial(int direction, int[][] cur, int row, int col)
            throws Exception
    {
        switch (direction) {
            case 0:
                return (cur[row - 1][col - 1] + cur[row + 1][col + 1]) >> 1;
            case 1:
                return (cur[row - 1][col] + cur[row + 1][col]) >> 1;
            case 2:
                return (cur[row - 1][col + 1] + cur[row + 1][col - 1]) >> 1;
        }
        throw new Exception("Undefined Direction!");
    }

    // The definition of 3D ELA in the original paper is wrong, we therefore use the
    // "better" interpretation as stated in "A Motion-Adaptive Deinterlacer
    // via Hybrid Motion Detection and Edge-Pattern-Recognition".
    private int threeDimensionalELAFilter(int[][] cur, int[][] prev, int[][] next, int row, int col)
            throws Exception
    {
        int[] temporalDifferences = new int[6];
        int[] spatialDifferences = new int[3];

        temporalDifferences[0] = Math.abs(prev[row - 2][col - 1] - next[row + 2][col + 1]);
        temporalDifferences[1] = Math.abs(prev[row - 2][col] - next[row + 2][col]);
        temporalDifferences[2] = Math.abs(prev[row - 2][col + 1] - next[row + 2][col - 1]);

        temporalDifferences[3] = Math.abs(prev[row + 2][col - 1] - next[row - 2][col + 1]);
        temporalDifferences[4] = Math.abs(prev[row + 2][col] - next[row - 2][col]);
        temporalDifferences[5] = Math.abs(prev[row + 2][col + 1] - next[row - 2][col - 1]);

        spatialDifferences[0] = Math.abs(cur[row - 1][col - 1] - cur[row + 1][col + 1]);
        spatialDifferences[1] = Math.abs(cur[row - 1][col] - cur[row + 1][col]);
        spatialDifferences[2] = Math.abs(cur[row - 1][col + 1] - cur[row + 1][col - 1]);

        int minTemporalValue = ArrayTools.minValue(temporalDifferences);
        int minSpatialValue = ArrayTools.minValue(spatialDifferences);

        if (minTemporalValue < minSpatialValue) {
            int direction = ArrayTools.minIndex(temporalDifferences);
            return this.interpolateTemporal(direction, prev, next, row, col);
        } else {
            int direction = ArrayTools.minIndex(spatialDifferences);
            return this.interpolateSpatial(direction, cur, row, col);
        }
    }

    private int verticalTemporalFilter(int[][] cur, int[][] prev, int row, int col)
    {
        double deinterlacedSample = (cur[row - 3][col]) * 1
                + (prev[row - 2][col] * (-5))
                + (cur[row - 1][col] * 8)
                + (prev[row][col] * 10)
                + (cur[row + 1][col] * 8)
                + (prev[row + 2][col] * (-5))
                + (cur[row + 3][col]) * 1;
        
        if(deinterlacedSample < 0) {
            deinterlacedSample = 0;
        }
        return (int) ((deinterlacedSample/18.0d) + 0.5d); // round up
    }
    
    
    private int modifiedELAFilter(int [][] cur, int [][] prev, int row, int col)
            throws Exception
    {
        int [] differences = new int[5];
        
        differences[0] = Math.abs(cur[row-1][col-2] - cur[row+1][col+2]);
        differences[1] = Math.abs(cur[row-1][col-1] - cur[row+1][col+1]);
        differences[2] = Math.abs(cur[row-1][col]   - cur[row+1][col]);
        differences[3] = Math.abs(cur[row-1][col+1] - cur[row+1][col-1]);
        differences[4] = Math.abs(cur[row-1][col+2] - cur[row+1][col-2]);
        
        int direction = ArrayTools.minIndex(differences);
        int directionalInterpolatedValue = this.interpolateModifiedELA(direction, cur, row, col);
        int verticalInterpolatedValue = (cur[row-1][col] + cur[row+1][col]) >> 1;
        int pastValue = prev[row][col];
        
        return this.median(directionalInterpolatedValue, verticalInterpolatedValue, pastValue);
    }
    
    private int interpolateModifiedELA(int direction, int [][] cur, int row, int col)
            throws Exception
    {
        switch(direction) 
        {
            case 0: return (cur[row-1][col-2] + cur[row+1][col+2]) >> 1;
            case 1: return (cur[row-1][col-1] + cur[row+1][col+1]) >> 1;
            case 2: return (cur[row-1][col]   + cur[row+1][col]) >> 1;
            case 3: return (cur[row-1][col+1] + cur[row+1][col-1]) >> 1;
            case 4: return (cur[row-1][col+2] + cur[row+1][col-2]) >> 1;                 
        }
        throw new Exception("Undefined Direction!");
    }
    

    /* Caller must be sure that indices are valid. 
     * Method uses upper and lower row of current frame and the current position of the pixel 
     * in the previous frame. 
     */
    private int verticalTemporalMedianFilter(int[][] cur, int[][] prev, int row, int col)
    {
        int A = cur[row - 1][col];
        int B = cur[row + 1][col];
        int C = prev[row][col];
        return this.median(A, B, C);
    }

    private int median(int a, int b, int c)
    {
        if (a <= b) {
            if (b <= c) {
                return b;
            } else if (a >= c) {
                return a;
            } else {
                return c;
            }
        } else {
            if (c >= a) {
                return a;
            } else if (c >= b) {
                return c;
            } else {
                return b;
            }
        }
    }

    private boolean[][] detectMotion(boolean topField, int[][] cur, int[][] prev2, int[][] prev, int[][] next)
    {
        boolean[][] motionInformation = new boolean[cur.length][cur[0].length];

        if (topField) {
            for (int row = 1; row < cur.length - 1; row += 2) {
                for (int col = 0; col < cur[0].length; col++) {
                    int d1 = Math.abs(cur[row - 1][col] - prev2[row - 1][col]);
                    int d2 = Math.abs(cur[row + 1][col] - prev2[row + 1][col]);
                    int d3 = Math.abs(next[row][col] - prev[row][col]);

                    int D = Math.max(Math.max(d1, d2), d3);
                    if (D < MOTION_THRESHOLD) {
                        motionInformation[row][col] = false;
                    } else {
                        motionInformation[row][col] = true;
                    }
                }
            }
        } else {
            for (int row = 2; row < cur.length - 1; row += 2) {
                for (int col = 0; col < cur[0].length; col++) {
                    int d1 = Math.abs(cur[row - 1][col] - prev2[row - 1][col]);
                    int d2 = Math.abs(cur[row + 1][col] - prev2[row + 1][col]);
                    int d3 = Math.abs(next[row][col] - prev[row][col]);

                    int D = Math.max(Math.max(d1, d2), d3);
                    if (D < MOTION_THRESHOLD) {
                        motionInformation[row][col] = false;
                    } else {
                        motionInformation[row][col] = true;
                    }
                }
            }
        }
        return motionInformation;
    }

    private boolean[][] detectTexture(boolean topField, int[][] cur)
    {
        boolean[][] textureInformation = new boolean[cur.length][cur[0].length];

        if (topField) {
            for (int row = 1; row < cur.length - 1; row += 2) {
                for (int col = 1; col < cur[0].length - 1; col++) {
                    double var = this.variance(cur, row, col);
                    if (var > TEXTURE_THRESHOLD) {
                        textureInformation[row][col] = true;
                    } else {
                        textureInformation[row][col] = false;
                    }
                }
            }
        } else {
            for (int row = 2; row < cur.length - 1; row += 2) {
                for (int col = 1; col < cur[0].length - 1; col++) {
                    double var = this.variance(cur, row, col);
                    if (var > TEXTURE_THRESHOLD) {
                        textureInformation[row][col] = true;
                    } else {
                        textureInformation[row][col] = false;
                    }
                }
            }
        }
        return textureInformation;
    }

    private double variance(int[][] cur, int row, int col)
    {
        double avg = (cur[row - 1][col - 1] + cur[row - 1][col] + cur[row - 1][col + 1]
                + cur[row + 1][col - 1] + cur[row + 1][col] + cur[row + 1][col + 1]) / 6;

        double var = ((cur[row - 1][col - 1] - avg) * (cur[row - 1][col - 1] - avg))
                + ((cur[row - 1][col] - avg) * (cur[row - 1][col] - avg))
                + ((cur[row - 1][col + 1] - avg) * (cur[row - 1][col + 1] - avg))
                + ((cur[row + 1][col - 1] - avg) * (cur[row + 1][col - 1] - avg))
                + ((cur[row + 1][col] - avg) * (cur[row + 1][col] - avg))
                + ((cur[row + 1][col + 1] - avg) * (cur[row + 1][col + 1] - avg));
        return var;
    }
}
