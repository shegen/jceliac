/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package deinterlace;

import data.LPFrame;
import java.util.Arrays;
import jceliac.gui.ImageViewerFrame;
import tools.ArrayTools;

/**
 * Implements the hybrid deinterlacer from the article: "A Motion-Adaptive Deinterlacer
 * via Hybrid Motion Detection and Edge-Pattern Recognition. "
 * @author shegen
 */
public class HMDERPDeinterlacer extends DeinterlacingFilter
{

    private final double TH1 = 8.0d;
    private final double TH2 = 20.0d;
    private final double TH3 = 16.0d;

    public HMDERPDeinterlacer()
    {
    }

    @Override
    public int getRequiredNumberOfFutureFrames()
    {
        return 1;
    }

    @Override
    public int getRequiredNumberOfPastFrames()
    {
        return 1;
    }

    private boolean[][] computeCombinedMotionMap(boolean topField, int[][] pastData, int[][] curData, int[][] futureData)
    {
        double[][] mapC1 = this.computeMotionMapCondition1(topField, pastData, curData, futureData);
        double[][] mapC2 = this.computeMotionMapCondition2(topField, pastData, curData, futureData);
        double[][] mapC3 = this.computeMotionMapCondition3(topField, pastData, curData, futureData);
        double[][] mapC4 = this.computeMotionMapCondition4(topField, pastData, curData, futureData);

        mapC1 = this.performErotionOnMap(mapC1);
        mapC1 = this.performDilationOnMap(mapC1);

        mapC2 = this.performErotionOnMap(mapC2);
        mapC2 = this.performDilationOnMap(mapC2);

        mapC3 = this.performErotionOnMap(mapC3);
        mapC3 = this.performDilationOnMap(mapC3);

        mapC4 = this.performErotionOnMap(mapC4);
        mapC4 = this.performDilationOnMap(mapC4);
        boolean[][] combinedMotionMap = this.combineMotionMaps(mapC1, mapC2, mapC3, mapC4);
        return combinedMotionMap;
    }

    @Override
    public LPFrame performDeinterlace(boolean topField) throws Exception
    {
        // expect row first mode
        if (this.currentFrame.isColumnFirstStored() == true) {
            throw new Exception("Frames not in row first mode!");
        }


        boolean[][] combinedMotionMapGreen = this.computeCombinedMotionMap(topField,
                this.pastFrames.get(0).getGreenData(),
                this.currentFrame.getGreenData(),
                this.futureFrames.get(0).getGreenData());

        boolean[][] combinedMotionMapRed = this.computeCombinedMotionMap(topField,
                this.pastFrames.get(0).getRedData(),
                this.currentFrame.getRedData(),
                this.futureFrames.get(0).getRedData());

        boolean[][] combinedMotionMapBlue = this.computeCombinedMotionMap(topField,
                this.pastFrames.get(0).getBlueData(),
                this.currentFrame.getBlueData(),
                this.futureFrames.get(0).getBlueData());

        boolean[][] combinedMotionMapGray = this.computeCombinedMotionMap(topField,
                this.pastFrames.get(0).getGrayData(),
                this.currentFrame.getGrayData(),
                this.futureFrames.get(0).getGrayData());

        int[][] redData = this.deinterlace(topField, this.currentFrame.getRedData(), this.pastFrames.get(0).getRedData(),
                this.futureFrames.get(0).getRedData(), combinedMotionMapRed);
        int[][] greenData = this.deinterlace(topField, this.currentFrame.getGreenData(), this.pastFrames.get(0).getGreenData(),
                this.futureFrames.get(0).getGreenData(), combinedMotionMapGreen);
        int[][] blueData = this.deinterlace(topField, this.currentFrame.getBlueData(), this.pastFrames.get(0).getBlueData(),
                this.futureFrames.get(0).getBlueData(), combinedMotionMapBlue);
        int[][] grayData = this.deinterlace(topField, this.currentFrame.getGrayData(), this.pastFrames.get(0).getGrayData(),
                this.futureFrames.get(0).getGrayData(), combinedMotionMapGray);



        LPFrame deinterlacedFrame = new LPFrame();
        deinterlacedFrame.setColumnFirstStored(false);
        deinterlacedFrame.setRedData(redData);
        deinterlacedFrame.setGreenData(greenData);
        deinterlacedFrame.setBlueData(blueData);
        deinterlacedFrame.setGrayData(grayData);

        return deinterlacedFrame;
    }

    private int[][] visualizeCombinedMotionMap(boolean[][] motionMap)
    {
        int[][] data = new int[motionMap.length][motionMap[0].length];
        for (int i = 0; i < motionMap.length; i++) {
            for (int j = 0; j < motionMap[0].length; j++) {
                data[i][j] = motionMap[i][j] ? 255 : 0;
            }
        }
        return data;
    }

    private int[][] deinterlace(boolean topField, int[][] curData, int[][] prevData, int[][] nextData, boolean[][] combinedMotionMap)
    {
        int[][] deinterlacedData = new int[curData.length][curData[0].length];

        // copy all good data to frame
        if (topField == true) { // copy all even lines
            for (int i = 0; i < curData.length; i += 2) {
                System.arraycopy(curData[i], 0, deinterlacedData[i], 0, curData[i].length);
            }
        } else { // copy all odd lines
            for (int i = 1; i < curData.length; i += 2) {
                System.arraycopy(curData[i], 0, deinterlacedData[i], 0, curData[i].length);
            }
        }

        if (topField == true) { // the first line (0-th) is good, so all even lines are good, deinterlace the odd lines
            for (int col = 1; col < curData[0].length - 1; col++) {
                for (int row = 3; row < curData.length - 2; row += 2) { // cannot use the two lines at each border 

                    if (combinedMotionMap[(row - 1) >> 1][col] == false) // stationary pixel use field insertion
                    {
                        deinterlacedData[row][col] = prevData[row][col];
                    } else { // motion pixel use ERP
                        EdgePattern pattern = this.getEdgePatternType(curData, prevData, combinedMotionMap, col, row);
                        double tmp = pattern.interpolate();
                        deinterlacedData[row][col] = (int) Math.round(tmp);
                    }

                }
            }
        } else { // second line (1-st) is good, so the odd lines are all good
            for (int col = 1; col < curData[0].length - 1; col++) {
                for (int row = 2; row < curData.length - 2; row += 2) { // cannot use the two lines at each border
                    if (combinedMotionMap[row >> 1][col] == false) // stationary pixel use field insertion
                    {
                        deinterlacedData[row][col] = prevData[row][col];
                    } else {
                        // the vertical resolution of the motion map is half, because each second line is missing due to interlacing
                        EdgePattern pattern = this.getEdgePatternType(curData, prevData, combinedMotionMap, col, row);
                        double tmp = pattern.interpolate();
                        deinterlacedData[row][col] = (int) Math.round(tmp);
                    }
                }
            }
        }
        return deinterlacedData;
    }

    private boolean[][] combineMotionMaps(double[][] c1, double[][] c2, double[][] c3, double[][] c4)
    {
        boolean[][] combinedMotionMap = new boolean[c1.length][c1[0].length];

        for (int i = 0; i < combinedMotionMap.length; i++) {
            for (int j = 0; j < combinedMotionMap[0].length; j++) {
                boolean motion;
                if (c1[i][j] > TH1) {
                    motion = true;
                } else if (c2[i][j] > TH1 && c3[i][j] < TH2) {
                    motion = true;
                } else if (c4[i][j] > TH3) {
                    motion = true;
                } else {
                    motion = false;
                }
                combinedMotionMap[i][j] = motion;
            }
        }
        return combinedMotionMap;
    }

    // the motionMaps have half vertical resolution an odd row is (row-1) >> 1 in the motionMap
    // and an even row is row >> 1 in the motionMap
    private EdgePattern getEdgePatternType(int[][] data, int[][] previousData, boolean[][] motionMap, int col, int row)
    {
        double p, a, q, b, c, r, d, s;

        p = data[row - 1][col - 1];
        a = data[row - 1][col];
        q = data[row - 1][col + 1];

        r = data[row + 1][col - 1];
        d = data[row + 1][col];
        s = data[row + 1][col + 1];

        // b and c are on the same line as X (i,j) so we cannot use the data directly
        // this data is adaptively obtained:
        //      b is obtained from the previous field if it is stationary and
        //      from the avarage of p and r if it is moving
        //
        //      the same thing holds for c - calculting as average from q and s
        //      if the pixel is moving in the previous field
        // 
        int rowMotionMapIndex = 0;
        if (row % 2 == 0) { // even
            rowMotionMapIndex = row >> 1;
        } else {
            rowMotionMapIndex = (row - 1) >> 1;
        }
        if (motionMap[rowMotionMapIndex][col - 1]) {  // true = motion
            b = (p + r) / 2.0d;
            c = (q + s) / 2.0d;
        } else {
            b = previousData[row][col - 1];
            c = previousData[row][col + 1];
        }

        // find out what edge pattern type is found, the paper defines 14 patterns
        // setting a pixel to value H if the pixel value is > than the average of a,b,c,d
        // else setting it to L
        double avg = (a + b + d + c) / 4.0d;
        return new EdgePattern(a, c, d, b, avg, q, p, r, s);
    }

    private double[][] performErotionOnMap(double[][] mapData)
    {
        double[][] outData = new double[mapData.length][mapData[0].length];

        for (int col = 1; col < mapData[0].length - 1; col++) {
            for (int row = 1; row < mapData.length - 1; row++) {
                outData[row][col] = this.erode(mapData, col, row);
            }
        }
        return outData;
    }

    private double[][] performDilationOnMap(double[][] mapData)
    {
        double[][] outData = new double[mapData.length][mapData[0].length];

        for (int col = 1; col < mapData[0].length - 1; col++) {
            for (int row = 1; row < mapData.length - 1; row++) {
                outData[row][col] = this.dilate(mapData, col, row);
            }
        }
        return outData;
    }

    private double erode(double[][] data, int col, int row)
    {
        double a, b, c, d, x;

        a = data[row][col - 1];
        c = data[row + 1][col];
        d = data[row][col + 1];
        b = data[row - 1][col];
        x = data[row][col];
        return Math.min(x, Math.min(Math.min(a, b), Math.min(c, d)));
    }

    private double dilate(double[][] data, int col, int row)
    {
        double[] points = new double[9];

        points[0] = data[row - 1][col - 1];
        points[1] = data[row][col - 1];
        points[2] = data[row + 1][col - 1];
        points[3] = data[row + 1][col];
        points[4] = data[row + 1][col + 1];
        points[5] = data[row][col + 1];
        points[6] = data[row - 1][col + 1];
        points[7] = data[row - 1][col];
        points[8] = data[row][col];
        return points[8]; // get maximum
    }

    private double[][] computeMotionMapCondition1(boolean topField, int[][] dataPrev, int[][] dataCur,
            int[][] dataNext)
    {

        double[][] motionMap = new double[dataPrev.length / 2][dataPrev[0].length];

        if (topField == true) { // the first line (0-th) is good, so all even lines are good, deinterlace the odd lines
            for (int col = 1; col < dataPrev[0].length - 1; col++) {
                for (int row = 3; row < dataPrev.length - 2; row += 2) { // cannot use the two lines at each border 
                    // the vertical resolution of the motion map is half, because each second line is missing due to interlacing
                    motionMap[(row - 1) >> 1][col] = this.condition1(dataPrev, dataCur, dataNext, col, row);

                }
            }
        } else { // second line (1-st) is good, so the odd lines are all good
            for (int col = 1; col < dataPrev[0].length - 1; col++) {
                for (int row = 2; row < dataPrev.length - 2; row += 2) { // cannot use the two lines at each border
                    // the vertical resolution of the motion map is half, because each second line is missing due to interlacing
                    motionMap[row >> 1][col] = this.condition1(dataPrev, dataCur, dataNext, col, row);
                }
            }
        }
        return motionMap;
    }

    private double[][] computeMotionMapCondition2(boolean topField, int[][] dataPrev, int[][] dataCur,
            int[][] dataNext)
    {

        double[][] motionMap = new double[dataPrev.length / 2][dataPrev[0].length];

        if (topField == true) { // the first line (0-th) is good, so all even lines are good
            for (int col = 1; col < dataPrev[0].length - 1; col++) {
                for (int row = 3; row < dataPrev.length - 2; row += 2) { // cannot use the two lines at each border 
                    // the vertical resolution of the motion map is half, because each second line is missing due to interlacing
                    motionMap[(row - 1) >> 1][col] = this.condition2(dataPrev, dataCur, dataNext, col, row);
                }
            }
        } else { // second line (1-st) is good, so the odd lines are all good
            for (int col = 1; col < dataPrev[0].length - 1; col++) {
                for (int row = 2; row < dataPrev.length - 2; row += 2) { // cannot use the two lines at each border
                    // the vertical resolution of the motion map is half, because each second line is missing due to interlacing
                    motionMap[row >> 1][col] = this.condition2(dataPrev, dataCur, dataNext, col, row);
                }
            }
        }
        return motionMap;
    }

    private double[][] computeMotionMapCondition3(boolean topField, int[][] dataPrev, int[][] dataCur,
            int[][] dataNext)
    {

        double[][] motionMap = new double[dataPrev.length / 2][dataPrev[0].length];

        if (topField == true) { // the first line (0-th) is good, so all even lines are good
            for (int col = 1; col < dataPrev[0].length - 1; col++) {
                for (int row = 3; row < dataPrev.length - 2; row += 2) { // cannot use the two lines at each border 
                    // the vertical resolution of the motion map is half, because each second line is missing due to interlacing
                    motionMap[(row - 1) >> 1][col] = this.condition3(dataPrev, dataCur, dataNext, col, row);
                }
            }
        } else { // second line (1-st) is good, so the odd lines are all good
            for (int col = 1; col < dataPrev[0].length - 1; col++) {
                for (int row = 2; row < dataPrev.length - 2; row += 2) { // cannot use the two lines at each border
                    // the vertical resolution of the motion map is half, because each second line is missing due to interlacing
                    motionMap[row >> 1][col] = this.condition3(dataPrev, dataCur, dataNext, col, row);
                }
            }
        }
        return motionMap;
    }

    private double[][] computeMotionMapCondition4(boolean topField, int[][] dataPrev, int[][] dataCur,
            int[][] dataNext)
    {

        double[][] motionMap = new double[dataPrev.length / 2][dataPrev[0].length];

        if (topField == true) { // the first line (0-th) is good, so all even lines are good
            for (int col = 1; col < dataPrev[0].length - 1; col++) {
                for (int row = 3; row < dataPrev.length - 2; row += 2) { // cannot use the two lines at each border 
                    // the vertical resolution of the motion map is half, because each second line is missing due to interlacing
                    motionMap[(row - 1) >> 1][col] = this.condition4(dataPrev, dataCur, dataNext, col, row);
                }
            }
        } else { // second line (1-st) is good, so the odd lines are all good
            for (int col = 1; col < dataPrev[0].length - 1; col++) {
                for (int row = 2; row < dataPrev.length - 2; row += 2) { // cannot use the two lines at each border
                    // the vertical resolution of the motion map is half, because each second line is missing due to interlacing
                    motionMap[row >> 1][col] = this.condition4(dataPrev, dataCur, dataNext, col, row);
                }
            }
        }
        return motionMap;
    }

    private double condition1(int[][] dataPrev, int[][] dataCur, int[][] dataNext, int col, int row)
    {
        int a, b;

        b = dataPrev[row][col];
        a = dataNext[row][col];

        double diff1 = Math.abs(a - b);
        return diff1;

    }

    private double condition2(int[][] dataPrev, int[][] dataCur, int[][] dataNext, int col, int row)
    {
        int b, c, d;

        b = dataPrev[row][col];
        c = dataCur[row - 1][col];
        d = dataCur[row + 1][col];

        double diff2 = Math.abs(b - ((c + d) / 2.0d));
        return diff2;
    }

    private double condition3(int[][] dataPrev, int[][] dataCur, int[][] dataNext, int col, int row)
    {
        int b, g, h;

        g = dataPrev[row - 2][col];
        b = dataPrev[row][col];
        h = dataPrev[row + 2][col];

        double diff3 = Math.abs(b - ((g + h) / 2.0d));
        return diff3;
    }

    private double condition4(int[][] dataPrev, int[][] dataCur, int[][] dataNext, int col, int row)
    {
        int a, b, e, f, g, h;

        g = dataPrev[row - 2][col];
        b = dataPrev[row][col];
        h = dataPrev[row + 2][col];

        e = dataNext[row - 2][col];
        a = dataNext[row][col];
        f = dataNext[row + 2][col];

        double diff4 = Math.abs(a + ((e + f) / 2.0d) - b - ((g + h) / 2.0d));
        return diff4;
    }

    class EdgePattern
    {

        boolean upper; // true = H, false = L
        boolean right;
        boolean lower;
        boolean left;
        double upperData, rightData, leftData, lowerData;
        double q, p, r, s;

        public EdgePattern(double upperData, double rightData, double lowerData, double leftData, double avg,
                double q, double p, double r, double s)
        {
            this.upperData = upperData;
            this.rightData = rightData;
            this.lowerData = lowerData;
            this.leftData = leftData;

            this.q = q;
            this.p = p;
            this.r = r;
            this.s = s;

            if (upperData > avg) {
                upper = true;
            } else {
                upper = false;
            }
            if (leftData > avg) {
                left = true;
            } else {
                left = false;
            }
            if (rightData > avg) {
                right = true;
            } else {
                right = false;
            }
            if (lowerData > avg) {
                lower = true;
            } else {
                lower = false;
            }
        }

        public int getPatternType()
                throws Exception
        {

            // 3H1L Pattern Types:
            //   H
            // H X H
            //   L
            if (upper == true && left == true && right == true && lower == false) {
                return 1;
            }

            //   H
            // L X H
            //   H
            if (upper == true && left == false && right == true && lower == true) {
                return 2;
            }

            //   L
            // H X H
            //   H
            if (upper == false && left == true && right == true && lower == true) {
                return 3;
            }

            //   H
            // H X L
            //   H
            if (upper == true && left == true && right == false && lower == true) {
                return 4;
            }

            // 3L1H Pattern Types:
            //   H
            // L X L
            //   L
            if (upper == true && left == false && right == false && lower == false) {
                return 5;
            }

            //   L
            // L X H
            //   L
            if (upper == false && left == false && right == true && lower == false) {
                return 6;
            }

            //   L
            // L X L
            //   H
            if (upper == false && left == false && right == false && lower == true) {
                return 7;
            }

            //   L
            // H X L
            //   L
            if (upper == false && left == true && right == false && lower == false) {
                return 8;
            }

            // 2H2L Corner Types:
            //   H
            // H X L
            //   L
            if (upper == true && left == true && right == false && lower == false) {
                return 9;
            }

            //   H
            // L X H
            //   L
            if (upper == true && left == false && right == true && lower == false) {
                return 10;
            }

            //   L
            // L X H
            //   H
            if (upper == false && left == false && right == true && lower == true) {
                return 11;
            }

            //   L
            // H X L
            //   H
            if (upper == false && left == true && right == false && lower == true) {
                return 12;
            }

            // 2H2L Stripe Pattern Types:

            //   H
            // L X L
            //   H
            if (upper == true && left == false && right == false && lower == true) {
                return 13;
            }

            //   L
            // H X H
            //   L
            if (upper == false && left == true && right == true && lower == false) {
                return 14;
            }

            // this type is not defined in the paper, but can happen if all pixels 
            // have the same value
            if (upper == false && left == false && right == false && lower == false) {
                return 15;
            }

            throw new Exception("Unexpected Pattern");
        }

        public double interpolate()
        {
            try {
                switch (this.getPatternType()) {
                    // 3H1L Type
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        return perform3H1LInterpolation();

                    // 3L1H Type
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                        return perform3L1HInterpolation();

                    // 2H2L Corner Type
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                        return perform2H2LCornerInterpolation();

                    // 2H2L Stripe Type
                    case 13:
                    case 14:
                        return perform2H2LStripeInterpolation();

                    // all data have the same value
                    case 15:
                        return this.upperData;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 0;
        }

        public double perform3H1LInterpolation()
        {
            double[] values = new double[3];
            int index = 0;
            if (this.upper == true) {
                values[index++] = this.upperData;
            }
            if (this.left == true) {
                values[index++] = this.leftData;
            }
            if (this.right == true) {
                values[index++] = this.rightData;
            }
            if (this.lower == true) {
                values[index++] = this.lowerData;
            }
            // find median
            Arrays.sort(values);
            return values[1];
        }

        public double perform3L1HInterpolation()
        {
            double[] values = new double[3];
            int index = 0;
            if (this.upper == false) {
                values[index++] = this.upperData;
            }
            if (this.left == false) {
                values[index++] = this.leftData;
            }
            if (this.right == false) {
                values[index++] = this.rightData;
            }
            if (this.lower == false) {
                values[index++] = this.lowerData;
            }
            // find median
            Arrays.sort(values);
            return values[1];
        }

        public double perform2H2LCornerInterpolation()
        {
            double[] H = new double[2];
            double[] L = new double[2];

            int hind = 0;
            int lind = 0;

            if (this.upper == true) {
                H[hind++] = this.upperData;
            } else {
                L[lind++] = this.lowerData;
            }
            if (this.left == true) {
                H[hind++] = this.leftData;
            } else {
                L[lind++] = this.leftData;
            }
            if (this.right == true) {
                H[hind++] = this.rightData;
            } else {
                L[lind++] = this.rightData;
            }
            if (this.lower == true) {
                H[hind++] = this.lowerData;
            } else {
                L[lind++] = this.lowerData;
            }

            if (Math.abs(p - q) > Math.abs(r - s)) {
                return Math.min(H[0], H[1]);
            } else {
                return Math.max(L[0], L[1]);
            }
        }

        public double perform2H2LStripeInterpolation()
        {
            double[] H = new double[2];
            double[] L = new double[2];

            int hind = 0;
            int lind = 0;

            if (this.upper == true) {
                H[hind++] = this.upperData;
            } else {
                L[lind++] = this.lowerData;
            }
            if (this.left == true) {
                H[hind++] = this.leftData;
            } else {
                L[lind++] = this.leftData;
            }
            if (this.right == true) {
                H[hind++] = this.rightData;
            } else {
                L[lind++] = this.rightData;
            }
            if (this.lower == true) {
                H[hind++] = this.lowerData;
            } else {
                L[lind++] = this.lowerData;
            }

            if ((Math.abs(p - q) + Math.abs(r - s)) > (Math.abs(p - r) + Math.abs(q - s))) {
                return Math.min(H[0], H[1]);
            } else {
                return Math.max(L[0], L[1]);
            }
        }
    }
}
