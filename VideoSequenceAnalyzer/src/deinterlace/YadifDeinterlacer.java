/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package deinterlace;

import data.LPFrame;
import deinterlace.DeinterlacingFilter;
import jceliac.tools.data.Frame;

/**
 *
 * @author shegen
 */
public class YadifDeinterlacer extends DeinterlacingFilter
{

    private YadifMode mode = YadifMode.SpatialOnly;
    
    // modes are not totally clear; TemporalAndSpatial mode
    // uses also a diff of the temporal information to find
    // the minimum change of the pixel to be interpolated
    public static enum YadifMode  {
        TemporalAndSpatial, // mode 0 or 1
        SpatialOnly, // mode 2 ?
    };
    
    public YadifDeinterlacer(YadifMode mode)
    {
        this.mode = mode;
    }

    public YadifDeinterlacer()
    {
    }
    
    @Override
    public int getRequiredNumberOfFutureFrames()
    {
        return 1;
    }

    @Override
    public int getRequiredNumberOfPastFrames()
    {
        return 1;
    }

    @Override
    public LPFrame performDeinterlace(boolean topField)
            throws Exception
    {
        if(this.currentFrame.isColumnFirstStored()) {
            throw new Exception("Due to performance issues YADIF needs the frames to be stored in"
                    + "row first mode!");
        }
        this.prepareAllFramesTopFieldFirst();
        
        LPFrame deinterlacedFrame = new LPFrame();
        deinterlacedFrame.setColumnFirstStored(false);
        
        int [][] grayDataDeinterlaced = new int[this.currentFrame.getHeight()][this.currentFrame.getWidth()];
        int [][] greenDataDeinterlaced = new int[this.currentFrame.getHeight()][this.currentFrame.getWidth()];
        int [][] redDataDeinterlaced = new int[this.currentFrame.getHeight()][this.currentFrame.getWidth()];
        int [][] blueDataDeinterlaced = new int[this.currentFrame.getHeight()][this.currentFrame.getWidth()];
        
        int [][] pastGrayData = this.pastFrames.get(0).getGrayData();
        int [][] pastGreenData = this.pastFrames.get(0).getGreenData();
        int [][] pastRedData = this.pastFrames.get(0).getRedData();
        int [][] pastBlueData = this.pastFrames.get(0).getBlueData();
        
        int [][] currentGrayData = this.currentFrame.getGrayData();
        int [][] currentGreenData = this.currentFrame.getGreenData();
        int [][] currentRedData = this.currentFrame.getRedData();
        int [][] currentBlueData = this.currentFrame.getBlueData();
        
        int [][] futureGrayData = this.futureFrames.get(0).getGrayData();
        int [][] futureGreenData = this.futureFrames.get(0).getGreenData();
        int [][] futureRedData = this.futureFrames.get(0).getRedData();
        int [][] futureBlueData = this.futureFrames.get(0).getBlueData();
        
        for(int i = 0; i < this.currentFrame.getHeight(); i++) {
            for(int j = 0; j < this.currentFrame.getWidth(); j++) { 
                if(i % 2 == 0) 
                { // do not deinterlace a good line
                    grayDataDeinterlaced[i][j]  =  currentGrayData[i][j];
                    redDataDeinterlaced[i][j]   =  currentRedData[i][j];
                    greenDataDeinterlaced[i][j] =  currentGreenData[i][j];
                    blueDataDeinterlaced[i][j]  =  currentBlueData[i][j];
                    continue;
                }
                grayDataDeinterlaced[i][j] =  this.predictPixel(pastGrayData,
                                                                currentGrayData, 
                                                                futureGrayData, i, j);
                
                greenDataDeinterlaced[i][j] =  this.predictPixel(pastGreenData, 
                                                                 currentGreenData, 
                                                                 futureGreenData, i, j);
                
                redDataDeinterlaced[i][j] =  this.predictPixel(pastRedData, 
                                                               currentRedData, 
                                                               futureRedData, i, j);
                
                blueDataDeinterlaced[i][j] =  this.predictPixel(pastBlueData, 
                                                                currentBlueData, 
                                                                futureBlueData, i, j);
            }
        }
        deinterlacedFrame.setGrayData(grayDataDeinterlaced);
        deinterlacedFrame.setGreenData(greenDataDeinterlaced);
        deinterlacedFrame.setBlueData(blueDataDeinterlaced);
        deinterlacedFrame.setRedData(redDataDeinterlaced);
        return deinterlacedFrame;
    }
    
    
    private void prepareAllFramesTopFieldFirst() 
    {
        // prepare the frames
        for(LPFrame tmp : this.pastFrames) 
        {   
            tmp.setGrayData(prepareTopFieldFirst(tmp.getGrayData()));
            tmp.setGreenData(prepareTopFieldFirst(tmp.getGreenData()));
            tmp.setRedData(prepareTopFieldFirst(tmp.getRedData()));
            tmp.setBlueData(prepareTopFieldFirst(tmp.getBlueData()));
       }
        for(LPFrame tmp : this.futureFrames) {
            tmp.setGrayData(prepareTopFieldFirst(tmp.getGrayData()));
            tmp.setGreenData(prepareTopFieldFirst(tmp.getGreenData()));
            tmp.setRedData(prepareTopFieldFirst(tmp.getRedData()));
            tmp.setBlueData(prepareTopFieldFirst(tmp.getBlueData()));
        }
        LPFrame tmp = this.currentFrame;
        tmp.setGrayData(prepareTopFieldFirst(tmp.getGrayData()));
        tmp.setGreenData(prepareTopFieldFirst(tmp.getGreenData()));
        tmp.setRedData(prepareTopFieldFirst(tmp.getRedData()));
        tmp.setBlueData(prepareTopFieldFirst(tmp.getBlueData()));
    }
    
    // go through the even lines and make it match the corresponding uneven lines
    // we demand the Frames to be stored in row first mode so data[n] gives the n-th row
    private int [][] prepareTopFieldFirst(int [][] data) 
    {
        int [][] preparedData = new int[data.length][data[0].length];
        
        // copy the even lines and fill the odd lines, this is odd field first
        // however, we use 0 indexing as start so we use the even lines
        for(int i = 0 ; i < data.length; i+=2) {
            System.arraycopy(data[i], 0, preparedData[i], 0, data[i].length);
            System.arraycopy(data[i], 0, preparedData[i+1], 0, data[i].length);
        }
        return preparedData;
    }
    
    private int predictPixel(int [][] previous, int [][] current, int [][] next, int i, int j)
    {
        
        // At this point we do not deinterlace border pixels, this is because we 
        // do not really need it; beware however if you use this code for some
        // different purpose
        if(i < 3 || j < 3 || i >= current.length-3 || j >= current[0].length-3) {
            return current[i][j];
        }
        
        // do the prediction
        int b = (int)((previous[i-2][j] + current[i-2][j]) >> 1);
        int c = (int)(current[i-1][j]);
        int d = (int)((previous[i][j] + current[i][j]) >> 1);
        int e = (int)(current[i+1][j]);
        int f = (int)((previous[i+2][j] + current[i+2][j]) >> 1);
        
        int spatialPrediction = this.predictSpatial(current, c, e, i, j);
        int temporalDifference = this.temporalDifferences(previous, current, next, i, j);
        
        if(spatialPrediction > d + temporalDifference) {
            spatialPrediction = (int)(d + temporalDifference);
        }else if(spatialPrediction < d - temporalDifference) {
            spatialPrediction = (int)(d - temporalDifference);
        }
        return spatialPrediction;
    }
    
    private int predictSpatial(int [][] current, int c, int e, int i, int j)
    {
        int spatialPrediction = (int)(0.5d * (c+e));
         
        // compute the spatial score which is the sum of differences 
        // of the pixels left of c and e; These are all informative pixels of
        // the current frame.
        int spatialScore = (int)((Math.abs(current[i-1][j-1] - current[i+1][j-1]) + 
                              Math.abs(current[i-1][j+1] - current[i+1][j+1]) +
                              Math.abs(c-e)) - 1);
        
       // now adjust the spatial predicition by checking from pper left to bottom right
       int spatialAdaptionScore = (int)(Math.abs(current[i-1][j-2] - current[i+1][j]) +
                                     Math.abs(current[i-1][j-1] - current[i+1][j+1]) + 
                                     Math.abs(current[i-1][j] - current[i+1][j+2]));
       // check if the adaption score is < than the initial spatial score
       if(spatialAdaptionScore < spatialScore) {
           spatialScore = spatialAdaptionScore;
           // update the spatial prediction to be a diagonal prediction
           spatialPrediction = (int)((current[i-1][j-1] + current[i+1][j+1]) >> 1);
           
           spatialAdaptionScore = (int)(Math.abs(current[i-1][j-3] - current[i+1][j+1]) +
                                  Math.abs(current[i-1][j-2] - current[i+1][j+2]) +
                                  Math.abs(current[i-1][j-1] - current[i+1][j+3]));
           if(spatialAdaptionScore < spatialScore) {
               spatialScore = spatialAdaptionScore;
               spatialPrediction = (int)((current[i-1][j-2] + current[i+1][j+2]) >> 1);
               
           }
       }
       spatialAdaptionScore = (int)(Math.abs(current[i-1][j] - current[i+1][j-2]) + 
                              Math.abs(current[i-1][j+1] - current[i+1][j-1])+
                              Math.abs(current[i-1][j+2] - current[i+1][j]));
       if(spatialAdaptionScore < spatialScore) {
           spatialScore = spatialAdaptionScore;
           spatialPrediction = (int)((current[i-1][j+1] + current[i+1][j-1]) >> 1);
           
           spatialAdaptionScore = (int)(Math.abs(current[i-1][j+1] - current[i+1][j-3])+
                                  Math.abs(current[i-1][j+2] - current[i+1][j-2]) + 
                                  Math.abs(current[i-1][j+3] - current[i+1][j-1]));
           if(spatialAdaptionScore < spatialScore) {
               spatialScore = spatialAdaptionScore;
               spatialPrediction = (int)((current[i-1][j+2] + current[i+1][j-2]) >> 1);
           }                       
       }
       return spatialPrediction;
    }
    
    // the ref frame is dependent on the current parity (either next or previous)
    private int temporalDifferences(int [][] previous, int [][] current, int [][] next, int i, int j) 
    {
        
        int c = (int)(current[i-1][j]);
        int d = (int)((previous[i][j] + current[i][j]) >> 1);
        int e = (int)(current[i+1][j]);
        int diff = 0;
        
        if(this.mode == YadifMode.TemporalAndSpatial) 
        {
             int b = (int)((previous[i-2][j] + current[i-2][j]) >> 1);
             int f = (int)((previous[i+2][j] + current[i+2][j]) >> 1);
             int max = this.max3((int)(d-e), (int)(d-c), (int)Math.min(b-c, f-e));
             int min = this.min3((int)(d-e), (int)(d-c), (int)Math.max(b-c, f-e));
             diff = this.max3(diff, min, (int)-max);
        }else {
            // temporal diffs
            int diff0 = (int)(Math.abs(previous[i][j] - next[i][j]) >> 1); 
            int diff1 = (int)((Math.abs(previous[i-1][j]-c) + Math.abs(previous[i+1][j] - e)) >> 1); // average change from previous to current
            int diff2 = (int)((Math.abs(next[i-1][j] - c) + Math.abs(next[i+1][j] - e)) >> 1); // average change from current to next
            diff = this.max3(diff0, diff1, diff2);
        }
        return diff;
    }
    
    private int max3(int a, int b, int c)
    {
        return (int)Math.max(Math.max(a, b), c);
    }
    
    private int min3(int a, int b, int c)
    {
        return (int)Math.min(Math.min(a, b), c);
    }
    
}
