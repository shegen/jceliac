/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package deinterlace;

import data.LPFrame;

/**
 * Implements a simply vertical temporal median deinterlacer. Such as described in 
 * Motion Adaptive Deinterlacing of Video Data with Texture Detection 
 * (use Reference 5 for citation).
 * @author shegen
 */
public class VTMedianDeinterlacer extends DeinterlacingFilter
{

    @Override
    public int getRequiredNumberOfFutureFrames()
    {
        return 0;
    }

    @Override
    public int getRequiredNumberOfPastFrames()
    {
        return 1;
    }

    // currently no border handling is implemented, we skip this because the frames are black anyways
    @Override
    public LPFrame performDeinterlace(boolean topField) throws Exception
    {
        
        if(this.currentFrame.isColumnFirstStored() || this.pastFrames.get(0).isColumnFirstStored()) {
            throw new Exception("Column First mode not supported!");
        }
        int [][] currentRedData = this.currentFrame.getRedData();
        int [][] currentGreenData = this.currentFrame.getGreenData();
        int [][] currentBlueData = this.currentFrame.getBlueData();
        int [][] currentGrayData = this.currentFrame.getGrayData();
        
        int [][] pastRedData = this.pastFrames.get(0).getRedData();
        int [][] pastGreenData = this.pastFrames.get(0).getGreenData();
        int [][] pastBlueData = this.pastFrames.get(0).getBlueData();
        int [][] pastGrayData = this.pastFrames.get(0).getGrayData();
        
        
        int height = currentRedData.length;
        int width = currentRedData[0].length;
        
        int [][] deinterlacedRedData = new int[height][width];
        int [][] deinterlacedGreenData = new int[height][width];
        int [][] deinterlacedBlueData = new int[height][width];
        int [][] deinterlacedGrayData = new int[height][width];

        
        // copy good lines
        if(topField) {
            // even lines are good
            for(int row = 0; row < height; row+=2) {
                    System.arraycopy(currentRedData[row], 0, deinterlacedRedData[row], 0, width);
                    System.arraycopy(currentGreenData[row], 0, deinterlacedGreenData[row], 0, width);
                    System.arraycopy(currentBlueData[row], 0, deinterlacedBlueData[row], 0, width);
                    System.arraycopy(currentGrayData[row], 0, deinterlacedGrayData[row], 0, width);
                
            }
        }else {
            // odd lines are good
            for(int row = 1; row < height; row+=2) {
                    System.arraycopy(currentRedData[row], 0, deinterlacedRedData[row], 0, width);
                    System.arraycopy(currentGreenData[row], 0, deinterlacedGreenData[row], 0, width);
                    System.arraycopy(currentBlueData[row], 0, deinterlacedBlueData[row], 0, width);
                    System.arraycopy(currentGrayData[row], 0, deinterlacedGrayData[row], 0, width);
                
            }
        }
        if(topField) { // odd lines have to be deinterlaced
            for(int row = 1; row < height-1; row+=2) {
                for(int col = 0; col < width; col++) {
                    int deinterlacedPixelRed = this.deinterlacePixelPosition(row, col, currentRedData, pastRedData);
                    int deinterlacedPixelGreen = this.deinterlacePixelPosition(row, col, currentGreenData, pastGreenData);
                    int deinterlacedPixelBlue = this.deinterlacePixelPosition(row, col, currentBlueData, pastBlueData);
                    int deinterlacedPixelGray = this.deinterlacePixelPosition(row, col, currentGrayData, pastGrayData);
                    
                    deinterlacedRedData[row][col] = deinterlacedPixelRed;
                    deinterlacedGreenData[row][col] = deinterlacedPixelGreen;
                    deinterlacedBlueData[row][col] = deinterlacedPixelBlue;
                    deinterlacedGrayData[row][col] = deinterlacedPixelGray;
                }
            }
        }else {
            for(int row = 2; row < height-1; row+=2) {
                for(int col = 0; col < width; col++) {
                    int deinterlacedPixelRed = this.deinterlacePixelPosition(row, col, currentRedData, pastRedData);
                    int deinterlacedPixelGreen = this.deinterlacePixelPosition(row, col, currentGreenData, pastGreenData);
                    int deinterlacedPixelBlue = this.deinterlacePixelPosition(row, col, currentBlueData, pastBlueData);
                    int deinterlacedPixelGray = this.deinterlacePixelPosition(row, col, currentGrayData, pastGrayData);
                    
                    deinterlacedRedData[row][col] = deinterlacedPixelRed;
                    deinterlacedGreenData[row][col] = deinterlacedPixelGreen;
                    deinterlacedBlueData[row][col] = deinterlacedPixelBlue;
                    deinterlacedGrayData[row][col] = deinterlacedPixelGray;
                }
            }
        }
        LPFrame deinterlacedFrame = new LPFrame();
        deinterlacedFrame.setColumnFirstStored(false);
        
        deinterlacedFrame.setRedData(deinterlacedRedData);
        deinterlacedFrame.setGreenData(deinterlacedGreenData);
        deinterlacedFrame.setBlueData(deinterlacedBlueData);
        deinterlacedFrame.setGrayData(deinterlacedGrayData);
        return deinterlacedFrame;
    }
    
    /* Caller must be sure that indices are valid. 
     * Method uses upper and lower row of current frame and the current position of the pixel 
     * in the previous frame. 
     */
    private int deinterlacePixelPosition(int row, int col, int [][] cur, int [][] prev)
    {
        int A = cur[row-1][col];
        int B = cur[row+1][col];
        int C = prev[row][col];
        return this.median(A, B, C);
    }
    
    private int median(int a, int b, int c) 
    {
        if(a <= b) {
            if(b <= c) return b;
            else if(a >= c) return a;
            else return c;
        }else {
            if(c >= a) return a;
            else if(c >= b) return c;
            else return b;
        }
    }
    
}
