/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package deinterlace.motionestimation;

/**
 *
 * @author shegen
 */
public class MotionVector
{
    private int iComponent;
    private int jComponent;
    private int iPosition;
    private int jPosition;
    
    private double reliability = -1;
    
    private double MAD = -1;
    
    private boolean sameParity;
    private int m; // gives the field number  -2 = prev2, -1 = prev, 1 = next, 2 = next2
    private double OMBHalfMVError;
    
    private MacroBlock macroBlock; // the macro block of this MotionVector
    
   public double normedDistance(MotionVector other) {
       int xOther = other.iComponent;
       int yOther = other.jComponent;
       
       return Math.sqrt( ((iComponent-xOther) * (iComponent-xOther)) + ((jComponent-yOther) * (jComponent-yOther)) );
   }

    public int getIComponent()
    {
        return iComponent;
    }

    public int getJComponent()
    {
        return jComponent;
    }

    public int getIPosition()
    {
        return iPosition;
    }

    public int getJPosition()
    {
        return jPosition;
    }

    public double getMAD()
    {
        return MAD;
    }

    public void setMAD(double MAD)
    {
        this.MAD = MAD;
    }
    
    

    public MotionVector(int iComponent, int jComponent, int iPosition, int jPosition)
    {
        this.iComponent = iComponent;
        this.jComponent = jComponent;
        this.iPosition = iPosition;
        this.jPosition = jPosition;
    }

    public double getReliability()
    {
        return reliability;
    }

    public void setReliability(double reliability)
    {
        this.reliability = reliability;
    }
    
    public double getLength()
    { 
        return Math.sqrt(this.iComponent*this.iComponent + this.jComponent*this.jComponent);
    }
   
    // identified by position of macro block, this will be unique for each image
    @Override
    public int hashCode()
    {
        double xd,yd;
        
        xd = (double)this.iPosition;
        yd = (double)this.jPosition;
        return (int)(0.5d*(xd+yd)*(xd+yd+1)+yd);
    }

    public boolean isSameParity()
    {
        return sameParity;
    }

    public void setSameParity(boolean sameParity)
    {
        this.sameParity = sameParity;
    }

    public int getM()
    {
        return m;
    }

    public void setM(int m)
    {
        this.m = m;
    }

    public double getOMBHalfMVError()
    {
        return OMBHalfMVError;
    }

    public void setOMBHalfMVError(double OMBHalfMVError)
    {
        this.OMBHalfMVError = OMBHalfMVError;
    }
    
    
        
}
