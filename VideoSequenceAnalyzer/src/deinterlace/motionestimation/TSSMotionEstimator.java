/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package deinterlace.motionestimation;

import data.LPFrame;
import java.util.ArrayList;
import java.util.HashMap;
import jceliac.gui.ImageViewerFrame;
import jceliac.tools.data.Frame;
import jceliac.tools.math.MathTools;


/**
 * This class implements the New-Three-Step-Search motion estimator. 
 * 
 * @author shegen
 */
public class TSSMotionEstimator extends MotionEstimator
{
    
    public static final int BLOCKSIZE = 15;
    public static final int S = 4;

    private LPFrame current, next;

    public TSSMotionEstimator(LPFrame current, LPFrame next)
    {
        this.current = current;
        this.next = next;
    }
    
    public HashMap <Integer,MotionVector> estimateMotionVectors(LPFrame reference, LPFrame next)
            throws Exception
    {
        if (reference.isColumnFirstStored() || next.isColumnFirstStored()) {
            throw new Exception("Column First Mode not supported!");
        }
        int[][] referenceGray = reference.getGrayData();
        int[][] nextGray = next.getGrayData();
        HashMap <Integer,MotionVector> motionVectors = new HashMap <Integer,MotionVector> ();
        
        for (int macroBlockI = (BLOCKSIZE / 2); macroBlockI < reference.getHeight() - (BLOCKSIZE / 2); macroBlockI += BLOCKSIZE) {
            for (int macroBlockJ = (BLOCKSIZE / 2); macroBlockJ < reference.getWidth() - (BLOCKSIZE / 2); macroBlockJ += BLOCKSIZE) {
               
                MotionVector m = this.estimateMotionVector(referenceGray, nextGray, macroBlockI, macroBlockJ);
                Integer key = MathTools.cantorTupel(macroBlockI, macroBlockJ);
                motionVectors.put(key, m);
            }
        }
        return motionVectors;
    }

    @Override
    public HashMap<Integer, MotionVector> computeMotionVectors()
            throws Exception
    {
        return this.estimateMotionVectors(this.current, this.next);
    }
    
    private MotionVector estimateMotionVector(int[][] reference, int[][] next, int centerI, int centerJ)
    {
        int[] coords;
        int minimumPosition;
        
        minimumPosition = performStep(reference, next, centerI, centerJ, centerI, centerJ, 4);
        coords = computeCoordinates(centerI, centerJ, minimumPosition, 4);
        minimumPosition = performStep(reference, next, centerI, centerJ, coords[0], coords[1], 2);
        coords = computeCoordinates(coords[0], coords[1], minimumPosition, 2);
        minimumPosition = performStep(reference, next, centerI, centerJ, coords[0], coords[1], 1);
        coords = computeCoordinates(coords[0], coords[1], minimumPosition, 1);
        
        double mad = this.computeMAD(reference, next, centerI, centerJ, coords[0], coords[1]);
        // compute motion vector as difference from center to coords
        MotionVector motionVector = new MotionVector(coords[0] - centerI, coords[1] - centerJ, centerI, centerJ);
        motionVector.setMAD(mad);
        return motionVector;
    }
    
    private int [] computeCoordinates(int i, int j, int minimumPosition, int step) 
    {
        switch(minimumPosition) 
        {
            case 0: return new int[] {i-step,j-step};
            case 1: return new int[] {i-step,j};
            case 2: return new int[] {i-step,j+step};
            case 3: return new int[] {i,j+step};
            case 4: return new int[] {i+step,j+step};
            case 5: return new int[] {i+step,j};
            case 6: return new int[] {i+step,j-step};
            case 7: return new int[] {i,j-step};   
            case 8: return new int[] {i,j};
        }
        return null; // unreached
    }
    
    
   
    // centerI is on the Y axis (first array dimension), centerJ on the X axis (second array dimension)
    private int performStep(int [][] reference, int [][] next, int refI, int refJ, int centerI, int centerJ, int stepsize)
    {
        double [] differences = new double[9];
        
        // initialize differences to a very big value
        for(int i = 0; i < differences.length; i++) {
            differences[i] = Double.MAX_VALUE;
        }
        
        // first step go through all 8 checking points
        // assuming row first mode
        // upper left checkpoint
        differences[8] = this.computeMAD(reference, next, refI, refJ, centerI, centerJ);
        if(differences[8] == 0) { // for two blank macro blocks no motion vector is needed
            return 8;
        }
        int bsh = BLOCKSIZE/2;
        int w = stepsize + bsh;
        // check position of macro blocks, if they are at the border we cannot search 
        // across that border 
        if((centerI - w) >= 0) { // can search above
            differences[1] = this.computeMAD(reference, next, refI, refJ, centerI-stepsize, centerJ);
            
            if((centerJ - w) >= 0) 
            { // can search to the left
                differences[0] = this.computeMAD(reference, next, refI, refJ,centerI-stepsize, centerJ-stepsize);
            }
            if((centerJ + w) < reference[0].length) 
            { // can search to the right
                differences[2] = this.computeMAD(reference, next, refI, refJ,centerI-stepsize, centerJ+stepsize);
            }
        }
        if((centerI + w) < reference.length) { // can search below
            differences[5] = this.computeMAD(reference, next, refI, refJ,centerI+stepsize, centerJ);
            if((centerJ - w) >= 0) 
            { // can search to the upper right
              differences[6] = this.computeMAD(reference, next, refI, refJ,centerI+stepsize, centerJ-stepsize);  
            }
            if((centerJ + w) < reference[0].length)
            { // can search to the lower right
              differences[4] = this.computeMAD(reference, next, refI, refJ,centerI+stepsize, centerJ+stepsize);  
            }
        }
        if((centerJ - w) >= 0) { // can search left
            differences[7] = this.computeMAD(reference, next, refI, refJ,centerI, centerJ-stepsize);
        }
        if((centerJ + w) < reference[0].length) 
        {   //can search right
            differences[3] = this.computeMAD(reference, next, refI, refJ,centerI, centerJ+stepsize);
        }  
        // finx minimum
        int minIndex = findMinimum(differences);
        return minIndex;
    }
    
    // mean absolute difference
    private double computeMAD(int [][] data1, int [][] data2, int centerI, int centerJ, int centerI2, int centerJ2)
    {
        double d = 0;
        for(int i = -(BLOCKSIZE/2); i < (BLOCKSIZE/2); i++) {
            for(int j = -(BLOCKSIZE/2); j < (BLOCKSIZE/2); j++) {
                short tmp = (short)(data1[centerI+i][centerJ+j] - data2[centerI2+i][centerJ2+j]);
                if(tmp < 0) {
                    d += (double)-tmp;
                }else {
                    d+= (double)tmp;
                }
            }
        }
        return d / (BLOCKSIZE*BLOCKSIZE);
    }
    
    private int findMinimum(double [] data) 
    {
        double min = Double.MAX_VALUE;
        int minIndex = -1;
        
        for(int i = 0; i < data.length; i++) {
            if(data[i] < min) {
                min = data[i];
                minIndex = i;
            }
        }
        return minIndex;
    } 
}
