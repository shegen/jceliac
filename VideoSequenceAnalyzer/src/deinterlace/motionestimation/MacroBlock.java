/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package deinterlace.motionestimation;

/**
 *
 * @author shegen
 */
public class MacroBlock
{
    public static final int BLOCKSIZE = 7;
    
    private int positionI;  // center position in dim 1
    private int positionJ;  // center position in dim 2
    
    private final int [][] imageDataBuffer; // all pixels from an image
    private MotionVector motionVector; // motion Vector for this MacroBlock
    
    public MacroBlock(int positionI, int positionJ, int [][] imageDataBuffer)
    {
        this.positionI = positionI;
        this.positionJ = positionJ;
        this.imageDataBuffer = imageDataBuffer; // remember the image data buffer
    }
    
    public double computeMAD(MacroBlock other) 
    {    
        final int [][] otherData = other.getImageDataBuffer();
        double d = 0;
        int otherPosI = other.getPositionI();
        int otherPosJ = other.getPositionJ();
        
        for(int i = -(BLOCKSIZE/2); i < (BLOCKSIZE/2); i++) {
            for(int j = -(BLOCKSIZE/2); j < (BLOCKSIZE/2); j++) {
                short tmp = (short)(this.imageDataBuffer[this.positionI+i][this.positionJ+j] - 
                             otherData[otherPosI+i][otherPosJ+j]);   
                if(tmp < 0) {
                    d += (double)-tmp;
                }else {
                    d+= (double)tmp;
                }
            }
        }
        return d;
    }

    public int[][] getImageDataBuffer()
    {
        return imageDataBuffer;
    }
    
    public int getPositionI()
    {
        return positionI;
    }

    public int getPositionJ()
    {
        return positionJ;
    }

    // identified by position of macro block, this will be unique for each image
    @Override
    public int hashCode()
    {
        double xd,yd;
        
        xd = (double)this.positionI;
        yd = (double)this.positionJ;
        return (int)(0.5d*(xd+yd)*(xd+yd+1)+yd);
    }
    
    
    
    
    
}
