/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package deinterlace.motionestimation;

import data.LPFrame;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import jceliac.tools.math.MathTools;

/**
 * Implements the motion estimation from "A threshold-based de-interlacing algorithm using
 * motion compensation and directional interpolation" and "A Five-Field Motion Compensated 
 * De-interlacing Method Based on Vertical Motion". 
 * 
 * 
 * 
 * 
 * @author shegen
 */
public class FiveFieldMotionEstimatorBugged extends MotionEstimator
{

    private final int SEARCHSIZE = 32;
    public static final int BLOCKSIZE = 15; // 8
    public static final double THRESHOLD = 3.0d;
    private LPFrame prev2, prev, cur, next, next2;
    private boolean topField;

    public FiveFieldMotionEstimatorBugged(boolean topField, LPFrame prev2, LPFrame prev, LPFrame cur, LPFrame next, LPFrame next2)
    {
        this.prev2 = prev2;
        this.prev = prev;
        this.cur = cur;
        this.next = next;
        this.next2 = next2;
        this.topField = topField;
    }

    @Override
    public HashMap<Integer, MotionVector> computeMotionVectors()
            throws Exception
    {
        return this.computeForwardAndBackwardMotionVectors();
    }

    
    
    
    
    private HashMap<Integer, MotionVector> computeForwardAndBackwardMotionVectors()
            throws Exception
    {
        if (this.cur.isColumnFirstStored() || this.prev.isColumnFirstStored() || this.prev2.isColumnFirstStored()) {
            throw new Exception("Column First Mode not supported!");
        }
        int[][] curGray = this.cur.getGrayData();
        int[][] prevGray = this.prev.getGrayData();
        int[][] prev2Gray = this.prev2.getGrayData();
        int[][] nextGray = this.next.getGrayData();
        int[][] next2Gray = this.next2.getGrayData();
        
        // copy good lines of all fields
        int [][] curGrayField = new int [curGray.length/2][curGray[0].length];
        int [][] prevGrayField = new int [prevGray.length/2][prevGray[0].length];
        int [][] prev2GrayField = new int [prev2Gray.length/2][prev2Gray[0].length];
        int [][] nextGrayField = new int [nextGray.length/2][nextGray[0].length];
        int [][] next2GrayField = new int [next2Gray.length/2][next2Gray[0].length];
        
         // copy all good data to frame
        if (topField == true) { // copy all even lines
            for (int i = 0; i < curGray.length; i += 2) {
                System.arraycopy(curGray[i], 0, curGrayField[i>>1], 0, curGray[i].length);
                System.arraycopy(next2Gray[i], 0, next2GrayField[i>>1], 0, next2Gray[i].length);
                System.arraycopy(prev2Gray[i], 0, prev2GrayField[i>>1], 0, prev2Gray[i].length);
            }
            for (int i = 1; i < curGray.length; i += 2) {
                System.arraycopy(prevGray[i], 0, prevGrayField[(i-1)>>1], 0, prevGray[i].length);
                System.arraycopy(nextGray[i], 0, nextGrayField[(i-1)>>1], 0, nextGray[i].length);
            }
        } else { // copy all odd lines
            for (int i = 1; i < curGray.length; i += 2) {
                System.arraycopy(curGray[i], 0, curGrayField[(i-1)>>1], 0, curGray[i].length);
                System.arraycopy(next2Gray[i], 0, next2GrayField[(i-1)>>1], 0, next2Gray[i].length);
                System.arraycopy(prev2Gray[i], 0, prev2GrayField[(i-1)>>1], 0, prev2Gray[i].length);
            }
            for (int i = 0; i < curGray.length; i += 2) {
                System.arraycopy(prevGray[i], 0, prevGrayField[i>>1], 0, prevGray[i].length);
                System.arraycopy(nextGray[i], 0, nextGrayField[i>>1], 0, nextGray[i].length);
            }
        }
        HashMap<Integer, MotionVector> motionVectors = new HashMap<Integer, MotionVector>();

        // macroBlock centers are aligned depending on the size of the macro blocks,
        // we have to fix this later to cope with the interlaced data
        for (int macroBlockI = (BLOCKSIZE / 2); macroBlockI < curGrayField.length - (BLOCKSIZE / 2); macroBlockI += BLOCKSIZE) {
            for (int macroBlockJ = (BLOCKSIZE / 2); macroBlockJ < curGrayField[0].length - (BLOCKSIZE / 2); macroBlockJ += BLOCKSIZE) {

                ArrayList<OptimalMatchingBlock> OMBList = new ArrayList<OptimalMatchingBlock>();

                // previous Frame is the opposite parity of the current frame
                OptimalMatchingBlock prevOMB = this.estimateBackwardsMotionVector(!this.topField, curGrayField, prevGrayField, macroBlockI, macroBlockJ);
                prevOMB.m = -1;
                OMBList.add(prevOMB);
                // previous before previous Frame is the same parity as the current frame
                OptimalMatchingBlock prev2OMB = this.estimateBackwardsMotionVector(this.topField, curGrayField, prev2GrayField, macroBlockI, macroBlockJ);
                prev2OMB.m = -2;
                OMBList.add(prev2OMB);
                
                // next Frame is the opposite parity as the current frame
                OptimalMatchingBlock nextOMB = this.estimateForwardMotionVector(!this.topField, curGrayField, nextGrayField, macroBlockI, macroBlockJ);
                nextOMB.m = 1;
                OMBList.add(nextOMB);
                
                OptimalMatchingBlock next2OMB = this.estimateForwardMotionVector(this.topField, curGrayField, next2GrayField, macroBlockI, macroBlockJ);
                next2OMB.m = 2;
                OMBList.add(next2OMB);

                // find OMB with smallest error
                Collections.sort(OMBList);
                OptimalMatchingBlock bestBlock = OMBList.get(0);

                // compute MotionVector
                int vecComponentI, vecComponentJ;
                MotionVector m;
                
              
                // motion vectors always point to the optimal matching block, this means however
                // that backward motion vectors point into the past while forward motion vectors
                // point into the future. keep this in mind when visualizing, we need the correct 
                // vector for deinterlacing
                vecComponentI = bestBlock.centerI - macroBlockI;
                vecComponentJ = bestBlock.centerJ - macroBlockJ;
                m = new MotionVector(vecComponentI, vecComponentJ, macroBlockI, macroBlockJ);
                m.setMAD(bestBlock.distance);
                m.setSameParity(bestBlock.sameParity);
                m.setM(bestBlock.m);
                
                if(m.isSameParity() && m.getIComponent() % 4 == 0) { 
                    // compute the error between the current block and the block with half the motion vector of the OMB
                    MADOffsetsWrapper OMBError = null;
                 
                    if(m.getM() == -2) {
                        OMBError = this.computeMADWithDoubleStep(this.topField, curGray, prev2Gray, macroBlockI, macroBlockJ, 
                                                                 macroBlockI+m.getIComponent()/2, 
                                                                 macroBlockJ+m.getJComponent()/2);
                    }
                    if(m.getM() == -1) {
                        OMBError = this.computeMADWithDoubleStep(this.topField, curGray, prevGray, macroBlockI, macroBlockJ, 
                                                                 macroBlockI+m.getIComponent()/2, 
                                                                 macroBlockJ+m.getJComponent()/2);
                    }
                    if(m.getM() == 1) {
                        OMBError = this.computeMADWithDoubleStep(this.topField, curGray, next2Gray, macroBlockI, macroBlockJ, 
                                                                 macroBlockI+m.getIComponent()/2, 
                                                                 macroBlockJ+m.getJComponent()/2);
                    }
                    if(m.getM() == 2) {
                        OMBError = this.computeMADWithDoubleStep(this.topField, curGray, next2Gray, macroBlockI, macroBlockJ, 
                                                                 macroBlockI+m.getIComponent()/2, 
                                                                 macroBlockJ+m.getJComponent()/2);
                    }
                    m.setOMBHalfMVError(OMBError.mad);
                    
                }
                Integer key = MathTools.cantorTupel(macroBlockI, macroBlockJ);
                motionVectors.put(key, m);
                // we sometimes add +1 to the row this might lead to some motion vectors that 
                // point outside the last block in the image
                if(this.isSaneMotionVector(this.cur.getWidth(), this.cur.getHeight(), m) == false) 
                {
                    System.out.println("Insane Motion Vector found!");
                    this.isSaneMotionVector(this.cur.getWidth(), this.cur.getHeight(), m);
               }
            }
        }
        return motionVectors;

    }

    private boolean isSaneMotionVector(int width, int height, MotionVector vector)
    {
        if(vector.getIComponent()+vector.getIPosition()+(BLOCKSIZE/2) >= height || vector.getIComponent()+vector.getIPosition()-(BLOCKSIZE/2) < 0)
        {
            return false;
        }
        if(vector.getJComponent()+vector.getJPosition()+(BLOCKSIZE/2) >= width || vector.getJComponent()+vector.getJPosition()-(BLOCKSIZE/2) < 0)
        {
            return false;
        }
        return true;
    }
    
    private OptimalMatchingBlock estimateBackwardsMotionVector(boolean prevTopField, int[][] cur, int[][] prev, int centerI, int centerJ)
    {
        OptimalMatchingBlock ret = this.performFullSearchOnPreviousFieldBlock(prevTopField, cur, prev, centerI, centerJ);
        ret.sameParity = (prevTopField == this.topField);
        ret.forwardMotionVector = false;
        return ret;
    }
    
    private OptimalMatchingBlock estimateForwardMotionVector(boolean nextTopField, int[][] cur, int[][] next, int centerI, int centerJ)
    {
        OptimalMatchingBlock ret = this.performFullSearchOnPreviousFieldBlock(nextTopField, cur, next, centerI, centerJ);
        ret.sameParity = (nextTopField == this.topField);
        ret.forwardMotionVector = true;
        return ret;
        
    }

    // perform full search in the prevField for the data in the current field,
    // use only original data, this means vertical steps of 2 pixels are performed
    // also we have to take care of the correct vertical indices in the previous field
    private OptimalMatchingBlock performFullSearchOnPreviousFieldBlock(boolean prevTopField, int[][] cur, int[][] prevField,
            int centerI, int centerJ)
    {
        double minValue = Double.MAX_VALUE;
        int minCenterI = -1;
        int minCenterJ = -1;

        // candidates with minimum MAD
        ArrayList<OptimalMatchingBlock> blockCandidates = null;


        // perform full search within the search window, do not use bad lines as center so skip each second row
        for (int i = -(SEARCHSIZE / 2) + (BLOCKSIZE / 2); i < (SEARCHSIZE / 2) + (BLOCKSIZE / 2); i += 2) {
            for (int j = -(SEARCHSIZE / 2) + (BLOCKSIZE / 2); j < (SEARCHSIZE / 2) - (BLOCKSIZE / 2); j++) {

                // check if we can search at this block position or it is outside the frame
                if (centerI + i <= (BLOCKSIZE / 2) || centerJ + j <= (BLOCKSIZE / 2)) {
                    continue;
                }
                if (centerI + i > (cur.length - (BLOCKSIZE / 2))-1 || centerJ + j > (cur[0].length - (BLOCKSIZE / 2))-1) {
                    continue;
                }
                MADOffsetsWrapper tmp = computeMADWithDoubleStep(prevTopField, cur, prevField, centerI, centerJ, centerI + i, centerJ + j);
                if (tmp.mad < minValue) {
                    minCenterI = centerI + i + (tmp.offsetCenterI1 - tmp.offsetCenterI2);
                    minCenterJ = centerJ + j;
                    minValue = tmp.mad;

                    // new minimum found clear block Candidates list and create new list
                    blockCandidates = new ArrayList<OptimalMatchingBlock>();
                    blockCandidates.add(new OptimalMatchingBlock(minValue, minCenterI, minCenterJ));
                } else if (tmp.mad == minValue) {
                    minCenterI = centerI + i + (tmp.offsetCenterI1 - tmp.offsetCenterI2);
                    minCenterJ = centerJ + j;
                    minValue = tmp.mad;
                    blockCandidates.add(new OptimalMatchingBlock(minValue, minCenterI, minCenterJ));
                }
            }
        }
        // pick the block candidate with the smallest motion vector
        return this.findOMBWithSmallestMotionVector(blockCandidates, centerI, centerJ);
    }

    private OptimalMatchingBlock findOMBWithSmallestMotionVector(ArrayList<OptimalMatchingBlock> blockCandidates,
            int macroBlockI, int macroBlockJ)
    {
        double smallestLength = Double.MAX_VALUE;
        OptimalMatchingBlock smallestOMB = null;

        for (OptimalMatchingBlock tmpBlock : blockCandidates) {
            double vecLength = Math.sqrt((tmpBlock.centerI - macroBlockI) * (tmpBlock.centerI - macroBlockI)
                    + (tmpBlock.centerJ - macroBlockJ) * (tmpBlock.centerJ - macroBlockJ));
            if (vecLength < smallestLength) {
                smallestOMB = tmpBlock;
                smallestLength = vecLength;
            }
        }
        return smallestOMB;
    }

    // compute the mean absolute difference using double steps 
    private MADOffsetsWrapper computeMADWithDoubleStep(boolean prevTopField, int[][] data1, int[][] data2,
            int centerI, int centerJ, int centerI2, int centerJ2)
    {
        double d = 0;

        int centerIOffset = 0;
        int centerI2Offset = 0;

        // fix indices 
        if (this.topField == true) {
            // make sure all indices into data1 will be on even lines
            if ((centerI - ((BLOCKSIZE / 2))) % 2 == 1) {
                centerIOffset = 1;
            }
        } else {
            // make sure all indices into data1 will be on odd lines
            if ((centerI - ((BLOCKSIZE / 2))) % 2 == 0) {
                centerIOffset = 1;
            }
        }

        if (prevTopField == true) { // make sure all indices into data2 will be on even lines
            // make sure all indices into data1 will be on even lines
            if ((centerI2 - ((BLOCKSIZE / 2))) % 2 == 1) {
                centerI2Offset = 1;
            }
        } else {
            // make sure all indices into data2 will be on odd lines
            if ((centerI2 - ((BLOCKSIZE / 2))) % 2 == 0) {
                centerI2Offset = 1;
            }
        }
        for (int i = -(BLOCKSIZE / 2); i < (BLOCKSIZE / 2); i += 2) {
            for (int j = -(BLOCKSIZE / 2); j < (BLOCKSIZE / 2); j++) {

                short tmp = (short) (data1[centerI + centerIOffset + i][centerJ + j] - data2[centerI2 + centerI2Offset + i][centerJ2 + j]);
                if (tmp < 0) {
                    d += (double) -tmp;
                } else {
                    d += (double) tmp;
                }
            }
        }
        MADOffsetsWrapper ret = new MADOffsetsWrapper();
        ret.mad = d / (BLOCKSIZE * BLOCKSIZE);
        ret.offsetCenterI1 = centerIOffset;
        ret.offsetCenterI2 = centerI2Offset;
        return ret;
    }

    class MADOffsetsWrapper
    {

        double mad;
        int offsetCenterI1;
        int offsetCenterI2;
    }

    class OptimalMatchingBlock implements Comparable<OptimalMatchingBlock>
    {

        public double distance;
        public int centerI, centerJ;
        public boolean forwardMotionVector;
        public boolean sameParity;
        public int m; // reference frame index 
        
        
        public OptimalMatchingBlock(double distance, int centerI, int centerJ)
        {
            this.distance = distance;
            this.centerI = centerI;
            this.centerJ = centerJ;
        }

        @Override
        public int compareTo(OptimalMatchingBlock t)
        {
            if (this.distance < t.distance) {
                return -1;
            } else if (this.distance == t.distance) {
                return 0;
            } else {
                return 1;
            }
        }
    }
}
