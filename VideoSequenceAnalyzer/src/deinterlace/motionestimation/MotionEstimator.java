/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package deinterlace.motionestimation;

import data.LPFrame;
import java.util.HashMap;
import jceliac.tools.math.MathTools;
import tools.ArrayTools;

/**
 *
 * @author shegen
 */
public abstract class MotionEstimator
{
    
    public abstract HashMap <Integer, MotionVector> computeMotionVectors() 
            throws Exception;
    
    public static int [] getMacroBlockCoordsForPixel(boolean topField, int i, int j, int blocksize) 
    {
       // if(topField == false) {
       //     i = i+1;
       // }
        int coordI = ((i/blocksize)*blocksize) + (blocksize/2) ;
        int coordJ = ((j/blocksize)*blocksize) + (blocksize/2);
        
        return new int [] {coordI, coordJ};
    }
    
    
    // mean absolute difference
    protected double computeMAD(int [][] data1, int [][] data2, int centerI, int centerJ, int centerI2, int centerJ2, int blocksize)
    {
        double d = 0;
        for(int i = -(blocksize/2); i < (blocksize/2); i++) {
            for(int j = -(blocksize/2); j < (blocksize/2); j++) {
                short tmp = (short)(data1[centerI+i][centerJ+j] - data2[centerI2+i][centerJ2+j]);
                if(tmp < 0) {
                    d += (double)-tmp;
                }else {
                    d+= (double)tmp;
                }
            }
        }
        return d / (blocksize*blocksize);
    }
    
    
    
    public LPFrame visualizeMotionVectors(LPFrame reference, HashMap <Integer,MotionVector> motionVectors, int blocksize, boolean topField)
            throws Exception
    {
        if (reference.isColumnFirstStored()) {
            throw new Exception("Column First Mode not supported!");
        }
        
        
        LPFrame mvFrame = new LPFrame();
        mvFrame.setColumnFirstStored(false);
        mvFrame.setBlueData(ArrayTools.cloneArray2D(reference.getBlueData()));
        mvFrame.setRedData(ArrayTools.cloneArray2D(reference.getRedData().clone()));
        mvFrame.setGreenData(ArrayTools.cloneArray2D(reference.getGreenData().clone()));
        mvFrame.setGrayData(ArrayTools.cloneArray2D(reference.getGrayData().clone()));
        
        for (int macroBlockI = (blocksize / 2); macroBlockI < reference.getHeight() - (blocksize / 2); macroBlockI += blocksize) {
            for (int macroBlockJ = (blocksize / 2); macroBlockJ < reference.getWidth() - (blocksize / 2); macroBlockJ += blocksize) {
               
                int [] macroBlockCoords = this.getMacroBlockCoordsForPixel(topField, macroBlockI, macroBlockJ, blocksize);
                Integer key = MathTools.cantorTupel(macroBlockCoords[0], macroBlockCoords[1]);
                MotionVector m = motionVectors.get(key);
                
                if(m == null) {
                     int blub = 0;
                     System.out.println("Motion Vector for Macro Block "+macroBlockI+" "+macroBlockJ+" not found!");
                }
                int [] mVec = new int[] {m.getIComponent(), m.getJComponent()};
                if(m.getM() < 0) { // backwards motion vector switch signs
                    mVec[0] = - mVec[0];
                    mVec[1] = -mVec[1];
                }
                this.drawMotionVector(mvFrame.getRedData(), mVec, macroBlockI, macroBlockJ, m.getReliability());
                this.drawMotionVector(mvFrame.getBlueData(), mVec, macroBlockI, macroBlockJ, m.getReliability());
                this.drawMotionVector(mvFrame.getGreenData(), mVec, macroBlockI, macroBlockJ, m.getReliability());
                this.drawMotionVector(mvFrame.getGrayData(), mVec, macroBlockI, macroBlockJ, m.getReliability());
            }
        }
        return mvFrame;
    }
    
    public void drawMotionVector(int [][] data, int [] mVector, int i, int j, double reliability) 
    {
        int color;
        
        color = (int)(255.0d*reliability);
        
        if(reliability > 0.01) {
            color = 255;
        }else if(reliability > 0) {
            color = 125;
        }else {
            color = 0;
        }
        
        // draw a line from (i,j) to (i+mVector[0],j+mVector[1]), first
        // compute length of mVector
        double vLength = Math.sqrt(mVector[0]*mVector[0] + mVector[1]*mVector[1]);
        if(vLength == 0) {
            return;
        }
        // normalize vector
        double [] normVec = {(double)mVector[0] / vLength, (double)mVector[1] / vLength};
        
        // draw line
        for(double lambda = 0; lambda <= vLength; lambda+=0.5) {
            int iPos = (int)Math.round((double)i + normVec[0] * lambda);
            int jPos = (int)Math.round((double)j + normVec[1] * lambda);
            if(iPos < 0 || jPos < 0 || iPos >= data.length || jPos >= data[0].length) {
                break;
            }
            data[iPos][jPos] = color;
        }
        
        if(mVector[1] >= 0 && mVector[0] >= 0) {
            drawArrowQ12(data, i+mVector[0], j+mVector[1], normVec, vLength/4, color);
        }else if(mVector[1] <= 0 && mVector[0] >= 0)
        {
            drawArrowQ12(data, i+mVector[0], j+mVector[1], normVec, vLength/4, color);
        }else if(mVector[1] <= 0 && mVector[0] <= 0) {
            drawArrowQ34(data, i+mVector[0], j+mVector[1], normVec, vLength/4, color);
        }else if(mVector[1] >= 0 && mVector[0] <= 0) {
            drawArrowQ34(data, i+mVector[0], j+mVector[1], normVec, vLength/4, color);
            
        }
    }
    
    
    // use this if x > 0 and y > 0
    private void drawArrowQ12(int[][] data, int i, int j, double[] normVec, double len, int color)
    {
        
        // jEnd is the length of the x-component of the vector, compute angle between vector
        // and x-axis
        double alpha;
        alpha = Math.acos(normVec[1] / 1.0d);
        double gamma, gamma2;

        gamma = Math.PI - (alpha + (Math.PI/4));
        gamma2 = (Math.PI / 2) - gamma; 

        
        // compute vector with an angle to the x-axis of gamma
        double[] gammaVec = new double[2];
        gammaVec[0] = -Math.sin(gamma); // Y invert Y because images coords grow downwards
        gammaVec[1] = Math.cos(gamma); // X

        double[] gamma2Vec = new double[2];
        gamma2Vec[0] = -Math.sin(gamma2);  // Y invert Y because image coords grow downwards
        gamma2Vec[1] = -Math.cos(gamma2);  // X invert X to point into the opposite direction as the first arrow part


        // normalize vectors 
        double gammaVecLen = Math.sqrt(gammaVec[0]*gammaVec[0] + gammaVec[1]*gammaVec[1]);
        gammaVec[0] /= gammaVecLen;
        gammaVec[1] /= gammaVecLen;
        
        double gamma2VecLen = Math.sqrt(gamma2Vec[0]*gamma2Vec[0] + gamma2Vec[1]*gamma2Vec[1]);
        gamma2Vec[0] /= gamma2VecLen;
        gamma2Vec[1] /= gamma2VecLen;

        for (double lambda = 0; lambda < len; lambda += 0.5) {
            int iPos = (int) Math.round((double) i + (gammaVec[0]) * lambda);
            int jPos = (int) Math.round((double) j + (gammaVec[1]) * lambda);
            if(iPos < 0 || jPos < 0 || iPos >= data.length || jPos >= data[0].length) {
                break;
            }
            data[iPos][jPos] = color;
        }

        for (double lambda = 0; lambda < len; lambda += 0.5) {
            int iPos = (int) Math.round((double) i + (gamma2Vec[0]) * lambda);
            int jPos = (int) Math.round((double) j + (gamma2Vec[1]) * lambda);
            if(iPos < 0 || jPos < 0 || iPos >= data.length || jPos >= data[0].length) {
                break;
            }
            data[iPos][jPos] = color;
        }
    }
    
    private void drawArrowQ34(int[][] data, int i, int j, double[] normVec, double len, int color)
    {
        
        // jEnd is the length of the x-component of the vector, compute angle between vector
        // and x-axis
        double alpha;
        alpha = Math.acos(normVec[1] / -1.0d); // -1 here
        double gamma, gamma2;

        gamma = Math.PI - (alpha + (Math.PI/4));
        gamma2 = (Math.PI / 2) - gamma; 

        // compute vector with an angle to the x-axis of gamma, switched signs
        double[] gammaVec = new double[2];
        gammaVec[0] = Math.sin(gamma); // Y 
        gammaVec[1] = -Math.cos(gamma); // X

        double[] gamma2Vec = new double[2];
        gamma2Vec[0] = Math.sin(gamma2);  // Y
        gamma2Vec[1] = Math.cos(gamma2);  // X 

        // normalize vectors 
        double gammaVecLen = Math.sqrt(gammaVec[0]*gammaVec[0] + gammaVec[1]*gammaVec[1]);
        gammaVec[0] /= gammaVecLen;
        gammaVec[1] /= gammaVecLen;
        
        double gamma2VecLen = Math.sqrt(gamma2Vec[0]*gamma2Vec[0] + gamma2Vec[1]*gamma2Vec[1]);
        gamma2Vec[0] /= gamma2VecLen;
        gamma2Vec[1] /= gamma2VecLen;

        for (double lambda = 0; lambda < len; lambda += 0.5) {
            int iPos = (int) Math.round((double) i + (gammaVec[0]) * lambda);
            int jPos = (int) Math.round((double) j + (gammaVec[1]) * lambda);
            if(iPos < 0 || jPos < 0 || iPos >= data.length || jPos >= data[0].length) {
                break;
            }
            data[iPos][jPos] = color;
        }

        for (double lambda = 0; lambda < len; lambda += 0.5) {
            int iPos = (int) Math.round((double) i + (gamma2Vec[0]) * lambda);
            int jPos = (int) Math.round((double) j + (gamma2Vec[1]) * lambda);
            if(iPos < 0 || jPos < 0 || iPos >= data.length || jPos >= data[0].length) {
                break;
            }
            data[iPos][jPos] = color;
        }
    }
  
    public double computeAngle(double [] vec1, double [] vec2) 
            
    {
        // use inner product
        double len1 = Math.sqrt(vec1[0]*vec1[0] + vec1[1]*vec1[1]);
        double len2 = Math.sqrt(vec2[0]*vec2[0] + vec2[1]*vec2[1]);
        
        double rad = Math.acos((vec1[0]*vec2[0]+vec1[1]*vec2[1]) / (len1*len2));
        return (rad / Math.PI) * 180;
    }
    
}
