/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package deinterlace.motionestimation;

import data.LPFrame;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import jceliac.tools.math.MathTools;

/**
 * Implements the motion estimation from "A threshold-based de-interlacing algorithm using
 * motion compensation and directional interpolation" and "A Five-Field Motion Compensated 
 * De-interlacing Method Based on Vertical Motion". 
 * 
 * @author shegen
 */
public class FiveFieldMotionEstimatorNew extends MotionEstimator
{

    // we do not use square sizes, because we only use the good lines, so the
    // vertical resolution is cut to half (in fact it isnt because we use only good lines)
    // but the dimensions of the fields will be cut to half in the vertical direction
    private final int SEARCHWIDTH = 32;
    private final int SEARCHHEIGHT = SEARCHWIDTH/2;
    
    public static final int BLOCKSIZE = 8;
    
    public static final int BLOCKWIDTH = BLOCKSIZE; // 8
    public static final int BLOCKHEIGHT = BLOCKSIZE/2;
    
    
    
    
    public static final double THRESHOLD = 3.0d;
    //private LPFrame prev2, prev, cur, next, next2;
    int [][] prev2;
    int [][] prev;
    int [][] next;
    int [][] next2;
    int [][] cur;
    
    private boolean topField;

   /* public FiveFieldMotionEstimatorNew(boolean topField, LPFrame prev2, LPFrame prev, LPFrame cur, LPFrame next, LPFrame next2)
    {
        this.prev2 = prev2;
        this.prev = prev;
        this.cur = cur;
        this.next = next;
        this.next2 = next2;
        this.topField = topField;
    }*/
    
    /* THIS EXPECTS ROW FIRST MODE!!!! BE CAREFUL!
     * 
     */
    public FiveFieldMotionEstimatorNew(boolean topField, int [][] cur, int [][] prev2, int [][] prev, int [][] next, int [][] next2)
    {
        this.prev2 = prev2;
        this.prev = prev;
        this.cur = cur;
        this.next = next;
        this.next2 = next2;
        this.topField = topField;
    }

    @Override
    public HashMap<Integer, MotionVector> computeMotionVectors()
            throws Exception
    {
        return this.computeForwardAndBackwardMotionVectors();
    }

    private HashMap<Integer, MotionVector> computeForwardAndBackwardMotionVectors()
            throws Exception
    {
        int[][] curData = this.cur;
        int[][] prevData = this.prev;
        int[][] prev2Data = this.prev2;
        int[][] nextData = this.next;
        int[][] next2Data = this.next2;
        
        // copy good lines of all fields
        int [][] curField = new int [curData.length/2][curData[0].length];
        int [][] prevField = new int [prevData.length/2][prevData[0].length];
        int [][] prev2Field = new int [prev2Data.length/2][prev2Data[0].length];
        int [][] nextField = new int [nextData.length/2][nextData[0].length];
        int [][] next2Field = new int [next2Data.length/2][next2Data[0].length];
        
        // copy all good data to frame
        if (topField == true) { // copy all even lines
            for (int i = 0; i < curData.length; i += 2) {
                System.arraycopy(curData[i], 0, curField[i>>1], 0, curData[i].length);
                System.arraycopy(next2Data[i], 0, next2Field[i>>1], 0, next2Data[i].length);
                System.arraycopy(prev2Data[i], 0, prev2Field[i>>1], 0, prev2Data[i].length);
            }
            for (int i = 1; i < curData.length; i += 2) {
                System.arraycopy(prevData[i], 0, prevField[(i-1)>>1], 0, prevData[i].length);
                System.arraycopy(nextData[i], 0, nextField[(i-1)>>1], 0, nextData[i].length);
            }
        } else { // copy all odd lines
            for (int i = 1; i < curData.length; i += 2) {
                System.arraycopy(curData[i], 0, curField[(i-1)>>1], 0, curData[i].length);
                System.arraycopy(next2Data[i], 0, next2Field[(i-1)>>1], 0, next2Data[i].length);
                System.arraycopy(prev2Data[i], 0, prev2Field[(i-1)>>1], 0, prev2Data[i].length);
            }
            for (int i = 0; i < curData.length; i += 2) {
                System.arraycopy(prevData[i], 0, prevField[i>>1], 0, prevData[i].length);
                System.arraycopy(nextData[i], 0, nextField[i>>1], 0, nextData[i].length);
            }
        }
        
        HashMap<Integer, MotionVector> motionVectors = new HashMap<Integer, MotionVector>();
         
        // we now have only good lines in all fields, which means we can directly compare indices
        // but have to be aware to fix the motion vectors later because the opposite parity
        // fields will always have an offset of +1
        // macroBlock centers are aligned depending on the size of the macro blocks,
        // we have to fix this later to cope with the interlaced data
        for (int macroBlockI = (BLOCKHEIGHT/ 2); macroBlockI < curField.length- (BLOCKHEIGHT/2); macroBlockI += BLOCKHEIGHT) 
        {
            for (int macroBlockJ = (BLOCKWIDTH / 2); macroBlockJ < curField[0].length - (BLOCKWIDTH / 2); macroBlockJ += BLOCKWIDTH) 
            {
              
                ArrayList<OptimalMatchingBlock> OMBList = new ArrayList<OptimalMatchingBlock>();
                // previous Frame is the opposite parity of the current frame
                OptimalMatchingBlock prevOMB = this.estimateBackwardsMotionVector(!this.topField, curField, prevField, macroBlockI, macroBlockJ);
                prevOMB.m = -1;
                OMBList.add(prevOMB);
                // previous before previous Frame is the same parity as the current frame
                OptimalMatchingBlock prev2OMB = this.estimateBackwardsMotionVector(this.topField, curField, prev2Field, macroBlockI, macroBlockJ);
                prev2OMB.m = -2;
                OMBList.add(prev2OMB);
                
                // next Frame is the opposite parity as the current frame
                OptimalMatchingBlock nextOMB = this.estimateForwardMotionVector(!this.topField, curField, nextField, macroBlockI, macroBlockJ);
                nextOMB.m = 1;
                OMBList.add(nextOMB);
                
                OptimalMatchingBlock next2OMB = this.estimateForwardMotionVector(this.topField, curField, next2Field, macroBlockI, macroBlockJ);
                next2OMB.m = 2;
                OMBList.add(next2OMB);

                // find OMB with smallest error
                Collections.sort(OMBList);
                OptimalMatchingBlock bestBlock = OMBList.get(0);
                
                 // compute MotionVector
                int vecComponentI, vecComponentJ;
                MotionVector m;
             
                
                // fix the vertical coordinates of the motion block centers, depending on the field type
                // if it's a top field the real index is 2*currentIndex
                // if it's a bottom field the real index is 2*currentIndex+2
                int fixedMacroBlockI;
                
                if(bestBlock.topField) {
                    bestBlock.centerI = bestBlock.centerI * 2;
                }else {
                    bestBlock.centerI = bestBlock.centerI * 2 + 1;
                }
                if(this.topField) {
                    fixedMacroBlockI = macroBlockI * 2;
                }else {
                    fixedMacroBlockI = macroBlockI * 2 +1 ;
                }
                vecComponentI = bestBlock.centerI - fixedMacroBlockI;
                vecComponentJ = bestBlock.centerJ - macroBlockJ;
                
                // f
                m = new MotionVector(vecComponentI, vecComponentJ, fixedMacroBlockI, macroBlockJ);
                m.setMAD(bestBlock.distance);
                m.setSameParity(bestBlock.sameParity);
                m.setM(bestBlock.m); 
                
                  // sanity check
                if(m.isSameParity()) { // vertical motion must be even
                    if(m.getIComponent() % 2 != 0) {
                        System.out.println("Wrong Motion Vector Found!");
                    }
                }else {
                    if(m.getIComponent() %2 == 0) {
                        System.out.println("Wrong Motion Vector Found!");
                    }
                }
                if(m.getMAD() > 0 && m.getM() == -1) {
                    if(m.getIComponent() % 2 == 1) {
                        int blub = 0;
                    }
                }
                if( (m.getM() == 2 || m.getM() == -2)  && m.getIComponent() %4 == 0) {
                    this.computeHalfMVError(m, curData, prevData, nextData);
                }
                Integer key = MathTools.cantorTupel(fixedMacroBlockI, macroBlockJ);
                motionVectors.put(key, m);
               
            }
        }
        return motionVectors;
    }
    
    
    
    private void computeHalfMVError(MotionVector mvec, int [][] cur, int [][] prev, int [][] next) 
    {
        int [][] field = null;
        
        if(mvec.getM() == -2) {
            field = prev;
        }
        if(mvec.getM() == 2) {
            field = next;
        }
        int offsetI = mvec.getIComponent() / 2;
        int offsetJ = mvec.getJComponent() / 2;
        
        if((mvec.getIComponent()+offsetI + (BLOCKSIZE/2)) > field.length ||
           (mvec.getJComponent()+offsetJ + (BLOCKSIZE/2)) > field[0].length) {
            mvec.setOMBHalfMVError(Double.MAX_VALUE);
        }else {
            double error = this.computeMAD(cur, field, mvec.getIPosition(), mvec.getJPosition(), mvec.getIPosition()+offsetI, mvec.getJPosition()+offsetJ);
            mvec.setOMBHalfMVError(error);           
        }
    }
    
    private OptimalMatchingBlock estimateBackwardsMotionVector(boolean prevTopField, int[][] cur, int[][] prev, int centerI, int centerJ)
    {
        OptimalMatchingBlock ret = this.performFullSearch(cur, prev, centerI, centerJ);
        ret.sameParity = (prevTopField == this.topField);
        ret.forwardMotionVector = false;
        ret.topField = prevTopField;
        return ret;
    }
    
    private OptimalMatchingBlock estimateForwardMotionVector(boolean nextTopField, int[][] cur, int[][] next, int centerI, int centerJ)
    {
        OptimalMatchingBlock ret = this.performFullSearch(cur, next, centerI, centerJ);
        ret.sameParity = (nextTopField == this.topField);
        ret.forwardMotionVector = true;
        ret.topField = nextTopField;
        return ret;
        
    }

    // perform full search in the prevField for the data in the current field,
    // use only original data, this means vertical steps of 2 pixels are performed
    // also we have to take care of the correct vertical indices in the previous field
    private OptimalMatchingBlock performFullSearch(int[][] cur, int[][] prevField, int centerI, int centerJ)
    {
        double minValue = Double.MAX_VALUE;
        int minCenterI = -1;
        int minCenterJ = -1;

        // candidates with minimum MAD
        ArrayList<OptimalMatchingBlock> blockCandidates = null;


        for (int i = -(SEARCHHEIGHT / 2) + (BLOCKHEIGHT / 2); i < (SEARCHHEIGHT / 2) - (BLOCKHEIGHT / 2); i++) {
            for (int j = -(SEARCHWIDTH / 2) + (BLOCKWIDTH / 2); j < (SEARCHWIDTH / 2) - (BLOCKWIDTH / 2); j++) {

                // check if we can search at this block position or it is outside the frame
                if (centerI + i < (BLOCKHEIGHT / 2) || centerJ + j < (BLOCKWIDTH / 2)) {
                    continue;
                }
                if (centerI + i >= (cur.length - (BLOCKHEIGHT / 2)) || centerJ + j >= (cur[0].length - (BLOCKWIDTH / 2))) {
                    continue;
                }
                double mad = computeMAD(cur, prevField, centerI, centerJ, centerI + i, centerJ + j);
                if (mad < minValue) {
                    minCenterI = centerI + i;
                    minCenterJ = centerJ + j;
                    minValue = mad;

                    // new minimum found clear block Candidates list and create new list
                    blockCandidates = new ArrayList<OptimalMatchingBlock>();
                    blockCandidates.add(new OptimalMatchingBlock(minValue, minCenterI, minCenterJ));
                } else if (mad == minValue) {
                    minCenterI = centerI + i;
                    minCenterJ = centerJ + j;
                    minValue = mad;
                    blockCandidates.add(new OptimalMatchingBlock(minValue, minCenterI, minCenterJ));
                }
            }
        }
        // pick the block candidate with the smallest motion vector
        return this.findOMBWithSmallestMotionVector(blockCandidates, centerI, centerJ);
    }

    private OptimalMatchingBlock findOMBWithSmallestMotionVector(ArrayList<OptimalMatchingBlock> blockCandidates,
            int macroBlockI, int macroBlockJ)
    {
        double smallestLength = Double.MAX_VALUE;
        OptimalMatchingBlock smallestOMB = null;

        for (OptimalMatchingBlock tmpBlock : blockCandidates) {
            double vecLength = Math.sqrt((tmpBlock.centerI - macroBlockI) * (tmpBlock.centerI - macroBlockI)
                    + (tmpBlock.centerJ - macroBlockJ) * (tmpBlock.centerJ - macroBlockJ));
            if (vecLength < smallestLength) {
                smallestOMB = tmpBlock;
                smallestLength = vecLength;
            }
        }
        return smallestOMB;
    }

    
    
    // mean absolute difference
    private double computeMAD(int [][] data1, int [][] data2, int centerI, int centerJ, int centerI2, int centerJ2)
    {
        double d = 0;
        for(int i = -(BLOCKHEIGHT/2); i < (BLOCKHEIGHT/2); i++) {
            for(int j = -(BLOCKWIDTH/2); j < (BLOCKWIDTH/2); j++) {
                short tmp = (short)(data1[centerI+i][centerJ+j] - data2[centerI2+i][centerJ2+j]);
                if(tmp < 0) {
                    d += (double)-tmp;
                }else {
                    d+= (double)tmp;
                }
            }
        }
        return d / (BLOCKWIDTH*BLOCKHEIGHT);   
    }
    

    class OptimalMatchingBlock implements Comparable<OptimalMatchingBlock>
    {

        public double distance;
        public int centerI, centerJ;
        public boolean forwardMotionVector;
        public boolean sameParity;
        public int m; // reference frame index 
        public boolean topField;
        
        
        public OptimalMatchingBlock(double distance, int centerI, int centerJ)
        {
            this.distance = distance;
            this.centerI = centerI;
            this.centerJ = centerJ;
        }

        @Override
        public int compareTo(OptimalMatchingBlock t)
        {
            if (this.distance < t.distance) {
                return -1;
            } else if (this.distance == t.distance) {
                return 0;
            } else {
                return 1;
            }
        }
    }
}
