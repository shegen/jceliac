/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package deinterlace.motionestimation;

import java.util.HashMap;
import jceliac.tools.math.MathTools;

/**
 * This class implements the method proposed by Wang, Vincent and Blanchfield
 * in their paper: Hybrid De-Interlacing Algorithm Based on Motion Vector Reliability. 
 * Which is based on a probabilistic framework. 
 * 
 * @author shegen
 */
public class MotionVectorReliability
{
    
    
    private int [][] imageDataK;
    private int [][] imageDataKMinus1;
    private HashMap <Integer,MotionVector> motionVectors;
    
    public MotionVectorReliability(int [][] imageDataK, int [][] imageDataKMinus1, HashMap <Integer,MotionVector> motionVectors)
    {
        this.imageDataK = imageDataK;
        this.imageDataKMinus1 = imageDataKMinus1;
        this.motionVectors = motionVectors;
    }
    
    
    private double computePosteriorProbability(MotionVector mVector) 
            throws Exception
    {
        int coordI = mVector.getIPosition();
        int coordJ = mVector.getJPosition();
        
        // gather motion vector
        MotionVector d = motionVectors.get(MathTools.cantorTupel(coordI, coordJ));
        // gather neighbors, in fact the numbering does not matter
        MotionVector d1 = motionVectors.get(MathTools.cantorTupel(coordI-TSSMotionEstimator.BLOCKSIZE, coordJ));
        MotionVector d2 = motionVectors.get(MathTools.cantorTupel(coordI, coordJ-TSSMotionEstimator.BLOCKSIZE));
        // note the number in the paper is not circular
        MotionVector d3 = motionVectors.get(MathTools.cantorTupel(coordI, coordJ+TSSMotionEstimator.BLOCKSIZE));
        MotionVector d4 = motionVectors.get(MathTools.cantorTupel(coordI+TSSMotionEstimator.BLOCKSIZE, coordJ));
        
        if(d1 == null || d2 == null || d3 == null || d4 == null) {
            return 0;
        }
        double C = this.computeC(coordI, coordJ, d, d1, d2, d3, d4);
        double U = this.computeU(d, d1, d2, d3, d4);
        double displacedBlockDifference = this.computeDisplacedBlockDifference(coordI, coordJ, d);
        double mad = this.computeDisplacedBlockMAD(coordI, coordJ, d);
        
        return (1.0d/C) * Math.exp(-U - displacedBlockDifference);  
    }
    
    // compute reliability of macro block at [coordI][coordJ]
    public double computeReliability(MotionVector mVector) 
            throws Exception
    {
        // according to the paper, the weighting coefficient is equal to zero for fast motion (||d|| >= 2Tfm)
        // it is equal to the posterior probability (reliability) if ||d|| < Tfm
        // and decreases linearly as ||d|| increases from Tfm to 2Tfm
        // Tfm should be equal to 2% of the image width, I don't know if that makes sense for use because
        // the max Vector is (7,7) hence 9.89 units long, the images are however ~600 pixels
        double Tfm = 4;
        double length = mVector.getLength();
        
        double reliability = this.computePosteriorProbability(mVector);
        if(length >= 2*Tfm) {
            return reliability;
        }else if(Tfm <= length && length <= 2*Tfm) {
            return ((2*Tfm - length) / Tfm) * reliability;
        }else {
            return 0;
        }
    }
    
    protected double computeU(MotionVector d, MotionVector d1, MotionVector d2, MotionVector d3, MotionVector d4)
    {
        // compute the mean of the difference d-di we can not compute the variance of
        // element-wise differences I therefore assume the variance of the norms is meant in the paper
        double dd1Norm = d.normedDistance(d1);
        double dd2Norm = d.normedDistance(d2);
        double dd3Norm = d.normedDistance(d3);
        double dd4Norm = d.normedDistance(d4);
        
        double ddMean =  (dd1Norm + dd2Norm + dd3Norm + dd4Norm) / 4;
        // compute the variance
        double ddVar = ((ddMean - dd1Norm)*(ddMean - dd1Norm)  + 
                        (ddMean - dd2Norm)*(ddMean - dd2Norm)  + 
                        (ddMean - dd3Norm)*(ddMean - dd3Norm)  +
                        (ddMean - dd4Norm)*(ddMean - dd4Norm)) / 4;
        
        // the paper is not totally clear what the norm is, I expect it is the euclidean
        // however the differences between norms should have a rather small impact
        double minNorm = -1;
        if(dd1Norm <= dd2Norm && dd1Norm <= dd3Norm && dd1Norm <= dd4Norm) {
            minNorm = dd1Norm;
        }
        if(dd2Norm <= dd1Norm && dd2Norm <= dd3Norm && dd2Norm <= dd4Norm) {
            minNorm = dd2Norm;
        }
        if(dd3Norm <= dd1Norm && dd3Norm <= dd2Norm && dd3Norm <= dd4Norm) {
            minNorm = dd3Norm;
        }
        if(dd4Norm <= dd1Norm && dd4Norm <= dd2Norm && dd4Norm <= dd3Norm) {
            minNorm = dd4Norm;
        }
        if(ddVar == 0) {
            return 0;
        }
        return (1.0d/(2.0d*(ddVar))) * (minNorm*minNorm); // assume the (euclidean)^2 is actually meant
    } 
           
    // generate the set of all possible estimates for the true motion vector,
    // which is, for optimization, reduced to some versions of the estimated motion vector
    // and the neighboring motion vectors
    private MotionVector [] generateD(MotionVector d, MotionVector d1, MotionVector d2, MotionVector d3, MotionVector d4)
    {
        MotionVector [] D = new MotionVector[25+4];
        D[0] = d1;
        D[1] = d2;
        D[2] = d3;
        D[3] = d4;
        
        int i = 4; 
        for(int u = -2 ; u <= 2; u++) {
            for(int v = -2; v <= 2; v++) {
                D[i++] = new MotionVector(d.getIComponent()+u, d.getJComponent()+v, -1, -1);
            }
        }
        return D;
    }
    
    
    private double computeC(int coordI, int coordJ, MotionVector d, MotionVector d1, MotionVector d2, MotionVector d3, MotionVector d4)
    {
        MotionVector [] D = this.generateD(d, d1, d2, d3, d4);
        
        // compute values of U and compute values of the likelihood
        double [] uValues = new double [D.length];
        double [] lValues = new double[D.length]; 
        double [] madValues = new double[D.length];
        for(int i = 0; i < uValues.length; i++) 
        {
            uValues[i] = this.computeU(D[i], d1, d2, d3, d4);
            lValues[i] = this.computeDisplacedBlockDifference(coordI, coordJ, D[i]);
            madValues[i] = this.computeDisplacedBlockMAD(coordI, coordJ, D[i]);
        }
        // compute C
        double C = 0;
        for(int i = 0; i < uValues.length; i++) {
            C += Math.exp(-uValues[i] - lValues[i]);
        }
        return C;
    }
    
    // used for debugging
    private double computeDisplacedBlockMAD(int centerI, int centerJ, MotionVector xi)
    {
        int centerI2 = centerI + xi.getIComponent();
        int centerJ2 = centerJ + xi.getJComponent();
        double d = 0;
        
         for(int i = -TSSMotionEstimator.BLOCKSIZE/2; i < TSSMotionEstimator.BLOCKSIZE/2; i++) {
            for(int j = -TSSMotionEstimator.BLOCKSIZE/2; j < TSSMotionEstimator.BLOCKSIZE/2; j++) {
                short tmp = (short)(imageDataK[centerI+i][centerJ+j] - imageDataKMinus1[centerI2+i][centerJ2+j]);
                if(tmp < 0) {
                    d += (double)-tmp;
                }else {
                    d+= (double)tmp;
                }
            }
        }
        return d / (TSSMotionEstimator.BLOCKSIZE*TSSMotionEstimator.BLOCKSIZE);
    }
    
    private double computeDisplacedBlockDifference(int centerI, int centerJ, MotionVector xi)
    {
        // compute the variance of the displaced pixel differences first
        // compute the mean first
        double diffMean = 0;
        // also store the differences to speed up computation later
        double [] pixelDifferences = new double[TSSMotionEstimator.BLOCKSIZE*TSSMotionEstimator.BLOCKSIZE];
        int index = 0;
        for(int i = centerI-TSSMotionEstimator.BLOCKSIZE/2; i < centerI+TSSMotionEstimator.BLOCKSIZE/2; i++) {
            for(int j = centerJ-TSSMotionEstimator.BLOCKSIZE/2; j < centerJ+TSSMotionEstimator.BLOCKSIZE/2; j++) {
                pixelDifferences[index] = this.imageDataK[i][j] - this.imageDataKMinus1[i+xi.getIComponent()][j+xi.getJComponent()];
                diffMean += pixelDifferences[index];
                index++;
            }
        }
        diffMean /= (TSSMotionEstimator.BLOCKSIZE*TSSMotionEstimator.BLOCKSIZE);
        // compute variance of differences and compute sum of squared pixel differences
        double diffVar = 0;
        double diffSum = 0;
        for(int i = 0; i < pixelDifferences.length; i++) {
            diffVar +=  ((diffMean - pixelDifferences[i]) * (diffMean - pixelDifferences[i]));
            diffSum += (pixelDifferences[i] * pixelDifferences[i]);
        }
        diffVar /= (pixelDifferences.length-1);
        if(diffVar == 0) {
            return 0;
        }
        return (1.0d/(2.0d*diffVar)) * (diffSum);
    }


}
