/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package deinterlace;

import data.LPFrame;
import java.util.ArrayList;
import jceliac.gui.ImageViewerFrame;
import jceliac.tools.data.Frame;

/**
 *
 * @author shegen
 */
public abstract class DeinterlacingFilter
{
    
    public static enum DeinterlacerType {
        ScanLineDuplication,
        ScanLineInterpolation,
        Blend,
        Yadif,
        Null,
        HMD,
        ELA,
        FiveFieldMC,
        VTMedian,
        MATexture
    }
    protected ArrayList <LPFrame> pastFrames;
    protected ArrayList <LPFrame> futureFrames;
    protected LPFrame currentFrame;
    
    // the required number of frames prior to the frame to be deinterlaced of the specific method
    public abstract int getRequiredNumberOfPastFrames();
    
    // the required number of frames posterior to the frame to be deinterlaced of the specific method
    public abstract int getRequiredNumberOfFutureFrames();
    
    public abstract LPFrame performDeinterlace(boolean topField) throws Exception;
    
    
    public LPFrame deinterlace(boolean topField) 
            throws Exception
    {
        this.checkData();
        LPFrame deinterlacedFrame = performDeinterlace(topField);
        
        this.currentFrame = null;
        this.futureFrames = null;
        this.pastFrames = null;
        
        return deinterlacedFrame;
    }
    
    private void checkData() 
            throws Exception
    {
        if(this.pastFrames == null || this.pastFrames.size() < this.getRequiredNumberOfPastFrames()) {
            throw new Exception("Not enough past frames set: "
                                +(this.pastFrames != null ? "Null" : this.pastFrames.size()));
        }
        if(this.futureFrames == null || this.futureFrames.size() < this.getRequiredNumberOfFutureFrames()) {
            throw new Exception("Not enough future frames set: "
                                +(this.futureFrames != null ? "Null" : this.futureFrames.size()));
        }
        if(this.currentFrame == null) {
            throw new Exception("No current frame set for deinterlacing!");
        }
    }
    
    public void setPastFrames(ArrayList <LPFrame> pastFrames)
    {
        this.pastFrames = pastFrames;
    }
    
    public void setFutureFrames(ArrayList <LPFrame> futureFrames)
    {
        this.futureFrames = futureFrames;
    }
    
    public void setCurrentFrame(LPFrame currentFrame)
    {
        this.currentFrame = currentFrame;
    }
    
    public boolean evenLinesInterlaced()
            throws Exception
    {
        this.checkData();
        return true;
    }
    
    protected LPFrame copyGoodData(boolean topField, LPFrame data) 
    {
        int [][] redData = data.getRedData();
        int [][] greenData = data.getGreenData();
        int [][] blueData = data.getBlueData();
        int [][] grayData = data.getGrayData();
        
        int [][] deinterlacedRedData = new int[redData.length][redData[0].length];
        int [][] deinterlacedGreenData = new int[greenData.length][greenData[0].length];
        int [][] deinterlacedBlueData = new int[blueData.length][blueData[0].length];
        int [][] deinterlacedGrayData = new int[grayData.length][grayData[0].length];
        
           // copy good lines
        if(topField) {
            // even lines are good
            for(int row = 0; row < redData.length; row+=2) {
                    System.arraycopy(redData[row], 0, deinterlacedRedData[row], 0, redData[0].length);
                    System.arraycopy(greenData[row], 0, deinterlacedGreenData[row], 0, greenData[0].length);
                    System.arraycopy(blueData[row], 0, deinterlacedBlueData[row], 0, blueData[0].length);
                    System.arraycopy(grayData[row], 0, deinterlacedGrayData[row], 0, grayData[0].length);
                
            }
        }else {
            // odd lines are good
            for(int row = 1; row < redData.length; row+=2) {
                    System.arraycopy(redData[row], 0, deinterlacedRedData[row], 0, redData[0].length);
                    System.arraycopy(greenData[row], 0, deinterlacedGreenData[row], 0, greenData[0].length);
                    System.arraycopy(blueData[row], 0, deinterlacedBlueData[row], 0, blueData[0].length);
                    System.arraycopy(grayData[row], 0, deinterlacedGrayData[row], 0, grayData[0].length);
                
            }
        }
        LPFrame deinterlacedFrame = new LPFrame();
        deinterlacedFrame.setSignalIdentifier(data.getSignalIdentifier());
        deinterlacedFrame.setFrameIdentifier(data.getFrameIdentifier());
        deinterlacedFrame.setColumnFirstStored(data.isColumnFirstStored());
        
        deinterlacedFrame.setRedData(deinterlacedRedData);
        deinterlacedFrame.setGreenData(deinterlacedGreenData);
        deinterlacedFrame.setBlueData(deinterlacedBlueData);
        deinterlacedFrame.setGrayData(deinterlacedGrayData);
        return deinterlacedFrame;
    }
   
        

}
