/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package deinterlace;

import data.LPFrame;

/**
 *
 * @author shegen
 */
public class NullDeinterlacer extends DeinterlacingFilter
{

    public NullDeinterlacer()
    {
    }

    @Override
    public int getRequiredNumberOfFutureFrames()
    {
        return 0;
    }

    @Override
    public int getRequiredNumberOfPastFrames()
    {
        return 0;
    }

    @Override
    public LPFrame performDeinterlace(boolean topField) throws Exception
    {
        return this.currentFrame;
    }
    
    
}
