/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package deinterlace;

import data.LPFrame;
import java.util.Arrays;

/**
 * Implements the enhanced edge-based line average algorithm.
 * Original paper: A motion-adaptive deinterlacing method using an efficient 
 * spatial and temporal interpolation.
 * 
 * Because I have no access to this paper I reference to the explanation in the paper: 
 * Modified De-interlacing method based on edge direction by Hsiao and Jeng.
 * 
 * 
 * @author shegen
 */
public class ELADeinterlacer extends DeinterlacingFilter
{
    
    @Override
    public int getRequiredNumberOfFutureFrames()
    {
        return 0;
    }

    @Override
    public int getRequiredNumberOfPastFrames()
    {
        return 0;
    }

    @Override
    public LPFrame performDeinterlace(boolean topField) 
            throws Exception
    {
        int [][] redChannel = this.currentFrame.getRedData();
        int [][] greenChannel = this.currentFrame.getGreenData();
        int [][] blueChannel = this.currentFrame.getBlueData();
        int [][] grayChannel = this.currentFrame.getGrayData();
        
        int [][] deinterlacedRedChannel = this.deinterlaceChannel(topField, redChannel);
        int [][] deinterlacedGreenChannel = this.deinterlaceChannel(topField, greenChannel);
        int [][] deinterlacedBlueChannel = this.deinterlaceChannel(topField, blueChannel);
        int [][] deinterlacedGrayChannel = this.deinterlaceChannel(topField, grayChannel);
       
        LPFrame frame = new LPFrame();
        frame.setColumnFirstStored(false);
        frame.setRedData(deinterlacedRedChannel);
        frame.setGreenData(deinterlacedGreenChannel);
        frame.setBlueData(deinterlacedBlueChannel);
        frame.setGrayData(deinterlacedGrayChannel);
        
        return frame;
    }
    
    
    
    private int [][] deinterlaceChannel(boolean topField, int [][] data)
            throws Exception
    {
        int[][] deinterlacedData = new int[data.length][data[0].length];

        // copy all good data to frame
        if (topField == true) { // copy all even lines
            for (int i = 0; i < data.length; i += 2) {
                System.arraycopy(data[i], 0, deinterlacedData[i], 0, data[i].length);
            }
        } else { // copy all odd lines
            for (int i = 1; i < data.length; i += 2) {
                System.arraycopy(data[i], 0, deinterlacedData[i], 0, data[i].length);
            }
        }

        // first index is the row not the column, we use column-first stored false mode
        if (topField == true) 
        { // the first line (0-th) is good, so all even lines are good, deinterlace the odd lines
            
            // fix the last row simply copy the pixel from the second
            System.arraycopy(data[data.length-2], 0, deinterlacedData[data.length-1], 0, data[data.length-1].length);
            
            // first the first two and last two pixels
            for(int colOffset = 0; colOffset < 2; colOffset++) {
                for(int row = 1; row < data.length-1;row+=2) {
                    int interpolatedPixel = (data[row-1][colOffset] + data[row+1][colOffset]) >> 1;
                    deinterlacedData[row][colOffset] = interpolatedPixel;
                    
                    interpolatedPixel = (data[row-1][data[0].length-1-colOffset] + data[row+1][data[0].length-1-colOffset]) >> 1;
                    deinterlacedData[row][data[0].length-1-colOffset] = interpolatedPixel;
                    
                }
            }
            for (int col = 2; col < data[0].length - 2; col++) {
                for (int row = 1; row < data.length - 1; row += 2) { // cannot use the two lines at each border 
                    int interpolatedPixel = this.interpolate(data, row, col);
                    deinterlacedData[row][col] = interpolatedPixel;
                }
            }
        } else { // second line (1-st) is good, so the odd lines are all good
            
            // fix the first row simply copy the pixel from the second
            System.arraycopy(data[1], 0, deinterlacedData[0], 0, data[1].length);
            
             // first the first two and last two pixels
            for(int colOffset = 0; colOffset < 2; colOffset++) {
                for(int row = 2; row < data.length-1;row+=2) {
                    int interpolatedPixel = (data[row-1][colOffset] + data[row+1][colOffset]) >> 1;
                    deinterlacedData[row][colOffset] = interpolatedPixel;
                    
                    interpolatedPixel = (data[row-1][data[0].length-1-colOffset] + data[row+1][data[0].length-1-colOffset]) >> 1;
                    deinterlacedData[row][data[0].length-1-colOffset] = interpolatedPixel;
                }
            }
            for (int col = 2; col < data[0].length - 2; col++) {
                for (int row = 2; row < data.length - 1; row += 2) { // cannot use the two lines at each border
                    int interpolatedPixel = this.interpolate(data, row, col);
                    deinterlacedData[row][col] = interpolatedPixel;
                }
            }
        }
        return deinterlacedData;
    }
    
    
     private int interpolate(int [][] data, int row, int col) 
            throws Exception
    {
        // the smallest difference indicates the highest correlation of pixels
        // these two pixels are used for interpolation
        int e1 = Math.abs(data[row-1][col-1] - data[row+1][col+1]);
        int e2 = Math.abs(data[row-1][col] - data[row+1][col]);
        int e3 = Math.abs(data[row-1][col+1] - data[row+1][col-1]);

        // find minimum direction
        int direction;
        
        if(e1 < e2 && e1 < e3) {
            direction = 1;
        }
        else if(e2 < e1 && e2 < e3) {
            direction = 2;
        }
        else if(e3 < e1 && e3 < e2) {
            direction = 3;
        }else {
            if(e1 == e2) { // e1 and e2 are max
                direction = 1;
            }
            if(e2 == e3) { 
                direction = 2;
            }else {
                direction = 3;
            }
        }
        if(direction == 1) {
            return (data[row-1][col-1] + data[row+1][col+1]) >> 1;
        }
        if(direction == 2) {
            return (data[row-1][col] + data[row+1][col]) >> 1;
        }else {
            return (data[row-1][col+1] + data[row+1][col-1]) >> 1;
        }
    }
}
