/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package deinterlace;

import data.LPFrame;

/**
 * The Bob deinterlacer can be described as spatial line doubling, in which the
 * lines in each field are doubled. The new line generated can either be
 * just a copy of the previous line (scan-line duplication mode) or computed
 * as an average of the lines above and below (scan-line interpolation method).
 * 
 * @author shegen
 */
public class BobDeinterlacer extends DeinterlacingFilter
{

    public static enum Mode
    {
        ScanLineDuplication,
        ScaneLineInterpolation
    };
    
    private Mode mode = Mode.ScanLineDuplication;
    
    public BobDeinterlacer()
    {
    }

    public BobDeinterlacer(Mode mode)
    {
        this.mode = mode;
    }
   
    
    @Override
    public int getRequiredNumberOfFutureFrames()
    {
        return 0;
    }

    @Override
    public int getRequiredNumberOfPastFrames()
    {
        return 0;
    }

    @Override
    public LPFrame performDeinterlace(boolean topField) throws Exception
    {
        switch(this.mode) {
            case ScanLineDuplication:
                return performDeinterlaceDuplication(topField);
            case ScaneLineInterpolation:
                return performDeinterlaceInterpolation(topField);
        }
        throw new Exception("Unknown Mode!");
        
    }
    
               
    private LPFrame performDeinterlaceDuplication(boolean topField)
    {
        int [][] redData = this.currentFrame.getRedData();
        int [][] greenData = this.currentFrame.getGreenData();
        int [][] blueData = this.currentFrame.getBlueData();
        int [][] grayData = this.currentFrame.getGrayData();
        
        int [][] deinterlacedRedData = new int[redData.length][redData[0].length];
        int [][] deinterlacedGreenData = new int[redData.length][redData[0].length];
        int [][] deinterlacedBlueData = new int[redData.length][redData[0].length];
        int [][] deinterlacedGrayData = new int[redData.length][redData[0].length];
        
        int lineWidth = redData[0].length;
        if(topField == true)  
        {
            // the first line is good, therefore replace the 2nd line with the first line
            // and so on, in general replace the 2n+1-th line with the 2n-th line
            for(int lineIndex = 0; lineIndex < redData.length; lineIndex ++) 
            {
                // remember 0 is a good line in topField first mode, therefore the even lines are good
                if(lineIndex % 2 == 0) { 
                    System.arraycopy(redData[lineIndex], 0, deinterlacedRedData[lineIndex], 0, lineWidth);
                    System.arraycopy(greenData[lineIndex], 0, deinterlacedGreenData[lineIndex], 0, lineWidth);
                    System.arraycopy(blueData[lineIndex], 0, deinterlacedBlueData[lineIndex], 0, lineWidth);
                    System.arraycopy(grayData[lineIndex], 0, deinterlacedGrayData[lineIndex], 0, lineWidth);
                }else 
                {
                    // odd lines are bad lines copy the line above
                    System.arraycopy(redData[lineIndex-1], 0, deinterlacedRedData[lineIndex], 0, lineWidth);
                    System.arraycopy(greenData[lineIndex-1], 0, deinterlacedGreenData[lineIndex], 0, lineWidth);
                    System.arraycopy(blueData[lineIndex-1], 0, deinterlacedBlueData[lineIndex], 0, lineWidth);
                    System.arraycopy(grayData[lineIndex-1], 0, deinterlacedGrayData[lineIndex], 0, lineWidth);
                }
            }
        }else 
        {
            // bottomField mode, the first line is bad, therefore ignore the first line
            // in general replace the 2n-th line with the 2n+1-th line
            // the first line is simply left untouched
            System.arraycopy(redData[1], 0, deinterlacedRedData[0], 0, lineWidth);
            System.arraycopy(greenData[1], 0, deinterlacedGreenData[0], 0, lineWidth);
            System.arraycopy(blueData[1], 0, deinterlacedBlueData[0], 0, lineWidth);
            System.arraycopy(grayData[1], 0, deinterlacedGrayData[0], 0, lineWidth);
            
            for(int lineIndex = 1; lineIndex < redData.length; lineIndex++) 
            {
                // we start with 1, the odd lines are good
                if(lineIndex % 2 == 1) {
                    System.arraycopy(redData[lineIndex], 0, deinterlacedRedData[lineIndex], 0, lineWidth);
                    System.arraycopy(greenData[lineIndex], 0, deinterlacedGreenData[lineIndex], 0, lineWidth);
                    System.arraycopy(blueData[lineIndex], 0, deinterlacedBlueData[lineIndex], 0, lineWidth);
                    System.arraycopy(grayData[lineIndex], 0, deinterlacedGrayData[lineIndex], 0, lineWidth);
                }else {
                    // even lines are bad, copy the line above
                    System.arraycopy(redData[lineIndex-1], 0, deinterlacedRedData[lineIndex], 0, lineWidth);
                    System.arraycopy(greenData[lineIndex-1], 0, deinterlacedGreenData[lineIndex], 0, lineWidth);
                    System.arraycopy(blueData[lineIndex-1], 0, deinterlacedBlueData[lineIndex], 0, lineWidth);
                    System.arraycopy(grayData[lineIndex-1], 0, deinterlacedGrayData[lineIndex], 0, lineWidth);
                }
            }
        }
        LPFrame deinterlacedFrame = new LPFrame();
        deinterlacedFrame.setColumnFirstStored(false);
        deinterlacedFrame.setRedData(deinterlacedRedData);
        deinterlacedFrame.setGreenData(deinterlacedGreenData);
        deinterlacedFrame.setBlueData(deinterlacedBlueData);
        deinterlacedFrame.setGrayData(deinterlacedGrayData);
        return deinterlacedFrame;
    }
    
    private LPFrame performDeinterlaceInterpolation(boolean topField)
    {
        int [][] redData = this.currentFrame.getRedData();
        int [][] greenData = this.currentFrame.getGreenData();
        int [][] blueData = this.currentFrame.getBlueData();
        int [][] grayData = this.currentFrame.getGrayData();
        
        int [][] deinterlacedRedData = new int[redData.length][redData[0].length];
        int [][] deinterlacedGreenData = new int[redData.length][redData[0].length];
        int [][] deinterlacedBlueData = new int[redData.length][redData[0].length];
        int [][] deinterlacedGrayData = new int[redData.length][redData[0].length];
        
        int lineWidth = redData[0].length;
        if(topField == true)  
        {    
             for(int lineIndex = 0; lineIndex < redData.length; lineIndex ++) 
             {
                 // if it is an even field it is a good field, simply copy
                 if(lineIndex % 2 == 0) {
                    System.arraycopy(redData[lineIndex], 0, deinterlacedRedData[lineIndex], 0, lineWidth);
                    System.arraycopy(greenData[lineIndex], 0, deinterlacedGreenData[lineIndex], 0, lineWidth);
                    System.arraycopy(blueData[lineIndex], 0, deinterlacedBlueData[lineIndex], 0, lineWidth);
                    System.arraycopy(grayData[lineIndex], 0, deinterlacedGrayData[lineIndex], 0, lineWidth);
                 }else {
                     if(lineIndex == redData.length-1) { // cannot interpolate the last line simply copy
                         System.arraycopy(redData[lineIndex], 0, deinterlacedRedData[lineIndex], 0, lineWidth);
                         System.arraycopy(greenData[lineIndex], 0, deinterlacedGreenData[lineIndex], 0, lineWidth);
                         System.arraycopy(blueData[lineIndex], 0, deinterlacedBlueData[lineIndex], 0, lineWidth);
                         System.arraycopy(grayData[lineIndex], 0, deinterlacedGrayData[lineIndex], 0, lineWidth);
                         continue;
                     }
                     // a bad line, interpolate all pixels of this line using the line above and below
                     int [] interpolatedRed = this.interpolateLine(redData[lineIndex-1], redData[lineIndex+1]);
                     int [] interpolatedGreen = this.interpolateLine(greenData[lineIndex-1], greenData[lineIndex+1]);
                     int [] interpolatedBlue = this.interpolateLine(blueData[lineIndex-1], blueData[lineIndex+1]);
                     int [] interpolatedGray = this.interpolateLine(grayData[lineIndex-1], grayData[lineIndex+1]);
                     
                     // copy interpolated lines
                     System.arraycopy(interpolatedRed, 0, deinterlacedRedData[lineIndex], 0, lineWidth);
                     System.arraycopy(interpolatedGreen, 0, deinterlacedGreenData[lineIndex], 0, lineWidth);
                     System.arraycopy(interpolatedBlue, 0, deinterlacedBlueData[lineIndex], 0, lineWidth);
                     System.arraycopy(interpolatedGray, 0, deinterlacedGrayData[lineIndex], 0, lineWidth);
                 }
             }
        }else { // bottom field
            for(int lineIndex = 0; lineIndex < redData.length; lineIndex ++) 
             {
                 // if it is an odd field it is a good field, simply copy
                 if(lineIndex % 2 == 1) {
                    System.arraycopy(redData[lineIndex], 0, deinterlacedRedData[lineIndex], 0, lineWidth);
                    System.arraycopy(greenData[lineIndex], 0, deinterlacedGreenData[lineIndex], 0, lineWidth);
                    System.arraycopy(blueData[lineIndex], 0, deinterlacedBlueData[lineIndex], 0, lineWidth);
                    System.arraycopy(grayData[lineIndex], 0, deinterlacedGrayData[lineIndex], 0, lineWidth);
                 }else {
                     // even fields are bad, interpolate
                     // cannot interpolate the last and the first line simply copy
                     if(lineIndex == redData.length-1 || lineIndex == 0) { 
                         System.arraycopy(redData[lineIndex], 0, deinterlacedRedData[lineIndex], 0, lineWidth);
                         System.arraycopy(greenData[lineIndex], 0, deinterlacedGreenData[lineIndex], 0, lineWidth);
                         System.arraycopy(blueData[lineIndex], 0, deinterlacedBlueData[lineIndex], 0, lineWidth);
                         System.arraycopy(grayData[lineIndex], 0, deinterlacedGrayData[lineIndex], 0, lineWidth);
                         continue;
                     }
                     // a bad line, interpolate all pixels of this line using the line above and below
                     int [] interpolatedRed = this.interpolateLine(redData[lineIndex-1], redData[lineIndex+1]);
                     int [] interpolatedGreen = this.interpolateLine(greenData[lineIndex-1], greenData[lineIndex+1]);
                     int [] interpolatedBlue = this.interpolateLine(blueData[lineIndex-1], blueData[lineIndex+1]);
                     int [] interpolatedGray = this.interpolateLine(grayData[lineIndex-1], grayData[lineIndex+1]);
                     
                     // copy interpolated lines
                     System.arraycopy(interpolatedRed, 0, deinterlacedRedData[lineIndex], 0, lineWidth);
                     System.arraycopy(interpolatedGreen, 0, deinterlacedGreenData[lineIndex], 0, lineWidth);
                     System.arraycopy(interpolatedBlue, 0, deinterlacedBlueData[lineIndex], 0, lineWidth);
                     System.arraycopy(interpolatedGray, 0, deinterlacedGrayData[lineIndex], 0, lineWidth);
                 }
             }
        }
        LPFrame deinterlacedFrame = new LPFrame();
        deinterlacedFrame.setColumnFirstStored(false);
        deinterlacedFrame.setRedData(deinterlacedRedData);
        deinterlacedFrame.setGreenData(deinterlacedGreenData);
        deinterlacedFrame.setBlueData(deinterlacedBlueData);
        deinterlacedFrame.setGrayData(deinterlacedGrayData);
        return deinterlacedFrame;
    }
    
    private int [] interpolateLine(int [] above, int [] below) 
    {
        int [] line = new int[above.length];
        for(int i = 0; i < line.length; i++) {
            line[i] = (int)((above[i] + below[i]) >> 1);
        }
        return line;
    }
    
}
