/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package deinterlace.motionCompensated;

import data.LPFrame;
import debug.DebugMain;
import deinterlace.DeinterlacingFilter;
import deinterlace.motionestimation.FiveFieldMotionEstimator;
import deinterlace.motionestimation.FiveFieldMotionEstimatorBugged;
import deinterlace.motionestimation.FiveFieldMotionEstimatorNew;
import deinterlace.motionestimation.MotionEstimator;
import deinterlace.motionestimation.MotionVector;
import java.util.HashMap;
import jceliac.gui.ImageViewerFrame;
import jceliac.tools.math.MathTools;

/**
 * Implements the method of Mohammadi, Langlois and Savaria published in
 * "A Five-Field Motion Compensated Deinterlacing Method Based on Vertical Motion".
 * 
 * @author shegen
 */
public class FiveFieldMCDeinterlacer extends DeinterlacingFilter
{
    private HashMap <Integer, MotionVector>  motionVectorsRed;
    private HashMap <Integer, MotionVector>  motionVectorsGreen;
    private HashMap <Integer, MotionVector>  motionVectorsBlue;
    
    private static final double ALPHA = 1.8; // XXXX
    private int numberOFImprovedPixels = 0;
    
    // order must be next,next2
    @Override
    public int getRequiredNumberOfFutureFrames()
    {
        return 2;
    }

    // order must be prev2, prev
    @Override
    public int getRequiredNumberOfPastFrames()
    {
        return 2;
    }

    @Override
    public LPFrame performDeinterlace(boolean topField) throws Exception
    {
        this.numberOFImprovedPixels = 0;
        FiveFieldMotionEstimatorNew estimatorRed = new FiveFieldMotionEstimatorNew(topField, this.pastFrames.get(0).getRedData(), 
                                                                                  this.pastFrames.get(1).getRedData(), 
                                                                                  this.currentFrame.getRedData(), 
                                                                                  this.futureFrames.get(0).getRedData(),
                                                                                  this.futureFrames.get(1).getRedData());
        
        FiveFieldMotionEstimatorNew estimatorBlue = new FiveFieldMotionEstimatorNew(topField, this.pastFrames.get(0).getBlueData(), 
                                                                                  this.pastFrames.get(1).getBlueData(), 
                                                                                  this.currentFrame.getBlueData(), 
                                                                                  this.futureFrames.get(0).getBlueData(),
                                                                                  this.futureFrames.get(1).getBlueData());
        
        FiveFieldMotionEstimatorNew estimatorGreen = new FiveFieldMotionEstimatorNew(topField, this.pastFrames.get(0).getGreenData(), 
                                                                                  this.pastFrames.get(1).getGreenData(), 
                                                                                  this.currentFrame.getGreenData(), 
                                                                                  this.futureFrames.get(0).getGreenData(),
                                                                                  this.futureFrames.get(1).getGreenData());
       
        this.motionVectorsRed = estimatorRed.computeMotionVectors();
        this.motionVectorsGreen = estimatorGreen.computeMotionVectors();
        this.motionVectorsBlue = estimatorBlue.computeMotionVectors();

        int [][] currentRedChannel = this.currentFrame.getRedData();
        int [][] currentGreenChannel = this.currentFrame.getGreenData();
        int [][] currentBlueChannel = this.currentFrame.getBlueData();
        int [][] currentGrayChannel = this.currentFrame.getGrayData();
        
        int [][] prevRedChannel = this.pastFrames.get(1).getRedData();
        int [][] prevGreenChannel = this.pastFrames.get(1).getGreenData();
        int [][] prevBlueChannel = this.pastFrames.get(1).getBlueData();
        int [][] prevGrayChannel = this.pastFrames.get(1).getGrayData();
        
        int [][] prev2RedChannel = this.pastFrames.get(0).getRedData();
        int [][] prev2GreenChannel = this.pastFrames.get(0).getGreenData();
        int [][] prev2BlueChannel = this.pastFrames.get(0).getBlueData();
        int [][] prev2GrayChannel = this.pastFrames.get(0).getGrayData();
        
        int [][] nextRedChannel = this.futureFrames.get(0).getRedData();
        int [][] nextGreenChannel = this.futureFrames.get(0).getGreenData();
        int [][] nextBlueChannel = this.futureFrames.get(0).getBlueData();
        int [][] nextGrayChannel = this.futureFrames.get(0).getGrayData();
        
        int [][] next2RedChannel = this.futureFrames.get(1).getRedData();
        int [][] next2GreenChannel = this.futureFrames.get(1).getGreenData();
        int [][] next2BlueChannel = this.futureFrames.get(1).getBlueData();
        int [][] next2GrayChannel = this.futureFrames.get(1).getGrayData();
        
        int [][] deinterlacedRedChannel = this.deinterlaceChannel(topField, this.motionVectorsRed, currentRedChannel, prev2RedChannel, prevRedChannel, nextRedChannel, next2RedChannel);
        int [][] deinterlacedGreenChannel = this.deinterlaceChannel(topField, this.motionVectorsGreen, currentGreenChannel, prev2GreenChannel, prevGreenChannel, nextGreenChannel, next2GreenChannel);
        int [][] deinterlacedBlueChannel = this.deinterlaceChannel(topField, this.motionVectorsBlue, currentBlueChannel, prev2BlueChannel, prevBlueChannel, nextBlueChannel, next2BlueChannel);

        LPFrame frame = new LPFrame();
        frame.setColumnFirstStored(false);
        frame.setRedData(deinterlacedRedChannel);
        frame.setGreenData(deinterlacedGreenChannel);
        frame.setBlueData(deinterlacedBlueChannel);

        
        //System.out.println("Improved Pixels: "+this.numberOFImprovedPixels);
        return frame;
        
    }
    
    // return either the previous or next field depending on m
    private int [][] getField(int m, int [][] prevData, int [][] nextData) 
    {
        if(m == -2) {
            return prevData;
        }
        if(m == 2) {
            return nextData;
        }
        return null;
    } 
    
    
    private int deinterlace(int col, int row, int [][] cur, int [][] prev2, int [][] prev, int [][] next, int[][] next2, MotionVector vector)
    {
        int pixelValue;
        int [][] field = null;

        switch(vector.getM()) {
            case -1:
                field = prev;
                break;
            case 1:
                field = next;
                break;
            case -2:
                field = prev2;
                break;
            case 2:
                field = next2;
                break;
        }
        if(vector.getM() == 1 || vector.getM() == -1) {
            if(vector.getIComponent() % 2 != 0) { // have to use standard blending
                return (cur[row-1][col] + cur[row+1][col]) >> 1;
            }
        }else if(vector.getM() == 2 || vector.getM() == -2) {
            if(vector.getIComponent() % 2 == 0) {
                return (cur[row-1][col] + cur[row+1][col]) >> 1;
            }
        }
        pixelValue = field[row+vector.getIComponent()][col+vector.getJComponent()];
      //  pixelValue = 0;
        if(pixelValue == -1) {
            int blergh = 0;
        }
        return pixelValue;
    }
    
    
    
    private int deinterlaceSameParityField(int col, int row, int [][] prev, int [][] next,
                                         MotionVector vector)
    {
          int pixelValue;
          int [][] field = null;
          
          
          // the paper states that if the motion vector is a multiple of 4 in a same parity field
          // we can use the half of the motion vector in the opposite parity field and gain extra
          // information
          switch(vector.getM()) {
              case -2:
                  field = prev;
                  break;
              case 2:
                  field = next;
                  break;
              default:
                  System.out.println("Wrong field with same parity!");
          }
          pixelValue = field[row+vector.getIComponent()/2][col+vector.getJComponent()/2];
          if(pixelValue == -1) {
              return field[row-1][col] + field[row+1][col] >> 1;
          }
          if(pixelValue != 0) {
              this.numberOFImprovedPixels++;
          }
          return pixelValue;
    }
    
    
    
    private int [][] deinterlaceChannel(boolean topField, HashMap <Integer, MotionVector> motionVectors, int [][] data, 
                                        int [][] prev2data, int [][] prevdata, int [][] nextdata, int [][] next2data)
            throws Exception
    {
        int[][] deinterlacedData = new int[data.length][data[0].length];

        // copy all good data to frame
        if (topField == true) { // copy all even lines
            for (int i = 0; i < data.length; i += 2) {
                System.arraycopy(data[i], 0, deinterlacedData[i], 0, data[i].length);
            }
        } else { // copy all odd lines
            for (int i = 1; i < data.length; i += 2) {
                System.arraycopy(data[i], 0, deinterlacedData[i], 0, data[i].length);
            }
        }

        if (topField == true) { // the first line (0-th) is good, so all even lines are good, deinterlace the odd lines
            for (int col = 0; col < data[0].length; col++) {
                for (int row = 1; row < data.length-1; row += 2) { 
                    
                    int pixelValue;
                    int [] coords = MotionEstimator.getMacroBlockCoordsForPixel(topField, row,col, FiveFieldMotionEstimatorNew.BLOCKSIZE);
                    Integer key = MathTools.cantorTupel(coords[0], coords[1]);
                    MotionVector vector = motionVectors.get(key);
                    
                    // this can only happen if the center pixel is beyond the last macro block because
                    // the image size is not divisible by the blocksize, in this case use line averaging
                    if(vector == null) { 
                        pixelValue = (data[row+1][col] + data[row-1][col])>>1;
                        continue;
                    }else {
                        if(vector.isSameParity() == true) {
                            if(vector.getIComponent() % 4 != 0) { // use opposite parity field
                                  pixelValue = this.deinterlace(col, row, data, prev2data, prevdata, nextdata, next2data, vector);
                            }else {
                                if(vector.getIComponent() != 0 && vector.getOMBHalfMVError() <= ALPHA * vector.getMAD()) {
                                    pixelValue = this.deinterlaceSameParityField(col, row, prevdata, nextdata, vector);
                                  //  pixelValue = 255;
                                }else {
                                    pixelValue = this.deinterlace(col, row,data, prev2data, prevdata, nextdata, next2data, vector);
                                }
                            }
                        }else {
                            pixelValue = this.deinterlace(col, row,data, prev2data, prevdata, nextdata, next2data, vector);
                        }
                    }
                    if(pixelValue == -1) {
                        int blub = 0;
                        pixelValue = this.deinterlace(col, row,data, prev2data, prevdata, nextdata, next2data, vector);
                        pixelValue = this.deinterlaceSameParityField(col, row, prevdata, nextdata, vector);
                    }
                    deinterlacedData[row][col] = pixelValue;
                }
            }
        } else { // second line (1-st) is good, so the odd lines are all good
            for (int col = 0; col < data[0].length; col++) {
                for (int row = 2; row < data.length-1; row += 2) { 
                    
                    
                    int pixelValue;
                    int [] coords = MotionEstimator.getMacroBlockCoordsForPixel(topField, row,col, FiveFieldMotionEstimatorNew.BLOCKSIZE);
                    // fix for the bottom field, we shift the field up so the original vertical value is +1
                    coords[0] ++;
                    
                    Integer key = MathTools.cantorTupel(coords[0], coords[1]);
                    MotionVector vector = motionVectors.get(key);
                    
                    // this can only happen if the center pixel is beyond the last macro block because
                    // the image size is not divisible by the blocksize, in this case use line averaging
                    if(vector == null) { 
                        pixelValue = (data[row+1][col] + data[row-1][col])>>1;
                        continue;
                    }else {
                        if(vector.isSameParity() == true) {
                            if(vector.getIComponent() % 4 != 0) 
                            { // use opposite parity field
                               pixelValue = this.deinterlace(col, row, data,prev2data, prevdata, nextdata, next2data, vector);
                            }else {
                                if(vector.getIComponent() != 0 && vector.getOMBHalfMVError() <= ALPHA * vector.getMAD()) {
                                    pixelValue = this.deinterlaceSameParityField(col, row, prevdata, nextdata, vector);
                                }else {
                                    pixelValue = this.deinterlace(col, row,data, prev2data, prevdata, nextdata, next2data, vector);
                                }
                            }
                        }else {
                            pixelValue = this.deinterlace(col, row,data, prev2data, prevdata, nextdata, next2data, vector);
                        }
                    }
                    if(pixelValue == -1) 
                    {
                        int blub = 0;
                         pixelValue = this.deinterlace(col, row,data, prev2data, prevdata, nextdata, next2data, vector);
                        pixelValue = this.deinterlaceSameParityField(col, row, prevdata, nextdata, vector);
                    }
                    deinterlacedData[row][col] = pixelValue;
                }
            }
        }
        return deinterlacedData;
    }
    
}
