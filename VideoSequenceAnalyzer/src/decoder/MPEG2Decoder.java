/*
 *  JCeliac Copyright (C) 2010-2011 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package decoder;


import java.awt.image.BufferedImage;
import com.xuggle.xuggler.Global;
import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IPacket;
import com.xuggle.xuggler.IPixelFormat;
import com.xuggle.xuggler.IStream;
import com.xuggle.xuggler.IStreamCoder;
import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.IVideoPicture;
import com.xuggle.xuggler.IVideoResampler;
import com.xuggle.xuggler.Utils;
import java.awt.Dimension;
import java.io.*;
import java.util.Timer;
import java.util.TimerTask;
import com.xuggle.xuggler.io.IURLProtocolHandler;
import com.xuggle.xuggler.IRational;
import java.awt.image.BufferedImage;
import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.IError;
import com.xuggle.xuggler.IPacket;
import com.xuggle.xuggler.IStream;
import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IStreamCoder;
import com.xuggle.xuggler.IVideoPicture;
import com.xuggle.xuggler.video.IConverter;
import com.xuggle.xuggler.video.ConverterFactory;
import com.xuggle.xuggler.IRational;
import java.util.*;
import com.xuggle.xuggler.IIndexEntry;
import com.xuggle.xuggler.*;
import java.awt.color.*;


/**
 *  Copied from MediaReader.java of xuggler.
 *  Adapted and extended to our needs by shegen
 * @author shegen
 */
public class MPEG2Decoder
{

    private final String url;
    private final IContainer container;
    private boolean mCloseOnEofOnly = false;
    private int videoStreamID; 
   
    
    public MPEG2Decoder(String url)
    {
        this.url = url;
        this.container = IContainer.make().copyReference();
    }

    public void init()
    {
        if (!IVideoResampler.isSupported(
            IVideoResampler.Feature.FEATURE_COLORSPACECONVERSION))
            throw new RuntimeException("you must install the GPL version" +
      		" of Xuggler (with IVideoResampler support) for " +
      		"this demo to work");
    }

    private IStreamCoder getVideoStreamCoder() throws Exception
    {
        // test valid stream index
        int numStreams = getContainer().getNumStreams();
        numStreams = getContainer().getNumStreams();
        for (int streamIndex = 0; streamIndex < numStreams; streamIndex++) {
            IStream stream = getContainer().getStream(streamIndex);
            IStreamCoder coder = stream.getStreamCoder();
            ICodec.Type type = coder.getCodecType();

            if (type == ICodec.Type.CODEC_TYPE_VIDEO) {
                if (coder.isOpen() == false) {
                    int ret = coder.open();
                    if (ret < 0) {
                        throw new Exception();
                    } else {
                        this.videoStreamID = streamIndex;
                        return coder;
                    }
                }else {
                    this.videoStreamID = streamIndex;
                    return coder;
                }
            }
        }
        throw new Exception();
    }


    protected IVideoPicture decodeNextPicture()
            throws Exception, PacketReadErrorException, VideoFinishedException
    {
        // if the container is not yet been opend, open it
        if (!isOpen()) {
            open();
        }
        IPacket packet = IPacket.make();
        IStreamCoder videoCoder = this.getVideoStreamCoder();
        IVideoResampler resampler = null;

        int rv;
        while ((rv = getContainer().readNextPacket(packet)) >= 0) {

            if (packet.getStreamIndex() == this.videoStreamID) {
                IVideoPicture picture = IVideoPicture.make(videoCoder.getPixelType(),
                        videoCoder.getWidth(),
                        videoCoder.getHeight());
                int offset = 0;
                while (offset < packet.getSize()) {
                    int r = videoCoder.decodeVideo(picture, packet, offset);
                    if (r < 0) {
                        throw new PacketReadErrorException(); // this can happen
                    }
                    offset += r;
                    if (picture.isComplete()) { 
                        
                        // Convert to correct format
                        if (videoCoder.getPixelType() != IPixelFormat.Type.BGR24) {
                            // if this stream is not in BGR24, we're going to need to
                            // convert it.  The VideoResampler does that for us.
                            resampler = IVideoResampler.make(videoCoder.getWidth(),
                                    videoCoder.getHeight(), IPixelFormat.Type.BGR24,
                                    videoCoder.getWidth(), videoCoder.getHeight(), videoCoder.getPixelType());
                            if (resampler == null) {
                                throw new RuntimeException("could not create color space "
                                        + "resampler for: " + this.url);
                            }

                            IVideoPicture newPic = IVideoPicture.make(resampler.getOutputPixelFormat(),
                                    picture.getWidth(), picture.getHeight());
                            if (resampler.resample(newPic, picture) < 0) {
                                throw new RuntimeException("could not resample video from: "
                                        + this.url);
                            }

                            if (newPic.getPixelType() != IPixelFormat.Type.BGR24) {
                                throw new RuntimeException("could not decode video"
                                        + " as BGR 24 bit data in: " + this.url);
                            }
                        }

                        return picture;

                    }
                }
            }
        }
        if (rv < 0) {
            IError error = IError.make(rv);
            // if this is an end of file, or unknow, call close

            if (!mCloseOnEofOnly || IError.Type.ERROR_EOF == error.getType()) {
                videoCoder.close();
                throw new VideoFinishedException();
            }
            throw new VideoFinishedException();
        }
        throw new RuntimeException("Video decoding failed!");
    }

    private IVideoPicture decodeVideo(IStreamCoder videoCoder, IPacket packet)
            throws Exception
    {
        // create a blank video picture
        IVideoPicture picture = IVideoPicture.make(videoCoder.getPixelType(),
                videoCoder.getWidth(), videoCoder.getHeight());

        // decode the packet into the video picture
        int rv = videoCoder.decodeVideo(picture, packet, 0);
        if (rv < 0) // we are done
        {
            throw new Exception();
        }
        // if this is a complete picture, dispatch the picture
        if (picture.isComplete()) {
            return picture;
        }
        return null;
    }

    protected BufferedImage convertPicture(IVideoPicture picture)
    {
        if (picture == null) {
            return null;
        }
        ConverterFactory.Type convType = ConverterFactory.findRegisteredConverter(ConverterFactory.XUGGLER_BGR_24);
        if (convType == null) {
            throw new UnsupportedOperationException("No converter found!");
        }
        IConverter converter = ConverterFactory.createConverter(convType.getDescriptor(), picture);
        BufferedImage image = converter.toImage(picture);
        return image;
    }

    /**
     * Get the underlying media {@link IContainer} that the {@link IMediaCoder} is
     * reading from or writing to.  The returned {@link IContainer} can
     * be further interrogated for media stream details.
     *
     * @return the media container.
     */
    public final IContainer getContainer()
    {
        return container == null ? null : container.copyReference();
    }

    public boolean isOpen()
    {
        return getContainer().isOpened();
    }

    public void open()
    {
        if (getContainer().open(this.url, IContainer.Type.READ, null) < 0) {
            throw new RuntimeException("could not open: " + this.url);
        }

    }

    public void close()
    {
        if (getContainer().close() < 0) {
            throw new RuntimeException("error failed close IContainer ");
        }
    }
}
  
  
  
  