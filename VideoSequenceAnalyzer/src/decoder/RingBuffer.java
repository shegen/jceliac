/*
 *  JCeliac Copyright (C) 2010-2011 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package decoder;

import java.util.*;

/**
 *
 * @author shegen
 */
public class RingBuffer <T>
{
    private ArrayList <T> buffer = new ArrayList <T>();
    public int capacity = 12;

    public RingBuffer()
    {
        this.capacity = 12;
    }

    public RingBuffer(int capacity)
    {
        this.capacity = capacity;
    }
    

    public void add(T data) {
        if(buffer.size() == capacity) {
            buffer.remove(0);
            buffer.add(data);
        }else {
            buffer.add(data);
        }
    }
    
    public ArrayList <T> getAll() {
        return buffer;
    }
    
    public boolean isFilled() {
        return this.buffer.size() == this.capacity;
    }
    
    public void flush() {
        this.buffer = new ArrayList <T>();
    }
}
