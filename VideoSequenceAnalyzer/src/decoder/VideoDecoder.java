/*
 *  JCeliac Copyright (C) 2010-2011 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package decoder;

import gui.*;

import java.awt.image.BufferedImage;
import com.xuggle.xuggler.Global;
import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IPacket;
import com.xuggle.xuggler.IPixelFormat;
import com.xuggle.xuggler.IStream;
import com.xuggle.xuggler.IStreamCoder;
import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.IVideoPicture;
import com.xuggle.xuggler.IVideoResampler;
import com.xuggle.xuggler.Utils;
import java.awt.Dimension;
import java.io.*;
import java.util.Timer;
import java.util.TimerTask;
import com.xuggle.xuggler.io.IURLProtocolHandler;
import com.xuggle.xuggler.IRational;
import java.awt.image.BufferedImage;
import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.IError;
import com.xuggle.xuggler.IPacket;
import com.xuggle.xuggler.IStream;
import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IStreamCoder;
import com.xuggle.xuggler.IVideoPicture;
import com.xuggle.xuggler.video.IConverter;
import com.xuggle.xuggler.video.ConverterFactory;
import com.xuggle.xuggler.IRational;
import java.util.*;
import com.xuggle.xuggler.IIndexEntry;
import com.xuggle.xuggler.*;
import data.LPFrame;
import deinterlace.DeinterlacingFilter;
import java.awt.color.*;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import jceliac.tools.data.Frame;



/**
 * Takes a media container, finds the first video stream,
 * decodes that stream, and then displays the video frames,
 * at the frame-rate specified by the container, on a 
 * window.
 * @author aclarke
 *
 */
public class VideoDecoder
{

   private String filename;
   private static VideoDecoder decoder;
   private MPEG2Decoder mpeg2decoder;
   private ArrayList <LPFrame> decodedFrames;
   
   public VideoDecoder(File video) 
           throws Exception 
   {
       this.filename = video.getCanonicalPath();
       this.decodedFrames = new ArrayList <LPFrame>();
       this.mpeg2decoder = new MPEG2Decoder(video.getAbsolutePath());
       this.mpeg2decoder.init();
       
   }
   
    public void decodeEntireVideo()
    {
        try {
            
            while (true) {
                IVideoPicture picture = null;
                try {
                    picture = this.mpeg2decoder.decodeNextPicture();
                } catch (PacketReadErrorException e) {
                    // ignore
                }
                if (picture != null) {
                    BufferedImage image = this.mpeg2decoder.convertPicture(picture);
                    LPFrame imageFrame = LPFrame.convertBufferedImageToFrameOptimized(image, false);
                    this.decodedFrames.add(imageFrame);
                }
            }
        } catch (VideoFinishedException e) {
            // video is finished
            this.mpeg2decoder.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void deinterlaceFrames(DeinterlacingFilter deinterlacer)
    {
        int neededFutureFrames = deinterlacer.getRequiredNumberOfFutureFrames();
        int neededPastFrames = deinterlacer.getRequiredNumberOfPastFrames();
        int frameCount = 0 ;
        boolean topField = true;
        
        // convert BufferedImage to Frames
        ArrayList <LPFrame> deinterlacedFrames = new ArrayList <LPFrame>();
        // skip until we have enough past frames 
        for(int curIndex = neededPastFrames; curIndex < this.decodedFrames.size()-neededFutureFrames; curIndex++) 
        {
            deinterlacer.setCurrentFrame(this.decodedFrames.get(curIndex));
            deinterlacer.setPastFrames(this.extractSublist(this.decodedFrames, curIndex-neededPastFrames, curIndex-1));
            deinterlacer.setFutureFrames(this.extractSublist(this.decodedFrames,curIndex+1, curIndex+neededFutureFrames));
            try {
                // assume top field first
                LPFrame deinterlacedFrame = deinterlacer.deinterlace(topField);
                deinterlacedFrames.add(deinterlacedFrame);
                System.out.println("Frame: "+curIndex);
                topField = !topField;
            }catch(Exception e) {
                e.printStackTrace();
            }finally {
                frameCount++;
            }
        }
        this.decodedFrames = deinterlacedFrames;
    }
    
    public ArrayList <LPFrame> extractSublist(ArrayList <LPFrame> list, int start, int end) 
    {
        ArrayList <LPFrame> copy = new ArrayList <LPFrame>();
        for(int i = start; i <= end; i++) {
            copy.add(list.get(i));
        }
        return copy;
    }
    
    public BufferedImage getFrameAsImage(int index) 
    {
        return this.decodedFrames.get(index).toImage();
    }
   
    
    public LPFrame getFrame(int index) {
        if(this.decodedFrames == null) {
            return this.decodedFrames.get(index);
        }
        return this.decodedFrames.get(index);
    }
    
    public ArrayList <LPFrame> getFrames() {
        return this.decodedFrames;
    }

    public int getFrameCount() 
    {
        return this.decodedFrames.size();
    }


 
}