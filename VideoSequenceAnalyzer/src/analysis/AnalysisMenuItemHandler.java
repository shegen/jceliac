/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package analysis;

import java.awt.event.InputMethodListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 *
 * @author shegen
 */
public class AnalysisMenuItemHandler implements ItemListener
{

    private HashMap <JCheckBoxMenuItem,AnalyzerFactory.AnalysisMethodType> methods;
    private JMenu menu;
    private boolean activatedItemStateListener = true;
    private AnalyzerFactory.AnalysisMethodType selectedAnalysisMethod;
    private AnalysisHandler handler;
    
    public AnalysisMenuItemHandler(JMenu menu, AnalysisHandler handler)
    {
        this.menu = menu;
        this.methods = new HashMap <JCheckBoxMenuItem,AnalyzerFactory.AnalysisMethodType>();
        this.handler = handler;
    }
    
    public void registerMethod(AnalyzerFactory.AnalysisMethodType methodType, JCheckBoxMenuItem menuItem)
            throws Exception
    {
        if(this.methods.containsValue(methodType)) {
            throw new Exception("Analyzation Method Type Registered Multiple Times!");
        }
        this.methods.put(menuItem,methodType);
        this.menu.add(menuItem);
        menuItem.addItemListener(this);
        
    }
    
    public void checkMethod(JCheckBoxMenuItem menuItem) 
    {
        for(int i = 0; i < this.menu.getItemCount(); i++) {
            JCheckBoxMenuItem tmpItem = (JCheckBoxMenuItem)this.menu.getItem(i);
            tmpItem.setSelected(false);
        }
        menuItem.setSelected(true);   
        this.activatedItemStateListener = true;
        
    }

    @Override
    public void itemStateChanged(ItemEvent ie)
    {
        if(this.activatedItemStateListener == false) {
            return;
        }
        else {
            this.activatedItemStateListener = false;
            JCheckBoxMenuItem menuItem = (JCheckBoxMenuItem)ie.getItem();
            this.checkMethod(menuItem);
            this.selectedAnalysisMethod = this.methods.get(menuItem);
            this.handler.analysisMethodChanged(selectedAnalysisMethod);
        }
    }
    
    public AnalyzerFactory.AnalysisMethodType getSelectedMethod()
    {
        return this.selectedAnalysisMethod;
    }
    
    public boolean isMethodSelected()
    {
        return this.selectedAnalysisMethod != null;
    }
    
    
}
