/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package analysis;

import data.LPFrame;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;
import org.jfree.data.general.AbstractDataset;

/**
 *
 * @author shegen
 */
public class AnalysisHandler
{
    
    private ArrayList <LPFrame> frames;
    protected AnalysisMethod [] scheduledMethods;
    public static final int NUMBER_OF_THREADS = 8;
    private ArrayList <AnalysisMethod> finishedMethods;
    private HashMap <Integer, AbstractDataset> collectedData;
    
    private boolean analysisRunning = false;
    private boolean cancelAnalysis = false;
    
    public AnalysisHandler()
    {
        this.scheduledMethods = new AnalysisMethod[NUMBER_OF_THREADS];
    }
     
    // i think this can only be called once, because the handler is running 
    // inside the GUI thread. therefore we do not need to cancel an analysis actually
    
    public synchronized void analysisMethodChanged(AnalyzerFactory.AnalysisMethodType methodType) 
    {
        try {
            AnalysisMethod analysisMethod = AnalyzerFactory.createAnalysisMethod(methodType, this);
            while(this.analysisRunning == true)  { // wait for the other analysis to finish
                this.wait();
            }
            this.runAnalysis(methodType);
        }catch(Exception e) {
            e.printStackTrace();
        }
        
    }
    
    public void setFrames(ArrayList <LPFrame> frames)
    {
        this.frames = frames;
    }
    
    public synchronized void notifyAnalysisFinished(AnalysisMethod analysisMethod) 
    {
        this.finishedMethods.add(analysisMethod);
        this.notify(); // wake up the waiting scheduler
    }
    
    public synchronized void runAnalysis(AnalyzerFactory.AnalysisMethodType methodType) 
    {
        int nextFrameIndex = 0;
        this.analysisRunning = true;
        this.finishedMethods = new ArrayList <AnalysisMethod>();
        
        try {
            AnalysisMethod method = AnalyzerFactory.createAnalysisMethod(methodType, this);
            if(this.frames == null) {
                return;
            }
            while(nextFrameIndex < this.frames.size())  
            {
                System.out.println("Scheduling for Frame: "+nextFrameIndex);
                this.scheduleJob(method, nextFrameIndex);
                nextFrameIndex++;
            }
            // all analysis is scheduled, wait for final threads to finish
            while(threadsFinished() == false) {
                this.wait();
            }
            this.analysisRunning = false;
            this.collectedData = this.collectFramesAndData();
            System.out.println("analysis finished!");
        }catch(Exception e) {
            System.out.println("EXCEPTION!");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
    
    private synchronized boolean threadsFinished() {
        // check if all threads that are currently schedules have finished
        for(int i = 0; i < this.scheduledMethods.length; i++) {
            if(this.scheduledMethods[i] == null) {
                continue;
            }
            if(this.scheduledMethods[i].isFinished() == false) {
                return false;
            }
        }
        return true;
    }
    
    
    private synchronized void scheduleJob(AnalysisMethod method, int index) 
    {
        AnalysisMethod threadedMethod = method.createThreadedMethod();
        LPFrame frameTmp = this.frames.get(index);
        
        try {
            while(true) {
                for(int i = 0; i < this.scheduledMethods.length; i++) 
                {
                    if(this.scheduledMethods[i] == null || this.scheduledMethods[i].isFinished()) 
                    {
                        this.scheduledMethods[i] = threadedMethod;
                        threadedMethod.runAnalysis(this, frameTmp, index);
                        return;
                    }
                }
                System.out.println("Waiting for thread to finish!");
                this.wait(); // wait for another frame to finish
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    private HashMap <Integer, AbstractDataset> collectFramesAndData() 
    {
        HashMap <Integer, AbstractDataset> data = new HashMap <Integer, AbstractDataset>();
        for(AnalysisMethod method : this.finishedMethods) 
        {
            AbstractDataset dataset = method.getData();
            int frameIndex = method.getFrameIndex();
            data.put(frameIndex, dataset);
        }
        
        Set <Integer> keySet = data.keySet();
        Integer [] keys =  (Integer [])keySet.toArray(new Integer[1]);
        
        Arrays.sort(keys);
        
        return data;
    }
    
    public AbstractDataset getDatasetForFrameIndex(int frameIndex) 
    {
        return this.collectedData.get(new Integer(frameIndex));
    }
    
    public int getNumberOfFrames() 
    {
        return this.collectedData.size();
    }
    
    public boolean hasData() 
    {
        return this.collectedData != null;
    }
}
