/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package analysis.methods.wavelets;

import analysis.AnalysisHandler;
import analysis.AnalysisMethod;
import data.LPFrame;
import gui.ChartVisualizationFrame.ChartType;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import jceliac.tools.data.DataSource;
import jceliac.tools.data.Frame;
import jceliac.tools.wavelet.WaveletDecomposition;
import jceliac.tools.wavelet.WaveletTransform;
import org.jfree.data.general.AbstractDataset;
import org.jfree.data.statistics.HistogramDataset;
import tools.ArrayTools;

/**
 *
 * @author shegen
 */
public class AnalysisMethodWaveletCoefficients extends AnalysisMethod
{

    private HistogramDataset dataset;
    
    public AnalysisMethodWaveletCoefficients(AnalysisHandler handler)
    {
        super(handler);
        this.dataset = new HistogramDataset();
    }

    @Override
    public void analyzeData(double[] colorChannel)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void analyzeData(LPFrame image)
    {
        
        HashMap <Integer, ArrayList <Double [][]>> approximationCoefficients; // key is the color channel
        HashMap <Integer, ArrayList <Double [][]>> horizontalCoefficients; // key is the color channel
        HashMap <Integer, ArrayList <Double [][]>> verticalCoefficients; // key is the color channel
        HashMap <Integer, ArrayList <Double [][]>> diagonalCoefficients; // key is the color channel
        
        int w = image.getWidth();
        int h = image.getHeight();

        int [][] red2D = image.getRedData();
        int [][] green2D = image.getGreenData();
        int [][] blue2D = image.getBlueData();

        try {
            int maxScale = 1;
            
            WaveletTransform wt = new WaveletTransform(WaveletTransform.EExtensionType.SYMMETRIC_HALFPOINT, WaveletTransform.EFilterBankType.CDF97);
            wt.transformForwardSingleChannel(ArrayTools.convertIntToDouble(red2D), maxScale, DataSource.CHANNEL_RED);
            wt.transformForwardSingleChannel(ArrayTools.convertIntToDouble(green2D), maxScale, DataSource.CHANNEL_GREEN);
            wt.transformForwardSingleChannel(ArrayTools.convertIntToDouble(blue2D), maxScale, DataSource.CHANNEL_BLUE);

            approximationCoefficients = new HashMap <Integer, ArrayList <Double [][]>> ();
            verticalCoefficients = new HashMap <Integer, ArrayList <Double [][]>> ();
            horizontalCoefficients = new HashMap <Integer, ArrayList <Double [][]>> ();
            diagonalCoefficients = new HashMap <Integer, ArrayList <Double [][]>> ();
            
            WaveletDecomposition decompRed = wt.getWaveletDecompositionTree(DataSource.CHANNEL_RED);
            WaveletDecomposition decompBlue = wt.getWaveletDecompositionTree(DataSource.CHANNEL_BLUE);        
            WaveletDecomposition decompGreen = wt.getWaveletDecompositionTree(DataSource.CHANNEL_GREEN);
            
            double [][] approxRed2D = decompRed.getApproximationSubband(1);
            double [][] approxGreen2D = decompRed.getApproximationSubband(1);
            double [][] approxBlue2D = decompRed.getApproximationSubband(1);
            
            double [] approxRed1D = ArrayTools.convert2Dto1D(approxRed2D);
            double [] approxGreen1D = ArrayTools.convert2Dto1D(approxGreen2D);
            double [] approxBlue1D = ArrayTools.convert2Dto1D(approxBlue2D);
            
            this.dataset.addSeries("HH Red", approxRed1D, 255);
            this.dataset.addSeries("HH Blue", approxBlue1D, 255);
            this.dataset.addSeries("HH Green", approxGreen1D, 255);
            
        }catch(Exception e) {
            e.printStackTrace();
        }
    }   

    @Override
    public AnalysisMethod createThreadedMethod()
    {
        return new AnalysisMethodWaveletCoefficients(this.handler);
    }

    @Override
    public ChartType getChartVisualizationType()
    {
        return ChartType.Histogram;
    }

    @Override
    public AbstractDataset getData()
    {
        return this.dataset;
    }
    
    
    
    
    
}
