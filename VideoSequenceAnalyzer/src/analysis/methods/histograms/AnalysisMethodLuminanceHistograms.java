/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package analysis.methods.histograms;

import analysis.AnalysisHandler;
import analysis.AnalysisMethod;
import data.LPFrame;
import java.awt.image.BufferedImage;
import jceliac.tools.data.Frame;
import tools.ArrayTools;
import tools.ColorSpaceConverter;

/**
 *
 * @author shegen
 */
public class AnalysisMethodLuminanceHistograms extends AnalysisMethodHistograms
{

    public AnalysisMethodLuminanceHistograms(AnalysisHandler handler)
    {
        super(handler);
    }

    @Override
    public void analyzeData(LPFrame image)
    {
        int w = image.getWidth();
        int h = image.getHeight();

        // the documentation is not totally clear here, it looks like the 0 channel is
        // always red, does not matter if its an RGB or BGR image
        int [][] red2D = image.getRedData();
        int [][] green2D = image.getGreenData();
        int [][] blue2D = image.getBlueData();
        
        double [] red1D = ArrayTools.convert2Dto1D(red2D);
        double [] green1D = ArrayTools.convert2Dto1D(green2D);
        double [] blue1D = ArrayTools.convert2Dto1D(blue2D);
        
        
        // convert from RGB to LAB, unfortunately the ColorSpaceConverter only supports
        // converting a single RGB value to LAB at this point, this is pretty slow and
        // should be improved if used further
        ColorSpaceConverter converter = new ColorSpaceConverter();
        double [] luminance = new double[image.getWidth() * image.getHeight()];
        for(int i = 0; i < red1D.length; i++) 
        {
            int [] intData = new int[3];
            intData[0] = (int)red1D[i];
            intData[1] = (int)green1D[i];
            intData[2] = (int)blue1D[i];
            
            double [] labData =  converter.RGBtoLAB(intData);
            luminance[i] = labData[0];
        }
        this.addSeriesToDataset("Luminance", luminance);
    }
    @Override
    public AnalysisMethod createThreadedMethod()
    {
        return new AnalysisMethodLuminanceHistograms(this.handler);
    }
    
    
}
