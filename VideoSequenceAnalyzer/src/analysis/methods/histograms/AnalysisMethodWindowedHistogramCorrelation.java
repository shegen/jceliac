/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package analysis.methods.histograms;

import analysis.AnalysisHandler;
import analysis.AnalysisMethod;
import data.LPFrame;
import gui.ChartVisualizationFrame.ChartType;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import jceliac.gui.ImageViewerFrame;
import jceliac.tools.data.Frame;
import jceliac.tools.math.MathTools;
import jceliac.video.CropFilter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.AbstractDataset;
import org.jfree.data.statistics.HistogramDataset;
import tools.ArrayTools;
import tools.ColorSpaceConverter;
import tools.CropFilterExtended;

/**
 *
 * @author shegen
 */
public class AnalysisMethodWindowedHistogramCorrelation extends AnalysisMethod
{

    public AnalysisMethodWindowedHistogramCorrelation(AnalysisHandler handler)
    {
        super(handler);
        this.dataset = new HistogramDataset();
    }

    private HistogramDataset dataset;
    public static final int HISTOGRAM_BINS = 255;
    
    @Override
    public void analyzeData(double[] colorChannel)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void analyzeData(LPFrame image)
    {
        
        int w = image.getWidth();
        int h = image.getWidth();

        int [][] red2D = image.getRedData();
        int [][] green2D = image.getGreenData();
        int [][] blue2D = image.getBlueData();
        
        double [] red1D = ArrayTools.convert2Dto1D(red2D);
        double [] green1D = ArrayTools.convert2Dto1D(green2D);
        double [] blue1D = ArrayTools.convert2Dto1D(blue2D);
        
        
        ColorSpaceConverter colorConverter = new ColorSpaceConverter();
        double [][] LAB = colorConverter.RGBtoLAB(red1D, green1D, blue1D);
        double [] luminance = LAB[0];
        

        int dim = 50;
        ArrayList<double[]> windows = this.extractWindows(luminance, dim, w, h);

   /*     double[] tmp1D = windows.get(3);

        double[][] tmp2D = new double[dim][dim];
        for (int i = 0; i < dim * dim; i++) {
            int x = i % dim;
            int y = i / dim;
            tmp2D[x][y] = tmp1D[i];

            ImageViewerFrame frame = ImageViewerFrame.createFrame(tmp2D);
            frame.setVisible(true);
        }*/
        
        
        double [] corrs = new double [windows.size() * windows.size()];
        
        for(int i = 0; i < windows.size(); i++) {
            for(int j = 0; j < windows.size(); j++) {
                corrs[i+windows.size()*j] = this.computeSampleCorrelationCoefficient(windows.get(i), windows.get(j));
            }
        }
        this.dataset.addSeries("Corrs", corrs, 20, -1d, 1d);  
    }
    
    private double [] computeNormalizedHistogram(double [] data)
    {
        double [] hist = new double[HISTOGRAM_BINS+1];
        this.scaleData(data);
        
        double sum = 0;
        for(int i = 0; i < data.length; i++) 
        {
            hist[(int)Math.round(data[i])]++;
            sum ++;
        }
        /*for(int i = 0; i < hist.length; i++) 
        {
            hist[i] = hist[i] / sum;
        }*/
        return hist;
    }
    
    private void scaleData(double[] data)
    {
        double min, max;

        min = Integer.MAX_VALUE;
        max = Integer.MIN_VALUE;

        for (int x = 0; x < data.length; x++) {
            double tmp = data[x];
            if (tmp < min) {
                min = tmp;
            }
            if (tmp > max) {
                max = tmp;
            }
        }
        double width = (Math.abs(max) - Math.abs(min));
        double scaleFactor = (255 / width); // scale it to max 255

        for (int x = 0; x < data.length; x++) {
            double k = data[x];
            double normalized = (k - Math.abs(min)) * scaleFactor;
            data[x] = normalized;
        }
    }
    

    private double computeSampleCorrelationCoefficient(double [] hist1, double [] hist2) 
    {
        double sum = 0;
        double mean1 = 0, mean2 = 0;
        
        for(int i = 0; i < hist1.length; i++) 
        {
            mean1 += hist1[i];
            mean2 += hist2[i];
        }
        mean1 = mean1 / hist1.length;
        mean2 = mean2 / hist1.length;
        
        for(int i = 0; i < hist1.length; i++) 
        {
            sum += (hist1[i] - mean1) * (hist2[i] - mean2);
        }
        double std1 = MathTools.stdev(hist1);
        double std2 = MathTools.stdev(hist2);
        
        double denom = (hist1.length - 1) * std1 * std2;
        return sum / denom;
    }
    
    
    private ArrayList <double []> extractWindows(double [] data, int dimension, int imageWidth, int imageHeight)
    {
        ArrayList <double[]> windows = new ArrayList <double[]>();

        for(int startI = 0; startI < imageWidth; startI += dimension) {
            for(int startJ = 0; startJ < imageHeight; startJ += dimension) {
        
                double [] windowData = new double[dimension*dimension];
                for(int i = 0; i < dimension; i++) {
                    for(int j = 0; j < dimension; j++) {
                        windowData[i+dimension*j] = data[(startI+i)+imageWidth*(startJ+j)];
                    }
                }
                windows.add(windowData);
           }
        }
        return windows;
    }
    
    private int getLinearIndex(int i, int j, int imageWidth)
    {
        // dimension 1 = i, dimension 2 = j
        return i*imageWidth +j;
    }
    
    @Override
    public AnalysisMethod createThreadedMethod()
    {
        return new AnalysisMethodWindowedHistogramCorrelation(this.handler);
    }

    @Override
    public ChartType getChartVisualizationType()
    {
        return ChartType.Histogram;
    }

    @Override
    public AbstractDataset getData()
    {
        return this.dataset;
    }
    
}
