/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package analysis.methods.histograms;

import analysis.AnalysisHandler;
import analysis.AnalysisMethod;
import data.LPFrame;
import gui.ChartVisualizationFrame.ChartType;
import java.awt.image.BufferedImage;
import java.util.Random;
import jceliac.tools.data.Frame;
import org.jfree.data.general.AbstractDataset;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.xy.IntervalXYDataset;
import tools.ArrayTools;

/**
 *
 * @author shegen
 */
public class AnalysisMethodHistograms extends AnalysisMethod
{

    private HistogramDataset dataset;
    public static final int HISTOGRAM_BINS = 255;
    
    public AnalysisMethodHistograms(AnalysisHandler handler)
    {
        super(handler);
        this.dataset = new HistogramDataset();
    }
    
    @Override
    public void analyzeData(double[] colorChannel)
    {
        return;
    }

    @Override
    public void analyzeData(LPFrame image)
    {
        int w = image.getWidth();
        int h = image.getHeight();
        
        int [][] red2D = image.getRedData();
        int [][] green2D = image.getGreenData();
        int [][] blue2D = image.getBlueData();
        
        double [] red1D = ArrayTools.convert2Dto1D(red2D);
        double [] green1D = ArrayTools.convert2Dto1D(green2D);
        double [] blue1D = ArrayTools.convert2Dto1D(blue2D);
        
        this.addSeriesToDataset("Red", red1D);
        this.addSeriesToDataset("Blue", blue1D);
        this.addSeriesToDataset("Green", green1D);
    }

    @Override
    public AnalysisMethod createThreadedMethod()
    {
        return new AnalysisMethodHistograms(this.handler);
    }

    @Override
    // returns HistogramDataset
    public AbstractDataset getData()
    {
        return this.dataset;
    }

    @Override
    public ChartType getChartVisualizationType()
    {
        return ChartType.Histogram;
    }
    
    
    
    protected void addSeriesToDataset(Comparable key, double [] data) 
    {
        this.dataset.addSeries(key, data, HISTOGRAM_BINS);
    }
    
    
    

   
}
