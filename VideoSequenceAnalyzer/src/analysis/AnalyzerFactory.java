/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package analysis;

import analysis.methods.histograms.AnalysisMethodHistograms;
import analysis.methods.histograms.AnalysisMethodLuminanceHistograms;
import analysis.methods.histograms.AnalysisMethodWindowedHistogramCorrelation;
import analysis.methods.wavelets.AnalysisMethodWaveletCoefficients;
import gui.ChartVisualizationFrame.ChartType;
import javax.swing.JMenuItem;

/**
 *
 * @author shegen
 */
public class AnalyzerFactory
{
    public static enum AnalysisMethodType {
        Histogram,
        LuminanceHistogram,
        WaveletCoefficients,
        WindowedHistograms
    }
    
    public static AnalysisMethod createAnalysisMethod(AnalysisMethodType methodType, AnalysisHandler handler) 
    {
        switch(methodType)
        {
            case Histogram:
                return new AnalysisMethodHistograms(handler);
            case LuminanceHistogram:
                return new AnalysisMethodLuminanceHistograms(handler);
            case WaveletCoefficients:
                return new AnalysisMethodWaveletCoefficients(handler);
            case WindowedHistograms:
                return new AnalysisMethodWindowedHistogramCorrelation(handler);
        }
        
        return null;
    }
    
    public static ChartType getChartTypeForMethod(AnalysisMethodType methodType) 
    {
       AnalysisMethod dummyMethod = AnalyzerFactory.createAnalysisMethod(methodType, null);
       return dummyMethod.getChartVisualizationType();
    }

    
}
