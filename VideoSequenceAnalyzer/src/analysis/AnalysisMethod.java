/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package analysis;

import data.LPFrame;
import gui.ChartVisualizationFrame.ChartType;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import org.jfree.data.general.*;
import tools.CropFilterExtended;

/**
 *
 * @author shegen
 */
public abstract class AnalysisMethod extends Thread
{
    public abstract void analyzeData(double [] colorChannel);
    public abstract void analyzeData(LPFrame image);
    public abstract AnalysisMethod createThreadedMethod();
    
    protected AnalysisHandler handler;
    protected boolean finished = false;
    private LPFrame frame;
    private int frameIndex;
    private boolean useCrop = false;
    
    public AnalysisMethod(AnalysisHandler handler)
    {
        this.handler = handler;
    }

    public void runAnalysis(AnalysisHandler handler, LPFrame frame, int frameIndex)
    {
        if(this.useCrop)
        {
            // extract only the part of the image showing mucosa without text and borders
            CropFilterExtended cropFilter = new CropFilterExtended();
            LPFrame croppedFrame = cropFilter.crop(frame);
            this.frame = croppedFrame;
        }else {
            this.frame = frame;
        }
        this.frameIndex = frameIndex;
        this.start();
    }

    @Override
    public void run()
    {
        this.analyzeData(this.frame);
        this.finished = true;
        this.handler.notifyAnalysisFinished(this);
    }
    
    public synchronized boolean isFinished()
    {
        return finished;
    }
    
    public int getFrameIndex()
    {
        return this.frameIndex;
    }

    public boolean isUseCrop()
    {
        return useCrop;
    }

    public void setUseCrop(boolean useCrop)
    {
        this.useCrop = useCrop;
    }
    
    public abstract AbstractDataset getData();
    public abstract ChartType getChartVisualizationType();
}
