/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package debug;

import data.LPFrame;
import decoder.VideoDecoder;
import deinterlace.BlendDeinterlacer;
import deinterlace.BobDeinterlacer;
import deinterlace.DeinterlacingFilter;
import deinterlace.ELADeinterlacer;
import deinterlace.HMDERPDeinterlacer;
import deinterlace.VTMedianDeinterlacer;
import deinterlace.motionCompensated.FiveFieldMCDeinterlacer;
import deinterlace.motionestimation.FiveFieldMotionEstimator;
import deinterlace.motionestimation.FiveFieldMotionEstimatorBugged;
import deinterlace.motionestimation.FiveFieldMotionEstimatorNew;
import deinterlace.motionestimation.MotionVector;
import deinterlace.motionestimation.TSSMotionEstimator;
import gui.ChartVisualizationFrame;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import javax.imageio.ImageIO;
import jceliac.gui.ImageViewerFrame;
import jceliac.tools.data.Frame;
import jceliac.tools.timer.PerformanceTimer;
import org.jfree.data.statistics.HistogramDataset;

/**
 *
 * @author shegen
 */
public class DebugMain
{


    
    
    public static void main(String[] args)
    {


        //DebugMain.testLPFrameSpeed();
       // DebugMain.testMotionEstimationSpeed();
        //DebugMain.debugHMDERPDeinterlacing();
     //   DebugMain.debugFiveFieldMCDeinterlacing();
        
        //DebugMain.debugBobDeinterlacer();
   //     DebugMain.debugFiveFieldMCDeinterlacing();
       // DebugMain.debugBlobDeinterlacing();
        DebugMain.debugVTMedianDeinterlacing();
                
    }
    
    public static void testMotionEstimationSpeed()
    {
        
    }
    
    public static void debugHMDERPDeinterlacing()
    {
        try {
             BufferedImage pic44 =  ImageIO.read(new File("/home/stud3/shegen/debug-deinterlacing/pics83.png"));
             BufferedImage pic45 =  ImageIO.read(new File("/home/stud3/shegen/debug-deinterlacing/pics84.png"));
             BufferedImage pic46 =  ImageIO.read(new File("/home/stud3/shegen/debug-deinterlacing/pics85.png"));
             
             LPFrame pastFrame = LPFrame.convertBufferedImageToFrameOptimized(pic44, false);
             LPFrame currentFrame = LPFrame.convertBufferedImageToFrameOptimized(pic45, false);
             LPFrame nextFrame = LPFrame.convertBufferedImageToFrameOptimized(pic46, false);
             
             int [][] tmp = pastFrame.getGrayData();
             for(int row = 0; row < tmp.length; row+=2) {
                 for(int col = 0; col < tmp[0].length; col++) {
                     tmp[row][col] = -1;
                 }
             }
             pastFrame.setRedData(tmp);
             pastFrame.setGreenData(tmp);
             pastFrame.setBlueData(tmp);
             pastFrame.setGrayData(tmp);
             
             tmp = currentFrame.getGrayData();
             for(int row = 1; row < tmp.length; row+=2) {
                 for(int col = 0; col < tmp[0].length; col++) {
                     tmp[row][col] = -1;
                 }
             }
             currentFrame.setRedData(tmp);
             currentFrame.setGreenData(tmp);
             currentFrame.setBlueData(tmp);
             currentFrame.setGrayData(tmp);
             
             tmp = nextFrame.getGrayData();
             for(int row = 0; row < tmp.length; row+=2) {
                 for(int col = 0; col < tmp[0].length; col++) {
                     tmp[row][col] = -1;
                 }
             }
             nextFrame.setRedData(tmp);
             nextFrame.setGreenData(tmp);
             nextFrame.setBlueData(tmp);
             nextFrame.setGrayData(tmp);
             
             
             
             
             DeinterlacingFilter df = new ELADeinterlacer();
             ArrayList <LPFrame> past = new ArrayList <LPFrame>();
             ArrayList <LPFrame> future = new ArrayList <LPFrame>();
            
             past.add(pastFrame);
             future.add(nextFrame);
             LPFrame current = currentFrame;
            
             df.setPastFrames(past);
             df.setFutureFrames(future);
             df.setCurrentFrame(current);
            
             LPFrame deinterlaced = df.deinterlace(true);
             
             ImageViewerFrame frame = ImageViewerFrame.createFrame(deinterlaced.getGrayData(), false);
             frame.setVisible(true);
             frame.setTitle("Deinterlaced");
            
            
            
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
    public static LPFrame interlaceFrameData(LPFrame frame, boolean topField)
    {
        
        LPFrame newFrame = new LPFrame();
        
        int[][] gray = frame.getGrayData();
        int[][] red = frame.getRedData();
        int[][] green = frame.getGreenData();
        int[][] blue = frame.getBlueData();

        
        int [][] grayCopy = new int[gray.length][gray[0].length];
        int [][] redCopy = new int[gray.length][gray[0].length];
        int [][] greenCopy = new int[gray.length][gray[0].length];
        int [][] blueCopy = new int[gray.length][gray[0].length];
        
        
        if(!topField) 
        {
            for (int row = 0; row < gray.length; row ++) {
                for (int col = 0; col < gray[0].length; col++) {
                    if(row % 2 == 0) 
                    { 
                        grayCopy[row][col] = -1;
                        redCopy[row][col] = -1;
                        greenCopy[row][col] = -1;
                        blueCopy[row][col] = -1;
                    }else {
                        grayCopy[row][col] = gray[row][col];
                        redCopy[row][col] = red[row][col];
                        greenCopy[row][col] = green[row][col];
                        blueCopy[row][col] = blue[row][col];
                    }
                }
            }
        }else {
           for (int row = 0; row < gray.length; row ++) {
                for (int col = 0; col < gray[0].length; col++) {
                    if(row % 2 == 1) 
                    { 
                        grayCopy[row][col] = -1;
                        redCopy[row][col] = -1;
                        greenCopy[row][col] = -1;
                        blueCopy[row][col] = -1;
                    }else {
                        grayCopy[row][col] = gray[row][col];
                        redCopy[row][col] = red[row][col];
                        greenCopy[row][col] = green[row][col];
                        blueCopy[row][col] = blue[row][col];
                    }
                }
            } 
        }
        newFrame.setRedData(grayCopy);
        newFrame.setGreenData(redCopy);
        newFrame.setBlueData(greenCopy);
        newFrame.setGrayData(blueCopy);
        newFrame.setColumnFirstStored(frame.isColumnFirstStored());
        return newFrame;
    }
    
    
    public static void debugFiveFieldMCDeinterlacing()
    {
        try 
        {
             BufferedImage prev2 =  ImageIO.read(new File("/home/stud3/shegen/interlaced-videos/2pixelshiftdown/frame1.png"));
             BufferedImage prev =  ImageIO.read(new File("/home/stud3/shegen/interlaced-videos/2pixelshiftdown/frame2.png"));
             BufferedImage cur =  ImageIO.read(new File("/home/stud3/shegen/interlaced-videos/2pixelshiftdown/frame3.png"));
             BufferedImage next =  ImageIO.read(new File("/home/stud3/shegen/interlaced-videos/2pixelshiftdown/frame4.png"));
             BufferedImage next2 =  ImageIO.read(new File("/home/stud3/shegen/interlaced-videos/2pixelshiftdown/frame5.png"));
             
             LPFrame prev2Frame = LPFrame.convertBufferedImageToFrameOptimized(prev2, false);
             LPFrame prevFrame = LPFrame.convertBufferedImageToFrameOptimized(prev, false);
             LPFrame curFrame = LPFrame.convertBufferedImageToFrameOptimized(cur, false);
             LPFrame nextFrame = LPFrame.convertBufferedImageToFrameOptimized(next, false);
             LPFrame next2Frame = LPFrame.convertBufferedImageToFrameOptimized(next2, false);
             
             prev2Frame = DebugMain.interlaceFrameData(prev2Frame, true);
             prevFrame = DebugMain.interlaceFrameData(prevFrame, false);
             curFrame = DebugMain.interlaceFrameData(curFrame, true);
             nextFrame = DebugMain.interlaceFrameData(nextFrame, false);
             next2Frame = DebugMain.interlaceFrameData(next2Frame, true);

             FiveFieldMotionEstimatorNew motionEstimator = new FiveFieldMotionEstimatorNew(true, prev2Frame.getGrayData(), 
                                                            prevFrame.getGrayData(),curFrame.getGrayData(), nextFrame.getGrayData(),
                                                            next2Frame.getGrayData());
             FiveFieldMCDeinterlacer d = new FiveFieldMCDeinterlacer();
             
             ArrayList <LPFrame> pastFrames = new ArrayList<LPFrame>();
             ArrayList <LPFrame> nextFrames = new ArrayList<LPFrame>();
             
             pastFrames.add(prev2Frame);
             pastFrames.add(prevFrame);
             nextFrames.add(nextFrame);
             nextFrames.add(next2Frame);
             
             d.setCurrentFrame(curFrame);
             d.setPastFrames(pastFrames);
             d.setFutureFrames(nextFrames);
             
             LPFrame deinterlacedFrame = d.performDeinterlace(true);
             ImageViewerFrame viewer = ImageViewerFrame.createFrame(deinterlacedFrame.getGrayData(), false);
             viewer.setVisible(true);
             System.in.read();
             

         //    HashMap <Integer,MotionVector> mv = motionEstimator.computeMotionVectors();          
           //  int blub = 0;
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
     public static void debugBlobDeinterlacing()
    {
        try 
        {
             BufferedImage cur =  ImageIO.read(new File("/home/stud3/shegen/interlaced-videos/1pixelshiftdown/frame3.png"));
             LPFrame curFrame = LPFrame.convertBufferedImageToFrameOptimized(cur, false);
             curFrame = DebugMain.interlaceFrameData(curFrame, true);
           
             BobDeinterlacer d = new BobDeinterlacer(BobDeinterlacer.Mode.ScaneLineInterpolation);
             d.setCurrentFrame(curFrame);

             
             LPFrame frameOut = d.performDeinterlace(true);
             ImageViewerFrame viewer = ImageViewerFrame.createFrame(frameOut.getGrayData(), false);
             viewer.setVisible(true);
             System.in.read();
             
             

         //    HashMap <Integer,MotionVector> mv = motionEstimator.computeMotionVectors();          
           //  int blub = 0;
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
     
     
    public static void debugVTMedianDeinterlacing()
    {
        try 
        {
             BufferedImage cur =  ImageIO.read(new File("/home/stud3/shegen/interlaced-videos/1pixelshiftdown/frame3.png"));
             BufferedImage past =  ImageIO.read(new File("/home/stud3/shegen/interlaced-videos/1pixelshiftdown/frame2.png"));
             
             
             LPFrame curFrame = LPFrame.convertBufferedImageToFrameOptimized(cur, false);
             curFrame = DebugMain.interlaceFrameData(curFrame, true);
             LPFrame pastFrame = LPFrame.convertBufferedImageToFrameOptimized(past, false);
             pastFrame = DebugMain.interlaceFrameData(pastFrame, true);
             
             ArrayList <LPFrame> pastFrames = new ArrayList <LPFrame>();
             pastFrames.add(pastFrame);
             
             VTMedianDeinterlacer d = new VTMedianDeinterlacer();
             d.setCurrentFrame(curFrame);
             d.setPastFrames(pastFrames);

             
             LPFrame frameOut = d.performDeinterlace(true);
             ImageViewerFrame viewer = ImageViewerFrame.createFrame(frameOut.getGrayData(), false);
             viewer.setVisible(true);
             System.in.read();
             
         //    HashMap <Integer,MotionVector> mv = motionEstimator.computeMotionVectors();          
           //  int blub = 0;
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    

    
    public static void debugELADeinterlacer()
    {
        try 
        {
             BufferedImage cur =  ImageIO.read(new File("/home/stud3/shegen/interlaced-videos/testimg.png"));
             
             LPFrame curFrame = LPFrame.convertBufferedImageToFrameOptimized(cur, false);

             curFrame = DebugMain.interlaceFrameData(curFrame, true);
             
             ELADeinterlacer ela = new ELADeinterlacer();
             ela.setCurrentFrame(curFrame);
             ela.performDeinterlace(true);

        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void debugBobDeinterlacer()
    {
        try 
        {
             BufferedImage cur =  ImageIO.read(new File("/home/stud3/shegen/interlaced-videos/testimg.png"));
             
             LPFrame curFrame = LPFrame.convertBufferedImageToFrameOptimized(cur, false);

             curFrame = DebugMain.interlaceFrameData(curFrame, true);
             
             BobDeinterlacer bob = new BobDeinterlacer(BobDeinterlacer.Mode.ScanLineDuplication);
             bob.setCurrentFrame(curFrame);
             bob.performDeinterlace(true);

        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
    public static void testXugglerSpeed()
    {
        try 
        {
           PerformanceTimer timer = new PerformanceTimer();
           timer.startTimer();
            
           File f = new File("/home/stud3/shegen/Sequences/40/Frontal/strtak_xenia_OEGD_00000000_0000_UKN-26.vob");
           VideoDecoder decoder = new VideoDecoder(f);
           decoder.decodeEntireVideo();
           
           timer.stopTimer();
           System.out.println("Video Decoded in: "+timer.getSeconds());

        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void testLPFrameSpeed() 
     {
        try 
        {
            BufferedImage image = ImageIO.read(new File("/home/stud3/shegen/WORK/Code/VideoSequenceAnalyzer/data/frames155.png"));
            
            PerformanceTimer timer = new PerformanceTimer();
            timer.startTimer();
            
            for(int i = 0; i < 10; i++) {
                Frame fr = Frame.convertBufferedImageToFrameOptimized(image);
            }
            
            timer.stopTimer();
            System.out.println("Traditional Frame Implementation: "+timer.getSeconds());
            
            timer.startTimer();
            
            for(int i = 0; i < 10; i++) {
                LPFrame fr = LPFrame.convertBufferedImageToFrameOptimized(image, false);
                int [][] g = fr.getGrayData();
            }
            
            timer.stopTimer();
            System.out.println("LPFrame Implementation Column First: "+timer.getSeconds());
            
            timer.startTimer();
            
            for(int i = 0; i < 10; i++) {
                LPFrame fr = LPFrame.convertBufferedImageToFrameOptimized(image, true);
                int [][] g = fr.getGrayData();
            }
            
            timer.stopTimer();
            System.out.println("LPFrame Implementation Row First: "+timer.getSeconds());
            
            /*double [] data = {1,2,3,4,5,6,7,8,9,10};
            double [] filter = { 0.274068619061197,   0.451862761877606,   0.274068619061197, 0.1};
            *
            ConvolutionFFT.overlapadd(data, filter);*/
                    
        }catch(Exception e) {
            e.printStackTrace();
        }
     }
    
}
