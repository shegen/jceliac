/*
 *  JCeliac Copyright (C) 2010-2011 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package gui;

import javax.swing.*;
import java.util.*;
import java.io.*;
/**
 *
 * @author shegen
 */
public class FileListModel extends DefaultListModel
{
    public void addAll(ArrayList <File> list) {
 
        Collections.sort(list, new FileComparator());
        for(File tmp : list) {
            super.addElement(tmp);
        }
    }

    @Override
    public Object getElementAt(int i)
    {
        Object element = super.getElementAt(i);
        return element;
    }

    @Override
    public Object remove(int i)
    {
        return super.remove(i);
    }
    
    private class FileComparator implements Comparator <File> {

        public int compare(File t, File t1)
        {
            return t.getName().compareTo(t1.getName());
        }
        
    }
    
}
