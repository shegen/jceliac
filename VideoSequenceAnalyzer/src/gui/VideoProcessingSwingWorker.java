/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package gui;

import data.LPFrame;
import decoder.VideoDecoder;
import deinterlace.DeinterlacingFilter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;

/**
 *
 * @author shegen
 */
public class VideoProcessingSwingWorker extends SwingWorker <ArrayList <LPFrame>, Object>
{
    
    private VideoDecoder decoder;
    private DeinterlacingFilter deinterlacingFilter;
    private int progress = 0;
    
    public VideoProcessingSwingWorker(File video, DeinterlacingFilter deinterlacingFilter)
            throws Exception
    {
        this.decoder = new VideoDecoder(video);
        this.deinterlacingFilter = deinterlacingFilter;
    }

    
    @Override
    protected ArrayList<LPFrame> doInBackground() throws Exception
    {
        this.decoder.decodeEntireVideo();
        this.decoder.deinterlaceFrames(this.deinterlacingFilter);
        return this.decoder.getFrames();
        
        
    }

    @Override
    protected void process(List<Object> list)
    {
        super.process(list);
    }
    
    
    
}
