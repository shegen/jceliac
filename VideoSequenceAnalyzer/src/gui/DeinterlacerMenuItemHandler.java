/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package gui;

import java.awt.event.InputMethodListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import deinterlace.*;
import deinterlace.DeinterlacingFilter.DeinterlacerType;
import deinterlace.motionCompensated.FiveFieldMCDeinterlacer;
import deinterlace.textureDetection.MATextureDetectionDeinterlacer;

/**
 *
 * @author shegen
 */
public class DeinterlacerMenuItemHandler implements ItemListener
{

    private HashMap <JCheckBoxMenuItem,DeinterlacingFilter.DeinterlacerType> methods;
    private JMenu menu;
    private boolean activatedItemStateListener = true;
    private DeinterlacingFilter.DeinterlacerType selectedMethod;
    private DeinterlacingFilter filter;
    
    public DeinterlacerMenuItemHandler(JMenu menu)
    {
        this.menu = menu;
        this.methods = new HashMap <JCheckBoxMenuItem,DeinterlacingFilter.DeinterlacerType>();
    }
    
    public void registerMethod(DeinterlacingFilter.DeinterlacerType methodType, JCheckBoxMenuItem menuItem)
            throws Exception
    {
        if(this.methods.containsValue(methodType)) {
            throw new Exception("Analyzation Method Type Registered Multiple Times!");
        }
        this.methods.put(menuItem,methodType);
        this.menu.add(menuItem);
        menuItem.addItemListener(this); 
    }
    
    public void checkMethod(JCheckBoxMenuItem menuItem) 
    {
        for(int i = 0; i < this.menu.getItemCount(); i++) {
            JCheckBoxMenuItem tmpItem = (JCheckBoxMenuItem)this.menu.getItem(i);
            tmpItem.setSelected(false);
        }
        menuItem.setSelected(true);   
        this.activatedItemStateListener = true;
        
    }

    @Override
    public void itemStateChanged(ItemEvent ie)
    {
        if(this.activatedItemStateListener == false) {
            return;
        }
        else {
            this.activatedItemStateListener = false;
            JCheckBoxMenuItem menuItem = (JCheckBoxMenuItem)ie.getItem();
            this.checkMethod(menuItem);
            this.selectedMethod = this.methods.get(menuItem);
        }
    }
    
    public DeinterlacingFilter createMethodObject() 
            throws Exception
    {
        if(this.selectedMethod == null) {
            return new NullDeinterlacer();
        }
        switch(this.selectedMethod) 
        {
            case Blend:
                return new BlendDeinterlacer();
                
            case ScanLineDuplication:
                return new BobDeinterlacer(BobDeinterlacer.Mode.ScanLineDuplication);
                
            case ScanLineInterpolation:
                return new BobDeinterlacer(BobDeinterlacer.Mode.ScaneLineInterpolation);
                
            case Yadif:
                return new YadifDeinterlacer();
             
            case HMD:
                return new HMDERPDeinterlacer();
            
            case ELA:
                return new ELADeinterlacer();
            
            case FiveFieldMC:
                return new FiveFieldMCDeinterlacer();
            
            case VTMedian:
                return new VTMedianDeinterlacer();
             
            case MATexture:
                return new MATextureDetectionDeinterlacer();
        }
        throw new Exception();
    }
    
    public DeinterlacingFilter.DeinterlacerType getSelectedMethod()
    {
        return this.selectedMethod;
    }
    
    public boolean isMethodSelected()
    {
        return this.selectedMethod != null;
    }
    
    
}
