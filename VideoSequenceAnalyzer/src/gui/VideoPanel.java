/*
 *  JCeliac Copyright (C) 2010-2011 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package gui;

import data.LPFrame;
import java.awt.*;
import javax.swing.*;
import java.awt.Dimension;
import javax.swing.SwingUtilities;
import java.awt.image.*;
import jceliac.tools.data.Frame;
/**
 *
 * @author shegen
 */
public class VideoPanel extends JPanel implements IVideoDisplay
{
    private Image image;
    private Rectangle rect;
    
    @Override
    public void paint(Graphics g)
    {
        if (image == null) {
            g.clearRect(0, 0, this.getWidth(), this.getHeight());
            return;
        }

        if ((image.getWidth(null) > this.getWidth()) || (image.getHeight(null) > this.getHeight())) {
            g.clearRect(0, 0, this.getWidth(), this.getHeight());
            g.drawImage(this.image, 0, 0, null);
            if (this.rect != null) {
                g.setColor(Color.WHITE);
                g.drawRect(this.rect.x, this.rect.y, this.rect.width, this.rect.height);
            }

        } else {
            g.clearRect(0, 0, this.getWidth(), this.getHeight());
            if (this.image != null) {
                g.drawImage(this.image, 0, 0, null);


                if (this.rect != null) {
                    g.setColor(Color.WHITE);
                    g.drawRect(this.rect.x, this.rect.y, this.rect.width, this.rect.height);
                }
            }
        }
    }

    
    private boolean alwaysDrawRectangle = false;
       
        
        
    public void setImage(Image image) {
        this.image = image;
        if(this.alwaysDrawRectangle == false) {
            this.rect = null;
        }
        repaint();
    }  
    
    public void setImage(LPFrame frame) {
        this.image = frame.toImage();
        if(this.alwaysDrawRectangle == false) {
            this.rect = null;
        }
        repaint();
    }
    
    public void drawRectangle(Rectangle rect) {
        this.rect = rect;
        repaint();
    }
    
    public void drawRectangleAlways(Rectangle rect) {
        this.rect = rect;
        this.alwaysDrawRectangle = true;
    }
    
    public void setImage(double [][] data) {
        this.setImage(toImage(data));
    }
    
    public void clear() {
        this.image = null;
        this.rect = null;
        repaint();
    }
    
    private BufferedImage toImage(double[][] data)
    {
        int[] intImage = new int[data.length * data[0].length];
        int width = data.length;
        int height = data[0].length;

        // this sucks, but there is no other way to convert the arrays (at least i
        // do not know)
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pixel = (int) data[x][y];

                /* display as grayscale image */
                intImage[y * width + x] = (int) pixel | pixel << 8 | pixel << 16;
            }
        }
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        image.setRGB(0, 0, width, height, intImage, 0, width);
        return image;
    }

    @Override
    public void refreshFrame(Image img, long count)
    {
        this.setImage(img);
    }
    
    
}
