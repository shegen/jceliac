/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.AbstractDataset;
import org.jfree.data.general.Dataset;
import org.jfree.data.statistics.HistogramDataset;

/**
 *
 * @author shegen
 */
public class ChartVisualizationFrame extends JFrame
{

    protected ChartPanel chartPanel;
    //protected ArrayList <Double []> data;
    protected AbstractDataset data;
    protected ChartType chartType;
    
    public static enum ChartType  {
        Histogram,
        LuminanceHistogram,
        BarChart
    };
    
    public ChartVisualizationFrame(ChartType type) 
            throws HeadlessException
    {
        super();
        this.setSize(new Dimension(500,500));
        this.chartType = type;
    }
    
    private int count = 0 ;
    
    /**
     *
     * @param chart 
     */
    private void repaintFrame(JFreeChart chart) 
    {
        this.chartPanel = new ChartPanel(chart);
        this.chartPanel.setSize(500, 500);
        this.getContentPane().removeAll();
        this.getContentPane().add(this.chartPanel);
        
        this.invalidate();
        this.validate();
        this.repaint();
    }
    
    private JFreeChart createChart(ChartType type) 
    {
        switch(type) {
            case Histogram:
            case LuminanceHistogram:
                return ChartFactory.createHistogram("Histogram","X", "Y", (HistogramDataset)this.data, PlotOrientation.VERTICAL, true, true, true);
            case BarChart:
                return ChartFactory.createBarChart("Bar Chart", "X", "Y", (CategoryDataset)this.data, PlotOrientation.VERTICAL, true, true, true);
        }
        return null;
    }
    
    public void setData(AbstractDataset dataset)
    {
        this.data = dataset;
        JFreeChart chart = this.createChart(this.chartType);
        this.repaintFrame(chart);
    }
    
    // <editor-fold defaultstate="collapsed" desc="Data Conversion Utilities">
    private double [] convertToPrimitive(Double [] array) 
    {
        double [] retArray = new double[array.length];
        for(int i = 0; i < retArray.length; i++) {
            retArray[i] = (double) array[i];
        }
        return retArray;
    }
    private Double [] convertToDouble(double [] array) 
    {
        Double [] retArray = new Double[array.length];
        for(int i = 0; i < array.length; i++) {
            retArray[i] = (Double) array[i];
        }
        return retArray;
    }
    // </editor-fold>
  
    
}
