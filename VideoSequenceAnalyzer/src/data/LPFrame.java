/*
 *  JCeliac Copyright (C) 2010-2012 by Sebastian Hegenbart <shegen at cosy.sbg.ac.at>
 * 
 *  The source code of this software is not yet made available to the open source 
 *  community. If you obtained this source code without permission of the author 
 *  please remove all the source files.
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 */
package data;

import com.xuggle.xuggler.IVideoPicture;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorConvertOp;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import javax.swing.GrayFilter;
import jceliac.tools.data.Frame;
import java.lang.Cloneable;

/**
 * A Low Precision Frame representation based on the frames storing pixels as double values. 
 * @author shegen
 */
public class LPFrame
{
    // identification variables
    private String signalIdentifier;
    private int frameIdentifier;
    
    // data variables
    protected int [][] grayData;
    protected int [][] greenData;
    protected int [][] blueData;
    protected int [][] redData;
    
    private int[][] luminanceData;
    private int[][] chromAData;
    private int[][] chromBData;
    
    private BufferedImage bufferedImage;
    private IVideoPicture videoPicture; 
    
    // state variables
    private int colorspace;
    public static final int COLORSPACE_RGB = 1;
    public static final int COLORSPACE_LAB = 2;
    private int width = -1;
    private int height = -1;
    private boolean columnFirstStored = true; // default
    

    public LPFrame(String signalID, int frameID)
    {
        this.signalIdentifier = signalID;
        this.frameIdentifier = frameID;
        this.colorspace = COLORSPACE_RGB;
    }

    public LPFrame(String signalID, int frameID, int colorspace)
    {
        this.signalIdentifier = signalID;
        this.frameIdentifier = frameID;
        this.colorspace = colorspace;
    }
    
    public LPFrame()
    {
        this.colorspace = COLORSPACE_RGB;
    }

    public LPFrame(int colorspace)
    {
        this.colorspace = colorspace;
    }
    
    // if this is set, we do not convert the picture until we need the data
    // this is an optimization:
    // if frames are skipped by the FrameProcessingStrategy we do not need to convert them which takes up a lot of
    // time
    public void setVideoPicture(IVideoPicture picture) {
        this.videoPicture = picture;
    }
    
    public int[][] getBlueData()
    {
        return blueData;
    }

    public int[][] getGrayData()
    {
        return grayData;
    }

    public void setBlueData(int[][] blueData)
    {
        this.blueData = blueData;
        
        if(this.columnFirstStored) {
            this.width = blueData.length;
            this.height = blueData[0].length;
        }else {
            this.width = blueData[0].length;
            this.height = blueData.length;
        }
    }

    public void setGrayData(int[][] grayData)
    {
        this.grayData = grayData;
        if(this.columnFirstStored) {
            this.width = grayData.length;
            this.height = grayData[0].length;
        }else {
            this.width = grayData[0].length;
            this.height = grayData.length;
        }
    }

    public void setGreenData(int[][] greenData)
    {
        this.greenData = greenData;
        
         if(this.columnFirstStored) {
            this.width = greenData.length;
            this.height = greenData[0].length;
        }else {
            this.width = greenData[0].length;
            this.height = greenData.length;
        }
         
    }

    public void setRedData(int[][] redData)
    {
        this.redData = redData;
        
        if(this.columnFirstStored) {
            this.width = redData.length;
            this.height = redData[0].length;
        }else {
            this.width = redData[0].length;
            this.height = redData.length;
        }
        
    }
    
    public void setData(BufferedImage image) {
        this.bufferedImage = image;
        this.width = image.getWidth();
        this.height = image.getHeight();

    }

    public int[][] getGreenData()
    {
        if(this.greenData == null) { // find out what kind of data is set
            
        }
        return greenData;
    }

    public int[][] getRedData()
    {
        return redData;
    }

    public int getFrameIdentifier()
    {
        return frameIdentifier;
    }

    public void setFrameIdentifier(int frameIdentifier)
    {
        this.frameIdentifier = frameIdentifier;
    }

    public String getSignalIdentifier()
    {
        return signalIdentifier;
    }

    public void setSignalIdentifier(String signalIdentifier)
    {
        this.signalIdentifier = signalIdentifier;
    }

    public int getWidth()
    {
        return this.width;
    }

    public int getHeight()
    {
        return this.height;
    }

    public int[][] getChromAData()
    {
        return chromAData;
    }

    public void setChromAData(int[][] chromAData)
    {
        this.chromAData = chromAData;
        this.width = chromAData.length;
        this.height = chromAData[0].length;
    }

    public int[][] getChromBData()
    {
        return chromBData;
    }

    public void setChromBData(int[][] chromBData)
    {
        this.chromBData = chromBData;
        this.width = chromBData.length;
        this.height = chromBData[0].length;
    }

    public int[][] getLuminanceData()
    {
        return luminanceData;
    }

    public void setLuminanceData(int[][] luminanceData)
    {
        this.luminanceData = luminanceData;
        this.width = luminanceData.length;
        this.height = luminanceData[0].length;
    }

    public int getColorspace()
    {
        return colorspace;
    }

    public boolean equals(LPFrame other)
    {
        // first check grayscale version
        int[][] oGray = other.getGrayData();
        if (arraysEqual(oGray, this.grayData) == false) {
            return false;
        }

        // check color channels
        int[][] oRed = other.getRedData();
        if (arraysEqual(oRed, this.redData) == false) {
            return false;
        }
        int[][] oGreen = other.getGreenData();
        if (arraysEqual(oGreen, this.getGreenData()) == false) {
            return false;
        }
        int[][] oBlue = other.getBlueData();
        if (arraysEqual(oBlue, this.blueData) == false) {
            return false;
        }

        return true;
    }

    public boolean almostEquals(LPFrame other)
    {
        // first check grayscale version
        int[][] oGray = other.getGrayData();
        if (arraysAlmostEqual(oGray, this.grayData) == false) {
            return false;
        }

        // check color channels
        int[][] oRed = other.getRedData();
        if (arraysAlmostEqual(oRed, this.redData) == false) {
            return false;
        }
        int[][] oGreen = other.getGreenData();
        if (arraysAlmostEqual(oGreen, this.getGreenData()) == false) {
            return false;
        }
        int[][] oBlue = other.getBlueData();
        if (arraysAlmostEqual(oBlue, this.blueData) == false) {
            return false;
        }

        return true;
    }

    public boolean arraysEqual(int[][] c1, int[][] c2)
    {
        for (int i = 0; i < c1.length; i++) {
            for (int j = 0; j < c1[0].length; j++) {
                if (c1[i][j] != c2[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean arraysAlmostEqual(int[][] c1, int[][] c2)
    {
        for (int i = 0; i < c1.length; i++) {
            for (int j = 0; j < c1[0].length; j++) {
                double tmp1 = c1[i][j];
                double tmp2 = c2[i][j];

                double diff = tmp1 - tmp2;

                if (Math.abs(diff) > 10) {
                    return false;
                }
            }
        }
        return true;
    }

    public BufferedImage toImage()
    {


        int[] intImage = new int[this.width*this.height];

        // this sucks, but there is no other way to convert the arrays (at least i
        // do not know)
        for (int x = 0; x < this.width; x++) {
            for (int y = 0; y < this.height; y++) {
                
                if(this.columnFirstStored) 
                {
                    int greenPixel = (int) greenData[x][y];
                    int redPixel = (int) redData[x][y];
                    int bluePixel = (int) blueData[x][y];
                    intImage[y * width + x] = (int) (255 << 24 | redPixel << 16 | greenPixel << 8 | bluePixel);
                }else 
                {
                    int greenPixel = (int) greenData[y][x];
                    int redPixel = (int) redData[y][x];
                    int bluePixel = (int) blueData[y][x];
                    intImage[y * width + x] = (int) (255 << 24 | redPixel << 16 | greenPixel << 8 | bluePixel);
                }
            }
        }
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        bufferedImage.setRGB(0, 0, width, height, intImage, 0, width);
        return bufferedImage;
    }

    public void crop(int startX, int startY, int dim)
    {
        int[][] newGrayData = new int[dim][dim];
        int[][] newRedData = new int[dim][dim];
        int[][] newGreenData = new int[dim][dim];
        int[][] newBlueData = new int[dim][dim];


        for (int j = 0; j < dim; j++) {
            System.arraycopy(this.grayData[j + startX], startY, newGrayData[j], 0, dim);
            System.arraycopy(this.greenData[j + startX], startY, newGreenData[j], 0, dim);
            System.arraycopy(this.redData[j + startX], startY, newRedData[j], 0, dim);
            System.arraycopy(this.blueData[j + startX], startY, newBlueData[j], 0, dim);
        }
        this.greenData = newGreenData;
        this.grayData = newGrayData;
        this.redData = newRedData;
        this.blueData = newBlueData;
    }

    public short[][] getColoredData()
    {
        short[][] colored = new short[this.getWidth()][this.getHeight()];

        if (this.colorspace == Frame.COLORSPACE_RGB) {
            if (this.redData != null) {
                for (int i = 0; i < this.getWidth(); i++) {
                    for (int j = 0; j < this.getHeight(); j++) {
                        int red = (int) redData[i][j];
                        int green = (int) greenData[i][j];
                        int blue = (int) blueData[i][j];

                        int curPixel = (blue | (green << 8) | (red << 16) | (255 << 24));
                        colored[i][j] = (short) curPixel;
                    }
                }
            } else if (this.grayData != null) {

                for (int i = 0; i < this.getWidth(); i++) {
                    for (int j = 0; j < this.getHeight(); j++) {
                        int gray = (int) grayData[i][j];

                        int curPixel = (gray | (gray << 8) | (gray << 16) | (255 << 24));
                        colored[i][j] = (short) curPixel;
                    }
                }
            }
        } else if (this.colorspace == Frame.COLORSPACE_LAB) {
            for (int i = 0; i < this.getWidth(); i++) {
                for (int j = 0; j < this.getHeight(); j++) {
                    int l = (int) luminanceData[i][j];
                    int a = (int) chromAData[i][j];
                    int b = (int) chromBData[i][j];

                    int curPixel = (l | (a << 8) | (b << 16) | (255 << 24));
                    colored[i][j] = (short) curPixel;
                }
            }
        }

        return colored;
    }
    
    public double [][] getNormalizedRedData()
    { 
        double [][] normalizedData = new double[this.redData.length][this.redData[0].length];
        
        for(int i = 0; i < this.redData.length; i++) {
            for(int j = 0; j < this.redData[0].length; j++) {
                normalizedData[i][j] = this.redData[i][j] / 255.0d;
            }
        }
        return normalizedData;
    }
    
    public double [][] getNormalizedGreenData()
    { 
        double [][] normalizedData = new double[this.greenData.length][this.greenData[0].length];
        
        for(int i = 0; i < this.greenData.length; i++) {
            for(int j = 0; j < this.greenData[0].length; j++) {
                normalizedData[i][j] = this.greenData[i][j] / 255.0d;
            }
        }
        return normalizedData;
    }
    
    public double [][] getNormalizedBlueData()
    { 
        double [][] normalizedData = new double[this.blueData.length][this.blueData[0].length];
        
        for(int i = 0; i < this.blueData.length; i++) {
            for(int j = 0; j < this.blueData[0].length; j++) {
                normalizedData[i][j] = this.blueData[i][j] / 255.0d;
            }
        }
        return normalizedData;
    }
    
    public double [][] getNormalizedGrayData()
    { 
        double [][] normalizedData = new double[this.grayData.length][this.grayData[0].length];
        
        for(int i = 0; i < this.grayData.length; i++) {
            for(int j = 0; j < this.grayData[0].length; j++) {
                normalizedData[i][j] = this.grayData[i][j] / 255.0d;
            }
        }
        return normalizedData;
    }
    
    
   
    public static LPFrame convertBufferedImageToFrameOptimized(BufferedImage image, boolean storeByColumns)
    {
        int[][] greenData;
        int[][] redData;
        int[][] blueData;
        int[][] grayData;
        LPFrame frame = null;

        try {
            int w = image.getWidth(null);
            int h = image.getHeight(null);

            // the documentation is not totally clear here, it looks like the 0 channel is
            // always red, does not matter if its an RGB or BGR image
            int[] red1D = image.getRaster().getSamples(0, 0, w, h, 0, (int[]) null);
            int[] green1D = image.getRaster().getSamples(0, 0, w, h, 1, (int[]) null);
            int[] blue1D = image.getRaster().getSamples(0, 0, w, h, 2, (int[]) null);



            if (storeByColumns) {
                greenData = new int[w][h];
                redData = new int[w][h];
                blueData = new int[w][h];
                grayData = new int[w][h];
            } else {
                greenData = new int[h][w];
                redData = new int[h][w];
                blueData = new int[h][w];
                grayData = new int[h][w];
            }
            frame = new LPFrame();

            if (storeByColumns == false) 
            {
                
                BufferedImage grayImage = new BufferedImage(w,h, BufferedImage.TYPE_BYTE_GRAY);
                Graphics g = grayImage.getGraphics();
                g.drawImage(image, 0, 0, null);
                g.dispose();
                int[] gray1D = grayImage.getRaster().getSamples(0, 0, w, h, 0, (int[]) null);
                
                for (int i = 0; i < h; i++) {
                    System.arraycopy(red1D, i * w, redData[i], 0, w);
                    System.arraycopy(green1D, i * w, greenData[i], 0, w);
                    System.arraycopy(blue1D, i * w, blueData[i], 0, w);
                    System.arraycopy(gray1D, i * w, grayData[i], 0, w);
                }
                frame.setColumnFirstStored(false);
            } else {

                for (int i = 0; i < w; i++) {
                    for (int j = 0; j < h; j++) {
                        int index = (j * w + i);

                        int red = (int) red1D[index];
                        int green = (int) green1D[index];
                        int blue = (int) blue1D[index];

                        System.out.println("Warning using slow Grayscale conversion!");
                        /* Be cautios, if you do not round you will screw up the
                         * result. This is not totaly compliant to the Matlab code,
                         * rounding errors.
                         */
                        int gray = (int) ((red * 0.2989 + green * 0.5870 + blue * 0.1140) + 0.5);

                        /* Unfortunately we cannot use arraycopy for this because due 
                         * to the type arrays are constructed we would have y,x instead of
                         * x,y and needed another transpose which destroyed the gained
                         * performance. 
                         */

                        // store by column so that data[n] gives the n-th column
                        greenData[i][j] = green1D[index];
                        redData[i][j] = red1D[index];
                        blueData[i][j] = blue1D[index];
                        grayData[i][j] = gray;

                    }
                }
            }
            frame.setBlueData(blueData);
            frame.setGreenData(greenData);
            frame.setRedData(redData);
            frame.setGrayData(grayData);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return frame;
    }
    
    // this automatically stores by columns so that data[n] gives the n-th column
    public static LPFrame convertBufferedImageToFrameOptimized(BufferedImage image)
    {
        int [][] greenData;
        int [][] redData;
        int [][] blueData;
        int [][] grayData;
        LPFrame frame = null;

        try {
            int w = image.getWidth(null);
            int h = image.getHeight(null);
            
            // the documentation is not totally clear here, it looks like the 0 channel is
            // always red, does not matter if its an RGB or BGR image
            int [] red1D = image.getRaster().getSamples(0,0, w, h, 0, (int [])null);
            int [] green1D = image.getRaster().getSamples(0,0, w, h, 1, (int [])null);
            int [] blue1D = image.getRaster().getSamples(0,0, w, h, 2, (int [])null);
            
            greenData = new int [w][h];
            redData = new int [w][h];
            blueData = new int [w][h];
            grayData = new int [w][h];
            
            frame = new LPFrame();
            for (int i = 0; i < w; i++) {
                for (int j = 0; j < h; j++) {
                    int index = (j * w + i);
                    
                    int red = (int)red1D[index];
                    int green = (int)green1D[index];
                    int blue = (int)blue1D[index];
                    
                    /* Be cautios, if you do not round you will screw up the
                     * result. This is not totaly compliant to the Matlab code,
                     * rounding errors.
                     */
                    int gray = (int) ((red * 0.2989 + green * 0.5870 + blue * 0.1140) + 0.5);
                    
                    /* Unfortunately we cannot use arraycopy for this because due 
                     * to the type arrays are constructed we would have y,x instead of
                     * x,y and needed another transpose which destroyed the gained
                     * performance. 
                     */
                     // store by column so that data[n] gives the n-th column
                     greenData[i][j] = (int)green1D[index];
                     redData[i][j] = (int)red1D[index];
                     blueData[i][j] = (int)blue1D[index]; 
                     grayData[i][j] = (int) gray;
                }
            }
            frame.setBlueData(blueData);
            frame.setGreenData(greenData);
            frame.setRedData(redData);
            frame.setGrayData(grayData);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return frame;
    }

    public boolean isColumnFirstStored()
    {
        return columnFirstStored;
    }

    public void setColumnFirstStored(boolean columnFirstStored)
    {
        this.columnFirstStored = columnFirstStored;
    }
    
    
    
}
