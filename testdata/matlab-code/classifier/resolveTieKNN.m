function [ resolvedClass ] = resolveTieKNN( classLabels )
%RESOLVETIEKNN Resolves a tie based on class label distribution. 

    % compute class label frequencies
    frequencies = histc(classLabels, unique(classLabels));
    frequencies = frequencies / sum(frequencies);
    frequencies = cumsum(frequencies);
    
    % generate random value between 0 and 1
    randValue = rand();
    
    % find bin of randValue
    resolvedClass = min(find(randValue < frequencies));
end

