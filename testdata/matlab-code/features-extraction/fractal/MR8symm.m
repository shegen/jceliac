function [featvec]=MR8symm(im)


F=makeRFSfilters;

i=1;
for j=1:6,
    for k=1:6
        m=(j-1)*6+k;
        % either correlation or convolution will yield the same feature
        % vectors later
        im1 = imfilter(im,F(:,:,m),'symmetric');
     
        % take max of abs response for first order derivative
        % Varma&Zisserman also take abs max of second order...
        im1 = abs(im1);
        
        if (k==1)
            maxim1 = im1;
        else
            maxim1 = max(maxim1, im1);
        end
    end

    ims{i} = maxim1; i=i+1;

end



ims{i} = imfilter(im,F(:,:,37),'symmetric');%gaussian filter
i=i+1;
ims{i} = imfilter(im,F(:,:,38),'symmetric');%laplacian of gaussian


featvec = [ims{8}(:) ims{7}(:) ims{1}(:) ims{3}(:) ims{5}(:) ims{2}(:) ims{4}(:) ims{6}(:)]';
