function [histogram]=fractalmr8clustertestset(data,dictionary,varargin)
%classifisd the images by partition an image ion binary images and computing the fractal dimension of these binary images. 


p = inputParser;
    p.addRequired('data',@iscell);
    p.addParamValue('debug',false,@islogical);
    p.addParamValue('preprocess',false,@islogical);
    p.addParamValue('method','sobel', @(x)any(strcmpi(x,{'original','laplace','gradient','gradient4','normal','sobel','versuch','sobel2'})));
    p.addParamValue('colorspace','rgb',@(x)any(strcmpi(x,{'rgb','ybr','hsv','yiq','rg','r','g','b'})));
    p.addParamValue('grayscale',true,@islogical);
    %computing the distance beween the feature vectors by euclidean distance or the Earth Movers Distance
    p.addParamValue('normalizing',false,@islogical);
    p.addParamValue('datatest',{},@iscell);%for train-and testset (datatest is the testset, data the trainset)
    p.addParamValue('dataset','gastro',@(x)any(strcmpi(x,{'gastro','pit','sonstige'})));
    %kind of imagedata (for the information how much classes are existing
    p.addParamValue('filter','symmetric' ,@(x)any(strcmpi(x,{'valid','symmetric'})));
    p.addParamValue('mr8','symmetric' ,@(x)any(strcmpi(x,{'valid','symmetric','imfilter','symmetric_and_smaller'})));
    p.addParamValue('real2class',false,@islogical);%build clusters only for the 2 classes. Result is only able for the 2-class-case
    p.addParamValue('mr8select','normal',@(x)any(strcmpi(x,{'normal','part'})));
    p.addParamValue('mr8dct',false,@islogical);
    p.addParamValue('fractaldct',false,@islogical);
    p.addParamValue('mr8abs',true,@islogical);%ablolute Value of the filter responses
    p.addParamValue('reduceFD',false,@islogical);
    p.addParamValue('reduce_factor',10, @(x) x>=1 && x <=100);
    p.addParamValue('precluster',false,@islogical);%reduce fractal data
    p.addParamValue('preclusters',20, @(x) x>=1 && x <=200);
    p.parse(data,varargin{:});
    debug = p.Results.debug;
    grayscale = p.Results.grayscale;
    preprocess = p.Results.preprocess;
    method = p.Results.method;
    colorspace = p.Results.colorspace;
    normalizing = p.Results.normalizing;
    datatest = p.Results.datatest;
    dataset=p.Results.dataset;
    filter = p.Results.filter;
    mr8 = p.Results.mr8;
    real2class = p.Results.real2class;
    mr8select = p.Results.mr8select;
    mr8dct = p.Results.mr8dct;
    fractaldct = p.Results.fractaldct;
    mr8abs =p.Results.mr8abs;
    reduceFD = p.Results.reduceFD;
    reduce_factor = p.Results.reduce_factor;
    precluster = p.Results.precluster;
    preclusters = p.Results.preclusters;
    

fractaldata={};
classTextonCluster={};
dim=[];

if (real2class)
    for i=1:length(data)
        switch dataset
            case 'gastro'
                if data{i}.idx > 2
                    data{i}.idx=2;
                end
            case 'pit'
                if data{i}.idx <= 2
                    data{i}.idx=1;
                else
                    data{i}.idx=2;
                end
        end
    end

    for i=1:length(datatest)
        switch dataset
            case 'gastro'
                if datatest{i}.idx > 2
                    datatest{i}.idx=2;
                end
            case 'pit'
                if datatest{i}.idx <= 2
                    datatest{i}.idx=1;
                else
                    datatest{i}.idx=2;
                end
        end
    end
end        
for m=1:length(data);
    if (debug)
        tic;
    end
    image=data{m}.image;
    dimension=[];
    if (grayscale && size(image,3)==3)
            image = rgb2gray(image);
    else
            switch colorspace
                case 'rgb'
                    image = double(image);
                case 'ybr'
                    image = double(rgb2ycbcr(image)); % convert to YBR color model
                case 'hsv'
                    image = double(rgb2hsv(image));
                case 'yiq'
                    image = double(rgb2ntsc(image));
                case 'rg'
                    image = double(image(:,:,1:2));
                case 'r'
                    image = double(image(:,:,1));
                case 'g'
                    image = double(image(:,:,2));
                case 'b'
                    image = double(image(:,:,3));
             end
     end
    
    
    for n=1:size(image,3)
         muB_laplace=[];
         im1=image(:,:,n);
       

         if (preprocess)
            warning off
            im1=adapthisteq(uint8(im1));
            warning on
         end

        im1=double(im1);
        
        switch mr8
            case 'valid'
                filtered=MR8fast(im1);%Verkleinerung des Bildes. Der Rand deranisotropic filterung wir nicht verwendet (entspricht 'valid' bei conv2) 
            case 'symmetric'
                filtered=MR8fast2(im1);%image keeps the same size
            case 'imfilter'
                filtered=MR8symm(im1);%self made Version with function  imfilter (symmetric filtering) 
            case 'symmetric_and_smaller'
                filtered=MR8fast3(im1);%image keeps same size and scale is 1/4 of the scale in Mr8fast or MR8fast2.
        end
        
        if (mr8abs)
            filtered=abs(filtered);
        end
        
        if (mr8dct)
            for i=1:length(filtered)
                filtered([3,4,5],i)=dct(filtered([3,4,5],i));
                filtered([6,7,8],i)=dct(filtered([6,7,8],i));
            end
        end
        
        
        switch mr8select
            case 'normal'
                filtered=filtered;
            case 'part'
                filtered=filtered([2,4,5,6,8],:);%without gaussian smallest scale edge filter and medium scale bar filter
        end
                
        is_nan=sum(sum(sum(isnan(filtered))));
        if (is_nan > 0)
            warning('filtered image contains NAN. Bild %d',m);
        end
        
        if (strcmp(mr8,'symmetric') | strcmp(mr8,'imfilter'))%also useable for images that are not quadratic    
            sizefiltered1=size(im1,1);;%die 8 filtered Bilder 
        %sind nach fkt. mr8 nur Vektoren und müsssen wieder zu
        %Matrizzen gemacht werden.
            sizefiltered2=size(im1,2);
        else  %not  useable for images that are not quadratic
            sizefiltered1=sqrt(size(filtered,2))
            sizefiltered2=sqrt(size(filtered,2));
        end
        filteredImage=[];
        for p=1:size(filtered,1)
             filteredImage(:,:,p)=reshape(filtered(p,:),[sizefiltered1,sizefiltered2]);
        end
        filteredImage=double(filteredImage);
                                

        
        for filt=1:size(filtered,1)
            im=filteredImage(:,:,filt);
            sizeimy=size(im,1);
            sizeimx=size(im,2);
            muB=[];
            sigma=0.5;
            for r=1:8
                rmask=zeros(2*r-1,2*r-1);%All points with radius <= r (from the center) are ones, the others are zeros
                for i=1:2*r-1
                    for j=1:2*r-1
                        if  sqrt((i-r)^2+(j-r)^2)<=r-1
                            rmask(i,j)=1;
                        end
                    end
                end
                gaussian=zeros(2*r-1,2*r-1);%gaussian mask for blurring.
                for x=1:2*r-1
                    for y=1:2*r-1
                        gaussian(x,y)=1/(r*sigma*sqrt(2*pi))*exp(- ( sqrt(sum((x-r)^2+(y-r)^2)))/(2* sigma^2 *r^2));
                    end
                end
                gaussian=gaussian/sum(sum(gaussian));
                switch filter
                    case 'valid'
                        imblurred=conv2(im,gaussian,'valid');%gaussian mask
                    case 'symmetric'
                        imblurred=imfilter(im,gaussian,'symmetric');
                end

                switch method
                    
                     case 'original'
                         switch filter
                            case 'valid'
                                sums=conv2(im,rmask,'valid');
                            case 'symmetric'
                                sums=imfilter(im,rmask,'symmetric');
                         end       
                    
                    case 'laplace'
                        [fx,fy]=gradient(imblurred);
                        fxx=gradient(fx);
                        [s,fyy]=gradient(fy);
                        laplace=abs(fxx+fyy);%Laplace-operator von imblurred
                        switch filter
                            case 'valid'
                                sums=conv2(laplace,rmask,'valid');%integral (Summe der Helligkeitswerte) von imblurred über der Kreisscheibe rmask
                            case 'symmetric'
                                sums=imfilter(laplace,rmask,'symmetric');
                        end

                    case 'gradient'
                        [fx,fy]=gradient(imblurred);
                        grad=abs(fx+fy);
                         switch filter
                            case 'valid'
                                sums=conv2(grad,rmask,'valid');
                            case 'symmetric'
                                sums=imfilter(grad,rmask,'symmetric');
                        end               
                    case 'gradient4'
                        [fx,fy]=gradient(imblurred);
                        sro=[0,0,0.5;0 0 0;-0.5,0,0];%schräg nach rechts oben
                        slo=[0.5,0,0;0 0 0;0 0 -0.5];%schräg nach links oben
                        fsro=imfilter(imblurred,sro,'symmetric');
                        fslo=imfilter(imblurred,slo,'symmetric');
                        grad=abs(fx+fy+fsro+fslo);
                        sums=conv2(grad,rmask,'valid');
                    case 'normal'
                         switch filter
                            case 'valid'
                                sums=conv2(imblurred,rmask,'valid');
                            case 'symmetric'
                                sums=imfilter(imblurred,rmask,'symmetric');
                         end     
                    case 'sobel'
                        sobely=fspecial('sobel')/4;
                        sobelx=-sobely';
                        sobel_fx=imfilter(imblurred, sobelx ,'symmetric');
                        sobel_fy=imfilter(imblurred, sobely ,'symmetric');
                        sobel=abs(sobel_fx + sobel_fy);
                        switch filter
                            case 'valid'
                                sums=conv2(sobel,rmask,'valid');
                            case 'symmetric'
                                sums=imfilter(sobel,rmask,'symmetric');
                        end  
                         
                    case 'sobel2'% absolutwerte von sobel_fx und sobel_fy addieren.
                        sobely=fspecial('sobel')/4;
                        sobelx=-sobely';
                        sobel_fx=imfilter(imblurred, sobelx ,'symmetric');
                        sobel_fy=imfilter(imblurred, sobely ,'symmetric');
                        sobel=abs(sobel_fx) + abs(sobel_fy);
                        switch filter
                            case 'valid'
                                sums=conv2(sobel,rmask,'valid');
                            case 'symmetric'
                                sums=imfilter(sobel,rmask,'symmetric');
                        end  

                    case 'versuch'
                        sobely=fspecial('sobel')/4;
                        sobelx=-sobely';
                        sobel_fx=imfilter(im, sobelx ,'symmetric');
                        sobel_fy=imfilter(im, sobely ,'symmetric');
                        vers=abs(sobel_fx) + abs(sobel_fy);
                         switch filter
                            case 'valid'
                                sums=conv2(vers,rmask,'valid');
                            case 'symmetric'
                                sums=imfilter(vers,rmask,'symmetric');
                         end  

                end



                switch filter
                    case 'valid'
                        size_sumsy=size(sums,1);%größe des Bildes nimmt mit jeder Convolution ab.
                        size_sumsx=size(sums,2);
                        %bei r=8 ist die Größe Bildes nur mehr 100*100 (bei 128*128 Originalgröße)
                        %(128  -  2*(8-1)[1. conv=blurring]  - 2*(8-1)[2. conv=sums]  =  100)
                        size_sums_r8y=sizeimy-2*(8-1)-2*(8-1);
                        size_sums_r8x=sizeimx-2*(8-1)-2*(8-1);
                        sy=size_sums_r8y;
                        sx=size_sums_r8x;
                        cy=(size_sumsy - size_sums_r8y)/2;
                        cx=(size_sumsx - size_sums_r8x)/2;
                        sums_same_size=sums(1+cy:size_sums_r8y+cy , 1+cx:size_sums_r8x+cx);
                        muB_laplace(:,:,r)=sums_same_size;

                    case 'symmetric'
                        muB_laplace(:,:,r)=sums;
                        sx=sizeimx;
                        sy=sizeimy;
                end


                muB_laplace=muB_laplace + double(muB_laplace<=0);%Alle Nullen werden durch Einsen ersetzt
                %da ich sonst den logarithmus nicht nehmen kann

                muB=muB_laplace;


            end


            rlog=log([1:8]);
            alpha=[];
            h=[];
            for i=1:sy
                for j=1:sx
                    for k=1:8
                        h(k)=log(muB(i,j,k)); 
                    end
                    alpha(i,j)=regress(  h'-h(1), rlog');%linear fitting des Anstiegs.  Fractale dim for every Pixel.
                    %der Geraden von  log mu(B(x,r) (h) gegen log r
                   
                end
            end
            if(reduceFD)
                fractaldata{m}(:,filt)=alpha(1:reduce_factor:end);
            else
                fractaldata{m}(:,filt)=alpha(:);
            end
            
        end
    end
     if (precluster)
        [idx,frac_help] = kmeans(fractaldata{m},preclusters,'MaxIter',200);
        fractaldata{m}=frac_help;
    end
    if (debug)
        tim=toc;
        if(mod(m,10)==0)
            tim=toc;
            fprintf('%d. Bild : 1   Bild in %0.2f [s]\n' ,m,  tim);
        end
    end
end

if (fractaldct)
    for m=1:length(fractaldata)
            for i=1:size(fractaldata{m},1)
                fractaldata{m}(i,[3,4,5])=dct(fractaldata{m}(i,[3,4,5]));
                fractaldata{m}(i,[6,7,8])=dct(fractaldata{m}(i,[6,7,8]));

            end
    end
end

for i=1:length(data);
    indexset(i)=data{i}.idx;
end



for i=1:length(data)
    D=disteu(dictionary',fractaldata{i}');
    [x,y]=min(D);%find the nearest reprsentative features (=Textons  for every "Pixel-feature" of every image
    fractalmodel=y;
    n=hist(fractalmodel,[1:size(dictionary,1)]);% counts the nearest Textons  
    if (normalizing)
        n=n/sum(n);  % to normalize the histogram. Necessary when images have
        %different sizes
    end
    histogram(:,i)=n';%Histogram, that counts how often each Texton occurs (is the nearest neighbor of a "pixel")  
end



