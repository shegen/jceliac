function [result,distmat,histogram,result_2class,result_4class,R,dictionary,TPRFPR,NN]=fractalmr8cluster(data,varargin)
%Makes clusters of the fractaldimensions of the the mr8-filterresponses for each class,
%builds with this clusters models of each image and then compares these models to classify an image.  
% code by gwimmer

% CALL with: fractalmr8cluster(cell, 'method','original','kvalue',1, 'debug', true, 'mr8select', 'part')

p = inputParser;
    p.addRequired('data',@iscell);%trainset if 2 test and training set
    p.addParamValue('debug',false,@islogical);
    p.addParamValue('preprocess',false,@islogical);
    p.addParamValue('method','sobel', @(x)any(strcmpi(x,{'original','laplace','gradient','gradient4','normal','sobel','sobel2','versuch','sobel3'})));
    p.addParamValue('colorspace','rgb',@(x)any(strcmpi(x,{'rgb','ybr','hsv','yiq','rg','r','g','b'})));%momemtly not possible, only grayscale images
    p.addParamValue('grayscale',true,@islogical);
    %computing the distance beween the feature vectors by euclidean distance or the Earth Movers Distance
    p.addParamValue('normalizing',false,@islogical);
    p.addParamValue('datatest',{},@iscell);%for train-and testset (datatest is the testset, data the trainset)
    p.addParamValue('dataset','gastro',@(x)any(strcmpi(x,{'gastro','pit','sonstige'})));
    %kind of imagedata (for the information how much classes are existing
    p.addParamValue('min_k',2, @(x) x>=1 && x <95);%only for distinct sets. Minimum k_value for Optimization of k=min_k. 
    p.addParamValue('kvalue',50, @(x) x>=1 && x <300);%kvalue for knn
    p.addParamValue('filter','symmetric' ,@(x)any(strcmpi(x,{'valid','symmetric'})));
    p.addParamValue('mr8','imfilter' ,@(x)any(strcmpi(x,{'valid','symmetric','imfilter','symmetric_and_smaller'})));%default was 'symmetric', but now it do not wor anymore (64-bit pc)
    p.addParamValue('image2cluster',13, @(x) x>=1 && x <300);%from how much images the features are clustered by k-means 
    p.addParamValue('clusters_all',true,@islogical);
    p.addParamValue('clusters',10, @(x) x>=1 && x <95);%how much clusters are made out of the features of the image2cluster images.
    p.addParamValue('secondcluster',false,@islogical);
    p.addParamValue('real2class',false,@islogical);%build clusters only for the 2 classes. Result is only able for the 2-class-case
    p.addParamValue('mr8select','part',@(x)any(strcmpi(x,{'normal','part'})));
    p.addParamValue('mr8dct',false,@islogical);
    p.addParamValue('fractaldct',false,@islogical);
    p.addParamValue('mr8abs',true,@islogical);%absolute Value of the filter responses
    p.addParamValue('lopo',false,@islogical);%leave-one -patient-out for pit pattern
    p.addParamValue('reduceFD',false,@islogical);%reduce fractal data
    p.addParamValue('reduce_factor',10, @(x) x>=1 && x <=100);%Take only one of #reducefactor features
    p.addParamValue('precluster',false,@islogical);%reduce fractal data =BLÖDSINN
    p.addParamValue('preclusters',20, @(x) x>=1 && x <=200);%reduce  to preclusters clusters (by kmeans)
    p.addParamValue('kthscales',false,@islogical);%especially for the KTH tips database scale tests
    p.addParamValue('CUReTscales',false,@islogical);%especially for the CUReT database scale tests
    
    p.parse(data,varargin{:});
    debug = p.Results.debug;
    grayscale = p.Results.grayscale;
    preprocess = p.Results.preprocess;
    method = p.Results.method;
    colorspace = p.Results.colorspace;
    normalizing = p.Results.normalizing;
    datatest = p.Results.datatest;
    dataset=p.Results.dataset;
    min_k = p.Results.min_k;
    filter = p.Results.filter;
    mr8 = p.Results.mr8;
    kvalue = p.Results.kvalue;
    image2cluster = p.Results.image2cluster;
    clusters = p.Results.clusters;
    secondcluster = p.Results.secondcluster;
    real2class = p.Results.real2class;
    mr8select = p.Results.mr8select;
    mr8dct = p.Results.mr8dct;
    fractaldct = p.Results.fractaldct;
    mr8abs =p.Results.mr8abs;
    clusters_all = p.Results.clusters_all;
    lopo = p.Results.lopo;
    reduceFD = p.Results.reduceFD;
    reduce_factor = p.Results.reduce_factor;
    precluster = p.Results.precluster;
    preclusters = p.Results.preclusters;
    kthscales=p.Results.kthscales;
    CUReTscales=p.Results.CUReTscales;
    
    
if(min_k >= kvalue & length(datatest)>0)
    error('min_k <= kvalue');
end
%if(1<=length(datatest) & length(datatest) < kvalue)
 %   error('length(datatest) < kvalue');
%end

if(length(data) < kvalue)
    error('length(data) < kvalue');
end
if (lopo & (dataset ~= 'pit'))
    error('lopo is only for pit pattern');
end

fractaldata={};
classTextonCluster={};
dim=[];


if (real2class)
    for i=1:length(data)
        switch dataset
            case 'gastro'
                if data{i}.idx > 2
                    data{i}.idx=2;
                end
            case 'pit'
                if data{i}.idx <= 2
                    data{i}.idx=1;
                else
                    data{i}.idx=2;
                end
        end
    end

    for i=1:length(datatest)
        switch dataset
            case 'gastro'
                if datatest{i}.idx > 2
                    datatest{i}.idx=2;
                end
            case 'pit'
                if datatest{i}.idx <= 2
                    datatest{i}.idx=1;
                else
                    datatest{i}.idx=2;
                end
        end
    end
end        
for m=1:length(data);
    if (debug)
        tic;
    end
    image=data{m}.image;
    dimension=[];
    if (grayscale && size(image,3)==3)
            image = rgb2gray(image);
    else
            switch colorspace
                case 'rgb'
                    image = double(image);
                case 'ybr'
                    image = double(rgb2ycbcr(image)); % convert to YBR color model
                case 'hsv'
                    image = double(rgb2hsv(image));
                case 'yiq'
                    image = double(rgb2ntsc(image));
                case 'rg'
                    image = double(image(:,:,1:2));
                case 'r'
                    image = double(image(:,:,1));
                case 'g'
                    image = double(image(:,:,2));
                case 'b'
                    image = double(image(:,:,3));
             end
     end
    
    
    for n=1:size(image,3)
         muB_laplace=[];
         im1=image(:,:,n);
       

         if (preprocess)
            warning off
            im1=adapthisteq(uint8(im1));
            warning on
         end

        im1=double(im1);
        
        switch mr8
            case 'valid'
                filtered=MR8fast(im1);%Verkleinerung des Bildes. Der Rand deranisotropic filterung wir nicht verwendet (entspricht 'valid' bei conv2) 
            case 'symmetric'
                filtered=MR8fast2(im1);%image keeps the same size (default)
            case 'imfilter'
                filtered=MR8symm(im1);%self made Version with function  imfilter (symmetric filtering) 
            case 'symmetric_and_smaller'
                filtered=MR8fast3(im1);%image keeps same size and scale is 1/4 of the scale in Mr8fast or MR8fast2.
        end
        
        % shegen: paper uses absolute values for the mu measure, hence
        % using the absolute reponse of mr8 is the correct variant
        if (mr8abs)
            filtered=abs(filtered);
        end
        
        if (mr8dct)
            for i=1:length(filtered)
                filtered([3,4,5],i)=dct(filtered([3,4,5],i));
                filtered([6,7,8],i)=dct(filtered([6,7,8],i));
            end
        end
        
        
        switch mr8select
            case 'normal'
                filtered=filtered;
            case 'part'
               % filtered=filtered([2,4,5,6,8],:);%XXX: ordering probably not consistent among the MR8 filtering methods: 
               % without gaussian smallest scale edge filter and medium
               % scale bar filter, fixed this to have the correct filters 
               filtered=filtered([2,3,4,6,8],:);
        end
                
        is_nan=sum(sum(sum(isnan(filtered))));
        if (is_nan > 0)
            warning('filtered image contains NAN. Bild %d',m);
        end
        
       if (strcmp(mr8,'symmetric') | strcmp(mr8,'imfilter'))%also useable for images that are not quadratic    
            sizefiltered1=size(im1,1);;%die 8 filtered Bilder 
        %sind nach fkt. mr8 nur Vektoren und müsssen wieder zu
        %Matrizzen gemacht werden.
            sizefiltered2=size(im1,2);
        else  %not  useable for images that are not quadratic
            sizefiltered1=sqrt(size(filtered,2))
            sizefiltered2=sqrt(size(filtered,2));
        end
        filteredImage=[];
        for p=1:size(filtered,1)
             filteredImage(:,:,p)=reshape(filtered(p,:),[sizefiltered1,sizefiltered2]);
        end
        filteredImage=double(filteredImage);
                                

        
        for filt=1:size(filtered,1)
            im=filteredImage(:,:,filt);
            sizeimy=size(im,1);
            sizeimx=size(im,2);
            muB=[];
            sigma=0.5;
            for r=1:8
                rmask=zeros(2*r-1,2*r-1);%All points with radius <= r (from the center) are ones, the others are zeros
                for i=1:2*r-1
                    for j=1:2*r-1
                        if  sqrt((i-r)^2+(j-r)^2)<=r-1
                            rmask(i,j)=1;
                        end
                    end
                end
                gaussian=zeros(2*r-1,2*r-1);%gaussian mask for blurring.
                for x=1:2*r-1
                    for y=1:2*r-1
                        
                        % shegen XXX: changed to fix the bug also found in
                        % fractaloriginal
                        % gaussian(x,y)=1/(r*sigma*sqrt(2*pi))*exp(- ( sqrt(sum((x-r)^2+(y-r)^2)))/(2* sigma^2 *r^2));
                        
                        % the authors of the paper explicitly compute the
                        % gaussian Gr by dividing by r^2, this is not a bug!
                        gaussian(x,y)= exp(- ( (sum((x-r)^2+(y-r)^2)))/(2* sigma^2 *r^2));
                    end
                end
                gaussian=gaussian/sum(sum(gaussian));
                switch filter
                    case 'valid'
                        imblurred=conv2(im,gaussian,'valid');%gaussian mask
                    case 'symmetric'
                        imblurred=imfilter(im,gaussian,'symmetric');
                end

                switch method
                    
                     case 'original'
                         switch filter
                            case 'valid'
                                % shegen: double checked paper, they do not
                                % use the gauss filtering
                                sums=conv2(im,rmask,'valid'); 
                            case 'symmetric'
                                % shegen: double checked paper, they do not
                                % use the gauss filtering
                                sums=imfilter(im,rmask,'symmetric');
                               
                         end     
               
                    case 'laplace'
                        [fx,fy]=gradient(imblurred);
                        fxx=gradient(fx);
                        [s,fyy]=gradient(fy);
                        laplace=abs(fxx+fyy);%Laplace-operator von imblurred
                        switch filter
                            case 'valid'
                                sums=conv2(laplace,rmask,'valid');%integral (Summe der Helligkeitswerte) von imblurred über der Kreisscheibe rmask
                            case 'symmetric'
                                sums=imfilter(laplace,rmask,'symmetric');
                        end

                    case 'gradient'
                        [fx,fy]=gradient(imblurred);
                        grad=abs(fx+fy);
                         switch filter
                            case 'valid'
                                sums=conv2(grad,rmask,'valid');
                            case 'symmetric'
                                sums=imfilter(grad,rmask,'symmetric');
                        end               
                    case 'gradient4'
                        [fx,fy]=gradient(imblurred);
                        sro=[0,0,0.5;0 0 0;-0.5,0,0];%schräg nach rechts oben
                        slo=[0.5,0,0;0 0 0;0 0 -0.5];%schräg nach links oben
                        fsro=imfilter(imblurred,sro,'symmetric');
                        fslo=imfilter(imblurred,slo,'symmetric');
                        grad=abs(fx+fy+fsro+fslo);
                    case 'normal'
                         switch filter
                            case 'valid'
                                sums=conv2(imblurred,rmask,'valid');
                            case 'symmetric'
                                sums=imfilter(imblurred,rmask,'symmetric');
                         end     
                    case 'sobel'
                        sobely=fspecial('sobel')/4;
                        sobelx=-sobely';
                        sobel_fx=imfilter(imblurred, sobelx ,'symmetric');
                        sobel_fy=imfilter(imblurred, sobely ,'symmetric');
                        sobel=abs(sobel_fx + sobel_fy);
                        switch filter
                            case 'valid'
                                sums=conv2(sobel,rmask,'valid');
                            case 'symmetric'
                                sums=imfilter(sobel,rmask,'symmetric');
                         end  

                    case 'versuch'
                        sobely=fspecial('sobel')/4;
                        sobelx=-sobely';
                        sobel_fx=imfilter(im, sobelx ,'symmetric');
                        sobel_fy=imfilter(im, sobely ,'symmetric');
                        vers=abs(sobel_fx) + abs(sobel_fy);
                         switch filter
                            case 'valid'
                                sums=conv2(vers,rmask,'valid');
                            case 'symmetric'
                                sums=imfilter(vers,rmask,'symmetric');
                         end  

           
                
                    case 'sobel2'% absolutwerte von sobel_fx und sobel_fy addieren.
                        sobely=fspecial('sobel')/4;
                        sobelx=-sobely';
                        sobel_fx=imfilter(imblurred, sobelx ,'symmetric');
                        sobel_fy=imfilter(imblurred, sobely ,'symmetric');
                        sobel=abs(sobel_fx) + abs(sobel_fy);
                        switch filter
                            case 'valid'
                                sums=conv2(sobel,rmask,'valid');
                            case 'symmetric'
                                sums=imfilter(sobel,rmask,'symmetric');
                        end  
                        
                   case 'sobel3'%als länge eines Vektors betrachten 
                        sobely=fspecial('sobel')/4;
                        sobelx=-sobely';
                        sobel_fx=imfilter(imblurred, sobelx ,'symmetric');
                        sobel_fy=imfilter(imblurred, sobely ,'symmetric');
                        sobel=sqrt(sobel_fx.^2 + sobel_fy.^2);
                        switch filter
                            case 'valid'
                                sums=conv2(sobel,rmask,'valid');
                            case 'symmetric'
                                sums=imfilter(sobel,rmask,'symmetric');
                        end  

                end

                switch filter
                    case 'valid'
                        size_sumsy=size(sums,1);%größe des Bildes nimmt mit jeder Convolution ab.
                        size_sumsx=size(sums,2);
                        %bei r=8 ist die Größe Bildes nur mehr 100*100 (bei 128*128 Originalgröße)
                        %(128  -  2*(8-1)[1. conv=blurring]  - 2*(8-1)[2. conv=sums]  =  100)
                        size_sums_r8y=sizeimy-2*(8-1)-2*(8-1);
                        size_sums_r8x=sizeimx-2*(8-1)-2*(8-1);
                        sy=size_sums_r8y;
                        sx=size_sums_r8x;
                        cy=(size_sumsy - size_sums_r8y)/2;
                        cx=(size_sumsx - size_sums_r8x)/2;
                        sums_same_size=sums(1+cy:size_sums_r8y+cy , 1+cx:size_sums_r8x+cx);
                        muB_laplace(:,:,r)=sums_same_size;

                    case 'symmetric'
                        muB_laplace(:,:,r)=sums;
                        sx=sizeimx;
                        sy=sizeimy;
                end


                muB_laplace=muB_laplace + double(muB_laplace<=0);%Alle Nullen werden durch Einsen ersetzt
                %da ich sonst den logarithmus nicht nehmen kann

                muB=muB_laplace;


            end


            rlog=log([1:8]);
            alpha=[];
            h=[];
            for i=1:sy
                for j=1:sx
                    for k=1:8
                        h(k)=log(muB(i,j,k)); 
                    end
                  %  alpha(i,j)=regress(  h'-h(1), rlog');%linear fitting des Anstiegs.  Fractale dim for every Pixel.
                    %der Geraden von  log mu(B(x,r) (h) gegen log r
                    p=polyfit(rlog, h, 1);
                    alpha(i,j) = p(1);
                   
                end
            end
            if(reduceFD)
                fractaldata{m}(:,filt)=alpha(1:reduce_factor:end);
            else
                fractaldata{m}(:,filt)=alpha(:);
            end
        end
    end
    if (precluster)
        [idx,frac_help] = kmeans(fractaldata{m},preclusters,'MaxIter',200, 'OnlinePhase' , 'off');
        fractaldata{m}=frac_help;
    end
    
    if (debug)
        tim=toc;
        if(mod(m,1)==0)
            tim=toc;
            fprintf('%d. Bild : 1   Bild in %0.2f [s]\n' ,m,  tim);
        end
    end
end
if (fractaldct)
    for m=1:length(fractaldata)
            for i=1:size(fractaldata{m},1)
                fractaldata{m}(i,[3,4,5])=dct(fractaldata{m}(i,[3,4,5]));
                fractaldata{m}(i,[6,7,8])=dct(fractaldata{m}(i,[6,7,8]));

            end
    end
end

for i=1:length(data);
    indexset(i)=data{i}.idx;
end



dictionary=[];
if(clusters_all)
    for i=1:max(indexset)
        class=find(indexset==i);%find images with class idx=i
        classTextons=[];
        for j=1:length(class)
            classTextons=[classTextons;fractaldata{class(j)}];%Features of the #image2cluster ramdomly chosen images
        end
        [idx,C]=kmeans(classTextons,clusters,'MaxIter',200,'emptyaction','singleton', 200, 'OnlinePhase' , 'off');%clustering the #clusters*size(image,1)*size(image,2) features to 10 representative features of a class 
        classTextonCluster{i}=C;
        dictionary=[dictionary;C];%representative features of each class
    
    end    
    
else
    for i=1:max(indexset)
        class=find(indexset==i);%find images with class idx=i
        classcount=length(class);
        randomClassNumbers=random('unid',classcount,[1,image2cluster]);%Randomly choose 13 images of every class
        randomClassRepresentative=class(randomClassNumbers);
        classRepresentative{i}=randomClassRepresentative;
        classTextons=[];
        for j=1:image2cluster
            classTextons=[classTextons;fractaldata{randomClassRepresentative(j)}];%Features of the #image2cluster ramdomly chosen images
        end
        [idx,C]=kmeans(classTextons,clusters,'MaxIter',200, 'OnlinePhase' , 'off');%clustering the #clusters*size(image,1)*size(image,2) features to 10 representative features of a class 
        classTextonCluster{i}=C;
        dictionary=[dictionary;C];%representative features of each class
        if (secondcluster)%make a second cluster for each class
            class=find(indexset==i);
            classcount=length(class);
            randomClassNumbers=random('unid',classcount,[1,image2cluster]);
            randomClassRepresentative=class(randomClassNumbers);
            classTextons=[];
            for j=1:image2cluster
                classTextons=[classTextons;fractaldata{randomClassRepresentative(j)}];
            end
            [idx,C]=kmeans(classTextons,clusters,'MaxIter',200,'emptyaction','singleton','OnlinePhase' , 'off');
            dictionary=[dictionary;C];
        end
    end
end

if (debug)
        fprintf('clustering completed \n');
    end
for i=1:length(data)
    D=disteu(dictionary',fractaldata{i}');
    [x,y]=min(D);%find the nearest reprsentative features (=Textons  for every "Pixel-feature" of every image
    fractalmodel=y;
    n=hist(fractalmodel,[1:size(dictionary,1)]);% counts the nearest Textons  
    if (normalizing)
        n=n/sum(n);  % to normalize the histogram. Necessary when images have
        %different sizes
    end
    histogram(:,i)=n';%Histogram, that counts how often each Texton occurs (is the nearest neighbor of a "pixel")  
end






 




if (length(datatest) > 0)%if train- and testset
    [histogramtest]=fractalmr8clustertestset(datatest,dictionary,'debug',debug,'preprocess',preprocess,'method',method, ...
        'colorspace',colorspace,'grayscale',grayscale, ...
        'dataset',dataset,'normalizing',normalizing,'filter',filter,'mr8',mr8,'real2class',real2class,'mr8select',mr8select, ...
        'mr8dct',mr8dct,'fractaldct',fractaldct,'mr8abs',mr8abs,'reduceFD',reduceFD,'reduce_factor',reduce_factor, ...
        'precluster',precluster,'preclusters',preclusters);
    
   
    distmat=distchi2(histogramtest,histogram);
    distmatoneset=distchi2(histogram,histogram);
    
    
    
else % nur ein set
     distmat=distchi2(histogram,histogram);       
end

if (length(datatest) > 0)%test andd trainset
     for k=1:kvalue
        [cp,R]=evalpitdifknn(data,distmatoneset,'knn',k);
        result_4class(k)=cp.corr;
        switch dataset
            case 'gastro'
                result_2class(k)=(cp.cou(1,1)+sum(sum(cp.coun(2:end,2:end))))/sum(sum(cp.coun));
            case 'pit'
                result_2class(k)=(sum(sum(cp.coun(1:2,1:2)))+sum(sum(cp.coun(3:end,3:end))))/sum(sum(cp.coun));
            case 'sonstige'
                result_2class(k)=0;
        end
     end
     result={};
      [result{2,1},result{2,2}]=max(result_2class);
      k2c=result{2,2};% k-value for the best result of the train set
      r1=max(result_2class(min_k+1:end));
      k2c_kmin=max(find(result_2class==r1));
      [result{2,3},result{2,4}]=max(result_4class);  
      k4c=result{2,4};% k-value for the best result of the train set
      r1=max(result_4class(min_k+1:end));
      k4c_kmin=max(find(result_4class==r1));
      result{1,11}='maxtrainset_2c';
      result{2,11}=result{2,1};
      result{1,12}='maxtrainset_4c';
      result{2,12}=result{2,3};
      result{1,14}='meantrainset_4c';
      result{2,14}=mean(result_4class);
    
    R=[];
    for k=1:kvalue
        [cp,R(k,:),NN]=evalpitdifknntestandtrainset(data,datatest,distmat,'knn',k,'kthscales',kthscales,'CUReTscales',CUReTscales);
        result_4class(k)=cp.corr;
        switch dataset
            case 'gastro'
                result_2class(k)=(cp.cou(1,1)+sum(sum(cp.coun(2:end,2:end))))/sum(sum(cp.coun));
            case 'pit'
                result_2class(k)=(sum(sum(cp.coun(1:2,1:2)))+sum(sum(cp.coun(3:end,3:end))))/sum(sum(cp.coun));
            case 'sonstige'
                result_2class(k)=0;
        end
    end
  
    result{1,1}='max2c'; 
    result{1,2}='2c_k';
    result{1,3}='max4c';
    result{1,4}='4c_k';
    result{1,5}='mean2c';
    result{1,6}='mean4c';
     
    [result{2,1},result{2,2}]=max(result_2class);
    [result{2,3},result{2,4}]=max(result_4class);
    result{2,5}=mean(result_2class);
    result{2,6}=mean(result_4class);
    
    knnstring=num2str(k2c);%best k-value of the train set
    knnstring2c=['knn2c=',knnstring];
    result{1,7}=knnstring2c;
    result{2,7}=result_2class(k2c);%most interesting result (because only in the train set optimized)
  
    knnstring=num2str(k4c);%best k-value of the train set
     knnstring4c=['knn4c=',knnstring];
     result{1,8}=knnstring4c;
     result{2,8}=result_4class(k4c);%most interesting result (because only in the train set optimized)
  
     knnstring=num2str(k2c_kmin);%best k-value of the train set wit k>kmin
     knnstring2=num2str(min_k);
    knnstring2c=['knn2c_kmin=',knnstring2,' : ',knnstring];
    result{1,9}=knnstring2c;
    result{2,9}=result_2class(k2c_kmin);%most interesting result (because only in the train set optimized)
  
    knnstring=num2str(k4c_kmin);%best k-value of the train set with k>kmin
    knnstring2=num2str(min_k);
    knnstring4c=['knn4c_kmin=',knnstring2,' : ',knnstring];
     result{1,10}=knnstring4c;
     result{2,10}=result_4class(k4c_kmin);%most interesting result (because only in the train set optimized)   
     
  %AUC of ROC-plot
     
     indexset=[];
     Positives=[];
     if (kvalue >= 20)
        knn=20;
     else
         knn=kvalue;
     end
     NN=NN(:,1:knn);
	c=find(NN>1);
	NN(c)=2;%only 2-class case
    Positives=sum(NN'==2);
    for k=1:length(datatest)
        indexset(k)= datatest{k}.idx;
    end
    c=find(indexset >= 2);
    indexset(c)=2;
     Klasse=[];
    for i=1:knn
        for k=1:length(datatest)
          Klasse(k)= Positives(k)>=i;
        end
        cp(i) = classperf(indexset-1,Klasse);%da indexset werte zwische 1und 2 hat (1=negativ 2=positiv)
                %Klasse: 0=negativ, 1=positiv.  daher indexset-1.
    %Achtung!!!! Da bei Fkt classperf die erste Klasse als Positiv gilt wird bei
        %classperf die sensitivity als specificity ausgegeben und umgekehrt.
    end
    TPR=[];
    FPR=[];

    %Berechne True Positive Rate (TPR) und False Positive Rate (FPR)
    for i=1:knn
        TPR(i)=cp(i).Specificity;% normalerweise: True Positive Rate = Sensitivity 
        %=(Anzahl der richtig klassifizierten Positiven /Anzahl der Positiven)
        %Da bei Fkt classperf die erste Klasse als Positiv gilt wird bei
        %classperf die sensitivity als specificity ausgegeben und umgekehrt.
        %(deshalb  TPR(i)=cp(i).Specificit)
       FPR(i)=1-cp(i).Sensitivity; %normalerweise: False Positive Rate = Specificity 
        %=(Anzahl der richtig klassifizierten Negativen /Anzahl der Negativen)
        %jedoch aus gleiche Gründen wie bei TPR wird Die FPR auf diese Weise
        %errechnet.

    end
    TPR=[1,TPR,0];
    FPR=[1,FPR,0];
    TPRFPR=[TPR;FPR];


    plot(FPR,TPR);set(gca(),'XLim',[0 1],'YLim',[0 1]);


    %Berechne arear under curve (AUC) mit Trapez-Integration
    AUC=0;
    for i=2:length(TPR)
        AUC=AUC + TPR(i) * (FPR(i-1)-FPR(i)) - 1/2* ( (TPR(i-1)-TPR(i)) * (FPR(i-1)-FPR(i)) );
    end
     result{1,11}='AUC';
     result{2,11}=AUC;     
        
     %[cp,R]=evalpitdifknntestandtrainset(data,datatest,distmat,'knn',k4c,'kthscales',kthscales,'CUReTscales',CUReTscales);%only to get the right 'R'    
        
else %only one set
    
    for k=1:kvalue
        [cp,R(k,:)]=evalpitdifknn(data,distmat,'knn',k);
        result_4class(k)=cp.corr;
        switch dataset
            case 'gastro'
                result_2class(k)=(cp.cou(1,1)+sum(sum(cp.coun(2:end,2:end))))/sum(sum(cp.coun));
            case 'pit'
                result_2class(k)=(sum(sum(cp.coun(1:2,1:2)))+sum(sum(cp.coun(3:end,3:end))))/sum(sum(cp.coun));
            case 'sonstige'
                result_2class(k)=0;
        end
    end
    result={};
    result{1,1}='max2c'; 
    result{1,2}='2c_k';
    result{1,3}='max4c';
    result{1,4}='4c_k';
    result{1,5}='mean2c';
    result{1,6}='mean4c';
     
    [result{2,1},result{2,2}]=max(result_2class);
    [result{2,3},result{2,4}]=max(result_4class);
    result{2,5}=mean(result_2class);
    result{2,6}=mean(result_4class);
    
    if (lopo) 
        for k=1:kvalue
        [cp,R(k,:)]=evalpitlopknn(data,distmat,'knn',k);
        result_4class(k)=cp.corr;
        switch dataset
            case 'gastro'
                result_2class(k)=(cp.cou(1,1)+sum(sum(cp.coun(2:end,2:end))))/sum(sum(cp.coun));
            case 'pit'
                result_2class(k)=(sum(sum(cp.coun(1:2,1:2)))+sum(sum(cp.coun(3:end,3:end))))/sum(sum(cp.coun));
            case 'sonstige'
                result_2class(k)=0;
            end
        end
        result={};
        result{1,1}='max2c'; 
        result{1,2}='2c_k';
        result{1,3}='max4c';
        result{1,4}='4c_k';
        result{1,5}='mean2c';
        result{1,6}='mean4c';

        [result{2,1},result{2,2}]=max(result_2class);
        [result{2,3},result{2,4}]=max(result_4class);
        result{2,5}=mean(result_2class);
        result{2,6}=mean(result_4class);
        %[cp,R]=evalpitdifknn(data,distmat,'knn',result{2,4});
    end 
end

end


















             