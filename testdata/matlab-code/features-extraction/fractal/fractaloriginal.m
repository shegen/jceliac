function [dim]=fractaloriginal(data,varargin)
%classifisd the images by partition an image ion binary images and computing the fractal dimension of these binary images. 
%riginal method of  paper 'Viewpoint invariant texture description using fractal analysis'
%if test and trainset then 'data' have to be the trainset.
p = inputParser;
    p.addRequired('data',@iscell);
    p.addParamValue('debug',false,@islogical);
    p.addParamValue('colorspace','rgb',@(x)any(strcmpi(x,{'rgb','ybr','hsv','yiq','rg','r','g','b'})));
    p.addParamValue('grayscale',true,@islogical);
    p.addParamValue('distmat','norm1',@(x)any(strcmpi(x,{'norm1','euclid','EMD'})));
    %computing the distance beween the feature vectors by euclidean distance or the Earth Movers Distance
    p.addParamValue('normalizing',false,@islogical);
    p.addParamValue('datatest',{},@iscell);%for train-and testset (datatest is the testset, data the trainset)
    p.addParamValue('dataset','sonstige',@(x)any(strcmpi(x,{'gastro','pit','sonstige'})));
    %kind of imagedata (for the information how much classes are existing
    p.addParamValue('steps',26,@(x)x>3 && x<= 100);
    p.addParamValue('stepmax',2.42,@(x)x>=1 && x<=5);
    p.addParamValue('min_k',2, @(x) x>=1 && x <95);%only for distinct sets. Min. k_value for Optimization of k=min_k. 
    p.addParamValue('filter','symmetric' ,@(x)any(strcmpi(x,{'valid','symmetric'})));
    p.addParamValue('kvalue',100, @(x) x>=1 && x <300);%kvalue for knn
    p.addParamValue('sigma',0.5, @(x) x>=0 && x <3);
    p.addParamValue('kthscales',false,@islogical);%especially for the KTH tips database scale tests
    p.addParamValue('CUReTscales',false,@islogical);%especially for the CUReT database scale tests
 
    p.parse(data,varargin{:});
    debug = p.Results.debug;
    grayscale = p.Results.grayscale;
    colorspace = p.Results.colorspace;
    distmat = p.Results.distmat;
    normalizing = p.Results.normalizing;
    datatest = p.Results.datatest;
    dataset=p.Results.dataset;
    steps = p.Results.steps;
    stepmax = p.Results.stepmax;
    min_k = p.Results.min_k;
    filter = p.Results.filter;
    kvalue = p.Results.kvalue;
    sigma = p.Results.sigma;
     kthscales=p.Results.kthscales;
    CUReTscales=p.Results.CUReTscales;

    debug = 1;
dim=[];
for m=1:length(data);
    if (debug)
        tic;
    end
    image=data{m}.image;
    dimension=[];
    stepsize=stepmax/(steps-1);
    if (grayscale & size(image,3)==3)
            image = rgb2gray(image);
    else
            switch colorspace
                case 'rgb'
                    image = double(image);
                case 'ybr'
                    image = double(rgb2ycbcr(image)); % convert to YBR color model
                case 'hsv'
                    image = double(rgb2hsv(image));
                case 'yiq'
                    image = double(rgb2ntsc(image));
                case 'rg'
                    image = double(image(:,:,1:2));
                case 'r'
                    image = double(image(:,:,1));
                case 'g'
                    image = double(image(:,:,2));
                case 'b'
                    image = double(image(:,:,3));
             end
     end
    
    
    for n=1:size(image,3)
         
         im=image(:,:,n);
         muB_laplace=[];
         muB_grad4=[];
         muB_normal=[];
         

        im=double(im);
        sizeimy=size(im,1);
        sizeimx=size(im,2);
        muB=[];
    
        for r=1:8
            rmask=zeros(2*r-1,2*r-1);%All points with radius <= r (from the center) are ones, the others are zeros
            for i=1:2*r-1
                for j=1:2*r-1
                    if  sqrt((i-r)^2+(j-r)^2)<=r-1
                        rmask(i,j)=1;
                    end
                end
            end
            gaussian=zeros(2*r-1,2*r-1);
            for x=1:2*r-1
                for y=1:2*r-1
                    % the authors of the paper explicitly compute the
                    % gaussian Gr by dividing by r^2, this is not a bug!
                    gaussian(x,y)= exp(- ( (sum((x-r)^2+(y-r)^2)))/(2* sigma^2 *r^2));
                end
            end
            gaussian=gaussian/sum(sum(gaussian));
            switch filter
                case 'valid'
                    imblurred=conv2(im,gaussian,'valid');%gaussian mask
                case 'symmetric'
                    imblurred=imfilter(im,gaussian,'symmetric');
            end
           
                    [fx,fy]=gradient(imblurred);
                    fxx=gradient(fx);
                    [s,fyy]=gradient(fy);
                    laplace=abs(fxx+fyy);%Laplace-operator von imblurred
                    switch filter
                        case 'valid'
                            sumslap=conv2(laplace,rmask,'valid');%integral (Summe der Helligkeitswerte) von imblurred über der Kreisscheibe rmask
                        case 'symmetric'
                            sumslap=imfilter(laplace,rmask,'symmetric');
                    end
                    

                    sro=[0,0,0.5;0 0 0;-0.5,0,0];%schräg nach rechts oben
                    slo=[0.5,0,0;0 0 0;0 0 -0.5];%schräg nach links oben
                    fsro=imfilter(imblurred,sro,'symmetric');
                    fslo=imfilter(imblurred,slo,'symmetric');
                    grad=sqrt(fx.^2+fy.^2+fsro.^2+fslo.^2);
                     switch filter
                        case 'valid'
                            sumsgrad4=conv2(grad,rmask,'valid');%integral (Summe der Helligkeitswerte) von imblurred über der Kreisscheibe rmask
                        case 'symmetric'
                            sumsgrad4=imfilter(grad,rmask,'symmetric');
                     end
        
              
                     
                    
                    switch filter
                        case 'valid'
                            sumsnormal=conv2(imblurred,rmask,'valid');
                        case 'symmetric'
                            sumsnormal=imfilter(imblurred,rmask,'symmetric');
                     end     
               

          
            
            
            switch filter
                case 'valid'
                    size_sumsy=size(sumslap,1);%größe des Bildes nimmt mit jeder Convolution ab.
                    size_sumsx=size(sumslap,2);
                    %bei r=8 ist die Größe Bildes nur mehr 100*100 (bei 128*128 Originalgröße)
                    %(128  -  2*(8-1)[1. conv=blurring]  - 2*(8-1)[2. conv=sums]  =  100)
                    size_sums_r8y=sizeimy-2*(8-1)-2*(8-1);
                    size_sums_r8x=sizeimx-2*(8-1)-2*(8-1);
                    sy=size_sums_r8y;
                    sx=size_sums_r8x;
                    cy=(size_sumsy - size_sums_r8y)/2;
                    cx=(size_sumsx - size_sums_r8x)/2;
                    
                    sums_same_size=sumslap(1+cy:size_sums_r8y+cy , 1+cx:size_sums_r8x+cx);
                    muB_laplace(:,:,r)=sums_same_size;
                    sums_same_size=sumsgrad4(1+cy:size_sums_r8y+cy , 1+cx:size_sums_r8x+cx);
                    muB_grad4(:,:,r)=sums_same_size;
                    sums_same_size=sumsnormal(1+cy:size_sums_r8y+cy , 1+cx:size_sums_r8x+cx);
                    muB_normal(:,:,r)=sums_same_size;
                    
                case 'symmetric'
                    muB_laplace(:,:,r)=sumslap;
                    muB_grad4(:,:,r)=sumsgrad4;
                    muB_normal(:,:,r)=sumsnormal;
                    
                    sx=sizeimx;
                    sy=sizeimy;
            end
            
            
            muB_laplace=muB_laplace + double(muB_laplace==0);%Alle Nullen werden durch Einsen ersetzt
            %da ich sonst den logarithmus nicht nehmen kann
            muB_grad4=muB_grad4 + double(muB_grad4==0);
            muB_normal=muB_normal + double(muB_normal==0);
        end
        
        boxlen = round((log(max(size(data{1}.image))) / log(2)) +1);
        rlog=log([1:boxlen]);
        alpha_laplace=[];
        alpha_grad4=[];
        alpha_normal=[];
        h=[];
        for i=1:sy
            for j=1:sx
                for k=1:8
                    h_laplace(k)=log(muB_laplace(i,j,k)); 
                    h_grad4(k)=log(muB_grad4(i,j,k)); 
                    h_normal(k)=log(muB_normal(i,j,k)); 
                end
                p=polyfit(rlog,h_laplace, 1);
                alpha_laplace(i,j)= p(1);%linear fitting des Anstiegs der Geraden von  log mu(B(x,r) (h) gegen log r
                
                p=polyfit(rlog,h_grad4, 1);
                alpha_grad4(i,j)=p(1);
                
                p=polyfit(rlog,h_normal, 1);
                alpha_normal(i,j)=p(1);
            end
        end


        alphaintervall=[];%unterteile Matrix alpha in binäre matrizzen je nach größe der alpha-werte. 
        alphaintervall_laplace=[];
        alphaintervall_grad4=[];
        alphaintervall_normal=[];
        a=0;
        for i=0:stepsize:stepmax
            a=a+1;
          
            alphaintervall_laplace(:,:,a)= ...
            i*ones(size(alpha_laplace,1),size(alpha_laplace,2)) <= alpha_laplace & alpha_laplace<(i+stepsize)*ones(size(alpha_laplace,1),size(alpha_laplace,2));
        
        
            alphaintervall_grad4(:,:,a)= i*ones(size(alpha_grad4,1),size(alpha_grad4,2)) <= alpha_grad4 & alpha_grad4<(i+stepsize)*ones(size(alpha_grad4,1),size(alpha_grad4,2));
            
            alphaintervall_normal(:,:,a)= (i+stepmax/2)*ones(size(alpha_normal,1),size(alpha_normal,2)) <= alpha_normal & alpha_normal<(i+stepsize+stepmax/2)*ones(size(alpha_normal,1),size(alpha_normal,2));
        end  %Bei alphaintervall_normal habe ich noch die hälfte von stepmax bei den hreshholdplanes dazugerechnet, da ansonsten die 
             %treshholdplanes außerhalb des bereichs der lokalen fraktalen dimension sind 
             %(lokale fraktale dim. mit mu=normal und Bild mit
             %einheitlicher Helligkeit ist die lokale fraktale dim
             %2.418(=defaultwert für stepmax)
        
        

        %berechne dimension der binären Matrizzen alphaintervall
         fractaldim_laplace=zeros(1,size(alphaintervall_laplace,3));
         fractaldim_grad4=zeros(1,size(alphaintervall_grad4,3));
         fractaldim_normal=zeros(1,size(alphaintervall_normal,3));
         
         
        for i=1:size(alphaintervall_laplace,3)
            if sum(sum(alphaintervall_laplace(:,:,i)))>0

                [n, r] = boxcount(alphaintervall_laplace(:,:,i));
               
                %fractaldim_laplace(i)=regress(-log(n)'+log(n(1)),log(r)');
                p=polyfit(rlog, -log(n), 1);
                fractaldim_laplace(i) =p(1);
            end
        end
        
        for i=1:size(alphaintervall_grad4,3)
            if sum(sum(alphaintervall_grad4(:,:,i)))>0

                [n, r] = boxcount(alphaintervall_grad4(:,:,i));
               % fractaldim_grad4(i)=regress(-log(n)'+log(n(1)),log(r)');
                p=polyfit(rlog, -log(n), 1);
                fractaldim_grad4(i) =p(1);
            end
        end
        
        for i=1:size(alphaintervall_normal,3)
            if sum(sum(alphaintervall_normal(:,:,i)))>0

                [n, r] = boxcount(alphaintervall_normal(:,:,i));
                %fractaldim_normal(i)=regress(-log(n)'+log(n(1)),log(r)');
                p=polyfit(rlog, -log(n), 1);
                fractaldim_normal(i) =p(1);
            end
        end       
         dimension=[dimension,fractaldim_laplace,fractaldim_grad4,fractaldim_normal];
        
    end
    if (debug)
        tim=toc;
        fprintf('%d. Bild : in %0.2f [s]\n' ,m,  tim);

    end
   dim(m,:)=dimension;
        
end


end














