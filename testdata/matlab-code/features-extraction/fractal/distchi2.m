function d = distchi2(x, y)
% DISTEU Pairwise chi^2 distances between columns of two matrices
%
% Input:
%       x, y:   Two matrices whose each column is an a vector data.
%
% Output:
%       d:      Element d(i,j) will be the chi^2 distance between two
%               column vectors X(:,i) and Y(:,j)
%
% Note:
%       The chi^2 distance D between two vectors X and Y is:
%       D = 1/2 * sum(  ((x-y).^2)./(x+y) )

[M, N] = size(x);
[M2, P] = size(y); 

if (M ~= M2)
    error('Matrix dimensions do not match.')
end

d = zeros(N, P);

if (N < P)
    copies = zeros(1,P);
    for n = 1:N
         q=((x(:, n+copies)  - y).^2) ./ (x(:, n+copies) + y+eps);%all Numbers taht are NAN (because of 0/0) are changed to 0
         q2=isnan(q);
         q(q2)=0;
         if (sum(sum(double(isinf(q))))>0)
             error('values in distance matrix are INFINITY')
         end
         d(n,:) = sum(q,1)'/2;
    end
else
    copies = zeros(1,N);
    for p = 1:P

         q=((x - y(:, p+copies)) .^2) ./ (x + y(:, p+copies)+eps);%all Numbers taht are NAN (because of 0/0) are changed to 0
          q2=isnan(q);
          q(q2)=0;
          if (sum(sum(double(isinf(q))))>0)
             error('values in distance matrix are INFINITY')
           end
          d(:,p) = sum(q,1)'/2;
         
    end
end

