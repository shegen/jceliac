function [result,blobs]=getblobsnew2(imbin,varargin)
%Only use this function, if you are only interested in the blobs of ones and not in the
%0-blobs.
%faster than the getblob-function.
%subfunction of blobown.m
 p = inputParser;
    p.addRequired('imbin');
    p.addParamValue('minimum',1,@(x)x>=1 && x<=100);
    p.addParamValue('neighborhood',4,@(x)x==4 || x==8);
    p.addParamValue('irregularity',false,@islogical);
    p.parse(imbin,varargin{:});
    minimum=p.Results.minimum;
    neighborhood = p.Results.neighborhood;
    irregularity=p.Results.irregularity;
    
    
[imbinblob,num]=bwlabel(imbin,neighborhood);    

j=0;
blobs={};
for i=1:max(max(imbinblob))
        j=j+1;
        blobs{j}.ziffer=i;
        h1=find(imbinblob==i);
        h2=length(h1);
        blobs{j}.anzahl=h2;%number of members of the blob
        if (irregularity)
            h3=[mod(h1,size(imbinblob,1))+double(mod(h1,size(imbinblob,1))==0)*size(imbinblob,1),floor(h1/size(imbinblob,1)-eps)+1];
            blobs{j}.position=h3;
            h4=[ (mean(h3(:,1))), (mean(h3(:,2)))];
            blobs{j}.center_of_mass=h4;%(y,x)koordinate des schwerpunkts
            h5=[sqrt((h3(:,1)-double(h4(1))).^2+(h3(:,2)-double(h4(2))).^2)];
            blobs{j}.irregularity=sqrt(pi)*max(h5)/sqrt(h2);
        end
 end




m1=0;
m0=0;
irgl1=[];
irgl2=[];
anzahl1=[];
anzahl0=[];
for i=1:j

        m1=m1+1;        
        anzahl1(m1)=blobs{i}.anzahl;
        if(irregularity)
            irgl1(m1)=blobs{i}.irregularity;
        end

end
for i=1:j

       if (blobs{i}.anzahl<minimum)%the number of bobs we count areonly these blobs, which have more or equal than 'minimum' members.
           m1=m1-1;
       end

end
result.number_of_blobs=m1;
if(irregularity)
    if (sum(anzahl1)==0)
        result.irregularity=0;
    else  
        result.irregularity=sum(irgl1.*anzahl1)/(sum(anzahl1)+eps);
    end
end


end












         
    








