function [result_number,result_irregularity]=bloboriginal2(data,varargin)
%faster than bloboriginal
%see paper 'Multiscale Blob Features for Gray Scale, Rotation and Spatial
%Scale Invariant Texture Classification'
%if irregualrity is false, we count only the number of blobs and do not
%consider their shape (=irregularity of the blobs)
p = inputParser;
    p.addRequired('data',@iscell);
    p.addParamValue('debug',false,@islogical);
    p.addParamValue('minimum',1,@(x)x>=1 && x<=1000);
    p.addParamValue('irregularity',true,@islogical);
     p.addParamValue('normalize',false,@islogical);%if images have different sizes.(needs normalizing of the final feature vector (fv=normalize(fv))
    p.parse(data,varargin{:});
    minimum=p.Results.minimum;
    irregularity=p.Results.irregularity;
    debug=p.Results.debug;
    normalize = p.Results.normalize;
preprocess=false;
 result_number=[];
 result_irregularity=[];


for m=1:length(data);
     tic;
     image=data{m}.image;
     if(size(image,3)==3)
        image = rgb2gray(image);
     end
     im=image;

     if (preprocess)
            warning off
            im=adapthisteq(uint8(im));
            warning on
     end

    im=double(im);
      





    b=std2(im);
    bias=[-b,-b/2,-b/4,-b/8,b/8,b/4,b/2,b];
    number_of_blobs=[];
    irregularity_of_blobs=[];
    sigmacount=0;

    for sigma=0.3:1:30
        sigmacount=sigmacount+1;
        biascount=0;
        for bb=1:8
            biascount=biascount+1;
            h = fspecial('gaussian', [128,128], sigma);
            h1=find(sum(h)>0.0001);
            h=h(h1,h1);
            imblurred=double(imfilter(im,h,'symmetric'));

            imbin = (im>=imblurred+bias(bb));
            imbin2=(imbin-1)*(-1);
            blobs=getblobsnew2(imbin,'minimum',minimum,'irregularity',irregularity);
            
            number_of_blobs=[number_of_blobs, blobs.number_of_blobs];
            if (irregularity)
                irregularity_of_blobs=[irregularity_of_blobs, blobs.irregularity];
            end
            blobs=getblobsnew2(imbin2,'minimum',minimum,'irregularity',irregularity);
            
            number_of_blobs=[number_of_blobs, blobs.number_of_blobs];
            if (irregularity)
                irregularity_of_blobs=[irregularity_of_blobs, blobs.irregularity];
            end
        end
    end
    if (normalize)
        number_of_blobs=number_of_blobs/(size(im,1)*size(im,2));
    end
    result_number=[result_number; number_of_blobs];
    if (irregularity)
        result_irregularity=[result_irregularity; irregularity_of_blobs];
    else
        result_irregularity=[];
    end
    if debug
        tim=toc;
        fprintf('%d. Bild : in %0.2f [s]\n' ,m,  tim);
    end
end
end
