#include "mex.h"
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_sf.h>

double 
kldist(double a1, double b1, double a2, double b2) {
    double res;
	  res = 0.5 * (-2.0 + pow(b1,2)/pow(b2,2) + pow(b2,2)/pow(b1,2));
    return ((res > 0.0) ? res : 0.0);
}

double 
probdist(double *fv1, double *fv2, int nb) {
    int i;
    double dist = 0.0;
    for (i = 0; i < nb; i++) {
	dist += kldist(fv1[2*i], fv1[2*i+1], fv2[2*i], fv2[2*i+1]);
    }
    return dist;
}


void
mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    double	*fs;
    double	*rr;

    int		nimages, ns, nb, nf;
    int		q, i, c, r;
    double	*dist, dr;
    double	*pr;

    fs = mxGetPr(prhs[0]);
    nf = mxGetM(prhs[0]);
    nb = nf / 2;			/* 2 features per band */
    nimages = mxGetN(prhs[0]);

    if (nrhs < 2)
	ns = 16;
    else
	ns = (int) *(mxGetPr(prhs[1]));

    /* Create/allocate output */
    plhs[0] = mxCreateDoubleMatrix(ns, nimages, mxREAL);
    rr = mxGetPr(plhs[0]);

    /* Array to keep distances */
    dist = (double *) malloc(nimages * sizeof(double));

    for (q = 0; q < nimages; q++) {
	for (i = 0; i < nimages; i++)
	    dist[i] = probdist(fs + q * nf, fs + i * nf, nb);

	c = q / ns;			/* class index of the query */

	/* Find the rank of the relevant images */
	for (r = 0; r < ns; r++) {
	    pr = rr + q * ns + r;	/* pointer to the corresponding rank */
	    dr = dist[c * ns + r];	/* distance of the relevant image */
	    *pr = 1;			/* restart */

	    for (i = 0; i < nimages; i++)
		if (dist[i] < dr)
		    (*pr)++;
	}
    }

    free(dist);
}
