function [ f] = normalize( X )
f=[];
M=repmat(mean(X),size(X,1),1);
S=repmat(std(X),size(X,1),1);
f=(X-M)./S;


end

