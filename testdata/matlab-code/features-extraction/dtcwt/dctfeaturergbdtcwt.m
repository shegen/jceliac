function [d] = dctfeaturergb(fv)
%Wendet spezielle DCT auf die Featurevektoren an.
s=size(fv,2)/6/2/3; 

 
hdfv=[];
hh=[];
for k=0:2 
	hh=[];
  	hh=fv(:,1+k*size(fv,2)/3 : (k+1)*(size(fv,2)/3));
	for n=0:5  %mu vorordnen
		for m=1:12:size(hh,2)
			m=m+2*n;
			hdfv=[hdfv,hh(:,m)];
		end
	end

	for n=0:5 % sigma hinten nachordnen
		for m=2:12:size(hh,2)
			m=m+2*n;
			hdfv=[hdfv,hh(:,m)];
		end
	end  
end





%DCT
d=[];
vec=1:s;
b=size(hdfv,2)/s;
m=size(hdfv,1);
for k=1:m
    for h=0:b-1
    d(k,vec+h*s)=fft(hdfv(k,vec+h*s));
    end
end



end

