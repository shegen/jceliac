#include "mex.h"
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_sf.h>

double 
kldist(double a1, double b1, double a2, double b2) {
    double res;
		res = -1.0 + gsl_sf_psi(1.0) + gsl_sf_log(pow(a1,-b1)*b1) - gsl_sf_log(pow(a2,-b2)*b2) + 
			gsl_sf_gamma(1+b2/b1)*pow(a1/a2,b2) + gsl_sf_log(a1)*b1 - gsl_sf_log(a1)*b2 - gsl_sf_psi(1)*b2/b1;
    return ((res > 0.0) ? res : 0.0);
}

double 
probdist(double *fv1, double *fv2, int nb) {
    int i;
    double dist = 0.0;
    for (i = 0; i < nb; i++) {
			dist += 0.5 *(kldist(fv1[2*i], fv1[2*i+1], fv2[2*i], fv2[2*i+1]) + 
		 kldist(fv2[2*i], fv2[2*i+1], fv1[2*i], fv1[2*i+1]));
    }
    return dist;
}


void
mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    double	*fs;
    double	*rr;
    int		nimages, ns, nb, nf;
    int		q, i, c, r;
    double	*pr;
    int cnt = 0;
    fs = mxGetPr(prhs[0]);
    nf = mxGetM(prhs[0]);
    nb = nf / 2;			/* 2 features per band */
    nimages = mxGetN(prhs[0]);

    plhs[0] = mxCreateDoubleMatrix(nimages, nimages, mxREAL);
    rr = mxGetPr(plhs[0]);
    cnt = 0;
    for (q = 0; q < nimages; q++) {
    	for (i = 0; i < nimages; i++)  {
    	    rr[cnt] = probdist(fs + q * nf, fs + i * nf, nb);
            cnt = cnt + 1;
        }
    }
}
