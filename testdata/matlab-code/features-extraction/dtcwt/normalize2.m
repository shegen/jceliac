function [ f] = normalize( X )
f=[];
M=repmat(mean(X),size(X,1),1);
s=std(X);
s1=find(s<=0.00001);
s(s1)=100;
S=repmat(s,size(X,1),1);
f=(X-M)./S;


end

