function res = rankdtcwt(data,varargin)
    p = inputParser;
    p.addRequired('data',@iscell);
    p.addParamValue('debug',false,@islogical);    
    p.addParamValue('method','l1', @(x)any(strcmpi(x,{'ma','jainhealey','l1','l2','weibull','gamma'})));
    p.addParamValue('splits',16,@isnumeric);
    p.parse(data,varargin{:});
    method = p.Results.method;
    splits = p.Results.splits;
    debug = p.Results.debug;
    len = length(data);
    
    featureMatrix = [];
    for i=1:len
        featureMatrix(i,:) = data{i}.features;
    end
    normV = std(featureMatrix);
    fs = featureMatrix./repmat(normV,size(featureMatrix,1),1);
    D = [];    
    if (sum(strcmp(method,{'ma','jainhealey','l1','l2'})) > 0)
        for i=1:len
            xi = data{i};
            if (debug)
                tic;
                fprintf('processing query %d ', i);
            end
            muD = []; sigmaD = []; 
            for j=1:len
               xj = data{j};
               switch method
                   % city-block like distance, normalized by std. deviation of
                   % the features
                   case 'ma'
                        muD = abs((xi.features(1:2:end) - xj.features(1:2:end))./normV(1:2:end));
                        sigmaD = abs((xi.features(2:2:end) - xj.features(2:2:end))./normV(2:2:end));
                        D(i,j) = sum(muD + sigmaD);
                   % Pseudo - Euclidean distance
                   case 'jainhealey'
                        % eq. (12) of Jain & Healey paper
                        D(i,j) = sum(power((xi.features - xj.features)./normV,2));
                   case 'l1' % classic l2 norm
                        D(i,j) = sum(abs(fs(i,:) - fs(j,:)));
                   case 'l2' % classic l2 norm
                        D(i,j) = sum((fs(i,:) - fs(j,:)).^2);   
               end
            end
            if (debug)
                tim = toc;
                fprintf('(%0.2f [s])\n', tim);
            end
        end
    else
        if (debug)
            fprintf('Calling mex file implementation ');
            tic;
        end
        switch method
            % kl-divergence between generalized gaussian distributions
            % (symmetrized) - see Do & Vetterli 2001
            case 'weibull'
                D = wblpd(featureMatrix',splits);
            case 'gamma'
                D = gampd(featureMatrix',splits);
        end
        if (debug)
            tim = toc;
            fprintf('(%0.2f [s])\n', tim);
        end
    end
    res.distance = D;
    res.metric = method;
end
