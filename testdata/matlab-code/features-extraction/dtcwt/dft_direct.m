function [ out ] = dft_direct( data )
%UNTITLED Summary of this function goes here
%                    N
%      X(k) =       sum  x(n)*exp(-j*2*pi*(k-1)*(n-1)/N), 1 <= k <= N.
%                   n=1


    X = zeros(size(data));
    N = length(data);
    
    for k=1:N
       % Xk = 0;
        Xkcos = 0;
        Xksin = 0;
        for n=1:N
            %Xk = Xk + data(n) * exp(-1i*2*pi*(k-1) * (n-1)/N);
            Xkcos = Xkcos + data(n) * cos(2*pi*(k-1) * (n-1)/N);
            Xksin = Xksin + data(n) * sin(2*pi*(k-1) * (n-1)/N);
        end
        X(k) = Xkcos - 1i*Xksin;
    end   
    out = X;
end

