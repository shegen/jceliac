function dispJavaFilt( inmat )

    str = [];
    for i=1:size(inmat,1)
        
        if(size(inmat,2) > 1)
            str = [str, '{', sprintf('%0.20e, ',inmat(i,1:end-1))];
            str = [str,  sprintf('%0.20e},',inmat(i,end))];
           % str = [str, '\n'];
        else
            str = [str, '{', sprintf('%0.20e',inmat(i,:)) '},'];
            %if(mod(i,5) == 0)
            %    str = [str, '\n'];
           % end
        end
    end
    disp(sprintf(str));

end

