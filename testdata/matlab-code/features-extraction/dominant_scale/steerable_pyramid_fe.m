function [feature_vector,dom_scales,dom_orientations]=steerable_pyramid_fe(data,varargin)
%steerable pyramid decomposition with (default) or without dominantscale


p = inputParser;
    p.addRequired('data',@iscell);
    p.addParamValue('debug',false,@islogical);
    p.addParamValue('preprocess',false,@islogical);
    p.addParamValue('dominant_scale',true,@islogical);
    
    p.parse(data,varargin{:});
    debug = p.Results.debug;
    preprocess = p.Results.preprocess;
    dominant_scale = p.Results.dominant_scale;
    
    progname = 'steerable_pyramid';
    if (debug)
        fprintf('%s: processing %d entries, %d scales using feature %s\n', progname, length(data), scales, method);
    end
    feature_vector=[];
    for i=1:length(data)
        if (debug)
            tic;
        end
        im = data{i}.image; 
        im = rgb2gray(im);
        im=double(im);
        options.wavelet_type = 'daubechies';
        options.nb_orientations =4;
        h=log2(size(im,1));
        Jmin=h-2;%We want 2 scale levels;
        MS = perform_steerable_transform(im, Jmin,options);
        
        if (dominant_scale)
            
            Energy=[sum(sum(abs(MS{2}))),sum(sum(abs(MS{3}))), sum(sum(abs(MS{4}))), sum(sum(abs(MS{5}))); ...
                    sum(sum(abs(MS{6}))),sum(sum(abs(MS{7}))), sum(sum(abs(MS{8}))), sum(sum(abs(MS{9})))];
                
            %orientations of the subband Energies of the matrix Energy: orientations in
            %the x-direction (horizontal) and scales in the y-directions


            energy_across_orientations=sum(Energy'); %Energy of the 2 scales
            energy_across_scales= sum(Energy); %Energy of the 4 directions         

            [maxvalue,dominant_orientation]=max(energy_across_scales);
            [maxvalue,dominant_scale]=max(energy_across_orientations);
             dom_scales(i)=dominant_scale;
             dom_orientations(i)=dominant_orientation;
            

            featurevectormean=[mean2(abs(MS{2})),mean2(abs(MS{3})),mean2(abs(MS{4})),mean2(abs(MS{5})); ...
                           mean2(abs(MS{6})),mean2(abs(MS{7})),mean2(abs(MS{8})),mean2(abs(MS{9}))];
            featurevectorstd=[std2(abs(MS{2})),std2(abs(MS{3})),std2(abs(MS{4})),std2(abs(MS{5})); ...
                           std2(abs(MS{6})),std2(abs(MS{7})),std2(abs(MS{8})),std2(abs(MS{9}))];

            dominantfvmean=[[featurevectormean(dominant_scale:end,dominant_orientation:end)], [featurevectormean(dominant_scale:end,1:dominant_orientation-1)]; ...
                            [featurevectormean(1:dominant_scale-1,dominant_orientation:end)], [featurevectormean(1:dominant_scale-1,1:dominant_orientation-1)]];

            dominantfvstd=[[featurevectorstd(dominant_scale:end,dominant_orientation:end)], [featurevectorstd(dominant_scale:end,1:dominant_orientation-1)]; ...
                           [featurevectorstd(1:dominant_scale-1,dominant_orientation:end)], [featurevectorstd(1:dominant_scale-1,1:dominant_orientation-1)]];


            fv=[dominantfvmean(:);dominantfvstd(:)]';
            
        else
             featurevectormean=[mean2(abs(MS{2})),mean2(abs(MS{3})),mean2(abs(MS{4})),mean2(abs(MS{5})); ...
                           mean2(abs(MS{6})),mean2(abs(MS{7})),mean2(abs(MS{8})),mean2(abs(MS{9}))];
             featurevectorstd=[std2(abs(MS{2})),std2(abs(MS{3})),std2(abs(MS{4})),std2(abs(MS{5})); ...
                           std2(abs(MS{6})),std2(abs(MS{7})),std2(abs(MS{8})),std2(abs(MS{9}))];
                       
              fv=[featurevectormean(:);featurevectorstd(:)]';          
        end
        
        feature_vector=[feature_vector;fv];
        
        if (debug)
            tim = toc;
            fprintf('%s: processed entry %d in %0.2f [s]\n', progname , i, tim);
        end
    end
end


