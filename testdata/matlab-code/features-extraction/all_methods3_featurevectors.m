%like all_methods3.m but also saving the feature vectors of the employed
%methods
%Allthe original scale invariant methods for the scale invariance test
%using the curet database, where the test set (evaluation set) contains
%scaled images (classes 29, 30,31,32) while the  trainingset consists of
%original images (classes 2,11,12,14)
% 
%IMPORTANT!!! :
%before computing change evalpitdifknntestandtrainset to
%p.addParamValue('scales',true,@islogical);  . Scales must be true and:
%
%and it has to look like this:
%
%		if (scales)
%			if (traindata{i}.name(4:6)==testdata{ind(k)}.name(4:6) & traindata{i}.idx==testdata{ind(k)}.idx ) %wenn 2 Bilder gleich sind(bis auf scale), wird das nächst genommen.
%					k=k+1;				%Dies ist der Unterschied zu Evalpit, wo gezählt wird, wie viele Bilder gleich sind 
%           end
%        end


curettrain=pitload('~/Bilder/scaleinvCuret3/trainset/','*.png');
curettest=pitload('~/Bilder/scaleinvCuret3/testset/','*.png');



[fvtestdomscale,dom_scales,dom_orientations]=steerable_pyramid_fe(curettest);
 sum(dom_scales~=1)

[fvtraindomscale,dom_scales,dom_orientations]=steerable_pyramid_fe(curettrain);
 sum(dom_scales~=1)

 [result1,result_2class,result_4class,Rdomscale,NN]=eval_test_and_trainset(fvtestdomscale,curettest,fvtraindomscale,curettrain,'CUReTscales',true,'kvalue',10);
result1
 
[r,Rtraindomscale]=eval_llocv(fvtraindomscale,curettrain,'kvalue',10);


fvtestlogpolarwpscaled=logpolarwpfe(curettest);
fvtrainlogpolarwpscaled=logpolarwpfe(curettrain);


 [result3,result_2class,result_4class,Rlogpolarwpscaled,NN]=eval_test_and_trainset(fvtestlogpolarwpscaled,curettest,fvtrainlogpolarwpscaled,curettrain,'CUReTscales',true,'kvalue',10);
result3
[r,Rtrainlogpolarwpscaled]=eval_llocv(fvtrainlogpolarwpscaled,curettrain,'kvalue',10);







fe=gaborfe_fixed_a(curettest,'scaling_factor',2,'grayscale',true);
fvtest=dtcwtex(fe);
fe=gaborfe_fixed_a(curettrain,'scaling_factor',2,'grayscale',true);
fvtrain=dtcwtex(fe);

distmat=disteu_slidematching_gabor(fvtest',fvtrain');
Dslidematchingscaledtest=distmat;
distmatoneset=disteu_slidematching_gabor(fvtrain',fvtrain');
Dslidematchingscaledtrain=distmatoneset;
 min_k=2;
kvalue=10;
knn=10;

     for k=1:kvalue
        [cp,Rtrainslidematchingscaled(k,:)]=evalpitdifknn(curettrain,distmatoneset,'knn',k);
        result_4class(k)=cp.corr;
    
                result_2class(k)=(cp.cou(1,1)+sum(sum(cp.coun(2:end,2:end))))/sum(sum(cp.coun));
           
     end
     result5train=result_4class;
     result={};
      [result{2,1},result{2,2}]=max(result_2class);
      k2c=result{2,2};% k-value for the best result of the train set
      r1=max(result_2class(min_k+1:end));
      k2c_kmin=max(find(result_2class==r1));
      [result{2,3},result{2,4}]=max(result_4class);  
      k4c=result{2,4};% k-value for the best result of the train set
      r1=max(result_4class(min_k+1:end));
      k4c_kmin=max(find(result_4class==r1));
      result{1,11}='maxtrainset_2c';
      result{2,11}=result{2,1};
      result{1,12}='maxtrainset_4c';
      result{2,12}=result{2,3};
      
      %[cp,Rtrainslidematchingscaled]=evalpitdifknn(curettrain,distmatoneset,'knn',k4c);  
    
    for k=1:kvalue
        [cp,Rslidematchingscaled(k,:),NN]=evalpitdifknntestandtrainset(curettrain,curettest,distmat,'knn',k,'CUReTscales',true);
        result_4class(k)=cp.corr;
         result_2class(k)=(cp.cou(1,1)+sum(sum(cp.coun(2:end,2:end))))/sum(sum(cp.coun));
          
    end
  
    result{1,1}='max2c'; 
    result{1,2}='2c_k';
    result{1,3}='max4c';
    result{1,4}='4c_k';
    result{1,5}='mean2c';
    result{1,6}='mean4c';
     
    [result{2,1},result{2,2}]=max(result_2class);
    [result{2,3},result{2,4}]=max(result_4class);
    result{2,5}=mean(result_2class);
    result{2,6}=mean(result_4class);
    
    knnstring=num2str(k2c);%best k-value of the train set
    knnstring2c=['knn2c=',knnstring];
    result{1,7}=knnstring2c;
    result{2,7}=result_2class(k2c);%most interesting result (because only in the train set optimized)
  
    knnstring=num2str(k4c);%best k-value of the train set
     knnstring4c=['knn4c=',knnstring];
     result{1,8}=knnstring4c;
     result{2,8}=result_4class(k4c);%most interesting result (because only in the train set optimized)
  
     knnstring=num2str(k2c_kmin);%best k-value of the train set wit k>kmin
     knnstring2=num2str(min_k);
    knnstring2c=['knn2c_kmin=',knnstring2,' : ',knnstring];
    result{1,9}=knnstring2c;
    result{2,9}=result_2class(k2c_kmin);%most interesting result (because only in the train set optimized)
  
    knnstring=num2str(k4c_kmin);%best k-value of the train set with k>kmin
    knnstring2=num2str(min_k);
    knnstring4c=['knn4c_kmin=',knnstring2,' : ',knnstring];
     result{1,10}=knnstring4c;
     result{2,10}=result_4class(k4c_kmin);
     
     
     
    
%ROC     
     
knn=10;

 roc=true;
NN1=NN(:,1:knn);
	c=find(NN1>1);
    

for k=1:length(curettest)
     indexset(k)= curettest{k}.idx;
end
c=find(indexset >= 2);
indexset(c)=2;
c=find(NN1>1);
NN1(c)=2;
Positives=sum(NN1'==2);
Klasse=[];
for i=1:knn
    for k=1:length(curettest)
      Klasse(k)= Positives(k)>=i;
    end
    cp(i) = classperf(indexset-1,Klasse);%da indexset werte zwische 1und 2 hat (1=negativ 2=positiv)
            %Klasse: 0=negativ, 1=positiv.  daher indexset-1.
%Achtung!!!! Da bei Fkt classperf die erste Klasse als Positiv gilt wird bei
    %classperf die sensitivity als specificity ausgegeben und umgekehrt.
end
TPR=[];
FPR=[];

%Berechne True Positive Rate (TPR) und False Positive Rate 
for i=1:knn
    TPR(i)=cp(i).Specificity;% normalerweise: True Positive Rate = Sensitivity 
    %=(Anzahl der richtig klassifizierten Positiven /Anzahl der Positiven)
    %Da bei Fkt classperf die erste Klasse als Positiv gilt wird bei
    %classperf die sensitivity als specificity ausgegeben und umgekehrt.
    %(deshalb  TPR(i)=cp(i).Specificit)
   FPR(i)=1-cp(i).Sensitivity; %normalerweise: False Positive Rate = Specificity 
    %=(Anzahl der richtig klassifizierten Negativen /Anzahl der Negativen)
    %jedoch aus gleiche Gründen wie bei TPR wird Die FPR auf diese Weise
    %errechnet.
    
end
TPR=[1,TPR,0];
FPR=[1,FPR,0];
TPRFPR=[TPR;FPR];

if roc
    plot(FPR,TPR);set(gca(),'XLim',[0 1],'YLim',[0 1]);
end

%Berechne arear under curve (AUC) mit Trapez-Integration
AUC=0;
for i=2:length(TPR)
    AUC=AUC + TPR(i) * (FPR(i-1)-FPR(i)) - 1/2* ( (TPR(i-1)-TPR(i)) * (FPR(i-1)-FPR(i)) );
end




result{1,13}='AUC';
result{2,13}=AUC;
result5=result
 
%[cp,Rslidematchingscaled]=evalpitdifknntestandtrainset(curettrain,curettest,distmat,'knn',k4c,'CUReTscales',true);%only to get the right 'R' 

cp1=cp













 fe=nnfe(curettest,'method','scmentropy');
fvtestscmscaled=dtcwtex(fe);
fe=nnfe(curettrain,'method','scmentropy');
 fvtrainscmscaled=dtcwtex(fe);
 [result7,result_2class,result_4class,Rscmscaled,NN]=eval_test_and_trainset(fvtestscmscaled,curettest,fvtrainscmscaled,curettrain,'CUReTscales',true,'kvalue',10);
result7
[r,Rtrainscmscaled]=eval_llocv(fvtrainscmscaled,curettrain,'kvalue',10);





 fe=nnfe(curettest,'method','icmentropy');
fvtesticmscaled=dtcwtex(fe);
fe=nnfe(curettrain,'method','icmentropy');
 fvtrainicmscaled=dtcwtex(fe);
 [result9,result_2class,result_4class,Ricmscaled,NN]=eval_test_and_trainset(fvtesticmscaled,curettest,fvtrainicmscaled,curettrain,'CUReTscales',true,'kvalue',10);
result9
[r,Rtrainicmscaled]=eval_llocv(fvtrainicmscaled,curettrain,'kvalue',10);







[result11,distmat,fvtrainfractalscaled,result_2class,result_4class,Rfractalscaled,fvtestfractalscaled]=fractaloriginal(curettrain,'datatest',curettest,'kvalue',10,'CUReTscales',true);
result11
[r,distmat,dim,result_2class,result_4class,Rtrainfractalscaled]=fractaloriginal(curettrain,'kvalue',10);



[result13,distmat,fvtrainfractalfilterscaled,result_2class,result_4class,Rfractalfilterscaled,fvtestfractalfilterscaled]=fractalmr8cluster(curettrain,'datatest',curettest,...
    'method','original','reduceFD',true,'reduce_factor',3,'kvalue',10,'CUReTscales',true);
result13
[r,distmat,dim,result_2class,result_4class,Rtrainfractalfilterscaled]=fractalmr8cluster(curettrain, ...
    'method','original','reduceFD',true,'reduce_factor',3,'kvalue',10);





[result15,distmat,fvtrainsiftscaled,result_2class,result_4class,Rsiftscaled,fvtestsiftscaled]=siftcluster(curettrain,'datatest',curettest,'reduceFD',true,'reduce_factor',3,'kvalue',10,'CUReTscales',true);
result15

[r,distmat,dim,result_2class,result_4class,Rtrainsiftscaled]=siftcluster(curettrain,'reduceFD',true,'reduce_factor',3,'kvalue',10);








%sparse_regions_fv wird normalerweise von mir auf meinem pma account
%gerechnet (zu wenig Speicherplatz auf stud2 account) und dann
%hereingeladen





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%scale variant methods for comparison





fe=lbpfe(curettrain,'grayscale',true);
fv=[];
 for i=1:length(fe)
    fv(i,:)=fe{i}{1};
end
fvtrainlbp=fv;


fe=lbpfe(curettest,'grayscale',true);
fv=[];
 for i=1:length(fe)
    fv(i,:)=fe{i}{1};
end
fvtestlbp=fv;


[result21,result_2class,result_4class,Rlbp,NN]=eval_test_and_trainset(fvtestlbp,curettest,fvtrainlbp,curettrain,'CUReTscales',true,'kvalue',10);
result21
[r,Rtrainlbp]=eval_llocv(fvtrainlbp,curettrain,'kvalue',10);






fe=gaborfe(curettrain,'imsize',200,'scales',5);
fvtraingabor=dtcwtex(fe);

fe=gaborfe(curettest,'imsize',200,'scales',5);
fvtestgabor=dtcwtex(fe);



[result23,result_2class,result_4class,Rgabor,NN]=eval_test_and_trainset(fvtestgabor,curettest,fvtraingabor,curettrain,'CUReTscales',true,'kvalue',10);
result23
[r,Rtraingabor]=eval_llocv(fvtraingabor,curettrain,'kvalue',10);



 fe=dtcwtfepre(curettest);
fvtest=dtcwtex(fe);
 fe=dtcwtfepre(curettrain);
 fvtrain=dtcwtex(fe);

  fvtraindtcwt=normalize(fvtrain);
fvtestdtcwt=normalize(fvtest);


[result24,result_2class,result_4class,Rdtcwt,NN]=eval_test_and_trainset(fvtestdtcwt,curettest,fvtraindtcwt,curettrain,'CUReTscales',true,'kvalue',10);
result24
[r,Rtraindtcwt]=eval_llocv(fvtraindtcwt,curettrain,'kvalue',10);

%%zwischenspeichern


save('~/matlab/matlabfiles/CUReT_fv.mat','fvtraindomscale','Dslidematchingscaledtrain','fvtrainlogpolarwpscaled','fvtrainfractalscaled',...
    'fvtrainfractalfilterscaled','fvtrainsiftscaled',...
    'fvtrainicmscaled','fvtrainscmscaled','fvtrainlbp','fvtraingabor','fvtraindtcwt',...
  'fvtestdomscale','Dslidematchingscaledtest','fvtestlogpolarwpscaled','fvtestfractalscaled',...
  'fvtestfractalfilterscaled','fvtestsiftscaled',...
  'fvtesticmscaled','fvtestscmscaled','fvtestlbp','fvtestgabor','fvtestdtcwt');





% zeitaufwändige tests



[result17, fvtestorientationframeletscaled, fvtrainorientationframeletscaled,Rorientationframeletscaled]=orientationframelet2(curettrain,'datatest',curettest,'kvalue',10,'CUReTscales',true);
result17
%[r, feature_framelet_test, feature_framelet_train,Rtrainorientationframeletscaled]=orientationframelet2(curettrain,'kvalue',10);


[r,Rtrainorientationframeletscaled]=eval_llocv(fvtrainorientationframeletscaled,curettrain,'kvalue',10);




 [blobnumberscaled,blobirrscaled]=bloboriginal2(curettrain,'irregularity',true);
 fvtrain=[blobnumberscaled,blobirrscaled];
 fvtrainblobscaled=normalize2(fvtrain);
 blobirrscaled=normalize2(blobirrscaled);
 fvtrainblobscaledirr=blobirrscaled;
   [blobnumbertest,blobirrtest]=bloboriginal2(curettest,'irregularity',true); 
fvtest=[blobnumbertest,blobirrtest];
 fvtestblobscaled=normalize2(fvtest);  
 blobirrtest=normalize2(blobirrtest); 
fvtestblobscaledirr=blobirrscaled;

   
[result19,result_2class,result_4class,Rblobscaled,NN]=eval_test_and_trainset(fvtestblobscaled,curettest,fvtrainblobscaled,curettrain,'CUReTscales',true,'kvalue',10);
result19
[r,Rtrainblobscaled]=eval_llocv(fvtrainblobscaled,curettrain,'kvalue',10);




[result199,result_2class,result_4class,Rblobscaledirr,NN]=eval_test_and_trainset(blobirrtest,curettest,blobirrscaled,curettrain,'CUReTscales',true,'kvalue',10);
result199
[r,Rtrainblobscaledirr]=eval_llocv(blobirrscaled,curettrain,'kvalue',10);




%%%%%%

Rall=[Rdomscale(:)' ;Rslidematchingscaled(:)'; Rlogpolarwpscaled(:)';Rfractalscaled(:)';Rfractalfilterscaled(:)';...
    Rorientationframeletscaled(:)';Rsiftscaled(:)'; Ricmscaled(:)';Rscmscaled(:)';Rblobscaled(:)';Rblobscaledirr(:)';Rlbp(:)';Rgabor(:)';Rdtcwt(:)'];









Rall=[Rdomscale(:)' ;Rslidematchingscaled(:)'; Rlogpolarwpscaled(:)';Rfractalscaled(:)';Rfractalfilterscaled(:)';...
    Rorientationframeletscaled(:)';Rsiftscaled(:)'; Ricmscaled(:)';Rscmscaled(:)';Rblobscaled(:)';Rblobscaledirr(:)';Rlbp(:)';Rgabor(:)';Rdtcwt(:)'];

RallCuret2=Rall;


save('~/matlab/matlabfiles/CUReT_R2.mat','RallCuret2')


Rtrainall=[Rtraindomscale(:)' ;Rtrainslidematchingscaled(:)';Rtrainlogpolarwpscaled(:)';Rtrainfractalscaled(:)';Rtrainfractalfilterscaled(:)';...
    Rtrainorientationframeletscaled(:)';Rtrainsiftscaled(:)';...
    Rtrainicmscaled(:)';Rtrainscmscaled(:)';Rtrainblobscaled(:)';Rtrainblobscaledirr(:)';Rtrainlbp(:)';Rtraingabor(:)';Rtraindtcwt(:)'];

RtrainallCuret2=Rtrainall;

save('~/matlab/matlabfiles/CUReT_Rtrain2.mat','RtrainallCuret2')





save('~/matlab/matlabfiles/CUReT_fv.mat','fvtraindomscale','Dslidematchingscaledtrain','fvtrainlogpolarwpscaled','fvtrainfractalscaled',...
    'fvtrainfractalfilterscaled','fvtrainorientationframeletscaled','fvtrainsiftscaled',...
    'fvtrainicmscaled','fvtrainscmscaled','fvtrainblobscaled','fvtrainblobscaledirr','fvtrainlbp','fvtraingabor','fvtraindtcwt',...
  'fvtestdomscale','Dslidematchingscaledtest','fvtestlogpolarwpscaled','fvtestfractalscaled',...
  'fvtestfractalfilterscaled','fvtestorientationframeletscaled','fvtestsiftscaled',...
  'fvtesticmscaled','fvtestscmscaled','fvtestblobscaled','fvtestblobscaledirr','fvtestlbp','fvtestgabor','fvtestdtcwt');









% 
% [resultAffineRegionstrain,RtrainAffineRegions]=sparse_regions_fv('/storage/databases/scaleinvCuret3/trainset/','*.haraff.sift',...
% 'dataset','sonstige','kvalue',10);
% [resultAffineRegions,RAffineRegions,NN,fvtrainAffineRegions,fvtestAffineRegions]=sparse_regions_fv('/storage/databases/scaleinvCuret3/trainset/',...
%'*.haraff.sift','featurefiletest','/storage/databases/scaleinvCuret3/testset/','dataset','sonstige','kvalue',10,'CUReTscales',true);
% 
% save('/tmp/R_CuretAffineRegions2.mat','RAffineRegions','RtrainAffineRegions','fvtrainAffineRegions','fvtestAffineRegions');
% 
%
% scp /tmp/R_CuretAffineRegions2.mat g.wimmer@bartgeier:~/matlab/matlabfiles/
% 
% load('~/matlab/matlabfiles/R_CuretAffineRegions2.mat');
% load('~/matlab/matlabfiles/CUReT_R2.mat');
%load('~/matlab/matlabfiles/CUReT_Rtrain2.mat');
% RallCuret2=[RallCuret2(1:end-3,:) ; RAffineRegions(:)' ; RallCuret2(end-2:end,:)];
%RtrainallCuret2=[RtrainallCuret2(1:end-3,:) ; RtrainAffineRegions(:)' ; RtrainallCuret2(end-2:end,:)];
% 
% save('~/matlab/matlabfiles/RCUReT2.mat','RtrainallCuret2','RallCuret2');
% 
% 
% curettest=pitload('~/Bilder/scaleinvCuret3/testset/','*.png');
% curettrain=pitload('~/Bilder/scaleinvCuret3/trainset/','*.png');
% 
% shit=zeros( size(RtrainallCuret2,1), size(RtrainallCuret2,2)/10);  
% for i=1:size(RtrainallCuret2,2)/10
%     for j=1:size(RtrainallCuret2,1)
%         R=RtrainallCuret2(j,i*10-9:i*10);
%         counter=[];
%         for k=1:10
%             counter(k)=sum(R==k);
%         end
%         [maxvalue,Rtrain_majority(j,i)]=max(counter);
%         b=find(counter==maxvalue);
%         if(length(b)>1)
%            shit(j,i)=1;
%            
%         end
%     end
% end
% sum(sum(shit))
% shit=zeros( size(RtrainallCuret2,1), size(RtrainallCuret2,2)/10);
% for i=1:size(RallCuret2,2)/10
%     for j=1:size(RallCuret2,1)
%         R=RallCuret2(j,i*10-9:i*10);
%         counter=[];
%         for k=1:10
%             counter(k)=sum(R==k);
%         end
%         [maxvalue,Rtest_majority(j,i)]=max(counter);
%         b=find(counter==maxvalue);
%         if(length(b)>1)
%            shit(j,i)=1;
%            
%         end
%     end
% end
% sum(sum(shit))
% [output,p,thr]=mcnemarscaleanalysis(Rtrain_majority,Rtest_majority,curettrain,curettest,'alpha',0.01)
% 
% 
% 
% %  alpha=0.001
% %  [output0001]=mcnemarmatrix(Rall,curettest,alpha)
% %  alpha=0.05;
% % [output005]=mcnemarmatrix(Rall,curettest,alpha)
% % alpha=0.01
% %  [output001]=mcnemarmatrix(Rall,curettest,alpha)
% % 
















