function [ out ] = mylogpolar( image )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    S = size(image,1);
    N = size(image,2);
    R = N;
    
    p = zeros(S,N);
    for a=0:S-1
        for r=0:floor(N/2)-1
            indy = floor(N/2) - r * sin((2*pi*a)/S);
            indx = floor(N/2) + r * cos((2*pi*a)/S);
            val = interp2(image, indx+1,indy+1, 'linear');
            p(a+1,r+1) = val;
        end
    end

    lp = zeros(S,N);
    for i=0:S-1
        for j=0:N-1
            ind = floor((log2(j+2) / log2(R+2)) * floor(N/2));
            lp(i+1,j+1) = p(i+1,ind+1);
        end
    end
    out = lp;
end

