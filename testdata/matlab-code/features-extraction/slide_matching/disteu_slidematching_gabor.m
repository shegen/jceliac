function [D,sliding_direction] = disteu_slidematching_gabor(fvtest,fvtrain)
%only for summed up orientations and 4 scales 


fvtest=fvtest([5,6,9,10,13,14,17,18],:);%only nominal scales

%shifted train vectors
fv1=fvtrain([1 2 5 6 9 10 13 14],:);%shifting -2
fv2=fvtrain([3 4 7 8 11 12 15 16],:);%shifting -1
fv3=fvtrain([5,6,9,10,13,14,17,18],:);%no shifting
fv4=fvtrain([7 8 11 12 15 16 19 20],:);%shifting +1
fv5=fvtrain([9,10,13,14,17,18,21,22],:);%shifting +2




[M, N] = size(fv1);
[M2, P] = size(fvtest);

if (M ~= M2)
    error('Matrix dimensions do not match.')
end



if(N>P)

    copies = zeros(1,N);
    for p = 1:P
        d1(:,p) = sum((fv1 - fvtest(:, p+copies)) .^2, 1)';%Minimum falls abfragebild um den scale factor 2 kleiner ist als vergleichsbild 
    end
    for p = 1:P
        d2(:,p) = sum((fv2 - fvtest(:, p+copies)) .^2, 1)';
    end
    for p = 1:P
        d3(:,p) = sum((fv3 - fvtest(:, p+copies)) .^2, 1)';
    end
    for p = 1:P
        d4(:,p) = sum((fv4 - fvtest(:, p+copies)) .^2, 1)';
    end
    for p = 1:P
        d5(:,p) = sum((fv5 - fvtest(:, p+copies)) .^2, 1)';
    end
else
    copies = zeros(1,P);
    for n = 1:N
        d1(n,:) = sum((fv1(:, n+copies) - fvtest).^2, 1)';%Minimum falls abfragebild um den scale factor 2 kleiner ist als vergleichsbild 
    end
    for n = 1:N
        d2(n,:) = sum((fv2(:, n+copies) - fvtest).^2, 1)';
    end
    for n = 1:N
        d3(n,:) = sum((fv3(:, n+copies) - fvtest).^2, 1)';
    end
    for n = 1:N
        d4(n,:) = sum((fv4(:, n+copies) - fvtest).^2, 1)';
    end
    for n = 1:N
        d5(n,:) = sum((fv5(:, n+copies) - fvtest).^2, 1)';
    end 
end

    
D=[];
counter=1;
sliding_direction=[];

for i=1:N
    for j=1:P  %nur bis i, da die Matrizzen ja symmetrisch sind (ergibt nur Einträge unterhalb und auf der Diagonale
        [D(i,j),sliding_direction(counter)]=min([d1(i,j),d2(i,j),d3(i,j),d4(i,j),d5(i,j)]);
        counter=counter+1;
    end 
end
D=D.^0.5;
D=D';








