function fe = gaborfe_fixed_a(data,varargin)
    % construct input parser
    p = inputParser;
    p.addRequired('data',@iscell);
    p.addParamValue('scales',4, @(x)x>=1 && x<= 8);
    p.addParamValue('orientations', 6, @(x)x>=2 && x <=8);
    p.addParamValue('debug',false,@islogical);
    p.addParamValue('grayscale',true,@islogical);
    p.addParamValue('frequencies', [0.05 0.4], @(x)~isempty(x));
    p.addParamValue('method','ma', @(x)any(strcmpi(x,{'ma','response','jainhealey-u','jainhealey-o'})));
    p.addParamValue('scaling_factor',sqrt(2));
    p.addParamValue('sum_up_orientations',true,@islogical);
        p.addParamValue('flexible_imsize',false,@islogical);
    % parse inputs
    p.parse(data,varargin{:});
    % assign arguments to variables
    scales = p.Results.scales;
    orientations = p.Results.orientations;
    frequencies = p.Results.frequencies;
    method = p.Results.method;
    grayscale = p.Results.grayscale;
    debug = p.Results.debug;
    scaling_factor = p.Results.scaling_factor;
    sum_up_orientations = p.Results.sum_up_orientations;
    flexible_imsize = p.Results.flexible_imsize;
    len = length(data);
    fprintf('processing %d entries, scales %d, orientations %d\n',len,scales,orientations);
    % build gabor filters (frequenc domain)
    GW = cell(scales,orientations); 
    
    im = data{1}.image;
    im=rgb2gray(im);
    imsize=size(im,1);
    for n = 1:orientations 
        [Gr,Gi,a] = gabor_versuch(imsize,n,scaling_factor,frequencies, [scales orientations],0);
        for s=1:length(Gr)%scales (including additional scales between nominal scales)
            F = fft2(Gr{s}+sqrt(-1)*Gi{s}); 
            F(1,1) = 0;
            GW{s,n} = F; 
        end
    end

    Grstandard=Gr;
    % iterate over data
    for i=1:len
        if (debug)
            tic;
            fprintf('processing image %d ', i);
        end
        im = data{i}.image;
        if (grayscale)
            im = rgb2gray(im);
        end
        im = double(im);
        % iterate over spectral bands (e.g. color channels)
        D = [];
        D1=[];
        for ch=1:size(im,3)
            A = fft2(im(:,:,ch));
            for s = 1:length(Gr) 
                for n=1:orientations
                     if(flexible_imsize)
                         imsize=[size(im,1),size(im,2)];
                        [Gr,Gi,a] = gabor_versuch_rectangular(imsize,n,scaling_factor,frequencies, [scales orientations],0);
                        F = fft2(Gr{s}+sqrt(-1)*Gi{s}); 
                        F(1,1) = 0;
                        GW{s,n} = F;  
                     end

                    band = ifft2(A.*GW{s,n});

                    D = [D mean2(abs(band)) std2(abs(band))];
                end
          
                    

                if (sum_up_orientations)
                        hmean=D(1:2:end);
                        hstd=D(2:2:end);
                        hmeansum=sum(hmean);
                        hmeanstd=sum(hstd);
                        D1=[D1 hmeansum hmeanstd];
                        D=[];    

                end

            end
        end

        

        if (sum_up_orientations)
            fe{i}.features = D1;
        else
            fe{i}.features = D;
        end               

        if (debug)
            tim = toc;
            fprintf('(%0.2f [s])\n',tim);
        end
    end

    
    
    
    
    
    
    
end
        
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 
 
 
function [Gr,Gi,a] = gabor_versuch_rectangular(imsize,orientation,a,freq,partition,flag)

% get parameters
%a is the scale factor
s = 1; %scale is fixed to s=1
n = orientation;
ys=imsize(1);
xs=imsize(2);

Ul = freq(1);
Uh = freq(2);

stage = partition(1);
orientation = partition(2);


% computer best variance of gaussian envelope

u0 = Uh/(a^((stage-s)));
Uvar = ((a-1)*u0)/((a+1)*sqrt(2*log(2)));


z = -2*log(2)*Uvar^2/u0;
Vvar = tan(pi/(2*orientation))*(u0+z)/sqrt(2*log(2)-z*z/(Uvar^2));


% generate the spetial domain of gabor wavelets

j = sqrt(-1);

if (rem(ys,2)==0)
    sidey = ys/2-0.5;
else
    sidey = fix(ys/2);
end;

if (rem(xs,2)==0)
    sidex = xs/2-0.5;
else
    sidex = fix(xs/2);
end;


x = -sidex:1:sidex;
y = (-sidey:1:sidey)';
X = ones(ys,1)*x;
Y = y*ones(1,xs);


t1 = cos(pi/orientation*(n-1));
t2 = sin(pi/orientation*(n-1));

XX = X*t1+Y*t2;
YY = -X*t2+Y*t1;



Xvar = 1/(2*pi*Uvar);
Yvar = 1/(2*pi*Vvar);

coef = 1/(2*pi*Xvar*Yvar);

%Gr = a^(stage-s)*coef*exp(-0.5*((XX.*XX)./(Xvar^2)+(YY.*YY)./(Yvar^2))).*cos(2*pi*u0*XX);
%Gi = a^(stage-s)*coef*exp(-0.5*((XX.*XX)./(Xvar^2)+(YY.*YY)./(Yvar^2))).*sin(2*pi*u0*XX);

%Now that we have all neede values for s=1, we can compute all other values
%based on the values for s=1

%i=0 entspricht s=1, i=2 entspricht s=2,i=4 entspricht s=3, ... 
for i=-2:2*stage
    u0new=u0*(2^(1/4))^i;
    Xvarnew=Xvar*(2^(1/4))^-i;
    Yvarnew=Yvar*(2^(1/4))^-i;
    coefnew = 1/(2*pi*Xvarnew*Yvarnew);
    Gr{(i+3)} = a^(stage-((i+2)/2))*coefnew*exp(-0.5*((XX.*XX)./(Xvarnew^2)+(YY.*YY)./(Yvarnew^2))).*cos(2*pi*u0new*XX);
    Gi{(i+3)} =a^(stage-((i+2)/2))*coefnew*exp(-0.5*((XX.*XX)./(Xvarnew^2)+(YY.*YY)./(Yvarnew^2))).*sin(2*pi*u0new*XX);

end
end


        
        
        
        
        
        
        
               
