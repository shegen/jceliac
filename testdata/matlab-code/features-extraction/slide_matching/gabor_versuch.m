
function [Gr,Gi,a] = gabor_versuch(N,orientation,a,freq,partition,flag)

% get parameters
%a is the scale factor
s = 1; %scale is fixed to s=1
n = orientation;


Ul = freq(1);
Uh = freq(2);

stage = partition(1);
orientation = partition(2);


% computer best variance of gaussian envelope

u0 = Uh/(a^((stage-s)));
Uvar = ((a-1)*u0)/((a+1)*sqrt(2*log(2)));


z = -2*log(2)*Uvar^2/u0;
Vvar = tan(pi/(2*orientation))*(u0+z)/sqrt(2*log(2)-z*z/(Uvar^2));


% generate the spetial domain of gabor wavelets

j = sqrt(-1);

if (rem(N,2) == 0)
    side = N/2-0.5;
else
    side = fix(N/2);
end;

x = -side:1:side;
l = length(x);
y = x';
X = ones(l,1)*x;
Y = y*ones(1,l);

t1 = cos(pi/orientation*(n-1));
t2 = sin(pi/orientation*(n-1));

XX = X*t1+Y*t2;
YY = -X*t2+Y*t1;



Xvar = 1/(2*pi*Uvar);
Yvar = 1/(2*pi*Vvar);

coef = 1/(2*pi*Xvar*Yvar);

%Gr = a^(stage-s)*coef*exp(-0.5*((XX.*XX)./(Xvar^2)+(YY.*YY)./(Yvar^2))).*cos(2*pi*u0*XX);
%Gi = a^(stage-s)*coef*exp(-0.5*((XX.*XX)./(Xvar^2)+(YY.*YY)./(Yvar^2))).*sin(2*pi*u0*XX);

%Now that we have all neede values for s=1, we can compute all other values
%based on the values for s=1

%i=0 entspricht s=1, i=2 entspricht s=2,i=4 entspricht s=3, ... 
for i=-2:2*stage
    u0new=u0*(2^(1/4))^i;
    Xvarnew=Xvar*(2^(1/4))^-i;
    Yvarnew=Yvar*(2^(1/4))^-i;
    coefnew = 1/(2*pi*Xvarnew*Yvarnew);
    Gr{(i+3)} = a^(stage-((i+2)/2))*coefnew*exp(-0.5*((XX.*XX)./(Xvarnew^2)+(YY.*YY)./(Yvarnew^2))).*cos(2*pi*u0new*XX);
    Gi{(i+3)} =a^(stage-((i+2)/2))*coefnew*exp(-0.5*((XX.*XX)./(Xvarnew^2)+(YY.*YY)./(Yvarnew^2))).*sin(2*pi*u0new*XX);

end
end

