function [ out] = scm( im)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
f=0.2;
g=0.9;
h=20;
N=37;
M=[0.1091,0.1409,0.1091;0.1409,0,0.1409;0.1091,0.1409,0.1091];
W=[];
im=double(im);
[m,n]=size(im);


F=zeros(m,n);
Y=F;
Theta=F;

gamma = 1;

for x=1:N
    W=imfilter(double(Y),M, 'conv', 'symmetric');
    
     F=f*F+im.*W+im; % wo ist denn das exp ?
     
    Theta=g*Theta+h*Y;
    Y=F>Theta;
    out(:,:,x)=Y;
end





end