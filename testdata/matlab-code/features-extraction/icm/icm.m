function [ out ] = icm( im,varargin)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
p=inputParser;
p.addRequired('im');
p.addParamValue('N',37, @(x)x>=1 && x<=166);
p.addParamValue('f',0.9, @(x)x>=0 && x<=16);
p.addParamValue('g',0.8, @(x)x>=0 && x<=16);
p.addParamValue('h',20, @(x)x>=0 && x<=166);
p.addParamValue('Mk',0.5);
p.addParamValue('Mg',1);
p.addParamValue('T',0.5);
p.parse(im,varargin{:});
N=p.Results.N;
f=p.Results.f;
g=p.Results.g;
h=p.Results.h;
Mk=p.Results.Mk;
Mg=p.Results.Mg;
thetainit=p.Results.T;




%M=[0.5,1,0.5;1,0,1;0.5,1,0.5];
%M=[Mk,Mg,Mk;Mg,0,Mg;Mk,Mg,Mk];
%W=[];

M = [0.5, 1, 0.5; 1,0,1; 0.5, 1, 0.5];

im=double(im);
[m,n]=size(im);


F=zeros(m,n);
Y=F;
Theta=F+thetainit;%variabel

for x=1:N
   W=imfilter(double(Y),M);
    
    F=f*F+W+im;
    Theta=g*Theta+h*Y;
    Y=F>Theta;
    out(:,:,x)=Y;
end





end












