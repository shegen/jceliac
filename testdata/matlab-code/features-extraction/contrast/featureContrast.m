function F = featureContrast(I,sizeFrom,sizeTo,angleMax)
%
% Author: mgadermayr
%
% compute gray level coocurrance matrix and haralick feature "Contrast"
% Input:
%  I           ... graylevel-image 
%  [size_from] ... start with this offset size
%  [size_to]   ... end with this offset size
%  [angleMax]  ... specify the number of angles considered
% Output:
%  F           ... feature (dimensionality: angleMax)



I = uint8(I);
    

F=[];
for sz=sizeFrom:sizeTo        % size ... size of offset into a direction in pixels
    for i=1:angleMax            % i...type of offset (x-direction, y-direction, x and y)
        if(i==1)
            offset = [sz 0];
        elseif(i==2)
            offset = [0 sz];
        elseif(i==3)
            offset = [sz sz];
        elseif(i==4)
            offset = [sz -sz];
        end
        glcm = graycomatrix(I,'numlevels',256,'symmetric',true,'Offset',offset); % compute the cooccurance matrix (built in function in matlab)
        f1 = graycoprops(glcm,'Contrast');
        F = [ F f1.Contrast ];
    end
end




end
