
function fe = nnfe(data,varargin)
%Neural Networks: 
%	    1.Intersceting cortical model (ICM) (paper Pulse coupled neural networks and one-class support vector machines for geometry invariant texture retrieval)
%       2.Spiking Cortical Model (SCM) (paper  New Spiking Cortical Model for Invariant Texture Retrieval and Image Processing)
%   Parameters are:
%
%       DATA                Is a cell array with elements 'image'
%                           'dim', 'idx', 'filename'. 'image' hold the
%                           color or grayscale image. 'dim' specifies the
%                           dimension of the image (in case of color images
%                           'dim' is 3, otherwise 1). 'idx' is not used
%                           here; in case of image classification
%                           it can be used to specify the class the
%                           image belongs to. Finally, 'filename' holds the
%                           image filename.
%
%  	imman			normalizing the image or dividing the image by a constant.		
%	N		Number of outputs per image (normally 37 (the 2 papers))      
%
    p = inputParser;
    p.addRequired('data',@iscell);
    p.addParamValue('N',37, @(x)x>=1 && x<= 166);
    p.addParamValue('debug',false,@islogical);
    p.addParamValue('preprocess',false,@islogical);
    p.addParamValue('method','scmentropy', @(x)any(strcmpi(x,{'icmentropy','icmstd','icmsum','icmstdmu','scmentropy','scmstd','scmsum','scmstdmu'})));
    p.addParamValue('imman','norm', @(x)any(strcmpi(x,{'max5','norm'}))); %image-manipulation
    p.addParamValue('colorspace','rgb',@(x)any(strcmpi(x,{'rgb','ybr','hsv','yiq'})));
    p.addParamValue('grayscale',true,@islogical);
    p.addParamValue('C',5, @(x)x>=0 && x<= 66);
    p.addParamValue('f',0.9, @(x)x>=0 && x<=16);
    p.addParamValue('g',0.8, @(x)x>=0 && x<=16);
    p.addParamValue('h',20, @(x)x>=0 && x<=360);
    p.addParamValue('Mk',0.5);
    p.addParamValue('Mg',1);
    p.addParamValue('T',0.5,@(x)x>=0 && x<=1);
    p.parse(data,varargin{:});
    debug = p.Results.debug;
    grayscale = p.Results.grayscale;
    preprocess = p.Results.preprocess;
    N=p.Results.N;
    method = p.Results.method;
    imman=p.Results.imman;
    colorspace = p.Results.colorspace;
    C=p.Results.C;
    f=p.Results.f;
    g=p.Results.g;
    h=p.Results.h;
    Mk=p.Results.Mk;%3x3 Matrix M zum falten bestehend aus 2 versch. Werten: Mg=größerer Wert, Mk=kleinerer Wert
    Mg=p.Results.Mg;
    T=p.Results.T;
    progname = 'nn';
    if (debug)
        fprintf('%s: processing %d entries,  using feature %s\n', progname, length(data), method);
    end
    for i=1:length(data)
        if (debug)
            tic;
        end
        im = data{i}.image; 
        if (grayscale && size(im,3) == 3)
            im = rgb2gray(im);
        else
            switch colorspace
                case 'rgb'
                    im = double(im);
                case 'ybr'
                    im = double(rgb2ycbcr(im)); % convert to YBR color model
                case 'hsv'
                    im = double(rgb2hsv(im));
                case 'yiq'
                    im = double(rgb2ntsc(im));    
            end
        end
        vec = []; % Feature Vector
        for ch=1:size(im,3)
	  %  warning off
            channel = double(im(:,:,ch));
            if (preprocess)
                channel=adapthisteq(uint8(channel));
                H=fspecial('gaussian');
                channel=double(imfilter(channel,H,'symmetric'));
            end
	  %  warning on
	    switch imman
                        case 'norm'	
				channel=(channel-mean2(channel))/std2(channel);
			
							
                        case 'max5'
                          	 channel=channel/max(max(channel))*C;
			
	    end
	    vec=[];
    
	    
            
                    switch method
                        case 'icmentropy'	
				hh=icm(channel,'N',N,'f',f,'g',g,'h',h,'Mk',Mk,'Mg',Mg,'T',T);
				for k=1:N
       
					if(mean2(hh(:,:,k))==0 | mean2(hh(:,:,k))==1)
                        vec=[vec,0];
                    else
                        % mean is the actual probability here, 
                        % XXX: p1 and p2 are mixed up in the log2 calculations
                        %vec=[vec,- mean2(hh(:,:,k))*(log2(1-mean2(hh(:,:,k))))-(1-mean2(hh(:,:,k)))*(log2(mean2(hh(:,:,k))))];
                        
                        % my fixed version
                        vec=[vec,-mean2(hh(:,:,k))*(log2(mean2(hh(:,:,k))))-(1-mean2(hh(:,:,k)))*(log2(1-mean2(hh(:,:,k))))];
                        
                    end
				end
							
                        case 'icmstd'
				vec=[vec,std2(channel)];
                          	 hh=icm(channel,'N',N,'f',f,'g',g,'h',h,'Mk',Mk,'Mg',Mg,'T',T);
			   	for k=1:N
					vec=[vec,std2(hh(:,:,k))];
				end

 			case 'icmsum'
                          	 hh=icm(channel,'N',N,'f',f,'g',g,'h',h,'Mk',Mk,'Mg',Mg,'T',T);
			   	for k=1:N
					vec=[vec,sum(sum((hh(:,:,k))))];
				end

			case 'icmstdmu'
                          	 hh=scm(channel);
				vec=[vec,mean2(channel),std2(channel)];
			   	for k=1:N
					vec=[vec,mean2(hh(:,:,k)),std2(hh(:,:,k))];
				end

			case 'scmentropy'	
				hh=scm(channel);
				
				for k=1:N
                    subvec = [];
                    if(mean2(hh(:,:,k))==0 | mean2(hh(:,:,k))==1)
                        subvec = [subvec, 0];
                    else
                        entropy = - mean2(hh(:,:,k))*(log2(mean2(hh(:,:,k))))-(1-mean2(hh(:,:,k)))*(log2(1-mean2(hh(:,:,k))));
                        subvec = [subvec, entropy];
                    end
                        average_residual = sum(sum(abs(hh(:,:,k) - mean2(hh(:,:,k)))));
                        standard_deviation = std2(hh(:,:,k));
                        vec=[vec,subvec,average_residual, standard_deviation];
                   
				end
							
                        case 'scmstd'
				
                          	 hh=scm(channel);
			   	for k=1:N
					vec=[vec,std2(hh(:,:,k))];
				end

 			case 'scmsum'
				
                          	 hh=scm(channel);
			   	for k=1:N
					vec=[vec,sum(sum((hh(:,:,k))))];
                end
                       
			case 'scmstdmu'
				vec=[vec,mean2(channel),std2(channel)];
                          	 hh=scm(channel);
			   	for k=1:N
					vec=[vec,mean2(hh(:,:,k)),std2(hh(:,:,k))];
				end
                    end
                end
             

        
	
        fe{i}.features = vec;
        if (debug)
            tim = toc;
            fprintf('%s: processed entry %d in %0.2f [s]\n', progname , i, tim);
        end
    end
end





