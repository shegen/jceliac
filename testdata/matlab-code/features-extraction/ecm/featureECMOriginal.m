function F = featureECMOriginal(I, varargin)

p = inputParser;
p.addRequired('I');                               % gray value image
p.addParamValue('distances', 1:1, @isnumeric);    % distances vector
p.parse(I, varargin{:});

d = p.Results.distances;

dirVec = [-1 -1; ...
          -1 0; ...
          -1 1; ...
          0 -1; ...
          0 1; ...
          1 -1; ...
          1 0; ...
          1 1];          

Template = zeros([3 3 8]);
Template(:,:,1) = [1 2 1   ; 0 0 0  ;-1 -2 -1];
Template(:,:,2) = [2 1 0   ; 1 0 -1 ;0 -1 -2 ];
Template(:,:,3) = [1 0 -1  ;2 0 -2  ; 1 0 -1 ];
Template(:,:,4) = [0 -1 -2 ; 1 0 -1 ; 2 1 0  ];
Template(:,:,5) = [-1 -2 -1; 0 0 0  ; 1 2 1  ];
Template(:,:,6) = [-2 -1 0 ;-1 0 1  ; 0 1 2  ];
Template(:,:,7) = [-1 0 1  ;-2 0 2  ;-1 0 1  ];
Template(:,:,8) = [0 1 2   ;-1 0 1  ;-2 -1 0 ];
      



I = double(I);

Edges = zeros(size(I,1), size(I,2),8);
for i = 1:8
    Edges(:,:,i) = imfilter(I,Template(:,:,i),'symmetric');
end  

[EdgeMax, EdgeMaxIndex] = max(Edges,[],3);

th = 0.25 * max(EdgeMax(:));
EdgeMask = EdgeMax >= th; 
EdgeMaxIndexMasked = EdgeMask .* EdgeMaxIndex;

result = zeros(64*length(d), 1);
for distIndex = 1:length(d)
    distance = d(distIndex);
    tmpResult = zeros(8, 8);
    for i=1:8
        F=graycomatrix(EdgeMaxIndexMasked,'Offset',distance.*dirVec(i,:),'GrayLimits',[0 8],'NumLevels',9);
        F=F(2:end,2:end);
        tmpResult = tmpResult + F;
    end    
    tmpResult = tmpResult / 8;

    result((distIndex-1)*64+1:(distIndex-1)*64+1+63) = tmpResult(:);
end
F = result(:);




end





