/*
SQLyog Community Edition- MySQL GUI v8.14 
MySQL - 5.0.77 : Database - HibernateTestDB
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`HibernateTestDB` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `HibernateTestDB`;

/*Table structure for table `CLResultConfiguration` */

DROP TABLE IF EXISTS `CLResultConfiguration`;

CREATE TABLE `CLResultConfiguration` (
  `PK` bigint(20) NOT NULL auto_increment COMMENT 'one-to-one to all subtables of type CLResultConfiguration',
  PRIMARY KEY  (`PK`)
) ENGINE=InnoDB AUTO_INCREMENT=78599 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `CLResultConfigurationKNN` */

DROP TABLE IF EXISTS `CLResultConfigurationKNN`;

CREATE TABLE `CLResultConfigurationKNN` (
  `PK` bigint(20) NOT NULL,
  `KValue` bigint(20) default NULL,
  `DistanceMeasure` varchar(56) default NULL,
  PRIMARY KEY  (`PK`),
  CONSTRAINT `FK_CLResultConfigurationKNN` FOREIGN KEY (`PK`) REFERENCES `CLResultConfiguration` (`PK`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `CVResultConfiguration` */

DROP TABLE IF EXISTS `CVResultConfiguration`;

CREATE TABLE `CVResultConfiguration` (
  `PK` bigint(20) NOT NULL auto_increment COMMENT 'one-to-one to all subtables of type CVResultConfiguration',
  PRIMARY KEY  (`PK`)
) ENGINE=InnoDB AUTO_INCREMENT=78599 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `CVResultConfigurationKFold` */

DROP TABLE IF EXISTS `CVResultConfigurationKFold`;

CREATE TABLE `CVResultConfigurationKFold` (
  `PK` bigint(20) NOT NULL,
  `Folds` bigint(20) default NULL,
  PRIMARY KEY  (`PK`),
  CONSTRAINT `FK_CVResultConfigurationKFold` FOREIGN KEY (`PK`) REFERENCES `CVResultConfiguration` (`PK`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `CVResultConfigurationLOPO` */

DROP TABLE IF EXISTS `CVResultConfigurationLOPO`;

CREATE TABLE `CVResultConfigurationLOPO` (
  `PK` bigint(20) NOT NULL,
  `PatientCount` bigint(20) default NULL,
  PRIMARY KEY  (`PK`),
  CONSTRAINT `FK_CVResultConfigurationLOPO` FOREIGN KEY (`PK`) REFERENCES `CVResultConfiguration` (`PK`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `ClassificationOutcome` */

DROP TABLE IF EXISTS `ClassificationOutcome`;

CREATE TABLE `ClassificationOutcome` (
  `pk` bigint(20) NOT NULL auto_increment,
  `SignalIdentifier` varchar(256) NOT NULL,
  `FrameNumber` binary(1) NOT NULL,
  `OriginalClass` bigint(20) NOT NULL,
  `EstimatedClass` bigint(20) NOT NULL,
  `Guessed` tinyint(1) NOT NULL,
  `Tied` tinyint(1) NOT NULL,
  `ResultPK` bigint(20) NOT NULL,
  `FEConfigPK` bigint(20) NOT NULL,
  `CLConfigPK` bigint(20) NOT NULL,
  `CVConfigPK` bigint(20) NOT NULL,
  `OPTConfigPK` bigint(20) NOT NULL,
  PRIMARY KEY  (`pk`),
  KEY `FK_FeatureVectorClassificationOutcome` (`ResultPK`),
  KEY `FK_FEResultConfiguration` (`FEConfigPK`),
  KEY `FK_CLResultConfiguration` (`CLConfigPK`),
  KEY `FK_CVResultConfiguration` (`CVConfigPK`),
  KEY `FK_OPTResultConfiguration` (`OPTConfigPK`),
  CONSTRAINT `FK_ClassificationOutcome` FOREIGN KEY (`FEConfigPK`) REFERENCES `FEResultConfiguration` (`PK`) ON DELETE CASCADE,
  CONSTRAINT `FK_CLResultConfiguration` FOREIGN KEY (`CLConfigPK`) REFERENCES `CLResultConfiguration` (`PK`) ON DELETE CASCADE,
  CONSTRAINT `FK_CVResultConfiguration` FOREIGN KEY (`CVConfigPK`) REFERENCES `CVResultConfiguration` (`PK`) ON DELETE CASCADE,
  CONSTRAINT `FK_FeatureVectorClassificationOutcome` FOREIGN KEY (`ResultPK`) REFERENCES `Result` (`pk`) ON DELETE CASCADE,
  CONSTRAINT `FK_FEResultConfiguration` FOREIGN KEY (`FEConfigPK`) REFERENCES `FEResultConfiguration` (`PK`) ON DELETE CASCADE,
  CONSTRAINT `FK_OPTResultConfiguratione` FOREIGN KEY (`OPTConfigPK`) REFERENCES `OPTResultConfiguration` (`PK`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=78582 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `Experiment` */

DROP TABLE IF EXISTS `Experiment`;

CREATE TABLE `Experiment` (
  `pk` bigint(20) NOT NULL auto_increment,
  `ExperimentName` varchar(64) NOT NULL,
  `FeatureExtractionMethod` varchar(64) NOT NULL,
  `FeatureOptimizationMethod` varchar(64) NOT NULL,
  `ClassificationMethod` varchar(64) NOT NULL,
  `ValidationMethod` varchar(64) NOT NULL,
  `OptimizationType` varchar(64) NOT NULL,
  `Hostname` varchar(256) NOT NULL,
  `ExperimentState` varchar(64) NOT NULL,
  `ExperimentUpdate` datetime NOT NULL,
  `ClassCount` bigint(20) NOT NULL,
  `ProjectPK` bigint(20) NOT NULL,
  `TrainingData` varchar(256) default NULL,
  `TestData` varchar(256) default NULL,
  PRIMARY KEY  (`pk`),
  KEY `FK_ExperimentTable` (`ProjectPK`),
  CONSTRAINT `FK_ExperimentTable` FOREIGN KEY (`ProjectPK`) REFERENCES `Project` (`PK`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `FEResultConfiguration` */

DROP TABLE IF EXISTS `FEResultConfiguration`;

CREATE TABLE `FEResultConfiguration` (
  `PK` bigint(20) NOT NULL auto_increment COMMENT 'PK is one-to-one to all subtables of type FEResultConfiguration',
  PRIMARY KEY  (`PK`)
) ENGINE=InnoDB AUTO_INCREMENT=78599 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `FEResultConfigurationLBP` */

DROP TABLE IF EXISTS `FEResultConfigurationLBP`;

CREATE TABLE `FEResultConfigurationLBP` (
  `PK` bigint(20) NOT NULL,
  `Scales` varchar(256) default NULL,
  `ColorChannels` varchar(256) default NULL,
  `Subbands` varchar(256) default NULL,
  `WaveletSubbands` varchar(256) default '-',
  `WaveletScales` varchar(256) default '-',
  `UniformPatterns` tinyint(1) default NULL,
  `Thresholds` varchar(256) default '""',
  `ContrastBins` int(11) default NULL,
  PRIMARY KEY  (`PK`),
  CONSTRAINT `FK_FEResultConfigurationLBP` FOREIGN KEY (`PK`) REFERENCES `FEResultConfiguration` (`PK`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `OPTResultConfiguration` */

DROP TABLE IF EXISTS `OPTResultConfiguration`;

CREATE TABLE `OPTResultConfiguration` (
  `PK` bigint(20) NOT NULL auto_increment COMMENT 'one-to-one to all subtables of type OPTResulConfiguration',
  PRIMARY KEY  (`PK`)
) ENGINE=InnoDB AUTO_INCREMENT=78598 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `OPTResultConfigurationSBS` */

DROP TABLE IF EXISTS `OPTResultConfigurationSBS`;

CREATE TABLE `OPTResultConfigurationSBS` (
  `PK` bigint(20) NOT NULL,
  `MinDimension` bigint(20) default NULL,
  `OptimizingCriterion` varchar(56) default NULL,
  PRIMARY KEY  (`PK`),
  CONSTRAINT `FK_OPTResultConfigurationSBS` FOREIGN KEY (`PK`) REFERENCES `OPTResultConfiguration` (`PK`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `OPTResultConfigurationSFS` */

DROP TABLE IF EXISTS `OPTResultConfigurationSFS`;

CREATE TABLE `OPTResultConfigurationSFS` (
  `PK` bigint(20) NOT NULL,
  `MaxDimension` bigint(20) default NULL,
  `OptimizingCriterion` varchar(56) default NULL,
  PRIMARY KEY  (`PK`),
  CONSTRAINT `FK_OPTResultConfigurationSFS` FOREIGN KEY (`PK`) REFERENCES `OPTResultConfiguration` (`PK`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `Project` */

DROP TABLE IF EXISTS `Project`;

CREATE TABLE `Project` (
  `PK` bigint(20) NOT NULL,
  `Name` varchar(56) NOT NULL,
  `Date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`PK`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `Result` */

DROP TABLE IF EXISTS `Result`;

CREATE TABLE `Result` (
  `pk` bigint(20) NOT NULL auto_increment,
  `RateC0` double default '-1',
  `RateC1` double default '-1',
  `RateC2` double default '-1',
  `RateC3` double default '-1',
  `RateC4` double default '-1',
  `RateC5` double default '-1',
  `RateC6` double default '-1',
  `OverallRate` double NOT NULL,
  `IdentifierC0` varchar(32) default NULL,
  `IdentifierC1` varchar(32) default NULL,
  `IdentifierC2` varchar(32) default NULL,
  `IdentifierC3` varchar(32) default NULL,
  `IdentifierC4` varchar(32) default NULL,
  `IdentifierC5` varchar(32) default NULL,
  `IdentifierC6` varchar(32) default NULL,
  `ExperimentPK` bigint(20) NOT NULL,
  `Date` timestamp NULL default CURRENT_TIMESTAMP,
  `FEConfigPK` bigint(20) default NULL COMMENT 'Feature Extraction Configuration PK',
  `CConfigPK` bigint(20) default NULL COMMENT 'Classification Configuration PK',
  PRIMARY KEY  (`pk`),
  KEY `FK_Result` (`ExperimentPK`),
  KEY `FK_FEConfig` (`FEConfigPK`),
  CONSTRAINT `FK_Result` FOREIGN KEY (`ExperimentPK`) REFERENCES `Experiment` (`pk`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=274 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
