#!/usr/bin/perl


$teststring =    '<Experiment>
        <exp-param field="experimentType">online</exp-param>
        <exp-param field="experimentName">OVERFIT_ELBP_%d</exp-param>
        <exp-param field="projectName">Overfit</exp-param>
        <exp-param field="trainingDataBaseDirectory">/home/shegen/Data/LNPO-Data/%d</exp-param>

        <FeatureExtractionMethod method="elbp">
            <fem-param field="useColor"> true </fem-param>
            <fem-param field="useUniformPatterns"> true </fem-param>
            <fem-param field="maxScale"> 3 </fem-param>
            <fem-param field="minScale"> 1 </fem-param>
        </FeatureExtractionMethod>

        <ClassificationMethod method="knn">
            <cm-param field="kMax"> 25 </cm-param>
            <cm-param field="kMin"> 1 </cm-param>
            <cm-param field="crossValidationType"> LOOCV </cm-param>
            <cm-param field="optimizationType"> OUTER </cm-param>
        </ClassificationMethod>
        <OptimizationMethod method="SFS">
        </OptimizationMethod>
    </Experiment>';


for($index = 0; $index < 41; $index++)
{
	$result = sprintf($teststring, $index, $index);
	print $result;
}



